﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="WarehouseP.aspx.cs" Inherits="ArrowLabs.Licence.Portal.Pages.WarehouseP" %>
 
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
             <style>
                                                        #taskDocument {
    overflow-y:scroll;
} 
                                                                         #ticketingViewCard {
    overflow-y:scroll;
} 
                 #newAssetModal {
    overflow-y:scroll;
} 
              #pswd_info{
    position:absolute;
    bottom: -180px;
    bottom: -115px\9; /* IE Specific */
    right:55px;
    width:250px;
    padding:15px;
    background:#fefefe;
    font-size:.875em;
    border-radius:5px;
    box-shadow:0 1px 3px #ccc;
    border:1px solid #ddd;
    z-index : 9999;
}
              #pswd_info h4 {
    margin:0 0 10px 0;
    padding:0;
    font-weight:normal;
}
              #pswd_info::before {
    content: "\25B2";
    position:absolute;
    top:-12px;
    left:45%;
    font-size:14px;
    line-height:14px;
    color:#ddd;
    text-shadow:none;
    display:block;
}
#pswd_info {
    display:none;
} 
              .invalid {
    /*background:url(../images/invalid.png) no-repeat 0 50%;*/
    padding-left:22px;
    line-height:24px;
    color:#ec3f41;
}
.valid {
    /*background:url(../images/valid.png) no-repeat 0 50%;*/
    padding-left:22px;
    line-height:24px;
    color:#3a7d34;
}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<script type="text/javascript" src="https://testportalcdn.azureedge.net/Scripts/JsBarcode.code128.js"></script>

    <script type="text/javascript">

        $j = jQuery.noConflict();
        var chat;
        $j(function () {
            try {
                var name = btoa('<%=senderName%>');
                var qs = "name=" + name;
                var url = "<%=ipaddress%>";
                //Set the hubs URL for the connection
                $j.connection.hub.url = url;
                $j.connection.hub.qs = qs;
                // Declare a proxy to reference the hub.
                chat = $j.connection.mIMSHub;
                // Create a function that the hub can call to broadcast messages.
                chat.client.addMessage = function (name, message) {
                    // Html encode display name and message.
                    var encodedName = $j('<div />').text(name).html();
                    var encodedMsg = $j('<div />').text(message).html();
                    // Add the message to the page.
                    //                    $('#discussion').append('<li><strong>' + encodedName
                    //                    + '</strong>:&nbsp;&nbsp;' + encodedMsg + '</li>');
                };
                // Get the user name and store it to prepend to messages.
                //                $('#displayname').val(prompt('Enter your name:', ''));
                // Set initial focus to message input box.

                // Start the connection.
                $j.connection.hub.start().done(function () {

                });
            }

            catch (err) {
                if ('<%=senderName%>' != 'superadmin') {
                    showError("Notification Service is not running or error occured while connecting. System will not let you login.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
                else {
                    showError("Error 44: Failed to connect to Notification Service!");
                }
            }
        });

        var loggedinUsername = '<%=senderName2%>';
        var mapIncidentLocation;
        var myMarkersIncidentLocation = new Array();
        var infoWindowIncidentLocation;
        var sourceLat = 25.2049808502197;
        var sourceLon = 55.2707939147949;
        var mapUrlLocation;
        var locationAllowed = false;
        var divArray = new Array();

        var rotate_factor = 0;
        var rotated = false;
        function startRot() {
            rotated = false;
            rotate_factor = 0;

        }
        function loadMe(e) {
            var newImageWidth = e.width;
            var newImageHeight = e.height;
            var canvasWidth = 400;
            var canvasHeight = 400;
            var imageAspectRatio = e.width / e.height;
            var canvasAspectRatio = canvasWidth / canvasHeight;
            if (imageAspectRatio < canvasAspectRatio) {
                newImageHeight = canvasHeight;
                newImageWidth = e.width * (newImageHeight / e.height);
            }
            else if (imageAspectRatio > canvasAspectRatio) {
                newImageWidth = canvasWidth
                newImageHeight = e.height * (newImageWidth / e.width);
            }
            var margins;
            if (newImageWidth == 400) {
                jQuery(e).css({
                    'width': newImageWidth,
                    'height': newImageHeight,
                    'margin-top': '10%'
                });
            }
            else {
                jQuery(e).css({
                    'width': newImageWidth,
                    'height': newImageHeight
                });
            }
        }
        function rotateMe(e) {
            try {
                rotate_factor += 1;
                var rotate_angle = (90 * rotate_factor) % 360;
                if (rotated) {

                    if (e.width == 400) {
                        jQuery(e).css({
                            'margin-top': '10%',
                            '-webkit-transform': 'rotate(' + rotate_angle + 'deg)',
                            '-moz-transform': 'rotate(' + rotate_angle + 'deg)',
                            '-o-transform': 'rotate(' + rotate_angle + 'deg)',
                            '-ms-transform': 'rotate(' + rotate_angle + 'deg)',
                            'transform': 'rotate(' + rotate_angle + 'deg)'
                        });
                    }
                    else {
                        jQuery(e).css({
                            '-webkit-transform': 'rotate(' + rotate_angle + 'deg)',
                            '-moz-transform': 'rotate(' + rotate_angle + 'deg)',
                            '-o-transform': 'rotate(' + rotate_angle + 'deg)',
                            '-ms-transform': 'rotate(' + rotate_angle + 'deg)',
                            'transform': 'rotate(' + rotate_angle + 'deg)'
                        });
                    }
                    rotated = false;
                }
                else {
                    if (e.width == 400 && e.height == 300) {
                        jQuery(e).css({
                            'margin-top': '10%',
                            '-webkit-transform': 'rotate(' + rotate_angle + 'deg)',
                            '-moz-transform': 'rotate(' + rotate_angle + 'deg)',
                            '-o-transform': 'rotate(' + rotate_angle + 'deg)',
                            '-ms-transform': 'rotate(' + rotate_angle + 'deg)',
                            'transform': 'rotate(' + rotate_angle + 'deg)'
                        });
                    }
                    else if (e.width == 400 && e.height < 300) {
                        jQuery(e).css({
                            'margin-top': '20%',
                            '-webkit-transform': 'rotate(' + rotate_angle + 'deg)',
                            '-moz-transform': 'rotate(' + rotate_angle + 'deg)',
                            '-o-transform': 'rotate(' + rotate_angle + 'deg)',
                            '-ms-transform': 'rotate(' + rotate_angle + 'deg)',
                            'transform': 'rotate(' + rotate_angle + 'deg)'
                        });
                    }
                    else {
                        jQuery(e).css({
                            '-webkit-transform': 'rotate(' + rotate_angle + 'deg)',
                            '-moz-transform': 'rotate(' + rotate_angle + 'deg)',
                            '-o-transform': 'rotate(' + rotate_angle + 'deg)',
                            '-ms-transform': 'rotate(' + rotate_angle + 'deg)',
                            'transform': 'rotate(' + rotate_angle + 'deg)'
                        });
                    }
                    rotated = true;
                }
            }
            catch (err) {
                alert(err);
            }
        }

        //User-profile
        function changePassword() {
            try {
                var newPw = document.getElementById("newPwInput").value;
                var confPw = document.getElementById("confirmPwInput").value;
                var isErr = false;
                if (!isErr) {
                    if (!letterGood) {
                        showAlert('Password does not contain letter');
                        isErr = true;
                    }
                    if (!isErr) {
                        if (!capitalGood) {
                            showAlert('Password does not contain capital letter');
                            isErr = true;
                        }
                    }
                    if (!isErr) {
                        if (!numGood) {
                            showAlert('Password does not contain number');
                            isErr = true;
                        }
                    }
                    if (!isErr) {
                        if (!lengthGood) {
                            showAlert('Password length not enough');
                            isErr = true;
                        }
                    }
                }
                if (!isErr) {
                    if (newPw == confPw && newPw != "" && confPw != "") {
                        jQuery.ajax({
                            type: "POST",
                            url: "WarehouseP.aspx/changePW",
                            data: "{'id':'0','password':'" + confPw + "','uname':'" + loggedinUsername + "'}",
                            async: false,
                            dataType: "json",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                if (data.d != "LOGOUT") {
                                    jQuery('#changePasswordModal').modal('hide');
                                    document.getElementById('successincidentScenario').innerHTML = "Password successfully changed";
                                    jQuery('#successfulDispatch').modal('show');
                                    document.getElementById("newPwInput").value = "";
                                    document.getElementById("confirmPwInput").value = "";
                                    document.getElementById("oldPwInput").value = confPw;
                                }
                                else {
                                    showError("Session has expired. Kindly login again.");
                                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                                }
                            },
                            error: function () {
                                showError("Session timeout. Kindly login again.");
                                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                            }
                        });
                    }
                    else {
                        showAlert("Kindly match new password with confirm password.")
                    }
                }
            }
            catch (ex)
            { showAlert('Error 60: Problem loading page element.-' + ex) }
        }
        function editUnlock() {
            document.getElementById("profilePhoneNumberDIV").style.display = "none";
            document.getElementById("profilePhoneNumberEditDIV").style.display = "block";
            document.getElementById("editProfileA").style.display = "none";
            document.getElementById("saveProfileA").style.display = "block";
            document.getElementById("profileEmailAddDIV").style.display = "none";
            document.getElementById("profileEmailAddEditDIV").style.display = "block";
            document.getElementById("userFullnameSpanDIV").style.display = "none";
            document.getElementById("userFullnameSpanEditDIV").style.display = "block";
            if (document.getElementById('profileRoleName').innerHTML == "User") {
                document.getElementById("superviserInfoDIV").style.display = "none";
                document.getElementById("managerInfoDIV").style.display = "block";
                document.getElementById("dirInfoDIV").style.display = "none";


            }
            else if (document.getElementById('profileRoleName').innerHTML == "Manager") {
                document.getElementById("superviserInfoDIV").style.display = "none";
                document.getElementById("managerInfoDIV").style.display = "none";
                document.getElementById("dirInfoDIV").style.display = "block";
            }
        }
        function editJustLock() {
            document.getElementById("profilePhoneNumberDIV").style.display = "block";
            document.getElementById("userFullnameSpanEditDIV").style.display = "none";
            document.getElementById("profilePhoneNumberEditDIV").style.display = "none";
            document.getElementById("editProfileA").style.display = "block";
            document.getElementById("saveProfileA").style.display = "none";
            document.getElementById("profileEmailAddDIV").style.display = "block";
            document.getElementById("profileEmailAddEditDIV").style.display = "none";
            document.getElementById("userFullnameSpanDIV").style.display = "block";
            document.getElementById("superviserInfoDIV").style.display = "block";
            document.getElementById("managerInfoDIV").style.display = "none";
            document.getElementById("dirInfoDIV").style.display = "none";
        }
        function editLock() {
            document.getElementById("profilePhoneNumberDIV").style.display = "block";
            document.getElementById("userFullnameSpanEditDIV").style.display = "none";
            document.getElementById("profilePhoneNumberEditDIV").style.display = "none";
            document.getElementById("editProfileA").style.display = "block";
            document.getElementById("saveProfileA").style.display = "none";
            document.getElementById("profileEmailAddDIV").style.display = "block";
            document.getElementById("profileEmailAddEditDIV").style.display = "none";
            document.getElementById("userFullnameSpanDIV").style.display = "block";
            document.getElementById("superviserInfoDIV").style.display = "block";
            document.getElementById("managerInfoDIV").style.display = "none";
            document.getElementById("dirInfoDIV").style.display = "none";
            var role = document.getElementById('UserRoleSelector').value;
            var roleid = 0;
            var supervisor = 0;
            var retVal = saveUserProfile(0, 0, document.getElementById('userFirstnameSpan').value, document.getElementById('userLastnameSpan').value, document.getElementById('profileEmailAddEdit').value, document.getElementById('profilePhoneNumberEdit').value, 0, 0, supervisor, roleid, document.getElementById('imagePath').text)
            if (retVal > 0) {
                assignUserProfileData();
            }
            else {
                showAlert("Error 61: Problem occured while trying to update user profile.")
            }

        }
        function saveUserProfile(id, username, firstname, lastname, emailaddress, phonenumber, password, devicetype, supervisor, role, img) {
            var output = 0;
            jQuery.ajax({
                type: "POST",
                url: "WarehouseP.aspx/addUserProfile",
                data: "{'id':'" + id + "','username':'" + username + "','firstname':'" + firstname + "','lastname':'" + lastname + "','emailaddress':'" + emailaddress + "','phonenumber':'" + phonenumber + "','password':'" + password + "','devicetype':'" + devicetype + "','supervisor':'" + supervisor + "','role':'" + role + "','imgPath':'" + img + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
                output = data.d;
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
        return output;
    }
    function saveTZ() {
        var scountr = $("#countrySelect option:selected").text();
        jQuery.ajax({
            type: "POST",
            url: "WarehouseP.aspx/saveTZ",
            data: "{'id':'" + scountr + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        document.getElementById('successincidentScenario').innerHTML = "Successfully changed timezone";
                        jQuery('#successfulDispatch').modal('show');
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function getserverInfo() {
            jQuery.ajax({
                type: "POST",
                url: "WarehouseP.aspx/getServerData",
                data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {

                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {

                    document.getElementById('mobileRemaining').value = data.d[9];
                    document.getElementById('mobileTotal').value = data.d[10];
                    document.getElementById('mobileUsed').value = data.d[11];

                    document.getElementById("countrySelect").value = data.d[28];
                    jQuery('#countrySelect').selectpicker('val', data.d[28]);

                    document.getElementById('surveillanceCheck').checked = false;
                    document.getElementById('notificationCheck').checked = false;
                    document.getElementById('locationCheck').checked = false;
                    document.getElementById('ticketingCheck').checked = false;
                    document.getElementById('taskCheck').checked = false;
                    document.getElementById('incidentCheck').checked = false;
                    document.getElementById('warehouseCheck').checked = false;
                    document.getElementById('chatCheck').checked = false;
                    document.getElementById('collaborationCheck').checked = false;
                    document.getElementById('lfCheck').checked = false;
                    document.getElementById('dutyrosterCheck').checked = false;
                    document.getElementById('postorderCheck').checked = false;
                    document.getElementById('verificationCheck').checked = false;
                    document.getElementById('requestCheck').checked = false;
                    document.getElementById('dispatchCheck').checked = false;
                    document.getElementById('activityCheck').checked = false;

                    if (data.d[12] == "true")
                        document.getElementById('surveillanceCheck').checked = true;
                    if (data.d[13] == "true")
                        document.getElementById('notificationCheck').checked = true;
                    if (data.d[14] == "true")
                        document.getElementById('locationCheck').checked = true;
                    if (data.d[15] == "true")
                        document.getElementById('ticketingCheck').checked = true;
                    if (data.d[16] == "true")
                        document.getElementById('taskCheck').checked = true;
                    if (data.d[17] == "true")
                        document.getElementById('incidentCheck').checked = true;
                    if (data.d[18] == "true")
                        document.getElementById('warehouseCheck').checked = true;
                    if (data.d[19] == "true")
                        document.getElementById('chatCheck').checked = true;
                    if (data.d[20] == "true")
                        document.getElementById('collaborationCheck').checked = true;
                    if (data.d[21] == "true")
                        document.getElementById('lfCheck').checked = true;
                    if (data.d[22] == "true")
                        document.getElementById('dutyrosterCheck').checked = true;
                    if (data.d[23] == "true")
                        document.getElementById('postorderCheck').checked = true;
                    if (data.d[24] == "true")
                        document.getElementById('verificationCheck').checked = true;
                    if (data.d[25] == "true")
                        document.getElementById('requestCheck').checked = true;
                    if (data.d[26] == "true")
                        document.getElementById('dispatchCheck').checked = true;
                    if (data.d[27] == "true")
                        document.getElementById('activityCheck').checked = true;
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function assignUserProfileData() {
        try {
            $.ajax({
                type: "POST",
                url: "WarehouseP.aspx/getUserProfileData",
                data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        try {
                            document.getElementById('containerDiv2').style.display = "none";
                            document.getElementById('profileUserNameSpan').innerHTML = data.d[0];
                            document.getElementById('userFullnameSpan').innerHTML = data.d[1];
                            document.getElementById('profilePhoneNumber').innerHTML = data.d[2];
                            document.getElementById('profileEmailAdd').innerHTML = data.d[3];
                            document.getElementById('profileLastLocation').innerHTML = data.d[4];
                            document.getElementById('profileRoleName').innerHTML = data.d[5];
                            document.getElementById('profileManagerName').innerHTML = data.d[6];
                            document.getElementById('userStatusSpan').innerHTML = data.d[8];

                            if (document.getElementById('profileRoleName').innerHTML == "Customer") {
                                document.getElementById('containerDiv2').style.display = "block";
                                document.getElementById('defaultGenderDiv').style.display = "none";
                                getserverInfo();
                            }

                            var el = document.getElementById('userStatusIconSpan');
                            if (el) {
                                el.className = data.d[9];
                            }
                            document.getElementById('userprofileImgSrc').src = data.d[10];
                            document.getElementById('deviceTypesDiv').innerHTML = data.d[11];
                            document.getElementById('supervisorTypeSpan').innerHTML = data.d[12];

                            document.getElementById('userFirstnameSpan').value = data.d[13];
                            document.getElementById('userLastnameSpan').value = data.d[14];
                            document.getElementById('profilePhoneNumberEdit').value = data.d[2];
                            document.getElementById('profileEmailAddEdit').value = data.d[3];

                            document.getElementById('oldPwInput').value = data.d[16];

                            document.getElementById('userSiteDisplay').innerHTML = data.d[19];

                            document.getElementById('profileEmployeeId').innerHTML = data.d[21];
                            document.getElementById('profileGender').innerHTML = data.d[20];
                        }
                        catch (err) { alert(err) }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        catch (err)
        { alert('Error 60: Problem loading page element.-' + err) }
    }
    function forceLogout() {
        document.getElementById('<%= closingbtn.ClientID %>').click();
    }
    var viewpopupUP = false;
    function showTicketCardModal(id) {
        viewpopupUP = true;
        rowchoice(id);
    }
    function closeModal() {
        if (viewpopupUP) {
            window.close();
        }
        else {
            location.reload();
        }
        showLoader();
    }
    var ff;
    jQuery(function () {
        try {
            // Now that the DOM is fully loaded, create the dropzone, and setup the
            // event listeners
            Dropzone.autoDiscover = false;
            var myDropzone = new Dropzone("#dz-test", {
                init: function () {
                    //You can do this
                    jQuery('#regFoundItem').on("click", function (e) {
                        myDropzone.removeFile(ff);
                        document.getElementById("imagePath").text = "";
                    });
                    jQuery('#clearFoundI').on("click", function (e) {
                        myDropzone.removeFile(ff);
                        document.getElementById("imagePath").text = "";
                    });
                    jQuery('#addFoundSave').on("click", function (e) {
                        myDropzone.removeFile(ff);
                        document.getElementById("imagePath").text = "";
                    });
                }
            });
            myDropzone.on("addedfile", function (file) {



                /* Maybe display some more file information on your page */
                file.previewElement.addEventListener("click", function () {
                    myDropzone.removeFile(file);
                    document.getElementById("imagePath").text = '';
                });
                if (typeof document.getElementById("imagePath").text === 'undefined' || document.getElementById("imagePath").text == '') {
                    if (file.type != "image/jpeg" && file.type != "image/png") {
                        showAlert("Kindly provided a JPEG or PNG Image for upload");
                        this.removeFile(file);
                    }
                    else {
                        ff = file;
                        var data = new FormData();
                        data.append(file.name, file);
                        jQuery.ajax({
                            url: "../Handlers/ItemFoundImgUpload.ashx",
                            type: "POST",
                            data: data,
                            contentType: false,
                            processData: false,
                            success: function (result) {
                                if (result != "FAIL") {
                                    document.getElementById("imagePath").text = result.replace(/\\/g, "|")
                                    document.getElementById("results").innerHTML = "";
                                    document.getElementById("resultsHeader").style.display = "none";
                                }
                                else
                                    showError("Problem faced trying to upload image");
                            },
                            error: function (err) {
                            }
                        })
                    }
                }
                else {
                    showAlert("You can only add one image at a time kindly delete previous image");
                    this.removeFile(file);
                }
            });

            var myDropzoneReturn = new Dropzone("#dz-return");
            myDropzoneReturn.on("addedfile", function (file) {
                /* Maybe display some more file information on your page */
                file.previewElement.addEventListener("click", function () {
                    myDropzoneReturn.removeFile(file);
                    document.getElementById("imagePathReturn").text = '';
                });
                if (typeof document.getElementById("imagePathReturn").text === 'undefined' || document.getElementById("imagePathReturn").text == '') {
                    if (file.type != "image/jpeg" && file.type != "image/png") {
                        showAlert("Kindly provided a JPEG or PNG Image for upload");
                        this.removeFile(file);
                    }
                    else {
                        var data = new FormData();
                        data.append(file.name, file);
                        jQuery.ajax({
                            url: "../Handlers/ItemFoundImgUpload.ashx",
                            type: "POST",
                            data: data,
                            contentType: false,
                            processData: false,
                            success: function (result) {
                                if (result != "FAIL")
                                    document.getElementById("imagePathReturn").text = result.replace(/\\/g, "|")
                                else
                                    showError("Problem faced trying to upload image");
                            },
                            error: function (err) {
                            }
                        })
                    }
                }
                else {
                    showAlert("You can only add one image at a time kindly delete previous image");
                    this.removeFile(file);
                }
            });
            var myDropzoneDispose = new Dropzone("#dz-dispose");
            myDropzoneDispose.on("addedfile", function (file) {
                /* Maybe display some more file information on your page */
                file.previewElement.addEventListener("click", function () {
                    myDropzoneDispose.removeFile(file);
                    document.getElementById("imagePathDispose").text = '';
                });
                if (typeof document.getElementById("imagePathDispose").text === 'undefined' || document.getElementById("imagePathDispose").text == '') {
                    if (file.type != "image/jpeg" && file.type != "image/png") {
                        showAlert("Kindly provided a JPEG or PNG Image for upload");
                        this.removeFile(file);
                    }
                    else {
                        var data = new FormData();
                        data.append(file.name, file);
                        jQuery.ajax({
                            url: "../Handlers/ItemFoundImgUpload.ashx",
                            type: "POST",
                            data: data,
                            contentType: false,
                            processData: false,
                            success: function (result) {
                                if (result != "FAIL")
                                    document.getElementById("imagePathDispose").text = result.replace(/\\/g, "|");
                                else
                                    showError("Problem faced trying to upload image");
                            },
                            error: function (err) {
                            }
                        })
                    }
                }
                else {
                    showAlert("You can only add one image at a time kindly delete previous image");
                    this.removeFile(file);
                }
            });



            var myDropzoneItem = new Dropzone("#checkitemDZ");
            myDropzoneItem.on("addedfile", function (file) {
                /* Maybe display some more file information on your page */

                if (file.type != "image/jpeg" && file.type != "image/png" && file.type != "application/pdf") {
                    showAlert("Kindly provided a JPEG , PNG Image or PDF for upload");
                    this.removeFile(file);
                }
                else {
                    var data = new FormData();
                    data.append(file.name, file);
                    jQuery.ajax({
                        url: "../Handlers/MobileIncidentUpload.ashx",
                        type: "POST",
                        data: data,
                        contentType: false,
                        processData: false,
                        success: function (result) {
                            jQuery.ajax({
                                type: "POST",
                                url: "WarehouseP.aspx/attachFileToItemCheckOut",
                                data: "{'id':'" + document.getElementById('rowidChoice').value + "','filepath':'" + result.replace(/\\/g, "|") + "','uname':'" + loggedinUsername + "'}",
                                async: false,
                                dataType: "json",
                                contentType: "application/json; charset=utf-8",
                                success: function (data) {
                                    if (data.d == "LOGOUT") {
                                        showError("Session has expired. Kindly login again.");
                                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                                    } else {
                                        showAlert(data.d);
                                        rowchoice(document.getElementById('rowidChoice').value)
                                    }
                                },
                                error: function () {
                                    showError("Session timeout. Kindly login again.");
                                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                                }
                            });

                        },
                        error: function (err) {
                        }
                    })
                }
            });

            var myDropzonePost = new Dropzone("#dz-post");
            myDropzonePost.on("addedfile", function (file) {
                /* Maybe display some more file information on your page */

                if (file.type != "image/jpeg" && file.type != "image/png" && file.type != "application/pdf") {
                    showAlert("Kindly provided a JPEG , PNG Image or PDF for upload");
                    this.removeFile(file);
                }
                else {
                    var data = new FormData();
                    data.append(file.name, file);
                    jQuery.ajax({
                        url: "../Handlers/MobileIncidentUpload.ashx",
                        type: "POST",
                        data: data,
                        contentType: false,
                        processData: false,
                        success: function (result) {
                            document.getElementById("imagePostAttachment").text = result.replace(/\\/g, "|")
                            jQuery.ajax({
                                type: "POST",
                                url: "WarehouseP.aspx/attachFileToItem",
                                data: "{'id':'" + document.getElementById('rowidChoice').value + "','filepath':'" + document.getElementById("imagePostAttachment").text + "','uname':'" + loggedinUsername + "'}",
                                async: false,
                                dataType: "json",
                                contentType: "application/json; charset=utf-8",
                                success: function (data) {
                                    if (data.d == "LOGOUT") {
                                        showError("Session has expired. Kindly login again.");
                                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                                        } else {
                                            showAlert(data.d);
                                            oldDivContainers();
                                            foundInsertAttachmentTabData(document.getElementById('rowidChoice').value);
                                            foundInsertAttachmentData(document.getElementById('rowidChoice').value);
                                            finfotabDefault();
                                            fdispatchAssignMapTab();
                                        }
                                    },
                                    error: function () {
                                        showError("Session timeout. Kindly login again.");
                                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                                    }
                                });

                            },
                            error: function (err) {
                            }
                        })
                    }
            });

                var myDropzoneAsset = new Dropzone("#dz-asset");
                myDropzoneAsset.on("addedfile", function (file) {
                    /* Maybe display some more file information on your page */
                    file.previewElement.addEventListener("click", function () {
                        myDropzoneAsset.removeFile(file);
                        document.getElementById("imagePathAsset").text = '';
                    });
                    if (typeof document.getElementById("imagePathAsset").text === 'undefined' || document.getElementById("imagePathAsset").text == '') {
                        if (file.type != "image/jpeg" && file.type != "image/png") {
                            showAlert("Kindly provided a JPEG or PNG Image for upload");
                            this.removeFile(file);
                        }
                        else {
                            var data = new FormData();
                            data.append(file.name, file);
                            jQuery.ajax({
                                url: "../Handlers/MobileIncidentUpload.ashx",
                                type: "POST",
                                data: data,
                                contentType: false,
                                processData: false,
                                success: function (result) {
                                    document.getElementById("imagePathAsset").text = result.replace(/\\/g, "|");

                                    jQuery.ajax({
                                        type: "POST",
                                        url: "WarehouseP.aspx/attachFileToAsset",
                                        data: "{'id':'" + document.getElementById('newAssetID').value + "','filepath':'" + document.getElementById("imagePathAsset").text + "','uname':'" + loggedinUsername + "'}",
                                        async: false,
                                        dataType: "json",
                                        contentType: "application/json; charset=utf-8",
                                        success: function (data) {
                                            if (data.d == "LOGOUT") {
                                                showError("Session has expired. Kindly login again.");
                                                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                                            }
                                            else {
                                                showAlert(data.d);
                                                myDropzoneAsset.removeFile(file);
                                                assetinsertAttachmentTabData(document.getElementById('newAssetID').value);
                                                document.getElementById("imagePathAsset").text = '';
                                                //rowchoiceTicket(document.getElementById('rowidChoiceTicket').value);
                                                //document.getElementById("pdfloadingAcceptTick").style.display = "none";
                                            }
                                        },
                                        error: function () {
                                            showError("Session timeout. Kindly login again.");
                                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                                        }
                                    }); 
                                },
                                error: function (err) {
                                }
                            })
                        }
                    }
                    else {
                        showAlert("You can only add one image at a time kindly delete previous image");
                        this.removeFile(file);
                    }
            });

            var myDropzonespareAsset = new Dropzone("#dz-spareasset", {
                init: function () {
                    //You can do this
                    jQuery('#newassetA').on("click", function (e) {
                        myDropzonespareAsset.removeFile(ff);
                        document.getElementById("imagePathPartAsset").text = "";
                    });
                }
            });
                myDropzonespareAsset.on("addedfile", function (file) {
                    /* Maybe display some more file information on your page */
                    file.previewElement.addEventListener("click", function () {
                        myDropzonespareAsset.removeFile(file);
                        document.getElementById("imagePathPartAsset").text = '';
                    });
                    if (typeof document.getElementById("imagePathPartAsset").text === 'undefined' || document.getElementById("imagePathPartAsset").text == '') {
                        if (file.type != "image/jpeg" && file.type != "image/png") {
                            showAlert("Kindly provided a JPEG or PNG Image for upload");
                            this.removeFile(file);
                        }
                        else {
                            ff = file;
                            var data = new FormData();
                            data.append(file.name, file);
                            jQuery.ajax({
                                url: "../Handlers/MobileIncidentUpload.ashx",
                                type: "POST",
                                data: data,
                                contentType: false,
                                processData: false,
                                success: function (result) {
                                    document.getElementById("imagePathPartAsset").text = result.replace(/\\/g, "|");
                                },
                                error: function (err) {
                                }
                            })
                        }
                    }
                    else {
                        showAlert("You can only add one image at a time kindly delete previous image");
                        this.removeFile(file);
                    }
                });

            var newmyDropzonespareAsset = new Dropzone("#dz-newspareasset", {
                init: function () {
                    //You can do this
                    jQuery('#clearnewSpareA').on("click", function (e) {
                        newmyDropzonespareAsset.removeFile(fff);
                        document.getElementById("newimagePathPartAsset").text = "";
                    });
                }
            });
                newmyDropzonespareAsset.on("addedfile", function (file) {
                    /* Maybe display some more file information on your page */
                    file.previewElement.addEventListener("click", function () {
                        myDropzonespareAsset.removeFile(file);
                        document.getElementById("newimagePathPartAsset").text = '';
                    });
                    if (typeof document.getElementById("newimagePathPartAsset").text === 'undefined' || document.getElementById("newimagePathPartAsset").text == '') {
                        if (file.type != "image/jpeg" && file.type != "image/png") {
                            showAlert("Kindly provided a JPEG or PNG Image for upload");
                            this.removeFile(file);
                        }
                        else {
                            fff = file;
                            var data = new FormData();
                            data.append(file.name, file);
                            jQuery.ajax({
                                url: "../Handlers/MobileIncidentUpload.ashx",
                                type: "POST",
                                data: data,
                                contentType: false,
                                processData: false,
                                success: function (result) {
                                    document.getElementById("newimagePathPartAsset").text = result.replace(/\\/g, "|");
                                },
                                error: function (err) {
                                }
                            })
                        }
                    }
                    else {
                        showAlert("You can only add one image at a time kindly delete previous image");
                        this.removeFile(file);
                    }
                });
            }
        catch (err) {
            alert(err);
        }
    })
        var ff;
        var fff;
        function getReminderSelectorDates() {
            var select1 = document.getElementById('foundTime');
            var select2 = document.getElementById('receiveTime');
            var select3 = document.getElementById('FromTime');
            var select4 = document.getElementById('ToTime');
            jQuery.ajax({
                type: "POST",
                url: "WarehouseP.aspx/getReminderSelectorDates",
                data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    for (var i = 0; i < data.d.length; i++) {
                        var opt = document.createElement('option');
                        opt.value = data.d[i];
                        opt.innerHTML = data.d[i];
                        select1.appendChild(opt);
                        var opt2 = document.createElement('option');
                        opt2.value = data.d[i];
                        opt2.innerHTML = data.d[i];
                        select2.appendChild(opt2);
                        var opt3 = document.createElement('option');
                        opt3.value = data.d[i];
                        opt3.innerHTML = data.d[i];
                        select3.appendChild(opt3);
                        var opt4 = document.createElement('option');
                        opt4.value = data.d[i];
                        opt4.innerHTML = data.d[i];
                        select4.appendChild(opt4);
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function cancelClick() {
        document.getElementById("fDisposeHolder").style.display = "none";
        document.getElementById("fReturnHolder").style.display = "none";
        document.getElementById("fdivAttachmentHolder").style.display = "block";
        document.getElementById("processHandleDiv").style.display = "none";
        document.getElementById("returnHandleDiv").style.display = "none";
        document.getElementById("foundDiv").style.display = "block";
        document.getElementById("diposeHandleDiv").style.display = "none";
    }
    function returnClick() {
        document.getElementById("fDisposeHolder").style.display = "none";
        document.getElementById("fReturnHolder").style.display = "block";
        document.getElementById("fdivAttachmentHolder").style.display = "none";
        document.getElementById("returnHandleDiv").style.display = "block";
        document.getElementById("processHandleDiv").style.display = "none";
        document.getElementById("foundDiv").style.display = "none";
        document.getElementById("diposeHandleDiv").style.display = "none";
    }
    function disposeClick() {
        document.getElementById("fDisposeHolder").style.display = "block";
        document.getElementById("fReturnHolder").style.display = "none";
        document.getElementById("fdivAttachmentHolder").style.display = "none";

        document.getElementById("returnHandleDiv").style.display = "none";
        document.getElementById("processHandleDiv").style.display = "none";
        document.getElementById("foundDiv").style.display = "none";
        document.getElementById("diposeHandleDiv").style.display = "block";

    }
    function ownerTypeSelectChange(e) {
        if (e.options[e.selectedIndex].value == "In-house-guest") {
            document.getElementById("roomNumberDiv").style.display = "block";
        }
        else {
            document.getElementById("roomNumberDiv").style.display = "none";
        }
    }
    function SelectCameraTypeOnChange(e) {
        if (e.options[e.selectedIndex].value == "Webcam") {
            document.getElementById("webcameraUploadDIV").style.display = "block";

            document.getElementById("cameraUploadDIV").style.display = "none";
            document.getElementById("webcameraCapture").style.display = "block";
            myDropzone.removeFile(file);
        }
        else {
            document.getElementById("cameraUploadDIV").style.display = "block";
            document.getElementById("webcameraUploadDIV").style.display = "none";

            document.getElementById("webcameraCapture").style.display = "none";
            document.getElementById("results").innerHTML = "";
            document.getElementById("resultsHeader").style.display = "none";
            document.getElementById("imagePath").text = '';
        }
    }
    function enquiryTypeSubSelectChange(e) {

        try {

            document.getElementById('enquiryTypeSubSelect').innerHTML = "";
            var select = document.getElementById("enquiryTypeSubSelect");
            var optt = document.createElement('option');
            optt.value = 'Please Select';
            optt.innerHTML = 'Please Select';
            select.appendChild(optt);
            jQuery.ajax({
                type: "POST",
                url: "WarehouseP.aspx/getSubItem",
                data: "{'id':'" + e.options[e.selectedIndex].value + "','uname':'" + loggedinUsername + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        for (var i = 0; i < data.d.length; i++) {
                            var res = data.d[i].split('_');
                            var opt = document.createElement('option');
                            opt.value = res[0];
                            opt.innerHTML = res[0];
                            select.appendChild(opt);
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        catch (ex) {
            alert(ex);
        }
    }
    function searchItemTypeSelectChange(e) {
        try {
            document.getElementById('searchItemSubSelect').innerHTML = "";
            var select = document.getElementById("searchItemSubSelect");
            var optt = document.createElement('option');
            optt.value = 'Please Select';
            optt.innerHTML = 'Please Select';
            select.appendChild(optt);
            jQuery.ajax({
                type: "POST",
                url: "WarehouseP.aspx/getSubItem",
                data: "{'id':'" + e.options[e.selectedIndex].value + "','uname':'" + loggedinUsername + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        for (var i = 0; i < data.d.length; i++) {
                            var res = data.d[i].split('_');
                            var opt = document.createElement('option');
                            opt.value = res[0];
                            opt.innerHTML = res[0];
                            select.appendChild(opt);
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        catch (ex) {
            alert(ex);
        }
    }
    function ItemTypeSelectChange(e) {

        try {

            document.getElementById('itemSubSelect').innerHTML = "";
            jQuery.ajax({
                type: "POST",
                url: "WarehouseP.aspx/getSubItem",
                data: "{'id':'" + e.options[e.selectedIndex].value + "','uname':'" + loggedinUsername + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        for (var i = 0; i < data.d.length; i++) {
                            var select = document.getElementById("itemSubSelect");
                            var res = data.d[i].split('_');
                            var opt = document.createElement('option');
                            opt.value = res[0];
                            opt.innerHTML = res[0];
                            select.appendChild(opt);
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        catch (ex) {
            alert(ex);
        }
        }
                    function newspareassetCategorySelectChange(e) {

        try {

            document.getElementById('newsparecategorySubAssetSelect').innerHTML = "";
            jQuery.ajax({
                type: "POST",
                url: "WarehouseP.aspx/getSubCategories",
                data: "{'id':'" + e.options[e.selectedIndex].value + "','uname':'" + loggedinUsername + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        for (var i = 0; i < data.d.length; i++) {
                            var select = document.getElementById("newsparecategorySubAssetSelect");
                            var res = data.d[i].split('_');
                            var opt = document.createElement('option');
                            opt.value = res[1];
                            opt.innerHTML = res[0];
                            select.appendChild(opt);
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        catch (ex) {
            alert(ex);
        }
    }
            function spareassetCategorySelectChange(e) {

        try {

            document.getElementById('sparecategorySubAssetSelect').innerHTML = "";
            jQuery.ajax({
                type: "POST",
                url: "WarehouseP.aspx/getSubCategories",
                data: "{'id':'" + e.options[e.selectedIndex].value + "','uname':'" + loggedinUsername + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        for (var i = 0; i < data.d.length; i++) {
                            var select = document.getElementById("sparecategorySubAssetSelect");
                            var res = data.d[i].split('_');
                            var opt = document.createElement('option');
                            opt.value = res[1];
                            opt.innerHTML = res[0];
                            select.appendChild(opt);
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        catch (ex) {
            alert(ex);
        }
    }
    function assetCategorySelectChange(e) {

        try {

            document.getElementById('categorySubAssetSelect').innerHTML = "";
            jQuery.ajax({
                type: "POST",
                url: "WarehouseP.aspx/getSubCategories",
                data: "{'id':'" + e.options[e.selectedIndex].value + "','uname':'" + loggedinUsername + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        for (var i = 0; i < data.d.length; i++) {
                            var select = document.getElementById("categorySubAssetSelect");
                            var res = data.d[i].split('_');
                            var opt = document.createElement('option');
                            opt.value = res[1];
                            opt.innerHTML = res[0];
                            select.appendChild(opt);
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        catch (ex) {
            alert(ex);
        }
    }

    function assetitemLocationSelectChange(e) {
        try {

            document.getElementById('categorySubMainLocation').innerHTML = "";
            jQuery.ajax({
                type: "POST",
                url: "WarehouseP.aspx/getSubLocations",
                data: "{'id':'" + e.options[e.selectedIndex].value + "','uname':'" + loggedinUsername + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        for (var i = 0; i < data.d.length; i++) {
                            var select = document.getElementById("categorySubMainLocation");
                            var opt = document.createElement('option');
                            opt.value = data.d[i];
                            opt.innerHTML = data.d[i];
                            select.appendChild(opt);
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        catch (ex) {
            alert(ex);
        }
    }
    function searchLocationStoreSelectChange(e) {
        try {

            document.getElementById('searchSubStoreSelect').innerHTML = "";
            var select = document.getElementById("searchSubStoreSelect");
            var optt = document.createElement('option');
            optt.value = 'Please Select';
            optt.innerHTML = 'Please Select';
            select.appendChild(optt);
            jQuery.ajax({
                type: "POST",
                url: "WarehouseP.aspx/getSubLocations",
                data: "{'id':'" + e.options[e.selectedIndex].value + "','uname':'" + loggedinUsername + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        for (var i = 0; i < data.d.length; i++) {
                            var opt = document.createElement('option');
                            opt.value = data.d[i];
                            opt.innerHTML = data.d[i];
                            select.appendChild(opt);
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        catch (ex) {
            alert(ex);
        }
    }
    function itemLocationSelectChange(e) {
        try {

            document.getElementById('itemStorageSelect').innerHTML = "";
            jQuery.ajax({
                type: "POST",
                url: "WarehouseP.aspx/getSubLocations",
                data: "{'id':'" + e.options[e.selectedIndex].value + "','uname':'" + loggedinUsername + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        for (var i = 0; i < data.d.length; i++) {
                            var select = document.getElementById("itemStorageSelect");
                            var opt = document.createElement('option');
                            opt.value = data.d[i];
                            opt.innerHTML = data.d[i];
                            select.appendChild(opt);
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        catch (ex) {
            alert(ex);
        }
    }
    function disposeToSelectChange(e) {
        if (e.options[e.selectedIndex].value == "Destroyed") {
            document.getElementById("destroySOPDiv").style.display = "block";
        }
        else {
            document.getElementById("destroySOPDiv").style.display = "none";
        }
    }
    function clearBarcode() {
        //document.getElementById('tbItemReference').disabled = false;
        document.getElementById('tbItemReference').value = '';
        document.getElementById('barcode').src = '';
    }
    function addrowtoTable5() {
        jQuery("#vehiclecolorTable tbody").empty();
        jQuery.ajax({
            type: "POST",
            url: "WarehouseP.aspx/getVehicleColorData",
            data: "{'id':'0','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    for (var i = 0; i < data.d.length; i++) {
                        jQuery("#vehiclecolorTable tbody").append(data.d[i]);
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function addrowtoTable6() {
        jQuery("#finderTable tbody").empty();
        jQuery.ajax({
            type: "POST",
            url: "WarehouseP.aspx/getFinderDepData",
            data: "{'id':'0','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    for (var i = 0; i < data.d.length; i++) {
                        jQuery("#finderTable tbody").append(data.d[i]);
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
        }
        function deleteAp() {
            jQuery.ajax({
                type: "POST",
                url: "WarehouseP.aspx/DeleteAssetPlanById",
                data: "{'id':'" + document.getElementById('AssetPlanID').value + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "SUCCESS") {
                        document.getElementById('successincidentScenario').innerHTML = "Plan successfully deleted!";
                        jQuery('#successfulDispatch').modal('show');
                    }
                    else if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                    else {
                        showError(data.d);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            }); 
        }
    function deleteItemLost() {
        jQuery.ajax({
            type: "POST",
            url: "WarehouseP.aspx/deleteItemLost",
            data: "{'id':'" + document.getElementById('rowidChoice').value + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d == "SUCCESS") {
                    document.getElementById('successincidentScenario').innerHTML = "Item successfully deleted!";
                    jQuery('#successfulDispatch').modal('show');
                }
                else if (data.d == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
                else {
                    showError(data.d);
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }

    function deleteItemFound() {

        jQuery.ajax({
            type: "POST",
            url: "WarehouseP.aspx/deleteItemFound",
            data: "{'id':'" + document.getElementById('rowidChoice').value + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d == "SUCCESS") {
                    document.getElementById('successincidentScenario').innerHTML = "Lost and Found successfully deleted!";
                    jQuery('#successfulDispatch').modal('show');
                }
                else if (data.d == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                    else {
                        showError(data.d);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function deleteItemEnquiry() {

            jQuery.ajax({
                type: "POST",
                url: "WarehouseP.aspx/deleteItemEnquiry",
                data: "{'id':'" + document.getElementById('rowidChoiceENQ').value + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "SUCCESS") {
                        document.getElementById('successincidentScenario').innerHTML = "Enquiry successfully deleted!";
                        jQuery('#successfulDispatch').modal('show');
                    }
                    else if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                    else {
                        showError(data.d);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function delVehColorSave() {
            jQuery.ajax({
                type: "POST",
                url: "WarehouseP.aspx/delVehColor",
                data: "{'id':'" + document.getElementById('tbVehicleColorChoice').value + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "SUCCESS") {
                        document.getElementById('successincidentScenario').innerHTML = "Color successfully deleted!";
                        jQuery('#successfulDispatch').modal('show');
                    }
                    else if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                    else {
                        showError(data.d);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function delFinderDep() {
            jQuery.ajax({
                type: "POST",
                url: "WarehouseP.aspx/delFinderDep",
                data: "{'id':'" + document.getElementById('tbFinderDepChoice').value + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "SUCCESS") {
                        document.getElementById('successincidentScenario').innerHTML = "Finder department successfully deleted!";
                        jQuery('#successfulDispatch').modal('show');
                    }
                    else if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                    else {
                        showError(data.d);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function editVehColorSave() {
            if (!isEmptyOrSpaces(document.getElementById('tbEditVehicleColorName').value)) {
                if (!isSpecialChar(document.getElementById('tbEditVehicleColorName').value)) {
                    jQuery.ajax({
                        type: "POST",
                        url: "WarehouseP.aspx/editVehColor",
                        data: "{'id':'" + document.getElementById('tbVehicleColorChoice').value + "','name':'" + document.getElementById('tbEditVehicleColorName').value + "','nameAR':'" + document.getElementById('tbEditArVehicleColorName').value + "','uname':'" + loggedinUsername + "'}",
                        async: false,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            if (data.d == "SUCCESS") {
                                jQuery('#editVehicleColorModal').modal('hide');
                                document.getElementById('successincidentScenario').innerHTML = "Color successfully edited!";
                                jQuery('#successfulDispatch').modal('show');
                            }
                            else if (data.d == "LOGOUT") {
                                showError("Session has expired. Kindly login again.");
                                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                            }
                            else {
                                showError(data.d);
                            }
                        },
                        error: function () {
                            showError("Session timeout. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                    });
                }
            }
            else {
                showAlert("Kindly provide color name");
            }
        }
        function editFinderDepartmentSave() {
            if (!isEmptyOrSpaces(document.getElementById('tbEditFinderDepartmentName').value)) {
                if (!isSpecialChar(document.getElementById('tbEditFinderDepartmentName').value)) {
                    jQuery.ajax({
                        type: "POST",
                        url: "WarehouseP.aspx/editFinderDepModal",
                        data: "{'id':'" + document.getElementById('tbFinderDepChoice').value + "','name':'" + document.getElementById('tbEditFinderDepartmentName').value + "','uname':'" + loggedinUsername + "'}",
                        async: false,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            if (data.d == "SUCCESS") {
                                jQuery('#editFinderDepModal').modal('hide');
                                document.getElementById('successincidentScenario').innerHTML = "Finder Department successfully edited!";
                                jQuery('#successfulDispatch').modal('show');
                            }
                            else if (data.d == "LOGOUT") {
                                showError("Session has expired. Kindly login again.");
                                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                            }
                            else {
                                showError(data.d);
                            }
                        },
                        error: function () {
                            showError("Session timeout. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                    });
                }
            }
            else {
                showAlert("Kindly provide finder department name");
            }
        }



        function checkoutItem() {
            document.getElementById('divAttachmentHolder').style.display = "none";
            document.getElementById('divOwnerHolder').style.display = "block";
            document.getElementById('checkoutDivPro').style.display = "block";

            document.getElementById('checkoutLiPro').style.display = "none";
            document.getElementById('checkinLiPro').style.display = "none";

            document.getElementById('checkoutDivIni').style.display = "none";
        }
        function cancelCheckOut() {
            document.getElementById('divAttachmentHolder').style.display = "block"; 
        }
        function checkinItem() {
            document.getElementById('divAttachmentHolder').style.display = "none";
            document.getElementById('divOwnerHolder').style.display = "block";
            document.getElementById('checkoutDivPro').style.display = "block";

            document.getElementById('checkoutLiPro').style.display = "none";
            document.getElementById('checkinLiPro').style.display = "none";

            document.getElementById('checkoutDivIni').style.display = "none";
        }

        function processItem(e) {

            var name = document.getElementById("tbCheckOutName").value;
            if (!isEmptyOrSpaces(name)) {
                if (!isSpecialChar(name)) {
                    jQuery.ajax({
                        type: "POST",
                        url: "WarehouseP.aspx/processAssetItem",
                        data: "{'id':'" + document.getElementById('rowidChoice').value + "','process':'" + e + "','chkname':'" + name + "','uname':'" + loggedinUsername + "'}",
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            if (data.d == "SUCCESS") {
                                jQuery('#ticketingViewCard').modal('hide');
                                document.getElementById('successincidentScenario').innerHTML = "Item successfully processed!";
                                jQuery('#successfulDispatch').modal('show');
                            }
                            else if (data.d == "LOGOUT") {
                                showError("Session has expired. Kindly login again.");
                                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                            }
                            else {
                                showError(data.d);
                            }
                        },
                        error: function () {
                            showError("Session timeout. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                    });
                }
            }
            else {
                showAlert("Kindly provide name");
            }
        }
        function clearPWBox() {
            document.getElementById("confirmPwInput").value = "";
            document.getElementById("newPwInput").value = "";
        }
            function customersOnChange(e) {
 
        if (e.options[e.selectedIndex].value > 0) { 
            if (document.getElementById('projectlst').value > 0) {

            }
            else {
                $('#projectlst option').remove();
                var projectoption = $("#projectlst");
                projectoption.append($('<option></option>').val(0).html('Select Project'));
                $.ajax({
                    type: "POST",
                    url: "WarehouseP.aspx/getProjectListByCustomerId",
                    data: "{'id':'" + e.options[e.selectedIndex].value + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var options = $("#projectlst");
                        for (var i = 0; i < data.d.length; i++) {
                            options.append(
                                 $('<option></option>').val(data.d[i].Id).html(data.d[i].Name)
                             );
                        }
                    }
                });
            }

        }
        else {
             
            $('#customerslst option').remove();
            $.ajax({
                type: "POST",
                url: "WarehouseP.aspx/getCustomerByProjectId",
                data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var options = $("#customerslst");
                    for (var i = 0; i < data.d.length; i++) {
                        cid = data.d[i].Id;
                        options.append(
                             $('<option></option>').val(data.d[i].Id).html(data.d[i].ClientName)
                         );
                    }
                }
            });

            $('#projectlst option').remove();
            var projectoption = $("#projectlst");
            projectoption.append($('<option></option>').val(0).html('Select Project'));
            $.ajax({
                type: "POST",
                url: "WarehouseP.aspx/getProjectListByCustomerId",
                data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var options = $("#projectlst");
                    for (var i = 0; i < data.d.length; i++) {
                        options.append(
                             $('<option></option>').val(data.d[i].Id).html(data.d[i].Name)
                         );
                    }
                }
            });
        }

    }


    function projectlstOnChange(e) {
        if (document.getElementById('customerslst').value > 0) {

        }
        else {
            $('#customerslst option').remove();
            if (e.options[e.selectedIndex].value == 0) { 
                $('#projectlst option').remove();
 
                var projectoption = $("#projectlst");
                projectoption.append($('<option></option>').val(0).html('Select Project'));

                $.ajax({
                    type: "POST",
                    url: "WarehouseP.aspx/getProjectListByCustomerId",
                    data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var options = $("#projectlst");
                        for (var i = 0; i < data.d.length; i++) {
                            options.append(
                                 $('<option></option>').val(data.d[i].Id).html(data.d[i].Name)
                             );
                        }
                    }
                });
            }
            $.ajax({
                type: "POST",
                url: "WarehouseP.aspx/getCustomerByProjectId",
                data: "{'id':'" + e.options[e.selectedIndex].value + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var options = $("#customerslst");
                    for (var i = 0; i < data.d.length; i++) {
                        options.append(
                             $('<option></option>').val(data.d[i].Id).html(data.d[i].ClientName)
                         );
                    }
                }
            });
        }
        }
                var endDSC = false;
    function dateDutyEndChange(e) {
        if (!endDSC)
            endDSC = true;
        else if (endDSC) {
            var isErr = false;
                            var startDate = new Date(document.getElementById('dateDutyStart').value);
                var enddate = new Date(e.value); 
            var dddd = dates.compare(startDate, enddate);
            if (dddd == 1) {
                isErr = true;
                showAlert("End Date should be set to date greater than Start Date.")
            }
            if (!isErr) {
                var split = e.value.split('/');
                var m1 = split[0];
                var d1 = split[1];
                var split2 = document.getElementById('dateDutyStart').value.split('/');
                var m2 = split2[0];
                var d2 = split2[1];
                try {
                    document.getElementById((parseInt(m1)) + '_' + (parseInt(d1))).style.backgroundColor = "lightgray";
                }
                catch (erx) { }
                //document.getElementById((parseInt(m2)) + '_' + (parseInt(d2))).style.backgroundColor = "lightgray";

                var date1 = new Date(document.getElementById('dateDutyStart').value);
                var date2 = new Date(e.value);
                var gd = getDates(date1, date2)

                try {

                    for (var i = 0; i < datedutylisttems.length; i++) {
                        if (typeof datedutylisttems[i] === 'undefined') {
                            // your code here.
                        }
                        else {
                            try {
                                var splitz = datedutylisttems[i].split('/');
                                var m1z = splitz[0];
                                var d1z = splitz[1];
                                if (document.getElementById((parseInt(m1z)) + '_' + (parseInt(d1z))))
                                    document.getElementById((parseInt(m1z)) + '_' + (parseInt((parseInt(d1z))))).style.backgroundColor = "white";
                            }
                            catch (errx) {
                                //alert(err);
                            }
                        }
                    }
                    datedutylisttems = [];

                    for (var i = 0; i < gd.length; i++) {
                        var dd = gd[i].getDate();
                        var mm = gd[i].getMonth() + 1;
                        var y = gd[i].getFullYear();

                        var someFormattedDate = mm + '/' + dd + '/' + y;
                        datedutylisttems.push(someFormattedDate);
                        try {
                            if (document.getElementById((parseInt(mm)) + '_' + (parseInt(dd))))
                                document.getElementById((parseInt(mm)) + '_' + dd).style.backgroundColor = "lightgray";

                        }
                        catch (erx) { }
                    }
                }
                catch (err) {
                    //alert(err);
                }
            }
        }
    }
    function dateback2() {
            var id = document.getElementById('currentdate').text;
            document.getElementById('dutycalendarRow1').innerHTML = "";
            document.getElementById('dutycalendarRow2').innerHTML = "";
            document.getElementById('dutycalendarRow3').innerHTML = "";
            document.getElementById('dutycalendarRow4').innerHTML = "";
            document.getElementById('dutycalendarRow5').innerHTML = "";

            jQuery.ajax({
                type: "POST",
                url: "WarehouseP.aspx/getCalendarDaysDuty",
                data: "{'id':'" + (parseInt(id) - 1) + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        if (data.d.length > 0) {
                            document.getElementById('dutycalendarRow1').innerHTML = data.d[0];
                            document.getElementById('dutycalendarRow2').innerHTML = data.d[1];
                            document.getElementById('dutycalendarRow3').innerHTML = data.d[2];
                            document.getElementById('dutycalendarRow4').innerHTML = data.d[3];
                            document.getElementById('dutycalendarRow5').innerHTML = data.d[4];
                            document.getElementById('dutydateHeader').innerHTML = data.d[5];
                            document.getElementById('currentdate').text = data.d[6];

                            for (var i = 0; i < datedutylisttems.length; i++) {
                                if (typeof datedutylisttems[i] === 'undefined') {
                                    // your code here.
                                }
                                else {
                                    try {
                                        var splitz = datedutylisttems[i].split('/');
                                        var m1z = splitz[0];
                                        var d1z = splitz[1];
                                        if (document.getElementById((parseInt(m1z)) + '_' + (parseInt((parseInt(d1z))))))
                                            document.getElementById((parseInt(m1z)) + '_' + (parseInt((parseInt(d1z))))).style.backgroundColor = "lightgray";
                                    }
                                    catch (errx) {
                                        //alert(err);
                                    }
                                }
                            }
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }

        function dateforward2() {
        var id = document.getElementById('currentdate').text;
        document.getElementById('dutycalendarRow1').innerHTML = "";
        document.getElementById('dutycalendarRow2').innerHTML = "";
        document.getElementById('dutycalendarRow3').innerHTML = "";
        document.getElementById('dutycalendarRow4').innerHTML = "";
        document.getElementById('dutycalendarRow5').innerHTML = "";

        jQuery.ajax({
            type: "POST",
            url: "WarehouseP.aspx/getCalendarDaysDuty",
            data: "{'id':'" + (parseInt(id) + 1) + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    if (data.d.length > 0) {
                        document.getElementById('dutycalendarRow1').innerHTML = data.d[0];
                        document.getElementById('dutycalendarRow2').innerHTML = data.d[1];
                        document.getElementById('dutycalendarRow3').innerHTML = data.d[2];
                        document.getElementById('dutycalendarRow4').innerHTML = data.d[3];
                        document.getElementById('dutycalendarRow5').innerHTML = data.d[4];
                        document.getElementById('dutydateHeader').innerHTML = data.d[5];
                        document.getElementById('currentdate').text = data.d[6];

                        for (var i = 0; i < datedutylisttems.length; i++) {
                            if (typeof datedutylisttems[i] === 'undefined') {
                                // your code here.
                            }
                            else {
                                try {
                                    var splitz = datedutylisttems[i].split('/');
                                    var m1z = splitz[0];
                                    var d1z = splitz[1];
                                    if (document.getElementById((parseInt(m1z)) + '_' + (parseInt((parseInt(d1z))))))
                                        document.getElementById((parseInt(m1z)) + '_' + (parseInt((parseInt(d1z))))).style.backgroundColor = "lightgray";
                                }

                                catch (errx) {
                                    //alert(err);
                                }
                            }
                    }
                }
            }
        },
        error: function () {
            showError("Session timeout. Kindly login again.");
            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
        }
    });
}

        var startDSC = false;
        function dateDutyStartChange(e) {
            if (!startDSC)
                startDSC = true;
            else if (startDSC) {
                var isErr = false;
                var enddate = new Date(document.getElementById('dateDutyEnd').value);
                var startDate = new Date(e.value);
                var dddd = dates.compare(startDate, enddate);
                if (dddd == 1) {
                    isErr = true;
                    showAlert("End Date should be set to date greater than Start Date.")
                }
                var today = new Date();

                var ddz = today.getDate();
                var mmz = today.getMonth() + 1;
                var yz = today.getFullYear();
                var someFormattedDatez = mmz + '/' + ddz + '/' + yz;
                today = new Date(someFormattedDatez);
                var ddd = dates.compare(today, startDate);
                if (ddd == 1) {
                    isErr = true;
                    showAlert("Start Date should be set to today or future date.")
                }
                if (!isErr) {
                    var split = e.value.split('/');
                    var id = split[0];
                    document.getElementById('dutycalendarRow1').innerHTML = "";
                    document.getElementById('dutycalendarRow2').innerHTML = "";
                    document.getElementById('dutycalendarRow3').innerHTML = "";
                    document.getElementById('dutycalendarRow4').innerHTML = "";
                    document.getElementById('dutycalendarRow5').innerHTML = "";

                    jQuery.ajax({
                        type: "POST",
                        url: "WarehouseP.aspx/getCalendarDaysDuty",
                        data: "{'id':'" + (parseInt(id)) + "','uname':'" + loggedinUsername + "'}",
                        async: false,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            if (data.d[0] == "LOGOUT") {
                                showError("Session has expired. Kindly login again.");
                                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                            } else {
                                if (data.d.length > 0) {
                                    document.getElementById('dutycalendarRow1').innerHTML = data.d[0];
                                    document.getElementById('dutycalendarRow2').innerHTML = data.d[1];
                                    document.getElementById('dutycalendarRow3').innerHTML = data.d[2];
                                    document.getElementById('dutycalendarRow4').innerHTML = data.d[3];
                                    document.getElementById('dutycalendarRow5').innerHTML = data.d[4];
                                    document.getElementById('dutydateHeader').innerHTML = data.d[5];
                                    document.getElementById('currentdate').text = data.d[6];

                                    var m1 = split[0];
                                    var d1 = split[1];
                                    var split2 = document.getElementById('dateDutyEnd').value.split('/');
                                    var m2 = split2[0];
                                    var d2 = split2[1];
                                    try {
                                        if (document.getElementById((parseInt(m1)) + '_' + (parseInt((parseInt(d1))))))
                                            document.getElementById((parseInt(m1)) + '_' + (parseInt(d1))).style.backgroundColor = "lightgray";
                                    }
                                    catch (erx) { }
                                    //document.getElementById((parseInt(m2)) + '_' + (parseInt(d2))).style.backgroundColor = "lightgray";

                                    var date1 = new Date(e.value);
                                    var date2 = new Date(document.getElementById('dateDutyEnd').value);
                                    var gd = getDates(date1, date2)

                                    try {
                                        for (var i = 0; i < datedutylisttems.length; i++) {
                                            if (typeof datedutylisttems[i] === 'undefined') {
                                                // your code here.
                                            }
                                            else {
                                                try {
                                                    var splitz = datedutylisttems[i].split('/');
                                                    var m1z = splitz[0];
                                                    var d1z = splitz[1];
                                                    if (document.getElementById((parseInt(m1z)) + '_' + (parseInt((parseInt(d1z))))))
                                                        document.getElementById((parseInt(m1z)) + '_' + (parseInt((parseInt(d1z))))).style.backgroundColor = "white";
                                                }
                                                catch (errx) {
                                                    //alert(err);
                                                }
                                            }
                                        }
                                        datedutylisttems = [];

                                        for (var i = 0; i < gd.length; i++) {
                                            var dd = gd[i].getDate();
                                            var mm = gd[i].getMonth() + 1;
                                            var y = gd[i].getFullYear();

                                            var someFormattedDate = mm + '/' + dd + '/' + y;
                                            datedutylisttems.push(someFormattedDate);
                                            try {
                                                if (document.getElementById((parseInt(mm)) + '_' + (parseInt(dd))))
                                                    document.getElementById((parseInt(mm)) + '_' + dd).style.backgroundColor = "lightgray";
                                            }
                                            catch (erx) { }
                                        }
                                    }
                                    catch (err) {
                                        //alert(err);
                                    }
                                }
                            }
                        },
                        error: function () {
                            showError("Session timeout. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                    });
                }
            }
        }

        var isMyself = false;
        function assignMySelf() {
            if (!isMyself) {
                document.getElementById("assigneeDIV").style.display = "none";
                isMyself = true;
            }
            else {
                document.getElementById("assigneeDIV").style.display = "block";
                isMyself = false;
            }
        }

        function getDates(startDate, stopDate) {
            var dateArray = new Array();
            var currentDate = startDate;
            while (currentDate <= stopDate) {
                dateArray.push(new Date(currentDate));
                var newdate = new Date(currentDate);
                newdate.setDate(newdate.getDate() + 1);

                var dd = newdate.getDate();
                var mm = newdate.getMonth() + 1;
                var y = newdate.getFullYear();

                var someFormattedDate = mm + '/' + dd + '/' + y;
                currentDate = new Date(someFormattedDate);
            }
            return dateArray;
        }

        function getDutyTableData(dateduty, item) {
            //alert(dateduty)
            var newdate = new Date(dateduty);
            var dd = newdate.getDate();
            var mm = newdate.getMonth() + 1;
            var y = newdate.getFullYear();
            var today = new Date();

            var ddz = today.getDate();
            var mmz = today.getMonth() + 1;
            var yz = today.getFullYear();
            var someFormattedDatez = mmz + '/' + ddz + '/' + yz;
            today = new Date(someFormattedDatez);
            var ddd = dates.compare(today, newdate);
            if (ddd == -1 || ddd == 0) {
                var someFormattedDate = mm + '/' + dd + '/' + y;
                if (item.style.backgroundColor == "lightgray") {
                    item.style.backgroundColor = "white";
                    var newdatedutylisttems = [];
                    for (var i = 0; i < datedutylisttems.length; i++) {
                        if (typeof datedutylisttems[i] === 'undefined') {
                            // your code here.
                        }
                        else {
                            if (datedutylisttems[i] == someFormattedDate) {

                            }
                            else { newdatedutylisttems.push(datedutylisttems[i]); }
                        }
                    }
                    datedutylisttems = [];
                    datedutylisttems = newdatedutylisttems;
                }
                else {
                    item.style.backgroundColor = "lightgray";
                    datedutylisttems.push(someFormattedDate);
                }
            }
            else {
                showAlert("Kindly select future date");
            }
        }  
                function pickdateassetplanTask2() {
            jQuery("#assetplanTaskTable2 tbody").empty();
            jQuery("#assetplanTaskTable2").dataTable().fnClearTable();
            jQuery("#assetplanTaskTable2").dataTable().fnDraw();
            jQuery("#assetplanTaskTable2").dataTable().fnDestroy();
            jQuery.ajax({
                type: "POST",
                url: "WarehouseP.aspx/plantaskViewTableByDate2",
                data: "{'id':'" + document.getElementById('AssetPlanID').text + "','date':'" + document.getElementById('assetplantaskDatepicker2').value + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d.length > 0) {
                        for (var i = 0; i < data.d.length; i++) {
                            jQuery("#assetplanTaskTable2 tbody").append(data.d[i]);
                        }
                        jQuery("#assetplanTaskTable2").DataTable({
                            "dom": '<"top"f>rt<"bottom" <"datatable-pagination-info"p> <"pull-right pagination-info"i>><"clearfx">',
                            'iDisplayLength': 3,
                            "order": [[0, "asc"]]
                        });
                    }
                    else {
                        jQuery("#assetplanTaskTable2").DataTable({
                            "dom": '<"top"f>rt<"bottom" <"datatable-pagination-info"p> <"pull-right pagination-info"i>><"clearfx">',
                            'iDisplayLength': 3,
                            "order": [[0, "asc"]]
                        });
                    }
                }
            });
        }
        function pickdateassetplanTask() {
            jQuery("#assetplanTaskTable tbody").empty();
            jQuery("#assetplanTaskTable").dataTable().fnClearTable();
            jQuery("#assetplanTaskTable").dataTable().fnDraw();
            jQuery("#assetplanTaskTable").dataTable().fnDestroy();
            jQuery.ajax({
                type: "POST",
                url: "WarehouseP.aspx/plantaskViewTableByDate",
                data: "{'id':'" + document.getElementById('AssetPlanID').text + "','date':'" + document.getElementById('assetplantaskDatepicker').value + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d.length > 0) {
                        for (var i = 0; i < data.d.length; i++) {
                            jQuery("#assetplanTaskTable tbody").append(data.d[i]);
                        }
                        jQuery("#assetplanTaskTable").DataTable({
                            "dom": '<"top"f>rt<"bottom" <"datatable-pagination-info"p> <"pull-right pagination-info"i>><"clearfx">',
                            'iDisplayLength': 3,
                            "order": [[0, "asc"]]
                        });
                    }
                    else {
                        jQuery("#assetplanTaskTable").DataTable({
                            "dom": '<"top"f>rt<"bottom" <"datatable-pagination-info"p> <"pull-right pagination-info"i>><"clearfx">',
                            'iDisplayLength': 3,
                            "order": [[0, "asc"]]
                        });
                    }
                }
            });
        }
                function planViewTables2(id) {
            jQuery("#assetplanTaskTable2 tbody").empty();
            jQuery("#assetplanTaskTable2").dataTable().fnClearTable();
            jQuery("#assetplanTaskTable2").dataTable().fnDraw();
            jQuery("#assetplanTaskTable2").dataTable().fnDestroy();
            jQuery.ajax({
                type: "POST",
                url: "WarehouseP.aspx/plantaskViewTable2",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d.length > 0) {
                        for (var i = 0; i < data.d.length; i++) {
                            jQuery("#assetplanTaskTable2 tbody").append(data.d[i]);
                        }
                        jQuery("#assetplanTaskTable2").DataTable({
                            "dom": '<"top"f>rt<"bottom" <"datatable-pagination-info"p> <"pull-right pagination-info"i>><"clearfx">',
                            'iDisplayLength': 3,
                            "order": [[0, "asc"]]
                        });
                    }
                    else {
                        jQuery("#assetplanTaskTable2").DataTable({
                            "dom": '<"top"f>rt<"bottom" <"datatable-pagination-info"p> <"pull-right pagination-info"i>><"clearfx">',
                            'iDisplayLength': 3,
                            "order": [[0, "asc"]]
                        });
                    }
                }
            });
        }
        function planViewTables(id) {
            jQuery("#assetplanTaskTable tbody").empty();
            jQuery("#assetplanTaskTable").dataTable().fnClearTable();
            jQuery("#assetplanTaskTable").dataTable().fnDraw();
            jQuery("#assetplanTaskTable").dataTable().fnDestroy();
            jQuery.ajax({
                type: "POST",
                url: "WarehouseP.aspx/plantaskViewTable",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d.length > 0) {
                        for (var i = 0; i < data.d.length; i++) {
                            jQuery("#assetplanTaskTable tbody").append(data.d[i]);
                        }
                        jQuery("#assetplanTaskTable").DataTable({
                            "dom": '<"top"f>rt<"bottom" <"datatable-pagination-info"p> <"pull-right pagination-info"i>><"clearfx">',
                            'iDisplayLength': 3,
                            "order": [[0, "asc"]]
                        });
                    }
                    else {
                        jQuery("#assetplanTaskTable").DataTable({
                            "dom": '<"top"f>rt<"bottom" <"datatable-pagination-info"p> <"pull-right pagination-info"i>><"clearfx">',
                            'iDisplayLength': 3,
                            "order": [[0, "asc"]]
                        });
                    }
                }
            });
        }
        function tracebackOn(type) {
    var id = 0;
    if (type == "task") {
        id = document.getElementById('rowChoiceTasks').value;
    }
    else
        id = document.getElementById('rowidChoiceInci').text;

    dduration = "0";

    jQuery.ajax({
        type: "POST",
        url: "WarehouseP.aspx/getTracebackLocationData",
        data: "{'id':'" + id + "','ttype':'" + type + "','uname':'" + loggedinUsername + "'}",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if (data.d == "LOGOUT") {
                showError("Session has expired. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            } else {
                var obj = jQuery.parseJSON(data.d)

                if (obj.length > 1) {
                    if (type == "task") {
                        updateTaskMarker(obj);
                        document.getElementById("rowtasktracebackUser").style.display = "block";
                    }
                    else {
                        document.getElementById("rowtracebackUser").style.display = "block";
                        updateIncidentMarker(obj);

                        getDispatchUserList(id, type);
                    }
                }
            }
        },
        error: function () {
            showError("Session timeout. Kindly login again.");
            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
        }
    });
        }
            function tasknextImg() {
        var found = false;
        for (var i = 0; i < divArray.length; i++) {
            var el = document.getElementById(divArray[i]);
            if (el.classList.contains("active")) {
                el.className = 'tab-pane fade ';
                if (i + 1 < imgcount) {
                    var el3 = document.getElementById(divArray[i + 1]);
                    if (el3) {
                        el3.className = 'tab-pane fade active in';
                    }
                }
                else {
                    var el3 = document.getElementById("tasklocation-tab");
                    if (el3) {
                        el3.className = 'tab-pane fade active in';
                    }
                }
                found = true;
                break;
            }
        }
        if (!found) {
            if (divArray.length > 0) {
                var ell = document.getElementById(divArray[0]);
                if (ell) {
                    ell.className = 'tab-pane fade active in';
                }
                var ell1 = document.getElementById("tasklocation-tab");
                if (ell1) {
                    ell1.className = 'tab-pane fade';
                }
            }
        }
    }
    function taskbackImg() {
        var found = false;
        for (var i = 0; i < divArray.length; i++) {
            var el = document.getElementById(divArray[i]);
            if (el.classList.contains("active")) {
                el.className = 'tab-pane fade ';
                if (i == 0) {
                    var elz = document.getElementById("tasklocation-tab");
                    if (elz) {
                        elz.className = 'tab-pane fade active in';
                    }
                }
                else if (i - imgcount < imgcount) {
                    var el3 = document.getElementById(divArray[i - 1]);
                    if (el3) {
                        el3.className = 'tab-pane fade active in';
                    }
                }
                else {
                    var el3 = document.getElementById("tasklocation-tab");
                    if (el3) {
                        el3.className = 'tab-pane fade active in';
                    }
                }
                found = true;
                break;
            }
        }
        if (!found) {
            if (divArray.length > 0) {
                var ell = document.getElementById(divArray[imgcount - 1]);
                if (ell) {
                    ell.className = 'tab-pane fade active in';
                }
                var ell1 = document.getElementById("tasklocation-tab");
                if (ell1) {
                    ell1.className = 'tab-pane fade';
                }
            }
        }
    }
        var datedutylisttems = [];
        var lengthGood = false;
        var letterGood = false;
        var capitalGood = false;
        var numGood = false;
        var firstpresstask = false;
           var myMarkersTasksLocation = new Array();
        jQuery(document).ready(function () {
            try {
                            var moment = window.moment,
            currentMonth = moment().format('YYYY-MM'),
            nextMonth = moment().add('month', 1).format('YYYY-MM'),
            myEvents = [{ date: currentMonth + '-' + '10', title: 'Persian Kitten Auction', location: 'Center for Beautiful Cats' },
            { date: currentMonth + '-' + '19', title: 'Cat Frisbee', location: 'Jefferson Park' },
            { date: currentMonth + '-' + '23', title: 'Kitten Demonstration', location: 'Center for Beautiful Cats' },
            { date: nextMonth + '-' + '07', title: 'Small Cat Photo Session', location: 'Center for Cat Photography' }];

                jQuery('#dutycalendar3').clndr({
                    template: $('#dutyfull-clndr-template').html(),
                    daysOfTheWeek: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
                    events: myEvents,
                    targets: {
                        nextButton: 'clndr-next-btn',
                        previousButton: 'clndr-prev-btn',
                        nextYearButton: 'clndr-next-year-btn',
                        previousYearButton: 'clndr-prev-year-btn',
                        todayButton: 'clndr-today-btn',
                        day: 'day',
                        empty: 'empty'
                    },
                });

            jQuery.ajax({
                type: "POST",
                url: "WarehouseP.aspx/getCalendarDaysDuty",
                data: "{'id':'99','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        if (data.d.length > 0) {
                            document.getElementById('dutycalendarRow1').innerHTML = data.d[0];
                            document.getElementById('dutycalendarRow2').innerHTML = data.d[1];
                            document.getElementById('dutycalendarRow3').innerHTML = data.d[2];
                            document.getElementById('dutycalendarRow4').innerHTML = data.d[3];
                            document.getElementById('dutycalendarRow5').innerHTML = data.d[4];
                            document.getElementById('dutydateHeader').innerHTML = data.d[5];
                            document.getElementById('currentdate').text = data.d[6];
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });

                $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
                    if ($(e.target).attr('href') == "#keys-tab" || $(e.target).attr('href') == "#newkeys-tab" || $(e.target).attr('href') == "#asset-tab")
                        localStorage.setItem('activeTabWare', $(e.target).attr('href'));
                    //alert($(e.target).attr('href')+'----------NOW Selected');
                });
                var activeTab = localStorage.getItem('activeTabWare');
                if (activeTab) {
                    //alert(activeTab +'--------Selected');
                    //$('#myTab a[href="' + activeTab + '"]').tab('show'); 
                    jQuery('a[data-toggle="tab"][href="' + activeTab + '"]').tab('show');
                }
                localStorage.removeItem("activeTabDev");
                localStorage.removeItem("activeTabInci");
                localStorage.removeItem("activeTabMessage");
                localStorage.removeItem("activeTabTask");
                localStorage.removeItem("activeTabTick");
                localStorage.removeItem("activeTabUB");
                localStorage.removeItem("activeTabVer");
                localStorage.removeItem("activeTabLost");

                $('input[type=password]').keyup(function () {
                    // keyup event code here
                    var pswd = $(this).val();
                    if (pswd.length < 8) {
                        $('#length').removeClass('valid').addClass('invalid');
                        lengthGood = false;
                    } else {
                        $('#length').removeClass('invalid').addClass('valid');
                        lengthGood = true;
                    }
                    //validate letter
                    if (pswd.match(/[A-z]/)) {
                        $('#letter').removeClass('invalid').addClass('valid');
                        letterGood = true;

                    } else {
                        $('#letter').removeClass('valid').addClass('invalid');
                        letterGood = false;
                    }

                    //validate capital letter
                    if (pswd.match(/[A-Z]/)) {
                        $('#capital').removeClass('invalid').addClass('valid');
                        capitalGood = true;
                    } else {
                        $('#capital').removeClass('valid').addClass('invalid');

                        capitalGood = false;
                    }

                    //validate number
                    if (pswd.match(/\d/)) {
                        $('#number').removeClass('invalid').addClass('valid');
                        numGood = true;

                    } else {
                        $('#number').removeClass('valid').addClass('invalid');
                        numGood = false;
                    }
                });
                $('input[type=password]').focus(function () {
                    // focus code here
                    $('#pswd_info').show();
                });
                $('input[type=password]').blur(function () {
                    // blur code here
                    $('#pswd_info').hide();
                });
            } catch (err)
            { alert(err); }
            try {
                infoWindowIncidentLocation = new google.maps.InfoWindow();
            } catch (err) { alert(err) }
            jQuery('#ticketingViewCard').on('shown.bs.modal', function () {
                jQuery.ajax({
                    type: "POST",
                    url: "WarehouseP.aspx/getIncidentLocationData",
                    data: "{'id':'" + document.getElementById('rowidChoice').value + "','uname':'" + loggedinUsername + "'}",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    var obj = jQuery.parseJSON(data.d)
                    getIncidentLocationMarkers(obj);
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
            });

            jQuery('#taskDocument').on('shown.bs.modal', function () {
                if (firstpresstask == false) {
                    jQuery.ajax({
                        type: "POST",
                        url: "WarehouseP.aspx/getTaskLocationData",
                        data: "{'id':'" + document.getElementById('rowChoiceTasks').value + "','uname':'" + loggedinUsername + "'}",
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            if (data.d == "LOGOUT") {
                                showError("Session has expired. Kindly login again.");
                                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                            } else {
                                var obj = $.parseJSON(data.d)
                                getTaskLocationMarkers(obj);
                            }
                        },
                        error: function () {
                            showError("Session timeout. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                    });
                    firstpresstask = true;
                }
                else {
                    jQuery.ajax({
                        type: "POST",
                        url: "WarehouseP.aspx/getTaskLocationData",
                        data: "{'id':'" + document.getElementById('rowChoiceTasks').value + "','uname':'" + loggedinUsername + "'}",
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            if (data.d == "LOGOUT") {
                                showError("Session has expired. Kindly login again.");
                                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                            } else {
                                var obj = $.parseJSON(data.d)
                                updateTaskMarker(obj);
                            }
                        },
                        error: function () {
                            showError("Session timeout. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                    });
                }
            });

    document.getElementById('tbItemReference').disabled = true;
            addrowtoTable1();
            addrowtoPlanItem();
            addrowtoTempPlanItem();
            addrowtoTableSpare();
    addrowtoTable2();
    addrowtoTable3();
    //addrowtoFoundItem();
    //addrowtoEnquiry();
    addrowtoTable5();
    addrowtoTable6();

    getReminderSelectorDates();

    addrowtoTableKeys();

    jQuery.ajax({
        type: "POST",
        url: "WarehouseP.aspx/getSubLocations",
        data: "{'id':'" + document.getElementById("MainContent_locationStoreSelect").value + "','uname':'" + loggedinUsername + "'}",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if (data.d == "LOGOUT") {
                showError("Session has expired. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            } else {
                var select = document.getElementById('itemStorageSelect');
                for (var i = 0; i < data.d.length; i++) {
                    var opt = document.createElement('option');
                    opt.value = data.d[i];
                    opt.innerHTML = data.d[i];
                    select.appendChild(opt);
                }
            }
        },
        error: function () {
            showError("Session timeout. Kindly login again.");
            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
        }
    });


    try {
        jQuery.ajax({
            type: "POST",
            url: "WarehouseP.aspx/getSubItem",
            data: "{'id':'" + document.getElementById("MainContent_itemTypeSelect").value + "','uname':'" + loggedinUsername + "'}",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    var select = document.getElementById('itemSubSelect');
                    for (var i = 0; i < data.d.length; i++) {
                        var res = data.d[i].split('_');
                        var opt = document.createElement('option');
                        opt.value = res[0];
                        opt.innerHTML = res[0];
                        select.appendChild(opt);
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    } catch (err) {
    }
    jQuery.ajax({
        type: "POST",
        url: "WarehouseP.aspx/getSubCategories",
        data: "{'id':'" + document.getElementById("MainContent_categoryAssetSelect").value + "','uname':'" + loggedinUsername + "'}",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            var select = document.getElementById('categorySubAssetSelect');
            for (var i = 0; i < data.d.length; i++) {
                var res = data.d[i].split('_');
                var opt = document.createElement('option');
                opt.value = res[1];
                opt.innerHTML = res[0];
                select.appendChild(opt);
            }
        }
    });





    jQuery.ajax({
        type: "POST",
        url: "WarehouseP.aspx/getSubLocations",
        data: "{'id':'" + document.getElementById("MainContent_categoryAssetMainLocation").value + "','uname':'" + loggedinUsername + "'}",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if (data.d == "LOGOUT") {
                showError("Session has expired. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            } else {
                var select = document.getElementById('categorySubMainLocation');
                for (var i = 0; i < data.d.length; i++) {
                    var opt = document.createElement('option');
                    opt.value = data.d[i];
                    opt.innerHTML = data.d[i];
                    select.appendChild(opt);
                }
            }
        },
        error: function () {
            showError("Session timeout. Kindly login again.");
            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
        }
            });

            $.ajax({
                type: "POST",
                url: "WarehouseP.aspx/getProjectListByCustomerId",
                data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var options = $("#projectlst");
                    for (var i = 0; i < data.d.length; i++) {
                        options.append(
                            $('<option></option>').val(data.d[i].Id).html(data.d[i].Name)
                        );
                    }
                }
            });

            $.ajax({
                type: "POST",
                url: "WarehouseP.aspx/getCustomerByProjectId",
                data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var options = $("#customerslst");
                    for (var i = 0; i < data.d.length; i++) {
                        options.append(
                            $('<option></option>').val(data.d[i].Id).html(data.d[i].ClientName)
                        );
                    }
                }
            });

        });

                function updateTaskMarker(obj) {
            try {
                var first = true;
                var poligonCoords = [];

                for (var i = 0; i < Object.size(myTraceBackMarkers) ; i++) {
                    if (myTraceBackMarkers[i] != null) {
                        myTraceBackMarkers[i].setMap(null);
                    }
                }

                if (typeof tracebackpoligon === 'undefined') {
                    // your code here.
                }
                else {
                    tracebackpoligon.setMap(null);
                }

                if (typeof myMarkersTasksLocation["Pending"] === 'undefined') {
                    // your code here.
                }
                else {
                    myMarkersTasksLocation["Pending"].setMap(null);
                }
                if (typeof myMarkersTasksLocation["Completed"] === 'undefined') {
                    // your code here.
                }
                else {
                    myMarkersTasksLocation["Completed"].setMap(null);
                }
                if (typeof myMarkersTasksLocation["Rejected"] === 'undefined') {
                    // your code here.
                }
                else {
                    myMarkersTasksLocation["Rejected"].setMap(null);
                }
                if (typeof myMarkersTasksLocation["Accepted"] === 'undefined') {
                    // your code here.
                }
                else {
                    myMarkersTasksLocation["Accepted"].setMap(null);
                }
                if (typeof myMarkersTasksLocation["InProgress"] === 'undefined') {
                    // your code here.
                }
                else {
                    myMarkersTasksLocation["InProgress"].setMap(null);
                }
                if (typeof myMarkersTasksLocation["Cancelled"] === 'undefined') {
                    // your code here.
                }
                else {
                    myMarkersTasksLocation["Cancelled"].setMap(null);
                }
                if (typeof myMarkersTasksLocation["OnRoute"] === 'undefined') {
                    // your code here.
                }
                else {
                    myMarkersTasksLocation["OnRoute"].setMap(null);
                }
                var tbcounter = 0;
                for (var i = 0; i < obj.length; i++) {
                    var contentString = '<div id="content">' + obj[i].Username + '<br/></div>';

                    var myLatlng = new google.maps.LatLng(obj[i].Lat, obj[i].Long);

                    if (obj[i].Username == "Pending") {
                        var marker = new google.maps.Marker({ position: myLatlng, map: mapTaskLocation, title: obj[i].Username });
                        marker.setIcon('https://testportalcdn.azureedge.net/Images/marker.png');
                        myMarkersTasksLocation[obj[i].Username] = marker;
                        createInfoWindowTaskLocation(marker, contentString);
                    }
                    else if (obj[i].Username == "InProgress") {
                        var marker = new google.maps.Marker({ position: myLatlng, map: mapTaskLocation, title: obj[i].Username });
                        marker.setIcon('https://testportalcdn.azureedge.net/Images/markerIdle.png');
                        myMarkersTasksLocation[obj[i].Username] = marker;
                        createInfoWindowTaskLocation(marker, contentString);
                    }
                    else if (obj[i].Username == "OnRoute") {
                        var marker = new google.maps.Marker({ position: myLatlng, map: mapTaskLocation, title: obj[i].Username });
                        marker.setIcon('https://testportalcdn.azureedge.net/Images/start-flag.png');
                        myMarkersTasksLocation[obj[i].Username] = marker;
                        createInfoWindowTaskLocation(marker, contentString);
                    }
                    else if (obj[i].Username == "Completed") {
                        var marker = new google.maps.Marker({ position: myLatlng, map: mapTaskLocation, title: obj[i].Username });
                        marker.setIcon('https://testportalcdn.azureedge.net/Images/finish-flag.png');
                        myMarkersTasksLocation[obj[i].Username] = marker;
                        createInfoWindowTaskLocation(marker, contentString);
                    }
                    else if (obj[i].Username == "Accepted") {
                        var marker = new google.maps.Marker({ position: myLatlng, map: mapTaskLocation, title: obj[i].Username });
                        marker.setIcon('https://testportalcdn.azureedge.net/Images/finish-flag.png');
                        myMarkersTasksLocation[obj[i].Username] = marker;
                        createInfoWindowTaskLocation(marker, contentString);
                    }
                    else if (obj[i].State == "RED") {
                        if (first) {
                            first = false;
                            var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                            poligonCoords.push(point);

                        }
                        else {
                            var point = new google.maps.LatLng(obj[i].Lat, obj[i].Long);
                            poligonCoords.push(point);
                            var marker = new google.maps.Marker({ position: myLatlng, map: mapTaskLocation, title: obj[i].LastLog });
                            marker.setIcon('https://testportalcdn.azureedge.net/Images/bluesmall.png');
                            myTraceBackMarkers[tbcounter] = marker;
                            tbcounter++;
                        }
                    }
                    else {
                        var marker = new google.maps.Marker({ position: myLatlng, map: mapTaskLocation, title: obj[i].Username });
                        marker.setIcon('https://testportalcdn.azureedge.net/Images/marker.png');
                        myMarkersTasksLocation[obj[i].Username] = marker;
                        createInfoWindowTaskLocation(marker, contentString);
                    }
                }
                if (poligonCoords.length > 0) {
                    tracebackpoligon = new google.maps.Polyline({
                        path: poligonCoords,
                        geodesic: true,
                        strokeColor: '#1b93c0',
                        strokeOpacity: 1.0,
                        strokeWeight: 7,
                        icons: [{
                            icon: iconsetngs,
                            offset: '100%'
                        }]
                    });
                    tracebackpoligon.setMap(mapTaskLocation);
                    animateCircle(tracebackpoligon);
                }
            }
            catch (err) {
                showAlert(err);
            }
        }
        function getTaskLocationMarkers(obj) {
            locationAllowed = true;
            //setTimeout(function () {
            google.maps.visualRefresh = true;

            var Liverpool = new google.maps.LatLng(obj[0].Lat, obj[0].Long);

            // These are options that set initial zoom level, where the map is centered globally to start, and the type of map to show
            var mapOptions = {
                zoom: 15,
                center: Liverpool,
                mapTypeId: google.maps.MapTypeId.G_NORMAL_MAP
            };

            // This makes the div with id "map_canvas" a google map
            mapTaskLocation = new google.maps.Map(document.getElementById("taskmap_canvasIncidentLocation"), mapOptions);

            for (var i = 0; i < obj.length; i++) {

                var contentString = '<div id="content">' + obj[i].Username +
                '<br/></div>';

                var myLatlng = new google.maps.LatLng(obj[i].Lat, obj[i].Long);

                var marker = new google.maps.Marker({ position: myLatlng, map: mapTaskLocation, title: obj[i].Username });

                if (obj[i].Username == "Pending") {
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/marker.png');
                }
                else if (obj[i].Username == "InProgress") {
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/markerIdle.png');
                }
                else if (obj[i].Username == "Completed") {
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/finish-flag.png');
                }
                else if (obj[i].Username == "Accepted") {
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/finish-flag.png');
                }
                else if (obj[i].Username == "OnRoute") {
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/start-flag.png');
                }
                else {
                    marker.setIcon('https://testportalcdn.azureedge.net/Images/marker.png');
                }
                myMarkersTasksLocation[obj[i].Username] = marker;
                createInfoWindowTaskLocation(marker, contentString);
            }
        }
        function createInfoWindowTaskLocation(marker, popupContent) {
            google.maps.event.addListener(marker, 'click', function () {
                infoWindowTaskLocation.setContent(popupContent);
                infoWindowTaskLocation.open(mapTaskLocation, this);
            });
        }

function searchBarcode() {
    jQuery.ajax({
        type: "POST",
        url: "WarehouseP.aspx/searchBarcode",
        data: "{'id':'" + document.getElementById("tbfindRefernce").value + "','uname':'" + loggedinUsername + "'}",
        async: false,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if (data.d[0] == "LOGOUT") {
                showError("Session has expired. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
            else if (data.d[0] == "FAIL") {
                showError("Error occured trying to find barcode.");
            }
            else if (data.d[0] == "ERROR") {
                showError("Could not find barcode.");
            }
            else {
                document.getElementById("tbfinderName").value = data.d[0];
                document.getElementById("tbreceiverName").value = data.d[1];
                document.getElementById("tbItemBrand").value = data.d[2];
                document.getElementById("tbroomNumber").value = data.d[3];
                document.getElementById("MainContent_finderDepartmentSelect").value = data.d[4];
                document.getElementById("MainContent_itemTypeSelect").value = data.d[5];
                document.getElementById("MainContent_locationFoundSelect").value = data.d[6];
                document.getElementById("MainContent_itemColourSelect").value = data.d[7];
                document.getElementById('categorySubMainLocation').innerHTML = "";

                document.getElementById("MainContent_locationStoreSelect").value = data.d[8];
                document.getElementById('itemStorageSelect').innerHTML = "";
                var locluz = data.d[13];
                jQuery.ajax({
                    type: "POST",
                    url: "WarehouseP.aspx/getSubLocations",
                    data: "{'id':'" + document.getElementById("MainContent_locationStoreSelect").value + "','uname':'" + loggedinUsername + "'}",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d[0] == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        } else {
                            var select = document.getElementById('itemStorageSelect');
                            for (var i = 0; i < data.d.length; i++) {
                                var opt = document.createElement('option');
                                opt.value = data.d[i];
                                opt.innerHTML = data.d[i];
                                select.appendChild(opt);
                            }
                            document.getElementById("itemStorageSelect").value = locluz;
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });




                var datesplit = data.d[9].split(' ');
                var dt = datesplit[0].split('/');
                var month = dt[0];
                if (dt[0].length < 2)
                    month = "0" + dt[0];
                var daymonth = dt[1];
                if (dt[1].length < 2)
                    daymonth = "0" + dt[1];
                var dayyear = dt[2];
                document.getElementById("dateFoundCalendar").value = month + "/" + daymonth + "/" + dayyear;
                var tt = datesplit[1].split(':');
                document.getElementById("foundTime").value = tt[0] + ":" + tt[1];

                var rdatesplit = data.d[10].split(' ');
                dt = rdatesplit[0].split('/');
                month = dt[0];
                if (dt[0].length < 2)
                    month = "0" + dt[0];
                daymonth = dt[1];
                if (dt[1].length < 2)
                    daymonth = "0" + dt[1];
                var dayyear = dt[2];
                document.getElementById("dateReceiveCalendar").value = month + "/" + daymonth + "/" + dayyear;
                tt = rdatesplit[1].split(':');
                document.getElementById("receiveTime").value = tt[0] + ":" + tt[1];
                //alert(data.d[10]);
                document.getElementById("imagePath").text = data.d[11].replace(/\\/g, "|")



                document.getElementById("shelfLifeSelect").value = data.d[14];

                document.getElementById("itemSubSelect").innerHTML = "";
                var valuz = data.d[12];
                jQuery.ajax({
                    type: "POST",
                    url: "WarehouseP.aspx/getSubItem",
                    data: "{'id':'" + document.getElementById("MainContent_itemTypeSelect").value + "','uname':'" + loggedinUsername + "'}",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d[0] == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        } else {
                            for (var i = 0; i < data.d.length; i++) {
                                var select = document.getElementById("itemSubSelect");
                                var res = data.d[i].split('_');
                                var opt = document.createElement('option');
                                opt.value = res[0];
                                opt.innerHTML = res[0];
                                select.appendChild(opt);
                            }
                            document.getElementById("itemSubSelect").value = valuz;
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            }
        }
    });
        }
        function generateSparePartBarcode2() {
            var x = 0;
            if (isEmptyOrSpaces(document.getElementById("tbPartBarcode").value)) {
                jQuery.ajax({
                    type: "POST",
                    url: "WarehouseP.aspx/getGenerateSpareBarcode",
                    data: "{'id':'" + x + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                        else if (data.d == "FAIL") {
                            showError("Error occured trying to generate barcode.");
                        }
                        else {
                            var bCode = data.d;
                            document.getElementById("tbPartBarcode").value = bCode;
                            document.getElementById("newbarcodeimgdiv2").style.display = "block"; 
                            JsBarcode("#newbarcode2", bCode); 
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            }
            else {
                   document.getElementById("newbarcodeimgdiv2").style.display = "block"; 
                JsBarcode("#newbarcode2", document.getElementById("tbPartBarcode").value); 
            }
        }
                function generateSparePartBarcode() {
            var x = 0;
            if (isEmptyOrSpaces(document.getElementById("newtbPartBarcode").value)) {
                jQuery.ajax({
                    type: "POST",
                    url: "WarehouseP.aspx/getGenerateSpareBarcode",
                    data: "{'id':'" + x + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                        else if (data.d == "FAIL") {
                            showError("Error occured trying to generate barcode.");
                        }
                        else {
                            var bCode = data.d;
                            document.getElementById("newtbPartBarcode").value = bCode;
                            document.getElementById("newbarcodeimgdiv").style.display = "block"; 
                            JsBarcode("#newbarcode", bCode); 
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            }
            else {
                   document.getElementById("newbarcodeimgdiv").style.display = "block"; 
                JsBarcode("#newbarcode", document.getElementById("newtbPartBarcode").value); 
            }
        }

        function generateAssetBarcode() {
            var x = 0;
            if (isEmptyOrSpaces(document.getElementById("tbAssetBarcode").value)) {
                jQuery.ajax({
                    type: "POST",
                    url: "WarehouseP.aspx/getGenerateAssetBarcode",
                    data: "{'id':'" + x + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                        else if (data.d == "FAIL") {
                            showError("Error occured trying to generate barcode.");
                        }
                        else {
                            var bCode = data.d;
                            document.getElementById("tbAssetBarcode").value = bCode;
                            document.getElementById("barcodeimgdiv").style.display = "block"; 
                            JsBarcode("#barcode", bCode); 
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            }
            else {
                   document.getElementById("barcodeimgdiv").style.display = "block"; 
                JsBarcode("#barcode", document.getElementById("tbAssetBarcode").value); 
            }
        }
function generateBarcode() {
    var x = 0;
    jQuery.ajax({
        type: "POST",
        url: "WarehouseP.aspx/getGenerateBarcode",
        data: "{'id':'" + x + "','uname':'" + loggedinUsername + "'}",
        async: false,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if (data.d == "LOGOUT") {
                showError("Session has expired. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
            else if (data.d == "FAIL") {
                showError("Error occured trying to generate barcode.");
            }
            else {
                var bCode = data.d;
                document.getElementById("tbItemReference").value = bCode;
                document.getElementById("barcodeimgdiv").style.display = "block";
                JsBarcode("#barcode", bCode);
                document.getElementById('tbItemReference').disabled = true;
            }
        },
        error: function () {
            showError("Session timeout. Kindly login again.");
            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
        }
    });
}
function newGenerateBarcode() {
    var bCode = document.getElementById("tbGenerateBarcode").value;
    if (!isEmptyOrSpaces(bCode)) {
        if (bCode.length == 10)
            JsBarcode("#barcode2", bCode);
        else
            showAlert("Please provide 10 character to be made into barcode");
    }
    else {
        showAlert("Please provide text to be made into barcode");
    }
}
function delOffenceTypeSave() {
    jQuery.ajax({
        type: "POST",
        url: "WarehouseP.aspx/delAssetCatSub",
        data: "{'id':'" + document.getElementById('tbOffenceIdChoice').value + "','uname':'" + loggedinUsername + "'}",
        async: false,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if (data.d == "SUCCESS") {
                document.getElementById('successincidentScenario').innerHTML = "Subcategory successfully deleted!";
                jQuery('#successfulDispatch').modal('show');
            }
            else if (data.d == "LOGOUT") {
                showError("Session has expired. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
            else {
                showError(data.d);
            }
        },
        error: function () {
            showError("Session timeout. Kindly login again.");
            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
        }
    });
}
function delOffenceCatTypeSave() {
    jQuery.ajax({
        type: "POST",
        url: "WarehouseP.aspx/delAssetCat",
        data: "{'id':'" + document.getElementById('tbOffenceCatChoice').value + "','uname':'" + loggedinUsername + "'}",
        async: false,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if (data.d == "SUCCESS") {
                document.getElementById('successincidentScenario').innerHTML = "Category successfully deleted!";
                jQuery('#successfulDispatch').modal('show');
            }
            else if (data.d == "LOGOUT") {
                showError("Session has expired. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                    else {
                        showError(data.d);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function rowchoiceOffenceType(id, name, typeId) {
            document.getElementById('tbOffenceIdChoice').value = id;
            document.getElementById('tbEditOffenceName').value = name;
            document.getElementById('MainContent_typeEditSelect').value = typeId;
        }
        function rowchoiceOffenceCategory(id, name) {
            document.getElementById('tbOffenceCatChoice').value = id;
            document.getElementById('tbEditCategoryName').value = name;
        }
        function addrowtoEnquiry() {
            jQuery("#enquiryTable tbody").empty();
            jQuery.ajax({
                type: "POST",
                url: "WarehouseP.aspx/getEnquiryData",
                data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            } else {
                for (var i = 0; i < data.d.length; i++) {
                    jQuery("#enquiryTable tbody").append(data.d[i]);
                }
            }
        },
        error: function () {
            showError("Session timeout. Kindly login again.");
            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
        }
    });
}
function addrowtoTable2() {
    jQuery("#assetTable tbody").empty();
    jQuery.ajax({
        type: "POST",
        url: "WarehouseP.aspx/getAssetCatData",
        data: "{'id':'0','uname':'" + loggedinUsername + "'}",
        async: false,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if (data.d[0] == "LOGOUT") {
                showError("Session has expired. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            } else {
                for (var i = 0; i < data.d.length; i++) {
                    jQuery("#assetTable tbody").append(data.d[i]);
                }
            }
        },
        error: function () {
            showError("Session timeout. Kindly login again.");
            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
        }
    });
}
function addrowtoTable3() {
    jQuery("#assetsubTable tbody").empty();
    jQuery.ajax({
        type: "POST",
        url: "WarehouseP.aspx/getAssetSubCatData",
        data: "{'id':'0','uname':'" + loggedinUsername + "'}",
        async: false,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if (data.d[0] == "LOGOUT") {
                showError("Session has expired. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            } else {
                for (var i = 0; i < data.d.length; i++) {
                    jQuery("#assetsubTable tbody").append(data.d[i]);
                }
            }
        },
        error: function () {
            showError("Session timeout. Kindly login again.");
            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
        }
    });
}
function rowchoiceEnquiry(id) {
    document.getElementById('rowidChoiceENQ').value = id;
    jQuery("#searchTableEnq tbody").empty();
    jQuery.ajax({
        type: "POST",
        url: "WarehouseP.aspx/getTableRowDataEnquiry",
        data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
        async: false,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if (data.d[0] == "LOGOUT") {
                showError("Session has expired. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            } else {
                try {
                    document.getElementById("enquirytbBrand").value = data.d[0];
                    document.getElementById("MainContent_enquiryLocationSelect").value = data.d[1];
                    document.getElementById("MainContent_enquiryColourSelect").value = data.d[2];
                    document.getElementById("MainContent_enquiryTypeSelect").value = data.d[3];
                    document.getElementById("enquirytbDesc").value = data.d[4];
                    document.getElementById("enquirytbName").value = data.d[5];
                    document.getElementById("enquirytbAddress").value = data.d[6];
                    document.getElementById("enquirytbContact").value = data.d[7];
                    document.getElementById("enquirytbRoom").value = data.d[8];

                    document.getElementById("enquiryReference").innerHTML = data.d[9];

                    document.getElementById("enquiryStatus").innerHTML = data.d[10];

                    document.getElementById("enquiryActivity").innerHTML = data.d[11];

                    document.getElementById('enquiryTypeSubSelect').innerHTML = "";

                    document.getElementById("enquiryTypeSubSelect").length

                    var valuz = data.d[13];
                    jQuery.ajax({
                        type: "POST",
                        url: "WarehouseP.aspx/getSubItem",
                        data: "{'id':'" + document.getElementById("MainContent_enquiryTypeSelect").value + "','uname':'" + loggedinUsername + "'}",
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            for (var i = 0; i < data.d.length; i++) {
                                var select = document.getElementById("enquiryTypeSubSelect");
                                var res = data.d[i].split('_');
                                var opt = document.createElement('option');
                                opt.value = res[0];
                                opt.innerHTML = res[0];
                                select.appendChild(opt);
                            }
                            document.getElementById("enquiryTypeSubSelect").value = valuz;
                        }
                    });

                    document.getElementById("enquiryRef").innerHTML = data.d[12];

                    lockEnquiry();

                    if (data.d[10] != "Not Found")
                        document.getElementById("searchEDiv").style.display = "none";

                }
                catch (err) {
                    alert(err)
                }
            }
        },
        error: function () {
            showError("Session timeout. Kindly login again.");
            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
        }
    });
}

function lockEnquiry() {
    document.getElementById("enquirytbBrand").disabled = true;
    document.getElementById("MainContent_enquiryLocationSelect").disabled = true;
    document.getElementById("MainContent_enquiryColourSelect").disabled = true;
    document.getElementById("MainContent_enquiryTypeSelect").disabled = true;

    document.getElementById("enquiryTypeSubSelect").disabled = true;

    document.getElementById("enquirytbDesc").disabled = true;
    document.getElementById("enquirytbName").disabled = true;
    document.getElementById("enquirytbAddress").disabled = true;
    document.getElementById("enquirytbContact").disabled = true;
    document.getElementById("enquirytbRoom").disabled = true;
    document.getElementById("closeEDiv").style.display = "block";
    document.getElementById("searchEDiv").style.display = "block";
    document.getElementById("saveEDiv").style.display = "none";

    document.getElementById("divenquiryActivity").style.display = "block";
    document.getElementById("divenquiry").style.display = "block";

    document.getElementById("divenquiryRef").style.display = "block";


}
function unlockEnquiry() {
    jQuery("#searchTableEnq tbody").empty();

    document.getElementById('rowidChoiceENQ').value = "0";
    document.getElementById("enquirytbBrand").value = "";
    //document.getElementById("MainContent_enquiryLocationSelect").value = "";
    //document.getElementById("MainContent_enquiryColourSelect").value = "";
    //document.getElementById("MainContent_enquiryTypeSelect").value = "";
    document.getElementById("enquirytbDesc").value = "";
    document.getElementById("enquirytbName").value = "";
    document.getElementById("enquirytbAddress").value = "";
    document.getElementById("enquirytbContact").value = "";
    document.getElementById("enquirytbRoom").value = "";

    document.getElementById("enquiryTypeSubSelect").disabled = false;

    document.getElementById("enquiryReference").innerHTML = "";

    document.getElementById("enquiryStatus").innerHTML = "";

    document.getElementById("enquiryActivity").innerHTML = "";

    document.getElementById("enquirytbBrand").disabled = false;
    document.getElementById("MainContent_enquiryLocationSelect").disabled = false;
    document.getElementById("MainContent_enquiryColourSelect").disabled = false;
    document.getElementById("MainContent_enquiryTypeSelect").disabled = false;

    document.getElementById("enquiryTypeSubSelect").disabled = false;

    document.getElementById("enquirytbDesc").disabled = false;
    document.getElementById("enquirytbName").disabled = false;
    document.getElementById("enquirytbAddress").disabled = false;
    document.getElementById("enquirytbContact").disabled = false;
    document.getElementById("enquirytbRoom").disabled = false;
    document.getElementById("saveEDiv").style.display = "block";
    document.getElementById("closeEDiv").style.display = "none";
    document.getElementById("searchEDiv").style.display = "none";



    document.getElementById("divenquiryActivity").style.display = "none";
    document.getElementById("divenquiry").style.display = "none";

    document.getElementById("divenquiryRef").style.display = "none";


}

function processDispose() {
    try {
        var itemid = document.getElementById('rowidChoice').value;
        var itemreference = document.getElementById("fItemReference").innerHTML;
        var disposeSelect = document.getElementById("disposeToSelect").value;
        var disposeName = document.getElementById("tbDisposeName").value;
        var disposeCompany = document.getElementById("tbDisposeCompany").value;
        var disposeLocation = document.getElementById("tbDisposeLocation").value;
        var disposeContact = document.getElementById("tbDisposeContact").value;
        var disposeIDNumber = document.getElementById("tbDisposeIDNumber").value;
        var sop = document.getElementById("sopSelect").value;
        var disposeAfter = document.getElementById("disposedAfterSelect").value;
        var disposeWitness = document.getElementById("tbDisposeWitness").value;
        var imgPath = document.getElementById("imagePathDispose").text;

        var isGood = true;
        if (isEmptyOrSpaces(disposeName)) {
            isGood = false;
            showAlert("Please provide recipient name.")
        }
        else if (isEmptyOrSpaces(disposeCompany)) {
            isGood = false;
            showAlert("Please provide recipient company.")
        }
        else if (isEmptyOrSpaces(disposeLocation)) {
            isGood = false;
            showAlert("Please provide recipient location.")
        }
        else if (isEmptyOrSpaces(disposeContact)) {
            isGood = false;
            showAlert("Please provide recipient contact.")
        }
        else if (isEmptyOrSpaces(disposeIDNumber)) {
            isGood = false;
            showAlert("Please provide recipient ID.")
        }
        else if (isEmptyOrSpaces(disposeWitness)) {
            isGood = false;
            showAlert("Please provide witness.")
        }
        else {
            if (isSpecialChar(disposeName)) {
                isGood = false;
            }
            else if (isSpecialChar(disposeCompany)) {
                isGood = false;
            }
            else if (isSpecialChar(disposeLocation)) {
                isGood = false;
            }
            else if (isSpecialChar(disposeContact)) {
                isGood = false;
            }
            else if (isSpecialChar(disposeIDNumber)) {
                isGood = false;
            }
            else if (isSpecialChar(disposeWitness)) {
                isGood = false;
            }
            else if (!isNumeric(disposeContact)) {
                isGood = false;
                showAlert("Please provide valid contact number.")
            }
        }
        if (isGood) {
            jQuery.ajax({
                type: "POST",
                url: "WarehouseP.aspx/saveDisposeItemData",
                data: "{'disposeSelect':'" + disposeSelect + "','disposeName':'" + disposeName + "','disposeCompany':'" + disposeCompany
                    + "','disposeLocation':'" + disposeLocation + "','disposeContact':'" + disposeContact + "','disposeIDNumber':'" + disposeIDNumber
                    + "','sop':'" + sop + "','disposeAfter':'" + disposeAfter + "','disposeWitness':'" + disposeWitness
                    + "','imgPath':'" + imgPath + "','uname':'" + loggedinUsername + "','itemreference':'" + itemreference + "','itemid':'" + itemid + "','eId':'" + document.getElementById('rowidChoiceENQ').value + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                    else if (data.d == "SUCCESS") {
                        $('#foundItemViewCard').modal('hide');
                        document.getElementById('successMessage').innerHTML = "Item has successfully been disposed";
                        jQuery('#successfulModal').modal('show');
                    }
                    else {
                        showError(data.d);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
    }
    catch (err) {
        alert(err)
    }
}
function saveEnquiry() {
    var itemBrnd = document.getElementById("enquirytbBrand").value;
    var locFound = document.getElementById("MainContent_enquiryLocationSelect").value;
    var itemColor = document.getElementById("MainContent_enquiryColourSelect").value;
    var itemType = document.getElementById("MainContent_enquiryTypeSelect").value;
    var itemDesc = document.getElementById("enquirytbDesc").value;
    var itemName = document.getElementById("enquirytbName").value;
    var itemAddress = document.getElementById("enquirytbAddress").value;
    var itemContact = document.getElementById("enquirytbContact").value;
    var itemRoom = document.getElementById("enquirytbRoom").value;
    var subtype = document.getElementById("enquiryTypeSubSelect").value;
    var isGood = true;
    if (isEmptyOrSpaces(itemBrnd)) {
        isGood = false;
        showAlert("Please provide item brand.")
    }
    else if (isEmptyOrSpaces(itemName)) {
        isGood = false;
        showAlert("Please provide enquirer name.")
    }
    else if (isEmptyOrSpaces(itemAddress)) {
        isGood = false;
        showAlert("Please provide enquirer address.")
    }
    else if (isEmptyOrSpaces(itemContact)) {
        isGood = false;
        showAlert("Please provide enquirer contact.")
    }
    else {
        if (isSpecialChar(itemBrnd)) {
            isGood = false;
        }
        else if (isSpecialChar(itemDesc)) {
            isGood = false;
        }
        else if (isSpecialChar(itemName)) {
            isGood = false;
        }
        else if (isSpecialChar(itemAddress)) {
            isGood = false;
        }
        else if (isSpecialChar(itemContact)) {
            isGood = false;
        }
        else if (!isNumeric(itemContact)) {
            isGood = false;
            showAlert("Please provide valid contact number.")
        }
    }
    if (isGood) {
        jQuery.ajax({
            type: "POST",
            url: "WarehouseP.aspx/saveItemEnquiryData",
            data: "{'itemBrnd':'" + itemBrnd + "','locFound':'" + locFound + "','itemName':'" + itemName
                    + "','itemColor':'" + itemColor + "','itemType':'" + itemType + "','itemDesc':'" + itemDesc
                    + "','itemAddress':'" + itemAddress + "','itemContact':'" + itemContact + "','itemRoom':'" + itemRoom
                    + "','subtype':'" + subtype + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    if (data.d.length > 0) {
                        document.getElementById('rowidChoiceENQ').value = data.d[0];
                        document.getElementById('enquiryRef').innerHTML = data.d[1];
                        lockEnquiry();
                        showAlert("Successfully saved item enquiry");
                    }
                    else {
                        showError("Error occured while trying to save item enquiry. kindly check logs for more information.");
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
}
function getChecklistItems(id) {
    document.getElementById("checklistItemsList").innerHTML = "";
    $.ajax({
        type: "POST",
        url: "WarehouseP.aspx/getGroupItemsData",
        data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
        async: false,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if (data.d[0] == "LOGOUT") {
                showError("Session has expired. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            } else {
                if (data.d.length > 0) {
                    document.getElementById("groupedItemsDIV").style.display = "block";
                    for (var i = 0; i < data.d.length; i++) {
                        var res = data.d[i].split("|");
                        var ul = document.getElementById("checklistItemsList");
                        var li = document.createElement("li");

                        li.innerHTML = '<i class="fa fa-object-group"></i><a onclick="rowchoiceFound(' + res[0] + ')" style="margin-left:5px;" href="#"  class="capitalize-text" >' + res[1] + '</a>';

                        ul.appendChild(li);
                    }
                }
                else {
                    document.getElementById("groupedItemsDIV").style.display = "none";
                }
            }
        },
        error: function () {
            showError("Session timeout. Kindly login again.");
            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
        }
    });

}
function searchEnquiry() {
    jQuery("#searchTableEnq tbody").empty();
    jQuery.ajax({
        type: "POST",
        url: "WarehouseP.aspx/searchForItemsTable",
        data: "{'searchItemTypeSelect':'" + document.getElementById("MainContent_enquiryTypeSelect").value + "','dateFromCalendar':'','FromTime':'','dateToCalendar':'','ToTime':'','tbSearchBrand':'" + document.getElementById("enquirytbBrand").value
            + "','searchLocationSelect':'" + document.getElementById("MainContent_enquiryLocationSelect").value
    + "','searchColorSelect':'" + document.getElementById("MainContent_enquiryColourSelect").value + "','searchItemSubSelect':'" + document.getElementById("enquiryTypeSubSelect").value
            + "','searchLocationStoreSelect':'','searchSubStoreSelect':'','searchShelfLifeSelect':'','description':'" + document.getElementById("enquirytbDesc").value + "','uname':'" + loggedinUsername + "','eId':'" + document.getElementById('rowidChoiceENQ').value + "'}",
        async: false,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if (data.d[0] == "LOGOUT") {
                showError("Session has expired. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            } else {
                if (data.d.length > 0) {
                    for (var i = 0; i < data.d.length; i++) {
                        jQuery("#searchTableEnq tbody").append(data.d[i]);
                    }
                }
                else {
                    showAlert("No Items Found");
                    rowchoiceEnquiry(document.getElementById('rowidChoiceENQ').value);
                }
            }
        },
        error: function () {
            showError("Session timeout. Kindly login again.");
            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
        }
    });
}
function closeEnquiry() {
    location.reload();
    showLoader();
}
function searchForItem() {
    try {
        jQuery("#searchTable tbody").empty();
        if (!isEmptyOrSpaces(document.getElementById('tbSearchBrand').value)) {
            if (!isSpecialChar(document.getElementById('tbSearchBrand').value)) {
                jQuery.ajax({
                    type: "POST",
                    url: "WarehouseP.aspx/searchForItemsTable",
                    data: "{'searchItemTypeSelect':'" + document.getElementById("MainContent_searchItemTypeSelect").value + "','dateFromCalendar':'" + document.getElementById("dateFromCalendar").value
                + "','FromTime':'" + document.getElementById("FromTime").value + "','dateToCalendar':'" + document.getElementById("dateToCalendar").value + "','ToTime':'" + document.getElementById("ToTime").value
                + "','tbSearchBrand':'" + document.getElementById("tbSearchBrand").value + "','searchLocationSelect':'" + document.getElementById("MainContent_searchLocationSelect").value
                + "','searchColorSelect':'" + document.getElementById("MainContent_searchColorSelect").value + "','searchItemSubSelect':'" + document.getElementById("searchItemSubSelect").value
                + "','searchLocationStoreSelect':'" + document.getElementById("MainContent_searchLocationStoreSelect").value + "','searchSubStoreSelect':'" + document.getElementById("searchSubStoreSelect").value
                + "','searchShelfLifeSelect':'" + document.getElementById("searchShelfLifeSelect").value + "','description':'','uname':'" + loggedinUsername + "','eId':'0'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d[0] == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        } else {
                            if (data.d.length > 0) {
                                for (var i = 0; i < data.d.length; i++) {
                                    jQuery("#searchTable tbody").append(data.d[i]);
                                }
                            }
                            else {
                                showAlert("No Items Found");
                            }
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            }
        }
        else {
            jQuery.ajax({
                type: "POST",
                url: "WarehouseP.aspx/searchForItemsTable",
                data: "{'searchItemTypeSelect':'" + document.getElementById("MainContent_searchItemTypeSelect").value + "','dateFromCalendar':'" + document.getElementById("dateFromCalendar").value
            + "','FromTime':'" + document.getElementById("FromTime").value + "','dateToCalendar':'" + document.getElementById("dateToCalendar").value + "','ToTime':'" + document.getElementById("ToTime").value
            + "','tbSearchBrand':'" + document.getElementById("tbSearchBrand").value + "','searchLocationSelect':'" + document.getElementById("MainContent_searchLocationSelect").value
            + "','searchColorSelect':'" + document.getElementById("MainContent_searchColorSelect").value + "','searchItemSubSelect':'" + document.getElementById("searchItemSubSelect").value
            + "','searchLocationStoreSelect':'" + document.getElementById("MainContent_searchLocationStoreSelect").value + "','searchSubStoreSelect':'" + document.getElementById("searchSubStoreSelect").value
            + "','searchShelfLifeSelect':'" + document.getElementById("searchShelfLifeSelect").value + "','description':'','uname':'" + loggedinUsername + "','eId':'0'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        if (data.d.length > 0) {
                            for (var i = 0; i < data.d.length; i++) {
                                jQuery("#searchTable tbody").append(data.d[i]);
                            }
                        }
                        else {
                            showAlert("No Items Found");
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
    }
    catch (err) {
        alert(err);
    }
}
function processReturn() {
    try {
        var itemid = document.getElementById('rowidChoice').value;
        var itemreference = document.getElementById("fItemReference").innerHTML;
        var ownerNationality = document.getElementById("ownerNationalitySelect").value;
        var ownerNumber = document.getElementById("tbOwnerNumber").value;
        var ownerName = document.getElementById("tbOwnerName").value;
        var ownerAddress = document.getElementById("tbOwnerAddress").value;
        var imgPath = document.getElementById("imagePathReturn").text;
        var ownerTypeSelect = document.getElementById("ownerTypeSelect").value;
        var ownerContact = document.getElementById("tbOwnerContact").value;
        var shipTracking = document.getElementById("tbShippingTracking").value;
        var ownerEmail = document.getElementById("tbOwnerEmail").value;
        var ownerRoomNumber = document.getElementById("tbRetRoomNumber").value;
        var siteTransfer = document.getElementById('MainContent_transferSiteSelect').value;
        var isTransfer = document.getElementById("transferCheck").checked;
        if (!isTransfer)
            siteTransfer = "0";


        var isGood = true;
        if (isEmptyOrSpaces(ownerNumber)) {
            isGood = false;
            showAlert("Please provide number.")
        }
        else if (isEmptyOrSpaces(ownerName)) {
            isGood = false;
            showAlert("Please provide name.")
        }
        else if (isEmptyOrSpaces(ownerAddress)) {
            isGood = false;
            showAlert("Please provide address.")
        }
        else if (isEmptyOrSpaces(ownerContact)) {
            isGood = false;
            showAlert("Please provide contact.")
        }
        else if (isEmptyOrSpaces(ownerEmail)) {
            isGood = false;
            showAlert("Please provide email.")
        }
        else {

            if (isSpecialChar(ownerNumber)) {
                isGood = false;
            }
            else if (isSpecialChar(ownerName)) {
                isGood = false;
            }
            else if (isSpecialChar(ownerAddress)) {
                isGood = false;
            }
            else if (isSpecialChar(ownerContact)) {
                isGood = false;
            }
            else if (isInvalidEmail(ownerEmail)) {
                isGood = false;
            }
            else if (!isNumeric(ownerContact)) {
                isGood = false;
                showAlert("Please provide valid contact number.")
            }
            if (ownerTypeSelect == "In-house-guest") {
                if (isEmptyOrSpaces(ownerRoomNumber)) {
                    isGood = false;
                    showAlert("Please provide room number.")
                }
                else if (isSpecialChar(ownerRoomNumber)) {
                    isGood = false;
                }
            }


            if (typeof document.getElementById("imagePathReturn").text === 'undefined' || isEmptyOrSpaces(imgPath)) {

                isGood = false;
                showAlert("Please attach item image.")

            }

        }
        if (isGood) {
            jQuery.ajax({
                type: "POST",
                url: "WarehouseP.aspx/saveReturnTimeData",
                data: "{'ownerNationality':'" + ownerNationality + "','ownerNumber':'" + ownerNumber + "','ownerName':'" + ownerName
                    + "','ownerAddress':'" + ownerAddress + "','imgPath':'" + imgPath + "','ownerTypeSelect':'" + ownerTypeSelect
                    + "','ownerContact':'" + ownerContact + "','shipTracking':'" + shipTracking + "','ownerEmail':'" + ownerEmail
                    + "','ownerRoomNumber':'" + ownerRoomNumber + "','uname':'" + loggedinUsername + "','itemreference':'" + itemreference
                    + "','itemid':'" + itemid + "','eId':'" + document.getElementById('rowidChoiceENQ').value + "','siteTransfer':'" + siteTransfer + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                    else if (data.d == "SUCCESS") {
                        $('#foundItemViewCard').modal('hide');
                        document.getElementById('successMessage').innerHTML = "Item has successfully been returned";
                        jQuery('#successfulModal').modal('show');
                    }
                    else {
                        showError(data.d);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
    }
    catch (err) {
        alert(err)
    }
        }
        var gcolor = "#3ebb64";
        var gcolor2 = "#027e27";
        function ddetailsClick() {

            document.getElementById("aattachClickLI").style.backgroundColor = "lightgray";
            document.getElementById("ssparepartClickLI").style.backgroundColor = "lightgray";
            document.getElementById("pplansClickLI").style.backgroundColor = "lightgray";
            document.getElementById("rremarksClickLI").style.backgroundColor = "lightgray";
            document.getElementById("ddetailsClickLI").style.backgroundColor = "lightgray";

            document.getElementById("aattachClickLI").style.borderColor = "lightgray";
            document.getElementById("ssparepartClickLI").style.borderColor = "lightgray";
            document.getElementById("pplansClickLI").style.borderColor = "lightgray";
            document.getElementById("rremarksClickLI").style.borderColor = "lightgray";
            document.getElementById("ddetailsClickLI").style.borderColor = "lightgray";

            document.getElementById('rremarksClickLI').style.pointerEvents = "none";
            document.getElementById('ddetailsClickLI').style.pointerEvents = "none";
            document.getElementById('pplansClickLI').style.pointerEvents = "none";
            document.getElementById('ssparepartClickLI').style.pointerEvents = "none";
            document.getElementById('aattachClickLI').style.pointerEvents = "none";

            document.getElementById("adetails-div").style.display = "block";
            document.getElementById("aplan-div").style.display = "none";
            document.getElementById("aspareparts-div").style.display = "none";
            document.getElementById("aremarks-div").style.display = "none";
            document.getElementById("aattachments-div").style.display = "none";

            document.getElementById("tbAssetName").value = "";
            document.getElementById("tbAssetComments").value = "";
            document.getElementById("tbAssetMake").value = "";
            document.getElementById("tbAssetModel").value = "";
            document.getElementById("tbAssetBarcode").value = "";
            document.getElementById("tbAssetSNumber").value = "";
            document.getElementById("tbAssetContractor").value = "";

            document.getElementById("tbNewTaskName").value = "";
            document.getElementById("tbNewDescription").value = "";
            document.getElementById("aremarks").value = "";
            jQuery('#dassetRemarksList div').html('');

            document.getElementById("tbPartName").value = "";
            document.getElementById("tbPartDesc").value = "";
            document.getElementById("tbPartMake").value = "";
            document.getElementById("tbPartModel").value = "";
            document.getElementById("tbPartSerial").value = "";
            document.getElementById("tbPartQT").value = "";

            document.getElementById("barcodeimgdiv").style.display = "none";
            document.getElementById('barcode').src = '';
            jQuery('#aattachments-info-tab div').html('');

            document.getElementById("newbarcodeimgdiv2").style.display = "none";
            document.getElementById('newbarcode2').src = '';

            document.getElementById("displayplanassetDIV").style.display = "none";

        }

        function clearnewSpare() {
            document.getElementById("newtbPartName").value = "";
            document.getElementById("newtbPartDesc").value = "";
            document.getElementById("newtbPartMake").value = "";
            document.getElementById("newtbPartModel").value = "";
            document.getElementById("newtbPartSerial").value = "";
            document.getElementById("newtbPartQT").value = "";
            document.getElementById("newtbPartBarcode").value = "";
            document.getElementById("newbarcodeimgdiv").style.display = "none";
            document.getElementById("linsspSave").style.display = "block";
                document.getElementById("linsspUpdate").style.display = "none";
             
            document.getElementById('newbarcode').src = '';
        }

        function rremarksClick() {

            document.getElementById("aattachClickLI").style.backgroundColor  = gcolor;
            document.getElementById("ssparepartClickLI").style.backgroundColor  = gcolor;
            document.getElementById("pplansClickLI").style.backgroundColor  = gcolor;
            document.getElementById("rremarksClickLI").style.backgroundColor  = gcolor2;
            document.getElementById("ddetailsClickLI").style.backgroundColor = gcolor;

            document.getElementById("aattachClickLI").style.borderColor   = gcolor;
            document.getElementById("ssparepartClickLI").style.borderColor   = gcolor;
            document.getElementById("pplansClickLI").style.borderColor   = gcolor;
            document.getElementById("rremarksClickLI").style.borderColor   = gcolor2;
            document.getElementById("ddetailsClickLI").style.borderColor   = gcolor;

            document.getElementById("adetails-div").style.display = "none";
            document.getElementById("aplan-div").style.display = "none";
            document.getElementById("aspareparts-div").style.display = "none";
            document.getElementById("aremarks-div").style.display = "block";
            document.getElementById("aattachments-div").style.display = "none";
            document.getElementById("updateAssetItemLi").style.display = "none";  
            document.getElementById("saveAssetItemLi").style.display = "none";  
            document.getElementById("saveAssetSpareLi").style.display = "none"; 
            document.getElementById("saveAssetRemarksLi").style.display = "block";  
            document.getElementById("saveAssetPlanLi").style.display = "none"; 
            document.getElementById("saveAssetPlanTempLi").style.display = "none"; 
            document.getElementById("updateAssetPlanLi").style.display = "none"; 
            
            document.getElementById("assetHeader").innerHTML = "ADD ASSET - REMARKS";
        }

        function newPlanClick(id) {
            pplansClick();
            document.getElementById('plantempassetdiv').style.display = "block";
            document.getElementById("assetHeader").innerHTML = "ADD MAINTENANCE PLAN"; 
            document.getElementById("displayplanassetDIV").style.display = "block";
            document.getElementById('MainContent_assetplanchartSelect').disabled = "";

            document.getElementById("MainContent_assetplanchartSelect").value = id;
            jQuery('#MainContent_assetplanchartSelect').selectpicker('val', id);

            document.getElementById("aattachClickLI").style.display = "none";
            document.getElementById("ssparepartClickLI").style.display = "none";
            document.getElementById("pplansClickLI").style.display = "none";
            document.getElementById("rremarksClickLI").style.display = "none";
            document.getElementById("ddetailsClickLI").style.display = "none";
            document.getElementById("saveAssetItemLi").style.display = "none";
            document.getElementById("updateAssetItemLi").style.display = "none";  
            document.getElementById("saveAssetSpareLi").style.display = "none";
            document.getElementById("saveAssetRemarksLi").style.display = "none";
            document.getElementById("saveAssetPlanLi").style.display = "block"; 
             document.getElementById("saveAssetPlanTempLi").style.display = "block"; 
            document.getElementById("updateAssetPlanLi").style.display = "none"; 
            document.getElementById("tbNewTaskName").value = "";
            document.getElementById("tbNewDescription").value = "";
            document.getElementById('newAssetID').value = "";
            fromAddPlanToAsset = true;
        }

        function pplansClick() {

            document.getElementById("aattachClickLI").style.backgroundColor  = gcolor;
            document.getElementById("ssparepartClickLI").style.backgroundColor  = gcolor;
            document.getElementById("pplansClickLI").style.backgroundColor  = gcolor2;
            document.getElementById("rremarksClickLI").style.backgroundColor  = gcolor;
            document.getElementById("ddetailsClickLI").style.backgroundColor = gcolor;

            document.getElementById("aattachClickLI").style.borderColor   = gcolor;
            document.getElementById("ssparepartClickLI").style.borderColor   = gcolor;
            document.getElementById("pplansClickLI").style.borderColor   = gcolor2;
            document.getElementById("rremarksClickLI").style.borderColor   = gcolor;
            document.getElementById("ddetailsClickLI").style.borderColor   = gcolor;

            document.getElementById("adetails-div").style.display = "none";
            document.getElementById("aplan-div").style.display = "block";
            document.getElementById("aspareparts-div").style.display = "none";
            document.getElementById("aremarks-div").style.display = "none";
            document.getElementById("aattachments-div").style.display = "none";

            document.getElementById("updateAssetItemLi").style.display = "none";  
            document.getElementById("saveAssetItemLi").style.display = "none";  
            document.getElementById("saveAssetSpareLi").style.display = "none"; 
            document.getElementById("saveAssetRemarksLi").style.display = "none";  
            document.getElementById("saveAssetPlanLi").style.display = "block"; 
            document.getElementById("updateAssetPlanLi").style.display = "none"; 
             document.getElementById("saveAssetPlanTempLi").style.display = "block"; 
            document.getElementById("assetHeader").innerHTML = "ADD ASSET - MAINTENANCE PLAN";

        }
        function ssparepartClick() {

            document.getElementById("aattachClickLI").style.backgroundColor  = gcolor;
            document.getElementById("ssparepartClickLI").style.backgroundColor  = gcolor2;
            document.getElementById("pplansClickLI").style.backgroundColor  = gcolor;
            document.getElementById("rremarksClickLI").style.backgroundColor  = gcolor;
            document.getElementById("ddetailsClickLI").style.backgroundColor = gcolor;

            document.getElementById("aattachClickLI").style.borderColor   = gcolor;
            document.getElementById("ssparepartClickLI").style.borderColor   = gcolor2;
            document.getElementById("pplansClickLI").style.borderColor   = gcolor;
            document.getElementById("rremarksClickLI").style.borderColor   = gcolor;
            document.getElementById("ddetailsClickLI").style.borderColor   = gcolor;

            document.getElementById("adetails-div").style.display = "none";
            document.getElementById("aplan-div").style.display = "none";
            document.getElementById("aspareparts-div").style.display = "block";
            document.getElementById("aremarks-div").style.display = "none";
            document.getElementById("aattachments-div").style.display = "none";
            document.getElementById("updateAssetItemLi").style.display = "none";  
            document.getElementById("saveAssetItemLi").style.display = "none";  
            document.getElementById("saveAssetSpareLi").style.display = "block"; 
            document.getElementById("saveAssetRemarksLi").style.display = "none";  
            document.getElementById("saveAssetPlanLi").style.display = "none"; 
            document.getElementById("saveAssetPlanTempLi").style.display = "none"; 
            document.getElementById("updateAssetPlanLi").style.display = "none"; 
            document.getElementById("assetHeader").innerHTML = "ADD ASSET - SPARE PART";
        }
        function aattachClick() {
            //#027e27
            //background-color:#3ebb64;border-color:#3ebb64
         document.getElementById("assetHeader").innerHTML = "ADD ASSET - ATTACHMENT";
            document.getElementById("aattachClickLI").style.backgroundColor  = gcolor2;
            document.getElementById("ssparepartClickLI").style.backgroundColor  = gcolor;
            document.getElementById("pplansClickLI").style.backgroundColor  = gcolor;
            document.getElementById("rremarksClickLI").style.backgroundColor  = gcolor;
            document.getElementById("ddetailsClickLI").style.backgroundColor = gcolor;

            document.getElementById("aattachClickLI").style.borderColor   = gcolor2;
            document.getElementById("ssparepartClickLI").style.borderColor   = gcolor;
            document.getElementById("pplansClickLI").style.borderColor   = gcolor;
            document.getElementById("rremarksClickLI").style.borderColor   = gcolor;
            document.getElementById("ddetailsClickLI").style.borderColor   = gcolor;

            document.getElementById("adetails-div").style.display = "none";
            document.getElementById("aplan-div").style.display = "none";
            document.getElementById("aspareparts-div").style.display = "none";
            document.getElementById("aremarks-div").style.display = "none";
            document.getElementById("aattachments-div").style.display = "block";
                        document.getElementById("updateAssetItemLi").style.display = "none";  
                        document.getElementById("saveAssetItemLi").style.display = "none";  
            document.getElementById("saveAssetSpareLi").style.display = "none"; 
            document.getElementById("saveAssetRemarksLi").style.display = "none";  
            document.getElementById("saveAssetPlanLi").style.display = "none"; 
            document.getElementById("saveAssetPlanTempLi").style.display = "none"; 
            document.getElementById("updateAssetPlanLi").style.display = "none"; 
        }
function clearAssetItem() {
    document.getElementById("tbAssetComments").value = "";
    document.getElementById("tbAssetName").value = "";
    document.getElementById('tbAssetSNumber').value = "";
    document.getElementById('tbAssetBarcode').value = "";
    document.getElementById('barcode').src = '';
}
var isKey = false;
        function assetClick(e) {
            fromAddPlanToAsset = false;
            if (e == "ASSET") {
                document.getElementById("assetHeader").innerHTML = "ADD ASSET";
                document.getElementById("assetName").innerHTML = "*Asset Name";
                document.getElementById("assetCom").innerHTML = "Asset Description";
                isKey = false;

                ddetailsClick();


                document.getElementById("aattachClickLI").style.display = "block";
                document.getElementById("ssparepartClickLI").style.display = "block";
                document.getElementById("pplansClickLI").style.display = "block";
                document.getElementById("rremarksClickLI").style.display = "block";
                document.getElementById("ddetailsClickLI").style.display = "none";
                
                document.getElementById("saveAssetRemarksLi").style.display = "none";
                document.getElementById("saveAssetSpareLi").style.display = "none"; 
                document.getElementById("updateAssetItemLi").style.display = "none";  
                document.getElementById("saveAssetItemLi").style.display = "block";
                document.getElementById("saveAssetPlanLi").style.display = "none";
                document.getElementById("saveAssetPlanTempLi").style.display = "none"; 
                document.getElementById("updateAssetPlanLi").style.display = "none"; 
            }
            else if (e == "KEY") {
                document.getElementById("assetHeader").innerHTML = "ADD KEY";
                document.getElementById("assetName").innerHTML = "*Key Name";
                document.getElementById("assetCom").innerHTML = "*Key Comment";
                isKey = true;
            }
        }
        function taskselectAssigneeTypeChange() {
            var assignType = document.getElementById("taskSelectAssigneeType").value;
            if (document.getElementById("taskSelectAssigneeType").selectedIndex == 0)//assignType == "User") 
            {
                document.getElementById("usersearchSelectDIV").style.display = "block";
                document.getElementById("groupsearchSelectDIV").style.display = "none"; 
            }
            else if (document.getElementById("taskSelectAssigneeType").selectedIndex == 1)//(assignType == "Group") 
            {
                document.getElementById("usersearchSelectDIV").style.display = "none";
                document.getElementById("groupsearchSelectDIV").style.display = "block"; 
            } 
        }
        function updatesavespareParts() {
            try {
                var aName = document.getElementById("newtbPartName").value;
                var aComment = document.getElementById("newtbPartDesc").value;
                var aAsset = document.getElementById('newsparecategorySubAssetSelect').value;
                var snumber = document.getElementById('newtbPartSerial').value;
                var asset = document.getElementById('MainContent_newspareassetCategorySelect').value;
                var imgPath = document.getElementById('newimagePathPartAsset').text;
                var pmake = document.getElementById('newtbPartMake').value;
                var pmodel = document.getElementById('newtbPartModel').value;
                var premarks = document.getElementById('newtbPartQT').value;
                var bcodes = document.getElementById('newtbPartBarcode').value;
                var isGood = true;
                if (isEmptyOrSpaces(aName)) {
                    isGood = false;
                    showAlert("Please provide part name.")
                }
                else {
                    if (isSpecialChar(aName)) {
                        isGood = false;
                    }

                    if (isEmptyOrSpaces(snumber)) {

                    }
                    else if (isSpecialChar(snumber)) {
                        isGood = false;
                    }

                    if (isEmptyOrSpaces(aComment)) {

                    }
                    else if (isSpecialChar(aComment)) {
                        isGood = false;
                    }
                }
                if (isGood) {
                    jQuery.ajax({
                        type: "POST",
                        url: "WarehouseP.aspx/updateAssetSparePart",
                        data: "{'name':'" + aName + "','comment':'" + aComment + "','asset':'" + aAsset
                            + "','imgpath':'" + imgPath + "','masset':'" + asset + "','serialno':'" + snumber
                            + "','uname':'" + loggedinUsername
                            + "','assetmake':'" + pmake + "','assetmodel':'" + pmodel
                            + "','aremarks':'" + premarks
                            + "','bcodes':'" + bcodes
                            + "','assetid':'" + document.getElementById('MainContent_assetsparechartSelect').value
                            + "','partid':'" + document.getElementById('rowidChoice').value + "'}",
                        async: false,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            if (isNumeric(data.d)) {
                                $('#newSpareModal').modal('hide');

                                showAlert("Sparepart has successfully been updated");
                                //document.getElementById('successMessage').innerHTML = "Sparepart has successfully been created";
                                //jQuery('#successfulModal').modal('show');
                                addrowtoTableSpare();
                            }
                            else if (data.d == "LOGOUT") {
                                showError("Session has expired. Kindly login again.");
                                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                            }
                            else {
                                showError(data.d);
                            }
                        },
                        error: function () {
                            showError("Session timeout. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                    });
                }
            }
            catch (err) {
                alert(err)
            }
        }
                function newsavespareParts() {
            try {
                var aName = document.getElementById("newtbPartName").value;
                var aComment = document.getElementById("newtbPartDesc").value;
                var aAsset = document.getElementById('newsparecategorySubAssetSelect').value;
                var snumber = document.getElementById('newtbPartSerial').value;
                var asset = document.getElementById('MainContent_newspareassetCategorySelect').value;
                var imgPath = document.getElementById('newimagePathPartAsset').text;
                var pmake = document.getElementById('newtbPartMake').value;
                var pmodel = document.getElementById('newtbPartModel').value;
                var premarks = document.getElementById('newtbPartQT').value;
                var bcodes = document.getElementById('newtbPartBarcode').value;
                var isGood = true;
                if (isEmptyOrSpaces(aName)) {
                    isGood = false;
                    showAlert("Please provide part name.")
                }
                else {
                    if (isSpecialChar(aName)) {
                        isGood = false;
                    }

                    if (isEmptyOrSpaces(snumber)) {

                    }
                    else if (isSpecialChar(snumber)) {
                        isGood = false;
                    }

                    if (isEmptyOrSpaces(aComment)) {

                    }
                    else if (isSpecialChar(aComment)) {
                        isGood = false;
                    }
                }
                if (isGood) {
                    jQuery.ajax({
                        type: "POST",
                        url: "WarehouseP.aspx/saveAssetSparePart",
                        data: "{'name':'" + aName + "','comment':'" + aComment + "','asset':'" + aAsset
                            + "','imgpath':'" + imgPath + "','masset':'" + asset + "','serialno':'" + snumber
                            + "','uname':'" + loggedinUsername
                            + "','assetmake':'" + pmake + "','assetmodel':'" + pmodel
                            + "','aremarks':'" + premarks
                            + "','bcodes':'" + bcodes
                            + "','assetid':'" + document.getElementById('MainContent_assetsparechartSelect').value + "'}",
                        async: false,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            if (isNumeric(data.d)) {
                                $('#newSpareModal').modal('hide');

                                showAlert("Sparepart has successfully been created");
                                //document.getElementById('successMessage').innerHTML = "Sparepart has successfully been created";
                                //jQuery('#successfulModal').modal('show');
                                addrowtoTableSpare();
                            }
                            else if (data.d == "LOGOUT") {
                                showError("Session has expired. Kindly login again.");
                                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                            }
                            else {
                                showError(data.d);
                            }
                        },
                        error: function () {
                            showError("Session timeout. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                    });
                }
            }
            catch (err) {
                alert(err)
            }
        }

        function savespareParts() {
            try {
                var aName = document.getElementById("tbPartName").value;
                var aComment = document.getElementById("tbPartDesc").value;
                var aAsset = document.getElementById('sparecategorySubAssetSelect').value;
                var snumber = document.getElementById('tbPartSerial').value;
                var asset = document.getElementById('MainContent_sparecategoryAssetSelect').value;
                var imgPath = document.getElementById('imagePathPartAsset').text;
                var pmake = document.getElementById('tbPartMake').value;
                var pmodel = document.getElementById('tbPartModel').value;
                var premarks = document.getElementById('tbPartQT').value;
                var bcodes = document.getElementById('tbPartBarcode').value;
                var isGood = true;
                if (isEmptyOrSpaces(aName)) {
                    isGood = false;
                    showAlert("Please provide part name.")
                }
                else {
                    if (isSpecialChar(aName)) {
                        isGood = false;
                    }

                    if (isEmptyOrSpaces(snumber)) {

                    }
                    else if (isSpecialChar(snumber)) {
                        isGood = false;
                    }

                    if (isEmptyOrSpaces(aComment)) {

                    }
                    else if (isSpecialChar(aComment)) {
                        isGood = false;
                    }
                }
                if (isGood) {
                    jQuery.ajax({
                        type: "POST",
                        url: "WarehouseP.aspx/saveAssetSparePart",
                        data: "{'name':'" + aName + "','comment':'" + aComment + "','asset':'" + aAsset
                            + "','imgpath':'" + imgPath + "','masset':'" + asset + "','serialno':'" + snumber
                            + "','uname':'" + loggedinUsername
                            + "','assetmake':'" + pmake + "','assetmodel':'" + pmodel+ "','bcodes':'" + bcodes
                            + "','aremarks':'" + premarks + "','assetid':'" + document.getElementById('newAssetID').value + "'}",
                        async: false,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            if (isNumeric(data.d)) {
                                showAlert("Successfully added spare part to asset");
                                 addrowtoTableSpare();
                            }
                            else if (data.d == "LOGOUT") {
                                showError("Session has expired. Kindly login again.");
                                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                            }
                            else {
                                showError(data.d);
                            }
                        },
                        error: function () {
                            showError("Session timeout. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                    });
                }
            }
            catch (err) {
                alert(err)
            }
        }
        function saveDRemarks() {
            var projn = document.getElementById("aremarks").value;
            var id = document.getElementById('newAssetID').value;
            var isPass = true;
            if (isEmptyOrSpaces(document.getElementById('aremarks').value)) {
                isPass = false;
                showAlert("Kindly provide remarks to be added");
            }
            else {
                if (isSpecialChar(document.getElementById('aremarks').value)) {
                    isPass = false;
                    showAlert("Kindly remove special character from remarks");
                }
            }
            if (isPass) {
                $.ajax({
                    type: "POST",
                    url: "WarehouseP.aspx/saveRemarksWP",
                    data: "{'id':'" + id + "','notes':'" + projn + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d == "SUCCESS") {
                            showAlert("Successfully added remarks");
                            document.getElementById("aremarks").value = "";
                            getassetRemarks(id);
                        }
                        else if (data.d == "LOGOUT") {
                            document.getElementById('<%= logoutbtn.ClientID %>').click();
                        }
                        else {
                            showAlert('Failed to save notes. ' + data.d);
                        }
                    }
                });
            }
        }
        function getassetRemarks(id) {
            jQuery('#dassetRemarksList div').html('');
            $.ajax({
                type: "POST",
                url: "WarehouseP.aspx/getAssetRemarksData",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    for (var i = 0; i < data.d.length; i++) {
                        var div = document.createElement('div');

                        div.className = 'row activity-block-container';

                        div.innerHTML = data.d[i];

                        document.getElementById('dassetRemarksList').appendChild(div);
                    }
                }
            });
        }
                function getviewassetspareRemarks(id) {
            jQuery('#assetspareRemarksList div').html('');
            $.ajax({
                type: "POST",
                url: "WarehouseP.aspx/getAssetRemarksData",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    for (var i = 0; i < data.d.length; i++) {
                        var div = document.createElement('div');

                        div.className = 'row activity-block-container';

                        div.innerHTML = data.d[i];

                        document.getElementById('assetspareRemarksList').appendChild(div);
                    }
                }
            });
        }
        function getviewassetRemarks(id) {
            jQuery('#assetRemarksList div').html('');
            $.ajax({
                type: "POST",
                url: "WarehouseP.aspx/getAssetRemarksData",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    for (var i = 0; i < data.d.length; i++) {
                        var div = document.createElement('div');

                        div.className = 'row activity-block-container';

                        div.innerHTML = data.d[i];

                        document.getElementById('assetRemarksList').appendChild(div);
                    }
                }
            });
        }
        function schedRadioClick(e) {
            document.getElementById('plancalDIV').style.display = "block";
            document.getElementById('planrecDIV').style.display = "none";
        }
        function recRadioClick(e) {
            document.getElementById('plancalDIV').style.display = "none";
            document.getElementById('planrecDIV').style.display = "block";
        }

        function savePlanSchedule(id,planid,startT) {
            jQuery.ajax({
                type: "POST",
                url: "WarehouseP.aspx/savePlanSchedule",
                data: JSON.stringify({ assetid: id,planid: planid,sched: datedutylisttems,schedtime: startT, uname: loggedinUsername }),
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function loadnewpart() {
            document.getElementById("MainContent_assetsparechartSelect").value = document.getElementById("rowidChoice").value;
            jQuery('#MainContent_assetsparechartSelect').selectpicker('val', document.getElementById("rowidChoice").value);
        }
        function getassetplanscheduledates(id) {
            
            jQuery.ajax({
                type: "POST",
                url: "WarehouseP.aspx/getassetplanScheduleDates",
                data: "{'id':'"+id+"','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        if (data.d.length > 0) {
                            datedutylisttems = [];
                            for (var i = 0; i < data.d.length; i++) {
                                 
                                var gd = new Date(data.d[i]);
                                var dd = gd.getDate();
                                var mm = gd.getMonth() + 1;
                                var y = gd.getFullYear();

                                var someFormattedDate = mm + '/' + dd + '/' + y;
                                datedutylisttems.push(someFormattedDate);
                                try {
                                    if (document.getElementById((parseInt(mm)) + '_' + (parseInt(dd))))
                                        document.getElementById((parseInt(mm)) + '_' + dd).style.backgroundColor = "lightgray";
                                }
                                catch (erx) { }
                            }
                        }
                    }
                }
            });
        }
        function loadnewplan(id) { 
            document.getElementById("MainContent_assetplanchartSelect").value = id;
            jQuery('#MainContent_assetplanchartSelect').selectpicker('val', id);
            document.getElementById('MainContent_assetplanchartSelect').disabled = true;
            fromAddPlanToAsset = true;
        }
        function resetcalendar(id) {

            document.getElementById('dutycalendarRow1').innerHTML = "";
            document.getElementById('dutycalendarRow2').innerHTML = "";
            document.getElementById('dutycalendarRow3').innerHTML = "";
            document.getElementById('dutycalendarRow4').innerHTML = "";
            document.getElementById('dutycalendarRow5').innerHTML = "";

            jQuery.ajax({
                type: "POST",
                url: "WarehouseP.aspx/getCalendarDaysDuty",
                data: "{'id':'99','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        if (data.d.length > 0) {
                            document.getElementById('dutycalendarRow1').innerHTML = data.d[0];
                            document.getElementById('dutycalendarRow2').innerHTML = data.d[1];
                            document.getElementById('dutycalendarRow3').innerHTML = data.d[2];
                            document.getElementById('dutycalendarRow4').innerHTML = data.d[3];
                            document.getElementById('dutycalendarRow5').innerHTML = data.d[4];
                            document.getElementById('dutydateHeader').innerHTML = data.d[5];

                            getassetplanscheduledates(id);
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function loadtempplan(id) {
            document.getElementById('AssetPlanID').value = id; 
            document.getElementById("MainContent_plantempSelect").value = id; 
            document.getElementById('plantempassetdiv').style.display = "none";
            document.getElementById('saveAssetPlanTempLi').style.display = "none";
            document.getElementById('saveAssetPlanLi').style.display = "none"; 
            document.getElementById("updateAssetPlanLi").style.display = "block"; 
            fromAddPlanToAsset = true;
            jQuery.ajax({
                type: "POST",
                url: "WarehouseP.aspx/getTableRowDataPlan",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        //document.getElementById("viewplanType").innerHTML = data.d[0];
                        document.getElementById("tbNewTaskName").value = data.d[1];
                        document.getElementById("tbNewDescription").value = data.d[2];
                        document.getElementById("MainContent_taskTypeSelect").value = data.d[19];
                        jQuery('#MainContent_taskTypeSelect').selectpicker('val', data.d[19]);
                        //document.getElementById("viewplanTType").innerHTML = data.d[3];

                        if (data.d[4] == "Scheduled") {
                            schedRadio.checked = true;
                        }
                        else {
                            recRadio.checked = true;
                        }

                        document.getElementById("prioritySelect").value = data.d[9];

                        document.getElementById("taskSelectAssigneeType").value = data.d[10];


                        document.getElementById("MainContent_checklistSelect").value = data.d[17];
                        jQuery('#MainContent_checklistSelect').selectpicker('val', data.d[17]);

                        if (data.d[12] == "Requires Signature") {
                            requiresignatureCheck.checked = true;
                        }
                        else {
                            requiresignatureCheck.checked = false;
                        }
                        if (data.d[18] == "TRUE") {
                            myselfCheck.checked = true;
                        }
                        else {
                            myselfCheck.checked = false;
                            if (data.d[10] == "User") {
                                document.getElementById("MainContent_usersearchSelect").value = data.d[16];
                                jQuery('#MainContent_usersearchSelect').selectpicker('val', data.d[16]);
                            }
                            else {
                                document.getElementById("MainContent_groupsearchSelect").value = data.d[16];
                                jQuery('#MainContent_groupsearchSelect').selectpicker('val', data.d[16]);
                            }
                        }



                        var dt = data.d[5].split('/');
                        var month = dt[0];
                        if (dt[0].length < 2)
                            month = "0" + dt[0];
                        var daymonth = dt[1];
                        if (dt[1].length < 2)
                            daymonth = "0" + dt[1];
                        var dayyear = dt[2];


                        document.getElementById("dateDutyStart").value = month + "/" + daymonth + "/" + dayyear;


                        var dt = data.d[6].split('/');
                        var month = dt[0];
                        if (dt[0].length < 2)
                            month = "0" + dt[0];
                        var daymonth = dt[1];
                        if (dt[1].length < 2)
                            daymonth = "0" + dt[1];
                        var dayyear = dt[2];

                        var tt = data.d[7].split(':');
                        if (tt[0].length < 2) {
                            tt[0] = "0" + tt[0];
                        }
                        document.getElementById("dateDutyEnd").value = month + "/" + daymonth + "/" + dayyear;

                        document.getElementById("newtaskTime").value = tt[0];//+":"+tt[1];
                        document.getElementById("newtaskTimeMinute").value = tt[1];//+":"+tt[1];

                        resetcalendar(id);
                        
                        
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }

        function plantempSelectTypeOnChange(e) {
            if (e.value > 0) {
                jQuery.ajax({
                    type: "POST",
                    url: "WarehouseP.aspx/getTableRowDataPlan",
                    data: "{'id':'" + e.value + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d[0] == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        } else {
                            
                            //document.getElementById("viewplanType").innerHTML = data.d[0];
                            document.getElementById("tbNewTaskName").value = data.d[1];
                            document.getElementById("tbNewDescription").value = data.d[2];
                            document.getElementById("MainContent_taskTypeSelect").value = data.d[19];
                            jQuery('#MainContent_taskTypeSelect').selectpicker('val', data.d[19]);
                            //document.getElementById("viewplanTType").innerHTML = data.d[3];

                            if (data.d[4] == "Scheduled") {
                                schedRadio.checked = true;
                            }
                            else {
                                recRadio.checked = true;
                            }

                            document.getElementById("prioritySelect").value = data.d[9];

                            document.getElementById("taskSelectAssigneeType").value = data.d[10];


                            document.getElementById("MainContent_checklistSelect").value = data.d[17];
                            jQuery('#MainContent_checklistSelect').selectpicker('val', data.d[17]);

                            if (data.d[12] == "Requires Signature") {
                                requiresignatureCheck.checked = true;
                            }
                            else {
                                requiresignatureCheck.checked = false;
                            }
                            if (data.d[18] == "TRUE") {
                                myselfCheck.checked = true;
                            }
                            else {
                                myselfCheck.checked = false;
                                if (data.d[10] == "User") {
                                    document.getElementById("MainContent_usersearchSelect").value = data.d[16];
                                    jQuery('#MainContent_usersearchSelect').selectpicker('val', data.d[16]);
                                }
                                else {
                                    document.getElementById("MainContent_groupsearchSelect").value = data.d[16];
                                    jQuery('#MainContent_groupsearchSelect').selectpicker('val', data.d[16]);
                                }
                            }
                                                    var dt = data.d[5].split('/');
                        var month = dt[0];
                        if (dt[0].length < 2)
                            month = "0" + dt[0];
                        var daymonth = dt[1];
                        if (dt[1].length < 2)
                            daymonth = "0" + dt[1];
                        var dayyear = dt[2];

                  
                        document.getElementById("dateDutyStart").value = month + "/" + daymonth + "/" + dayyear;


                        var dt = data.d[6].split('/');
                        var month = dt[0];
                        if (dt[0].length < 2)
                            month = "0" + dt[0];
                        var daymonth = dt[1];
                        if (dt[1].length < 2)
                            daymonth = "0" + dt[1];
                        var dayyear = dt[2];

                        var tt = data.d[7].split(':');
                        if (tt[0].length < 2) {
                            tt[0] = "0" + tt[0];
                        }
                        document.getElementById("dateDutyEnd").value = month + "/" + daymonth + "/" + dayyear;

                        document.getElementById("newtaskTime").value = tt[0];//+":"+tt[1];
                            document.getElementById("newtaskTimeMinute").value = tt[1];//+":"+tt[1];

                            resetcalendar(e.value);
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            }
        }
        function plantypeSelectTypeOnChange(e) {

        }

        var fromAddPlanToAsset = false;
        function updateAssetPlan() {
            var plantype = document.getElementById("plantypeSelect").value;
            var taskname = document.getElementById("tbNewTaskName").value;
            var tasktype = document.getElementById("MainContent_taskTypeSelect").value;
            var taskdesc = document.getElementById("tbNewDescription").value;
            var isrec = document.getElementById("recRadio").checked;
            var sdate = document.getElementById("dateDutyStart").value;
            var edate = document.getElementById("dateDutyEnd").value;
            var startT = document.getElementById("newtaskTime").value + ":" + document.getElementById("newtaskTimeMinute").value;
            var checklistid = document.getElementById("MainContent_checklistSelect").value;
            var isErr = false;
            var assignType = document.getElementById("taskSelectAssigneeType").value;
            var assignId = "0";
            var assigneeName = "";
            if (assignType == "User") {
                assignId = document.getElementById("MainContent_usersearchSelect").value;
                assigneeName = getSelectedText('MainContent_usersearchSelect');
            }
            else if (assignType == "Group") {
                assignId = document.getElementById("MainContent_groupsearchSelect").value;
                assigneeName = getSelectedText('MainContent_groupsearchSelect');
            }

            if (assignId == "0" && document.getElementById('myselfCheck').checked == false) {
                isErr = true;
                showAlert("Kindly provide assignee for the plan");
            }
            var priority = document.getElementById("prioritySelect").value;
            var issig = document.getElementById("requiresignatureCheck").checked;
            var lbrecurring = document.getElementById("newrecurringSelect").value;

            var recurring = false;
            if (lbrecurring == "Recurring") {
                recurring = false;
            }
            else {
                recurring = true;
            }

            if (!isEmptyOrSpaces(taskname)) {
                if (isSpecialChar(taskname)) {
                    isErr = true;
                }
            }
            else {
                isErr = true;
                showAlert("Kindly provide plan name");
            }
            if (!isEmptyOrSpaces(taskdesc)) {
                if (isSpecialChar(taskdesc)) {
                    isErr = true;
                }
            }
            if (fromAddPlanToAsset)
                document.getElementById('newAssetID').value = document.getElementById("MainContent_assetplanchartSelect").value;

            if (!isErr) {
                $.ajax({
                    type: "POST",
                    url: "WarehouseP.aspx/insertUpdatePlan",
                    data: "{'name':'" + plantype
                        + "','tname':'" + taskname
                        + "','desc':'" + taskdesc
                        + "','assignType':'" + assignType
                        + "','assignId':'" + assignId
                        + "','assigneeName':'" + assigneeName
                        + "','startdate':'" + sdate
                        + "','enddate':'" + edate
                        + "','priority':'" + priority
                        + "','checklistid':'" + checklistid
                        + "','recurring':'" + recurring
                        + "','lbrecurring':'" + lbrecurring
                        + "','uname':'" + loggedinUsername
                        + "','tasktype':'" + tasktype
                        + "','startT':'" + startT
                        + "','issig':'" + issig
                        + "','planid':'" + document.getElementById('AssetPlanID').value
                        + "','assetid':'" + document.getElementById('newAssetID').value
                        + "','myself':'" + document.getElementById('myselfCheck').checked
                        + "'}",

                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (isNumeric(data.d)) {
                            showAlert("Plan has successfully been updated");
                            savePlanSchedule(data.d, document.getElementById('AssetPlanID').value, startT);
                            $('#newAssetModal').modal('hide');
                            addrowtoTempPlanItem();
                            addrowtoPlanItem();
                        }
                        else if (data.d[0] == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                        else {
                            hideLoader();
                            showAlert('Error 48: Error occured while assigning task-' + data.d[0]);
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            }
        }
        function saveAssetPlan(istemplate) {
            var plantype = document.getElementById("plantypeSelect").value;
            var taskname = document.getElementById("tbNewTaskName").value;
            var tasktype = document.getElementById("MainContent_taskTypeSelect").value;
            var taskdesc = document.getElementById("tbNewDescription").value;
            var isrec = document.getElementById("recRadio").checked;
            var sdate = document.getElementById("dateDutyStart").value;
            var edate = document.getElementById("dateDutyEnd").value;
            var startT = document.getElementById("newtaskTime").value + ":" + document.getElementById("newtaskTimeMinute").value;
            var checklistid = document.getElementById("MainContent_checklistSelect").value;
            var isErr = false;
            var assignType = document.getElementById("taskSelectAssigneeType").value;
            var assignId = "0";
            var assigneeName = "";
            if (assignType == "User") {
                assignId = document.getElementById("MainContent_usersearchSelect").value;
                assigneeName = getSelectedText('MainContent_usersearchSelect');
            }
            else if (assignType == "Group") {
                assignId = document.getElementById("MainContent_groupsearchSelect").value;
                assigneeName = getSelectedText('MainContent_groupsearchSelect');
            }

            if (assignId == "0" && document.getElementById('myselfCheck').checked == false) {
                isErr = true;
                showAlert("Kindly provide assignee for the plan");
            }
            var priority = document.getElementById("prioritySelect").value;
            var issig = document.getElementById("requiresignatureCheck").checked;
            var lbrecurring = document.getElementById("newrecurringSelect").value;

            var recurring = false;
            if (lbrecurring == "Recurring") {
                recurring = false;
            }
            else {
                recurring = true;
            }

            if (!isEmptyOrSpaces(taskname)) {
                if (isSpecialChar(taskname)) {
                    isErr = true;
                }
            }
            else {
                isErr = true;
                showAlert("Kindly provide plan name");
            }
            if (!isEmptyOrSpaces(taskdesc)) {
                if (isSpecialChar(taskdesc)) {
                    isErr = true;
                }
            }
            if (fromAddPlanToAsset)
                document.getElementById('newAssetID').value = document.getElementById("MainContent_assetplanchartSelect").value;

            if (!isErr) {
                $.ajax({
                    type: "POST",
                    url: "WarehouseP.aspx/insertSavePlan",
                    data: "{'name':'" + plantype
                        + "','tname':'" + taskname
                        + "','desc':'" + taskdesc
                        + "','assignType':'" + assignType
                        + "','assignId':'" + assignId
                        + "','assigneeName':'" + assigneeName
                        + "','startdate':'" + sdate
                        + "','enddate':'" + edate
                        + "','priority':'" + priority
                        + "','checklistid':'" + checklistid
                        + "','recurring':'" + recurring
                        + "','lbrecurring':'" + lbrecurring
                        + "','uname':'" + loggedinUsername
                        + "','tasktype':'" + tasktype
                        + "','startT':'" + startT
                        + "','issig':'" + issig
                        + "','ttemplate':'" + istemplate
                        + "','assetid':'" + document.getElementById('newAssetID').value
                        + "','myself':'" + document.getElementById('myselfCheck').checked
                        + "'}",

                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (isNumeric(data.d)) { 
                            document.getElementById('newPlanID').value = data.d;
                            if (document.getElementById('displayplanassetDIV').style.display == "block") {
                                showAlert("Plan has successfully been saved");
                                savePlanSchedule(document.getElementById('newAssetID').value, document.getElementById('newPlanID').value, startT);
                                $('#newAssetModal').modal('hide');
                                addrowtoPlanItem();
                                addrowtoTempPlanItem();
                            }
                            else {
                                showAlert("Plan has successfully been added");

                                rremarksClick();

                                document.getElementById('pplansClickLI').style.display = "none";

                                savePlanSchedule(document.getElementById('newAssetID').value, document.getElementById('newPlanID').value, startT);
                                document.getElementById("saveAssetSpareLi").style.display = "none";
                                document.getElementById("updateAssetItemLi").style.display = "none";  
                                document.getElementById("saveAssetItemLi").style.display = "none";
                                document.getElementById("saveAssetPlanLi").style.display = "none";
                                document.getElementById("saveAssetPlanTempLi").style.display = "none";
                                document.getElementById("saveAssetRemarksLi").style.display = "block";
                                           addrowtoTempPlanItem();
                                 addrowtoPlanItem();
                            }
                        }
                        else if (data.d[0] == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                        else {
                            hideLoader();
                            showAlert('Error 48: Error occured while assigning task-' + data.d[0]);
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            }
        }
        function updateAssetItem() {
    try {
        var aName = document.getElementById("tbAssetName").value;
        var aComment = document.getElementById("tbAssetComments").value;
        var aAsset = document.getElementById('categorySubAssetSelect').value;
        var snumber = document.getElementById('tbAssetSNumber').value;
        var barcodeN = document.getElementById('tbAssetBarcode').value;
        var asset = document.getElementById('MainContent_categoryAssetSelect').value;

        var aLoc = document.getElementById('categorySubMainLocation').value;

        var Loc = document.getElementById('MainContent_categoryAssetMainLocation').value;

        var imgPath = document.getElementById('imagePathAsset').text;

        var isGood = true;
        if (isEmptyOrSpaces(aName)) {
            isGood = false;
            showAlert("Please provide item name.")
        }
        else {
            if (isSpecialChar(aName)) {
                isGood = false;
            }

            if (isEmptyOrSpaces(snumber)) {

            }
            else if (isSpecialChar(snumber)) {
                isGood = false;
            }

            if (isEmptyOrSpaces(aComment)) {

            }
            else if (isSpecialChar(aComment)) {
                isGood = false;
            }
        }
        if (isGood) {
            jQuery.ajax({
                type: "POST",
                url: "WarehouseP.aspx/updateAssetItemData",
                data: "{'name':'" + aName + "','comment':'" + aComment + "','asset':'" + aAsset + "','location':'" + aLoc
                    + "','imgpath':'" + imgPath + "','masset':'" + asset + "','Loc':'" + Loc + "','barcode':'" + barcodeN + "','serialno':'" + snumber
                    + "','uname':'" + loggedinUsername + "','isKey':'" + isKey + "','accountid':'" + customerslst.value
                    + "','projectid':'" + projectlst.value + "','assetmake':'" + tbAssetMake.value + "','assetmodel':'" + tbAssetModel.value
                    + "','lvd':'" + lvdpicker.value + "','assetcontractor':'" + tbAssetContractor.value
                    + "','aremarks':'" + aremarks.value + "','assetid':'" + document.getElementById('rowidChoice').value  + "'}",

                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (isNumeric(data.d)) {
                        addrowtoTable1();
                                    $('#newAssetModal').modal('hide');
                        showAlert("Item has successfully been updated");
                    }
                    else if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                    else {
                        showError(data.d);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
    }
    catch (err) {
        alert(err)
    }
}

function saveAssetItem() {
    try {
        var aName = document.getElementById("tbAssetName").value;
        var aComment = document.getElementById("tbAssetComments").value;
        var aAsset = document.getElementById('categorySubAssetSelect').value;
        var snumber = document.getElementById('tbAssetSNumber').value;
        var barcodeN = document.getElementById('tbAssetBarcode').value;
        var asset = document.getElementById('MainContent_categoryAssetSelect').value;

        var aLoc = document.getElementById('categorySubMainLocation').value;

        var Loc = document.getElementById('MainContent_categoryAssetMainLocation').value;

        var imgPath = document.getElementById('imagePathAsset').text;

        var isGood = true;
        if (isEmptyOrSpaces(aName)) {
            isGood = false;
            showAlert("Please provide item name.")
        }
        else {
            if (isSpecialChar(aName)) {
                isGood = false;
            }

            if (isEmptyOrSpaces(snumber)) {

            }
            else if (isSpecialChar(snumber)) {
                isGood = false;
            }

            if (isEmptyOrSpaces(aComment)) {

            }
            else if (isSpecialChar(aComment)) {
                isGood = false;
            }
        }
        if (isGood) {
            jQuery.ajax({
                type: "POST",
                url: "WarehouseP.aspx/saveAssetItemData",
                data: "{'name':'" + aName + "','comment':'" + aComment + "','asset':'" + aAsset + "','location':'" + aLoc
                    + "','imgpath':'" + imgPath + "','masset':'" + asset + "','Loc':'" + Loc + "','barcode':'" + barcodeN + "','serialno':'" + snumber
                    + "','uname':'" + loggedinUsername + "','isKey':'" + isKey + "','accountid':'" + customerslst.value
                    + "','projectid':'" + projectlst.value + "','assetmake':'" + tbAssetMake.value + "','assetmodel':'" + tbAssetModel.value
                    + "','lvd':'" + lvdpicker.value + "','assetcontractor':'" + tbAssetContractor.value + "','aremarks':'" + aremarks.value + "'}",

                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (isNumeric(data.d)) {
                        addrowtoTable1();
                        pplansClick();
                        document.getElementById("saveAssetRemarksLi").style.display = "none";
                        document.getElementById("updateAssetItemLi").style.display = "none";  
                        document.getElementById('saveAssetItemLi').style.display = "none";
                        document.getElementById('saveAssetPlanLi').style.display = "block";
                         document.getElementById("saveAssetPlanTempLi").style.display = "block"; 
                         document.getElementById("saveAssetSpareLi").style.display = "none"; 
                        document.getElementById('rremarksClickLI').style.pointerEvents = "auto";
                        document.getElementById('ddetailsClickLI').style.pointerEvents = "auto";
                        document.getElementById('pplansClickLI').style.pointerEvents = "auto";
                        document.getElementById('ssparepartClickLI').style.pointerEvents = "auto";
                        document.getElementById('aattachClickLI').style.pointerEvents = "auto"; 

                        document.getElementById('newAssetID').value = data.d;
                        
                        showAlert("Item has successfully been added");
                    }
                    else if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                    else {
                        showError(data.d);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
    }
    catch (err) {
        alert(err)
    }
}
function removeFromList(name) {
    var elSel = document.getElementById('sendToListBox');
    var i;
    for (i = elSel.length - 1; i >= 0; i--) {
        if (elSel.options[i].text == name) {
            var oldid = document.getElementById("<%=tbUserID.ClientID%>").value;
                    var oldstring2 = document.getElementById("<%=tbUserName.ClientID%>").value;
                    document.getElementById("<%=tbUserID.ClientID%>").value = oldid.replace("-" + elSel.options[i].value, "");
                    document.getElementById("<%=tbUserName.ClientID%>").value = oldstring2.replace("-" + elSel.options[i].text, "");
                    elSel.remove(i);
                }
            }
        }
        function liOnclickRemove(name, id) {
            removeFromList(name);
            removenameFromDispatchList(name);
        }
        function removenameFromDispatchList(name) {
            var element = document.getElementById("li-" + name);
            element.parentNode.removeChild(element);
        }
        function addFoundItem(id, name) {
            if (id != "0") {
                var exists = jQuery("#sendToListBox option[value=" + id + "]").length > 0;
                if (exists == false) {
                    var myOption;
                    myOption = document.createElement("Option");
                    myOption.text = name; //Textbox's value
                    myOption.value = id; //Textbox's value
                    sendToListBox.add(myOption);
                    document.getElementById("<%=tbUserID.ClientID%>").value = document.getElementById("<%=tbUserID.ClientID%>").value + '-' + id;
                    document.getElementById("<%=tbUserName.ClientID%>").value = document.getElementById("<%=tbUserName.ClientID%>").value + '-' + name;
                    addnametoAlarmList(name, id);
                }
            }
        }
        function addnametoAlarmList(name, id) {
            var ul = document.getElementById("alarmToSendList");
            var li = document.createElement("li");
            li.setAttribute("id", "li-" + name);
            li.innerHTML = '<a href="#"  class="capitalize-text" onclick="liOnclickRemove(&apos;' + name + '&apos;,&apos;' + id + '&apos;)">' + name + '<i class="fa fa-close" onclick="liOnclickRemove(&apos;' + name + '&apos;,&apos;' + id + '&apos;)"></i></a>';
            ul.appendChild(li);
        }
        function saveFoundItem(func) {
            try {
                var isGrp = document.getElementById("grpCheck").checked;
                if (isGrp && func == 'normal') {
                    var selected = [];
                    var list = document.getElementById("sendToListBox");
                    if (list.length > 0) {
                        for (i = 0; i < list.length; i++) {
                            selected.push(list.options[i].value);
                        }
                        jQuery.ajax({
                            type: "POST",
                            url: "WarehouseP.aspx/saveMultipleItemsGroup",
                            data: JSON.stringify({ userIds: selected, uname: loggedinUsername }),
                            dataType: "json",
                            traditional: true,
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                if (data.d == "LOGOUT") {
                                    showError("Session has expired. Kindly login again.");
                                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                        else if (data.d == "SUCCESS") {
                            $('#newFoundItem').modal('hide');
                            document.getElementById('successMessage').innerHTML = "Itemgroup has successfully been created";
                            jQuery('#successfulModal').modal('show');
                        }
                        else {
                            showError(data.d);
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            }
            else {
                showAlert("No items to group");
            }
        }
        else {

            var founddate = document.getElementById("dateFoundCalendar").value;
            var foundT = document.getElementById("foundTime").value;
            var receivedate = document.getElementById("dateReceiveCalendar").value;
            var receiveT = document.getElementById("receiveTime").value;
            var itemBrnd = document.getElementById("tbItemBrand").value;
            var itemStatus = document.getElementById("itemStatusSelect").value;
            var recName = document.getElementById("tbreceiverName").value;
            var locFound = document.getElementById("MainContent_locationFoundSelect").value;
            var finderName = document.getElementById("tbfinderName").value;
            var roomNumber = document.getElementById("tbroomNumber").value;
            var itemColor = document.getElementById("MainContent_itemColourSelect").value;

            var itemStorage = document.getElementById("MainContent_locationStoreSelect").value;
            var itemSubStorage = document.getElementById("itemStorageSelect").value;

            var shelfLife = document.getElementById("shelfLifeSelect").value;

            var itemReference = document.getElementById("tbItemReference").value;
            var imgPath = document.getElementById("imagePath").text;
            var finderDepartment = document.getElementById("MainContent_finderDepartmentSelect").value;
            var itemStatus = document.getElementById("itemStatusSelect").value;
            var itemType = document.getElementById("MainContent_itemTypeSelect").value;
            var itemSubType = document.getElementById("itemSubSelect").value;
            var siteTransfer = document.getElementById('MainContent_transferSiteSelectFrom').value;
            var isTransfer = document.getElementById("transferFromCheck").checked;
            if (!isTransfer)
                siteTransfer = "0";
            var isGood = true;
            if (isEmptyOrSpaces(itemBrnd)) {
                isGood = false;
                showAlert("Please provide item brand.")
            }
            else if (isEmptyOrSpaces(recName)) {
                isGood = false;
                showAlert("Please provide receiver name.")
            }
            else if (isEmptyOrSpaces(finderName)) {
                isGood = false;
                showAlert("Please provide finder name.")
            }
            else if (isEmptyOrSpaces(roomNumber)) {
                isGood = false;
                showAlert("Please provide room number.")
            }
            else if (isEmptyOrSpaces(itemReference)) {
                isGood = false;
                showAlert("Please provide item reference.")
            }
            else {
                if (isSpecialChar(itemBrnd)) {
                    isGood = false;
                }
                else if (isSpecialChar(recName)) {
                    isGood = false;
                }
                else if (isSpecialChar(finderName)) {
                    isGood = false;
                }
                else if (isSpecialChar(roomNumber)) {
                    isGood = false;
                }

                if (itemReference.length != 10) {
                    isGood = false;
                    showAlert("Please item reference with 10 character inputs.")
                }
                else {
                    if (isSpecialChar(itemReference)) {
                        isGood = false;
                    }
                }
            }

            if (isGood) {
                jQuery.ajax({
                    type: "POST",
                    url: "WarehouseP.aspx/saveFoundTimeData",
                    data: "{'founddate':'" + founddate + "','receivedate':'" + receivedate + "','foundT':'" + foundT + "','receiveT':'" + receiveT
                        + "','itemBrnd':'" + itemBrnd + "','itemType':'" + itemType + "','recName':'" + recName + "','finderName':'" + finderName
                        + "','roomNumber':'" + roomNumber + "','itemColor':'" + itemColor + "','itemStorage':'" + itemStorage
                        + "','itemReference':'" + itemReference + "','imgPath':'" + imgPath + "','finderDepartment':'" + finderDepartment
                        + "','itemStatus':'" + itemStatus + "','locFound':'" + locFound + "','siteTransfer':'" + siteTransfer + "','itemSubStorage':'" + itemSubStorage + "','itemSubType':'" + itemSubType + "','shelfLife':'" + shelfLife + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d[0] == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                        else if (data.d[0] == "SUCCESS") {

                            if (isTransfer) {
                                jQuery.ajax({
                                    type: "POST",
                                    url: "WarehouseP.aspx/tranferImages",
                                    data: "{'id':'" + document.getElementById('tbfindRefernce').value + "','newid':'" + data.d[1] + "','uname':'" + loggedinUsername + "'}",
                                    async: false,
                                    dataType: "json",
                                    contentType: "application/json; charset=utf-8",
                                    success: function (data) {

                                    }
                                });
                            }


                            if (func == 'normal') {
                                $('#newFoundItem').modal('hide');
                                document.getElementById('successMessage').innerHTML = "Item has successfully been added";
                                jQuery('#successfulModal').modal('show');
                            }
                            else if (func == 'add') {
                                addFoundItem(data.d[1], [data.d[2]]);
                                document.getElementById('tbItemReference').value = '';
                                document.getElementById('barcode').src = '';
                                document.getElementById("tbItemBrand").value = "";
                                document.getElementById("tbItemReference").value = "";
                                document.getElementById("imagePath").text = "";
                                document.getElementById("resultsHeader").style.display = "none";
                                document.getElementById('results').innerHTML = "";
                            }

                        }
                        else {
                            showError(data.d[0]);
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            }
        }
    }
    catch (err) {
        alert(err)
    }
}

function activaTab(tab) {
    var el = document.getElementById('video-0-tab');
    if (el) {
        el.className = 'tab-pane fade ';
    }
    var el3 = document.getElementById('location-tab');
    if (el3) {
        el3.className = 'tab-pane fade';
    }
    var el4 = document.getElementById('image-1-tab');
    if (el4) {
        el4.className = 'tab-pane fade';
    }
    var el5 = document.getElementById('image-2-tab');
    if (el5) {
        el5.className = 'tab-pane fade';
    }
    var el6 = document.getElementById('image-3-tab');
    if (el6) {
        el6.className = 'tab-pane fade';
    }
    var el7 = document.getElementById('image-4-tab');
    if (el7) {
        el7.className = 'tab-pane fade';
    }
    var el8 = document.getElementById('image-5-tab');
    if (el8) {
        el8.className = 'tab-pane fade';
    }
    var el9 = document.getElementById('image-6-tab');
    if (el9) {
        el9.className = 'tab-pane fade';
    }
    var el10 = document.getElementById('image-0-tab');
    if (el10) {
        el10.className = 'tab-pane fade';
    }
    var ell = document.getElementById(tab);
    if (ell) {
        ell.className = 'tab-pane fade active in';
    }
}
var attachmentIndex = 0;
function onclickAttachmentForward() {
    attachmentIndex++;
    if (attachmentIndex < 8) {
        activaTab('image-' + attachmentIndex + '-tab');
    }
    else if (attachmentIndex == 8) {
        activaTab('video-0-tab');
    }
    else if (attachmentIndex == 9) {
        attachmentIndex = 0;
        activaTab('location-tab');
    }
}
function onclickAttachmentBack() {
    attachmentIndex--;
    if (attachmentIndex == 0) {
        attachmentIndex = 9;
    }
    else if (attachmentIndex < 0) {
        attachmentIndex = 8;
    }
    if (attachmentIndex < 8) {
        activaTab('image-' + attachmentIndex + '-tab');
    }
    else if (attachmentIndex == 8) {
        activaTab('video-0-tab');
    }
    else if (attachmentIndex == 9) {
        attachmentIndex = 0;
        activaTab('location-tab');
    }
}
function editOffenceTypeSave() {
    if (!isEmptyOrSpaces(document.getElementById('tbEditOffenceName').value)) {
        if (!isSpecialChar(document.getElementById('tbEditOffenceName').value)) {
            if (document.getElementById('MainContent_typeEditSelect').value > 0) {
                jQuery.ajax({
                    type: "POST",
                    url: "WarehouseP.aspx/editAssetSub",
                    data: "{'id':'" + document.getElementById('tbOffenceIdChoice').value + "','name':'" + document.getElementById('tbEditOffenceName').value + "','type':'" + document.getElementById('MainContent_typeEditSelect').value + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                        else if (data.d == "SUCCESS") {
                            jQuery('#editOffenceModal').modal('hide');
                            document.getElementById('successincidentScenario').innerHTML = "SubCategory successfully edited!";
                            jQuery('#successfulDispatch').modal('show');
                        }
                        else {
                            showError(data.d);
                        }
                    }
                });
            }
            else {
                showAlert("Kindly select category");
            }
        }
    }
    else {
        showAlert("Kindly provide subcategory name");
    }
}
function editOffenceCatTypeSave() {
    if (!isEmptyOrSpaces(document.getElementById('tbEditCategoryName').value)) {
        if (!isSpecialChar(document.getElementById('tbEditCategoryName').value)) {
            jQuery.ajax({
                type: "POST",
                url: "WarehouseP.aspx/editAssetCat",
                data: "{'id':'" + document.getElementById('tbOffenceCatChoice').value + "','name':'" + document.getElementById('tbEditCategoryName').value + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "SUCCESS") {
                        jQuery('#EditOffenceCategoryModal').modal('hide');
                        document.getElementById('successincidentScenario').innerHTML = "Category successfully edited!";
                        jQuery('#successfulDispatch').modal('show');
                    }
                    else if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                    else {
                        showError(data.d);
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
    }
    else {
        showAlert("Kindly provide category name");
    }
}
        function addOffenceTypeSave() {
            if (!isEmptyOrSpaces(document.getElementById('tbName').value)) {
                if (!isSpecialChar(document.getElementById('tbName').value)) {
                    if (document.getElementById('MainContent_typeSelect').value > 0) {
                        jQuery.ajax({
                            type: "POST",
                            url: "WarehouseP.aspx/addAssetSubCategory",
                            data: "{'id':'0','name':'" + document.getElementById('tbName').value + "','type':'" + document.getElementById('MainContent_typeSelect').value + "','uname':'" + loggedinUsername + "'}",
                            async: false,
                            dataType: "json",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                if (data.d == "SUCCESS") {
                                    jQuery('#newOffenceModal').modal('hide');
                                    document.getElementById('successincidentScenario').innerHTML = "SubCategory successfully added!";
                                    jQuery('#successfulDispatch').modal('show');
                                }
                                else if (data.d == "LOGOUT") {
                                    showError("Session has expired. Kindly login again.");
                                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                                }
                                else {
                                    showError(data.d);
                                }
                            },
                            error: function () {
                                showError("Session timeout. Kindly login again.");
                                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                            }
                        });
                    }
                    else {
                        showAlert("Kindly select category");
                    }
                }
            }
            else {
                showAlert("Kindly provide subcategory name");
            }
        }
        function addOffenceCatTypeSave() {
            if (!isEmptyOrSpaces(document.getElementById('tbCategoryName').value)) {
                if (!isSpecialChar(document.getElementById('tbCategoryName').value)) {
                    jQuery.ajax({
                        type: "POST",
                        url: "WarehouseP.aspx/addAssetCat",
                        data: "{'id':'0','name':'" + document.getElementById('tbCategoryName').value + "','uname':'" + loggedinUsername + "'}",
                        async: false,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            if (data.d == "SUCCESS") {
                                jQuery('#newOffenceCategoryModal').modal('hide');
                                document.getElementById('successincidentScenario').innerHTML = "Category successfully added!";
                                jQuery('#successfulDispatch').modal('show');
                            }
                            else if (data.d == "LOGOUT") {
                                showError("Session has expired. Kindly login again.");
                                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                            }
                            else {
                                showError(data.d);
                            }
                        },
                        error: function () {
                            showError("Session timeout. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                    });
                }
            }
            else {
                showAlert("Kindly provide category name");
            }
        }
        function addVehColorSave() {
            if (!isEmptyOrSpaces(document.getElementById('tbVehicleColorName').value)) {
                if (!isSpecialChar(document.getElementById('tbVehicleColorName').value)) {
                    jQuery.ajax({
                        type: "POST",
                        url: "WarehouseP.aspx/addVehColor",
                        data: "{'id':'0','name':'" + document.getElementById('tbVehicleColorName').value + "','nameAR':'" + document.getElementById('tbArVehicleColorName').value + "','uname':'" + loggedinUsername + "'}",
                        async: false,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            if (data.d == "SUCCESS") {
                                jQuery('#newVehicleColorModal').modal('hide');
                                document.getElementById('successincidentScenario').innerHTML = "Color successfully added!";
                                jQuery('#successfulDispatch').modal('show');
                            }
                            else if (data.d == "LOGOUT") {
                                showError("Session has expired. Kindly login again.");
                                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                        else {
                            showError(data.d);
                        }
                    },
                    error: function () {
                        showError("Session timeout. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                });
            }
        }
        else {
            showAlert("Kindly provide color name");
        }
    }
    function addFinderDepartmentSave() {
        if (!isEmptyOrSpaces(document.getElementById('tbNewFinderDepartmentName').value)) {
            if (!isSpecialChar(document.getElementById('tbNewFinderDepartmentName').value)) {
                jQuery.ajax({
                    type: "POST",
                    url: "WarehouseP.aspx/addFinderDep",
                    data: "{'id':'0','name':'" + document.getElementById('tbNewFinderDepartmentName').value + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d == "SUCCESS") {
                            jQuery('#newFinderDeprModal').modal('hide');
                            document.getElementById('successincidentScenario').innerHTML = "Finder Department successfully added!";
                            jQuery('#successfulDispatch').modal('show');
                        }
                        else if (data.d == "LOGOUT") {
                            showError("Session has expired. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                            }
                            else {
                                showError(data.d);
                            }
                        },
                        error: function () {
                            showError("Session timeout. Kindly login again.");
                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                        }
                    });
                }
            }
            else {
                showAlert("Kindly provide finder department name");
            }
        }
        function oldDivContainers() {
            try {
                for (var i = 0; i < divArray.length; i++) {
                    if (typeof divArray[i] === "undefined") {
                    }
                    else {
                        var el = document.getElementById(divArray[i]);
                        el.parentNode.removeChild(el);
                    }
                }
                divArray = new Array();
            }
            catch (ex) {
                // alert(ex);
            }
        }
        function infotabDefault() {
            var el3 = document.getElementById('info-tab');
            if (el3) {
                el3.className = 'tab-pane fade active in';
            }
            var el4 = document.getElementById('attachments-tab');
            if (el4) {
                el4.className = 'tab-pane fade';
            }

            var el4 = document.getElementById('viewplans-tab');
            if (el4) {
                el4.className = 'tab-pane fade';
            }
             
            var el4 = document.getElementById('remarks-tab');
            if (el4) {
                el4.className = 'tab-pane fade';
            }

            var el2 = document.getElementById('liInfo');
            if (el2) {
                el2.className = 'active';
            }
            var el6 = document.getElementById('liAtta');
            if (el6) {
                el6.className = ' ';
            }

            var el61 = document.getElementById('liRem');
            if (el61) {
                el61.className = ' ';
            }
             

            var el63 = document.getElementById('liPlan');
            if (el63) {
                el63.className = ' ';
            } 
        }
        function finfotabDefault() {
            var el3 = document.getElementById('finfo-tab');
            if (el3) {
                el3.className = 'tab-pane fade active in';
            }
            var el44 = document.getElementById('freturn-tab');
            if (el44) {
                el44.className = 'tab-pane fade';
            }
            var el444 = document.getElementById('fdispose-tab');
            if (el444) {
                el444.className = 'tab-pane fade';
            }
            var el4 = document.getElementById('fattachments-tab');
            if (el4) {
                el4.className = 'tab-pane fade';
            }
            var el2 = document.getElementById('fliInfo');
            if (el2) {
                el2.className = 'active';
            }
            var el6 = document.getElementById('fliAtta');
            if (el6) {
                el6.className = ' ';
            }
            var el7 = document.getElementById('fliDispose');
            if (el7) {
                el7.className = ' ';
            }
            var el8 = document.getElementById('fliReturn');
            if (el8) {
                el8.className = ' ';
            }
        } 
        function rowchoiceVehicleMake(id, name, nameAR) {
            document.getElementById('tbVehicleMakeChoice').value = id;
            document.getElementById('tbEditVehicleMakeName').value = name;
            document.getElementById('tbEditArVehicleMakeName').value = nameAR;
        }
        function rowchoiceVehicleColor(id, name, nameAR) {
            document.getElementById('tbVehicleColorChoice').value = id;
            document.getElementById('tbEditVehicleColorName').value = name;
            document.getElementById('tbEditArVehicleColorName').value = nameAR;
        }
        function rowchoiceFinderDep(id, name) {
            document.getElementById('tbFinderDepChoice').value = id;
            document.getElementById('tbEditFinderDepartmentName').value = name;
        }
        function rowchoicePlateCode(id, name, nameAR, typeId) {
            document.getElementById('tbPlateCodeChoice').value = id;
            document.getElementById('tbEditPlateCodeName').value = name;
            document.getElementById('tbEditArPlateCodeName').value = nameAR;
            document.getElementById('MainContent_typeEditSelectPlateSource').value = typeId;
        }
        function rowchoicePlateSource(id, name, nameAR) {
            document.getElementById('tbPlateSourceChoice').value = id;
            document.getElementById('tbEditPlateSourceName').value = name;
            document.getElementById('tbEditArPlateSourceName').value = nameAR;
        }
        var grpClicked = false;
        function grpCheckClick() {
            if (!grpClicked) {
                document.getElementById('addLi').style.display = "block";
                grpClicked = true;
            }
            else {
                grpClicked = false;
                document.getElementById('addLi').style.display = "none";
            }

        }
        function getAssetPlanlistItems(id) {
            document.getElementById("assetplansList").innerHTML = "";
            $.ajax({
                type: "POST",
                url: "WarehouseP.aspx/getAssetPlansList",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        if (data.d.length > 0) {
                            document.getElementById("viewplansDIV").style.display = "block";
                            for (var i = 0; i < data.d.length; i++) {
                                var res = data.d[i].split("|");
                                var ul = document.getElementById("assetplansList");
                                var li = document.createElement("li");

                                li.innerHTML = '<i class="fa fa-object-group"></i><a onclick="displayBackPlan();rowchoicePlan2(' + res[0] + ')" style="margin-left:5px;" href="#"  class="capitalize-text" >' + res[1] + '</a>';

                                ul.appendChild(li);
                            }

                        }
                        else {
                            document.getElementById("viewplansDIV").style.display = "none";
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function rowchoicePlan2(name) {
            document.getElementById('AssetPlanID').value = name;
            planViewTables2(name);
            getAssetScheduleItems2(name);
            jQuery.ajax({
                type: "POST",
                url: "WarehouseP.aspx/getTableRowDataPlan",
                data: "{'id':'" + name + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else { 
                        document.getElementById("viewplanType2").innerHTML = data.d[0];
                        document.getElementById("viewplanName2").innerHTML = data.d[1];
                        document.getElementById("viewplanDesc2").innerHTML = data.d[2];
                        document.getElementById("viewplanTType2").innerHTML = data.d[3];
                        document.getElementById("viewplanTiType2").innerHTML = data.d[4];
                        document.getElementById("viewplanSDate2").innerHTML = data.d[5];
                        document.getElementById("viewplanEDate2").innerHTML = data.d[6];
                        document.getElementById("viewplanPTime2").innerHTML = data.d[7];
                        document.getElementById("viewplanCHK2").innerHTML = data.d[8];
                        document.getElementById("viewplanPrio2").innerHTML = data.d[9];
                        document.getElementById("viewplanAType2").innerHTML = data.d[10];
                        document.getElementById("viewplanAssignee2").innerHTML = data.d[11];
                        document.getElementById("viewplanSig2").innerHTML = data.d[12]; 
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function divassetplanbacktoMap() {
            document.getElementById("divAssetSpareDetails").style.display = "none";
            document.getElementById("divAssetPlanDetails").style.display = "none";
            document.getElementById("divAttachmentHolder").style.display = "block";      
        }
        function displayBackSpare() {
            document.getElementById("divAssetPlanDetails").style.display = "none";
            document.getElementById("divAttachmentHolder").style.display = "none";   
            document.getElementById("divAssetSpareDetails").style.display = "block";
        }
        function displayBackPlan() {
            document.getElementById("divAssetPlanDetails").style.display = "block";
            document.getElementById("divAttachmentHolder").style.display = "none";   
            document.getElementById("divAssetSpareDetails").style.display = "none";
        }
        function hideBackPlan() {
            //document.getElementById("liplanBack").style.display = "none";
        }
        function getSparePartlistItems(id) {
            document.getElementById("sparepartsList").innerHTML = "";
            $.ajax({
                type: "POST",
                url: "WarehouseP.aspx/getSparePartsList",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        if (data.d.length > 0) {
                            document.getElementById("viewpartsDIV").style.display = "block";
                            for (var i = 0; i < data.d.length; i++) {
                                var res = data.d[i].split("|");
                                var ul = document.getElementById("sparepartsList");
                                var li = document.createElement("li");

                                li.innerHTML = '<i class="fa fa-cog"></i><a onclick="rowchoice2(' + res[0] + ')" style="margin-left:5px;" href="#"  class="capitalize-text" >' + res[1] + '</a>';

                                ul.appendChild(li);
                            }
                        }
                        else {
                            document.getElementById("viewpartsDIV").style.display = "none";
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });

        }
        function showAssetBack() { 
            document.getElementById("liPlan").style.display = "block";
            document.getElementById("liaddPlan").style.display = "block"; 
             document.getElementById("liaddPart").style.display = "block"; 
            
        }
        function hideAssetBack() {
            document.getElementById("liPlan").style.display = "none";
            document.getElementById("liaddPlan").style.display = "none"; 
                 document.getElementById("liaddPart").style.display = "none"; 
        }
                function showTaskDocument2(id) {
            document.getElementById('rowChoiceTasks').value = id;
            document.getElementById("taskinitialOptionsDiv").style.display = "block";
            document.getElementById("taskhandleOptionsDiv").style.display = "none";
                    document.getElementById("taskrejectOptionsDiv").style.display = "none";
            document.getElementById("liapVCardX").style.display = "none";
                    document.getElementById("liapVCard").style.display = "none";
                         document.getElementById("liaVCardX").style.display = "block";
                    document.getElementById("liaVCard").style.display = "block";      

            var el = document.getElementById('tasklocation-tab');
            if (el) {
                el.className = 'tab-pane fade active in';
            }
            var el2 = document.getElementById('taskrejection-tab');
            if (el2) {
                el2.className = 'tab-pane fade';
            }

            oldDivContainers();

            var retVal = assignrowDataTask(id);
            if (retVal == "Completed")
                TaskIsCompleted();

            taskHistoryData(id);
            taskinsertAttachmentIcons(id);
            taskinsertAttachmentTabData(id);
            taskinsertAttachmentData(id);
            taskgetChecklistItems(id);
            getChecklistItemsNotes(id);
            getCanvasNotes(id);
            taskinfotabDefault();
            gettaskRemarks(id);
            hideAllRemarks();
        }
        function showTaskDocument(id) {
            document.getElementById('rowChoiceTasks').value = id;
            document.getElementById("taskinitialOptionsDiv").style.display = "block";
            document.getElementById("taskhandleOptionsDiv").style.display = "none";
            document.getElementById("taskrejectOptionsDiv").style.display = "none";
                document.getElementById("liapVCardX").style.display = "block";
                    document.getElementById("liapVCard").style.display = "block";
                         document.getElementById("liaVCardX").style.display = "none";
                    document.getElementById("liaVCard").style.display = "none";   
            var el = document.getElementById('tasklocation-tab');
            if (el) {
                el.className = 'tab-pane fade active in';
            }
            var el2 = document.getElementById('taskrejection-tab');
            if (el2) {
                el2.className = 'tab-pane fade';
            }

            oldDivContainers();

            var retVal = assignrowDataTask(id);
            if (retVal == "Completed")
                TaskIsCompleted();

            taskHistoryData(id);
            taskinsertAttachmentIcons(id);
            taskinsertAttachmentTabData(id);
            taskinsertAttachmentData(id);
            taskgetChecklistItems(id);
            getChecklistItemsNotes(id);
            getCanvasNotes(id);
            taskinfotabDefault();
            gettaskRemarks(id);
            hideAllRemarks();
        }
        function gettaskRemarks(id) {
            jQuery('#taskRemarksList div').html('');
            $.ajax({
                type: "POST",
                url: "WarehouseP.aspx/getTaskRemarksData",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    for (var i = 0; i < data.d.length; i++) {
                        var div = document.createElement('div');

                        div.className = 'row activity-block-container';

                        div.innerHTML = data.d[i];

                        document.getElementById('taskRemarksList').appendChild(div);
                    }
                }
            });
        }
        function hideAllRemarks() {
            var el2 = document.getElementById('tasklocation-tab');
            if (el2) {
                el2.className = 'tab-pane fade active in';
            }
            var el = document.getElementById('tremarks-tab');
            if (el) {
                el.className = 'tab-pane fade ';
            }
            document.getElementById("taskrotationDIV1").style.display = "block";
            document.getElementById("taskrotationDIV2").style.display = "block";
        }
        function showAllRemarks(id) {
            document.getElementById("taskrotationDIV1").style.display = "none";
            document.getElementById("taskrotationDIV2").style.display = "none";
            if (document.getElementById('taskrejection-tab').className == "tab-pane fade active in") {

            }
            else {
                var el2 = document.getElementById('tasklocation-tab');
                if (el2) {
                    el2.className = 'tab-pane fade';
                }
                var el = document.getElementById('tremarks-tab');
                if (el) {
                    el.className = 'tab-pane fade active in';
                }

                for (var i = 0; i < divArray.length; i++) {
                    var el2 = document.getElementById(divArray[i]);
                    el2.className = 'tab-pane fade';
                }

                jQuery('#taskRemarksList2 div').html('');
                jQuery.ajax({
                    type: "POST",
                    url: "WarehouseP.aspx/getTaskRemarksData2",
                    data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        for (var i = 0; i < data.d.length; i++) {
                            var div = document.createElement('div');

                            div.className = 'row activity-block-container';

                            div.innerHTML = data.d[i];

                            document.getElementById('taskRemarksList2').appendChild(div);
                        }
                    }
                });
            }
        }
        function getCanvasNotes(id) {
            document.getElementById("canvasItemsListNotes").innerHTML = "";
            $.ajax({
                type: "POST",
                url: "WarehouseP.aspx/getCanvasNotesData",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        if (data.d.length > 0) {
                            document.getElementById("pCanvasNotes").style.display = "block";
                            for (var i = 0; i < data.d.length; i++) {
                                var res = data.d[i].split("|");
                                var ul = document.getElementById("canvasItemsListNotes");
                                var li = document.createElement("li");
                                li.innerHTML = '<i style="margin-left:-15px;" ></i><a style="margin-left:5px;" href="#"  class="capitalize-text" >' + res[0] + '</a>';
                                ul.appendChild(li);
                                if (res.length > 1) {
                                    if (res[1] != '') {
                                        var li2 = document.createElement("li");
                                        li2.innerHTML = '<i style="margin-left:-15px;" class="fa fa-comments-o"></i><a style="margin-left:5px;" href="#"  class="capitalize-text" >' + res[1] + '</a>';
                                        ul.appendChild(li2);
                                    }
                                }
                            }
                        }
                        else {
                            document.getElementById("pCanvasNotes").style.display = "none";
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });

        }
        function getChecklistItemsNotes(id) {
            document.getElementById("checklistItemsListNotes").innerHTML = "";
            $.ajax({
                type: "POST",
                url: "WarehouseP.aspx/getChecklistNotesData",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        if (data.d.length > 0) {
                            document.getElementById("pchecklistItemsListNotes").style.display = "block";
                            for (var i = 0; i < data.d.length; i++) {
                                var res = data.d[i].split("|");
                                var ul = document.getElementById("checklistItemsListNotes");
                                var li = document.createElement("li");
                                li.innerHTML = '<i style="margin-left:-15px;" class="fa fa-square-o"></i><a style="margin-left:5px;" href="#"  class="capitalize-text" >' + res[0] + '</a>';
                                ul.appendChild(li);
                                if (res.length > 1) {
                                    if (res[1] != '') {
                                        var li2 = document.createElement("li");
                                        li2.innerHTML = '<i style="margin-left:-15px;" class="fa fa-comments-o"></i><a style="margin-left:5px;" href="#"  class="capitalize-text" >' + res[1] + '</a>';
                                        ul.appendChild(li2);
                                    }
                                }
                            }
                        }
                        else {
                            document.getElementById("pchecklistItemsListNotes").style.display = "none";
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });

        }
        function taskgetChecklistItems(id) {
            document.getElementById("taskchecklistItemsList").innerHTML = "";
            $.ajax({
                type: "POST",
                url: "WarehouseP.aspx/taskgetChecklistData",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    }
                    else {
                        if (data.d[0] == "False") {
                            var el = document.getElementById('checklistnamespanFA');
                            if (el) {
                                el.className = "fa fa-square-o";
                            }
                        }
                        else {
                            var el = document.getElementById('checklistnamespanFA');
                            if (el) {
                                el.className = "fa fa-check-square-o";
                            }
                        }
                        for (var i = 1; i < data.d.length; i++) {
                            var res = data.d[i].split("|");
                            var ul = document.getElementById("taskchecklistItemsList");
                            var li = document.createElement("li");
                            var marginLeft = '';
                            if (res[2] == 'True') {
                                marginLeft = 'style = "margin-left:-15px;"';
                            }

                            if (res[2] == '3') {

                                if (res[1] == "Checked")
                                    li.innerHTML = '<i ' + marginLeft + ' class="fa fa-check-square-o"></i><a style="margin-left:5px;cursor:default;" href="#"  class="capitalize-text" >' + res[0] + '</a>' + res[4];
                                else
                                    li.innerHTML = '<i ' + marginLeft + ' class="fa fa-square-o"></i><a style="margin-left:5px;cursor:default;" href="#"  class="capitalize-text" >' + res[0] + '</a>' + res[4];

                                ul.appendChild(li);

                                var li2 = document.createElement("li");
                                li2.innerHTML = '<a style="margin-left:5px;" href="#"  class="capitalize-text" >Notes: ' + res[3] + '</a>';
                                ul.appendChild(li2);
                            }
                            else {
                                if (res[1] == "Checked")
                                    li.innerHTML = '<i ' + marginLeft + ' class="fa fa-check-square-o"></i><a style="margin-left:5px;cursor:default;" href="#"  class="capitalize-text" >' + res[0] + '</a>' + res[3];
                                else
                                    li.innerHTML = '<i ' + marginLeft + ' class="fa fa-square-o"></i><a style="margin-left:5px;cursor:default;" href="#"  class="capitalize-text" >' + res[0] + '</a>' + res[3];

                                ul.appendChild(li);
                            }
                            //li.appendChild(document.createTextNode(data.d[i]));

                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });

        }
        function taskinsertAttachmentData(id) {
            document.getElementById('taskrotationDIV1').style.display = "none";
            document.getElementById('taskrotationDIV2').style.display = "none";
            $.ajax({
                type: "POST",
                url: "WarehouseP.aspx/taskgetAttachmentData",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        imgcount = 0;
                        document.getElementById('taskAudioDIV').style.display = "none";
                        for (var i = 0; i < data.d.length; i++) {
                            if (data.d[i].indexOf("video") >= 0) {
                                var div = document.createElement('div');
                                div.className = 'tab-pane fade';
                                div.innerHTML = data.d[i];
                                div.id = 'video-' + (i + 1) + '-tab';
                                document.getElementById('taskdivAttachmentHolder').appendChild(div);
                                divArray[i] = 'video-' + (i + 1) + '-tab';
                                imgcount++;
                            }
                            else {
                                var div = document.createElement('div');
                                div.className = 'tab-pane fade';
                                div.align = 'center';
                                div.style.height = '380px';
                                div.innerHTML = data.d[i];
                                div.id = 'image-' + (i + 1) + '-tab';
                                document.getElementById('taskdivAttachmentHolder').appendChild(div);
                                divArray[i] = 'image-' + (i + 1) + '-tab';
                                imgcount++;
                            }
                        }

                        if (imgcount > 0) {
                            document.getElementById('taskrotationDIV1').style.display = "block";
                            document.getElementById('taskrotationDIV2').style.display = "block";
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }

        function taskinsertAttachmentTabData(id) {
            jQuery('#taskattachments-info-tab div').html('');

            jQuery.ajax({
                type: "POST",
                url: "WarehouseP.aspx/taskgetAttachmentDataTab",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        document.getElementById("taskattachments-info-tab").innerHTML = data.d;
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function taskinsertAttachmentIcons(id) {
            jQuery('#taskdivAttachment div').html('');
            $.ajax({
                type: "POST",
                url: "WarehouseP.aspx/taskgetAttachmentDataIcons",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else
                        document.getElementById("taskdivAttachment").innerHTML = data.d;
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function taskHistoryData(id) {
            jQuery('#taskdivIncidentHistoryActivity div').html('');
            $.ajax({
                type: "POST",
                url: "WarehouseP.aspx/getTaskHistoryData",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        for (var i = 0; i < data.d.length; i++) {
                            var div = document.createElement('div');

                            div.className = 'row activity-block-container';

                            div.innerHTML = data.d[i];

                            document.getElementById('taskdivIncidentHistoryActivity').appendChild(div);
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function TaskIsCompleted() {
            document.getElementById("taskinitialOptionsDiv").style.display = "block";
            document.getElementById("taskhandleOptionsDiv").style.display = "none";
            document.getElementById("taskrejectOptionsDiv").style.display = "none";
        }
            function assignrowDataTask(id) {
        var output = "";
        $.ajax({
            type: "POST",
            url: "WarehouseP.aspx/getTableRowDataTask",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    document.getElementById("taskusernameSpan").innerHTML = data.d[0];
                    document.getElementById("tasktimeSpan").innerHTML = data.d[1];
                    document.getElementById("tasktypeSpan").innerHTML = data.d[2];
                    document.getElementById("taskstatusSpan").innerHTML = data.d[3];
                    document.getElementById("tasklocSpan").innerHTML = data.d[4];
                    document.getElementById("taskdescriptionSpan").innerHTML = data.d[8];
                    document.getElementById("taskinstructionSpan").innerHTML = data.d[9];
                    document.getElementById("taskincidentNameHeader").innerHTML = data.d[10];
                    document.getElementById("assignedTimeSpan").innerHTML = data.d[11];
                    document.getElementById("checklistNotesSpan").innerHTML = data.d[12];
                    document.getElementById("checklistnameSpan").innerHTML = data.d[14];

                    var el = document.getElementById('headerImageClass');
                    if (el) {
                        el.className = data.d[13];
                    }
                    output = data.d[3];

                    document.getElementById('ttypeSpan').innerHTML = data.d[15];

                    document.getElementById("incidentItemsList").innerHTML = "";
                    //var res = data.d[16].split("|");
                    //if (res.length > 0) {
                    //    document.getElementById("incidentItemsList").innerHTML = 'Ticket: <a style="color:#b2163b;" href="#ticketingViewCard"  data-toggle="modal" data-dismiss="modal"  class="capitalize-text" onclick="rowchoice(&apos;' + res[1] + '&apos;)">' + res[0] + '</a>';
                    //}

                    document.getElementById("CustomerNameSpan").innerHTML = "";
                    document.getElementById("ProjectNameSpan").innerHTML = "";
                    document.getElementById("ContractNameSpan").innerHTML = "";

                    document.getElementById('dvCustomerNameSpan').style.display = "none";
                    document.getElementById('dvProjectNameSpan').style.display = "none";
                    document.getElementById('dvContractNameSpan').style.display = "none";

                    //NEWCUSTOMER

                    if (data.d[17] != "N/A") {
                        document.getElementById("CustomerNameSpan").innerHTML = data.d[17];
                        document.getElementById('dvCustomerNameSpan').style.display = "block";
                    }
                    if (data.d[18] != "N/A") {
                        document.getElementById("ProjectNameSpan").innerHTML = data.d[18];
                        document.getElementById('dvProjectNameSpan').style.display = "block";
                    }
                    if (data.d[19] != "N/A") {
                        document.getElementById("ContractNameSpan").innerHTML = data.d[19];
                        document.getElementById('dvContractNameSpan').style.display = "block";
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
        return output;
    }
    function taskinfotabDefault() {
        var el = document.getElementById('taskactivity-tab');
        if (el) {
            el.className = 'tab-pane fade ';
        }
        var el3 = document.getElementById('taskinfo-tab');
        if (el3) {
            el3.className = 'tab-pane fade active in';
        }
        var el4 = document.getElementById('taskattachments-tab');
        if (el4) {
            el4.className = 'tab-pane fade';
        }
        var el2 = document.getElementById('taskliInfo');
        if (el2) {
            el2.className = 'active';
        }
        var el5 = document.getElementById('taskliActi');
        if (el5) {
            el5.className = ' ';
        }
        var el6 = document.getElementById('taskliAtta');
        if (el6) {
            el6.className = ' ';
        }
        var el7 = document.getElementById('taskliNotes');
        if (el7) {
            el7.className = ' ';
        }
        var el8 = document.getElementById('tasknotes-tab');
        if (el8) {
            el8.className = 'tab-pane fade';
        }
    }
                function getAssetScheduleItems2(id) {
            document.getElementById("assetscheduleList2").innerHTML = "";
            $.ajax({
                type: "POST",
                url: "WarehouseP.aspx/getAssetScheduleItems",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        if (data.d.length > 0) {
                            document.getElementById("assetplanscheduleDIV2").style.display = "block";
                            for (var i = 0; i < data.d.length; i++) {
                                var res = data.d[i].split("|");
                                var ul = document.getElementById("assetscheduleList2");
                                var li = document.createElement("li");

                                var action = 'href="#taskDocument" data-dismiss="modal"  data-toggle="modal" onclick="showTaskDocument2(' + res[0] + ')"';
                                if (res[2] == "red")
                                    action = 'href=#';
                                li.innerHTML = '<i class="fa fa-calendar" style="color:' + res[2] + '"></i><a '+action+' style="margin-left:5px;" href="#"  class="capitalize-text" >' + res[1] + '</a>';

                                ul.appendChild(li);
                            }
                        }
                        else {
                            document.getElementById("assetplanscheduleDIV2").style.display = "none";
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            }); 
        }
        function getAssetScheduleItems(id) {
            document.getElementById("assetscheduleList").innerHTML = "";
            $.ajax({
                type: "POST",
                url: "WarehouseP.aspx/getAssetScheduleItems",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        if (data.d.length > 0) {
                            document.getElementById("assetplanscheduleDIV").style.display = "block";
                            for (var i = 0; i < data.d.length; i++) {
                                var res = data.d[i].split("|");
                                var ul = document.getElementById("assetscheduleList");
                                var li = document.createElement("li");

                                var action = 'href="#taskDocument" data-dismiss="modal"  data-toggle="modal" onclick="showTaskDocument(' + res[0] + ')"';
                                if (res[2] == "red")
                                    action = 'href=#';
                                li.innerHTML = '<i class="fa fa-calendar" style="color:' + res[2] + '"></i><a '+action+' style="margin-left:5px;" href="#"  class="capitalize-text" >' + res[1] + '</a>';

                                ul.appendChild(li);
                            }
                        }
                        else {
                            document.getElementById("assetplanscheduleDIV").style.display = "none";
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            }); 
        }
        function rowchoicePlan(name) {
            jQuery('#ticketingViewCard').modal('hide');
            document.getElementById('AssetPlanID').value = name;
            planViewTables(name);
            getAssetScheduleItems(name);
            jQuery.ajax({
                type: "POST",
                url: "WarehouseP.aspx/getTableRowDataPlan",
                data: "{'id':'" + name + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        document.getElementById("incidentNameHeader").innerHTML = data.d[1];
                        document.getElementById("viewplanType").innerHTML = data.d[0];
                        document.getElementById("viewplanName").innerHTML = data.d[1];
                        document.getElementById("viewplanDesc").innerHTML = data.d[2];
                        document.getElementById("viewplanTType").innerHTML = data.d[3];
                        document.getElementById("viewplanTiType").innerHTML = data.d[4];
                        document.getElementById("viewplanSDate").innerHTML = data.d[5];
                        document.getElementById("viewplanEDate").innerHTML = data.d[6];
                        document.getElementById("viewplanPTime").innerHTML = data.d[7];
                        document.getElementById("viewplanCHK").innerHTML = data.d[8];
                        document.getElementById("viewplanPrio").innerHTML = data.d[9];
                        document.getElementById("viewplanAType").innerHTML = data.d[10];
                        document.getElementById("viewplanAssignee").innerHTML = data.d[11];
                        document.getElementById("viewplanSig").innerHTML = data.d[12];
                        document.getElementById("planCBSpan").innerHTML = data.d[13];
                        document.getElementById("planSiteSpan").innerHTML = data.d[14];
                        document.getElementById("planCreatedOnSpan").innerHTML = data.d[15]; 
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function rowchoice2(name) {
            displayBackSpare();
            getviewassetspareRemarks(name);
            jQuery.ajax({
                type: "POST",
                url: "WarehouseP.aspx/getTableRowData",
                data: "{'id':'" + name + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        document.getElementById("platenumberSpan2").innerHTML = data.d[0];
                        document.getElementById("platesourceSpan2").innerHTML = data.d[1];
                        document.getElementById("plateCodeSpan2").innerHTML = data.d[2]; 
                         
                        document.getElementById("itemsubCategorySpan2").innerHTML = data.d[8];

                        document.getElementById("itembarcodeSpan2").innerHTML = data.d[9];
                        document.getElementById("itemserialnoSpan2").innerHTML = data.d[10];

                        document.getElementById("itemmakeSpan2").innerHTML = data.d[13];
                        document.getElementById("itemmodelSpan2").innerHTML = data.d[14];

                        if (data.d[15] == "N/A") {
                            document.getElementById('quantitySpan2div').style.display = 'none';
                        }
                        else {
                            document.getElementById('quantitySpan2div').style.display = 'block';
                            document.getElementById("quantitySpan2").innerHTML = data.d[15];
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function addNotesToItem() {
            if (document.getElementById('additemNotesTA').style.display != 'block') {
                document.getElementById('additemNotesTA').style.display = 'block';
            }
            else {
                saveItemRemarks("false");
            }
        }
                function saveItemRemarks() {
            var projn = document.getElementById("additemNotesTA").value;
            var id = document.getElementById('rowidChoice').value;
            var isPass = true;
            if (isEmptyOrSpaces(document.getElementById('additemNotesTA').value)) {
                isPass = false;
                showAlert("Kindly provide remarks to be added");
            }
            else {
                if (isSpecialChar(document.getElementById('additemNotesTA').value)) {
                    isPass = false;
                    showAlert("Kindly remove special character from remarks");
                }
            }
            if (isPass) {
                $.ajax({
                    type: "POST",
                    url: "WarehouseP.aspx/addNewItemRemarks",
                    data: "{'id':'" + id + "','notes':'" + projn + "','uname':'" + loggedinUsername + "'}",
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d == "SUCCESS") {
                            showAlert("Successfully added remarks");
                            document.getElementById("additemNotesTA").value = "";
                            rowchoice(id);
                        }
                        else if (data.d == "LOGOUT") {
                            document.getElementById('<%= logoutbtn.ClientID %>').click();
                        }
                        else {
                            showAlert('Failed to save remarks. ' + data.d);
                        }
                    }
                });
            }
        }
    
        function rowchoice(name) {
            startRot();
            document.getElementById('rowidChoiceOld').value = name;
            document.getElementById('rowidChoice').value = name;
            document.getElementById('divAttachmentHolder').style.display = "block";
            document.getElementById('divAssetPlanDetails').style.display = "none";
            document.getElementById('divAssetSpareDetails').style.display = "none";
            
            
            assignrowData(name);
            getSparePartlistItems(name);
            getAssetPlanlistItems(name);
            oldDivContainers();
            insertAttachmentIcons(name);
            insertAttachmentTabData(name);
            insertAttachmentData(name);
            infotabDefault();
            dispatchAssignMapTab();
            getviewassetRemarks(name);
        }
        function rowchoiceSpare(name) {
            startRot();
            document.getElementById('rowidChoice').value = name;
            document.getElementById('divAttachmentHolder').style.display = "block"; 
            assignrowData(name);
            getSparePartlistItems(name);
            getAssetPlanlistItems(name);
            oldDivContainers();
            insertAttachmentIcons(name);
            insertAttachmentTabData(name);
            insertAttachmentData(name);
            infotabDefault();
            dispatchAssignMapTab();
            getviewassetRemarks(name);
        }
        function clearFoundItem() {
            grpClicked = false;
            document.getElementById('addLi').style.display = "none";
            document.getElementById('grpCheck').checked = false;
            document.getElementById('transferFromCheck').checked = false;
            document.getElementById('MainContent_transferSiteSelectFrom').selectedIndex = "0";
            document.getElementById('SelectCameraType').selectedIndex = "0";
            document.getElementById('tbfindRefernce').value = '';

            document.getElementById("cameraUploadDIV").style.display = "block";
            document.getElementById("webcameraUploadDIV").style.display = "none";

            document.getElementById("webcameraCapture").style.display = "none";
            document.getElementById("results").innerHTML = "";
            document.getElementById("resultsHeader").style.display = "none";
            document.getElementById("imagePath").text = '';

            document.getElementById('tbItemReference').value = '';
            document.getElementById('barcode').src = '';
            document.getElementById("tbItemBrand").value = "";
            document.getElementById("itemStatusSelect").value = "";
            document.getElementById("tbreceiverName").value = "";
            document.getElementById("tbfinderName").value = "";
            document.getElementById("tbroomNumber").value = "";
            document.getElementById("tbItemReference").value = "";
            document.getElementById("imagePath").text = "";
            document.getElementById("resultsHeader").style.display = "none";
            document.getElementById('results').innerHTML = "";
        }

        function rowchoiceFound(name) {
            document.getElementById("fDisposeHolder").style.display = "none";
            document.getElementById("fReturnHolder").style.display = "none";
            document.getElementById("fdivAttachmentHolder").style.display = "block";
            jQuery('#searchFoundItem').modal('hide');
            jQuery('#itemEnquiryModal').modal('hide');
            jQuery("#searchTable tbody").empty();
            document.getElementById("tbSearchBrand").value = "";
            startRot();
            document.getElementById('rowidChoice').value = name;
            assignrowDataFound(name);
            getChecklistItems(name);
            oldDivContainers();
            foundInsertAttachmentTabData(name);
            foundInsertAttachmentData(name);
            finfotabDefault();
            fdispatchAssignMapTab();
        }

        function fdispatchAssignMapTab() {
            var el = document.getElementById('video-0-tab');
            if (el) {
                el.className = 'tab-pane fade ';
            }
            var el3 = document.getElementById('location-tab');
            if (el3) {
                el3.className = 'tab-pane fade ';
            }
            var el4 = document.getElementById('image-1-tab');
            if (el4) {
                el4.className = 'tab-pane fade active in';
            }
            var el5 = document.getElementById('image-2-tab');
            if (el5) {
                el5.className = 'tab-pane fade';
            }
            var el6 = document.getElementById('image-3-tab');
            if (el6) {
                el6.className = 'tab-pane fade';
            }
            var el7 = document.getElementById('image-4-tab');
            if (el7) {
                el7.className = 'tab-pane fade';
            }
            var el8 = document.getElementById('image-5-tab');
            if (el8) {
                el8.className = 'tab-pane fade';
            }
            var el9 = document.getElementById('image-6-tab');
            if (el9) {
                el9.className = 'tab-pane fade';
            }
            var el9 = document.getElementById('image-7-tab');
            if (el9) {
                el9.className = 'tab-pane fade';
            }
            var el9 = document.getElementById('image-8-tab');
            if (el9) {
                el9.className = 'tab-pane fade';
            }
            var el9 = document.getElementById('image-9-tab');
            if (el9) {
                el9.className = 'tab-pane fade';
            }
            var el9 = document.getElementById('image-10-tab');
            if (el9) {
                el9.className = 'tab-pane fade';
            }
            var el10 = document.getElementById('image-0-tab');
            if (el10) {
                el10.className = 'tab-pane fade';
            }

        }
        function dispatchAssignMapTab() {
            var el = document.getElementById('video-0-tab');
            if (el) {
                el.className = 'tab-pane fade ';
            }
            var el3 = document.getElementById('location-tab');
            if (el3) {
                el3.className = 'tab-pane fade active in';
            }
            var el4 = document.getElementById('image-1-tab');
            if (el4) {
                el4.className = 'tab-pane fade';
            }
            var el5 = document.getElementById('image-2-tab');
            if (el5) {
                el5.className = 'tab-pane fade';
            }
            var el6 = document.getElementById('image-3-tab');
            if (el6) {
                el6.className = 'tab-pane fade';
            }
            var el7 = document.getElementById('image-4-tab');
            if (el7) {
                el7.className = 'tab-pane fade';
            }
            var el8 = document.getElementById('image-5-tab');
            if (el8) {
                el8.className = 'tab-pane fade';
            }
            var el9 = document.getElementById('image-6-tab');
            if (el9) {
                el9.className = 'tab-pane fade';
            }
            var el10 = document.getElementById('image-0-tab');
            if (el10) {
                el10.className = 'tab-pane fade';
            }

        }
        function insertAttachmentIcons(id) {
            jQuery('#divAttachment div').html('');
            jQuery.ajax({
                type: "POST",
                url: "WarehouseP.aspx/getAttachmentDataIcons",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    document.getElementById("divAttachment").innerHTML = data.d;
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
        }
        function assetinsertAttachmentTabData(id) {
            jQuery('#aattachments-info-tab div').html(''); 
            jQuery.ajax({
                type: "POST",
                url: "WarehouseP.aspx/getAttachmentAssetDataTab",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        document.getElementById("aattachments-info-tab").innerHTML = data.d;
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
    function insertAttachmentTabData(id) {
        jQuery('#attachments-info-tab div').html('');

        jQuery.ajax({
            type: "POST",
            url: "WarehouseP.aspx/getAttachmentDataTab",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    document.getElementById("attachments-info-tab").innerHTML = data.d;
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
        }
        function deleteAttachmentChoiceAsset2(id) {
            jQuery('#deleteAttachModalAsset2').modal('show');
            document.getElementById('rowidChoiceAttachment').value = id;
            jQuery('#newAssetModal').modal('hide');
        }
            function deleteAttachmentAsset2() {
        jQuery.ajax({
            type: "POST",
            url: "WarehouseP.aspx/deleteAttachmentDataAsset",
            data: "{'id':'" + document.getElementById('rowidChoiceAttachment').value + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d != "LOGOUT") {
                    showAlert(data.d);
                    assetinsertAttachmentTabData(document.getElementById('newAssetID').value);
                    jQuery('#newAssetModal').modal('show');
                }
                else {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function deleteAttachmentChoiceAsset(id) {
        jQuery('#deleteAttachModalAsset').modal('show');
        document.getElementById('rowidChoiceAttachment').value = id;
        jQuery('#ticketingViewCard').modal('hide');
    }
    function deleteAttachmentAsset() {
        jQuery.ajax({
            type: "POST",
            url: "WarehouseP.aspx/deleteAttachmentDataAsset",
            data: "{'id':'" + document.getElementById('rowidChoiceAttachment').value + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d != "LOGOUT") {
                    showAlert(data.d);
                    rowchoice(document.getElementById('rowidChoice').value);
                    jQuery('#ticketingViewCard').modal('show');
                }
                else {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function deleteAttachmentChoice(id) {

        jQuery('#deleteAttachModal').modal('show');
        document.getElementById('rowidChoiceAttachment').value = id;
        jQuery('#foundItemViewCard').modal('hide');
    }
    function deleteAttachment() {
        jQuery.ajax({
            type: "POST",
            url: "WarehouseP.aspx/deleteAttachmentData",
            data: "{'id':'" + document.getElementById('rowidChoiceAttachment').value + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d != "LOGOUT") {
                    showAlert(data.d);
                    oldDivContainers();
                    foundInsertAttachmentTabData(document.getElementById('rowidChoice').value);
                    foundInsertAttachmentData(document.getElementById('rowidChoice').value);
                    finfotabDefault();
                    fdispatchAssignMapTab();
                    jQuery('#foundItemViewCard').modal('show');
                }
                else {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function insertAttachmentData(id) {
        jQuery.ajax({
            type: "POST",
            url: "WarehouseP.aspx/getAttachmentData",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    for (var i = 0; i < data.d.length; i++) {
                        if (data.d[i].indexOf("video") >= 0) {
                            var div = document.createElement('div');
                            div.className = 'tab-pane fade';
                            div.innerHTML = data.d[i];
                            div.id = 'video-' + (i + 1) + '-tab';
                            document.getElementById('divAttachmentHolder').appendChild(div);
                            divArray[i] = 'video-' + (i + 1) + '-tab';
                        }
                        else {
                            var div = document.createElement('div');
                            div.className = 'tab-pane fade';
                            div.align = 'center';
                            div.style.height = '420px';
                            div.innerHTML = data.d[i];
                            div.id = 'image-' + (i + 1) + '-tab';
                            document.getElementById('divAttachmentHolder').appendChild(div);
                            divArray[i] = 'image-' + (i + 1) + '-tab';
                        }
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function foundInsertAttachmentTabData(id) {
        jQuery('#fattachments-info-tab div').html('');

        jQuery.ajax({
            type: "POST",
            url: "WarehouseP.aspx/getAttachmentDataTabFound",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        document.getElementById("fattachments-info-tab").innerHTML = data.d;
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function foundInsertAttachmentData(id) {
            jQuery.ajax({
                type: "POST",
                url: "WarehouseP.aspx/getAttachmentDataFound",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    for (var i = 0; i < data.d.length; i++) {
                        if (data.d[i].indexOf("video") >= 0) {
                            var div = document.createElement('div');
                            div.className = 'tab-pane fade';
                            div.innerHTML = data.d[i];
                            div.id = 'video-' + (i + 1) + '-tab';
                            document.getElementById('fdivAttachmentHolder').appendChild(div);
                            divArray[i] = 'video-' + (i + 1) + '-tab';
                        }
                        else {
                            var div = document.createElement('div');
                            div.className = 'tab-pane fade active in';
                            div.align = 'center';
                            div.style.height = '420px';
                            div.innerHTML = data.d[i];
                            div.id = 'image-' + (i + 1) + '-tab';
                            document.getElementById('fdivAttachmentHolder').appendChild(div);
                            divArray[i] = 'image-' + (i + 1) + '-tab';
                        }
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function play(i) {
        try {
            var player = document.getElementById('Video0');
            player.play();
        } catch (error) {
            //alert('play-' + err);
        }
    }
    function assignrowDataFound(id) {
        jQuery.ajax({
            type: "POST",
            url: "WarehouseP.aspx/getTableRowDataFound",
            data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    document.getElementById("fItemStatus").innerHTML = data.d[0];
                    document.getElementById("foundBySpan").innerHTML = data.d[1];
                    document.getElementById("flocSpan").innerHTML = data.d[2];
                    document.getElementById("ftimeSpan").innerHTML = data.d[3];
                    document.getElementById("fItemType").innerHTML = data.d[4];
                    document.getElementById("fItemBrand").innerHTML = data.d[5];
                    document.getElementById("fItemStorage").innerHTML = data.d[6];
                    document.getElementById("fItemReference").innerHTML = data.d[7];
                    document.getElementById("fDepartmentSpan").innerHTML = data.d[8];
                    document.getElementById("fReceiveName").innerHTML = data.d[9];
                    document.getElementById("fReceiveDT").innerHTML = data.d[10];
                    document.getElementById("fRoomNumber").innerHTML = data.d[11];
                    document.getElementById("fItemColour").innerHTML = data.d[12];

                    document.getElementById("fItemSubType").innerHTML = data.d[13];
                    document.getElementById("fItemSubStorage").innerHTML = data.d[14];
                    document.getElementById("fItemShelfLife").innerHTML = data.d[15];

                    if (data.d[0] == "FOUND ITEM") {
                        document.getElementById("fliDispose").style.display = "none";
                        document.getElementById("fliReturn").style.display = "none";

                        document.getElementById("returnHandleDiv").style.display = "none";
                        document.getElementById("processHandleDiv").style.display = "none";
                        document.getElementById("foundDiv").style.display = "block";
                        document.getElementById("diposeHandleDiv").style.display = "none";
                    }
                    else if (data.d[0] == "RETURN ITEM") {
                        document.getElementById("fliDispose").style.display = "none";
                        document.getElementById("fliReturn").style.display = "block";

                        document.getElementById("returnHandleDiv").style.display = "none";
                        document.getElementById("processHandleDiv").style.display = "block";
                        document.getElementById("foundDiv").style.display = "none";
                        document.getElementById("diposeHandleDiv").style.display = "none";

                        document.getElementById("fReturnDate").innerHTML = data.d[16];
                        document.getElementById("fOwnerName").innerHTML = data.d[17];
                        document.getElementById("fOwnerType").innerHTML = data.d[18];
                        document.getElementById("fOwnerRoom").innerHTML = data.d[19];
                        document.getElementById("fOwnerNation").innerHTML = data.d[20];
                        document.getElementById("fOwnerAddress").innerHTML = data.d[21];
                        document.getElementById("fOwnerContact").innerHTML = data.d[22];
                        document.getElementById("fOwnerEmail").innerHTML = data.d[23];
                        document.getElementById("fOwnerNumber").innerHTML = data.d[24];
                        document.getElementById("fOwnerTracking").innerHTML = data.d[25];
                        document.getElementById("fOwnerHanded").innerHTML = data.d[26];

                        document.getElementById("fOwnerTransfersiteDIV").style.display = data.d[27];
                        document.getElementById("fOwnerTransfersite").innerHTML = data.d[28];

                    }
                    else if (data.d[0] == "DISPOSE ITEM" || data.d[0] == "DONATE ITEM") {
                        document.getElementById("fliDispose").style.display = "block";
                        document.getElementById("fliReturn").style.display = "none";

                        document.getElementById("returnHandleDiv").style.display = "none";
                        document.getElementById("processHandleDiv").style.display = "block";
                        document.getElementById("foundDiv").style.display = "none";
                        document.getElementById("diposeHandleDiv").style.display = "none";

                        document.getElementById("dReturnDate").innerHTML = data.d[16];
                        document.getElementById("dOwnerName").innerHTML = data.d[17];
                        document.getElementById("dOwnerType").innerHTML = data.d[18];
                        document.getElementById("dOwnerRoom").innerHTML = data.d[19];
                        document.getElementById("dOwnerNation").innerHTML = data.d[20];
                        document.getElementById("dOwnerAddress").innerHTML = data.d[21];
                        document.getElementById("dOwnerContact").innerHTML = data.d[22];
                        document.getElementById("dOwnerEmail").innerHTML = data.d[23];
                        document.getElementById("dOwnerNumber").innerHTML = data.d[24];
                        document.getElementById("dOwnerTracking").innerHTML = data.d[25];
                        document.getElementById("dOwnerHanded").innerHTML = data.d[26];

                        if (data.d[0] == "DONATE ITEM") {
                            document.getElementById("DAPSDiv").style.display = "none";
                        }
                        if (data.d[17] != "Destroyed") {
                            document.getElementById("DAPSDiv").style.display = "none";
                        }
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
        }
        var subid = 0;
        function rowchoiceSpareEdit(id) {
                        document.getElementById('rowidChoice').value = id;
                        document.getElementById("linsspSave").style.display = "none";
                document.getElementById("linsspUpdate").style.display = "block";
            jQuery.ajax({
                type: "POST",
                url: "WarehouseP.aspx/getTableRowData",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {

                        document.getElementById("MainContent_assetsparechartSelect").value = data.d[19];
                        jQuery('#MainContent_assetsparechartSelect').selectpicker('val', data.d[19]);
                        document.getElementById("newtbPartQT").value = data.d[15];
                        document.getElementById("newtbPartName").value = data.d[0];
                        document.getElementById("newtbPartDesc").value = data.d[1];
                        document.getElementById("newtbPartMake").value = data.d[13];
                        document.getElementById("newtbPartModel").value = data.d[14];
                        document.getElementById("newtbPartBarcode").value = data.d[9];
                        document.getElementById("newtbPartSerial").value = data.d[10];
                        subid = parseInt(data.d[21]);
                        try {
                            if (parseInt(data.d[20]) > 0) {
                                document.getElementById('newsparecategorySubAssetSelect').innerHTML = "";
                                document.getElementById('MainContent_newspareassetCategorySelect').value = data.d[20]; 
                                jQuery.ajax({
                                    type: "POST",
                                    url: "WarehouseP.aspx/getSubCategories",
                                    data: "{'id':'" + data.d[20] + "','uname':'" + loggedinUsername + "'}",
                                    dataType: "json",
                                    contentType: "application/json; charset=utf-8",
                                    success: function (datax) {
                                        if (datax.d[0] == "LOGOUT") {
                                            showError("Session has expired. Kindly login again.");
                                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                                        } else {
                                            for (var i = 0; i < datax.d.length; i++) {
                                                var select = document.getElementById("newsparecategorySubAssetSelect");
                                                var res = datax.d[i].split('_');
                                                var opt = document.createElement('option');
                                                opt.value = res[1];
                                                opt.innerHTML = res[0];
                                                select.appendChild(opt);
                                            }
                                            if (subid > 0) {
                                                document.getElementById('newsparecategorySubAssetSelect').value = subid; 
                                            }
                                        }
                                    },
                                    error: function () {
                                        showError("Session timeout. Kindly login again.");
                                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                                    }
                                });
                            }
                        }
                        catch (ex) {
                            alert(ex);
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        var subid2 = 0
        var locid2 = 0
        var locid = "";
        function rowchoiceEdit(id) {
            ddetailsClick();
            document.getElementById('rowidChoice').value = id;
            document.getElementById("assetHeader").innerHTML = "EDIT ASSET";
            document.getElementById("updateAssetItemLi").style.display = "block"; 
            
                        document.getElementById("updateAssetPlanLi").style.display = "none";
            document.getElementById("saveAssetItemLi").style.display = "none";
            document.getElementById("pplansClickLI").style.display = "none";
            document.getElementById("rremarksClickLI").style.display = "none";
            document.getElementById("ssparepartClickLI").style.display = "none";
            document.getElementById("aattachClickLI").style.display = "none";

            
            jQuery.ajax({
                type: "POST",
                url: "WarehouseP.aspx/getTableRowData",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                           document.getElementById('tbAssetContractor').value = data.d[24];
                        document.getElementById('customerslst').value = data.d[23];
                        document.getElementById('projectlst').value = data.d[22]; 
                        document.getElementById("tbAssetName").value = data.d[0];
                        document.getElementById("tbAssetComments").value = data.d[1];
                        document.getElementById("tbAssetMake").value = data.d[13];
                        document.getElementById("tbAssetModel").value = data.d[14];
                        document.getElementById("tbAssetBarcode").value = data.d[9];
                        document.getElementById("tbAssetSNumber").value = data.d[10];
                        subid2 = parseInt(data.d[21]);
                        locid2 = parseInt(data.d[26]);
                        locid = data.d[3];
                        try {
                            if (parseInt(data.d[20]) > 0) {
                                document.getElementById('categorySubAssetSelect').innerHTML = "";
                                document.getElementById('MainContent_categoryAssetSelect').value = data.d[20];
                                jQuery.ajax({
                                    type: "POST",
                                    url: "WarehouseP.aspx/getSubCategories",
                                    data: "{'id':'" + data.d[20] + "','uname':'" + loggedinUsername + "'}",
                                    dataType: "json",
                                    contentType: "application/json; charset=utf-8",
                                    success: function (datax) {
                                        if (datax.d[0] == "LOGOUT") {
                                            showError("Session has expired. Kindly login again.");
                                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                                        } else {
                                            for (var i = 0; i < datax.d.length; i++) {
                                                var select = document.getElementById("categorySubAssetSelect");
                                                var res = datax.d[i].split('_');
                                                var opt = document.createElement('option');
                                                opt.value = res[1];
                                                opt.innerHTML = res[0];
                                                select.appendChild(opt);
                                            }
                                            if (subid2 > 0) {
                                                document.getElementById('categorySubAssetSelect').value = subid2;
                                            }
                                        }
                                    },
                                    error: function () {
                                        showError("Session timeout. Kindly login again.");
                                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                                    }
                                });
                            }

                            if (parseInt(data.d[25]) > 0) {
                                document.getElementById('categorySubMainLocation').innerHTML = "";
                                document.getElementById('MainContent_categoryAssetMainLocation').value = data.d[7];
                                jQuery.ajax({
                                    type: "POST",
                                    url: "WarehouseP.aspx/getSubLocations",
                                    data: "{'id':'" + data.d[7] + "','uname':'" + loggedinUsername + "'}",
                                    dataType: "json",
                                    contentType: "application/json; charset=utf-8",
                                    success: function (datax) {
                                        if (datax.d[0] == "LOGOUT") {
                                            showError("Session has expired. Kindly login again.");
                                            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                                        } else {
                                            for (var i = 0; i < datax.d.length; i++) {
                                                var select = document.getElementById("categorySubMainLocation");
                                                var opt = document.createElement('option');
                                                opt.value = datax.d[i];
                                                opt.innerHTML = datax.d[i];
                                                select.appendChild(opt);
                                            }
                                            if (locid2 > 0) {
                                                document.getElementById('categorySubMainLocation').value = locid;
                                            }
                                        }
                                    },
                                    error: function () {
                                        showError("Session timeout. Kindly login again.");
                                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                                    }
                                });
                            }
                        }
                        catch (ex) {
                            alert(ex);
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        function assignrowData(id) {
            //document.getElementById("liPlan").style.display = "none";
            jQuery.ajax({
                type: "POST",
                url: "WarehouseP.aspx/getTableRowData",
                data: "{'id':'" + id + "','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        document.getElementById("platenumberSpan").innerHTML = data.d[0];
                        document.getElementById("platesourceSpan").innerHTML = data.d[1];
                        document.getElementById("plateCodeSpan").innerHTML = data.d[2];
                        document.getElementById("vehicleMakeSpan").innerHTML = data.d[3];
                        document.getElementById("usernameSpan").innerHTML = data.d[4];
                        document.getElementById("locSpan").innerHTML = data.d[5];
                        document.getElementById("timeSpan").innerHTML = data.d[6];

                        document.getElementById("incidentNameHeader").innerHTML = data.d[0];

                        document.getElementById("mainlocationSpan").innerHTML = data.d[7];
                        document.getElementById("itemsubCategorySpan").innerHTML = data.d[8];

                        document.getElementById("itembarcodeSpan").innerHTML = data.d[9];
                        document.getElementById("itemserialnoSpan").innerHTML = data.d[10];
                        //document.getElementById("liPlan").style.display = data.d[12];
                        document.getElementById("itemmakeSpan").innerHTML = data.d[13];
                        document.getElementById("itemmodelSpan").innerHTML = data.d[14];

                        document.getElementById("asssetaccountSpan").innerHTML = data.d[16];
                        document.getElementById("asssetprojectSpan").innerHTML = data.d[17];


                        if (data.d[15] == "N/A") {
                            document.getElementById('quantitySpandiv').style.display = 'none';
                        }
                        else {
                            document.getElementById('quantitySpandiv').style.display = 'block';
                            document.getElementById("quantitySpan").innerHTML = data.d[15];
                        }

                        if (data.d[18] == "N/A") {
                            document.getElementById('parentnameSpandiv').style.display = 'none';
                        }
                        else {
                            document.getElementById('parentnameSpandiv').style.display = 'block';
                            document.getElementById("parentnameSpan").innerHTML = data.d[18];
                        }
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }

        
                function addrowtoTempPlanItem() { 
            jQuery("#tempplansTable").dataTable().fnClearTable();
            jQuery("#tempplansTable").dataTable().fnDraw();
            jQuery("#tempplansTable").dataTable().fnDestroy();
            jQuery.ajax({
                type: "POST",
                url: "WarehouseP.aspx/getTempPlanData",
                data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        for (var i = 0; i < data.d.length; i++) {
                            jQuery("#tempplansTable tbody").append(data.d[i]);
                        }
                        jQuery("#tempplansTable").DataTable({
                            "dom": '<"top"f>rt<"bottom" <"datatable-pagination-info"p> <"pull-right pagination-info"i>><"clearfx">',
                            'iDisplayLength': 10,
                            "order": [[1, "desc"]]
                        });
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });

        }
        function addrowtoPlanItem() { 
            jQuery("#plansTable").dataTable().fnClearTable();
            jQuery("#plansTable").dataTable().fnDraw();
            jQuery("#plansTable").dataTable().fnDestroy();
            jQuery.ajax({
                type: "POST",
                url: "WarehouseP.aspx/getPlanData",
                data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        for (var i = 0; i < data.d.length; i++) {
                            jQuery("#plansTable tbody").append(data.d[i]);
                        }
                        jQuery("#plansTable").DataTable({
                            "dom": '<"top"f>rt<"bottom" <"datatable-pagination-info"p> <"pull-right pagination-info"i>><"clearfx">',
                            'iDisplayLength': 10,
                            "order": [[1, "desc"]]
                        });
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });

        }
    function addrowtoFoundItem() {

        jQuery("#founditemTable tbody").empty();
        jQuery.ajax({
            type: "POST",
            url: "WarehouseP.aspx/getFoundItemData",
            data: "{'id':'0','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    for (var i = 0; i < data.d.length; i++) {
                        jQuery("#founditemTable tbody").append(data.d[i]);
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
    function addrowtoTableKeys() {
        jQuery("#newkeysTable tbody").empty();
        jQuery.ajax({
            type: "POST",
            url: "WarehouseP.aspx/getKeysData",
            data: "{'id':'0','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    for (var i = 0; i < data.d.length; i++) {
                        jQuery("#newkeysTable tbody").append(data.d[i]);
                    }
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
    }
        function addrowtoTable1() {
                    jQuery("#keysTable").dataTable().fnClearTable();
            jQuery("#keysTable").dataTable().fnDraw();
            jQuery("#keysTable").dataTable().fnDestroy();
 
        jQuery.ajax({
            type: "POST",
            url: "WarehouseP.aspx/getTicketingData",
            data: "{'id':'0','uname':'" + loggedinUsername + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d[0] == "LOGOUT") {
                    showError("Session has expired. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                } else {
                    for (var i = 0; i < data.d.length; i++) {
                        jQuery("#keysTable tbody").append(data.d[i]);
                    }
                    jQuery("#keysTable").DataTable({
                        "dom": '<"top"f>rt<"bottom" <"datatable-pagination-info"p> <"pull-right pagination-info"i>><"clearfx">',
                        'iDisplayLength': 10,
                        "order": [[1, "desc"]]
                    });
                }
            },
            error: function () {
                showError("Session timeout. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            }
        });
        }
        function addrowtoTableSpare() { 
            jQuery("#spareTable").dataTable().fnClearTable();
            jQuery("#spareTable").dataTable().fnDraw();
            jQuery("#spareTable").dataTable().fnDestroy();
            jQuery.ajax({
                type: "POST",
                url: "WarehouseP.aspx/getSparePartData",
                data: "{'id':'0','uname':'" + loggedinUsername + "'}",
                async: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d[0] == "LOGOUT") {
                        showError("Session has expired. Kindly login again.");
                        setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                    } else {
                        for (var i = 0; i < data.d.length; i++) {
                            jQuery("#spareTable tbody").append(data.d[i]);
                        }
                        jQuery("#spareTable").DataTable({
                            "dom": '<"top"f>rt<"bottom" <"datatable-pagination-info"p> <"pull-right pagination-info"i>><"clearfx">',
                            'iDisplayLength': 10,
                            "order": [[1, "desc"]]
                        });
                    }
                },
                error: function () {
                    showError("Session timeout. Kindly login again.");
                    setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
                }
            });
        }
        
    function getLocationNoOnline() {


        locationAllowed = true;
        setTimeout(function () {
            google.maps.visualRefresh = true;
            var Liverpool = new google.maps.LatLng(sourceLat, sourceLon);

            // These are options that set initial zoom level, where the map is centered globally to start, and the type of map to show
            var mapOptions = {
                zoom: 8,
                center: Liverpool,
                mapTypeId: google.maps.MapTypeId.G_NORMAL_MAP
            };

            // This makes the div with id "map_canvas" a google map
            mapIncidentLocation = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);

        }, 1000);
    }
    function getIncidentLocationMarkers(obj) {

        try {
            locationAllowed = true;
            //setTimeout(function () {
            google.maps.visualRefresh = true;
            var Liverpool = new google.maps.LatLng(obj[0].Lat, obj[0].Long);
            // These are options that set initial zoom level, where the map is centered globally to start, and the type of map to show
            var mapOptions = {
                zoom: 15,
                center: Liverpool,
                mapTypeId: google.maps.MapTypeId.G_NORMAL_MAP
            };

            /// This makes the div with id "map_canvas" a google map
            //mapIncidentLocation = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
            mapIncidentLocation = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);

            for (var i = 0; i < obj.length; i++) {

                var contentString = '<div id="content">' + obj[i].Username +
                '<br/></div>';

                var myLatlng = new google.maps.LatLng(obj[i].Lat, obj[i].Long);

                var marker = new google.maps.Marker({ position: myLatlng, map: mapIncidentLocation, title: obj[i].Username });
                marker.setIcon('https://testportalcdn.azureedge.net/Images/marker.png')
                myMarkersIncidentLocation[obj[i].Username] = marker;
                createInfoWindowIncidentLocation(marker, contentString);
            }
        }
        catch (err) {
            alert(err)
        }
    }
    function createInfoWindowIncidentLocation(marker, popupContent) {
        google.maps.event.addListener(marker, 'click', function () {
            infoWindowIncidentLocation.setContent(popupContent);
            infoWindowIncidentLocation.open(mapIncidentLocation, this);
        });
        }

        
function tracebackOnFilter(duration, type) {

    var id = 0;
    if (type == "task")
        id = document.getElementById('rowChoiceTasks').value;
    else
        id = document.getElementById('rowidChoiceInci').text;

    changeFilterMarkers(id, type, duration);
    }
            var infoWindowTaskLocation;
        var dduration = "";
function changeFilterMarkers(id, type, duration) {

    if (type == "task") {
        if (duration == "1") {
            var el = document.getElementById('taskfilter1');
            if (el) {
                if (el.className == 'fa fa-check-square-o') {
                    el.className = 'fa fa-square-o';
                    duration = "0";
                }
                else
                    el.className = 'fa fa-check-square-o';
            }
            var ell2 = document.getElementById('taskfilter2');
            if (ell2) {
                ell2.className = 'fa fa-square-o';
            }
            var ell3 = document.getElementById('taskfilter3');
            if (ell3) {
                ell3.className = 'fa fa-square-o';
            }
            var ell4 = document.getElementById('taskfilter4');
            if (ell4) {
                ell4.className = 'fa fa-square-o';
            }
        }
        else if (duration == "5") {
            var el2 = document.getElementById('taskfilter2');
            if (el2) {
                if (el2.className == 'fa fa-check-square-o') {
                    el2.className = 'fa fa-square-o';
                    duration = "0";
                }
                else
                    el2.className = 'fa fa-check-square-o';
            }
            var ell = document.getElementById('taskfilter1');
            if (ell) {
                ell.className = 'fa fa-square-o';
            }
            var ell3 = document.getElementById('taskfilter3');
            if (ell3) {
                ell3.className = 'fa fa-square-o';
            }
            var ell4 = document.getElementById('taskfilter4');
            if (ell4) {
                ell4.className = 'fa fa-square-o';
            }
        }
        else if (duration == "30") {
            var el3 = document.getElementById('taskfilter3');
            if (el3) {
                if (el3.className == 'fa fa-check-square-o') {
                    el3.className = 'fa fa-square-o';
                    duration = "0";
                }
                else
                    el3.className = 'fa fa-check-square-o';
            }
            var ell = document.getElementById('taskfilter1');
            if (ell) {
                ell.className = 'fa fa-square-o';
            }
            var ell2 = document.getElementById('taskfilter2');
            if (ell2) {
                ell2.className = 'fa fa-square-o';
            }
            var ell4 = document.getElementById('taskfilter4');
            if (ell4) {
                ell4.className = 'fa fa-square-o';
            }
        }
        else if (duration == "60") {
            var el4 = document.getElementById('taskfilter4');
            if (el4) {
                if (el4.className == 'fa fa-check-square-o') {
                    el4.className = 'fa fa-square-o';
                    duration = "0";
                }
                else
                    el4.className = 'fa fa-check-square-o';
            }
            var ell = document.getElementById('taskfilter1');
            if (ell) {
                ell.className = 'fa fa-square-o';
            }
            var ell2 = document.getElementById('taskfilter2');
            if (ell2) {
                ell2.className = 'fa fa-square-o';
            }
            var ell3 = document.getElementById('taskfilter3');
            if (ell3) {
                ell3.className = 'fa fa-square-o';
            }
        }
    }
    else {
        if (duration == "1") {
            var el = document.getElementById('filter1');
            if (el) {
                if (el.className == 'fa fa-check-square-o') {
                    el.className = 'fa fa-square-o';
                    duration = "0";
                }
                else
                    el.className = 'fa fa-check-square-o';
            }
            var elll2 = document.getElementById('filter2');
            if (elll2) {
                elll2.className = 'fa fa-square-o';
            }
            var elll3 = document.getElementById('filter3');
            if (elll3) {
                elll3.className = 'fa fa-square-o';
            }
            var elll4 = document.getElementById('filter4');
            if (elll4) {
                elll4.className = 'fa fa-square-o';
            }
        }
        else if (duration == "5") {
            var el2 = document.getElementById('filter2');
            if (el2) {
                if (el2.className == 'fa fa-check-square-o') {
                    el2.className = 'fa fa-square-o';
                    duration = "0";
                }
                else
                    el2.className = 'fa fa-check-square-o';
            }
            var elll = document.getElementById('filter1');
            if (elll) {
                elll.className = 'fa fa-square-o';
            }
            var elll3 = document.getElementById('filter3');
            if (elll3) {
                elll3.className = 'fa fa-square-o';
            }
            var elll4 = document.getElementById('filter4');
            if (elll4) {
                elll4.className = 'fa fa-square-o';
            }
        }
        else if (duration == "30") {
            var el3 = document.getElementById('filter3');
            if (el3) {
                if (el3.className == 'fa fa-check-square-o') {
                    el3.className = 'fa fa-square-o';
                    duration = "0";
                }
                else
                    el3.className = 'fa fa-check-square-o';
            }
            var elll = document.getElementById('filter1');
            if (elll) {
                elll.className = 'fa fa-square-o';
            }
            var elll2 = document.getElementById('filter2');
            if (elll2) {
                elll2.className = 'fa fa-square-o';
            }
            var elll4 = document.getElementById('filter4');
            if (elll4) {
                elll4.className = 'fa fa-square-o';
            }
        }
        else if (duration == "60") {
            var el4 = document.getElementById('filter4');
            if (el4) {
                if (el4.className == 'fa fa-check-square-o') {
                    el4.className = 'fa fa-square-o';
                    duration = "0";
                }
                else
                    el4.className = 'fa fa-check-square-o';
            }
            var elll = document.getElementById('filter1');
            if (elll) {
                elll.className = 'fa fa-square-o';
            }
            var elll2 = document.getElementById('filter2');
            if (elll2) {
                elll2.className = 'fa fa-square-o';
            }
            var elll3 = document.getElementById('filter3');
            if (elll3) {
                elll3.className = 'fa fa-square-o';
            }
        }
    }
    dduration = duration;
    jQuery.ajax({
        type: "POST",
        url: "WarehouseP.aspx/getTracebackLocationDataByUser",
        data: JSON.stringify({ id: id, duration: dduration, ttype: type, userIds: sselectedUserIds, uname: loggedinUsername }),
        //data: "{'id':'" + id + "','duration':'" + dduration + "','ttype':'" + type + "','userIds':'" + uid + "'}",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if (data.d == "LOGOUT") {
                showError("Session has expired. Kindly login again.");
                setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
            } else {
                var obj = jQuery.parseJSON(data.d)

                if (type == "task")
                    updateTaskMarker(obj);
                else
                    updateIncidentMarker(obj);
            }
        },
        error: function () {
            showError("Session timeout. Kindly login again.");
            setTimeout(function () { document.getElementById('<%= logoutbtn.ClientID %>').click(); }, 3000);
        }
    });
        }
        var mapTaskLocation;
        var sselectedUserIds = [];
            var myTraceBackMarkers = new Array();
    </script>

            <section class="content-wrapper" role="main">
                                              
            <div class="content">
                <div class="content-body">
                    <div class="panel fade in panel-default panel-main-page" data-init-panel="true">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-md-2">
                                    <h3 class="panel-title"><span class="hidden-xs">WAREHOUSE</span></h3>
                                </div>
                                <div class="col-md-7">
                                    <div class="panel-control">
                                        <ul id="demo3-tabs" class="nav nav-tabs nav-main">
                                            <li style="display:none;"   class="active" ><a data-toggle="tab" href="#home-tab" onclick="jQuery('.show-component').show();">L&F Inventory</a>
                                            </li>
                                            <li  style="display:none;" ><a data-toggle="tab" href="#enquiry-tab" onclick="jQuery('.show-component').show();">Enquiry</a>
                                            </li>
                                            <li <%=assetview%>  class="active" ><a data-toggle="tab" href="#keys-tab" onclick="jQuery('.show-component').show();">Asset</a>
                                            </li>
                                            <li <%=keyview%>><a data-toggle="tab" href="#newkeys-tab" onclick="jQuery('.show-component').show();">Key</a>
                                            </li>
                                             <li><a data-toggle="tab" href="#plans-tab" onclick="jQuery('.show-component').show();">Plans</a>
                                            </li>
                                            <li><a data-toggle="tab" href="#asset-tab" onclick="jQuery('.show-component').show();">Settings</a>
                                            </li>
                                        </ul>
                                        <!-- /.nav -->
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div role="group" class="pull-right">
                                        <%=siteName%>
                                        <a style="font-size:smaller;color:gray;margin-right:5px" onmouseover="this.style.color='#b2163b'" onmouseout="this.style.color='gray'" data-toggle='tab' href='#user-profile-tab' onclick='assignUserProfileData()'><%=senderName3%></a><a style="margin-left:0px;color:gray" onmouseover="this.style.color='#b2163b'" onmouseout="this.style.color='gray'" href="#" onclick="forceLogout()" class="fa fa-circle-o-notch fa-lg"></a>
                                    <asp:Button ID="closingbtn" runat="server" OnClick="LogoutButton_Click" Text="LOGOUT" style="display:none"/>
                                        <asp:Button ID="logoutbtn" runat="server" OnClick="forceLogoutButton_Click" Text="LOGOUT" style="display:none"/>
                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="tab-content">
							<div class="tab-pane fade" style="display:none;" id="home-tab">
                            <div class="tab-content" style="display:none;">
                                <div class="row mb-4x">
                                                                        <div class="col-md-2">
                                        <div class="row vertical-navigation vertical-components-show">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">
                                                    <li class="active" style="display:none;"><a href="#show-component" data-toggle="tab">All</a>
                                                    </li>
                                                </ul>
                                                <!-- /.nav -->
                                            </div>

                                        </div>
                                        <div class="row vertical-navigation new-events">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">
                                                    <li ><a href="#" data-target="#newFoundItem" data-toggle="modal" class="capitalize-text" id="regFoundItem" onclick="clearFoundItem()">New Item Received</a>
                                                    </li>
                                                    <li ><a href="#" data-target="#searchFoundItem" data-toggle="modal" class="capitalize-text">Search Item</a>
                                                    </li>
                                                    <li ><a href="#" data-target="#itemEnquiryModal" data-toggle="modal" onclick="unlockEnquiry();" class="capitalize-text">Item Enquiry</a>
                                                    </li>
                                                    <li ><a href="#" data-target="#generateBcodeModal" data-toggle="modal" class="capitalize-text" onclick="document.getElementById('tbGenerateBarcode').value = '';document.getElementById('barcode2').src = '';">Create Barcode</a>
                                                    </li>
                                                </ul>
                                                <!-- /.nav -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="row show-component component-number-1">
                                            <div class="col-md-12">
                                                <div data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-8">
                                                        <h3 class="panel-title capitalize-text">L&F INVENTORY</h3>
                                                        <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div class="progress" style="display:none;">
                                                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="display:none;width: 0px">
                                                                            </div>
                                                                        </div>                                                          
                                                                    </div>
                                                            <div class="col-md-8">
                                                                <p class="white-color progress-bar-title"></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" id="founditemTable" order-of-rows="desc" order-of-column="1" role="grid">
                                                        <thead>
                                                            <tr role="row">
                                                                 <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">REF#<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="USER">DATE<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">BRAND<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="TIME">FOUND<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="TIME">STORAGE<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="STATUS">STATUS<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                            </div>
                                        </div>
                                        <!-- /.table -->

                                    </div>
                                </div>
                            </div>
							</div>
                            <div class="tab-pane fade " style="display:none;" id="enquiry-tab">
                            <div class="tab-content" style="display:none;">
                                <div class="row mb-4x">
                                                                        <div class="col-md-2">
                                        <div class="row vertical-navigation vertical-components-show">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">
                                                    <li class="active" style="display:none;"><a href="#show-component" data-toggle="tab">All</a>
                                                    </li>
                                                </ul>
                                                <!-- /.nav -->
                                            </div>

                                        </div>
                                        <div class="row vertical-navigation new-events">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">
                                                    <li  ><a href="#" data-target="#itemEnquiryModal" data-toggle="modal" onclick="unlockEnquiry()" class="capitalize-text">Item Enquiry</a>
                                                    </li>
                                                </ul>
                                                <!-- /.nav -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="row show-component component-number-keys">
                                            <div class="col-md-12">
                                                <div data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-8">
                                                        <h3 class="panel-title capitalize-text">ITEM ENQUIRY</h3>
                                                        <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div class="progress" style="display:none;">
                                                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="display:none;width: 0px">
                                                                            </div>
                                                                        </div>                                                          
                                                                    </div>
                                                            <div class="col-md-8">
                                                                <p class="white-color progress-bar-title"></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" id="enquiryTable" role="grid">
                                                        <thead>
                                                            <tr role="row">
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="REF">REFERENCE NO<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="USER">ENQUIRER<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">DATE<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="TIME">CREATED BY<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                            </div>
                                        </div>
                                        <!-- /.table -->

                                    </div>
                                </div>
                            </div>
							</div>
                            <div class="tab-pane fade <%=assetactive%>" id="keys-tab">
                            <div class="tab-content">
                                <div class="row mb-4x">
                                   <div class="col-md-2">
                                        <div class="row vertical-navigation vertical-components-show">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">
                                                    <li class="active" style="display:none;"><a href="#show-component" data-toggle="tab">All</a>
                                                    </li>
                                                </ul>
                                                <!-- /.nav -->
                                            </div>

                                        </div>
                                        <div class="row vertical-navigation new-events">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">
                                                    <li><a href="#" data-target="#newAssetModal" data-toggle="modal" class="capitalize-text" onclick="assetClick('ASSET');" id="newassetA">+ NEW ASSET</a>
                                                    </li>
                                                    <li><a href="#" data-target="#newSpareModal" data-toggle="modal" class="capitalize-text" onclick="clearnewSpare()" id="clearnewSpareA">+ NEW SPARE</a>
                                                    </li>
                                                </ul>
                                                <!-- /.nav -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="row show-component component-number-keys">
                                            <div class="col-md-12">
                                                <div data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-8">
                                                        <h3 class="panel-title capitalize-text">Assets</h3>
                                                        <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div class="progress" style="display:none;">
                                                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="display:none;width: 0px">
                                                                            </div>
                                                                        </div>                                                          
                                                                    </div>
                                                            <div class="col-md-8">
                                                                <p class="white-color progress-bar-title"></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" id="keysTable" order-of-rows="desc" order-of-column="1" role="grid">
                                                        <thead>
                                                            <tr role="row">
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="USER">NAME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">DATE<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="TIME">CREATED BY<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="TIME">LOCATION<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="STATUS">BARCODE<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                            </div>
                                        </div>
                                        <!-- /.table -->
                                        <div class="row show-component component-number-sparts">
                                            <div class="col-md-12">
                                                <div data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-8">
                                                        <h3 class="panel-title capitalize-text">Spare Parts</h3>
                                                        <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div class="progress" style="display:none;">
                                                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="display:none;width: 0px">
                                                                            </div>
                                                                        </div>                                                          
                                                                    </div>
                                                            <div class="col-md-8">
                                                                <p class="white-color progress-bar-title"></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" id="spareTable" order-of-rows="desc" order-of-column="1" role="grid">
                                                        <thead>
                                                            <tr role="row">
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="USER">NAME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">DATE<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="TIME">CREATED BY<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="TIME">BARCODE<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="STATUS">ASSET<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                            </div>
                                        </div>
                                        <!-- /.table -->
                                    </div>
                                </div>
                            </div>
							</div>
                            <div class="tab-pane fade <%=keyactive%>"   id="newkeys-tab">
                            <div class="tab-content" >
                                <div class="row mb-4x">
                                   <div class="col-md-2">
                                        <div class="row vertical-navigation vertical-components-show">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">
                                                    <li class="active" style="display:none;"><a href="#show-component" data-toggle="tab">All</a>
                                                    </li>
                                                </ul>
                                                <!-- /.nav -->
                                            </div>

                                        </div>
                                        <div class="row vertical-navigation new-events">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">
                                                    <li ><a href="#" data-target="#newAssetModal" data-toggle="modal" onclick="assetClick('KEY');" class="capitalize-text">NEW KEY</a>
                                                    </li>
                                                </ul>
                                                <!-- /.nav -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="row show-component component-number-keys">
                                            <div class="col-md-12">
                                                <div data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-8">
                                                        <h3 class="panel-title capitalize-text">Keys</h3>
                                                        <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div class="progress" style="display:none;">
                                                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="display:none;width: 0px">
                                                                            </div>
                                                                        </div>                                                          
                                                                    </div>
                                                            <div class="col-md-8">
                                                                <p class="white-color progress-bar-title"></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" id="newkeysTable" role="grid">
                                                        <thead>
                                                            <tr role="row">
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="USER">NAME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">DATE<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="TIME">CREATED BY<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="TIME">LOCATION<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="STATUS">STATUS<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                            </div>
                                        </div>
                                        <!-- /.table -->

                                    </div>
                                </div>
                            </div>
							</div>
                            <div class="tab-pane fade" id="asset-tab">
                               <div class="tab-content">
                                <div class="row mb-4x">
                                    <div class="col-md-2">
                                        <div class="row vertical-navigation vertical-components-show">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">
                                                    <li class="active"><a href="#show-component" data-toggle="tab">All</a>
                                                    </li>
                                                    <li><a href="#component-assetcat" data-toggle="tab">Category</a>
                                                    </li>
                                                    <li><a href="#component-assetsubcat" data-toggle="tab">SubCategory</a>
                                                    </li> 
                                                    
                                                </ul>
                                                <!-- /.nav -->
                                            </div>

                                        </div>
                                        <div class="row vertical-navigation new-events">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">
                                                    <li style="display:<%=userinfoDisplay%>" ><a href="#" data-target="#newOffenceCategoryModal" data-toggle="modal" class="capitalize-text">+ NEW CATEGORY</a>
                                                    </li>
                                                    <li style="display:<%=userinfoDisplay%>" ><a href="#" data-target="#newOffenceModal" data-toggle="modal" class="capitalize-text">+ NEW SUBCATEGORY</a>
                                                    </li> 
                                                </ul>
                                                <!-- /.nav -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-10">
                                       <div class="row show-component component-assetcat">
                                            <div class="col-md-12">
                                                <div data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-8">
                                                        <h3 class="panel-title capitalize-text">CATEGORY</h3>
                                                                                                                        <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div class="progress" style="display:none;">
                                                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="display:none;width: 0px">
                                                                            </div>
                                                                        </div>                                                          
                                                                    </div>
                                                            <div class="col-md-8">
                                                                <p class="white-color progress-bar-title"></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" id="assetTable" role="grid">
                                                        <thead>
                                                            <tr role="row">
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="CATEGORY">CATEGORY<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="DATE">DATE<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="USER">CREATED BY<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                            </div>
                                        </div>
                                        <!-- /.table -->
                                       <div class="row show-component component-assetsubcat">
                                            <div class="col-md-12">
                                                <div data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-8">
                                                        <h3 class="panel-title capitalize-text">SUBCATEGORY</h3>
                                                                                                                        <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div class="progress" style="display:none;">
                                                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="display:none;width: 0px">
                                                                            </div>
                                                                        </div>                                                          
                                                                    </div>
                                                            <div class="col-md-8">
                                                                <p class="white-color progress-bar-title"></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" id="assetsubTable" role="grid">
                                                        <thead>
                                                            <tr role="row">
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="CATEGORY">NAME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="CATEGORY">CATEGORY<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="DATE">DATE<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="USER">CREATED BY<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                            </div>
                                        </div>
                                        <!-- /.table -->
                                        <div class="row show-component component-vehiclecolor">
                                            <div class="col-md-12" style="display:none;">
                                                <div data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-8">
                                                        <h3 class="panel-title capitalize-text">COLOR</h3>
                                                           <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div class="progress" style="display:none;">
                                                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="display:none;width: 0px">
                                                                            </div>
                                                                        </div>                                                          
                                                                    </div>
                                                            <div class="col-md-8">
                                                                <p class="white-color progress-bar-title"></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" id="vehiclecolorTable" role="grid">
                                                        <thead>
                                                            <tr role="row">
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">NAME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                            </div>
                                        </div>
                                        <div class="row show-component component-finderdep">
                                            <div class="col-md-12" style="display:none;">
                                                <div data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-8">
                                                        <h3 class="panel-title capitalize-text">FINDER DEPARTMENT</h3>
                                                           <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div class="progress" style="display:none;">
                                                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="display:none;width: 0px">
                                                                            </div>
                                                                        </div>                                                          
                                                                    </div>
                                                            <div class="col-md-8">
                                                                <p class="white-color progress-bar-title"></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" id="finderTable" role="grid">
                                                        <thead>
                                                            <tr role="row">
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">NAME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
							</div>
                            <div class="tab-pane fade" id="plans-tab">
                               <div class="tab-content">
                                <div class="row mb-4x">
                                    <div class="col-md-2">
                                        <div class="row vertical-navigation vertical-components-show">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">
 
                                                    
                                                </ul>
                                                <!-- /.nav -->
                                            </div>

                                        </div>
                                        <div class="row vertical-navigation new-events">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">
                                                    <li ><a href="#" data-target="#newAssetModal" data-toggle="modal" class="capitalize-text" onclick="newPlanClick()">+ NEW PLAN</a>
                                                    </li> 
                                                </ul>
                                                <!-- /.nav -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-10">
                                       <div class="row show-component component-plans">
                                            <div class="col-md-12">
                                                <div data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-8">
                                                        <h3 class="panel-title capitalize-text">ACTIVE PLANS</h3>
                                                                                                                        <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div class="progress" style="display:none;">
                                                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="display:none;width: 0px">
                                                                            </div>
                                                                        </div>                                                          
                                                                    </div>
                                                            <div class="col-md-8">
                                                                <p class="white-color progress-bar-title"></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" id="plansTable" role="grid">
                                                        <thead>
                                                            <tr role="row">
                                                                 <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="USER">NAME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">DATE<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="TIME">CREATED BY<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="TIME">START<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="STATUS">END<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                            </div>
                                        </div>
                                        <!-- /.table --> 

                                         <div class="row show-component component-tempplans">
                                            <div class="col-md-12">
                                                <div data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-8">
                                                        <h3 class="panel-title capitalize-text">TEMPLATE PLANS</h3>
                                                                                                                        <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div class="progress" style="display:none;">
                                                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="display:none;width: 0px">
                                                                            </div>
                                                                        </div>                                                          
                                                                    </div>
                                                            <div class="col-md-8">
                                                                <p class="white-color progress-bar-title"></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" id="tempplansTable" role="grid">
                                                        <thead>
                                                            <tr role="row">
                                                                 <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="USER">NAME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">DATE<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="TIME">CREATED BY<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="TIME">START<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="STATUS">END<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                            </div>
                                        </div>
                                        <!-- /.table --> 
                                    </div>
                                </div>
                            </div>
							</div>
                                

                            <div class="tab-pane fade" id="user-profile-tab">
                                <div class="tab-content">
                                <div class="row mb-4x">
                                    <div class="col-md-2">
                                        <div class="row vertical-navigation vertical-components-show">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">
                                                </ul>
                                                <!-- /.nav -->
                                            </div>
                                        </div>
                                        <div class="row vertical-navigation new-events">
                                            <div class="panel-control">
                                                <ul class="nav nav-tabs nav-contrast-dark">

                                                </ul>
                                                <!-- /.nav -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 pr-1x">
                                        <img id="userprofileImgSrc" src="" class="user-profile-image"/>
                                        <div class="gray-background user-info">
                                            <div class="container-block">
                                                <span class="circle-point-container"><span id="userStatusIconSpan" class="circle-point circle-point-green"></span></span>
                                                <p id="userStatusSpan"></p>
                                            </div>
                                            <div  class="container-block">
                                                <a onclick="clearPWBox();" href="#changePasswordModal" data-toggle="modal" ><i class="fa fa-lock red-color"></i>Change Password</a>
                                            </div> 
                                        </div> 
                                    </div>
                                    <div class="col-md-7 pl-1x">
                                        <div class="panel-heading no-hpadding">
                                            <div class="row">
                                                <div class="col-md-12" id="userFullnameSpanDIV">
                                                    <h2 class="panel-title red-color large-font" id="userFullnameSpan"></h2>
                                                </div> 
                                                 <div class="col-md-12" style="display:none;" id="userFullnameSpanEditDIV">
                                                     <div class="col-md-6">
                                                    <input id="userFirstnameSpan" class="inline-block form-control" />
                                                    </div>
                                                   <div class="col-md-6">
                                                   <input id="userLastnameSpan" class="inline-block form-control" />  
                                                   </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-body no-hpadding">                                                        
                                            <div class="row border-bottom">
                                                <div class="col-md-6">
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="profileUserNameSpanDIV">
                                                            <i class="fa fa-user red-color mr-3x"></i>
                                                            <p class="inline-block" id="profileUserNameSpan">
                                                            </p>                                                                
                                                        </div> 
                                                    </div>
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="profilePhoneNumberDIV"> 
                                                            <i class="fa fa-phone red-color mr-3x"></i><p class="inline-block" id="profilePhoneNumber"></p>                       
                                                        </div>
                                                        <div class="col-md-12"  style="display:none;" id="profilePhoneNumberEditDIV">
                                                            <i class="fa fa-phone red-color mr-3x" ></i>
                                                            <input style="width:88%;margin-top:-9px;" id="profilePhoneNumberEdit" class="inline-block form-control" /> 
                                                        </div>
                                                    </div>
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="profileEmailAddDIV">
                                                            <i class="fa fa-envelope red-color mr-3x"></i>
                                                            <p class="inline-block" id="profileEmailAdd">
                                                            </p>                                                                
                                                        </div> 
                                                        <div class="col-md-12" style="display:none;" id="profileEmailAddEditDIV">
                                                            <i class="fa fa-envelope red-color mr-3x"></i>
                                                            <input id="profileEmailAddEdit"  style="width:87%;margin-top:-8px;" class="inline-block form-control" />                   
                                                        </div>
                                                    </div>           
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="profileEmployeeAddDIV">
                                                            <i class="fa fa-credit-card red-color mr-3x"></i>
                                                            <p class="inline-block" id="profileEmployeeId">
                                                            </p>                                                                    
                                                        </div>
                                                        <div class="col-md-12" style="display:none;" id="profileEmployeeEditDIV"> 
                                                            <i class="fa fa-credit-card red-color mr-3x"></i>
                                                            <input id="profileEmployeeAddEdit"  style="width:87%;margin-top:-8px;" class="inline-block form-control" />                   
                                                        </div>
                                                    </div>                                         
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12">
                                                            <i class="fa fa-map-marker red-color mr-3x"></i>
                                                            <p class="inline-block" id="profileLastLocation">
                                                            </p>                                                                    
                                                        </div>
                                                    </div>                                                  
                                                </div>
                                                <div class="col-md-6">
													<div class="row mb-4x">
													 <div class="col-md-12" id="defaultDeviceType1">
                                                            <p class="font-bold red-color no-margin">
                                                                Site Name
                                                            </p>
                                                            <a class="inline-block" id="userSiteDisplay" onclick="siteListShow()">                                                            
                                                            </a> 
                                                             <label style="display:none;margin-bottom:10px;" id="siteSelectorDIV" class="select select-o">
                                                                <select id="siteSelector" runat="server">
                                                                </select>
                                                             </label>                                                                            
                                                        </div>
													</div>
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" style="margin-top:-20px;">
                                                            <p class="font-bold red-color no-vmargin">
                                                                Role
                                                            </p>
                                                            <p id="profileRoleName">
                                                            </p>                                                   
                                                        </div>
                                                    </div>
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="superviserInfoDIV" style="margin-top:-20px;">
                                                            <p class="font-bold red-color no-vmargin" id="supervisorTypeSpan">
                                                            </p>
                                                            <p id="profileManagerName">
                                                            </p>                                                        
                                                        </div>
                                                        <div class="col-md-12" id="managerInfoDIV" style="display:none;">
                                                            <p class="font-bold red-color no-vmargin" >Manager</p>
                                                   		 <label  class="select select-o">
                                                            <select id="editmanagerpickerSelect"  runat="server">
                                                            </select>
															</label>
                                                        </div>
                                                        <div class="col-md-12" id="dirInfoDIV" style="display:none;">
                                                            <p class="font-bold red-color no-vmargin" >Director</p>
                                                           <label  class="select select-o">
                                                            <select id="editdirpickerSelect" runat="server">
                                                            </select>
															</label>
                                                        </div>
                                                    </div>
                                                    <div class="row mb-4x">
                                                        <div class="col-md-12" id="defaultDeviceType" style="margin-top:-20px;">
                                                            <p class="font-bold red-color no-vmargin">
                                                                Device Type
                                                            </p>
                                                            <div class="container-block" id="deviceTypesDiv">
                                                            </div>                                                   
                                                        </div>
                                                        <div class="form-group" id="editDeviceType" style="display:none">
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <h3 class="capitalize-text no-margin">DEVICE</h3>
                                                                </div>
                                                                <div class="col-md-4">
                                                                  <div style="margin-top:7px" class="nice-checkbox inline-block no-vmargin">
                                                                    <input type="checkbox" id="editMobileCheck" name="niceCheck">
                                                                    <label for="editMobileCheck">Mobile</label>
                                                                  </div><!--/nice-checkbox-->                                               
                                                                </div>
                                                                <div class="col-md-4" style="display:none;">
                                                                  <div style="margin-top:7px" class="nice-checkbox inline-block no-vmargin">
                                                                    <input type="checkbox" id="editClientCheck" name="niceCheck"> 
                                                                    <label for="editClientCheck">Client</label>
                                                                  </div><!--/nice-checkbox-->                                                   
                                                                </div>                                                  
                                                            </div>
                                                        </div>
                                                    </div>                 
                                                    <div class="row mb-4x">  
                                                        <div class="col-md-12" id="defaultGenderDiv"  style="margin-top:-20px;">
                                                            <p class="font-bold red-color no-vmargin">
                                                                Gender
                                                            </p>
                                                            <div class="container-block" id="profileGender">
                                                            </div>                                                   
                                                        </div>
                                                    </div>                                       
                                                </div>                                              
                                            </div>
                                        </div>
                                        <div class="panel-heading no-hpadding">
                                            <div class="row" id="containerDiv" style="display:none;">
                                                <div class="col-md-12">
                                                    <div class="panel-control">
                                                        <ul class="nav nav-tabs nav-contrast-red" ">
                                                            <li class="active" ><a href="#userLoc-tab" data-toggle="tab" class="capitalize-text">LOCATION</a>
                                                            </li>
                                                            <li ><a href="#userGroup-tab" data-toggle="tab" class="capitalize-text">GROUP</a>
                                                            </li>	
                                                            <li ><a href="#userActivity-tab" data-toggle="tab" class="capitalize-text">ACTIVITY</a>
                                                            </li>						
                                                        </ul>
                                                        <!-- /.nav -->
                                                   </div>
                                                    <div class="row" style="height:20px;">

                                                    </div>
                                                   <div class="row">
									                    <div class="col-md-12">
										                    <div class="tab-pane fade active in" id="userLoc-tab">
                                                                <div id="usermap_canvas" style="width:100%;height:378px;"></div>
                                                            </div>
                                                            <div class="tab-pane fade" id="userGroup-tab">
                                                                 <div class="drop-elements" id="userGroupList">                                                  
                                                                </div>
                                                            </div>
                                                            <div class="tab-pane fade" id="userActivity-tab">

                                                                <div class="col-md-10">
                                                               <div data-fill-color="true" class="panel fade in panel-default panel-fill" data-init-panel="true">
                                                                    <div class="panel-heading">
                                                                        <h3 class="panel-title">RECENT ACTIVITY</h3>
                                                                    </div>
                                                                    <div class="panel-body">
                                                                            <div id="divrecentUserActivity" data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:263px">												
                                                    
                                                                            </div>
                                                                            <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
                                                                            <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>
                                                
                                                                    </div>
                                                                    <!-- /.panel-body -->
                                                                </div>
                                                            </div>
                                                                                                                                <div class="col-md-2">
                                                                    </div>
                                                                </div>
                                                        </div>                               
                                                </div>
                                            </div>
                                        </div>
                                            <div class="row" id="containerDiv2">
                                                <div class="col-md-12">
                                          <div class="panel panel-red" data-context="success">
                                             <div class="panel-heading">
                                                <h3 class="panel-title">ACCOUNT INFORMATION</h3>
                                             </div>
                                             <!-- /.panel-heading -->
                                             <div class="panel-body">
                                                <div class="row mb-2x" style="margin-top:10px;">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">TOTAL:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mobileTotal" readonly="readonly">
                                                         </div>
                                                      </div>
                                                </div>

                                                <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">REMAINING:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mobileRemaining" readonly="readonly">
                                                         </div>
                                                      </div>
                                                </div>
                                                <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">USED:</h3>
                                                      </div>
                                                      <div class="col-md-9">
                                                         <div class="form-group relative movable-swticher mt-1x checkbox-label-switcher">
                                                             <input class="form-control padding-switcher" id="mobileUsed" readonly="readonly">
                                                              </div>
                                                      </div>
                                                </div>    
                                                 <div class="row mb-2x">
                                                      <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">TIME ZONE:</h3>
                                                      </div>
                                                      <div class="col-md-8">
                                  			<label class="select select-o" >
                                                                                   <select id="countrySelect" class="selectpicker form-control"  data-live-search="true">
                                                
<option>Country</option>
<option value="Afghanistan ">Afghanistan (+4:00)</option>
<option value="Albania ">Albania (+1:00)</option>
<option value="Algeria ">Algeria (+1:00)</option>
<option value="Andorra ">Andorra (+1:00)</option>
<option value="Angola ">Angola (+1:00)</option>
<option value="Antigua & Deps ">Antigua & Deps (-4:00)</option>
<option value="Argentina ">Argentina (-3:00)</option>
<option value="Armenia ">Armenia (+4:00)</option> 
<option value="Australia ">Australia (+10:00)</option>      
                                                                      
<option value="Austria ">Austria (+1:00)</option>
<option value="Azerbaijan ">Azerbaijan (+4:00)</option>
<option value="Bahamas ">Bahamas (-5:00)</option>
<option value="Bahrain ">Bahrain (+3:00)</option>
<option value="Bangladesh ">Bangladesh (+6:00)</option>
<option value="Barbados ">Barbados (−04:00)</option>
<option value="Belarus ">Belarus (+03:00) </option>
<option value="Belgium ">Belgium (+01:00) </option>
<option value="Belize ">Belize (−06:00)</option>
<option value="Benin ">Benin (+01:00)</option>
<option value="Bhutan ">Bhutan(+06:00)</option>
<option value="Bolivia ">Bolivia (−04:00)</option>
<option value="Bosnia Herzegovina ">Bosnia Herzegovina (+01:00)</option>
<option value="Botswana ">Botswana(+02:00)</option>
<option value="Brazil ">Brazil(−02:00)</option>
<option value="Brunei ">Brunei (+08:00)</option>
<option value="Bulgaria ">Bulgaria (+02:00)</option>
<option value="Burkina ">Burkina (+02:00)</option>
<option value="Burundi ">Burundi (+02:00)</option>
<option value="Cambodia ">Cambodia (+07:00)</option>
<option value="Cameroon ">Cameroon (+01:00)</option>
<option value="Canada ">Canada (−05:00)</option>
<option value="Cape Verde ">Cape Verde (−01:00)</option>
<option value="Central African Rep ">Central African Rep (+01:00)</option>
<option value="Chad ">Chad (+01:00)</option>
<option value="Chile ">Chile (−04:00)</option>
<option value="China ">China (+08:00)</option>
<option value="Colombia ">Colombia (−05:00)</option>
<option value="Comoros ">Comoros (+03:00)</option>
<option value="Congo ">Congo (+01:00)</option>
<option value="Costa Rica ">Costa Rica (−06:00)</option>
<option value="Croatia ">Croatia (+01:00)</option>
<option value="Cuba ">Cuba (−05:00)</option>
<option value="Cyprus ">Cyprus (+02:00)</option>
<option value="Czech Republic ">Czech Republic (+01:00)</option>
<option value="Denmark ">Denmark (+01:00)</option>
<option value="Djibouti ">Djibouti (+03:00)</option>
<option value="Dominica ">Dominica (−04:00)</option>
<option value="Dominican Republic ">Dominican Republic (−04:00)</option>
<option value="East Timor ">East Timor (+09:00)</option>
<option value="Ecuador ">Ecuador (−05:00)</option>
<option value="Egypt ">Egypt (+02:00)</option>
<option value="El Salvador ">El Salvador (−06:00)</option>
<option value="Equatorial Guinea ">Equatorial Guinea (+01:00)</option>
<option value="Eritrea ">Eritrea (+03:00)</option>
<option value="Estonia ">Estonia (+02:00)</option>
<option value="Ethiopia ">Ethiopia (+03:00)</option>
<option value="Fiji ">Fiji (+12:00)</option>
<option value="Finland ">Finland (+02:00)</option>
<option value="France ">France (+01:00)</option>
<option value="Gabon ">Gabon (+01:00)</option>
<option value="Gambia ">Gambia (+00:00)</option>
<option value="Georgia ">Georgia (+04:00)</option>
<option value="Germany ">Germany (+01:00)</option>
<option value="Ghana ">Ghana (+00:00)</option>
<option value="Greece ">Greece (+02:00)</option>
<option value="Grenada ">Grenada (−04:00)</option>
<option value="Guatemala ">Guatemala (−06:00)</option>
<option value="Guinea ">Guinea (+00:00)</option>
<option value="Guinea-Bissau ">Guinea-Bissau (+00:00)</option>
<option value="Guyana ">Guyana (−04:00)</option>
<option value="Haiti ">Haiti (−05:00)</option>
<option value="Honduras ">Honduras (−06:00)</option>
<option value="Hong Kong ">Hong Kong(+08:00)</option>
<option value="Hungary ">Hungary (+01:00)</option>
<option value="Iceland ">Iceland (+00:00)</option>
<option value="India ">India (+05:00)</option>
<option value="Indonesia ">Indonesia (+07:00)</option>
<option value="Iran">Iran (+03:00)</option>
<option value="Iraq">Iraq (+03:00)</option>
<option value="Ireland {Republic} ">Ireland {Republic} (+00:00)</option>
<option value="Israel ">Israel (+02:00)</option>
<option value="Italy ">Italy (+01:00)</option>
<option value="Jamaica ">Jamaica (−05:00)</option>
<option value="Japan ">Japan (+09:00)</option>
<option value="Jordan ">Jordan (+02:00)</option>
<option value="Kazakhstan ">Kazakhstan (+06:00)</option>
<option value="Kenya ">Kenya (+03:00)</option>
<option value="Kiribati ">Kiribati (+12:00)</option>
<option value="Korea North ">Korea North (+08:00)</option>
<option value="Korea South ">Korea South (+09:00)</option>
<option value="Kosovo ">Kosovo (+01:00)</option>
<option value="Kuwait ">Kuwait (+03:00)</option>
<option value="Kyrgyzstan ">Kyrgyzstan (+06:00)</option>
<option value="Laos ">Laos (+07:00)</option>
<option value="Latvia ">Latvia (+02:00)</option>
<option value="Lebanon ">Lebanon (+02:00)</option>
<option value="Lesotho ">Lesotho (+02:00)</option>
<option value="Liberia ">Liberia (+00:00)</option>
<option value="Libya ">Libya (+02:00)</option>
<option value="Liechtenstein ">Liechtenstein (+01:00)</option>
<option value="Lithuania ">Lithuania (02:00)</option>
<option value="Luxembourg ">Luxembourg (+01:00)</option>
<option value="Macedonia ">Macedonia (+01:00)</option>
<option value="Madagascar ">Madagascar (+03:00)</option>
<option value="Malawi ">Malawi (+02:00)</option>
<option value="Malaysia ">Malaysia (+08:00)</option>
<option value="Maldives ">Maldives (+05:00)</option>
<option value="Mali ">Mali (+00:00)</option>
<option value="Malta ">Malta (+01:00)</option>
<option value="Marshall Islands ">Marshall Islands (+12:00)</option>
<option value="Mauritania ">Mauritania (+00:00)</option>
<option value="Mauritius ">Mauritius (+04:00)</option>
<option value="Mexico ">Mexico (−06:00 )</option>
<option value="Moldova ">Moldova (+02:00)</option>
<option value="Monaco ">Monaco (+01:00)</option>
<option value="Mongolia ">Mongolia (+08:00)</option>
<option value="Montenegro ">Montenegro(+01:00)</option>
<option value="Morocco ">Morocco (+00:00)</option>
<option value="Mozambique ">Mozambique (+02:00)</option>
<option value="Myanmar ">Myanmar (+06:00)</option>
<option value="Namibia ">Namibia (+01:00)</option>
<option value="Nauru ">Nauru (+12:00)</option>
<option value="Nepal ">Nepal (+06:00 )</option>
<option value="Netherlands ">Netherlands (+01:00)</option>
<option value="ew Zealand ">New Zealand (+12:00)</option>
<option value="Nicaragua ">Nicaragua (−06:00)</option>
<option value="Niger ">Niger (+01:00)</option>
<option value="Nigeria ">Nigeria (+01:00)</option>
<option value="Norway ">Norway (+01:00)</option>
<option value="Oman ">Oman (04:00)</option>
<option value="Pakistan ">Pakistan (+05:00)</option>
<option value="Palau ">Palau (+09:00)</option>
<option value="Panama ">Panama (−05:00)</option>
<option value="Papua New Guinea ">Papua New Guinea (+10:00)</option>
<option value="Paraguay ">Paraguay (−04:00)</option>
<option value="Peru ">Peru (−05:00)</option>
<option value="Philippines ">Philippines (+08:00)</option>
<option value="Poland ">Poland (+01:00)</option>
<option value="Portugal ">Portugal (+00:00)</option>
<option value="Qatar ">Qatar (+03:00)</option>
<option value="Romania ">Romania (+02:00)</option>
<option value="Russian Federation ">Russian Federation (+03:00)</option>
<option value="Rwanda ">Rwanda (+02:00)</option>
<option value="St Kitts & Nevis ">St Kitts & Nevis (04:00)</option>
<option value="St Lucia ">St Lucia (−04:00)</option>
<option value="Saint Vincent & the Grenadines ">Saint Vincent & the Grenadines (−04:00)</option>
<option value="Samoa ">Samoa (+13:00)</option>
<option value="San Marino ">San Marino (+01:00)</option>
<option value="Saudi Arabia ">Saudi Arabia (03:00)</option>
<option value="Senegal ">Senegal (+00:00)</option>
<option value="Serbia ">Serbia (+01:00)</option>
<option value="Seychelles ">Seychelles (+04:00 )</option>
<option value="Sierra Leone ">Sierra Leone (+00:00)</option>
<option value="Singapore ">Singapore (+08:00)</option>
<option value="Slovakia ">Slovakia (+01:00)</option>
<option value="Slovenia">Slovenia (+01:00)</option>
<option value="Solomon Islands ">Solomon Islands (+11:00)</option>
<option value="Somalia ">Somalia (+03:00)</option>
<option value="South Africa ">South Africa (+02:00)</option>
<option value="South Sudan ">South Sudan (+03:00)</option>
<option value="Spain ">Spain (+00:00)</option>
<option value="Sri Lanka ">Sri Lanka (+05:00)</option>
<option value="Sudan ">Sudan (+03:00)</option>
<option value="Suriname ">Suriname (−03:00)</option>
<option value="Swaziland ">Swaziland (+02:00)</option>
<option value="Sweden ">Sweden (+01:00)</option>
<option value="Switzerland ">Switzerland (+01:00)</option>
<option value="Syria ">Syria (+02:00)</option>
<option value="Taiwan ">Taiwan (+08:00)</option>
<option value="Tajikistan ">Tajikistan (+05:00)</option>
<option value="Tanzania ">Tanzania (03:00)</option>
<option value="Thailand ">Thailand (+07:00)</option>
<option value="Togo ">Togo (+00:00)</option>
<option value="Tonga ">Tonga (+13:00)</option>
<option value="Trinidad & Tobago ">Trinidad & Tobago (04:00)</option>
<option value="Tunisia ">Tunisia (+01:00)</option>
<option value="Turkey ">Turkey (+03:00)</option>
<option value="Turkmenistan ">Turkmenistan (+05:00)</option>
<option value="Tuvalu ">Tuvalu (+12:00)</option>
<option value="Uganda ">Uganda (+03:00)</option>
<option value="Ukraine ">Ukraine (+02:00</option>
<option value="United Arab Emirates ">United Arab Emirates (+04:00)</option>
<option value="United Kingdom ">United Kingdom (+00:00)</option>
<option value="United States ">United States (-05:00) </option>
<option value="Uruguay ">Uruguay (−03:00)</option>
<option value="Uzbekistan ">Uzbekistan (+05:00)</option>
<option value="Vanuatu ">Vanuatu (+11:00)</option>
<option value="Vatican City ">Vatican City (+01:00)</option>
<option value="Venezuela ">Venezuela (−04:00)</option>
<option value="Vietnam ">Vietnam (+07:00)</option>
<option value="Yemen ">Yemen (+03:00)</option>
<option value="Zambia ">Zambia (+02:00)</option>
<option value="Zimbabwe ">Zimbabwe (+02:00 )</option>
											 
											
											</select>
										 </label>
                                                      </div>
<div class="col-md-1" style="
    margin-top: 6px;
    margin-left: -12px;
">
                                                         <a onclick="saveTZ();" href="#"><i class="fa fa-save fa-2x " style="
    color: lightgray;
"></i></a>
                                                      </div>
                                                </div>     
                                                            <div class="row mb-2x" style="margin-top:20px;">
                                                     <div class="col-md-3">
                                                         <h3 class="panel-title capitalize-text">MODULES:</h3>
                                                     </div>
                                                                      <div class="col-md-9">
                                                                                                     <div class="row">
                                                <div class="col-md-4" >    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="activityCheck" name="niceCheck">
                                            <label for="activityCheck">Activity</label>
                                          </div><!--/nice-checkbox-->   
                                          </div> 
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="notificationCheck" name="niceCheck">
                                            <label for="notificationCheck">M.Board</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="locationCheck" name="niceCheck">
                                            <label for="locationCheck">Contract</label>
                                          </div><!--/nice-checkbox-->
                                            </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">                                              
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="ticketingCheck" name="niceCheck">
                                            <label for="ticketingCheck">Ticketing</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="taskCheck" name="niceCheck">
                                            <label for="taskCheck">Task</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="incidentCheck" name="niceCheck">
                                            <label for="incidentCheck">Incident</label>
                                          </div><!--/nice-checkbox-->
                                            </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">                                              
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="warehouseCheck" name="niceCheck">
                                            <label for="warehouseCheck">Warehouse</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="chatCheck" name="niceCheck">
                                            <label for="chatCheck">Chat</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                                   <div class="col-md-4">                                              
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="surveillanceCheck" name="niceCheck">
                                            <label for="surveillanceCheck">Surveillance</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">                                              
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="lfCheck" name="niceCheck">
                                            <label for="lfCheck">Lost&Found</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="dutyrosterCheck" name="niceCheck">
                                            <label for="dutyrosterCheck">Duty Roster</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="postorderCheck" name="niceCheck">
                                            <label for="postorderCheck">Post Order</label>
                                          </div><!--/nice-checkbox-->
                                            </div>
                                            </div>
                                            <div class="row">
                                                
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="requestCheck" name="niceCheck">
                                            <label for="requestCheck">Request</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                         <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="dispatchCheck" name="niceCheck">
                                            <label for="dispatchCheck">Dispatch</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                                                                                        
                                            </div>
                                                         <div class="row" style="display:none;">
                                            <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="collaborationCheck" name="niceCheck">
                                            <label for="collaborationCheck">Collaboration</label>
                                          </div><!--/nice-checkbox-->
                                            </div>
                                                             <div class="col-md-4">                                              
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" disabled id="verificationCheck" name="niceCheck">
                                            <label for="verificationCheck">Verification</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                                         </div>
                                                     </div>
                                                 </div>                                                                                   
                                             </div>
                                             <!-- /.panel-body -->
                                          </div>
                                          <!-- /.panel -->
                                       </div>
                                            </div>
                                        <div class="panel-body no-hpadding">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                                </div>
                        </div>
                    </div>
                    <!-- /tab-content -->
                </div>
                <!-- /panel-body -->
            </div>
            <!-- /.panel -->
            <div aria-hidden="true" aria-labelledby="newOffenceModal" role="dialog" tabindex="-1" id="newOffenceModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">				  
					<div class="modal-header">
					  <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
					  <h4 class="modal-title capitalize-text">CREATE NEW SUBCATEGORY</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-12">
										<input placeholder="Name" id="tbName" class="form-control">
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<label class="select select-o">
											<select id="typeSelect" runat="server">
											</select>
										 </label>
									</div>
								</div>				
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<div class="row horizontal-navigation">
							<div class="panel-control">
								<ul class="nav nav-tabs">
<%--									<li class="active"><a href="#" class="capitalize-text" data-target="#newDocument2" data-toggle="modal" onclick="$('#newDocument').modal('hide')">Next</a>
									</li>--%>
                                    <li ><a href="#" onclick="addOffenceTypeSave()" class="capitalize-text" >CREATE</a>
									</li>
								</ul>
								<!-- /.nav -->
							</div>
						</div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>
             <div aria-hidden="true" aria-labelledby="generateBcodeModal" role="dialog" tabindex="-1" id="generateBcodeModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">				  
					<div class="modal-header">
					  <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
					  <h4 class="modal-title capitalize-text">CREATE BARCODE</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-12">
										<input placeholder="BARCODE" id="tbGenerateBarcode" class="form-control">
									</div>
								</div>
                                                                                                            <div class="row text-center">
                                                                              <img id="barcode2" onclick="newWindow = window.open('');newWindow.document.write(barcode2.outerHTML);
                                                                                newWindow.print();"></img>

                                                                            </div>			
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<div class="row horizontal-navigation">
							<div class="panel-control">
								<ul class="nav nav-tabs">
                                    <li ><a href="#" data-dismiss="modal" class="capitalize-text" >CLOSE</a>
									</li>
                                    <li ><a href="#" onclick="newGenerateBarcode()" class="capitalize-text" >GENERATE</a>
									</li>
								</ul>
								<!-- /.nav -->
							</div>
						</div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>
            <div aria-hidden="true" aria-labelledby="newOffenceCategoryModal" role="dialog" tabindex="-1" id="newOffenceCategoryModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">				  
					<div class="modal-header">
					  <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
					  <h4 class="modal-title capitalize-text">CREATE NEW CATEGORY</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-12">
										<input placeholder="Name" id="tbCategoryName" class="form-control">
									</div>
								</div>		
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<div class="row horizontal-navigation">
							<div class="panel-control">
								<ul class="nav nav-tabs">
<%--									<li class="active"><a href="#" class="capitalize-text" data-target="#newDocument2" data-toggle="modal" onclick="$('#newDocument').modal('hide')">Next</a>
									</li>--%>
                                    <li ><a href="#"  onclick="addOffenceCatTypeSave()" class="capitalize-text" >CREATE</a>
									</li>
								</ul>
								<!-- /.nav -->
							</div>
						</div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>	
		    <div aria-hidden="true" aria-labelledby="editOffenceModal" role="dialog" tabindex="-1" id="editOffenceModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">				  
					<div class="modal-header">
					  <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
					  <h4 class="modal-title capitalize-text">EDIT SUBCATEGORY</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-12">
										<input placeholder="Name" id="tbEditOffenceName" class="form-control">
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<label class="select select-o">
											<select id="typeEditSelect" runat="server">
											</select>
										 </label>
									</div>
								</div>				
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<div class="row horizontal-navigation">
							<div class="panel-control">
								<ul class="nav nav-tabs">
<%--									<li class="active"><a href="#" class="capitalize-text" data-target="#newDocument2" data-toggle="modal" onclick="$('#newDocument').modal('hide')">Next</a>
									</li>--%>
                                    <li ><a href="#"  onclick="editOffenceTypeSave()" class="capitalize-text" >SAVE</a>
									</li>
								</ul>
								<!-- /.nav -->
							</div>
						</div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>
            <div aria-hidden="true" aria-labelledby="EditOffenceCategoryModal" role="dialog" tabindex="-1" id="EditOffenceCategoryModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">				  
					<div class="modal-header">
					  <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
					  <h4 class="modal-title capitalize-text">EDIT CATEGORY</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-12">
										<input placeholder="Name" id="tbEditCategoryName" class="form-control">
									</div>
								</div>		
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<div class="row horizontal-navigation">
							<div class="panel-control">
								<ul class="nav nav-tabs">
<%--									<li class="active"><a href="#" class="capitalize-text" data-target="#newDocument2" data-toggle="modal" onclick="$('#newDocument').modal('hide')">Next</a>
									</li>--%>
                                    <li ><a href="#"  onclick="editOffenceCatTypeSave()" class="capitalize-text" >SAVE</a>
									</li>
								</ul>
								<!-- /.nav -->
							</div>
						</div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>
<div aria-hidden="true" aria-labelledby="taskDocument" role="dialog" tabindex="-1" id="taskDocument" class="modal fade videoModal" style="display: none;">   
				<div class="modal-dialog modal-lg">
				  <div class="modal-content">
					<div class="modal-header">
					  
					  <div class="row">
						<div class="col-md-11">
							<span class="circle-point-container pull-left mt-2x mr-1x"><span id="taskheaderImageClass" class="circle-point circle-point-orange"></span></span>
							<h4 class="modal-title capitalize-text" id="taskincidentNameHeader"></h4>
						</div>
						<div class="col-md-1" id="liapVCardX" style="display:none;">
							<button aria-hidden="true" data-dismiss="modal" data-target="#assetplanViewCard" onclick="rowchoicePlan(document.getElementById('AssetPlanID').value)" data-toggle="modal" class="close" type="button" ><i class="icon_close fa-lg"></i></button>
						</div>		
                        <div class="col-md-1" id="liaVCardX" style="display:none;">
							<button aria-hidden="true" data-dismiss="modal" data-target="#ticketingViewCard" onclick="rowchoice(document.getElementById('rowidChoice').value)" data-toggle="modal" class="close" type="button" ><i class="icon_close fa-lg"></i></button>
						</div>	
					  </div>
					  <div class="row">
						<div class="col-md-3">
							<p>Status: <span id="taskstatusSpan"></span></p>
						</div>	
						<div class="col-md-3">
							<p>Created by: <span id="taskusernameSpan"></span></p>
						</div>	
						<div class="col-md-3">
							<p>Assigned to: <span id="tasktypeSpan"></span></p>
						</div>		
                                   <div class="col-md-3">
							<p>Task Type: <span id="ttypeSpan"></span></p>
						</div>						
					  </div>
					  <div class="row">
						<div class="col-md-3">
							<p>Location: <span id="tasklocSpan"></span></p>
						</div>	
						<div class="col-md-3">
							<p>Created on: <span id="tasktimeSpan"></span></p>
						</div>	
                         <div class="col-md-3">
							<p>Assigned on: <span id="assignedTimeSpan"></span></p>
						</div>
                          <div class="col-md-3">
							<p id="incidentItemsList"></p>
						</div>									
					  </div>				
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-5" style="border-right: 1px #bbbbbb solid;">
								<div class="panel-control">
                                        <ul class="nav nav-tabs nav-contrast-red">
                                            <li class="active" id="taskliInfo"><a href="#taskinfo-tab" data-toggle="tab" class="capitalize-text">INFO</a>
                                            </li>
                                            <li id="taskliNotes"><a href="#notes-tab" data-toggle="tab" class="capitalize-text">NOTES</a>
                                            </li>
                                            <li id="taskliActi"><a href="#taskactivity-tab" data-toggle="tab" onclick="tracebackOn('task')" class="capitalize-text">ACTIVITY</a>
                                            </li>
                                            <li id="taskliAtta"><a href="#taskattachments-tab" data-toggle="tab" class="capitalize-text">ATTACHMENTS</a>
                                            </li>											
                                        </ul>
                                        <!-- /.nav -->
                                   </div>
									
								<div class="row">
									<div class="col-md-12">
                                        <div class="tab-content">
										<div class="tab-pane fade active in" id="taskinfo-tab">
											<div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;">	
											<div class="row mb-2x">
												<div class="col-md-12">
													<p class="red-color"><b>Description:</b></p>
													<p id="taskdescriptionSpan"></p>
												</div>
                                                <div class="col-md-12" id="dvCustomerNameSpan">
													<p class="red-color"><b>Account Name:</b></p>
													<p id="CustomerNameSpan"></p>
												</div>
                                                <div class="col-md-12" id="dvContractNameSpan">
													<p class="red-color"><b>Contract Name:</b></p>
													<p id="ContractNameSpan"></p>
												</div>
                                                 <div class="col-md-12" id="dvProjectNameSpan">
													<p class="red-color"><b>Project Name:</b></p>
													<p id="ProjectNameSpan"></p>
												</div>
                                                <div class="col-md-12">
													<p class="red-color"><b>Checklist Name : </b><b id="checklistnameSpan"></b><i id="checklistnamespanFA" style="margin-left:5px;" class="fa fa-check-square-o"></i></p>
                                                    <ul id="taskchecklistItemsList" style="list-style-type: none;">

                                                    </ul>
												</div>
											</div>		
											<div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
											<div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>																					
										</div>
										</div>
                                        <div class="tab-pane fade" id="notes-tab">
											<div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;">	
											<div class="row mb-2x" style="display:none;">
												<div class="col-md-12">
													<p class="red-color"><b>Notes:</b></p>
													<p id="taskinstructionSpan"></p>
												</div>
											</div>
                                                <div class="row mb-2x">
												<div class="col-md-12">
													<p class="red-color"><b>Notes:</b></p>
                                                    <div id="taskRemarksList" >
												    </div>
												</div>
											</div>	
											<div class="row">
												<div class="col-md-12">
													<p class="red-color"><b>Checklist Notes:</b></p>
													<p id="checklistNotesSpan"></p>
												</div>
											</div>	
                                           <div id="pchecklistItemsListNotes" class="row">
												<div class="col-md-12">
													<p  class="red-color"><b>Unchecked Items:</b></p>
													 <ul id="checklistItemsListNotes" style="list-style-type: none;">

                                                    </ul>
												</div>
											</div>	
                                            <div id="pCanvasNotes" class="row">
												<div class="col-md-12">
													<p  class="red-color"><b>Canvas Notes:</b></p>
													 <ul id="canvasItemsListNotes" style="list-style-type: none;">

                                                    </ul>
												</div>
											</div>	
											<div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
											<div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>																					
										</div>
										</div>
										<div class="tab-pane fade" id="taskactivity-tab">
                                                    <div id="taskdivIncidentHistoryActivity" data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;">
												    </div>
                                                    <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
                                                    <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>										
										</div>
										<div class="tab-pane fade" id="taskattachments-tab" onclick="startRot();nextbackTask();">	
                                              <div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;">
                                            <div id="taskattachments-info-tab">

                                            </div>
                                            <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
											<div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>																					
                                            </div>								
										</div>
							            </div>
									</div>
								</div>
							</div>

                            
                            <div class="col-md-1"  style="width:40px;">

                                <i id="taskrotationDIV1" style="
    margin-top: 180px;
    margin-left: 5px;color:#bbbbbb" class="fa fa-angle-double-left  fa-2x" onclick="taskbackImg()"></i>

                            </div>

							<div class="col-md-5" id="taskdivAttachmentHolder" style="width:440px;margin-top:4px;border-right: 1px #bbbbbb solid;border-left: 1px #bbbbbb solid;">
                                <div class="tab-pane fade" id="tremarks-tab">
                                                                            								<div class="panel-control">
                                        <ul class="nav nav-tabs nav-contrast-red">
                                            <li><a href="#tasklocation-tab" data-toggle="tab" onclick="hideAllRemarks()" class="capitalize-text">BACK</a>
                                            </li> 										
                                        </ul>
                                        <!-- /.nav -->
                                   </div>
                                                    <div id="taskRemarksList2"  data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;">
												    </div>
                                                    <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
                                                    <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>										
										</div> 
								<div class="tab-pane fade active in" id="tasklocation-tab">
                                    <div id="taskmap_canvasIncidentLocation" style="width:100%;height:380px;"></div>
									<div id="taskdivAttachment" class="overlapping-map-image">
									</div>
								</div>		
                                <div class="tab-pane fade" id="taskrejection-tab">
	                                      <p class="red-color text-center"><b>Rejection Notes:</b></p>
									        <div class="col-md-12" style="height:350px;">
										        <textarea placeholder="Rejection Notes" id="taskrejectionTextarea" class="form-control" rows="12"></textarea>
									        </div>
                                </div>									
							</div>

                                                         <div class="col-md-1"  style="width:40px;">
                                 <i id="taskrotationDIV2" class="fa fa-angle-double-right  fa-2x" style="
    margin-top: 180px;
    margin-left: 5px;color:#bbbbbb;"  onclick="tasknextImg()"></i>

                             </div>
						</div>
 
                        <div class="row">
                                                        <div class="col-md-5">

                            </div>
                                    <div class="col-md-7" style="display:none;border-left:0px;" id="taskAudioDIV"> 
                                        <audio id="taskAudio" style="width: 100%;" width="100%" controls><source id="taskAudioSrc" type="audio/mpeg"/></audio>
                                    </div>
                        </div>
					</div>
					<div class="modal-footer">
                                               <div class="row" id="rowtasktracebackUser" style="display:none;">
                        <div class="col-md-5">
                            </div>
                               <div class="col-md-7" style="text-align:left;margin-top:-8px;">
 <a style="font-size:16px;" href="#" id="playpausefilter" onclick="playpauseclick()"><i class='fa fa-play-circle'></i>PLAY</a>
                                        <div style="margin-top:10px;" class="row horizontal-navigation">
                                                <a style="font-size:16px;" href="#" onclick="tracebackOnFilter(1,'task')"><i id="taskfilter1" class="fa fa-square-o"></i>1 MIN</a>
                                                |<a style="font-size:16px;" href="#" onclick="tracebackOnFilter(5,'task')"><i id="taskfilter2" class="fa fa-square-o"></i>5 MIN</a>
									            |<a style="font-size:16px;" href="#" onclick="tracebackOnFilter(30,'task')"><i id="taskfilter3" class="fa fa-square-o"></i>30 MIN</a>
									            |<a style="font-size:16px;" href="#" onclick="tracebackOnFilter(60,'task')"><i id="taskfilter4" class="fa fa-square-o"></i>1 HOUR</a>
						                </div>
                            </div>
                        </div>
						<div class="row horizontal-navigation">
							<div class="panel-control">
                                <ul class="nav nav-tabs" id="taskinitialOptionsDiv">
									<li id="liapVCard" style="display:none;"><a href="#" data-dismiss="modal" data-target="#assetplanViewCard" onclick="rowchoicePlan(document.getElementById('AssetPlanID').value)"  data-toggle="modal">CLOSE</a>
									</li>
                                    <li id="liaVCard" style="display:none;"><a href="#" data-dismiss="modal" data-target="#ticketingViewCard" onclick="rowchoice(document.getElementById('rowidChoice').value)"  data-toggle="modal">CLOSE</a>
									</li>
								</ul>
								<ul class="nav nav-tabs" id="taskhandleOptionsDiv" style="display:none;">
                                    <li><a href="#" data-dismiss="modal" data-target="#assetplanViewCard" onclick="rowchoicePlan(document.getElementById('AssetPlanID').value)"  data-toggle="modal">CLOSE</a>
									</li>		
                                    <li><a href="#">ACCEPT</a>
									</li>
									<li><a href="#" data-target="#taskrejection-tab" data-toggle="tab"  onclick="rejectionSelect();">REJECT</a>
									</li>							
								</ul>
                                <ul class="nav nav-tabs" id="taskrejectOptionsDiv" style="display:none;">
                                    <li><a href="#" data-dismiss="modal" data-target="#assetplanViewCard" onclick="rowchoicePlan(document.getElementById('AssetPlanID').value)"  data-toggle="modal">CLOSE</a>
									</li>
                                    <li><a href="#">ASSIGN</a>
									</li>
									<li><a href="#">REASSIGN</a>
									</li>
									<li><a href="#">SAVE</a>
									</li>
								</ul>
								<!-- /.nav -->
							</div>
						</div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>	
			<div aria-hidden="true" aria-labelledby="ticketingViewCard" role="dialog" tabindex="-1" id="ticketingViewCard" class="modal fade videoModal" style="display: none;">
				<div class="modal-dialog modal-lgx">
				  <div class="modal-content">
					<div class="modal-header">
					  <div class="row">
						<div class="col-md-11">
							<span class="circle-point-container pull-left mt-2x mr-1x"><span id="headerImageClass" class="circle-point circle-point-orange"></span></span>
							<h4 class="modal-title capitalize-text" id="incidentNameHeader">ITEM</h4>
						</div>
						<div class="col-md-1">
							<button aria-hidden="true" data-dismiss="modal" class="close" type="button" onclick="cancelCheckOut()"><i class="icon_close fa-lg" ></i></button>
						</div>						
					  </div>
					  <div class="row">
						<div class="col-md-4">
							<p>Created by: <span id="usernameSpan"></span></p> 
						</div>		
                        <div class="col-md-4">
							<p>Site: <span id="locSpan"></span></p>
						</div>	
						<div class="col-md-4">
							<p>Created on: <span id="timeSpan"></span></p>
						</div>				
					  </div>			
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-4">
								<div class="panel-control">
                                        <ul class="nav nav-tabs nav-contrast-red" ">
                                            <li class="active" id="liInfo"><a href="#info-tab" data-toggle="tab" class="capitalize-text">INFO</a>
                                            </li>
                                            <li id="liPlan"><a href="#viewplans-tab" data-toggle="tab" class="capitalize-text">DETAILS</a>
                                            </li>	 
                                            <li id="liRem"><a href="#remarks-tab" data-toggle="tab" class="capitalize-text">REMARKS</a>
                                            </li> 
                                            <li id="liAtta"><a href="#attachments-tab" data-toggle="tab" class="capitalize-text">ATTACH</a>
                                            </li>											
                                        </ul>
                                        <!-- /.nav -->
                                   </div>
									
								<div class="row">
									<div class="col-md-12">
                                        <div class="tab-content">
										<div class="tab-pane fade active in" id="info-tab">
											<div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;">	
												 <div class="col-md-6">
													<p class="red-color"><b>Item Name:</b></p>
													<p id="platenumberSpan"></p>
												</div>
                                                <div class="col-md-6">
                                                <p class="red-color"><b>Item Description:</b></p>
													<p id="platesourceSpan"></p>
												</div>
                                                
                                                <div class="col-md-6">
                                                <p class="red-color"><b>Barcode:</b></p>
													<p id="itembarcodeSpan"></p>
												</div> 
                                                <div class="col-md-6">
                                                <p class="red-color"><b>Serial No:</b></p>
													<p id="itemserialnoSpan"></p>
												</div> 
                                                <div class="col-md-6">
                                                <p class="red-color"><b>Make:</b></p>
													<p id="itemmakeSpan"></p>
												</div>
                                                
                                                <div class="col-md-6">
                                                <p class="red-color"><b>Model:</b></p>
													<p id="itemmodelSpan"></p>
												</div> 
                                                <div class="col-md-6">
                                                <p class="red-color"><b>Item Category:</b></p>
													<p id="plateCodeSpan"></p>
												</div>
                                                <div class="col-md-6">
                                                <p class="red-color"><b>Item Sub Category:</b></p>
													<p id="itemsubCategorySpan"></p>
												</div> 
                                                <div class="col-md-6">
                                            	<p class="red-color"><b>Main Location:</b></p>
													<p id="mainlocationSpan"></p>
												</div>
                                                <div class="col-md-6">
                                            	<p class="red-color"><b>Sub Location:</b></p>
													<p id="vehicleMakeSpan"></p>
												</div> 
                                                <div class="col-md-6">
                                            	<p class="red-color"><b>Account:</b></p>
													<p id="asssetaccountSpan"></p>
												</div>
                                                <div class="col-md-6">
                                            	<p class="red-color"><b>Project:</b></p>
													<p id="asssetprojectSpan"></p>
												</div> 
                                                <div class="col-md-6" id="quantitySpandiv" style="display:none;">
                                            	<p class="red-color"><b>Quantity:</b></p>
													<p id="quantitySpan"></p>
												</div> 
                                                <div class="col-md-6" id="parentnameSpandiv" style="display:none;">
                                            	<p class="red-color"><b>Asset Name:</b></p>
													<p id="parentnameSpan"></p>
												</div> 
											    <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
											    <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>																					
										    </div>
                                        </div>

                                        <div class="tab-pane fade" id="viewplans-tab">
                                            <div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:160px;">	
												 <div class="col-md-12">
                                               <p id="viewplansDIV" class="red-color"><b>Maintenance Plans: </b></p>
                                                    <ul id="assetplansList" style="list-style-type: none;">
                                                    </ul>
												</div>
                                                
                                                 
											    <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
											    <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>																					
										    </div> 
                                            <div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:160px;">	
												 <div class="col-md-12">
                                               <p id="viewpartsDIV" class="red-color"><b>Spare Part Items : </b></p>
                                                    <ul id="sparepartsList" style="list-style-type: none;">
                                                    </ul>
												</div>
                                                
                                                 
											    <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
											    <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>																					
										    </div>
                                        </div>

                                               

                                            <div class="tab-pane fade" id="remarks-tab">	
                                             <div id="assetRemarksList"  data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;">
												    </div>
                                                    <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
                                                    <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>								
										</div>

										<div class="tab-pane fade" id="attachments-tab">	
                                            <div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;">
                                            <div id="attachments-info-tab">

                                            </div>
                                            <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
											<div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>																					
                                            </div>							
										</div>		
                                            </div>								
									</div>
								</div>
							</div>
							<div class="col-md-8" id="divAttachmentHolder" style="display:none;height:420px;" >
								<div class="tab-pane fade active in" id="location-tab">
                                    <div id="map_canvas" style="width:100%;height:415px;"></div>
									<div id="divAttachment" class="overlapping-map-image">
									</div>
								</div>			
							</div> 
                            							<div class="col-md-8" id="divAssetPlanDetails" style="display:none;height:420px;">
								 	           <div class="panel-control">
                                        <ul class="nav nav-tabs nav-contrast-red">
                                            <li   ><a href="#" onclick="divassetplanbacktoMap()" class="capitalize-text">BACK</a> 
                                            </li>
                                             <li  class="active" ><a href="#assetplandetails-tab" data-toggle="tab" class="capitalize-text">PLAN DETAILS</a> 
                                            </li>
                                            <li   ><a href="#assetplansched-tab" data-toggle="tab" class="capitalize-text">SCHEDULES</a> 
                                            </li>
                                            <li   ><a href="#assetplantasks-tab" data-toggle="tab" class="capitalize-text">TASKS</a> 
                                            </li>
                                        </ul>
                                        <!-- /.nav -->
                                   </div>
                                        <div class="tab-content">
 
                                        <div class="tab-pane fade active in" id="assetplandetails-tab">
  	<div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;margin-top:10px;">	
												<div class="col-md-4">
                                                <p class="red-color"><b>Plan Type:</b></p>
													<p id="viewplanType2"></p>
												</div> 
                                                <div class="col-md-4">
                                                <p class="red-color"><b>Plan Name:</b></p>
													<p id="viewplanName2"></p>
												</div>
                                                <div class="col-md-4">
                                                <p class="red-color"><b>Plan Description:</b></p>
													<p id="viewplanDesc2"></p>
												</div>
                                                <div class="col-md-4">
                                                <p class="red-color"><b>Type:</b></p>
													<p id="viewplanTType2"></p>
												</div>
                                                 <div class="col-md-4">
                                                <p class="red-color"><b>Frequency</b></p>
													<p id="viewplanTiType2"></p>
												</div>
          <div class="col-md-4">
                                                <p class="red-color"><b>Work Order Time:</b></p>
													<p id="viewplanPTime2"></p>
												</div>
                                                <div class="col-md-4">
                                                <p class="red-color"><b>Start Date:</b></p>
													<p id="viewplanSDate2"></p>
												</div>

                                                <div class="col-md-4">
                                                <p class="red-color"><b>End Date:</b></p>
													<p id="viewplanEDate2"></p>
												</div>

                                                

                                                <div class="col-md-4">
                                                <p class="red-color"><b>Checklist:</b></p>
													<p id="viewplanCHK2"></p>
												</div>

                                                <div class="col-md-4">
                                                <p class="red-color"><b>Priority:</b></p>
													<p id="viewplanPrio2"></p>
												</div>

                                                <div class="col-md-4">
                                                <p class="red-color"><b>Assignee Type:</b></p>
													<p id="viewplanAType2"></p>
												</div>

                                                <div class="col-md-4">
                                                <p class="red-color"><b>Assignee:</b></p>
													<p id="viewplanAssignee2"></p>
												</div>

                                                <div class="col-md-4">
                                                <p class="red-color"><b>Signature:</b></p>
													<p id="viewplanSig2"></p>
												</div>

											    <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
											    <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>																					
										    </div>
						    
										</div>

                                        <div class="tab-pane fade" id="assetplansched-tab">
 <div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;margin-top:10px;">	
												 <div class="col-md-12">
                                               <p id="assetplanscheduleDIV2" class="red-color"><b>Scheduled : </b></p>
                                                    <ul id="assetscheduleList2" style="list-style-type: none;">
                                                    </ul>
												</div>
                                                
                                                 
											    <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
											    <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>																					
										    </div>
						    
										</div>

                                        <div class="tab-pane fade" id="assetplantasks-tab">
   <div style="margin-top:10px;" data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-4">
                                                        <h3 class="panel-title capitalize-text">TASKS</h3>
                                                    </div>
                                                    <div class="col-md-4 mt-3x">
															<div class="form-group">
															   <div class="input-group input-group-in transparent-bg"> 
																	<span class="input-group-addon white-color"><i class="fa fa-calendar"></i></span>      
																  <input placeholder="Pick a date" class="form-control white-color" data-input="daterangepicker" id="assetplantaskDatepicker2" onchange="pickdateassetplanTask2()" data-show-dropdowns="true" data-single-date-picker="true">
															   </div>
															   <!-- /input-group-in -->
															</div>
													   </div>
                                                    <div class="col-md-4 mt-2x" style="padding-bottom:10px;">
                                                        <input type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" id="assetplanTaskTable2" role="grid" number-of-rows="3">
                                                        <thead>
                                                            <tr role="row">
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">ID<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                  <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">NAME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                   <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">STATUS<i class="fa fa-sort ml-2x"></i>
                                                                </th> 
                                                                 <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">TASK TYPE<i class="fa fa-sort ml-2x"></i>
                                                                </th> 
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
						    
										</div>

							            </div>
							</div> 
                                                        							<div class="col-md-8" id="divAssetSpareDetails" style="display:none;height:420px;">
								 	           <div class="panel-control">
                                        <ul class="nav nav-tabs nav-contrast-red">
                                            <li   ><a href="#" onclick="divassetplanbacktoMap()" class="capitalize-text">BACK</a> 
                                            </li>
                                             <li  class="active" ><a href="#assetsparedetails-tab" data-toggle="tab" class="capitalize-text">PART DETAILS</a> 
                                            </li>
                                            <li   ><a href="#assetspareremarks-tab" data-toggle="tab" class="capitalize-text">REMARKS</a> 
                                            </li> 
                                        </ul>
                                        <!-- /.nav -->
                                   </div>
                                        <div class="tab-content">
 
                                        <div class="tab-pane fade active in" id="assetsparedetails-tab">
   				<div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;margin-top:10px;">	
												 <div class="col-md-6">
													<p class="red-color"><b>Item Name:</b></p>
													<p id="platenumberSpan2"></p>
												</div>
                                                <div class="col-md-6">
                                                <p class="red-color"><b>Item Description:</b></p>
													<p id="platesourceSpan2"></p>
												</div>
                                                
                                                <div class="col-md-6">
                                                <p class="red-color"><b>Barcode:</b></p>
													<p id="itembarcodeSpan2"></p>
												</div>
                                                
                                                <div class="col-md-6">
                                                <p class="red-color"><b>Serial No:</b></p>
													<p id="itemserialnoSpan2"></p>
												</div> 
                       <div class="col-md-6">
                                                <p class="red-color"><b>Make:</b></p>
													<p id="itemmakeSpan2"></p>
												</div>
                                                
                                                <div class="col-md-6">
                                                <p class="red-color"><b>Model:</b></p>
													<p id="itemmodelSpan2"></p>
												</div> 
                                                <div class="col-md-6">
                                                <p class="red-color"><b>Item Category:</b></p>
													<p id="plateCodeSpan2"></p>
												</div>
                                                <div class="col-md-6">
                                                <p class="red-color"><b>Item Sub Category:</b></p>
													<p id="itemsubCategorySpan2"></p>
												</div> 
                                                 
                                                <div class="col-md-6" id="quantitySpan2div" style="display:none;">
                                            	<p class="red-color"><b>Quantity:</b></p>
													<p id="quantitySpan2"></p>
												</div> 
											    <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
											    <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>																					
										    </div>
						    
										</div>

                                        <div class="tab-pane fade" id="assetspareremarks-tab">
   <div id="assetspareRemarksList"  data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;">
												    </div>
                                                    <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
                                                    <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>								
										
						    
										</div> 

							            </div>
							</div> 
                            

						</div>
					</div>
					<div class="modal-footer">
						<div class="row horizontal-navigation" id="checkoutDivIni">
							<div class="panel-control">
								<ul class="nav nav-tabs">
                                   
									<li><a href="#" data-dismiss="modal">CLOSE</a>
									</li>
									<li id="checkoutLi" style="display:none;"><a href="#" onclick="checkoutItem()">CHECKOUT</a>
									</li>	
                                    <li id="checkinLi" style="display:none;"><a href="#" onclick="checkinItem()">CHECKIN</a>
									</li>
                                    <li id="liaddPlan" style="display:none;"> 
                                        <a href="#" data-target="#newAssetModal" data-toggle="modal" data-dismiss="modal" onclick="loadnewplan();newPlanClick(document.getElementById('rowidChoice').value);">ADD PLAN</a>
                                    </li>	
                                     <li id="liaddPart" style="display:none;"> 
                                        <a href="#" data-target="#newSpareModal" data-toggle="modal" data-dismiss="modal" onclick="loadnewpart();clearnewSpare();">ADD PART</a>
                                    </li>	
                                    <li> 
                                        <a href="#" onclick="document.getElementById('checkitemDZ').click()">ATTACH</a>
                                    </li>		
                                    <li>
                                    <a href="#" onclick="addNotesToItem()">ADD REMARKS</a>
                                    </li>	
								</ul>
								<!-- /.nav -->
							</div>
                             <textarea placeholder="Add Remarks" style="display:none;" id="additemNotesTA" class="form-control" rows="3"></textarea>
						</div>
                        <div class="row horizontal-navigation" id="checkoutDivPro" style="display:none;">
							<div class="panel-control">
								<ul class="nav nav-tabs"> 
									<li><a href="#" onclick="cancelCheckOut()">CANCEL</a>
									</li>
									<li id="checkoutLiPro"><a href="#" onclick="processItem('out')">PROCESS</a>
									</li>	
                                    <li  id="checkinLiPro"><a href="#" onclick="processItem('in')">PROCESS</a>
									</li>			
								</ul>
								<!-- /.nav -->
							</div>
						</div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>
            
                
			<div aria-hidden="true" aria-labelledby="assetplanViewCard" role="dialog" tabindex="-1" id="assetplanViewCard" class="modal fade videoModal" style="display: none;">
				<div class="modal-dialog modal-lgx">	 
				  <div class="modal-content">
					<div class="modal-header">
					  <div class="row">
						<div class="col-md-11">
							<span class="circle-point-container pull-left mt-2x mr-1x"><span class="circle-point circle-point-orange"></span></span>
							<h4 class="modal-title capitalize-text" id="planNameHeader">MAINTENANCE PLAN</h4>
						</div>
						<div class="col-md-1">
							<button aria-hidden="true" data-dismiss="modal" class="close" type="button" ><i class="icon_close fa-lg" ></i></button>
						</div>						
					  </div>
					  <div class="row">
						<div class="col-md-4">
							<p>Created by: <span id="planCBSpan"></span></p> 
						</div>		
                        <div class="col-md-4">
							<p>Link to Asset: <span id="planSiteSpan"></span></p>
						</div>	
						<div class="col-md-4">
							<p>Created on: <span id="planCreatedOnSpan"></span></p>
						</div>				
					  </div>			
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-4">
								<div class="panel-control">
                                        <ul class="nav nav-tabs nav-contrast-red" ">
                                            <li class="active" id="pliInfo"><a href="#pinfo-tab" data-toggle="tab" class="capitalize-text">INFO</a>
                                            </li>
                                            <li id="pliTasks" ><a href="#ptasks-tab" data-toggle="tab" class="capitalize-text">SCHEDULED</a>
                                            </li>	 									
                                        </ul>
                                        <!-- /.nav -->
                                   </div>
								<div class="row">
									<div class="col-md-12">
                                      
										<div class="tab-pane fade active in" id="pinfo-tab">
 	<div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;">	
												<div class="col-md-6">
                                                <p class="red-color"><b>Plan Type:</b></p>
													<p id="viewplanType"></p>
												</div> 
                                                <div class="col-md-6">
                                                <p class="red-color"><b>Plan Name:</b></p>
													<p id="viewplanName"></p>
												</div>
                                                <div class="col-md-6">
                                                <p class="red-color"><b>Plan Description:</b></p>
													<p id="viewplanDesc"></p>
												</div>
                                                <div class="col-md-6">
                                                <p class="red-color"><b>Type:</b></p>
													<p id="viewplanTType"></p>
												</div>
                                                 <div class="col-md-6">
                                                <p class="red-color"><b>Frequency</b></p>
													<p id="viewplanTiType"></p>
												</div>

                                                <div class="col-md-6">
                                                <p class="red-color"><b>Start Date:</b></p>
													<p id="viewplanSDate"></p>
												</div>

                                                <div class="col-md-6">
                                                <p class="red-color"><b>End Date:</b></p>
													<p id="viewplanEDate"></p>
												</div>

                                                <div class="col-md-6">
                                                <p class="red-color"><b>Work Order Time:</b></p>
													<p id="viewplanPTime"></p>
												</div>

                                                <div class="col-md-6">
                                                <p class="red-color"><b>Checklist:</b></p>
													<p id="viewplanCHK"></p>
												</div>

                                                <div class="col-md-6">
                                                <p class="red-color"><b>Priority:</b></p>
													<p id="viewplanPrio"></p>
												</div>

                                                <div class="col-md-6">
                                                <p class="red-color"><b>Assignee Type:</b></p>
													<p id="viewplanAType"></p>
												</div>

                                                <div class="col-md-6">
                                                <p class="red-color"><b>Assignee:</b></p>
													<p id="viewplanAssignee"></p>
												</div>

                                                <div class="col-md-6">
                                                <p class="red-color"><b>Signature:</b></p>
													<p id="viewplanSig"></p>
												</div>

											    <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
											    <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>																					
										    </div>

                                        </div>

                                        <div class="tab-pane fade" id="ptasks-tab">
                                <div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;">	
												 <div class="col-md-12">
                                               <p id="assetplanscheduleDIV" class="red-color"><b>Scheduled : </b></p>
                                                    <ul id="assetscheduleList" style="list-style-type: none;">
                                                    </ul>
												</div>
                                                
                                                 
											    <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
											    <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>																					
										    </div>
                                        </div> 	
                                        </div>								
									</div>
								</div>
							
							<div class="col-md-8" style="height:420px;">
								 	           <div class="panel-control">
                                        <ul class="nav nav-tabs nav-contrast-red">
                                             <li  class="active" ><a href="#plantasklist-tab" data-toggle="tab" class="capitalize-text">TASK LIST</a> 
                                            </li>
                                        </ul>
                                        <!-- /.nav -->
                                   </div>
                                        <div class="tab-content">
 
                                        <div class="tab-pane fade active in" id="plantasklist-tab">
  <div style="margin-top:10px;" data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-4">
                                                        <h3 class="panel-title capitalize-text">TASKS</h3>
                                                    </div>
                                                    <div class="col-md-4 mt-3x">
															<div class="form-group">
															   <div class="input-group input-group-in transparent-bg"> 
																	<span class="input-group-addon white-color"><i class="fa fa-calendar"></i></span>      
																  <input placeholder="Pick a date" class="form-control white-color" data-input="daterangepicker" id="assetplantaskDatepicker" onchange="pickdateassetplanTask()" data-show-dropdowns="true" data-single-date-picker="true">
															   </div>
															   <!-- /input-group-in -->
															</div>
													   </div>
                                                    <div class="col-md-4 mt-2x" style="padding-bottom:10px;">
                                                        <input type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" id="assetplanTaskTable" role="grid" number-of-rows="3">
                                                        <thead>
                                                            <tr role="row">
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">ID<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                  <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">NAME<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                   <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">STATUS<i class="fa fa-sort ml-2x"></i>
                                                                </th> 
                                                                 <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">TASK TYPE<i class="fa fa-sort ml-2x"></i>
                                                                </th> 
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
						    
										</div>
							            </div>
							</div> 
                            </div>
						</div> 
					<div class="modal-footer">
						<div class="row horizontal-navigation" >
							<div class="panel-control">
								<ul class="nav nav-tabs">
                                    <li id="liplanBack" style="display:none;"><a href="#" onclick="jQuery('#ticketingViewCard').modal('show');" data-dismiss="modal">BACK</a>
									</li> 
									<li><a href="#" data-dismiss="modal">CLOSE</a>
									</li> 	
								</ul>
								<!-- /.nav -->
							</div>
						</div> 
					</div>
				  </div><!-- /.modal-content -->
                    </div>
				</div><!-- /.modal-dialog -->
		
                
            <div aria-hidden="true" aria-labelledby="successfulModal" role="dialog" tabindex="-1" id="successfulModal" class="modal fade" style="display: none;">
                <div class="modal-dialog modal-sm">
                  <div class="modal-content">
<%--                    <div class="modal-header">
                      <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                    </div>--%>
                    <div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        </div>
                        <div class="row">
                            <h2 class="text-center">GOOD JOB!</h2>
                        </div>
                        <div class="text-center row">
                            <img  src="https://testportalcdn.azureedge.net/Images/smileface.png"/>
                        </div>
                        <div class="row">
                            <h4 class="text-center" id="successMessage"></h4>
                        </div>
                        <div class="row">
                            <div class="horizontal-navigation ">
                                <div class="panel-control ">
                                    <ul class="nav nav-tabs text-center">
                                        <li><a href="#" data-dismiss="modal" onclick="closeModal();">CLOSE</a>
                                        </li>       
                                    </ul>
                                    <!-- /.nav -->
                                </div>
                            </div>
                        </div>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
             </div>
            <div aria-hidden="true" aria-labelledby="successfulDispatch" role="dialog" tabindex="-1" id="successfulDispatch" class="modal fade" style="display: none;">
                <div class="modal-dialog modal-sm">
                  <div class="modal-content">
<%--                    <div class="modal-header">
                      <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                    </div>--%>
                    <div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        </div>
                        <div class="row">
                            <h2 style="color:gray" class="text-center">GOOD JOB!</h2>
                        </div>
                        <div class="text-center row">
                            <img  src="https://testportalcdn.azureedge.net/Images/smileface.png"/>
                        </div>
                        <div class="row">
                            <h4 style="color:gray" class="text-center" id="successincidentScenario"></h4>
                        </div>
                        <div class="row">
                            <div class="horizontal-navigation ">
                                <div class="panel-control ">
                                    <ul class="nav nav-tabs text-center">
                                        <li><a href="#" data-dismiss="modal" onclick="location.reload(); showLoader();">CLOSE</a>
                                        </li>       
                                    </ul>
                                    <!-- /.nav -->
                                </div>
                            </div>
                        </div>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
             </div> 
            <div aria-hidden="true" aria-labelledby="changePasswordModal" role="dialog" tabindex="-1" id="changePasswordModal" class="modal fade" style="display: none;">
               <div class="modal-dialog modal-sm">
                  <div class="modal-content">
                     <div class="modal-header">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        <h4 class="modal-title capitalize-text">CHANGE PASSWORD</h4>
                     </div>
                     <div class="modal-body">
                        <form role="form">
                           <div class="row" style="display:none;">
                              <div class="col-md-12">
                                 <input class="form-control" placeholder="Old Password" id="oldPwInput"/>
                              </div>
                           </div>
                                                       <div class="row">
                              <div class="col-md-12">
                                 <input type="password" class="form-control" placeholder="New Password" id="newPwInput"/>
                              </div>
                           </div>
                                                       <div class="row">
                              <div class="col-md-12">
                                 <input type="password" class="form-control" placeholder="Confirm Password" id="confirmPwInput"/>
                              </div>
                           </div>
                            		                                                            <div id="pswd_info">
    <h4>Password must meet the following requirements:</h4>
    <ul>
        <li id="letter" class="invalid">At least <strong>one letter</strong></li>
        <li id="capital" class="invalid">At least <strong>one capital letter</strong></li>
        <li id="number" class="invalid">At least <strong>one number</strong></li>
        <li id="length" class="invalid">Be at least <strong>8 characters</strong></li>
    </ul>
</div>
                        </form>
                     </div>
                     <div class="modal-footer">
                        <div class="row horizontal-navigation">
                           <div class="panel-control">
                              <ul class="nav nav-tabs">
                                 <li><a href="#" data-dismiss="modal">CANCEL</a>
                                 </li>
                                 <li><a href="#" onclick="changePassword()" >SAVE</a>
                                 </li>
                              </ul>
                              <!-- /.nav -->
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- /.modal-content -->
               </div>
               <!-- /.modal-dialog -->
            </div> 
                            <div aria-hidden="true" aria-labelledby="newAssetModal" role="dialog" tabindex="-1" id="newAssetModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-lg">
				  <div class="modal-content">				  
					<div class="modal-header">
					  <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
					  <h4 class="modal-title capitalize-text" id="assetHeader">ADD ASSET</h4>
					</div>
					<div class="modal-body">
                        <div class="row" style="display:none;" id="aremarks-div">
                              <div class="col-md-6" >
                                    <p style="margin-left:5px;" class="red-color" >Remarks:</p>
                                    <textarea placeholder="Asset Remarks"  id="aremarks" class="form-control"rows="5"></textarea>
                              </div>
                              <div class="col-md-6" style="border-left: 1px solid #bbbbbb;">
                                  						                            <div id="dassetRemarksList"  data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:348px;">
												    </div>
                                                    <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
                                                    <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>		
                              </div>
                        </div>
                        <div class="row" style="display:none;" id="aspareparts-div">
                                      <div class="col-md-4" style="border-right: 1px solid #bbbbbb;">
                            <div class="row">
                                                                  	<div class="col-md-12">
                                        <p style="margin-left:5px;" class="red-color" id="partName">*Part Name:</p>
										<input placeholder="Part Name"  id="tbPartName" class="form-control">
									</div>
									
								</div>

                            <div class="row">
                                                                  	<div class="col-md-12">
                                        <p style="margin-left:5px;" class="red-color" id="partDesc">Part Description:</p>
										<input placeholder="Part Description"  id="tbPartDesc" class="form-control">
									</div>
									
								</div>
               

                            <div class="row">
                                                                  	<div class="col-md-12">
                                        <p style="margin-left:5px;" class="red-color" id="partMake">Make:</p>
										<input placeholder="Part Make"  id="tbPartMake" class="form-control">
									</div>
									
								</div>

                            <div class="row">
                                                                  	<div class="col-md-12">
                                        <p style="margin-left:5px;" class="red-color" id="partModel">Model:</p>
										<input placeholder="Part Model"  id="tbPartModel" class="form-control">
									</div>
									
								</div>
                                          <div class="row">
                                                                  	<div class="col-md-12">
                                        <p style="margin-left:5px;" class="red-color" id="partQT">Quantity:</p>
										<input placeholder="Part Quantity"  id="tbPartQT" class="form-control">
									</div>
									
								</div>
                                          </div>
                              <div class="col-md-4" style="border-right: 1px solid #bbbbbb;">
                                  <div class="row">
                                    <div class="col-md-12">
                                     <p style="margin-left:5px;" class="red-color">Category:</p>
                                    	<label class="select select-o">
											<select id="sparecategoryAssetSelect" onchange="spareassetCategorySelectChange(this)" runat="server">
											</select>
										 </label>
                                        </div>
								</div>
                                  <div class="row">
                                    <div class="col-md-12">
                                     <p style="margin-left:5px;" class="red-color">Sub Category:</p>
                                    	<label class="select select-o">
											<select id="sparecategorySubAssetSelect" >
											</select>
										 </label>
                                        </div>
								</div>
                                                <div class="row">
                                                                  	<div class="col-md-12">
                                        <p style="margin-left:5px;" class="red-color" id="partSerial">Serial #:</p>
										<input placeholder="Part Serial"  id="tbPartSerial" class="form-control">
									</div>
									
								</div>
                                          <div class="row">
                                    <p style="margin-left:5px;" class="red-color" >Barcode:</p>
                                                                  	<div class="col-md-7">
                                        
										<input placeholder="Barcode"  id="tbPartBarcode" class="form-control">
									</div>
									<div class="col-md-4" style="margin-top:3px;padding-bottom:0px;margin-bottom:-10px;">
                                        <div class="row horizontal-navigation" style="margin-bottom:0px;">
							<div class="panel-control">
								<ul class="nav nav-tabs">  
                                    <li class="active"><a  style="padding-top:5px;padding-bottom:5px;" href="#" class="capitalize-text" onclick="generateSparePartBarcode2()">GENERATE</a>
									</li> 
								</ul>
								<!-- /.nav -->
							</div>
						</div>
									</div>
								</div>
                                                                                        <div id="newbarcodeimgdiv2" class="row text-center" style="margin-top:-10px;">
                                                                              <img style="height:84px;" id="newbarcode2" onclick="newWindow = window.open('');newWindow.document.write(newbarcode2.outerHTML);
                                                                                newWindow.print();"></img>

                                                                            </div>                    
                              </div>

                            <div class="col-md-4">
                                <div class="row"> 
                                        <div class="panel-body">
                                       <p style="margin-left:5px;" class="red-color" >Part Image:</p>
                                          <form enctype="multipart/form-data" id="dz-spareasset" method="post" data-input="dropzone" class="dropzone dz-clickable" action="/file-upload">
                                            <div class="dz-message">
                                               <i class="fa fa-upload fa-2x gray-color"></i>
                                              <h1>DRAG & DROP</h1>
                                            </div>
                                          </form>
                                        </div>
                                    </div>
                                <div style="margin-top:-17px;margin-bottom:-14px;" class="row horizontal-navigation pull-right">
							
						        </div> 
                            </div>
                        </div>
                        <div class="row" style="display:none;" id="aplan-div">
                            <div class="panel-control">
                                        <ul class="nav nav-tabs nav-contrast-red" ">
                                            <li class="active"><a href="#pnewinfo-tab" data-toggle="tab" class="capitalize-text">INFO</a>
                                            </li>
                                            <li ><a href="#pnewschedule-tab" data-toggle="tab" class="capitalize-text">SCHEDULE</a>
                                            </li>	 									
                                        </ul>
                                        <!-- /.nav -->
                                   </div>
						<div class="row">
							<div class="col-md-12" >
                                                                         <div class="tab-content">
 
                                        <div class="tab-pane fade active in" id="pnewinfo-tab">
                                             <div class="row" id="plantempassetdiv"> 
                                <div class="col-md-6">
                                                                           <p style="margin-left:5px;" class="red-color" >Plan Template:</p>
                            <label class="select select-o" >
                                           <select id="plantempSelect" onchange="plantempSelectTypeOnChange(this)" runat="server">

											</select>
										 </label>
                                                                          </div> 
                                                                    <div class="col-md-6" id="displayplanassetDIV" style="display:none;">
                                     <p style="margin-left:5px;" class="red-color">Plan Asset:</p>
                                    	<select id="assetplanchartSelect" class="selectpicker form-control"  data-live-search="true" runat="server">
                                            </select>
                                        </div> 
                                </div>
                                <div class="row"> 
                                <div class="col-md-12">
                                                                           <p style="margin-left:5px;" class="red-color" >Plan Type:</p>
                            <label class="select select-o" >
                                           <select id="plantypeSelect" onchange="plantypeSelectTypeOnChange(this)">
											  <option>Time Based Service</option> 
                                              <option>Usage Based Service</option> 
											</select>
										 </label>
                                                                          </div> 
                                </div>
								<div class="row">
									<div class="col-md-6">
                                         <p style="margin-left:5px;" class="red-color" >*Plan Name:</p>
										<input placeholder="Plan Name" id="tbNewTaskName" class="form-control">
									</div>
                                    <div class="col-md-6">
                                             <p style="margin-left:5px;" class="red-color" >Type:</p>
                                        <label class="select select-o">
                                        
                                           <select id="taskTypeSelect" class="selectpicker form-control"  data-live-search="true" runat="server">
											</select>
										 </label>
                                    </div>
								</div>
								<div class="row">
									<div class="col-md-12">
                                         <p style="margin-left:5px;" class="red-color" >Plan Description:</p>
                                        <textarea placeholder="Plan Description" id="tbNewDescription" class="form-control" rows="5"></textarea>
									</div>
								</div>	
                                            </div>
                                                                             <div class="tab-pane fade" id="pnewschedule-tab">
                                                                                                                 <div class="row">
 



                      <div class="col-md-6">
                          <div class="row"><div class="col-md-12">
            <p class="font-bold red-color no-vmargin" style="margin-left:5px">Frequency:</p>
                                                                             
                                            <div class="row" style="margin-top:5px;"> 
                                                <div class="col-md-6">
                                                      <div class="nice-radio inline-block no-vmargin">
                                                        <input type="radio" checked="checked" id="schedRadio" onclick="schedRadioClick(this)"  name="niceRadio">
                                                        <label for="schedRadio">Scheduled</label>
                                                      </div><!--/nice-radio-->                                              
                                                </div>
                                                <div class="col-md-6"> 
                                                  <div class="nice-radio inline-block no-vmargin">
                                                    <input type="radio" id="recRadio" onclick="recRadioClick(this)" name="niceRadio">
                                                    <label for="recRadio">Recurrance</label>
                                                  </div><!--/nice-radio-->                                              
                                                </div>  
                                            </div> 
                  </div></div>
                                            
                          <div class="row"> <div class="col-md-6">
                                                 <p class="font-bold red-color no-vmargin" style="margin-left:5px">Start Date:</p>
                                             <div class="form-group">
                                          <div class="input-group input-group-in">

                                            <input onchange="dateDutyStartChange(this)" id="dateDutyStart" data-input="daterangepicker" data-single-date-picker="true" data-show-dropdowns="true" class="form-control" placeholder="Choose a start date">

                                            <span class="input-group-addon red-color"><i class="fa fa-calendar"></i></span>                                            
                                          </div><!-- /input-group-in -->
                                        </div><!--/form-group-->
                                                 </div>
                                             <div class="col-md-6">
                                                 <p class="font-bold red-color no-vmargin" >End Date:</p>
                                             <div class="form-group">
                                          <div class="input-group input-group-in"> 
                                            <input id="dateDutyEnd" onchange="dateDutyEndChange(this)" data-input="daterangepicker" data-single-date-picker="true" data-show-dropdowns="true" class="form-control" placeholder="Choose a end date">

                                            <span class="input-group-addon red-color"><i class="fa fa-calendar"></i></span>                                            
                                          </div><!-- /input-group-in -->
                                        </div><!--/form-group-->
                                                 </div></div>
                                            
                          <div class="row"> <div class="col-md-6">
                                             <p style="margin-left:5px;" class="red-color" >Work Order Time:</p>
                                                                                    <label class="select select-o">
                                         <select id="newtaskTime" >
                                              <option>00</option>
                                             <option>01</option>
                                             <option>02</option>
                                             <option>03</option>
                                             <option>04</option>
                                             <option>05</option>
                                             <option>06</option>
                                             <option>07</option>
                                             <option>08</option>
                                             <option>09</option>
                                             <option>10</option>
                                             <option>11</option>
                                             <option>12</option>
                                             <option>13</option>
                                             <option>14</option>
                                             <option>15</option>
                                             <option>16</option>
                                             <option>17</option>
                                             <option>18</option>
                                             <option>19</option>
                                             <option>20</option>
                                             <option>21</option>
                                             <option>22</option>
                                             <option>23</option>
                                            
                                         </select>
                                        </label>
                                        </div> 
                                        <div class="col-md-6" style="margin-top:20px;">
                                             <p style="margin-left:5px;" class="red-color" ></p>
                                                                                    <label class="select select-o">
                                         <select id="newtaskTimeMinute" >
                                                                                        <option>00</option>
                                             <option>01</option>
                                             <option>02</option>
                                             <option>03</option>
                                             <option>04</option>
                                             <option>05</option>
                                             <option>06</option>
                                             <option>07</option>
                                             <option>08</option>
                                             <option>09</option>
                                             <option>10</option>
                                             <option>11</option>
                                             <option>12</option>
                                             <option>13</option>
                                             <option>14</option>
                                             <option>15</option>
                                             <option>16</option>
                                             <option>17</option>
                                             <option>18</option>
                                             <option>19</option>
                                             <option>20</option>
                                             <option>21</option>
                                             <option>22</option>
                                             <option>23</option>
                                             <option>24</option>
                                             <option>25</option>
                                             <option>26</option>
                                             <option>27</option>
                                             <option>28</option>
                                             <option>29</option>
                                             <option>30</option>
                                             <option>31</option>
                                             <option>32</option>
                                             <option>33</option>
                                             <option>34</option>
                                             <option>35</option>
                                             <option>36</option>
                                             <option>37</option>
                                             <option>38</option>
                                             <option>39</option>
                                             <option>40</option>
                                             <option>41</option>
                                             <option>42</option>
                                             <option>43</option>
                                             <option>44</option>
                                             <option>45</option>
                                             <option>46</option>
                                             <option>47</option>
                                             <option>48</option>
                                             <option>49</option>
                                             <option>50</option>
                                             <option>51</option>
                                             <option>52</option>
                                             <option>53</option>
                                             <option>54</option>
                                             <option>55</option>
                                             <option>56</option>
                                             <option>57</option>
                                             <option>58</option>
                                             <option>59</option>
                                         </select>
                                        </label>
                                        </div></div>
                             
                          <div class="row">
									<div class="col-md-12">
                                        <p style="margin-left:5px;" class="red-color"> Priority:</p>
										<label class="select select-o">
                                           <select id="prioritySelect">
                                              <option><%=severePlaceholder%></option>
											  <option><%=highPlaceholder%></option>
											  <option><%=mediumPlaceholder%></option>
											  <option><%=lowPlaceholder%></option>
											</select>
										 </label>
									</div>
								</div>	

                                                       <div class='row' style="padding-top:0px">
                                    
                                                                                                      <div class="nice-checkbox inline-block no-vmargin">
                                                                    <input onclick="assignMySelf()" type="checkbox" id="myselfCheck" name="niceCheck">
                                                                    <label for="myselfCheck"><%=assigntomyselfPlaceholder%></label> 
                                                                                                          </div>
                                           </div> 
                                <div class="row" id="assigneeDIV">
                                    <p style="margin-left:5px;" class="red-color">*Assignee:</p>
									<div class="col-md-4">
										<label class="select select-o">
                                           <select id="taskSelectAssigneeType" onchange="taskselectAssigneeTypeChange()">
											  <option><%=userPlaceholder%></option>
											  <option style="display:<%=grpOptionView%>"><%=groupPlaceholder%></option>
											</select>
										 </label>
									</div>
									<div id="usersearchSelectDIV" class="col-md-8">
									<select id="usersearchSelect" class="selectpicker form-control"  data-live-search="true" runat="server">
                                    </select>
									</div>	

                                    <div id="groupsearchSelectDIV" style="display:none;" class="col-md-8">
                                        <select id="groupsearchSelect"  class="selectpicker form-control"  data-live-search="true" runat="server">
                                        </select>	
                                    </div>						
								</div>	
										
                          </div> 
                          <div class="col-md-6">
                                                <div id="plancalDIV">
                                                      <p style="margin-left:5px;" class="red-color" >Calendar:</p>
                                            <div id="dutycalendar3">
                <script type="text/template" id="dutyfull-clndr-template">
                  <div class="row">
                    <div class="col-md-12 no-hpadding gray-border">
                      <div class="clndr-ctrl btn-toolbar red-background">
                        <button class="btn btn-nofill btn-default pull-left" onclick="dateback2();return false;"><i class="icon-arrow-left fa fa-2x white-color"></i></button>
                        <button class="btn btn-nofill btn-default pull-right" onclick="dateforward2();return false;"><i class="icon-arrow-right white-color fa fa-2x"></i></button>
                        <p class="month lead text-center help-block mt-2x white-color" id="dutydateHeader"></p>
                      </div>
                      <table id="calendarTable" class="clndr-table" border="0" cellspacing="0" cellpadding="0">
                         <thead>
                        <tr class="header-days">
                            <td class="header-day">Sun</td>
                            <td class="header-day">Mon</td>
                            <td class="header-day">Tue</td>
                            <td class="header-day">Wed</td>
                            <td class="header-day">Thu</td>
                            <td class="header-day">Fri</td>
                            <td class="header-day">Sat</td>
                          </tr>
                        </thead>
                         <tbody>
                             <tr id="dutycalendarRow1">
                             </tr>
                             <tr id="dutycalendarRow2">
                             </tr>
                             <tr id="dutycalendarRow3">
                             </tr>
                             <tr id="dutycalendarRow4">
                             </tr>
                             <tr id="dutycalendarRow5">
                             </tr>
                        </tbody>
                      </table>
                    </div><!-- /.cols -->
                  </div><!-- /.row</script>
              </div><!-- /#calendar3 -->
                    </script>

</div>
                                        <div style="display:none;" id="planrecDIV"><!--/recurringSelectDIV-->
                                             <p style="margin-left:5px;" class="red-color" >Plan Recurring:</p>
									    <label class="select select-o">
                                           <select id="newrecurringSelect">
											  <option>Recurring</option>
											  <option>Daily</option>
											  <option>Weekly</option>
                                              <option>Monthly</option>
											</select>
										 </label>
									</div>	
                      <div class="row" style="margin-top:12px;">
									<div class="col-md-12">
                                        <p style="margin-left:5px;" class="red-color" >Checklist:</p>
										<label class="select select-o">
											<select id="checklistSelect" class="selectpicker form-control"  data-live-search="true" runat="server">
											</select>
										 </label>
									</div>
								</div>	
                                                     </div> 
                              

                          </div>                                                                                           

                                      
                                </div>
                                <div class='row' style="padding-top:0px">
                                    	<div class="col-md-6">
                                                                                                      <div class="nice-checkbox inline-block no-vmargin">
                                                                    <input type="checkbox" id="requiresignatureCheck" name="niceCheck">
                                                                    <label for="requiresignatureCheck"><%=requiresignaturePlaceholder%></label> 
                                                                                                          </div>
                                            </div>
                                    <div class="col-md-6">
                                        
                                    </div>

                                </div>
                                            </div>
                                                                             </div>
                               

                                					
							</div>
						</div>

						<div class="row"  id="adetails-div">
                            <div class="col-md-4">
                                <div class="row" style="display:<%=customersDisplay%>;">
									<div class="col-md-12">
                                           <p style="margin-left:5px;" class="red-color" >Account:</p>
                                        <label class="select select-o">
                                           <select id="customerslst" onchange="customersOnChange(this);">
											</select>
										 </label>

									</div>
								</div>
                                <div class="row">
									<div class="col-md-12">
                                          <p style="margin-left:5px;" class="red-color" >Project:</p>
                                        <label class="select select-o">
    <select id="projectlst" onchange="projectlstOnChange(this);"> 

                                               <option value="0" >Select Project</option>
											</select>
										 </label>

									</div>
                                        
								</div>                      
                                <div class="row">
                                                                  	<div class="col-md-12">
                                        <p style="margin-left:5px;" class="red-color" id="assetName">Item Name:</p>
										<input placeholder="Item Name"  id="tbAssetName" class="form-control">
									</div>
									
								</div>
                                <div class="row">
                                                                  	<div class="col-md-12">
                                        <p style="margin-left:5px;" class="red-color" id="assetCom">Item Comments:</p>
										<input placeholder="Item Description"  id="tbAssetComments" class="form-control">
									</div>
									
								</div>
                                <div class="row">
                                                                  	<div class="col-md-12">
                                        <p style="margin-left:5px;" class="red-color" >Make:</p>
										<input placeholder="Make"  id="tbAssetMake" class="form-control">
									</div>
									
								</div>       
							</div>

                            <div class="col-md-4">
                                 <div class="row">
                                                                  	<div class="col-md-12">
                                        <p style="margin-left:5px;" class="red-color" >Model:</p>
										<input placeholder="Model"  id="tbAssetModel" class="form-control">
									</div>
									
								</div>
                      <div class="row">
                          <div class="col-md-12">
                                        <p style="margin-left:5px;" class="red-color">Last Visit Date:</p>
		 										<div class="form-group">
                                          <div class="input-group input-group-in">

                                            <input id="lvdpicker" data-input="daterangepicker" data-single-date-picker="true" data-show-dropdowns="true" class="form-control" placeholder="Last Visit Date">

                                            <span class="input-group-addon red-color"><i class="fa fa-calendar"></i></span>                                            
                                          </div><!-- /input-group-in -->
                                        </div><!--/form-group-->
									</div>
                                                      
									
								</div>
                                <div class="row">
                                    <p style="margin-left:5px;" class="red-color" >Barcode:</p>
                                                                  	<div class="col-md-7">
                                        
										<input placeholder="Barcode"  id="tbAssetBarcode" class="form-control">
									</div>
									<div class="col-md-4" style="margin-top:3px;padding-bottom:0px;margin-bottom:-10px;">
                                        <div class="row horizontal-navigation" style="margin-bottom:0px;">
							<div class="panel-control">
								<ul class="nav nav-tabs">  
                                    <li class="active"><a  style="padding-top:5px;padding-bottom:5px;" href="#" class="capitalize-text" onclick="generateAssetBarcode()">GENERATE</a>
									</li> 
								</ul>
								<!-- /.nav -->
							</div>
						</div>
									</div>
								</div>
                                                                                        <div id="barcodeimgdiv" class="row text-center" style="margin-top:-10px;">
                                                                              <img style="height:84px;" id="barcode" onclick="newWindow = window.open('');newWindow.document.write(barcode.outerHTML);
                                                                                newWindow.print();"></img>

                                                                            </div>
                              <div class="row" style="margin-top:-4px;">
									            	<div class="col-md-12">
                                        <p style="margin-left:5px;" class="red-color" >Serial Number:</p>
										<input placeholder="Serial Number"  id="tbAssetSNumber" class="form-control">
									</div>
								</div>
								   
                            </div>

                            <div class="col-md-4" style="border-left:none">
                                <div class="row">
                                    <div class="col-md-12">
                                     <p style="margin-left:5px;" class="red-color">Category:</p>
                                    	<label class="select select-o">
											<select id="categoryAssetSelect" onchange="assetCategorySelectChange(this)" runat="server">
											</select>
										 </label>
                                        </div>
								</div>
                          <div class="row">
                                    <div class="col-md-12">
                                     <p style="margin-left:5px;" class="red-color">Sub Category:</p>
                                    	<label class="select select-o">
											<select id="categorySubAssetSelect" >
											</select>
										 </label>
                                        </div>
								</div>
								   <div class="row">
									<div class="col-md-12">
                                        <p style="margin-left:5px;" class="red-color">Main Location:</p>
										<label class="select select-o">
											<select id="categoryAssetMainLocation" onchange="assetitemLocationSelectChange(this)" runat="server">
											</select>
										 </label>
									</div>
								</div>
                               <div class="row">
									<div class="col-md-12">
                                        <p style="margin-left:5px;" class="red-color">Sub Location:</p>
										<label class="select select-o">
											<select id="categorySubMainLocation" > 
											</select>
										 </label>
									</div>
								</div>
                                   <div class="row">
                                                                  	<div class="col-md-12">
                                        <p style="margin-left:5px;" class="red-color" >Contractor:</p>
										<input placeholder="Contractor"  id="tbAssetContractor" class="form-control">
									</div>
									
								</div> 

                            </div>
						</div>
						<div class="row" style="margin-bottom:0px;display:none;" id="aattachments-div">
                            <div class="col-md-6" > 
                                        <div class="panel-body">
                                          <p style="margin-left:5px;" class="red-color" >Attachment:</p>
                                          <form enctype="multipart/form-data" id="dz-asset" method="post" data-input="dropzone" class="dropzone dz-clickable" action="/file-upload">
                                            <div class="dz-message">
                                               <i class="fa fa-upload fa-2x gray-color"></i>
                                              <h1>DRAG & DROP</h1>
                                            </div>
                                          </form>
                                        </div>
                                    </div>
                               <div class="col-md-6" style="border-left:1px solid lightgray"> 
                                    <div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;">
                                            <div id="aattachments-info-tab">

                                            </div>
                                            <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
											<div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>																					
                                            </div>	
                               </div>
                        </div> 
					</div>
					<div class="modal-footer">
						<div class="row horizontal-navigation">
							<div class="panel-control">
								<ul class="nav nav-tabs">  
                                    <li style="background-color:#3ebb64;border-color:#3ebb64;pointer-events:none;display:none;" class="active" id="ddetailsClickLI"><a href="#" onclick="ddetailsClick()">+ Details</a> 
                                    </li>
                                    <li style="background-color:lightgray;border-color:lightgray;pointer-events:none;" class="active" id="pplansClickLI"><a href="#" onclick="pplansClick()">+ Plan</a>
                                    </li>   
                                    <li style="background-color:lightgray;border-color:lightgray;pointer-events:none;" class="active" id="rremarksClickLI"><a href="#" onclick="rremarksClick()">+ Remarks</a> 
                                    </li>

                                    <li style="background-color:lightgray;border-color:lightgray;pointer-events:none;" class="active" id="ssparepartClickLI"><a href="#" onclick="ssparepartClick()">+ Spare Parts</a>
                                    </li>   
                                    <li style="background-color:lightgray;border-color:lightgray;pointer-events:none;" class="active" id="aattachClickLI"><a href="#" onclick="aattachClick()">+ Attachment</a>
                                    </li>  
                                    <li class="pull-right"><a href="#" class="capitalize-text" data-dismiss="modal">CLOSE</a>
									</li> 
                                    <li class="pull-right" id="saveAssetRemarksLi" style="display:none;"><a href="#" class="capitalize-text" onclick="saveDRemarks()">SAVE</a>
									</li>
                                    <li class="pull-right" id="updateAssetPlanLi" style="display:none;"><a href="#" class="capitalize-text" onclick="updateAssetPlan()">UPDATE</a>
									</li> 
                                    <li class="pull-right" id="saveAssetPlanLi" style="display:none;"><a href="#" class="capitalize-text" onclick="saveAssetPlan('false')">SAVE</a>
									</li> 
                                    <li class="pull-right" id="saveAssetSpareLi" style="display:none;"><a href="#" class="capitalize-text" onclick="savespareParts()">SAVE</a>
									</li>
                                    <li class="pull-right" id="updateAssetItemLi" style="display:none;"><a href="#" class="capitalize-text" onclick="updateAssetItem()">UPDATE</a>
									</li> 
                                    <li class="pull-right" id="saveAssetItemLi" ><a href="#" class="capitalize-text" onclick="saveAssetItem()">SAVE</a>
									</li>
                                    <li class="pull-right" id="saveAssetPlanTempLi" style="display:none;"><a href="#" class="capitalize-text" onclick="saveAssetPlan('true')">SAVE AS TEMPLATE</a>
									</li>
								</ul>
								<!-- /.nav -->
							</div>
						</div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>


                                
            <div aria-hidden="true" aria-labelledby="newSpareModal" role="dialog" tabindex="-1" id="newSpareModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-lg">
				  <div class="modal-content">				  
					<div class="modal-header">
					  <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
					  <h4 class="modal-title capitalize-text" >SPARE PART</h4>
					</div>
					<div class="modal-body">
                        <div class="row">
                                                         <div class="col-md-4">
                            <div class="row">
                                <div class="col-md-12">
                                     <p style="margin-left:5px;" class="red-color">Asset:</p>
                                    	<select id="assetsparechartSelect" class="selectpicker form-control"  data-live-search="true" runat="server">
                                            </select>
                                        </div> 
								</div>

                            <div class="row">
                                <div class="col-md-12">
                                        <p style="margin-left:5px;" class="red-color" >*Part Name:</p>
										<input placeholder="Part Name"  id="newtbPartName" class="form-control">
									</div>
                                                                  	
									
								</div>
               

                            <div class="row">
                                <div class="col-md-12">
                                        <p style="margin-left:5px;" class="red-color"  >Part Description:</p>
										<input placeholder="Part Description"  id="newtbPartDesc" class="form-control">
									</div>
                                                                  	
									
								</div>

                            <div class="row">
                                <div class="col-md-12">
                                        <p style="margin-left:5px;" class="red-color" >Make:</p>
										<input placeholder="Part Make"  id="newtbPartMake" class="form-control">
									</div> 
								</div>

                                                             <div class="row">
                                    <div class="col-md-12">
                                        <p style="margin-left:5px;" class="red-color"  >Model:</p>
										<input placeholder="Part Model"  id="newtbPartModel" class="form-control">
									</div>
								</div>

                                          </div>
                              <div class="col-md-4">
                                  
                                  <div class="row">
                                    <div class="col-md-12">
                                     <p style="margin-left:5px;" class="red-color">Category:</p>
                                    	<label class="select select-o">
											<select id="newspareassetCategorySelect" onchange="newspareassetCategorySelectChange(this)" runat="server">
											</select>
										 </label>
                                        </div>
								</div>
                                  <div class="row">
                                    <div class="col-md-12">
                                     <p style="margin-left:5px;" class="red-color">Sub Category:</p>
                                    	<label class="select select-o">
											<select id="newsparecategorySubAssetSelect" >
											</select>
										 </label>
                                        </div>
								</div>
                                                <div class="row">
                                                                  	<div class="col-md-12">
                                        <p style="margin-left:5px;" class="red-color" >Serial #:</p>
										<input placeholder="Part Serial"  id="newtbPartSerial" class="form-control">
									</div>
									
								</div>
                                                  <div class="row">
                                    <p style="margin-left:5px;" class="red-color" >Barcode:</p>
                                                                  	<div class="col-md-7">
                                        
										<input placeholder="Barcode"  id="newtbPartBarcode" class="form-control">
									</div>
									<div class="col-md-4" style="margin-top:3px;padding-bottom:0px;margin-bottom:-10px;">
                                        <div class="row horizontal-navigation" style="margin-bottom:0px;">
							<div class="panel-control">
								<ul class="nav nav-tabs">  
                                    <li class="active"><a  style="padding-top:5px;padding-bottom:5px;" href="#" class="capitalize-text" onclick="generateSparePartBarcode()">GENERATE</a>
									</li> 
								</ul>
								<!-- /.nav -->
							</div>
						</div>
									</div>
								</div>
                                                                                        <div id="newbarcodeimgdiv" class="row text-center" style="margin-top:-10px;">
                                                                              <img style="height:84px;" id="newbarcode" onclick="newWindow = window.open('');newWindow.document.write(newbarcode.outerHTML);
                                                                                newWindow.print();"></img>

                                                                            </div>
                              </div>

                            <div class="col-md-4" style="padding-bottom:0px;margin-bottom:-10px;">
                                            <div class="row">
                                                                  	<div class="col-md-12">
                                        <p style="margin-left:5px;" class="red-color" >Quantity:</p>
										<input placeholder="Part Quantity"  id="newtbPartQT" class="form-control">
									</div>
									
								</div>
                                <div class="row" style="margin-top:-20px;margin-bottom:0px;"> 
                                        <div class="panel-body">
                                       <p style="margin-left:5px;margin-bottom:1px;" class="red-color" >Part Image:</p>
                                          <form enctype="multipart/form-data" id="dz-newspareasset" method="post" data-input="dropzone" class="dropzone dz-clickable" action="/file-upload">
                                            <div class="dz-message">
                                               <i class="fa fa-upload fa-2x gray-color"></i>
                                              <h1>DRAG & DROP</h1>
                                            </div>
                                          </form>
                                        </div>
                                    </div> 
                            </div>
                            </div>
					</div>
					<div class="modal-footer">
						<div class="row horizontal-navigation">
							<div class="panel-control">
								<ul class="nav nav-tabs">
                                    <li ><a href="#" class="capitalize-text" data-dismiss="modal">CLOSE</a>
									</li> 
                                    <li style="display:none;" id="linsspUpdate"><a href="#" class="capitalize-text" onclick="updatesavespareParts()">UPDATE</a>
									</li>
                                    <li id="linsspSave"><a href="#" class="capitalize-text" onclick="newsavespareParts()">SAVE</a>
									</li>
								</ul>
								<!-- /.nav -->
							</div>
						</div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>
            <div aria-hidden="true" aria-labelledby="newFoundItem" role="dialog" tabindex="-1" id="newFoundItem" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-lg">
				  <div class="modal-content">				  
					<div class="modal-header">
					  <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
					  <h4 class="modal-title capitalize-text" id="taskHeader">NEW ITEM RECEIVED</h4>
					</div>
					<div class="modal-body">
                                                            <div class="row">
								   <div class="col-md-1">
                                                                               <div class="nice-checkbox inline-block no-vmargin" style="margin-top:7px;">
                                                                    <input onclick="grpCheckClick()" type="checkbox" id="grpCheck" name="niceCheck">
                                                                    <label for="grpCheck" style="color:#b2163b">Group</label>
                                   </div>
								   </div>
                                        <div class="col-md-3">
                                    <div class="nice-checkbox inline-block no-vmargin" style="margin-top:7px;">
                                                                    <input type="checkbox" id="transferFromCheck" name="niceCheck">
                                                                    <label for="transferFromCheck" style="color:#b2163b">Transfered from Site</label>
                                   </div><!--/nice-checkbox-->   
                                </div>
								<div class="col-md-2">
                                    <select id="transferSiteSelectFrom" class="form-control"  runat="server"> 
                                    </select>
								</div>
                                <div class="col-md-2">
                                    <input placeholder="Barcode"  id="tbfindRefernce" class="form-control">
                                </div>
                                <div class="col-md-4">
                                    <label class="select select-o" style="display:none;">
                                           <select id="SelectCameraType" onchange="SelectCameraTypeOnChange(this)">
											  <option>Upload</option>
                                               <option>Webcam</option>
											</select>
										 </label>
								</div>
                                    </div>
						<div class="row">
							<div class="col-md-4">
                                <div class="row">

                                        <p style="margin-left:5px;margin-bottom:5px;margin-top:5px;" class="red-color">*Date/Time Found:</p>
                                        <div class="col-md-8">
                                            																			<div class="form-group">
                                          <div class="input-group input-group-in">

                                            <input id="dateFoundCalendar" data-input="daterangepicker" data-single-date-picker="true" data-show-dropdowns="true" class="form-control" placeholder="Choose a start date">

                                            <span class="input-group-addon red-color"><i class="fa fa-calendar"></i></span>                                            
                                          </div><!-- /input-group-in -->
                                        </div><!--/form-group-->
                                        </div>
                                        <div class="col-md-4">
                                                                                    <label class="select select-o">
                                         <select id="foundTime" >
                                         </select>
                                        </label>
                                        </div>
								</div>
								<div class="row">
                                                                  	<div class="col-md-12">
                                        <p style="margin-left:5px;" class="red-color">*Finder Name:</p>
										<input placeholder="Finder Name"  id="tbfinderName" class="form-control">
									</div>
								</div>
                                								<div class="row">
                                    <div class="col-md-12">
                                        <p style="margin-left:5px;" class="red-color">*Item Brand:</p>
										<input placeholder="Item Brand" id="tbItemBrand" class="form-control">
									</div>
								</div>	
								<div class="row">
                                    <div class="col-md-12">
                                     <p style="margin-left:5px;" class="red-color">*Item Type:</p>
                                    	<label class="select select-o">
											<select id="itemTypeSelect" onchange="ItemTypeSelectChange(this)" runat="server">
											</select>
										 </label>
                                        </div>
								</div>
                                <div class="row" style="display:none;">
                                    <div class="col-md-12">
                                     <p style="margin-left:5px;" class="red-color">*Item Status:</p>
										<label class="select select-o">
                                           <select id="itemStatusSelect">
											  <option>Found</option>
											  <option>Disposed</option>
											  <option>Returned</option>
                                              <option>Donated</option>
											</select>
										 </label>
                                       </div>
								</div>
								<div class="row">
									<div class="col-md-12">
                                        <p style="margin-left:5px;" class="red-color">*Location Found:</p>
										<label class="select select-o">
											<select id="locationFoundSelect"  runat="server">
											</select>
										 </label>
									</div>
								</div>
								<div class="row">
								   <div class="col-md-12">
                                        <p style="margin-left:5px;" class="red-color">*Receiver Name:</p>
										<input placeholder="Receiver Name" id="tbreceiverName" class="form-control">
									</div>
								</div>		
                                								<div class="row">
									<div class="col-md-12">
                                        <p style="margin-left:5px;" class="red-color">*Item Storage:</p>
										<label class="select select-o">
											<select id="locationStoreSelect" onchange="itemLocationSelectChange(this)" runat="server">
											</select>
										 </label>
									</div>
								</div>
					
							</div>
							<div class="col-md-4" >
                                <div class="row">
                                     <p class="red-color" style="margin-top:5px;margin-bottom:5px;margin-left:5px;">*Received Date/Time:</p>
                                    									<div class="col-md-8">
                                                    																			<div class="form-group">
                                          <div class="input-group input-group-in">

                                            <input id="dateReceiveCalendar" data-input="daterangepicker" data-single-date-picker="true" data-show-dropdowns="true" class="form-control" placeholder="Choose a start date">

                                            <span class="input-group-addon red-color"><i class="fa fa-calendar"></i></span>                                            
                                          </div><!-- /input-group-in -->
                                        </div><!--/form-group-->

                                    </div>

                                    <div class="col-md-4">

                                        <label class="select select-o">
                                             <select id="receiveTime" >
                                             </select>
                                         <//label>
                                    </div>
                                </div>
                                                                <div class="row">
																	<div class="col-md-12">
                                        <p style="margin-left:5px;" class="red-color">*Finder Department:</p>
										<label class="select select-o">
                                           <select id="finderDepartmentSelect" runat="server">

											</select>
										 </label>
									</div>
								</div>		
	                            <div class="row">
														<div class="col-md-12">
                                        <p style="margin-left:5px;" class="red-color">*Shelf Life:</p>
										<label class="select select-o">
											<select id="shelfLifeSelect">
                                                <option>24h</option>
                                                <option>1 Week</option>
                                                <option>1 Month</option>
											</select>
										 </label>
									</div>
								</div>	                                                                     
                                	                                                                  <div class="row">
														<div class="col-md-12">
                                        <p style="margin-left:5px;" class="red-color">*Item Sub Type:</p>
										<label class="select select-o">
											<select id="itemSubSelect">
											</select>
										 </label>
									</div>
								</div>		
                                                                <div class="row">
														<div class="col-md-12">
                                        <p style="margin-left:5px;" class="red-color">*Item Colour:</p>
										<label class="select select-o">
											<select id="itemColourSelect" runat="server">
											</select>
										 </label>
									</div>
								</div>		
                                <div class="row">
                                    <div class="col-md-12">
          <p style="margin-left:5px;" class="red-color">*Room Number:</p>
										<input placeholder="Room Number" id="tbroomNumber" class="form-control">
                                        
									</div>
								</div>	
	                                                                  <div class="row">
														<div class="col-md-12">
                                        <p style="margin-left:5px;" class="red-color">*Item Sub Storage:</p>
										<label class="select select-o">
											<select id="itemStorageSelect">
											</select>
										 </label>
									</div>
								</div>	
						    </div>
                            <div class="col-md-4">


                                <div class="row">
                                      <div id="cameraUploadDIV" class="col-md-12"> 
                                        <div class="panel-body">
                                          <h3>Item Image</h3>
                                          <form enctype="multipart/form-data" id="dz-test" method="post" data-input="dropzone" class="dropzone dz-clickable" action="/file-upload">
                                            <div class="dz-message">
                                               <i class="fa fa-upload fa-2x gray-color"></i>
                                              <h1>DRAG & DROP</h1>
                                            </div>
                                          </form>
                                        </div>
                                    </div>
                                    <div id="webcameraUploadDIV" class="col-md-12" style="display:none;">
                                        	<div id="my_camera"></div>
                                        	<script type="text/javascript" src="../Scripts/webcam.min.js"></script>
	
	<!-- Configure a few settings and attach camera -->
	<script language="JavaScript">

	</script>
                                        <div id="resultsHeader" style="display:none;">CAPTURED IMAGE</div>
                                        <div id="results"></div>
                                        
                                        	<script language="JavaScript">
                                        	    function take_snapshot() {
                                        	        // take snapshot and get image data
                                        	        Webcam.snap(function (dataURI) {
                                        	            // display results in page
                                        	            document.getElementById('results').innerHTML =
                                                            '<img src="' + dataURI + '"/>';

                                        	            //var png = img_b64.split(',')[1];

                                        	            var byteString;
                                        	            if (dataURI.split(',')[0].indexOf('base64') >= 0)
                                        	                byteString = atob(dataURI.split(',')[1]);
                                        	            else
                                        	                byteString = unescape(dataURI.split(',')[1]);

                                        	            // separate out the mime component
                                        	            var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

                                        	            // write the bytes of the string to a typed array
                                        	            var ia = new Uint8Array(byteString.length);
                                        	            for (var i = 0; i < byteString.length; i++) {
                                        	                ia[i] = byteString.charCodeAt(i);
                                        	            }

                                        	            var bblob = new Blob([ia], { type: mimeString });

                                        	            var data = new FormData();
                                        	            data.append("image_data", bblob);
                                        	            jQuery.ajax({
                                        	                url: "../Handlers/ItemFoundImgUpload.ashx",
                                        	                type: "POST",
                                        	                data: data,
                                        	                contentType: false,
                                        	                processData: false,
                                        	                success: function (result) {
                                        	                    document.getElementById("imagePath").text = result.replace(/\\/g, "|")
                                        	                    document.getElementById("resultsHeader").style.display = "block";

                                        	                },
                                        	                error: function (err) {
                                        	                }
                                        	            })
                                        	        });
                                        	    }
	                                        </script>
                                    </div>
                                </div>
                                 <div class="row">
									<div class="col-md-12">
                                        <p style="margin-left:5px;" class="red-color">*Item Reference:</p>
										<input placeholder="Item Reference" id="tbItemReference" class="form-control">
									</div>
								</div>
                                                                                                            
                            </div>
						</div>
                        <div class="row horizontal-navigation">
                                                        <div class="panel-control">
                                                            <ul class="nav nav-tabs" id="alarmToSendList">
                                                            </ul>
                                                        </div>
                                                    </div>
					</div>
					<div class="modal-footer">
						<div class="row horizontal-navigation">
							<div class="panel-control">
								<ul class="nav nav-tabs">
                                    <li class="active"><a href="#" class="capitalize-text" id="clearFoundI" onclick="clearFoundItem()">CLEAR</a>
									</li>
                                    <li class="active"><a href="#" class="capitalize-text" onclick="saveFoundItem('normal')">SAVE</a>
									</li>
                                    <li class="active"><a href="#" class="capitalize-text" onclick="generateBarcode()">GENERATE BARCODE</a>
									</li>
                                    <li class="active"><a href="#" class="capitalize-text" onclick="searchBarcode()">SEARCH BARCODE</a>
									</li>
                                    <li class="active" style="display:none;" id="addLi"><a href="#" class="capitalize-text" id="addFoundSave" onclick="saveFoundItem('add')">ADD</a> 
									</li>
                                    <li id="webcameraCapture" style="display:none;" class="active"><a href="#" class="capitalize-text" onclick="take_snapshot()">CAPTURE IMAGE</a>
									</li>
								</ul>
								<!-- /.nav -->
							</div>
						</div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>
            <div aria-hidden="true" aria-labelledby="foundItemViewCard" role="dialog" tabindex="-1" id="foundItemViewCard" class="modal fade videoModal" style="display: none;">
				<div class="modal-dialog modal-lg">
				  <div class="modal-content">
					<div class="modal-header">
					  <div class="row">
						<div class="col-md-11">
							<span class="circle-point-container pull-left mt-2x mr-1x"><span  class="circle-point circle-point-orange"></span></span>
							<h4 class="modal-title capitalize-text" id="fItemStatus">FOUND ITEM</h4>
						</div>
						<div class="col-md-1">
							<button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
						</div>						
					  </div>
					  <div class="row">
						<div class="col-md-4">
							<p>Finder Name: <span id="foundBySpan"></span></p>
						</div>		
                        <div class="col-md-4">
							<p>Location Found: <span id="flocSpan"></span></p>
						</div>	
						<div class="col-md-4">
							<p>Date Found: <span id="ftimeSpan"></span></p>
						</div>				
					  </div>	
                      <div class="row">
						<div class="col-md-4">
							<p>Finder Department: <span id="fDepartmentSpan"></span></p>
						</div>		
                        <div class="col-md-4">
							<p>Item Reference: <span id="fItemReference"></span></p>
						</div>	
						<div class="col-md-4">
							<p>Item Storage: <span id="fItemStorage"></span></p>
						</div>				
					  </div>		
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-5">
								<div class="panel-control">
                                        <ul class="nav nav-tabs nav-contrast-red" "> 
                                            <li class="active" id="fliInfo"><a href="#finfo-tab" data-toggle="tab" class="capitalize-text">INFO</a>
                                            </li>
                                            <li id="fliDispose" style="display:none;"><a href="#fdispose-tab" data-toggle="tab" class="capitalize-text">DETAILS</a>
                                            </li>
                                            <li id="fliReturn" style="display:none;"><a href="#freturn-tab" data-toggle="tab" class="capitalize-text">DETAILS</a>
                                            </li>
                                            <li id="fliAtta"><a href="#fattachments-tab" data-toggle="tab" class="capitalize-text">ATTACHMENTS</a>
                                            </li>											
                                        </ul>
                                        <!-- /.nav -->
                                   </div>
								<div class="row">
									<div class="col-md-12">
                                        <div class="tab-content">
										<div class="tab-pane fade active in" id="finfo-tab">
											<div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:400px;">	
                                                <div class="col-md-12">
													<p class="red-color"><b>Item Brand:</b></p>
													<p id="fItemBrand"></p>
												</div>
                                                <div class="col-md-12">
                                                <p class="red-color"><b>Item Type:</b></p>
													<p id="fItemType"></p>
												</div>
                                                <div class="col-md-12">
                                                <p class="red-color"><b>Item Sub Type:</b></p>
													<p id="fItemSubType"></p>
												</div>
                                                <div class="col-md-12">
                                                <p class="red-color"><b>Item Sub Storage:</b></p>
													<p id="fItemSubStorage"></p>
												</div>
                                                <div class="col-md-12">
                                                <p class="red-color"><b>Item ShelfLife:</b></p>
													<p id="fItemShelfLife"></p>
												</div>
                                                <div class="col-md-12">
                                                <p class="red-color"><b>Item Colour:</b></p>
													<p id="fItemColour"></p>
												</div>
                                                <div class="col-md-12">
                                            	<p class="red-color"><b>Receiver Name:</b></p>
													<p id="fReceiveName"></p>
												</div>
                                                <div class="col-md-12">
                                            	<p class="red-color"><b>Receiver Date/Time:</b></p>
													<p id="fReceiveDT"></p>
												</div>
                                                <div class="col-md-12">
                                            	<p class="red-color"><b>Room Number:</b></p>
													<p id="fRoomNumber"></p>
												</div>
                                                <div class="col-md-12">
													<p id="groupedItemsDIV" class="red-color"><b>Grouped Items : </b></p>
                                                    <ul id="checklistItemsList" style="list-style-type: none;">
                                                    </ul>
												</div>
											    <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
											    <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>																					
										    </div>
                                        </div>
                                        <div class="tab-pane fade" id="fdispose-tab">
											<div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:400px;">	
                                                                                                <div class="col-md-12">
                                                <p class="red-color"><b>Disposed Date:</b></p>
													<p id="dReturnDate"></p>
												</div>
                                                <div class="col-md-12">
                                                <p class="red-color"><b>Disposed To:</b></p>
													<p id="dOwnerName"></p>
												</div>
                                                <div class="col-md-12">
                                                <p class="red-color"><b>Recipient Name:</b></p>
													<p id="dOwnerType"></p>
												</div>
                                                <div class="col-md-12">
                                                <p class="red-color"><b>Recipient Company:</b></p>
													<p id="dOwnerRoom"></p>
												</div>
                                                <div class="col-md-12">
                                                <p class="red-color"><b>Recipient Location:</b></p>
													<p id="dOwnerNation"></p>
												</div>
                                                <div class="col-md-12">
                                                <p class="red-color"><b>Recipient Contact#:</b></p>
													<p id="dOwnerAddress"></p>
												</div>
                                                <div class="col-md-12">
                                                <p class="red-color"><b>Recipient ID#:</b></p>
													<p id="dOwnerContact"></p>
												</div>
                                                <div class="col-md-12" id="DAPSDiv">
                                                <p class="red-color"><b>Destroyed as per SOP:</b></p>
													<p id="dOwnerEmail"></p>
												</div>
                                                <div class="col-md-12">
                                                <p class="red-color"><b>Disposed After:</b></p>
													<p id="dOwnerNumber"></p>
												</div>
                                                <div class="col-md-12">
                                                <p class="red-color"><b>Disposed of By:</b></p>
													<p id="dOwnerTracking"></p>
												</div>
                                                <div class="col-md-12">
                                                <p class="red-color"><b>Witness:</b></p>
													<p id="dOwnerHanded"></p>
												</div>
											    <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
											    <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>																					
										    </div>
                                        </div>
                                        <div class="tab-pane fade" id="freturn-tab">
											<div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:400px;">	
                                                <div class="col-md-12">
                                                <p class="red-color"><b>Return Date:</b></p>
													<p id="fReturnDate"></p>
												</div>
                                                <div class="col-md-12">
                                                <p class="red-color"><b>Name:</b></p>
													<p id="fOwnerName"></p>
												</div>
                                                <div class="col-md-12">
                                                <p class="red-color"><b>Type:</b></p>
													<p id="fOwnerType"></p>
												</div>
                                                <div class="col-md-12">
                                                <p class="red-color"><b>Room Number:</b></p>
													<p id="fOwnerRoom"></p>
												</div>
                                                <div class="col-md-12">
                                                <p class="red-color"><b>Nationality:</b></p>
													<p id="fOwnerNation"></p>
												</div>
                                                <div class="col-md-12">
                                                <p class="red-color"><b>Address:</b></p>
													<p id="fOwnerAddress"></p>
												</div>
                                                <div class="col-md-12">
                                                <p class="red-color"><b>Contact#:</b></p>
													<p id="fOwnerContact"></p>
												</div>
                                                <div class="col-md-12">
                                                <p class="red-color"><b>Email:</b></p>
													<p id="fOwnerEmail"></p>
												</div>
                                                <div class="col-md-12">
                                                <p class="red-color"><b>ID Number:</b></p>
													<p id="fOwnerNumber"></p>
												</div>
                                                <div class="col-md-12">
                                                <p class="red-color"><b>Tracking #:</b></p>
													<p id="fOwnerTracking"></p>
												</div>
                                                <div class="col-md-12">
                                                <p class="red-color"><b>Handed over by:</b></p>
													<p id="fOwnerHanded"></p>
												</div>
                                                <div class="col-md-12" id="fOwnerTransfersiteDIV" style="display:none">
                                                <p class="red-color"><b>Transfered to:</b></p> 
													<p id="fOwnerTransfersite"></p>
												</div>
											    <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
											    <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>																					
										    </div>
                                        </div>
										<div class="tab-pane fade" id="fattachments-tab">	
                                            <div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:338px;">
                                            <div id="fattachments-info-tab">

                                            </div>
                                            <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
											<div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>																					
                                            </div>							
										</div>			
                                            </div>							
									</div>
								</div>
							</div>
							<div class="col-md-7" id="fdivAttachmentHolder">
								<div class="tab-pane fade active in" id="flocation-tab">
								</div>			
							</div>
                            <div class="col-md-7" id="fReturnHolder" style="display:none;">
                                <div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:400px;">	
                                <div class="row text-center"> <p  class="red-color"><b>RETURN ITEM FORM:</b></p></div>
                                    <div class="row">
								   <div class="col-md-1">
								   </div>
                                        <div class="col-md-4">
                                    <div class="nice-checkbox inline-block no-vmargin" style="margin-top:7px;">
                                                                    <input type="checkbox" id="transferCheck" name="niceCheck">
                                                                    <label for="transferCheck" style="color:#b2163b">Transfer Site</label>
                                   </div><!--/nice-checkbox-->   
                                </div>
								<div class="col-md-6">
                                    <select id="transferSiteSelect" class="form-control"  runat="server">
                                    </select>
								</div>
                                    </div>
                                <div class="row">
                                
								<div class="col-md-4">
                                                                    <div class="row">
                                                              	<div class="col-md-12">
                                        <p style="margin-left:5px;" class="red-color">Name:</p>
										<input   id="tbOwnerName" class="form-control">
									</div>
								</div>
                                    								<div class="row">
                                    <div class="col-md-12">
                                     <p style="margin-left:5px;" class="red-color">Nationality:</p>
										<label class="select select-o" >
                                           <select id="ownerNationalitySelect" class="selectpicker form-control"  data-live-search="true">

<option>Afghanistan</option>
<option>Albania</option>
<option>Algeria</option>
<option>Andorra</option>
<option>Angola</option>
<option>Antigua & Deps</option>
<option>Argentina</option>
<option>Armenia</option>
<option>Australia</option>
<option>Austria</option>
<option>Azerbaijan</option>
<option>Bahamas</option>
<option>Bahrain</option>
<option>Bangladesh</option>
<option>Barbados</option>
<option>Belarus</option>
<option>Belgium</option>
<option>Belize</option>
<option>Benin</option>
<option>Bhutan</option>
<option>Bolivia</option>
<option>Bosnia Herzegovina</option>
<option>Botswana</option>
<option>Brazil</option>
<option>Brunei</option>
<option>Bulgaria</option>
<option>Burkina</option>
<option>Burundi</option>
<option>Cambodia</option>
<option>Cameroon</option>
<option>Canada</option>
<option>Cape Verde</option>
<option>Central African Rep</option>
<option>Chad</option>
<option>Chile</option>
<option>China</option>
<option>Colombia</option>
<option>Comoros</option>
<option>Congo</option>
<option>Congo {Democratic Rep}</option>
<option>Costa Rica</option>
<option>Croatia</option>
<option>Cuba</option>
<option>Cyprus</option>
<option>Czech Republic</option>
<option>Denmark</option>
<option>Djibouti</option>
<option>Dominica</option>
<option>Dominican Republic</option>
<option>East Timor</option>
<option>Ecuador</option>
<option>Egypt</option>
<option>El Salvador</option>
<option>Equatorial Guinea</option>
<option>Eritrea</option>
<option>Estonia</option>
<option>Ethiopia</option>
<option>Fiji</option>
<option>Finland</option>
<option>France</option>
<option>Gabon</option>
<option>Gambia</option>
<option>Georgia</option>
<option>Germany</option>
<option>Ghana</option>
<option>Greece</option>
<option>Grenada</option>
<option>Guatemala</option>
<option>Guinea</option>
<option>Guinea-Bissau</option>
<option>Guyana</option>
<option>Haiti</option>
<option>Honduras</option>
<option>Hungary</option>
<option>Iceland</option>
<option>India</option>
<option>Indonesia</option>
<option>Iran</option>
<option>Iraq</option>
<option>Ireland {Republic}</option>
<option>Israel</option>
<option>Italy</option>
<option>Ivory Coast</option>
<option>Jamaica</option>
<option>Japan</option>
<option>Jordan</option>
<option>Kazakhstan</option>
<option>Kenya</option>
<option>Kiribati</option>
<option>Korea North</option>
<option>Korea South</option>
<option>Kosovo</option>
<option>Kuwait</option>
<option>Kyrgyzstan</option>
<option>Laos</option>
<option>Latvia</option>
<option>Lebanon</option>
<option>Lesotho</option>
<option>Liberia</option>
<option>Libya</option>
<option>Liechtenstein</option>
<option>Lithuania</option>
<option>Luxembourg</option>
<option>Macedonia</option>
<option>Madagascar</option>
<option>Malawi</option>
<option>Malaysia</option>
<option>Maldives</option>
<option>Mali</option>
<option>Malta</option>
<option>Marshall Islands</option>
<option>Mauritania</option>
<option>Mauritius</option>
<option>Mexico</option>
<option>Micronesia</option>
<option>Moldova</option>
<option>Monaco</option>
<option>Mongolia</option>
<option>Montenegro</option>
<option>Morocco</option>
<option>Mozambique</option>
<option>Myanmar, {Burma}</option>
<option>Namibia</option>
<option>Nauru</option>
<option>Nepal</option>
<option>Netherlands</option>
<option>New Zealand</option>
<option>Nicaragua</option>
<option>Niger</option>
<option>Nigeria</option>
<option>Norway</option>
<option>Oman</option>
<option>Pakistan</option>
<option>Palau</option>
<option>Panama</option>
<option>Papua New Guinea</option>
<option>Paraguay</option>
<option>Peru</option>
<option>Philippines</option>
<option>Poland</option>
<option>Portugal</option>
<option>Qatar</option>
<option>Romania</option>
<option>Russian Federation</option>
<option>Rwanda</option>
<option>St Kitts & Nevis</option>
<option>St Lucia</option>
<option>Saint Vincent & the Grenadines</option>
<option>Samoa</option>
<option>San Marino</option>
<option>Sao Tome & Principe</option>
<option>Saudi Arabia</option>
<option>Senegal</option>
<option>Serbia</option>
<option>Seychelles</option>
<option>Sierra Leone</option>
<option>Singapore</option>
<option>Slovakia</option>
<option>Slovenia</option>
<option>Solomon Islands</option>
<option>Somalia</option>
<option>South Africa</option>
<option>South Sudan</option>
<option>Spain</option>
<option>Sri Lanka</option>
<option>Sudan</option>
<option>Suriname</option>
<option>Swaziland</option>
<option>Sweden</option>
<option>Switzerland</option>
<option>Syria</option>
<option>Taiwan</option>
<option>Tajikistan</option>
<option>Tanzania</option>
<option>Thailand</option>
<option>Togo</option>
<option>Tonga</option>
<option>Trinidad & Tobago</option>
<option>Tunisia</option>
<option>Turkey</option>
<option>Turkmenistan</option>
<option>Tuvalu</option>
<option>Uganda</option>
<option>Ukraine</option>
<option>United Arab Emirates</option>
<option>United Kingdom</option>
<option>United States</option>
<option>Uruguay</option>
<option>Uzbekistan</option>
<option>Vanuatu</option>
<option>Vatican City</option>
<option>Venezuela</option>
<option>Vietnam</option>
<option>Yemen</option>
<option>Zambia</option>
<option>Zimbabwe</option>
											 
											
											</select>
										 </label>
                                        </div>
								</div>
								<div class="row">
                                                                  	<div class="col-md-12">
                                        <p style="margin-left:5px;" class="red-color">ID Number:</p>
										<input   id="tbOwnerNumber" class="form-control">
									</div>
								</div>

								</div>	
                                <div class="col-md-4">
                                                                        								<div class="row">
                                    <div class="col-md-12">
                                     <p style="margin-left:5px;" class="red-color">Type:</p>
										<label class="select select-o">
                                           <select id="ownerTypeSelect" onchange="ownerTypeSelectChange(this)">
                                               <option>Staff</option>
                                               <option>In-house-guest</option>
											</select>
										 </label>
                                        </div>
								</div>
                                                                        								<div class="row">
                                                                  	<div class="col-md-12">
                                        <p style="margin-left:5px;" class="red-color">Contact #:</p>
										<input style="height:40px;margin-bottom:-4px;"  id="tbOwnerContact" class="form-control">
									</div>
								</div>
                                <div class="row">
                                                                  	<div class="col-md-12">
                                        <p style="margin-left:5px;" class="red-color">Tracking #:</p>
										<input   id="tbShippingTracking" class="form-control">
									</div>
								</div>

								</div>	
                                <div class="col-md-4">
                                                                        								<div class="row">
                                                                  	<div class="col-md-12">
                                        <p style="margin-left:5px;" class="red-color">Address:</p>
										<input   id="tbOwnerAddress" class="form-control">
									</div>
								</div>
                                                                                                        <div class="row">
                                                                  	<div class="col-md-12">
                                        <p style="margin-left:5px;" class="red-color">Email:</p>
										<input style="height:40px;margin-bottom:-4px;"  id="tbOwnerEmail" class="form-control">
									</div>
								</div>
                                                                    <div class="row" id="roomNumberDiv" style="display:none;">
                                                                  	<div class="col-md-12"  >
                                        <p style="margin-left:5px;" class="red-color">Room Number:</p>
										<input   id="tbRetRoomNumber" class="form-control">
									</div>
								</div>
								</div>	
                                </div>
                                <div class="row">
                                    <div class="col-md-12" style="margin-top:-20px;">
                                        <div class="panel-body">
                                            <p class="red-color">Attachment:</p>
                                          <form enctype="multipart/form-data" id="dz-return" method="post" data-input="dropzone" class="dropzone dz-clickable" action="/file-upload">
                                            <div class="dz-message">
                                               <i class="fa fa-upload fa-2x gray-color"></i>
                                              <h1>DRAG & DROP</h1>
                                            </div>
                                          </form>
                                        </div>
                                    </div>  
                                </div>
                                 <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
											    <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>
                                </div>
							</div>
                            <div class="col-md-7" id="fDisposeHolder"  style="display:none;">
                                <div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:400px;">	
                                <div class="row text-center"> <p  class="red-color"><b>DISPOSE ITEM FORM:</b></p></div>
                                <div class="row">
								<div class="col-md-4">
                                                                    <div class="row">
                                                              	<div class="col-md-12">
                                        <p style="margin-left:5px;" class="red-color">Disposed To:</p>
																				<label class="select select-o">
                                           <select id="disposeToSelect" onchange="disposeToSelectChange(this)">
                                               <option>Charity</option>
                                                 <option>Authorities</option>
                                               <option>Destroyed</option>
											</select>
										 </label>
									</div>
								</div>
                                    								<div class="row">
                                    <div class="col-md-12">
                                     <p style="margin-left:5px;" class="red-color">Recipient Name:</p>
                                        								<input   id="tbDisposeName" class="form-control">
                                        </div>
								</div>
								<div class="row">
                                                                  	<div class="col-md-12">
                                        <p style="margin-left:5px;" class="red-color">Company:</p>
										<input   id="tbDisposeCompany" class="form-control">
									</div>
								</div>

								</div>	
                                <div class="col-md-4">
                                                                                                                                         <div class="row">
                                                                  	<div class="col-md-12">
                                        <p style="margin-left:5px;" class="red-color">Disposed After:</p>
																				 <label class="select select-o">
                                           <select id="disposedAfterSelect">
                                               <option>24h</option>
                                                 <option>1 week</option>
                                               <option>1 month</option>
											</select>
										 </label>
									</div>
								</div>

                                                                        								<div class="row">
                                                                  	<div class="col-md-12">
                                        <p style="margin-left:5px;" class="red-color">Contact #:</p>
										<input   id="tbDisposeContact" class="form-control">
									</div>
								</div>
                                <div class="row">
                                                                  	<div class="col-md-12">
                                        <p style="margin-left:5px;" class="red-color">Recipient ID #:</p>
										<input   id="tbDisposeIDNumber" class="form-control">
									</div>
								</div>

								</div>	
                                <div class="col-md-4">
                                                                        								<div class="row" id="destroySOPDiv" style="display:none;">
                                                                  	<div class="col-md-12">
                                        <p style="margin-left:5px;" class="red-color">Destroy SOP:</p>
										 <label class="select select-o">
                                           <select id="sopSelect">
                                               <option>Alcohol</option>
                                                 <option>Perishable</option>
                                               <option>Medicine</option>
											</select>
										 </label>
									</div>
								</div>
                                                                      <div class="row">
                                    <div class="col-md-12">
                                     <p style="margin-left:5px;" class="red-color">Recipient Location:</p>
                                        <input   id="tbDisposeLocation" class="form-control">

                                        </div>
								</div>
   
                                                                    <div class="row">
                                                                  	<div class="col-md-12" >
                                        <p style="margin-left:5px;" class="red-color">Witness:</p>
										<input   id="tbDisposeWitness" class="form-control">
									</div>
								</div>
								</div>	
                                    </div>
                                <div class="row">
                                    <div class="col-md-12" style="margin-top:-20px;">
                                        <div class="panel-body">
                                            <p class="red-color">Attachment:</p>
                                          <form enctype="multipart/form-data" id="dz-dispose" method="post" data-input="dropzone" class="dropzone dz-clickable" action="/file-upload">
                                            <div class="dz-message">
                                               <i class="fa fa-upload fa-2x gray-color"></i>
                                              <h1>DRAG & DROP</h1>
                                              
                                            </div>
                                          </form>
                                        </div>
                                    </div>  
                                </div>
                                                             <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
											    <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>
                                </div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<div class="row horizontal-navigation">
							<div class="panel-control">
								<ul class="nav nav-tabs" id="foundDiv">
                                    <li><a href="#" data-dismiss="modal">CLOSE</a>
									</li>	
									<li><a href="#" onclick="disposeClick()">DISPOSE</a>
									</li>
									<li><a href="#" onclick="returnClick()">RETURN</a>
									</li>	
                                    <li>
<a href="#" onclick="document.getElementById('dz-post').click()">ATTACH</a>

                                    </li>		
								</ul> 
                                <ul class="nav nav-tabs" id="returnHandleDiv" style="display:none;">
                                    <li><a href="#" onclick="cancelClick()">CANCEL</a>
									</li>	
									<li><a href="#" onclick="processReturn()">RETURN</a>
									</li>		
								</ul>
                                <ul class="nav nav-tabs" id="diposeHandleDiv" style="display:none;">
                                    <li><a href="#" onclick="cancelClick()">CANCEL</a>
									</li>	
									<li><a href="#" onclick="processDispose()">DISPOSE</a>
									</li>		
								</ul>
                                <ul class="nav nav-tabs" id="processHandleDiv" style="display:none;">
                                    <li><a href="#" data-dismiss="modal">CLOSE</a>
									</li>	
<li>
<a href="#" onclick="document.getElementById('dz-post').click()">ATTACH</a>

                                    </li>									
								</ul>
								<!-- /.nav -->
							</div>
							                                          <form style="display:none" enctype="multipart/form-data" id="dz-post" method="post" data-input="dropzone" class="dropzone dz-clickable" action="/file-upload">
                                            <div class="dz-message">
                                               <i class="fa fa-upload fa-2x gray-color"></i>
                                              <h1>DRAG & DROP</h1>
                                              
                                            </div>
                                          </form>
                             <form style="display:none" enctype="multipart/form-data" id="checkitemDZ" method="post" data-input="dropzone" class="dropzone dz-clickable" action="/file-upload">
                                            <div class="dz-message">
                                               <i class="fa fa-upload fa-2x gray-color"></i>
                                              <h1>DRAG & DROP</h1>
                                              
                                            </div>
                                          </form>
						</div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>
            <div aria-hidden="true" aria-labelledby="searchFoundItem" role="dialog" tabindex="-1" id="searchFoundItem" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-lg">
				  <div class="modal-content">				  
					<div class="modal-header">
					  <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
					  <h4 class="modal-title capitalize-text">SEARCH FOR AN ITEM</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-4">
                                <div class="row">
                                    <div class="col-md-12">
                                     <p style="margin-left:5px;" class="red-color">Item Type:</p>
                                    	<label class="select select-o">
											<select id="searchItemTypeSelect" onchange="searchItemTypeSelectChange(this)" runat="server">
											</select>
										 </label>
                                        </div>
								</div>
                                <div class="row">
                                    <div class="col-md-12">
                                     <p style="margin-left:5px;" class="red-color">Item Sub Type:</p>
                                    	<label class="select select-o">
											<select id="searchItemSubSelect" >
                                                <option>Please Select</option>
											</select>
										 </label>
                                        </div>
								</div>
                                <div class="row">

                                        <p style="margin-left:5px;margin-bottom:5px;margin-top:5px;" class="red-color">*Date From:</p>
                                        <div class="col-md-12">
                                            																			<div class="form-group">
                                          <div class="input-group input-group-in">

                                            <input id="dateFromCalendar" data-input="daterangepicker" data-single-date-picker="true" data-show-dropdowns="true" class="form-control" placeholder="Choose a start date">

                                            <span class="input-group-addon red-color"><i class="fa fa-calendar"></i></span>                                            
                                          </div><!-- /input-group-in -->
                                        </div><!--/form-group-->
                                        </div>
                                        <div style="display:none;" class="col-md-4">
                                                                                    <label class="select select-o">
                                         <select id="FromTime" >
                                         </select>
                                        </label>
                                        </div>
								</div>
                                                                <div class="row">

                                        <p style="margin-left:5px;margin-bottom:5px;margin-top:5px;" class="red-color">*Date To:</p>
                                        <div class="col-md-12">
                                            																			<div class="form-group">
                                          <div class="input-group input-group-in">

                                            <input id="dateToCalendar" data-input="daterangepicker" data-single-date-picker="true" data-show-dropdowns="true" class="form-control" placeholder="Choose a start date">

                                            <span class="input-group-addon red-color"><i class="fa fa-calendar"></i></span>                                            
                                          </div><!-- /input-group-in -->
                                        </div><!--/form-group-->
                                        </div>
                                        <div style="display:none;" class="col-md-4">
                                                                                    <label class="select select-o">
                                         <select id="ToTime" >
                                         </select>
                                        </label>
                                        </div>
								</div>
								<div class="row">
                                                                  	<div class="col-md-12">
                                        <p style="margin-left:5px;" class="red-color">Item Brand:</p>
										<input   id="tbSearchBrand" class="form-control">
									</div>
									
								</div>
								<div class="row">
									<div class="col-md-12">
                                        <p style="margin-left:5px;" class="red-color">Item Found Location:</p>
										<label class="select select-o">
											<select id="searchLocationSelect" runat="server">
											</select>
										 </label>
									</div>
								</div>
                                								<div class="row">
									<div class="col-md-12">
                                        <p style="margin-left:5px;" class="red-color">Item Colour:</p>
										<label class="select select-o">
											<select id="searchColorSelect" runat="server">
											</select>
										 </label>
									</div>
								</div>
                                <div class="row">
									<div class="col-md-12">
                                        <p style="margin-left:5px;" class="red-color">Item Store Location:</p>
										<label class="select select-o">
											<select id="searchLocationStoreSelect" onchange="searchLocationStoreSelectChange(this)" runat="server">
											</select>
										 </label>
									</div>
								</div>
                                <div class="row">
                                    <div class="col-md-12">
                                     <p style="margin-left:5px;" class="red-color">Item Sub Location:</p>
                                    	<label class="select select-o">
											<select id="searchSubStoreSelect" >
                                                <option>Please Select</option>
											</select>
										 </label>
                                        </div>
								</div>
                                <div class="row">
                                    <div class="col-md-12">
                                     <p style="margin-left:5px;" class="red-color">Item Shelf Life:</p>
                                    	<label class="select select-o">
											<select id="searchShelfLifeSelect" >
                                                <option>Please Select</option>
                                                <option>24h</option>
                                                <option>1 Week</option>
                                                <option>1 Month</option>
											</select>
										 </label>
                                        </div>
								</div>
							</div>
							<div class="col-md-8" >
                                 <div  data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-8">
                                                        <h3 class="panel-title capitalize-text">SEARCH RESULTS</h3>
                                                    </div>
                                                    <div class="col-md-4 mt-2x" style="display:none;">
                                                        <input type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" number-of-rows="5" id="searchTable" role="grid">
                                                        <thead>
                                                            <tr role="row">
                                                                                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="USER">DATE<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">REF#<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="STATUS">STATUS<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
						    </div>
						</div>
					</div>
					<div class="modal-footer">
						<div class="row horizontal-navigation">
							<div class="panel-control">
								<ul class="nav nav-tabs">
                                    <li ><a href="#" class="capitalize-text" onclick="searchForItem()">SEARCH</a>
									</li>
								</ul>
								<!-- /.nav -->
							</div>
						</div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>
            <div aria-hidden="true" aria-labelledby="itemEnquiryModal" role="dialog" tabindex="-1" id="itemEnquiryModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-lg">
				  <div class="modal-content">				  
					<div class="modal-header">
					  <button aria-hidden="true" data-dismiss="modal" class="close" type="button" onclick="closeEnquiry()"><i class="icon_close fa-lg"></i></button>
					  <h4 class="modal-title capitalize-text">Item Enquiry</h4>
                        					  <div class="row" style="display:none;" id="divenquiry">
						<div class="col-md-4">
							<p>Item Reference: <span id="enquiryReference"></span></p>
						</div>		
                        <div class="col-md-4">
							<p>Item Status: <span id="enquiryStatus"></span></p>
						</div>	
						<div class="col-md-4">

						</div>				
					  </div>	
					</div>
					<div class="modal-body">
                       
						<div class="row">
							<div class="col-md-4">
                                 <div data-allow-page-scroll="true" data-color="#CCD1D9" data-toggle="slimScroll" style="height:500px;">	
                                                                                                 <div class="row" style="display:none;" id="divenquiryActivity">                                    								
                                                                                                    
                                                                  	<div class="col-md-12">
                                        <p class="red-color">Activity:</p>
										<p id="enquiryActivity"></p>
									</div>
									
								
                                                    </div>             
                                        <div class="row" style="display:none;" id="divenquiryRef">                                    								
                                                                                                    
                                                                  	<div class="col-md-12">
                                        <p class="red-color">Reference:</p>
										<p id="enquiryRef"></p>
									</div>
									
								
                                                    </div>                                  								<div class="row">
                                                                  	<div class="col-md-12">
                                        <p style="margin-left:5px;" class="red-color">*Enquirer Name:</p>
										<input   id="enquirytbName" class="form-control">
									</div>
									
								</div>
                                                                								<div class="row">
                                                                  	<div class="col-md-12">
                                       <p style="margin-left:5px;" class="red-color">*Enquirer Address:</p>
										<input   id="enquirytbAddress" class="form-control">
									</div>
									
								</div>
                                                                                                								<div class="row">
                                                                  	<div class="col-md-12">
                                       <p style="margin-left:5px;" class="red-color">*Enquirer Contact Tel:</p>
										<input   id="enquirytbContact" class="form-control">
									</div>
								</div>
                                                                                                                                								<div class="row">
                                                                  	<div class="col-md-12">
                                       <p style="margin-left:5px;" class="red-color">Enquirer Room Number:</p>
										<input   id="enquirytbRoom" class="form-control">
									</div>
								</div>
                                <div class="row">
                                    <div class="col-md-12">
                                     <p style="margin-left:5px;" class="red-color">Item Lost Location:</p>
                                    	<label class="select select-o">
											<select id="enquiryLocationSelect" runat="server">
											</select>
										 </label>
                                        </div>
								</div>
                                                                <div class="row">
                                    <div class="col-md-12">
                                     <p style="margin-left:5px;" class="red-color">Item Type:</p>
                                    	<label class="select select-o">
											<select id="enquiryTypeSelect" onchange="enquiryTypeSubSelectChange(this)" runat="server">
											</select>
										 </label>
                                        </div>
								</div>
                                <div class="row">
                                    <div class="col-md-12">
                                     <p style="margin-left:5px;" class="red-color">Item Sub Type:</p>
                                    	<label class="select select-o">
											<select id="enquiryTypeSubSelect" >
                                                <option>Please Select</option>
											</select>
										 </label>
                                        </div>
								</div>
								<div class="row">
                                                                  	<div class="col-md-12">
                                        <p style="margin-left:5px;" class="red-color">*Item Brand:</p>
										<input   id="enquirytbBrand" class="form-control">
									</div>
									
								</div>
								<div class="row">
									<div class="col-md-12">
                                        <p style="margin-left:5px;" class="red-color">Item Colour:</p>
										<label class="select select-o">
											<select id="enquiryColourSelect" runat="server">
											</select>
										 </label>
									</div>
								</div>
                                								<div class="row">
                                                                  	<div class="col-md-12">
                                        <p style="margin-left:5px;" class="red-color">Item Description:</p>
										<input   id="enquirytbDesc" class="form-control">
									</div>
									
								</div>
                                       <div class="slimScrollBar" style="background: rgb(204, 209, 217) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 273.07px;"></div>
											    <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>																					

                                </div>
							</div>
							<div class="col-md-8" >
                                 <div  data-fill-color="true" class="panel fade in panel-default panel-table panel-datatable" data-init-panel="true">
                                            <div class="panel-heading">
                                                <div class="row no-gutter">
                                                    <div class="col-md-8">
                                                        <h3 class="panel-title capitalize-text">SEARCH RESULTS</h3>
                                                    </div>
                                                    <div class="col-md-4 mt-2x " style="display:none;">
                                                        <input type="search" class="form-control white-color mt-1x datatable-search" placeholder="Search"><i class="fa fa-search fa-1x white-color"></i>
                                                        <div class="clearfx"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed table-noborder table-striped bordered-top datatable-table" number-of-rows="5" id="searchTableEnq" role="grid">
                                                        <thead>
                                                            <tr role="row">
                                                                                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="USER">DATE<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="NAME">REF#<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="STATUS">STATUS<i class="fa fa-sort ml-2x"></i>
                                                                </th>
                                                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" aria-label="ACTION">ACTION
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
						    </div>
						
					</div>
					<div class="modal-footer">
						<div class="row horizontal-navigation">
							<div class="panel-control">
								<ul class="nav nav-tabs">
                                    <li class="active" id="saveEDiv"><a href="#" class="capitalize-text" onclick="saveEnquiry()">SAVE</a>
									</li>
                                    <li class="active" id="searchEDiv" style="display:none"><a href="#" class="capitalize-text" onclick="searchEnquiry()">SEARCH</a>
									</li>
                                     <li class="active" id="closeEDiv" style="display:none"><a href="#" class="capitalize-text" onclick="closeEnquiry()">ENQUIRY CLOSE</a>
									</li>
								</ul> 
								<!-- /.nav -->
							</div>
						</div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>
            </div>
           <div aria-hidden="true" aria-labelledby="editFinderDepModal" role="dialog" tabindex="-1" id="editFinderDepModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">				  
					<div class="modal-header">
					  <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
					  <h4 class="modal-title capitalize-text">EDIT FINDER DEPARTMENT</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-12">
										<input placeholder="Name" id="tbEditFinderDepartmentName" class="form-control">
									</div>
								</div>		
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<div class="row horizontal-navigation">
							<div class="panel-control">
								<ul class="nav nav-tabs">
<%--									<li class="active"><a href="#" class="capitalize-text" data-target="#newDocument2" data-toggle="modal" onclick="$('#newDocument').modal('hide')">Next</a>
									</li>--%>
                                    <li ><a href="#" onclick="editFinderDepartmentSave()"  class="capitalize-text" >SAVE</a>
									</li>
								</ul>
								<!-- /.nav -->
							</div>
						</div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>	
            <div aria-hidden="true" aria-labelledby="editVehicleColorModal" role="dialog" tabindex="-1" id="editVehicleColorModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">				  
					<div class="modal-header">
					  <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
					  <h4 class="modal-title capitalize-text">EDIT COLOR</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-12">
										<input placeholder="Name" id="tbEditVehicleColorName" class="form-control">
									</div>
								</div>
								<div class="row" style="display:none;">
									<div class="col-md-12">
										<input placeholder="Arabic" id="tbEditArVehicleColorName" class="form-control">
									</div>
								</div>			
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<div class="row horizontal-navigation">
							<div class="panel-control">
								<ul class="nav nav-tabs">
<%--									<li class="active"><a href="#" class="capitalize-text" data-target="#newDocument2" data-toggle="modal" onclick="$('#newDocument').modal('hide')">Next</a>
									</li>--%>
                                    <li ><a href="#" onclick="editVehColorSave()"  class="capitalize-text" >SAVE</a>
									</li>
								</ul>
								<!-- /.nav -->
							</div>
						</div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>	
                                                <div aria-hidden="true" aria-labelledby="deleteAssetItem" role="dialog" tabindex="-1" id="deleteAssetItem" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">
					<div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        </div>
                            <div class="row">
							<div class="text-center">
                                <a><i class='fa fa-trash fa-4x' style="color:gray"></i></a>
                            </div>
                         </div>
                        <div class="row">
                            <h4 style="color:gray" class="text-center">Are you sure you want to delete this entry?</h4>
                        </div>
                        <div class="row">
                            <p class="red-color text-center">*Note: There is no undo!*</p>
                        </div>
                        <div class="row">
						    <div class="horizontal-navigation ">
							    <div class="panel-control ">
								    <ul class="nav nav-tabs text-center">
									    <li><a href="#" data-dismiss="modal">CANCEL</a>
									    </li>	 
                                        <li><a href="#" data-dismiss="modal" onclick="deleteAp()"><i class='fa fa-trash'></i>DELETE</a>
									    </li>	
								    </ul>
								    <!-- /.nav -->
							    </div>
						    </div>
                        </div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>	
                <div aria-hidden="true" aria-labelledby="deletelostItem" role="dialog" tabindex="-1" id="deletelostItem" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">
					<div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        </div>
                            <div class="row">
							<div class="text-center">
                                <a><i class='fa fa-trash fa-4x' style="color:gray"></i></a>
                            </div>
                         </div>
                        <div class="row">
                            <h4 style="color:gray" class="text-center">Are you sure you want to delete this entry?</h4>
                        </div>
                        <div class="row">
                            <p class="red-color text-center">*Note: There is no undo!*</p>
                        </div>
                        <div class="row">
						    <div class="horizontal-navigation ">
							    <div class="panel-control ">
								    <ul class="nav nav-tabs text-center">
									    <li><a href="#" data-dismiss="modal">CANCEL</a>
									    </li>	 
                                        <li><a href="#" data-dismiss="modal" onclick="deleteItemLost()"><i class='fa fa-trash'></i>DELETE</a>
									    </li>	
								    </ul>
								    <!-- /.nav -->
							    </div>
						    </div>
                        </div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>	
                <div aria-hidden="true" aria-labelledby="deleteinquiryItem" role="dialog" tabindex="-1" id="deleteinquiryItem" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">
					<div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        </div>
                            <div class="row">
							<div class="text-center">
                                <a><i class='fa fa-trash fa-4x' style="color:gray"></i></a>
                            </div>
                         </div>
                        <div class="row">
                            <h4 style="color:gray" class="text-center">Are you sure you want to delete this entry?</h4>
                        </div>
                        <div class="row">
                            <p class="red-color text-center">*Note: There is no undo!*</p>
                        </div>
                        <div class="row">
						    <div class="horizontal-navigation ">
							    <div class="panel-control ">
								    <ul class="nav nav-tabs text-center">
									    <li><a href="#" data-dismiss="modal">CANCEL</a>
									    </li>	 
                                        <li><a href="#" data-dismiss="modal" onclick="deleteItemEnquiry()"><i class='fa fa-trash'></i>DELETE</a>
									    </li>	
								    </ul>
								    <!-- /.nav -->
							    </div>
						    </div>
                        </div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>	
                                            <div aria-hidden="true" aria-labelledby="deletefoundItem" role="dialog" tabindex="-1" id="deletefoundItem" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">
					<div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        </div>
                            <div class="row">
							<div class="text-center">
                                <a><i class='fa fa-trash fa-4x' style="color:gray"></i></a>
                            </div>
                         </div>
                        <div class="row">
                            <h4 style="color:gray" class="text-center">Are you sure you want to delete this entry?</h4>
                        </div>
                        <div class="row">
                            <p class="red-color text-center">*Note: There is no undo!*</p>
                        </div>
                        <div class="row">
						    <div class="horizontal-navigation ">
							    <div class="panel-control ">
								    <ul class="nav nav-tabs text-center">
									    <li><a href="#" data-dismiss="modal">CANCEL</a>
									    </li>	 
                                        <li><a href="#" data-dismiss="modal" onclick="deleteItemFound()"><i class='fa fa-trash'></i>DELETE</a>
									    </li>	
								    </ul>
								    <!-- /.nav -->
							    </div>
						    </div>
                        </div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>	
                            <div aria-hidden="true" aria-labelledby="deleteOffenceTypeModal" role="dialog" tabindex="-1" id="deleteOffenceTypeModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">
					<div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        </div>
                            <div class="row">
							<div class="text-center">
                                <a><i class='fa fa-trash fa-4x' style="color:gray"></i></a>
                            </div>
                         </div>
                        <div class="row">
                            <h4 style="color:gray" class="text-center">Are you sure you want to delete this entry?</h4>
                        </div>
                        <div class="row">
                            <p class="red-color text-center">*Note: There is no undo!*</p>
                        </div>
                        <div class="row">
						    <div class="horizontal-navigation ">
							    <div class="panel-control ">
								    <ul class="nav nav-tabs text-center">
									    <li><a href="#" data-dismiss="modal">CANCEL</a>
									    </li>	 
                                        <li><a href="#" data-dismiss="modal" onclick="delOffenceTypeSave()"><i class='fa fa-trash'></i>DELETE</a>
									    </li>	
								    </ul>
								    <!-- /.nav -->
							    </div>
						    </div>
                        </div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>			
            <div aria-hidden="true" aria-labelledby="deleteOffenceCatModal" role="dialog" tabindex="-1" id="deleteOffenceCatModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">
					<div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        </div>
                            <div class="row">
							<div class="text-center">
                                <a><i class='fa fa-trash fa-4x' style="color:gray"></i></a>
                            </div>
                         </div>
                        <div class="row">
                            <h4 style="color:gray" class="text-center">Are you sure you want to delete this entry?</h4>
                        </div>
                        <div class="row">
                            <p class="red-color text-center">*Note:It will also delete connected sub categories. There is no undo!*</p>
                        </div>
                        <div class="row">
						    <div class="horizontal-navigation ">
							    <div class="panel-control ">
                                    <ul class="nav nav-tabs text-center">
									    <li><a href="#" data-dismiss="modal">CANCEL</a>
									    </li>	
                                        <li><a href="#" data-dismiss="modal" onclick="delOffenceCatTypeSave()"><i class='fa fa-trash'></i>DELETE</a>
									    </li>	
								    </ul>
								    <!-- /.nav -->
							    </div>
						    </div>
                        </div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>	
                                                            <div aria-hidden="true" aria-labelledby="deleteAttachModalAsset2" role="dialog" tabindex="-1" id="deleteAttachModalAsset2" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">
					<div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button" onclick="jQuery('#newAssetModal').modal('show');"><i class="icon_close fa-lg"></i></button>
                        </div>
                            <div class="row">
							<div class="text-center">
                                <a><i class='fa fa-trash fa-4x' style="color:gray"></i></a>
                            </div>
                         </div>
                        <div class="row">
                            <h4 style="color:gray" class="text-center">Are you sure you want to delete this file?</h4>
                        </div>
                        <div class="row">
                            <p class="red-color text-center">*Note: There is no undo!*</p>
                        </div>
                        <div class="row">
						    <div class="horizontal-navigation ">
							    <div class="panel-control ">
                                    <ul class="nav nav-tabs text-center">
									    <li><a href="#" data-dismiss="modal" onclick="jQuery('#newAssetModal').modal('show');">CANCEL</a>
									    </li>	
                                        <li><a href="#" data-dismiss="modal" onclick="deleteAttachmentAsset2()"><i class='fa fa-trash'></i>DELETE</a>
									    </li>	
								    </ul>
								    <!-- /.nav -->
							    </div>
						    </div>
                        </div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>
                            <div aria-hidden="true" aria-labelledby="deleteAttachModalAsset" role="dialog" tabindex="-1" id="deleteAttachModalAsset" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">
					<div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button" onclick="jQuery('#ticketingViewCard').modal('show');"><i class="icon_close fa-lg"></i></button>
                        </div>
                            <div class="row">
							<div class="text-center">
                                <a><i class='fa fa-trash fa-4x' style="color:gray"></i></a>
                            </div>
                         </div>
                        <div class="row">
                            <h4 style="color:gray" class="text-center">Are you sure you want to delete this file?</h4>
                        </div>
                        <div class="row">
                            <p class="red-color text-center">*Note: There is no undo!*</p>
                        </div>
                        <div class="row">
						    <div class="horizontal-navigation ">
							    <div class="panel-control ">
                                    <ul class="nav nav-tabs text-center">
									    <li><a href="#" data-dismiss="modal" onclick="jQuery('#ticketingViewCard').modal('show');">CANCEL</a>
									    </li>	
                                        <li><a href="#" data-dismiss="modal" onclick="deleteAttachmentAsset()"><i class='fa fa-trash'></i>DELETE</a>
									    </li>	
								    </ul>
								    <!-- /.nav -->
							    </div>
						    </div>
                        </div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>
            <div aria-hidden="true" aria-labelledby="deleteAttachModal" role="dialog" tabindex="-1" id="deleteAttachModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">
					<div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button" onclick="jQuery('#foundItemViewCard').modal('show');"><i class="icon_close fa-lg"></i></button>
                        </div>
                            <div class="row">
							<div class="text-center">
                                <a><i class='fa fa-trash fa-4x' style="color:gray"></i></a>
                            </div>
                         </div>
                        <div class="row">
                            <h4 style="color:gray" class="text-center">Are you sure you want to delete this file?</h4>
                        </div>
                        <div class="row">
                            <p class="red-color text-center">*Note: There is no undo!*</p>
                        </div>
                        <div class="row">
						    <div class="horizontal-navigation ">
							    <div class="panel-control ">
                                    <ul class="nav nav-tabs text-center">
									    <li><a href="#" data-dismiss="modal" onclick="jQuery('#foundItemViewCard').modal('show');">CANCEL</a>
									    </li>	
                                        <li><a href="#" data-dismiss="modal" onclick="deleteAttachment()"><i class='fa fa-trash'></i>DELETE</a>
									    </li>	
								    </ul>
								    <!-- /.nav -->
							    </div>
						    </div>
                        </div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>
            <div aria-hidden="true" aria-labelledby="deleteVehColorModal" role="dialog" tabindex="-1" id="deleteVehColorModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">
					<div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        </div>
                            <div class="row">
							<div class="text-center">
                                <a><i class='fa fa-trash fa-4x' style="color:gray"></i></a>
                            </div>
                         </div>
                        <div class="row">
                            <h4 style="color:gray" class="text-center">Are you sure you want to delete this entry?</h4>
                        </div>
                        <div class="row">
                            <p class="red-color text-center">*Note: There is no undo!*</p>
                        </div>
                        <div class="row">
						    <div class="horizontal-navigation ">
							    <div class="panel-control ">
                                    <ul class="nav nav-tabs text-center">
									    <li><a href="#" data-dismiss="modal">CANCEL</a>
									    </li>	
                                        <li><a href="#" data-dismiss="modal" onclick="delVehColorSave()"><i class='fa fa-trash'></i>DELETE</a>
									    </li>	
								    </ul>
								    <!-- /.nav -->
							    </div>
						    </div>
                        </div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>	  
                           <div aria-hidden="true" aria-labelledby="deleteFinderModal" role="dialog" tabindex="-1" id="deleteFinderModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">
					<div class="modal-body" >
                        <div class="row">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        </div>
                            <div class="row">
							<div class="text-center">
                                <a><i class='fa fa-trash fa-4x' style="color:gray"></i></a>
                            </div>
                         </div>
                        <div class="row">
                            <h4 style="color:gray" class="text-center">Are you sure you want to delete this entry?</h4>
                        </div>
                        <div class="row">
                            <p class="red-color text-center">*Note: There is no undo!*</p>
                        </div>
                        <div class="row">
						    <div class="horizontal-navigation ">
							    <div class="panel-control ">
                                    <ul class="nav nav-tabs text-center">
									    <li><a href="#" data-dismiss="modal">CANCEL</a>
									    </li>	
                                        <li><a href="#" data-dismiss="modal" onclick="delFinderDep()"><i class='fa fa-trash'></i>DELETE</a>
									    </li>	
								    </ul>
								    <!-- /.nav -->
							    </div>
						    </div>
                        </div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>	 
                 <div aria-hidden="true" aria-labelledby="newFinderDeprModal" role="dialog" tabindex="-1" id="newFinderDeprModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">				  
					<div class="modal-header">
					  <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
					  <h4 class="modal-title capitalize-text">CREATE NEW FINDER DEPARTMENT</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-12">
										<input placeholder="Name" id="tbNewFinderDepartmentName" class="form-control">
									</div>
								</div>		
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<div class="row horizontal-navigation">
							<div class="panel-control">
								<ul class="nav nav-tabs">
<%--									<li class="active"><a href="#" class="capitalize-text" data-target="#newDocument2" data-toggle="modal" onclick="$('#newDocument').modal('hide')">Next</a>
									</li>--%>
                                    <li><a href="#"   onclick="addFinderDepartmentSave()" class="capitalize-text" >CREATE</a>
									</li>
								</ul>
								<!-- /.nav -->
							</div>
						</div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>	
                 <div aria-hidden="true" aria-labelledby="newVehicleColorModal" role="dialog" tabindex="-1" id="newVehicleColorModal" class="modal fade" style="display: none;">
				<div class="modal-dialog modal-sm">
				  <div class="modal-content">				  
					<div class="modal-header">
					  <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
					  <h4 class="modal-title capitalize-text">CREATE NEW COLOR</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-12">
										<input placeholder="Name" id="tbVehicleColorName" class="form-control">
									</div>
								</div>
								<div class="row" style="display:none;">
									<div class="col-md-12">
										<input placeholder="Arabic" id="tbArVehicleColorName" class="form-control">
									</div>
								</div>			
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<div class="row horizontal-navigation">
							<div class="panel-control">
								<ul class="nav nav-tabs">
<%--									<li class="active"><a href="#" class="capitalize-text" data-target="#newDocument2" data-toggle="modal" onclick="$('#newDocument').modal('hide')">Next</a>
									</li>--%>
                                    <li ><a href="#"   onclick="addVehColorSave()" class="capitalize-text" >CREATE</a>
									</li>
								</ul>
								<!-- /.nav -->
							</div>
						</div>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			 </div>	
            <select style="color:white;background-color: #5D5D5D;width:125px;height:105px;margin-top:-15px;display:none;" multiple="multiple" id="sendToListBox" ></select>				  
              <input id="tbOffenceIdChoice" style="display:none;">
                <input id="tbOffenceCatChoice" style="display:none;">
                <input id="tbVehicleMakeChoice" style="display:none;">
                <input id="tbVehicleColorChoice" style="display:none;">
                <input id="tbFinderDepChoice" style="display:none;">
                <input id="tbPlateCodeChoice" style="display:none;"> 
                <input id="tbPlateSourceChoice" style="display:none;">
                <input style="display:none;" id="imagePathAsset" type="text"/>
                         <input style="display:none;" id="imagePathPartAsset" type="text"/>

 <input style="display:none;" id="newimagePathPartAsset" type="text"/>
                <input style="display:none;" id="newAssetID" type="text"/>
<input style="display:none;" id="AssetPlanID" type="text"/>
                                <input style="display:none;" id="newPlanID" type="text"/>
                                
                                     <input style="display:none;" id="rowChoiceTasks" type="text"/>
                <input style="display:none;" id="imagePath" type="text"/>
                <input style="display:none;" id="imagePathDispose" type="text"/>
                <input style="display:none;" id="imagePathReturn" type="text"/>
                <input style="display:none;" id="imagePostAttachment" type="text"/>
                                <input id="rowidChoice" style="display:none;">
                                <input id="rowidChoiceOld" style="display:none;">
                <input id="rowidChoiceENQ" value="0" style="display:none;">
                <input id="rowidChoiceAttachment" value="0" style="display:none;">
              <asp:HiddenField runat="server" ID="tbUserID" />
              <asp:HiddenField runat="server" ID="tbUserName" />
                             <input style="display:none;" id="currentdate" type="text"/>
             </section>
</asp:Content>
