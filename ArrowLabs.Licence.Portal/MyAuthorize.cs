﻿using Arrowlabs.Business.Layer;
using ArrowLabs.Licence.Portal.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ArrowLabs.Licence.Portal
{
    public class MyAuthorize 
    {
        public  bool AuthorizeCore(HttpContext httpContext)
        {
            if (httpContext == null) throw new ArgumentNullException("httpContext");
            if (httpContext.User.Identity.IsAuthenticated == false) return false;
            bool allow = CommonUtility.HandleConcurrentSession(
                System.Web.HttpContext.Current.User.Identity.Name.ToLower(),System.Web.HttpContext.Current.Session.SessionID);
                //CommonUtility.GetCurrentUserSession((ClaimsIdentity)HttpContext.Current.User.Identity));
            return allow;
            //return true;
        }
        public  void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (!filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
               // base.HandleUnauthorizedRequest(filterContext);
            }
            else
            {
              //  filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Error", action = "Index" }));
            }
        }
    }
}