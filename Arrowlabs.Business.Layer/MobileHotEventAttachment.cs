﻿// -----------------------------------------------------------------------
// <copyright file="MobileHotEventAttachment.cs" company="Microsoft">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Arrowlabs.Business.Layer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Runtime.Serialization;
    using System.Data.SqlClient;
    using System.Data;
    using Arrowlabs.Mims.Audit.Models;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    /// 
    [Serializable]
    [DataContract]
    public class MobileHotEventAttachment
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int HotEventId { get; set; }

        [DataMember]
        public int EventId { get; set; }
        

        [DataMember]
        public byte[] Attachment { get; set; }
        [DataMember]
        public string AttachmentPath { get; set; }
        [DataMember]
        public string AttachmentName { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public string AccountName { get; set; }
        [DataMember]
        public DateTime? CreatedDate { get; set; }
        [DataMember]
        public string UpdatedBy { get; set; }
        [DataMember]
        public DateTime? UpdatedDate { get; set; }
        public int SiteId { get; set; }

        public int CustomerId { get; set; }

        [DataMember]
        public string Password { get; set; }
        [DataMember]
        public string SessionId { get; set; }

        public bool IsPortal { get; set; }

        public bool IsVideo { get; set; }

        private static MobileHotEventAttachment GetMobileHotEventAttachmentFromSqlReader(SqlDataReader reader)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
            var hotEvent = new MobileHotEventAttachment();

            if (reader["Id"] != DBNull.Value)
                hotEvent.Id = Convert.ToInt32(reader["Id"].ToString());
            if (reader["HotEventId"] != DBNull.Value)
                hotEvent.HotEventId = Convert.ToInt32(reader["HotEventId"].ToString());

            if (reader["CreatedBy"] != DBNull.Value)
                hotEvent.CreatedBy = reader["CreatedBy"].ToString();
            if (reader["CreatedDate"] != DBNull.Value)
                hotEvent.CreatedDate = Convert.ToDateTime(reader["CreatedDate"]);
            if (reader["UpdatedBy"] != DBNull.Value)
                hotEvent.UpdatedBy = reader["UpdatedBy"].ToString();
            if (reader["UpdatedDate"] != DBNull.Value)
                hotEvent.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"]);
            if (reader["Attachment"] != DBNull.Value)
                hotEvent.Attachment = (byte[])reader["Attachment"];
            if (reader["AttachmentPath"] != DBNull.Value)
                hotEvent.AttachmentPath = reader["AttachmentPath"].ToString();

            if (columns.Contains( "EventId"))
            {
                if (reader["EventId"] != DBNull.Value)
                    hotEvent.EventId = Convert.ToInt32(reader["EventId"].ToString());
            }
            if (columns.Contains("AttachmentName"))
            {
                if (reader["AttachmentName"] != DBNull.Value)
                    hotEvent.AttachmentName = reader["AttachmentName"].ToString();
            }
            

            if (columns.Contains( "SiteId") & reader["SiteId"] != DBNull.Value)
                hotEvent.SiteId = Convert.ToInt32(reader["SiteId"].ToString());

            if (columns.Contains("CustomerId") & reader["CustomerId"] != DBNull.Value)
                hotEvent.CustomerId = Convert.ToInt32(reader["CustomerId"].ToString());

            if (columns.Contains("IsVideo"))
            {
                if (reader["IsVideo"] != DBNull.Value)
                    hotEvent.IsVideo = Convert.ToBoolean(reader["IsVideo"].ToString());
            }

            if (reader["IsPortal"] != DBNull.Value)
                hotEvent.IsPortal = Convert.ToBoolean(reader["IsPortal"].ToString());
         
            return hotEvent;
        }

        public static List<MobileHotEventAttachment> GetAllMobileHotEventAttachments(string dbConnection)
        {
            var hotEvents = new List<MobileHotEventAttachment>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetAllMobileHotEventAttachments";
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var hotEvent = GetMobileHotEventAttachmentFromSqlReader(reader);
                    hotEvents.Add(hotEvent);
                }
                connection.Close();
                reader.Close();
            }
            return hotEvents;
        }

        public static List<MobileHotEventAttachment> GetMobileHotEventAttachmentById(int id, string dbConnection)
        {
            var hotEvents = new List<MobileHotEventAttachment>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetMobileHotEventAttachmentById";
                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var hotEvent = GetMobileHotEventAttachmentFromSqlReader(reader);
                    hotEvents.Add(hotEvent);
                }
                connection.Close();
                reader.Close();
            }
            return hotEvents;
        }

        public static bool DeleteMobileHotEventAttachmentById(int locationId, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var cmd = new SqlCommand("DeleteMobileHotEventAttachmentById", connection);

                cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                cmd.Parameters["@Id"].Value = locationId;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.ExecuteNonQuery();
            }
            return true;
        }
        

        public static List<MobileHotEventAttachment> GetMobileHotEventAttachmentByEventId(int id, string dbConnection)
        {
            var hotEvents = new List<MobileHotEventAttachment>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetMobileHotEventAttachmentByEventId";
                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var hotEvent = GetMobileHotEventAttachmentFromSqlReader(reader);
                    hotEvents.Add(hotEvent);
                }
                connection.Close();
                reader.Close();
            }
            return hotEvents;
        }

        public static List<MobileHotEventAttachment> GetMobileHotEventAttachmentByHotEventId(int hotEventId, string dbConnection)
        {
            var hotEvents = new List<MobileHotEventAttachment>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetMobileHotEventAttachmentByHotEventId";
                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlCommand.Parameters.Add(new SqlParameter("@HotEventId", SqlDbType.Int));
                sqlCommand.Parameters["@HotEventId"].Value = hotEventId;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var hotEvent = GetMobileHotEventAttachmentFromSqlReader(reader);
                    hotEvents.Add(hotEvent);
                }
                connection.Close();
                reader.Close();
            }
            return hotEvents;
        }

        public static bool InsertOrUpdateMobileHotEventAttachment(MobileHotEventAttachment hotEventAttachment, string dbConnection
            , string auditDbConnection = "", bool isAuditEnabled = true)
        {
            int id = hotEventAttachment.Id;
            var action = (id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;

            if (hotEventAttachment != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateMobileHotEventAttachment", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = hotEventAttachment.Id;

                    cmd.Parameters.Add(new SqlParameter("@HotEventId", SqlDbType.Int));
                    cmd.Parameters["@HotEventId"].Value = hotEventAttachment.HotEventId;

                    cmd.Parameters.Add(new SqlParameter("@Attachment", SqlDbType.VarBinary));
                    cmd.Parameters["@Attachment"].Value = hotEventAttachment.Attachment;

                    cmd.Parameters.Add(new SqlParameter("@AttachmentPath", SqlDbType.NVarChar));
                    cmd.Parameters["@AttachmentPath"].Value = hotEventAttachment.AttachmentPath;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = hotEventAttachment.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    if (hotEventAttachment.Id == 0)
                    {
                        cmd.Parameters["@CreatedDate"].Value = DateTime.Now;
                    }
                    else
                    {
                        cmd.Parameters["@CreatedDate"].Value = hotEventAttachment.CreatedDate;
                    }
                    
                    cmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@UpdatedBy"].Value = hotEventAttachment.UpdatedBy;

                    cmd.Parameters.Add(new SqlParameter("@AttachmentName", SqlDbType.NVarChar));
                                        cmd.Parameters["@AttachmentName"].Value = hotEventAttachment.AttachmentName;
                    

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = DateTime.Now;
                   
                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value =hotEventAttachment.SiteId;

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = hotEventAttachment.CustomerId;
                     
                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();

                    if (id == 0)
                        id = (int)returnParameter.Value;
                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            hotEventAttachment.Id = id;
                            var audit = new MobileHotEventAttachmentAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, hotEventAttachment.UpdatedBy);
                            Reflection.CopyProperties(hotEventAttachment, audit);
                            MobileHotEventAttachmentAudit.InsertMobileHotEventAttachmentAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertOrUpdateMobileHotEventAttachment", ex, dbConnection);
                    }
                }
            }
            return true;
        }

        public static bool InsertOrUpdateIncidentMobileAttachment(MobileHotEventAttachment hotEventAttachment, string dbConnection
            , string auditDbConnection = "", bool isAuditEnabled = true)
        {
            int id = hotEventAttachment.Id;
            var action = (id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;

            if (hotEventAttachment != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateIncidentMobileAttachment", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = hotEventAttachment.Id;

                    cmd.Parameters.Add(new SqlParameter("@HotEventId", SqlDbType.Int));
                    cmd.Parameters["@HotEventId"].Value = hotEventAttachment.HotEventId;

                    cmd.Parameters.Add(new SqlParameter("@Attachment", SqlDbType.VarBinary));
                    cmd.Parameters["@Attachment"].Value = hotEventAttachment.Attachment;

                    cmd.Parameters.Add(new SqlParameter("@AttachmentPath", SqlDbType.NVarChar));
                    cmd.Parameters["@AttachmentPath"].Value = hotEventAttachment.AttachmentPath;

                    cmd.Parameters.Add(new SqlParameter("@AttachmentName", SqlDbType.NVarChar));
                    cmd.Parameters["@AttachmentName"].Value = hotEventAttachment.AttachmentName;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = hotEventAttachment.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    if (hotEventAttachment.Id == 0)
                    {
                        cmd.Parameters["@CreatedDate"].Value = DateTime.Now;
                    }
                    else
                    {
                        cmd.Parameters["@CreatedDate"].Value = hotEventAttachment.CreatedDate;
                    }
                    
                    cmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@UpdatedBy"].Value = hotEventAttachment.UpdatedBy;

                    cmd.Parameters.Add(new SqlParameter("@IsPortal", SqlDbType.Bit));
                    cmd.Parameters["@IsPortal"].Value = hotEventAttachment.IsPortal;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = DateTime.Now;

                    cmd.Parameters.Add(new SqlParameter("@EventId", SqlDbType.Int));
                    cmd.Parameters["@EventId"].Value = hotEventAttachment.EventId;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = hotEventAttachment.SiteId;

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = hotEventAttachment.CustomerId;

                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();

                    if (id == 0)
                        id = (int)returnParameter.Value;
                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            hotEventAttachment.Id = id;
                            var audit = new MobileHotEventAttachmentAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, hotEventAttachment.UpdatedBy);
                            Reflection.CopyProperties(hotEventAttachment, audit);
                            MobileHotEventAttachmentAudit.InsertMobileHotEventAttachmentAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertOrUpdateIncidentMobileAttachment", ex, dbConnection);
                    }
                }
            }
            return true;
        }

        public static List<MobileHotEventAttachment> GetMobileHotEventAttachmentByCreatedUser(string name, string dbConnection)
        {
            var hotEvents = new List<MobileHotEventAttachment>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetMobileHotEventAttachmentByCreatedUser";
                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = name;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var hotEvent = GetMobileHotEventAttachmentFromSqlReader(reader);
                    hotEvents.Add(hotEvent);
                }
                connection.Close();
                reader.Close();
            }
            return hotEvents;
        }
        public static List<MobileHotEventAttachment> GetMobileHotEventAttachmentBySiteId(int siteId, string dbConnection)
        {
            var hotEvents = new List<MobileHotEventAttachment>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetMobileHotEventAttachmentBySiteId";
                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                sqlCommand.Parameters["@SiteId"].Value = siteId;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var hotEvent = GetMobileHotEventAttachmentFromSqlReader(reader);
                    hotEvents.Add(hotEvent);
                }
                connection.Close();
                reader.Close();
            }
            return hotEvents;
        }
    }
}
