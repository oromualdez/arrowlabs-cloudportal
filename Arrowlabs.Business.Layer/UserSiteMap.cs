﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Arrowlabs.Business.Layer
{
    [Serializable]
    public class UserSiteMap
    {
        public int UserId { get; set; }
        public string AccountName { get; set; }
        public string UserName { get; set; }
        public int SiteId { get; set; }
        public string SiteName { get; set; }
        public int CustomerId { get; set; }
        public static List<UserSiteMap> GetAllUserSiteMapByUsername(string Name, string dbConnection)
        {
            var modList = new List<UserSiteMap>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllUserSiteMapByUsername", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = Name;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllUserSiteMapByUsername";
                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    var userClass = GetFromSqlReader(reader);
                    modList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return modList;
        }
        public static UserSiteMap GetFromSqlReader(SqlDataReader reader)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
            var Class = new UserSiteMap();

            if (!string.IsNullOrEmpty(reader["UserId"].ToString()))
                Class.UserId = Convert.ToInt32(reader["UserId"].ToString());
            if (!string.IsNullOrEmpty(reader["SiteId"].ToString()))
                Class.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
            if (!string.IsNullOrEmpty(reader["CustomerId"].ToString()))
                Class.CustomerId = Convert.ToInt32(reader["CustomerId"].ToString());


            Class.AccountName = reader["AccountName"].ToString();
            Class.UserName = reader["UserName"].ToString();

            if (columns.Contains("SiteName") && reader["SiteName"] != DBNull.Value)
                Class.SiteName = reader["SiteName"].ToString();

            return Class;
        }
        public static void DeleteUserSiteMapByUserId(int userid, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteUserSiteMapByUserId", connection);


                cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                cmd.Parameters["@UserId"].Value = userid;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteUserSiteMapByUserId";

                cmd.ExecuteNonQuery();
            }
        }
        public static bool InsertUserSiteMap(int UserId, int SiteId, int CustomerId, string dbConnection, string username)
        {
            if (UserId > 0 && SiteId > 0)
            {


                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertUserSiteMap", connection);

                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                    cmd.Parameters["@UserId"].Value = UserId;

                    cmd.Parameters.Add(new SqlParameter("@UserName", SqlDbType.NVarChar));
                    cmd.Parameters["@UserName"].Value = username;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = SiteId;

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = CustomerId;


                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.CommandText = "InsertUserSiteMap";

                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }
    }
}
