﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Arrowlabs.Business.Layer;
using System.Configuration;
using ArrowLabs.Licence.Portal.Helpers;

namespace ArrowLabs.Licence.Portal.Account
{
    public partial class ChangePassword : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dbConnection = CommonUtility.dbConnection;
            ChangeUserPassword.ChangingPassword += new LoginCancelEventHandler(this.ChangePasswordPushButton_Click);
        }
        string dbConnection { get; set; }

        protected void ChangePasswordPushButton_Click(object sender, LoginCancelEventArgs e)
        {
            var superAdmin = Users.GetUserByName(ChangeUserPassword.UserName, dbConnection);
            if (superAdmin.Username == ChangeUserPassword.UserName)
            {
                if (ChangeUserPassword.CurrentPassword == Decrypt.DecryptData(superAdmin.Password, true, dbConnection))
                {
                    if (ChangeUserPassword.CurrentPassword.ToString() != ChangeUserPassword.NewPassword.ToString())
                    {
                        if (ChangeUserPassword.NewPassword == ChangeUserPassword.ConfirmNewPassword)
                        {
                            superAdmin.Password = Encrypt.EncryptData(ChangeUserPassword.NewPassword, true, dbConnection);
                            superAdmin.UpdatedDate = DateTime.Now;
                            superAdmin.UpdatedBy = ChangeUserPassword.UserName;
                            Users.InsertOrUpdateUsers(superAdmin, dbConnection);
                            Response.Redirect("~/Pages/About.aspx");
                            e.Cancel = true;
                        }
                    }
                    else
                    {
                        e.Cancel = false;
                    }
                }
            }
        }
    }
}
