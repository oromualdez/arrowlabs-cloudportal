﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class GeofenceLocationAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public int Id { get; set; }
        public int LocationId { get; set; }
        public int IncidentId { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string LocationDesc { get; set; }
        public string IncidentName { get; set; }
        public int SiteId { get; set; }

        public int CustomerId { get; set; }

        public static bool InsertOrUpdateGeofenceLocation(GeofenceLocationAudit fenceLocation, string dbConnection)
        {
            var baseAuditId = BaseAudit.InsertBaseAudit(fenceLocation.BaseAudit, dbConnection);

            if (fenceLocation != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateGeofenceLocation", connection);

                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = fenceLocation.Id;

                    cmd.Parameters.Add(new SqlParameter("@LocationId", SqlDbType.Int));
                    cmd.Parameters["@LocationId"].Value = fenceLocation.LocationId;

                    cmd.Parameters.Add(new SqlParameter("@IncidentId", SqlDbType.Int));
                    cmd.Parameters["@IncidentId"].Value = fenceLocation.IncidentId;

                    cmd.Parameters.Add(new SqlParameter("@Latitude", SqlDbType.Float));
                    cmd.Parameters["@Latitude"].Value = fenceLocation.Latitude;

                    cmd.Parameters.Add(new SqlParameter("@Longitude", SqlDbType.Float));
                    cmd.Parameters["@Longitude"].Value = fenceLocation.Longitude;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = fenceLocation.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = fenceLocation.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = fenceLocation.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = fenceLocation.SiteId;

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = fenceLocation.CustomerId;

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }
    }
}
