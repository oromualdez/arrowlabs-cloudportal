﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using Arrowlabs.Mims.Audit.Models;

namespace Arrowlabs.Business.Layer
{
    public class VehicleColor
    {
        public int ColorCode { get; set; }
        public string ColorDesc { get; set; }
        public string ColorDesc_AR { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public string CreatedBy { get; set; }
        public int AccountId { get; set; }
        public string AccountName { get; set; }
        public DatabaseOperation Operation { get; set; }

        public int CustomerId { get; set; }

        public int SiteId { get; set; }

        public static VehicleColor GetVehicleColorByName(string name, string dbConnection)
        {
            VehicleColor plateCode = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetVehicleColorByName", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = name;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    plateCode = new VehicleColor();
                    plateCode.ColorCode = Convert.ToInt32(reader["COLORCODE"].ToString());
                    plateCode.ColorDesc = reader["COLORDESC"].ToString();
                    plateCode.ColorDesc_AR = reader["COLORDESC_AR"].ToString();
                    plateCode.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    plateCode.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                    plateCode.CreatedBy = reader["CreatedBy"].ToString().ToLower();
                    if (columns.Contains( "AccountId"))
                        plateCode.AccountId = Convert.ToInt32(reader["AccountId"]);

                    if (columns.Contains("SiteId"))
                        plateCode.SiteId = Convert.ToInt32(reader["SiteId"]);

                    if (columns.Contains("CustomerId"))
                        plateCode.CustomerId = Convert.ToInt32(reader["CustomerId"]);

                    if (columns.Contains( "AccountName"))
                        plateCode.AccountName = reader["AccountName"].ToString();

                }
                connection.Close();
                reader.Close();
            }
            return plateCode;
        }

        public static VehicleColor GetVehicleColorById(int id, string dbConnection)
        {
            VehicleColor plateCode = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetVehicleColorById", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    plateCode = new VehicleColor();
                    plateCode.ColorCode = Convert.ToInt32(reader["COLORCODE"].ToString());
                    plateCode.ColorDesc = reader["COLORDESC"].ToString();
                    plateCode.ColorDesc_AR = reader["COLORDESC_AR"].ToString();
                    plateCode.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    plateCode.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                    plateCode.CreatedBy = reader["CreatedBy"].ToString().ToLower();
                    if (columns.Contains("AccountId"))
                        plateCode.AccountId = Convert.ToInt32(reader["AccountId"]);

                    if (columns.Contains("SiteId"))
                        plateCode.SiteId = Convert.ToInt32(reader["SiteId"]);

                    if (columns.Contains("CustomerId"))
                        plateCode.CustomerId = Convert.ToInt32(reader["CustomerId"]);

                    if (columns.Contains("AccountName"))
                        plateCode.AccountName = reader["AccountName"].ToString();

                }
                connection.Close();
                reader.Close();
            }
            return plateCode;
        }

        public static VehicleColor GetVehicleColorByNameAndCId(string name,int id ,string dbConnection)
        {
            VehicleColor plateCode = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetVehicleColorByNameAndCId", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = name;

                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    plateCode = new VehicleColor();
                    plateCode.ColorCode = Convert.ToInt32(reader["COLORCODE"].ToString());
                    plateCode.ColorDesc = reader["COLORDESC"].ToString();
                    plateCode.ColorDesc_AR = reader["COLORDESC_AR"].ToString();
                    plateCode.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    plateCode.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                    plateCode.CreatedBy = reader["CreatedBy"].ToString().ToLower();
                    if (columns.Contains( "AccountId"))
                        plateCode.AccountId = Convert.ToInt32(reader["AccountId"]);

                    if (columns.Contains( "AccountName"))
                        plateCode.AccountName = reader["AccountName"].ToString();

                    if (columns.Contains("SiteId"))
                        plateCode.SiteId = Convert.ToInt32(reader["SiteId"]);

                    if (columns.Contains("CustomerId"))
                        plateCode.CustomerId = Convert.ToInt32(reader["CustomerId"]);

                }
                connection.Close();
                reader.Close();
            }
            return plateCode;
        }

        public static VehicleColor GetVehicleColorByNameAndCIdAndSiteId(string name, int id, int siteid, string dbConnection)
        {
            VehicleColor plateCode = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetVehicleColorByNameAndCIdAndSiteId", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = name;

                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;

                sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                sqlCommand.Parameters["@SiteId"].Value = siteid;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    plateCode = new VehicleColor();
                    plateCode.ColorCode = Convert.ToInt32(reader["COLORCODE"].ToString());
                    plateCode.ColorDesc = reader["COLORDESC"].ToString();
                    plateCode.ColorDesc_AR = reader["COLORDESC_AR"].ToString();
                    plateCode.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    plateCode.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                    plateCode.CreatedBy = reader["CreatedBy"].ToString().ToLower();
                    if (columns.Contains("AccountId"))
                        plateCode.AccountId = Convert.ToInt32(reader["AccountId"]);

                    if (columns.Contains("AccountName"))
                        plateCode.AccountName = reader["AccountName"].ToString();

                    if (columns.Contains("SiteId"))
                        plateCode.SiteId = Convert.ToInt32(reader["SiteId"]);

                    if (columns.Contains("CustomerId"))
                        plateCode.CustomerId = Convert.ToInt32(reader["CustomerId"]);

                }
                connection.Close();
                reader.Close();
            }
            return plateCode;
        }

        public static List<VehicleColor> GetAllVehicleColor(string dbConnection)
        {
            var plateCodes = new List<VehicleColor>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllVehicleColor", connection);


                sqlCommand.CommandText = "GetAllVehicleColor";

                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    var plateCode = new VehicleColor();

                    plateCode.ColorCode = Convert.ToInt32(reader["COLORCODE"].ToString());
                    plateCode.ColorDesc = reader["COLORDESC"].ToString();
                    plateCode.ColorDesc_AR = reader["COLORDESC_AR"].ToString();
                    plateCode.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    plateCode.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                    plateCode.CreatedBy = reader["CreatedBy"].ToString().ToLower();
                    if (columns.Contains( "AccountId"))
                        plateCode.AccountId = Convert.ToInt32(reader["AccountId"]);

                    if (columns.Contains( "AccountName"))
                        plateCode.AccountName = reader["AccountName"].ToString();

                    if (columns.Contains("SiteId"))
                        plateCode.SiteId = Convert.ToInt32(reader["SiteId"]);

                    if (columns.Contains("CustomerId"))
                        plateCode.CustomerId = Convert.ToInt32(reader["CustomerId"]);

                    plateCodes.Add(plateCode);
                }
                connection.Close();
                reader.Close();
            }
            return plateCodes;
        }
        public static List<VehicleColor> GetAllVehicleColorByCId(int id,string dbConnection)
        {
            var plateCodes = new List<VehicleColor>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllVehicleColorByCId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllVehicleColorByCId";

                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    var plateCode = new VehicleColor();

                    plateCode.ColorCode = Convert.ToInt32(reader["COLORCODE"].ToString());
                    plateCode.ColorDesc = reader["COLORDESC"].ToString();
                    plateCode.ColorDesc_AR = reader["COLORDESC_AR"].ToString();
                    plateCode.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    plateCode.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                    plateCode.CreatedBy = reader["CreatedBy"].ToString().ToLower();
                    if (columns.Contains( "AccountId"))
                        plateCode.AccountId = Convert.ToInt32(reader["AccountId"]);

                    if (columns.Contains( "AccountName"))
                        plateCode.AccountName = reader["AccountName"].ToString();

                    if (columns.Contains("SiteId"))
                        plateCode.SiteId = Convert.ToInt32(reader["SiteId"]);

                    if (columns.Contains("CustomerId"))
                        plateCode.CustomerId = Convert.ToInt32(reader["CustomerId"]);

                    plateCodes.Add(plateCode);
                }
                connection.Close();
                reader.Close();
            }
            return plateCodes;
        }

        public static List<VehicleColor> GetAllVehicleColorBySiteId(int id, string dbConnection)
        {
            var plateCodes = new List<VehicleColor>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllVehicleColorBySiteId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllVehicleColorBySiteId";

                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    var plateCode = new VehicleColor();

                    plateCode.ColorCode = Convert.ToInt32(reader["COLORCODE"].ToString());
                    plateCode.ColorDesc = reader["COLORDESC"].ToString();
                    plateCode.ColorDesc_AR = reader["COLORDESC_AR"].ToString();
                    plateCode.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    plateCode.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                    plateCode.CreatedBy = reader["CreatedBy"].ToString().ToLower();
                    if (columns.Contains("AccountId"))
                        plateCode.AccountId = Convert.ToInt32(reader["AccountId"]);

                    if (columns.Contains("AccountName"))
                        plateCode.AccountName = reader["AccountName"].ToString();

                    if (columns.Contains("SiteId"))
                        plateCode.SiteId = Convert.ToInt32(reader["SiteId"]);

                    if (columns.Contains("CustomerId"))
                        plateCode.CustomerId = Convert.ToInt32(reader["CustomerId"]);

                    plateCodes.Add(plateCode);
                }
                connection.Close();
                reader.Close();
            }
            return plateCodes;
        }

        public static List<VehicleColor> GetAllVehicleColorByLevel5(int id, string dbConnection)
        {
            var plateCodes = new List<VehicleColor>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllVehicleColorByLevel5", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllVehicleColorByLevel5";

                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    var plateCode = new VehicleColor();

                    plateCode.ColorCode = Convert.ToInt32(reader["COLORCODE"].ToString());
                    plateCode.ColorDesc = reader["COLORDESC"].ToString();
                    plateCode.ColorDesc_AR = reader["COLORDESC_AR"].ToString();
                    plateCode.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    plateCode.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                    plateCode.CreatedBy = reader["CreatedBy"].ToString().ToLower();
                    if (columns.Contains("AccountId"))
                        plateCode.AccountId = Convert.ToInt32(reader["AccountId"]);

                    if (columns.Contains("AccountName"))
                        plateCode.AccountName = reader["AccountName"].ToString();

                    if (columns.Contains("SiteId"))
                        plateCode.SiteId = Convert.ToInt32(reader["SiteId"]);

                    if (columns.Contains("CustomerId"))
                        plateCode.CustomerId = Convert.ToInt32(reader["CustomerId"]);

                    plateCodes.Add(plateCode);
                }
                connection.Close();
                reader.Close();
            }
            return plateCodes;
        }

        public static List<VehicleColor> GetAllVehicleColorByDate(DateTime updatedDate, string dbConnection)
        {
            var vehicleColors = new List<VehicleColor>();
            var vehicleColorAudits = VehicleColor_Audit.GetAllVehicleColor_AuditByDate(updatedDate, dbConnection);
            DataSet dataset = new DataSet();
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add("COLORCODE");
            foreach (var vehicleColorAudit in vehicleColorAudits)
            {
                if (vehicleColorAudit.Operation == "Updated" || vehicleColorAudit.Operation == "Inserted")
                {
                    var dr = dataTable.NewRow();
                    dr["COLORCODE"] = vehicleColorAudit.ColorCode;
                    dataTable.Rows.Add(dr);
                }
                else
                {
                    var vehicleColor = new VehicleColor();
                    vehicleColor.ColorCode = vehicleColorAudit.ColorCode;
                    vehicleColor.Operation = DatabaseOperation.Deleted;
                    vehicleColor.ColorDesc = DatabaseOperation.Unknown.ToString();
                    vehicleColor.ColorDesc_AR = DatabaseOperation.Unknown.ToString();
                    vehicleColor.CreatedBy = DatabaseOperation.Unknown.ToString();
                    vehicleColor.CreatedDate = DateTime.Now;
                    vehicleColor.UpdatedDate = DateTime.Now;
                    vehicleColors.Add(vehicleColor);
                }
            }
            if (dataTable.Rows.Count > 0)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var sqlCommand = new SqlCommand("GetAllVehicleColorByDate", connection);
                    sqlCommand.Parameters.AddWithValue("@VehicleColorCodes", dataTable);
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    sqlCommand.CommandText = "GetAllVehicleColorByDate";

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        var vehicleColor = new VehicleColor();

                        vehicleColor.ColorCode = Convert.ToInt32(reader["COLORCODE"].ToString());
                        vehicleColor.ColorDesc = reader["COLORDESC"].ToString();
                        vehicleColor.ColorDesc_AR = reader["COLORDESC_AR"].ToString();
                        vehicleColor.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                        vehicleColor.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
                        vehicleColor.CreatedBy = reader["CreatedBy"].ToString().ToLower();
                        vehicleColor.Operation = DatabaseOperation.Updated;
                        vehicleColors.Add(vehicleColor);
                    }
                    connection.Close();
                    reader.Close();
                }
            }
            return vehicleColors;
        }

        public static int InsertorUpdateVehicleColor(VehicleColor vehicleColor, string dbConnection,
            string auditDbConnection = "", bool isAuditEnabled = true)
        {
            var id = 0;
            if (vehicleColor != null)
            {
                id = vehicleColor.ColorCode;
                var action = (id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate; 

                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateVehicleColor", connection);

                    cmd.Parameters.Add(new SqlParameter("@COLORCODE", SqlDbType.Int));
                    cmd.Parameters["@COLORCODE"].Value = vehicleColor.ColorCode;

                    cmd.Parameters.Add(new SqlParameter("@COLORDESC", SqlDbType.NVarChar));
                    cmd.Parameters["@COLORDESC"].Value = vehicleColor.ColorDesc;

                    cmd.Parameters.Add(new SqlParameter("@COLORDESC_AR", SqlDbType.NVarChar));
                    cmd.Parameters["@COLORDESC_AR"].Value = vehicleColor.ColorDesc_AR;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = vehicleColor.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = vehicleColor.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = vehicleColor.CreatedBy.ToLower();

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = vehicleColor.CustomerId;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = vehicleColor.SiteId;

                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandText = "InsertOrUpdateVehicleColor";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                    if (id == 0)
                        id = (int)returnParameter.Value;

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            vehicleColor.ColorCode = id;
                            var audit = new VehicleColorAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, vehicleColor.UpdatedBy);
                            Reflection.CopyProperties(vehicleColor, audit);
                            VehicleColorAudit.InsertVehicleColorAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertOrUpdateVehicleColor", ex, auditDbConnection);
                    }
                }
            }
            return id;
        }

        public static bool DeleteVehicleColorByColorCode(int colorCode, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteVehicleColorByColorCode", connection);

                cmd.Parameters.Add(new SqlParameter("@COLORCODE", SqlDbType.Int));
                cmd.Parameters["@COLORCODE"].Value = colorCode;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteVehicleColorByColorCode";

                cmd.ExecuteNonQuery();
            }
            return true;
        }

        public static DateTime GetLastEffectiveDate(string dbConnection)
        {
            var effectiveDate = DateTime.Now;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetVehicleColor_Audit_EffectiveDate", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlCommand.CommandText = "GetVehicleColor_Audit_EffectiveDate";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    if (reader["EffectiveDate"] != DBNull.Value)
                        effectiveDate = Convert.ToDateTime(reader["EffectiveDate"].ToString());
                }
                connection.Close();
                reader.Close();
            }
            return effectiveDate;
        }
    }

    internal class VehicleColor_Audit
    {
        public int ColorCode { get; set; }
        public string Operation { get; set; }
        public DateTime EffectiveDate { get; set; }

        public static List<VehicleColor_Audit> GetAllVehicleColor_AuditByDate(DateTime updatedDate, string dbConnection)
        {
            var vehicleColor_Audits = new List<VehicleColor_Audit>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllVehicleColor_AuditByDate", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                sqlCommand.Parameters["@UpdatedDate"].Value = updatedDate;
                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlCommand.CommandText = "GetAllVehicleColor_AuditByDate";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var vehicleColor_Audit = new VehicleColor_Audit();

                    if (reader["COLORCODE"] != DBNull.Value)
                        vehicleColor_Audit.ColorCode = Convert.ToInt32(reader["COLORCODE"].ToString());

                    vehicleColor_Audit.Operation = reader["Operation"].ToString();
                    vehicleColor_Audit.EffectiveDate = Convert.ToDateTime(reader["EffectiveDate"].ToString());
                    vehicleColor_Audits.Add(vehicleColor_Audit);
                }
                connection.Close();
                reader.Close();
            }
            return vehicleColor_Audits;
        }
    }
}
