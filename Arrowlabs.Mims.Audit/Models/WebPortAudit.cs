﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class WebPortAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public int WebPortID { get; set; }
        public int Port { get; set; }
        public bool Active { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public int SiteId { get;set;}

        private static WebPortAudit GetWebPortAuditFromSqlReader(SqlDataReader reader)
        {
            var webPort = new WebPortAudit();
            webPort.BaseAudit = BaseAudit.GetBaseAuditFromSqlReader(reader);
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

            if (columns.Contains("WebPortID") && reader["WebPortID"] != DBNull.Value)
                webPort.WebPortID = Convert.ToInt16(reader["WebPortID"].ToString());
            if (columns.Contains("Port") && reader["Port"] != DBNull.Value)
                webPort.Port = (String.IsNullOrEmpty(reader["Port"].ToString())) ? 0 : Convert.ToInt32(reader["Port"].ToString());
            if (columns.Contains("Active") && reader["Active"] != DBNull.Value)
                webPort.Active = (String.IsNullOrEmpty(reader["Active"].ToString())) ? false : Convert.ToBoolean(reader["Active"].ToString());
            if (columns.Contains("CreatedDate") && reader["CreatedDate"] != DBNull.Value)
                webPort.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            if (columns.Contains("CreatedBy") && reader["CreatedBy"] != DBNull.Value)
                webPort.CreatedBy = reader["CreatedBy"].ToString();
            if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                webPort.SiteId = Convert.ToInt32(reader["SiteId"].ToString());

            return webPort;
        }
        public static List<WebPortAudit> GetAllWebPortAudit(string dbConnection)
        {
            var webPorts = new List<WebPortAudit>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllWebPortAudit", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var webPort = GetWebPortAuditFromSqlReader(reader);
                    webPorts.Add(webPort);
                }
                connection.Close();
                reader.Close();
            }
            return webPorts;
        }
        public static bool InsertWebPortAudit(WebPortAudit webPort, string dbConnection)
        {
            if (webPort != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertWebPortAudit", connection);

                    var baseAuditId = BaseAudit.InsertBaseAudit(webPort.BaseAudit, dbConnection);
                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;

                    cmd.Parameters.Add("@WebPortID", SqlDbType.Int).Value = webPort.WebPortID;

                    cmd.Parameters.Add("@Port", SqlDbType.NVarChar).Value = webPort.Port;

                    cmd.Parameters.Add("@Active", SqlDbType.Bit).Value = webPort.Active;

                    cmd.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = webPort.CreatedDate;

                    cmd.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = webPort.CreatedBy;
                    cmd.Parameters.Add("@SiteId", SqlDbType.Int).Value = webPort.SiteId;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }

    }
}
