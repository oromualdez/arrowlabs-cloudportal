﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace Arrowlabs.Business.Layer
{
    public class MailSetting
    {
        public int Id { get; set; }
        public string Host { get; set; }
        public int Port { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string MailFrom { get; set; }
        public bool EnableSSL { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string CreatedBy { get; set; }

        public static MailSetting GetMailSetting(string dbConnection)
        {
            MailSetting mailSetting = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetMailSetting", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    mailSetting = new MailSetting();

                    mailSetting.Id = Convert.ToInt32(reader["Id"].ToString());
                    mailSetting.Host = reader["Host"].ToString();
                    mailSetting.Port = Convert.ToInt32(reader["Port"].ToString());
                    mailSetting.Username = reader["Username"].ToString();
                    mailSetting.Password = reader["Password"].ToString();
                    mailSetting.MailFrom = reader["MailFrom"].ToString();
                    mailSetting.EnableSSL = Convert.ToBoolean(reader["EnableSSL"].ToString());
                    mailSetting.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    mailSetting.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"].ToString());
                    mailSetting.CreatedBy = reader["CreatedBy"].ToString();
                }
                connection.Close();
                reader.Close();
            }
            return mailSetting;
        }

        public static bool InsertOrUpdateMailSetting(MailSetting mailSetting, string dbConnection)
        {
            if (mailSetting != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateMailSetting", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = mailSetting.Id;

                    cmd.Parameters.Add(new SqlParameter("@Host", SqlDbType.NVarChar));
                    cmd.Parameters["@Host"].Value = mailSetting.Host;

                    cmd.Parameters.Add(new SqlParameter("@Port", SqlDbType.Int));
                    cmd.Parameters["@Port"].Value = mailSetting.Port;

                    cmd.Parameters.Add(new SqlParameter("@Username", SqlDbType.NVarChar));
                    cmd.Parameters["@Username"].Value = mailSetting.Username;

                    cmd.Parameters.Add(new SqlParameter("@Password", SqlDbType.NVarChar));
                    cmd.Parameters["@Password"].Value = mailSetting.Password;

                    cmd.Parameters.Add(new SqlParameter("@MailFrom", SqlDbType.NVarChar));
                    cmd.Parameters["@MailFrom"].Value = mailSetting.MailFrom;

                    cmd.Parameters.Add(new SqlParameter("@EnableSsl", SqlDbType.Bit));
                    cmd.Parameters["@EnableSsl"].Value = mailSetting.EnableSSL;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = mailSetting.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = mailSetting.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = mailSetting.CreatedBy;

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }
    }
}
