﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Runtime.Serialization;
using Arrowlabs.Mims.Audit.Models;

namespace Arrowlabs.Business.Layer
{
    [Serializable]
    public class UserManager
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int ManagerId { get; set; }
        public string UpdatedBy { get; set; }
        public int SiteId { get; set; }
        public int CustomerId { get; set; }
        public static bool DeleteManagersByUserIdAndMangId(int id, int id2, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var cmd = new SqlCommand("DeleteManagersByUserIdAndMangId", connection);

                cmd.Parameters.Add(new SqlParameter("@MangId", SqlDbType.Int));
                cmd.Parameters["@MangId"].Value = id;
                cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                cmd.Parameters["@UserId"].Value = id2;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            }
            return true;
        }

        /// <summary>
        /// It returns the list of manager Ids to whom specified user is attached
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="dbConnection"></param>
        /// <returns></returns>
        public static List<int> GetAllManagersByUserId(int userId, string dbConnection)
        {
            var managerIdsList = new List<int>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllManagersByUserId", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                sqlCommand.Parameters["@UserId"].Value = userId;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    managerIdsList.Add(Convert.ToInt32(reader["ManagerId"].ToString()));
                }
                connection.Close();
                reader.Close();
            }
            return managerIdsList;
        }

        /// <summary>
        /// It returns all users associated with specific manager
        /// </summary>
        /// <param name="managerId"></param>
        /// <param name="dbConnection"></param>
        /// <returns></returns>
        public static List<int> GetAllUsersByManagerId(int managerId, string dbConnection)
        {
            var userIdsList = new List<int>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllUsersByManagerId", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@ManagerId", SqlDbType.Int));
                sqlCommand.Parameters["@ManagerId"].Value = managerId;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    userIdsList.Add(Convert.ToInt32(reader["UserId"].ToString()));
                }
                connection.Close();
                reader.Close();
            }
            return userIdsList;
        }

        /// <summary>
        /// it will insert user, manager relationship
        /// </summary>
        /// <param name="userAndManagers"></param>
        /// <param name="dbConnection"></param>
        /// <returns></returns>
        public static bool InsertUserManagers(List<UserManager> userAndManagers, string dbConnection,
            string auditDbConnection = "", bool isAuditEnabled = true)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                if (userAndManagers.Count > 0)
                {

                    var sqlCommand1 = new SqlCommand("DeleteManagersByUserId", connection);

                    sqlCommand1.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                    sqlCommand1.Parameters["@UserId"].Value = userAndManagers.First().UserId;

                    sqlCommand1.CommandType = CommandType.StoredProcedure;
                    sqlCommand1.ExecuteNonQuery();
                }

                foreach (var userAndManager in userAndManagers)
                {
                    
                    var action = Arrowlabs.Mims.Audit.Enums.Action.Create; 

                    var sqlCommand = new SqlCommand("InsertUserManagers", connection);

                    sqlCommand.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                    sqlCommand.Parameters["@UserId"].Value = userAndManager.UserId;

                    sqlCommand.Parameters.Add(new SqlParameter("@ManagerId", SqlDbType.Int));
                    sqlCommand.Parameters["@ManagerId"].Value = userAndManager.ManagerId;


                    sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    sqlCommand.Parameters["@SiteId"].Value = userAndManager.SiteId;


                    sqlCommand.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    sqlCommand.Parameters["@CustomerId"].Value = userAndManager.CustomerId;

                    

                    var returnParameter = sqlCommand.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;
                    
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                     sqlCommand.ExecuteNonQuery();

                    var id = (int)returnParameter.Value;

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            userAndManager.Id = id;
                            var audit = new UserManagerAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, userAndManager.UpdatedBy, id.ToString());
                            Reflection.CopyProperties(userAndManager, audit);
                            UserManagerAudit.InsertUserManagerAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertUserManagers", ex, auditDbConnection);
                    }
                }
                connection.Close();
            }
            return true;
        }
    }
}
