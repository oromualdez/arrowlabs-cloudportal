﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
namespace Arrowlabs.Mims.Audit.Models
{
    // It contains common data for every audit transaction.
    public class  BaseAudit
    {
        public int Id { get; set; }
        public int Action { get; set; }
        public string Account { get; set; }
        public DateTime AuditDate { get; set; }
        public string AuditBy { get; set; }
        public string Description { get; set; }

        public static BaseAudit FillBaseAudit(string account, Arrowlabs.Mims.Audit.Enums.Action action, string auditBy,
            string description = "")
        {
            var baseAuditObj = new BaseAudit();

            baseAuditObj.Account = account;
            baseAuditObj.Action = (int)action;
            baseAuditObj.AuditDate = DateTime.Now;
            baseAuditObj.AuditBy = auditBy;
            baseAuditObj.Description = description;

            return baseAuditObj;
        }
        public static BaseAudit GetBaseAuditFromSqlReader(SqlDataReader reader)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

            var baseAudit = new BaseAudit();

            if (columns.Contains("AuditParentId") && reader["AuditParentId"] != DBNull.Value)
                baseAudit.Id = Convert.ToInt32(reader["AuditParentId"].ToString());

            if (columns.Contains("Action") && reader["Action"] != DBNull.Value)
                baseAudit.Action = Convert.ToInt32(reader["Action"].ToString());

            if (columns.Contains("Account") && reader["Account"] != DBNull.Value)
                baseAudit.Account = reader["Account"].ToString();

            if (columns.Contains("AuditDate") && reader["AuditDate"] != DBNull.Value)
                baseAudit.AuditDate = Convert.ToDateTime( reader["AuditDate"].ToString());

            if (columns.Contains("AuditBy") && reader["AuditBy"] != DBNull.Value)
                baseAudit.AuditBy = reader["AuditBy"].ToString();

            if (columns.Contains("Description") && reader["Description"] != DBNull.Value)
                baseAudit.Description = reader["Description"].ToString();

            return baseAudit;
        }
        public static int InsertBaseAudit(BaseAudit baseAudit, string dbConnection)
        {
            var baseAuditId = 0;
            if (baseAudit != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertBaseAudit", connection);

                    cmd.Parameters.Add("@Action", SqlDbType.Int).Value = baseAudit.Action;
                    cmd.Parameters.Add("@Account", SqlDbType.NVarChar).Value = baseAudit.Account;
                    cmd.Parameters.Add("@AuditDate", SqlDbType.DateTime).Value = baseAudit.AuditDate;
                    cmd.Parameters.Add("@AuditBy", SqlDbType.NVarChar).Value = baseAudit.AuditBy;
                    cmd.Parameters.Add("@Description", SqlDbType.NVarChar).Value = baseAudit.Description;

                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();

                    baseAuditId = (int)returnParameter.Value;
                }
            }
            return baseAuditId;
        }
    }
}
