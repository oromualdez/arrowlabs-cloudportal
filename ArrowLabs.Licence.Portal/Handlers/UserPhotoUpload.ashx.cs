﻿using Arrowlabs.Business.Layer;
using ArrowLabs.Licence.Portal.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ArrowLabs.Licence.Portal.Handlers
{
    /// <summary>
    /// Summary description for UserPhotoUpload
    /// </summary>
    public class UserPhotoUpload : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            if (context.Request.Files.Count > 0)
            {
                HttpFileCollection files = context.Request.Files;
                var mimcon = MIMSConfigSetting.GetMIMSConfigSettings(CommonUtility.dbConnection);
                var consplit = mimcon.FilePath.Split('\\');
                var newstring = string.Empty;
                var count = 0;
                foreach (var split in consplit)
                {
                    if (count < 4)
                    {
                        newstring += split + "\\";
                        count++;
                    }
                    else
                    {
                        break;
                    }
                }
                HttpPostedFile file = files[0];

                newstring = newstring + "Uploads\\UserPhotos\\" + Guid.NewGuid().ToString() + ".jpeg";
                file.SaveAs(newstring);
                context.Response.ContentType = "text/plain";
                context.Response.Write(newstring);
            }    
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}