﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class TaskJobActivityAudit
    {
        public BaseAudit BaseAudit { get; set; }  
        public int Id { get; set; }
        public int TaskId { get; set; }
        public int JobAction { get; set; }

        public DateTime? CreatedDate { get; set; }

        public string CreatedBy { get; set; }

        public string Latitude { get; set; }
        public string Longtitude { get; set; }

        public int SiteId { get; set; }

        public int CustomerId { get; set; }

        public string CustomerUName { get; set; }
        public string CustomerFullName { get; set; }

        public static bool InsertTaskJobActivityAudit(TaskJobActivityAudit eventHistory, string dbConnection,
string auditDbConnection = "", bool isAuditEnabled = true)
        {
            var baseAuditId = BaseAudit.InsertBaseAudit(eventHistory.BaseAudit, dbConnection);

            if (eventHistory != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertTaskJobActivityAudit", connection);

                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;

                    cmd.Parameters.Add(new SqlParameter("@TaskId", SqlDbType.Int));
                    cmd.Parameters["@TaskId"].Value = eventHistory.TaskId;

                    cmd.Parameters.Add(new SqlParameter("@JobAction", SqlDbType.Int));
                    cmd.Parameters["@JobAction"].Value = eventHistory.JobAction;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = eventHistory.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = eventHistory.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = eventHistory.SiteId;

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = eventHistory.CustomerId;


                    cmd.Parameters.Add(new SqlParameter("@Longtitude", SqlDbType.NVarChar));
                    cmd.Parameters["@Longtitude"].Value = eventHistory.Longtitude;

                    cmd.Parameters.Add(new SqlParameter("@Latitude", SqlDbType.NVarChar));
                    cmd.Parameters["@Latitude"].Value = eventHistory.Latitude;

                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();

                }
            }
            return true;
        }
    }
}
