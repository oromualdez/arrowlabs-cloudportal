﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using Arrowlabs.Mims.Audit.Models;

namespace Arrowlabs.Business.Layer
{
    public class Users
    {
        public int ID { get; set; }
        public string UserID { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public int RoleId { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }
        public DateTime? LastLogin { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string State { get; set; }
        public string Register { get; set; }
        public int DeviceType { get; set; }
        public string imageURL { get; set; }

        public bool Engaged { get; set; }

        public string EngagedIn { get; set; }
        public int AccountId { get; set; }
        public string AccountName { get; set; }
        public string Status { get; set; }
        public bool IsLatest { get; set; }

        public bool IsGroup { get; set; }
        public int Active { get; set; }
        public DateTime? LoginDate { get; set; }
        public int GeofenceStatus { get; set; }

        public string Telephone { get; set; }

        public int SiteId { get; set; }

        public string SiteName { get; set; }

        //public int DayOffUsed { get; set; }

        //public int DayInLieu { get; set; }

        //public int OTHours { get; set; }

        public string Gender { get; set; }

        public string EmployeeID { get; set; }

        public int CustomerInfoId { get; set; } 
        public string CustomerUName { get; set; }

        public bool IsServerPortal { get; set; }

        public bool IsWatch { get; set; }

        public int ShiftOn { get; set; }

        public int ContractLinkId { get; set; }

        public int CustomerLinkId { get; set; } 

        public string ShiftOnDesc { get; set; }

        public Double TimeZone { get; set; }

        public enum UserState : int
        {
            Unknown = -1,
            UnRegistered = 0,
            Enable = 1,
            Disable = 2,
            Curfew = 3
        }
        public enum DeviceTypes : int
        {
            Unknown = -1,
            None = 0,
            Mobile = 1,
            Client = 2,
            MobileAndClient = 3,
            ServerPortal = 4,
            MimsOperatorClient = 5,
            Watch = 6,
            MobileAndWatch = 7

            //Client = 3,
            //PhoneTablet = 4,
            //PhoneClient = 5,
            //TabletClient = 6,
            //PhoneTabletClient = 7
        }
        /*
        public static List<Users> GetAllUsers(string dbConnection)
        {
            var userList = new List<Users>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllUsers", connection);


                sqlCommand.CommandText = "GetAllUsers";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = new Users();

                    if (!string.IsNullOrEmpty(reader["ID"].ToString()))
                        userClass.ID = Convert.ToInt32(reader["ID"].ToString());

                    //userClass.UserID = reader["UserID"].ToString();
                    userClass.Username = reader["Username"].ToString();
                    userClass.Password = reader["Password"].ToString();


                    userList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return userList;
        }
        */
        public static List<Users> GetAllUsers(string dbConnection)
        {
            var userList = new List<Users>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllUsers", connection);
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = GetFullUserFromSqlReader(reader, dbConnection);
                    userList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return userList;
        }
        public static List<Users> GetAllSystemUsers(string dbConnection)
        {
            var userList = new List<Users>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllSystemUsers", connection);
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = GetFullUserFromSqlReader(reader, dbConnection);
                    userList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return userList;
        }
        public static List<Users> GetAllUsersBySiteId(int siteid,string dbConnection)
        {
            var userList = new List<Users>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllUsersBySiteId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                sqlCommand.Parameters["@SiteId"].Value = siteid;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = GetFullUserFromSqlReader(reader, dbConnection);
                    userList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return userList;
        }

        public static List<Users> GetAllUsersBySiteIdAndRole(int siteid, int roleid, string dbConnection)
        {
            var userList = new List<Users>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllUsersBySiteIdAndRole", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                sqlCommand.Parameters["@SiteId"].Value = siteid;
                sqlCommand.Parameters.Add(new SqlParameter("@RoleId", SqlDbType.Int));
                sqlCommand.Parameters["@RoleId"].Value = roleid;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = GetFullUserFromSqlReader(reader, dbConnection);
                    userList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return userList;
        }

        public static List<Users> GetAllUsersByLevel5(int siteid, string dbConnection)
        {
            var userList = new List<Users>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllUsersByLevel5", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                sqlCommand.Parameters["@UserId"].Value = siteid; 
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = GetFullUserFromSqlReader(reader, dbConnection);
                    userList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return userList;
        }

        public static List<Users> GetAllMessageboardUsersByLevel5(int siteid, string dbConnection)
        {
            var userList = new List<Users>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllMessageboardUsersByLevel5", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                sqlCommand.Parameters["@UserId"].Value = siteid;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = GetFullUserFromSqlReader(reader, dbConnection);
                    userList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return userList;
        }

        public static int GetAllMobileOnlineUsers(string dbConnection)
        {
            var onlineUsers = 0;
            var userList = new List<Users>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllMobileOnlineUsers", connection);
                onlineUsers = (Int32)sqlCommand.ExecuteScalar();                
                connection.Close();
            }
            return onlineUsers;
        }

        public static List<Users> GetAllUsersAndManagers(string dbConnection)
        {
            var userList = new List<Users>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllUsersAndManagers", connection);

                sqlCommand.CommandText = "GetAllUsersAndManagers";

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = GetFullUserFromSqlReader(reader, dbConnection);
                    userList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return userList;
        }
        
        public static bool InsertOrUpdateUsers(Users usersClass, string dbConnection, 
            string auditDbConnection = "", bool isAuditEnabled = true)
        {
           
            int id = usersClass.ID;
            var action = (id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate; 

            try
            {
                if (usersClass != null && !string.IsNullOrEmpty(usersClass.Username))
                {

                    using (var connection = new SqlConnection(dbConnection))
                    {
                        connection.Open();
                        var cmd = new SqlCommand("InsertOrUpdateUser", connection);


                        cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                        cmd.Parameters["@Id"].Value = usersClass.ID;

                        cmd.Parameters.Add(new SqlParameter("@ContractLinkId", SqlDbType.Int));
                        cmd.Parameters["@ContractLinkId"].Value = usersClass.ContractLinkId;

                        cmd.Parameters.Add(new SqlParameter("@CustomerLinkId", SqlDbType.Int));
                        cmd.Parameters["@CustomerLinkId"].Value = usersClass.CustomerLinkId;
                         
                        cmd.Parameters.Add(new SqlParameter("@Username", SqlDbType.NVarChar));
                        cmd.Parameters["@Username"].Value = usersClass.Username;

                        cmd.Parameters.Add(new SqlParameter("@Password", SqlDbType.NVarChar));
                        cmd.Parameters["@Password"].Value = usersClass.Password;

                        cmd.Parameters.Add(new SqlParameter("@Email", SqlDbType.NVarChar));
                        cmd.Parameters["@Email"].Value = usersClass.Email;

                        cmd.Parameters.Add(new SqlParameter("@RoleId", SqlDbType.Int));
                        cmd.Parameters["@RoleId"].Value = usersClass.RoleId;

                        cmd.Parameters.Add(new SqlParameter("@Latitude", SqlDbType.Float));
                        cmd.Parameters["@Latitude"].Value = usersClass.Latitude;

                        cmd.Parameters.Add(new SqlParameter("@Longitude", SqlDbType.Float));
                        cmd.Parameters["@Longitude"].Value = usersClass.Longitude;

                        cmd.Parameters.Add(new SqlParameter("@LastLogin", SqlDbType.DateTime));
                        cmd.Parameters["@LastLogin"].Value = usersClass.LastLogin;

                        cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                        cmd.Parameters["@CreatedDate"].Value = usersClass.CreatedDate;

                        cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                        cmd.Parameters["@UpdatedDate"].Value = usersClass.UpdatedDate;

                        cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                        cmd.Parameters["@CreatedBy"].Value = usersClass.CreatedBy;

                        cmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                        cmd.Parameters["@UpdatedBy"].Value = usersClass.UpdatedBy;

                        cmd.Parameters.Add(new SqlParameter("@FirstName", SqlDbType.NVarChar));
                        cmd.Parameters["@FirstName"].Value = usersClass.FirstName;

                        cmd.Parameters.Add(new SqlParameter("@LastName", SqlDbType.NVarChar));
                        cmd.Parameters["@LastName"].Value = usersClass.LastName;

                        cmd.Parameters.Add(new SqlParameter("@Register", SqlDbType.NVarChar));
                        cmd.Parameters["@Register"].Value = usersClass.Register;

                        cmd.Parameters.Add(new SqlParameter("@DeviceType", SqlDbType.Int));
                        cmd.Parameters["@DeviceType"].Value = usersClass.DeviceType;

                        cmd.Parameters.Add(new SqlParameter("@Engaged", SqlDbType.Bit));
                        cmd.Parameters["@Engaged"].Value = usersClass.Engaged;

                        cmd.Parameters.Add(new SqlParameter("@EngagedIn", SqlDbType.NVarChar));
                        cmd.Parameters["@EngagedIn"].Value = usersClass.EngagedIn;


                        cmd.Parameters.Add(new SqlParameter("@Telephone", SqlDbType.NVarChar));
                        cmd.Parameters["@Telephone"].Value = usersClass.Telephone;

                        cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                        cmd.Parameters["@SiteId"].Value = usersClass.SiteId;

                        if (!string.IsNullOrEmpty(usersClass.Gender))
                        {
                            cmd.Parameters.Add(new SqlParameter("@Gender", SqlDbType.NVarChar));
                            cmd.Parameters["@Gender"].Value = usersClass.Gender;
                        }
                        if (!string.IsNullOrEmpty(usersClass.EmployeeID))
                        {
                            cmd.Parameters.Add(new SqlParameter("@EmployeeID", SqlDbType.NVarChar));
                            cmd.Parameters["@EmployeeID"].Value = usersClass.EmployeeID;
                        }

                            cmd.Parameters.Add(new SqlParameter("@CustomerInfoId", SqlDbType.Int));
                            cmd.Parameters["@CustomerInfoId"].Value = usersClass.CustomerInfoId;
                        
                        

                        SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                        returnParameter.Direction = ParameterDirection.ReturnValue;

                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.ExecuteNonQuery();

                        if (id == 0)
                            id = (int)returnParameter.Value;
                        try
                        {
                            if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                            {
                                usersClass.ID = id;
                                var audit = new Arrowlabs.Mims.Audit.Models.UserAudit();
                                audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, usersClass.UpdatedBy);
                                Reflection.CopyProperties(usersClass, audit);
                                Arrowlabs.Mims.Audit.Models.UserAudit.InsertUserAudit(audit, auditDbConnection);
                            }
                        }
                        catch(Exception ex)
                        {
                            MIMSLog.MIMSLogSave("Business Layer", "InsertOrUpdateUsers", ex, auditDbConnection);
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Business Layer", "InsertOrUpdateUsers", ex, dbConnection);
                return false;
            }
        }

        public static List<Users> GetAllUsersByRoleId(int groupId, string dbConnection)
        {
            var userList = new List<Users>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllUsersByRoleId", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@RoleId", SqlDbType.Int));
                sqlCommand.Parameters["@RoleId"].Value = groupId;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = GetFullUserFromSqlReader(reader, dbConnection);
                    userList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return userList;
        }

        public static List<Users> GetAllUserByGroupId(int groupId, string dbConnection)
        {
            var userList = new List<Users>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllUserByGroupId", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@GroupId", SqlDbType.Int));
                sqlCommand.Parameters["@GroupId"].Value = groupId;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = GetFullUserFromSqlReader(reader, dbConnection);
                    userList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return userList;
        }

        public static List<Users> GetAllUserNotInGroupId(int groupId, string dbConnection)
        {
            var userList = new List<Users>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllUserNotInGroupId", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@GroupId", SqlDbType.Int));
                sqlCommand.Parameters["@GroupId"].Value = groupId;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = new Users();

                    if (!string.IsNullOrEmpty(reader["ID"].ToString()))
                        userClass.ID = Convert.ToInt32(reader["ID"].ToString());
                    userClass.Username = reader["Username"].ToString();
                    userClass.Password = reader["Password"].ToString();
                    userClass.Email = reader["Email"].ToString();

                    userList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return userList;
        }

        public static List<Users> GetAllUsersAndManagersNotInGroupId(int groupId, string dbConnection)
        {
            var userList = new List<Users>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllUsersAndManagersNotInGroupId", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@GroupId", SqlDbType.Int));
                sqlCommand.Parameters["@GroupId"].Value = groupId;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = new Users();

                    if (!string.IsNullOrEmpty(reader["ID"].ToString()))
                        userClass.ID = Convert.ToInt32(reader["ID"].ToString());
                    userClass.Username = reader["Username"].ToString();
                    userClass.Password = reader["Password"].ToString();
                    userClass.Email = reader["Email"].ToString();

                    userList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return userList;
        }

        public static List<Users> GetAllUserByGroupIdAndManagerId(int groupId, int userId, string dbConnection)
        {
            var userList = new List<Users>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllUserByGroupIdAndManagerId", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@GroupId", SqlDbType.Int));
                sqlCommand.Parameters["@GroupId"].Value = groupId;
                sqlCommand.Parameters.Add(new SqlParameter("@ManagerId", SqlDbType.Int));
                sqlCommand.Parameters["@ManagerId"].Value = userId;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = GetFullUserFromSqlReader(reader, dbConnection);
                    userList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return userList;
        }

        public static List<Users> GetAllUserByGroupIdAndManagerIdAndLastName(int groupId, int userId,string name, string dbConnection)
        {
            var userList = new List<Users>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllUserByGroupIdAndManagerIdAndLastName", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@GroupId", SqlDbType.Int));
                sqlCommand.Parameters["@GroupId"].Value = groupId;
                sqlCommand.Parameters.Add(new SqlParameter("@ManagerId", SqlDbType.Int));
                sqlCommand.Parameters["@ManagerId"].Value = userId;
                sqlCommand.Parameters.Add(new SqlParameter("@LastName", SqlDbType.NVarChar));
                sqlCommand.Parameters["@LastName"].Value = name;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = GetFullUserFromSqlReader(reader, dbConnection);
                    userList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return userList;
        }

        public static List<Users> GetAllUserByGroupIdAndManagerIdAndUsername(int groupId, int userId, string name, string dbConnection)
        {
            var userList = new List<Users>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllUserByGroupIdAndManagerIdAndUsername", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@GroupId", SqlDbType.Int));
                sqlCommand.Parameters["@GroupId"].Value = groupId;
                sqlCommand.Parameters.Add(new SqlParameter("@ManagerId", SqlDbType.Int));
                sqlCommand.Parameters["@ManagerId"].Value = userId;
                sqlCommand.Parameters.Add(new SqlParameter("@Username", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Username"].Value = name;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = GetFullUserFromSqlReader(reader, dbConnection);
                    userList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return userList;
        }

        public static List<Users> GetAllUserByGroupIdAndManagerIdAndFirstName(int groupId, int userId, string name, string dbConnection)
        {
            var userList = new List<Users>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllUserByGroupIdAndManagerIdAndFirstName", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@GroupId", SqlDbType.Int));
                sqlCommand.Parameters["@GroupId"].Value = groupId;
                sqlCommand.Parameters.Add(new SqlParameter("@ManagerId", SqlDbType.Int));
                sqlCommand.Parameters["@ManagerId"].Value = userId;
                sqlCommand.Parameters.Add(new SqlParameter("@FirstName", SqlDbType.NVarChar));
                sqlCommand.Parameters["@FirstName"].Value = name;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = GetFullUserFromSqlReader(reader, dbConnection);
                    userList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return userList;
        }

        public static List<Users> GetAllFullUsersByManagerId(int userId, string dbConnection)
        {
            var managerIdsList = new List<Users>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllFullUsersByManagerId", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@ManagerId", SqlDbType.Int));
                sqlCommand.Parameters["@ManagerId"].Value = userId;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = GetFullUserFromSqlReader(reader, dbConnection);
                    managerIdsList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return managerIdsList;
        }

        public static List<Users> GetAllFullUsersByManagerIdForDirector(int userId, string dbConnection)
        {
            var managerIdsList = new List<Users>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllFullUsersByManagerIdForDirector", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@ManagerId", SqlDbType.Int));
                sqlCommand.Parameters["@ManagerId"].Value = userId;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = GetFullUserFromSqlReader(reader, dbConnection);
                    managerIdsList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return managerIdsList;
        }

        public static Users GetAllFullManagersByUserId(int userId, string dbConnection)
        {
            var userClass = new Users();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllFullManagersByUserId", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                sqlCommand.Parameters["@UserId"].Value = userId;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    userClass = GetFullUserFromSqlReader(reader, dbConnection);

                }
                connection.Close();
                reader.Close();
            }
            return userClass;
        }

        public static List<Users> GetAllUsersByManagerIdAndRoleId(int userId, int roleId, string dbConnection)
        {
            var managerIdsList = new List<Users>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllUsersByManagerIdAndRoleId", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@ManagerId", SqlDbType.Int));
                sqlCommand.Parameters["@ManagerId"].Value = userId;
                sqlCommand.Parameters.Add(new SqlParameter("@RoleId", SqlDbType.Int));
                sqlCommand.Parameters["@RoleId"].Value = roleId;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = GetFullUserFromSqlReader(reader, dbConnection);
                    managerIdsList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return managerIdsList;
        }

        public static List<Users> GetAllUsersByManagerIdAndRoleIdAndUsername(int userId, int roleId, string name, string dbConnection)
        {
            var managerIdsList = new List<Users>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllUsersByManagerIdAndRoleIdAndUsername", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@ManagerId", SqlDbType.Int));
                sqlCommand.Parameters["@ManagerId"].Value = userId;
                sqlCommand.Parameters.Add(new SqlParameter("@RoleId", SqlDbType.Int));
                sqlCommand.Parameters["@RoleId"].Value = roleId;
                sqlCommand.Parameters.Add(new SqlParameter("@Username", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Username"].Value = name;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = GetFullUserFromSqlReader(reader, dbConnection);
                    managerIdsList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return managerIdsList;
        }
        public static List<Users> GetAllUsersByManagerIdAndRoleIdAndFirstName(int userId, int roleId, string name, string dbConnection)
        {
            var managerIdsList = new List<Users>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllUsersByManagerIdAndRoleIdAndFirstName", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@ManagerId", SqlDbType.Int));
                sqlCommand.Parameters["@ManagerId"].Value = userId;
                sqlCommand.Parameters.Add(new SqlParameter("@RoleId", SqlDbType.Int));
                sqlCommand.Parameters["@RoleId"].Value = roleId;
                sqlCommand.Parameters.Add(new SqlParameter("@FirstName", SqlDbType.NVarChar));
                sqlCommand.Parameters["@FirstName"].Value = name;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = GetFullUserFromSqlReader(reader, dbConnection);
                    managerIdsList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return managerIdsList;
        }
        public static List<Users> GetAllUsersByManagerIdAndRoleIdAndLastName(int userId, int roleId, string name, string dbConnection)
        {
            var managerIdsList = new List<Users>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllUsersByManagerIdAndRoleIdAndLastName", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@ManagerId", SqlDbType.Int));
                sqlCommand.Parameters["@ManagerId"].Value = userId;
                sqlCommand.Parameters.Add(new SqlParameter("@RoleId", SqlDbType.Int));
                sqlCommand.Parameters["@RoleId"].Value = roleId;
                sqlCommand.Parameters.Add(new SqlParameter("@LastName", SqlDbType.NVarChar));
                sqlCommand.Parameters["@LastName"].Value = name;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = GetFullUserFromSqlReader(reader, dbConnection);
                    managerIdsList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return managerIdsList;
        }

        public static List<Users> GetAllInUsersByManagerIdAndRoleIdAndUsername(int userId, int roleId, string name, string dbConnection)
        {
            var managerIdsList = new List<Users>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllInUsersByManagerIdAndRoleIdAndUsername", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@ManagerId", SqlDbType.Int));
                sqlCommand.Parameters["@ManagerId"].Value = userId;
                sqlCommand.Parameters.Add(new SqlParameter("@RoleId", SqlDbType.Int));
                sqlCommand.Parameters["@RoleId"].Value = roleId;
                sqlCommand.Parameters.Add(new SqlParameter("@Username", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Username"].Value = name;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = GetFullUserFromSqlReader(reader, dbConnection);
                    managerIdsList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return managerIdsList;
        }
        public static List<Users> GetAllInUsersByManagerIdAndRoleIdAndFirstName(int userId, int roleId, string name, string dbConnection)
        {
            var managerIdsList = new List<Users>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllInUsersByManagerIdAndRoleIdAndFirstName", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@ManagerId", SqlDbType.Int));
                sqlCommand.Parameters["@ManagerId"].Value = userId;
                sqlCommand.Parameters.Add(new SqlParameter("@RoleId", SqlDbType.Int));
                sqlCommand.Parameters["@RoleId"].Value = roleId;
                sqlCommand.Parameters.Add(new SqlParameter("@FirstName", SqlDbType.NVarChar));
                sqlCommand.Parameters["@FirstName"].Value = name;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = GetFullUserFromSqlReader(reader, dbConnection);
                    managerIdsList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return managerIdsList;
        }
        public static List<Users> GetAllInUsersByManagerIdAndRoleIdAndLastName(int userId, int roleId, string name, string dbConnection)
        {
            var managerIdsList = new List<Users>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllInUsersByManagerIdAndRoleIdAndLastName", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@ManagerId", SqlDbType.Int));
                sqlCommand.Parameters["@ManagerId"].Value = userId;
                sqlCommand.Parameters.Add(new SqlParameter("@RoleId", SqlDbType.Int));
                sqlCommand.Parameters["@RoleId"].Value = roleId;
                sqlCommand.Parameters.Add(new SqlParameter("@LastName", SqlDbType.NVarChar));
                sqlCommand.Parameters["@LastName"].Value = name;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = GetFullUserFromSqlReader(reader, dbConnection);
                    managerIdsList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return managerIdsList;
        }

        public static List<Users> GetAllUsersByLastName(string name, string dbConnection)
        {
            var managerIdsList = new List<Users>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllUsersByLastName", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = name;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = GetFullUserFromSqlReader(reader, dbConnection);
                    managerIdsList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return managerIdsList;
        }
        public static List<Users> GetAllUsersByUserName(string name, string dbConnection)
        {
            var managerIdsList = new List<Users>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllUsersByUserName", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = name;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = GetFullUserFromSqlReader(reader, dbConnection);
                    managerIdsList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return managerIdsList;
        }
        public static List<Users> GetAllUsersByFirstName(string name, string dbConnection)
        {
            var managerIdsList = new List<Users>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllUsersByFirstName", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@FirstName", SqlDbType.NVarChar));
                sqlCommand.Parameters["@FirstName"].Value = name;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = GetFullUserFromSqlReader(reader, dbConnection);
                    managerIdsList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return managerIdsList;
        }



        public static int GetLastUserId(string dbConnection)
        {
            int lastUserId = 0;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetLastUserId", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    lastUserId = Convert.ToInt32(reader["ID"].ToString());
                    break;
                }
                connection.Close();
                reader.Close();
            }
            return lastUserId;
        }

        public static List<Users> GetAllManagers(string dbConnection)
        {
            var userList = new List<Users>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllManager", connection);

                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = GetFullUserFromSqlReader(reader, dbConnection);
                    userList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return userList;
        }
        public static List<Users> GetAccountAllManagers(string dbConnection)
        {
            var userList = new List<Users>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllManager", connection);

                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = GetFullAccountUserFromSqlReader(reader, dbConnection);
                    userList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return userList;
        }
        public static Users GetManagersByName(string name, string dbConnection)
        {
            var userClass = new Users();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetManagerByName", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = name;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    userClass = GetFullUserFromSqlReader(reader, dbConnection);
                }
                connection.Close();
                reader.Close();
            }
            return userClass;
        }

        public static Users CheckEmployeeIdExist(string employeeid,int siteid ,int customerid,string dbConnection)
        {
            Users userClass = null;  
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("CheckEmployeeIdExist", connection);
                
                sqlCommand.Parameters.Add(new SqlParameter("@EmployeeId", SqlDbType.NVarChar));
                sqlCommand.Parameters["@EmployeeId"].Value = employeeid;

                sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                sqlCommand.Parameters["@SiteId"].Value = siteid;

                sqlCommand.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                sqlCommand.Parameters["@CustomerId"].Value = customerid;
                 
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    userClass = GetFullUserFromSqlReader(reader, dbConnection);
                }
                connection.Close();
                reader.Close();
            }
            return userClass;
        }

        public static Users GetUserByName(string name, string dbConnection)
        {
            Users userClass = null; //new Users();
            if (String.IsNullOrEmpty(name))
                return null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetUserByName", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = name;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    userClass = GetFullUserFromSqlReader(reader, dbConnection);
                }
                connection.Close();
                reader.Close();
            }
            return userClass;
        }

        public static Users GetUserByEncryptedName(string name, string dbConnection)
        {
            Users userClass = null; //new Users();
            if (String.IsNullOrEmpty(name))
                return null;
            else
                name = Decrypt.DecryptData(name, true, dbConnection);
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetUserByName", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = name;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    userClass = GetFullUserFromSqlReader(reader, dbConnection);
                }
                connection.Close();
                reader.Close();
            }
            return userClass;
        }

        public static Users GetAccountUserByName(string name, string dbConnection)
        {
            var userClass = new Users();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetUserByName", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = name;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    userClass = GetFullAccountUserFromSqlReader(reader, dbConnection);


                }
                connection.Close();
                reader.Close();
            }
            return userClass;
        }

        public static Users GetAdminByName(string name, string dbConnection)
        {
            var userClass = new Users();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAdminByName", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = name;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    userClass = GetFullUserFromSqlReader(reader, dbConnection);


                }
                connection.Close();
                reader.Close();
            }
            return userClass;
        }

        public static List<Users> GetAllAdmins(string dbConnection)
        {
            var userList = new List<Users>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllAdmins", connection);

                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = GetFullUserFromSqlReader(reader, dbConnection);
                    userList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return userList;
        }

        public static List<Users> GetAllOperators(string dbConnection)
        {
            var userList = new List<Users>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllOperators", connection);

                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = GetFullUserFromSqlReader(reader, dbConnection);
                    userList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return userList;
        }

        public static List<Users> GetAllUnassignedOperators(string dbConnection)
        {
            var userList = new List<Users>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllUnassignedOperators", connection);

                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = GetFullUserFromSqlReader(reader, dbConnection);
                    userList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return userList;
        }

        public static List<Users> GetAllUnassignedManagers(int cid,string dbConnection)
        {
            var userList = new List<Users>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllUnassignedManagers", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                sqlCommand.Parameters["@CustomerId"].Value = cid;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = GetFullUserFromSqlReader(reader, dbConnection);
                    userList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return userList;
        }
  
        public static Users GetFullAccountUserFromSqlReader(SqlDataReader reader, string dbConn)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

            var userClass = new Users();

            if (!string.IsNullOrEmpty(reader["ID"].ToString()))
                userClass.ID = Convert.ToInt32(reader["ID"].ToString());
            userClass.Username = reader["Username"].ToString();
            userClass.Password = reader["Password"].ToString();
            userClass.FirstName = reader["FirstName"].ToString();
            userClass.LastName = reader["LastName"].ToString();
            userClass.Email = reader["Email"].ToString();
            if (!string.IsNullOrEmpty(reader["RoleId"].ToString()))
                userClass.RoleId = Convert.ToInt32(reader["RoleId"].ToString());
            if (!string.IsNullOrEmpty(reader["Latitude"].ToString()))
                userClass.Latitude = Convert.ToSingle(reader["Latitude"].ToString());
            if (!string.IsNullOrEmpty(reader["Longitude"].ToString()))
                userClass.Longitude = Convert.ToSingle(reader["Longitude"].ToString());


            if (!string.IsNullOrEmpty(reader["LastLogin"].ToString()))
                userClass.LastLogin = Convert.ToDateTime(reader["LastLogin"].ToString());
            if (!string.IsNullOrEmpty(reader["CreatedBy"].ToString()))
                userClass.CreatedBy = reader["CreatedBy"].ToString();
            if (!string.IsNullOrEmpty(reader["CreatedDate"].ToString()))
                userClass.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            if (!string.IsNullOrEmpty(reader["UpdatedDate"].ToString()))
                userClass.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"].ToString());
            if (!string.IsNullOrEmpty(reader["UpdatedBy"].ToString()))
                userClass.UpdatedBy = reader["UpdatedBy"].ToString();

            // To check if it is mobile/tablet user.
            if (columns.Contains("DeviceType") && reader["DeviceType"] != DBNull.Value)
            {
                if (!string.IsNullOrEmpty(reader["DeviceType"].ToString()))
                    userClass.DeviceType = Convert.ToInt32(reader["DeviceType"].ToString());
            }

            userClass.Register = reader["Register"].ToString();

            if (userClass.Register == ((int)UserState.Disable).ToString())
                userClass.State = UserState.Disable.ToString();
            else if (userClass.Register == ((int)UserState.Enable).ToString())
                userClass.State = UserState.Enable.ToString();
            else if (userClass.Register == ((int)UserState.UnRegistered).ToString())
                userClass.State = UserState.UnRegistered.ToString();
            else if (userClass.Register == ((int)UserState.Curfew).ToString())
                userClass.State = UserState.Curfew.ToString();
            else
                userClass.State = UserState.Unknown.ToString();

            if (columns.Contains( "Engaged"))
            {

                if (reader["Engaged"] != DBNull.Value)
                    userClass.Engaged = Convert.ToBoolean(reader["Engaged"].ToString());
                if (reader["EngagedIn"] != DBNull.Value)
                    userClass.EngagedIn = reader["EngagedIn"].ToString();

            }
            if (columns.Contains( "AccountId"))
                if (reader["AccountId"] != DBNull.Value)
                    userClass.AccountId = Convert.ToInt32(reader["AccountId"]);

            if (columns.Contains( "AccountName"))
                userClass.AccountName = reader["AccountName"].ToString();

            if (columns.Contains( "Active")  && reader["Active"] != DBNull.Value)
                userClass.Active = Convert.ToInt16(reader["Active"].ToString());
            if (columns.Contains( "LoginDate") && reader["LoginDate"] != DBNull.Value)
                userClass.LoginDate = Convert.ToDateTime(reader["LoginDate"].ToString());

            if (columns.Contains( "SiteId") && reader["SiteId"] != DBNull.Value)
                userClass.SiteId = Convert.ToInt32(reader["SiteId"]);

            return userClass;
        }

        public static Users GetFullUserFromSqlReader(SqlDataReader reader, string dbConn)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

            var userClass = new Users();

            if (!string.IsNullOrEmpty(reader["ID"].ToString()))
                userClass.ID = Convert.ToInt32(reader["ID"].ToString());
            userClass.Username = reader["Username"].ToString();
            userClass.Password = reader["Password"].ToString();
            userClass.FirstName = reader["FirstName"].ToString();
            userClass.LastName = reader["LastName"].ToString();
            userClass.Email = reader["Email"].ToString();
            if (!string.IsNullOrEmpty(reader["RoleId"].ToString()))
                userClass.RoleId = Convert.ToInt32(reader["RoleId"].ToString());
            if (!string.IsNullOrEmpty(reader["Latitude"].ToString()))
                userClass.Latitude = Convert.ToSingle(reader["Latitude"].ToString());
            if (!string.IsNullOrEmpty(reader["Longitude"].ToString()))
                userClass.Longitude = Convert.ToSingle(reader["Longitude"].ToString());

            if (!string.IsNullOrEmpty(reader["LastLogin"].ToString()))
                userClass.LastLogin = Convert.ToDateTime(reader["LastLogin"].ToString());
            if (!string.IsNullOrEmpty(reader["CreatedBy"].ToString()))
                userClass.CreatedBy = reader["CreatedBy"].ToString();
            if (!string.IsNullOrEmpty(reader["CreatedDate"].ToString()))
                userClass.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            if (!string.IsNullOrEmpty(reader["UpdatedDate"].ToString()))
                userClass.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"].ToString());
            if (!string.IsNullOrEmpty(reader["UpdatedBy"].ToString()))
                userClass.UpdatedBy = reader["UpdatedBy"].ToString();

            // To check if it is mobile/tablet user.
            if (columns.Contains("DeviceType") && reader["DeviceType"] != DBNull.Value)
            {
                if (!string.IsNullOrEmpty(reader["DeviceType"].ToString()))
                    userClass.DeviceType = Convert.ToInt32(reader["DeviceType"].ToString());
            }
            if (columns.Contains("CustomerLinkId") && reader["CustomerLinkId"] != DBNull.Value)
            {
                if (!string.IsNullOrEmpty(reader["CustomerLinkId"].ToString()))
                    userClass.CustomerLinkId = Convert.ToInt32(reader["CustomerLinkId"].ToString());
            }

                        if (columns.Contains("ContractLinkId") && reader["ContractLinkId"] != DBNull.Value)
                        {
                            if (!string.IsNullOrEmpty(reader["ContractLinkId"].ToString()))
                                userClass.ContractLinkId = Convert.ToInt32(reader["ContractLinkId"].ToString());
                        }
             

            userClass.Register = reader["Register"].ToString();

            if (userClass.Register == ((int)UserState.Disable).ToString())
                userClass.State = UserState.Disable.ToString();
            else if (userClass.Register == ((int)UserState.Enable).ToString())
                userClass.State = UserState.Enable.ToString();
            else if (userClass.Register == ((int)UserState.UnRegistered).ToString())
                userClass.State = UserState.UnRegistered.ToString();
            else if (userClass.Register == ((int)UserState.Curfew).ToString())
                userClass.State = UserState.Curfew.ToString();
            else
                userClass.State = UserState.Unknown.ToString();


            if (columns.Contains( "Telephone"))
                userClass.Telephone = reader["Telephone"].ToString();

            if (columns.Contains( "CustomerUName"))
                userClass.CustomerUName = reader["CustomerUName"].ToString();
            

            if (columns.Contains( "Engaged"))
            {
                if (reader["Engaged"] != DBNull.Value)
                    userClass.Engaged = Convert.ToBoolean(reader["Engaged"].ToString());
                if (reader["EngagedIn"] != DBNull.Value)
                    userClass.EngagedIn = reader["EngagedIn"].ToString();
            }
            if (columns.Contains( "AccountId"))
                userClass.AccountId = Convert.ToInt32(reader["AccountId"]);

            if (columns.Contains( "AccountName"))
                userClass.AccountName = reader["AccountName"].ToString();

            if (columns.Contains( "Active") && reader["Active"] != DBNull.Value)
            {
                  userClass.Active = Convert.ToInt32(reader["Active"]);
            }

            if (columns.Contains( "SiteId") && reader["SiteId"] != DBNull.Value)
                userClass.SiteId = Convert.ToInt32(reader["SiteId"]);
            
            if (columns.Contains( "GeofenceStatus") && reader["GeofenceStatus"] != DBNull.Value)
            {
                userClass.GeofenceStatus = Convert.ToInt32(reader["GeofenceStatus"]);
            }
            if (columns.Contains( "SiteName") && reader["SiteName"] != DBNull.Value)
                userClass.SiteName = reader["SiteName"].ToString();

            if (columns.Contains( "CustomerInfoId") && reader["CustomerInfoId"] != DBNull.Value)
                userClass.CustomerInfoId = Convert.ToInt32(reader["CustomerInfoId"]);            

            if (columns.Contains( "EmployeeID") && reader["EmployeeID"] != DBNull.Value)
                userClass.EmployeeID = reader["EmployeeID"].ToString();

            if (columns.Contains( "Gender") && reader["Gender"] != DBNull.Value)
                userClass.Gender = reader["Gender"].ToString();

            if (columns.Contains( "IsServerPortal") && reader["IsServerPortal"] != DBNull.Value)
                userClass.IsServerPortal = Convert.ToBoolean(reader["IsServerPortal"]);
            else
                userClass.IsServerPortal = false;

            if (columns.Contains("IsWatch") && reader["IsWatch"] != DBNull.Value)
                userClass.IsWatch = Convert.ToBoolean(reader["IsWatch"]);
            else
                userClass.IsWatch = false;

            if (columns.Contains("TimeZone") && reader["TimeZone"] != DBNull.Value)
                userClass.TimeZone = Convert.ToDouble(reader["TimeZone"].ToString());

            try
            {
                ////var settings = MIMSConfigSetting.GetMIMSConfigSettings(dbConn);
                ////var logDetails = LoginSession.GetLoginStatusByUsername(userClass.Username, dbConn);
                //////var dt = DateTime.Now.Subtract(new TimeSpan(0, 0,0,Convert.ToInt32(settings.MIMSMobileGPSInterval), 15));

                //if (!logDetails)
                //{
                //    //Offline
                //    userClass.imageURL = @"../Images/red.png";
                //    userClass.Status = "Offline";
                //}
                //else
                //{
                //    var newLoginDate = userClass.LastLogin.Value.ToString("d MMM  hh:mm tt");
                //    if (userClass.LastLogin.Value > dt)
                //    {
                //        //Online  
                //        userClass.imageURL = @"../Images/green.png";
                //        userClass.Status = "Online";
                //    }
                //    else
                //    {
                //        //Yellow         
                //        userClass.imageURL = @"../Images/yellow1.png";
                //        userClass.Status = "GPS Unavailable";
                //    }
                //}

                userClass.ShiftOnDesc = "Off Duty";

                if (columns.Contains("ShiftOn"))
                {
                    if (reader["ShiftOn"] != DBNull.Value)
                        userClass.ShiftOn = Convert.ToInt32(reader["ShiftOn"].ToString());
                }

                if(userClass.ShiftOn == 1)
                    userClass.ShiftOnDesc = "On Duty";

                if (userClass.IsWatch)
                {
                    if (userClass.Register == ((int)Users.UserState.Enable).ToString())
                    {
                        if (userClass.ShiftOn == 1)
                        {
                            userClass.Active = (int)HealthCheck.DeviceStatus.Online;
                        }
                    }
                    else
                    {
                        userClass.ShiftOn = 0;
                        userClass.Active = (int)HealthCheck.DeviceStatus.Offline;
                        userClass.ShiftOnDesc = "Off Duty";
                    }
                }

                if (userClass.Active == (int)HealthCheck.DeviceStatus.Offline)
                {
                    //Offline
                    userClass.imageURL = @"../Images/red.png";
                    userClass.Status = "Offline";
                }
                else if (userClass.Active == (int)HealthCheck.DeviceStatus.Online)
                {
                    //Online  
                    userClass.imageURL = @"../Images/green.png";
                    userClass.Status = "Online";
                }
                else if (userClass.Active == (int)HealthCheck.DeviceStatus.Error)
                {
                    //Yellow         
                    userClass.imageURL = @"../Images/yellow1.png";
                    userClass.Status = "GPS Unavailable";
                }

                //if (columns.Contains( "DayOffUsed") && reader["DayOffUsed"] != DBNull.Value)
                //    userClass.DayOffUsed = Convert.ToInt32(Decrypt.DecryptData(reader["DayOffUsed"].ToString(), true, dbConn));

                //if (columns.Contains( "DayInLieu") && reader["DayInLieu"] != DBNull.Value)
                //    userClass.DayInLieu = Convert.ToInt32(Decrypt.DecryptData(reader["DayInLieu"].ToString(), true, dbConn));

                //if (columns.Contains( "OTHours") && reader["OTHours"] != DBNull.Value)
                //    userClass.OTHours = Convert.ToInt32(Decrypt.DecryptData(reader["OTHours"].ToString(), true, dbConn));
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Users Business Layer", "GetFullUserFromSqlReader", ex, dbConn, 0);
            }
            return userClass;
        }

        public static Users GetUserById(int id, string dbConnection)
        {
            var userClass = new Users();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetUserById", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@UserId", SqlDbType.NVarChar));
                sqlCommand.Parameters["@UserId"].Value = id;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    userClass = GetFullUserFromSqlReader(reader, dbConnection);
                }
                connection.Close();
                reader.Close();
            }
            return userClass;
        }

        public static List<Users> GetAllUserByState(int state, string dbConnection)
        {
            var userList = new List<Users>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllUserByState", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Register", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Register"].Value = state.ToString();
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = GetFullUserFromSqlReader(reader, dbConnection);
                                        userList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return userList;
        }

        public static Users GetSuperAdmin(string dbConnection)
        {
            var userClass = new Users();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetSuperAdmin", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {


                    if (!string.IsNullOrEmpty(reader["ID"].ToString()))
                        userClass.ID = Convert.ToInt32(reader["ID"].ToString());

                    userClass.Username = reader["Username"].ToString();
                    userClass.Password = reader["Password"].ToString();
                    userClass.Email = reader["Email"].ToString();
                    userClass.LastLogin = Convert.ToDateTime(reader["LastLogin"].ToString());
                    userClass.CreatedBy = reader["CreatedBy"].ToString();
                    userClass.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    userClass.RoleId = Convert.ToInt32(reader["RoleId"].ToString());


                }
                connection.Close();
                reader.Close();
            }
            return userClass;
        }

        public static Users Login(string username, string password, string dbConnection)
        {
            Users userClass = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var command = new SqlCommand("LoginUser", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter paramUsername = new SqlParameter("@Username", SqlDbType.VarChar);
                paramUsername.Value = username;
                command.Parameters.Add(paramUsername);

                SqlParameter paramPassword = new SqlParameter("@Password", SqlDbType.VarChar);
                paramPassword.Value = Encrypt.EncryptData(password, true, dbConnection);
                command.Parameters.Add(paramPassword);

                var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    userClass = new Users();
                    userClass = GetFullUserFromSqlReader(reader, dbConnection);
                    
                    //if (!string.IsNullOrEmpty(reader["ID"].ToString()))
                    //    userClass.ID = Convert.ToInt32(reader["ID"].ToString());
                    //userClass.Username = reader["Username"].ToString();
                    //userClass.Password = reader["Password"].ToString();
                    //userClass.Email = reader["Email"].ToString();
                    //if (!string.IsNullOrEmpty(reader["RoleId"].ToString()))
                    //    userClass.RoleId = Convert.ToInt32(reader["RoleId"].ToString());
                    //if (!string.IsNullOrEmpty(reader["Latitude"].ToString()))
                    //    userClass.Latitude = Convert.ToSingle(reader["Latitude"].ToString());
                    //if (!string.IsNullOrEmpty(reader["Longitude"].ToString()))
                    //    userClass.Longitude = Convert.ToSingle(reader["Longitude"].ToString());
                    //if (!string.IsNullOrEmpty(reader["LastLogin"].ToString()))
                    //    userClass.LastLogin = Convert.ToDateTime(reader["LastLogin"].ToString());
                }

                connection.Close();
                reader.Close();
            }
            return userClass;
        }

        public static Users Login(string accountName, string username, string password, string dbConnection)
        {
            Users userClass = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var command = new SqlCommand("LoginUser", connection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add("@Username", SqlDbType.VarChar).Value = username;
                command.Parameters.Add("@Password", SqlDbType.VarChar).Value = Encrypt.EncryptData(password, true, dbConnection);
                command.Parameters.Add("@Account", SqlDbType.VarChar).Value = accountName;

                var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    userClass = new Users();
                    userClass = GetFullUserFromSqlReader(reader, dbConnection);
                }

                connection.Close();
                reader.Close();
            }
            return userClass;
        }

        public static bool DeleteUserById(int id, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var cmd = new SqlCommand("DeleteUserById", connection);

                cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                cmd.Parameters["@Id"].Value = id;

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            }
            return true;
        }

        public static bool DeleteUserByIdAndUsername(int id,string username ,string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var cmd = new SqlCommand("DeleteUserByIdAndUsername", connection);

                cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                cmd.Parameters["@Id"].Value = id;

                cmd.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                cmd.Parameters["@Name"].Value = username;

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            }
            return true;
        }

        public static List<Users> GetAllMimsMobileOnlineUsers(string dbConnection)
        {
            var userList = new List<Users>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllMimsMobileOnlineUsers", connection);

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = GetFullUserFromSqlReader(reader, dbConnection);
                    userList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return userList;
        }

        public static List<Users> GetAllUsersByCustomerId(int id, string dbConnection)
        {
            var userList = new List<Users>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllUsersByCustomerId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@CId", SqlDbType.Int));
                sqlCommand.Parameters["@CId"].Value = id;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = GetFullUserFromSqlReader(reader, dbConnection);
                    userList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return userList;
        }

        public static List<Users> GetAllUsersByCustomerIdNotIncludeCustomerUser(int id, string dbConnection)
        {
            var userList = new List<Users>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllUsersByCustomerIdNotIncludeCustomerUser", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@CId", SqlDbType.Int));
                sqlCommand.Parameters["@CId"].Value = id;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = GetFullUserFromSqlReader(reader, dbConnection);
                    userList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return userList;
        }

        public static List<Users> GetAllMimsMobileOnlineUsersByManagerIdInThePastInterval(DateTime dt, int userId, string dbConnection)
        {
            var userList = new List<Users>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllMimsMobileOnlineUsersByManagerIdInThePastInterval", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@ManagerId", SqlDbType.Int));
                sqlCommand.Parameters["@ManagerId"].Value = userId;
                sqlCommand.Parameters.Add(new SqlParameter("@DateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@DateTime"].Value = dt;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = GetFullUserFromSqlReader(reader, dbConnection);
                    userList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return userList;
        }

        public static List<Users> GetAllMimsMobileOnlineUsersLoggedInThePastInterval(DateTime dt, string dbConnection)
        {
            var userList = new List<Users>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllMimsMobileOnlineUsersLoggedInThePastInterval", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@DateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@DateTime"].Value = dt;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = GetFullUserFromSqlReader(reader, dbConnection);
                    userList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return userList;
        }
        
        public static List<Users> GetAllMimsMobileOnlineUsersNotLoggedInThePastInterval(DateTime dt,string dbConnection)
        {
            var userList = new List<Users>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllMimsMobileOnlineUsersNotLoggedInThePastInterval", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@DateTime", SqlDbType.DateTime));
                sqlCommand.Parameters["@DateTime"].Value = dt;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = GetFullUserFromSqlReader(reader, dbConnection);
                    userList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return userList;
        }

        public static List<Users> GetAllMimsMobileOfflineUsers(string dbConnection)
        {
            var userList = new List<Users>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllMimsMobileOfflineUsers", connection);

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = GetFullUserFromSqlReader(reader, dbConnection);
                    userList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return userList;
        }

        public static List<Users> GetAllMimsMobileOnlineUsersByManagerId(int userId, string dbConnection)
        {
            var userList = new List<Users>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllMimsMobileOnlineUsersByManagerId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@ManagerId", SqlDbType.Int));
                sqlCommand.Parameters["@ManagerId"].Value = userId;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = GetFullUserFromSqlReader(reader, dbConnection);
                    userList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return userList;
        }

        public static List<Users> GetAllMimsMobileOfflineUsersByManagerId(int userId, string dbConnection)
        {
            var userList = new List<Users>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllMimsMobileOfflineUsersByManagerId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@ManagerId", SqlDbType.Int));
                sqlCommand.Parameters["@ManagerId"].Value = userId;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = GetFullUserFromSqlReader(reader, dbConnection);
                    userList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return userList;
        }

        public static List<Users> GetAllUsersByType(int type, string dbConnection)
        {
            var userList = new List<Users>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllUsersByType", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Type", SqlDbType.Int));
                sqlCommand.Parameters["@Type"].Value = type;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = GetFullUserFromSqlReader(reader, dbConnection);
                    userList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return userList;
        }

        public static List<Users> GetAllUsersByTypeAndManager(int type, int mangId, string dbConnection)
        {
            var userList = new List<Users>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllUsersByTypeAndManager", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Type", SqlDbType.Int));
                sqlCommand.Parameters["@Type"].Value = type;
                sqlCommand.Parameters.Add(new SqlParameter("@ManagerId", SqlDbType.Int));
                sqlCommand.Parameters["@ManagerId"].Value = mangId;
                
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = GetFullUserFromSqlReader(reader, dbConnection);
                    userList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return userList;
        }

        public static bool UpdateUserGeofenceStatus(string username, int geofenceStatus, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("UpdateUserGeofenceStatus", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@username", SqlDbType.NVarChar));
                sqlCommand.Parameters["@username"].Value = username;

                sqlCommand.Parameters.Add(new SqlParameter("@GeofenceStatus", SqlDbType.Int));
                sqlCommand.Parameters["@GeofenceStatus"].Value = ( geofenceStatus== -1)? System.Data.SqlTypes.SqlInt32.Null: geofenceStatus;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlCommand.CommandText = "UpdateUserGeofenceStatus";

                sqlCommand.ExecuteNonQuery();

                connection.Close();
            }
            return true;
        }

        public static bool IsMobileOrServerUser(string username, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("IsMobileOrServerUser", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@username", SqlDbType.NVarChar));
                sqlCommand.Parameters["@username"].Value = username;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlCommand.CommandText = "IsMobileOrServerUser";

                sqlCommand.ExecuteNonQuery();

                connection.Close();
            }
            return true;
        }
    }
}
