﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class  GroupUserAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public int Id { get; set; }
        public int GroupId { get; set; }
        public int UserId { get; set; }
        public string Username { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public int SiteId { get; set; }
        public static bool InsertGroupUserAudit(GroupUserAudit groupUserAudit, string dbConnection)
        {
            var baseAuditId = BaseAudit.InsertBaseAudit(groupUserAudit.BaseAudit, dbConnection);

            if (groupUserAudit != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertGroupUserAudit", connection);

                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;
                    cmd.Parameters.Add("@GroupId", SqlDbType.Int).Value = groupUserAudit.GroupId;
                    cmd.Parameters.Add("@UserId", SqlDbType.Int).Value = groupUserAudit.UserId;
                    cmd.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = groupUserAudit.CreateDate;
                    cmd.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = groupUserAudit.CreatedBy;
                    cmd.Parameters.Add("@SiteId", SqlDbType.Int).Value = groupUserAudit.SiteId;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }
        private static GroupUserAudit GetGroupUserAuditFromSqlReader(SqlDataReader reader)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

            var groupUserAudit = new GroupUserAudit();
            groupUserAudit.BaseAudit = BaseAudit.GetBaseAuditFromSqlReader(reader);

            if (columns.Contains("Id") && reader["Id"] != DBNull.Value)
                groupUserAudit.Id = Convert.ToInt32(reader["Id"].ToString());
            if (columns.Contains("Username") && reader["Username"] != DBNull.Value)
                groupUserAudit.Username = reader["Username"].ToString();
            if (columns.Contains("CreateDate") && reader["CreateDate"] != DBNull.Value)
                groupUserAudit.CreateDate = Convert.ToDateTime(reader["CreateDate"].ToString());
            if (columns.Contains("CreatedBy") && reader["CreatedBy"] != DBNull.Value)
                groupUserAudit.CreatedBy = reader["CreatedBy"].ToString();
            if (columns.Contains("GroupId") && reader["GroupId"] != DBNull.Value)
                groupUserAudit.GroupId = Convert.ToInt16(reader["GroupId"].ToString());
            if (columns.Contains("UserId") && reader["UserId"] != DBNull.Value)
                groupUserAudit.UserId = Convert.ToInt16(reader["UserId"].ToString());
            if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                groupUserAudit.SiteId = Convert.ToInt16(reader["SiteId"].ToString());

   
            return groupUserAudit;
        } 
        public static List<GroupUserAudit> GetAllGroupUserAudit(string dbConnection)
        {
            var allGroupUsers = new List<GroupUserAudit>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var cmd = new SqlCommand("GetAllGroupUserAudit", connection);

                cmd.CommandType = CommandType.StoredProcedure;

                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var groupUser = GetGroupUserAuditFromSqlReader(reader);
                    allGroupUsers.Add(groupUser);
                }
                connection.Close();
                reader.Close();
                return allGroupUsers;
            }
        }

        
    }
}
