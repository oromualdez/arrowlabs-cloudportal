﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class DirectorManagerAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public int DirectorId { get; set; }
        public int ManagerId { get; set; }
        public string ManagerName { get; set; }
        public string ManagerAccountName { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public int SiteId { get; set; }
        public int CustomerId { get; set; }
        public static bool InsertDirectorManager(List<DirectorManagerAudit> directormanager, string dbConnection)
        {
            

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                foreach (var item in directormanager)
                {
                    var baseAuditId = BaseAudit.InsertBaseAudit(item.BaseAudit, dbConnection);

                    var sqlCommand = new SqlCommand("InsertDirectorManager", connection);

                    sqlCommand.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;

                    sqlCommand.Parameters.Add(new SqlParameter("@DirectorId", SqlDbType.Int));
                    sqlCommand.Parameters["@DirectorId"].Value = item.DirectorId;

                    sqlCommand.Parameters.Add(new SqlParameter("@ManagerId", SqlDbType.Int));
                    sqlCommand.Parameters["@ManagerId"].Value = item.ManagerId;

                    sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    sqlCommand.Parameters["@SiteId"].Value = item.SiteId;

                    sqlCommand.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    sqlCommand.Parameters["@CustomerId"].Value = item.CustomerId;
                    

                    sqlCommand.Parameters.Add(new SqlParameter("@ManagerName", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@ManagerName"].Value = item.ManagerName;

                    sqlCommand.Parameters.Add(new SqlParameter("@ManagerAccountName", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@ManagerAccountName"].Value = item.ManagerAccountName;

                    sqlCommand.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    sqlCommand.Parameters["@CreatedDate"].Value = item.CreatedDate;

                    sqlCommand.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    sqlCommand.Parameters["@UpdatedDate"].Value = item.UpdatedDate;

                    sqlCommand.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@CreatedBy"].Value = item.CreatedBy;

                    sqlCommand.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@UpdatedBy"].Value = item.UpdatedBy;

                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    var reader = sqlCommand.ExecuteNonQuery();
                }
                connection.Close();
            }
            return true;
        }
    }
}
