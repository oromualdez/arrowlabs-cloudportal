﻿using Arrowlabs.Mims.Audit.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Business.Layer
{
    public class Duty
    {
        public int Id { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        
        public DateTime? DutyDate { get; set; }
        public int DutyStatus { get; set; }
        public int SiteId { get; set; }
        public int CustomerId { get; set; }
        public float Latitude { get; set; }
        public float Longtitude { get; set; }

        public enum DutyStatusType
        {
            DutyOff = 0,
            DutyOn = 1
        }
        public static int InsertOrUpdateDuty(Duty duty, string dbConnection, string auditDbConnection = "", bool isAuditEnabled = true)
        {
            int id = duty.Id;

            var action = (duty.Id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;

            if (duty != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateDuty", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = duty.Id;

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = duty.CustomerId;
                 
                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = duty.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = duty.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@DutyDate", SqlDbType.DateTime));
                    cmd.Parameters["@DutyDate"].Value = duty.DutyDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = duty.CreatedBy;
                   
                    cmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@UpdatedBy"].Value = duty.UpdatedBy;

                    cmd.Parameters.Add(new SqlParameter("@DutyStatus", SqlDbType.Int));
                    cmd.Parameters["@DutyStatus"].Value = duty.DutyStatus;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = duty.SiteId;

                    cmd.Parameters.Add(new SqlParameter("@Longitude", SqlDbType.Float));
                    cmd.Parameters["@Longitude"].Value = duty.Longtitude;

                   cmd.Parameters.Add(new SqlParameter("@Latitude", SqlDbType.Float));
                    cmd.Parameters["@Latitude"].Value = duty.Latitude;
                      

                    
                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandText = "InsertOrUpdateDuty";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();


                    if (id == 0)
                        id = (int)returnParameter.Value;
                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            duty.Id = id;
                            var audit = new DutyAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, duty.CreatedBy);
                            Reflection.CopyProperties(duty, audit);
                            DutyAudit.InsertDutyAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertDutyAudit", ex, dbConnection);
                    }
                }
            }
            return id;
        }
        private static Duty GetDutyFromSqlReader(SqlDataReader reader)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
            var duty = new Duty();
            if (reader["Id"] != DBNull.Value)
                duty.Id = Convert.ToInt32(reader["Id"].ToString());
            if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                duty.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
            if (columns.Contains("CustomerId") && reader["CustomerId"] != DBNull.Value)
                duty.CustomerId = Convert.ToInt32(reader["CustomerId"].ToString());
            if (columns.Contains("CreatedBy") && reader["CreatedBy"] != DBNull.Value)
                duty.CreatedBy = reader["CreatedBy"].ToString();
            if (columns.Contains("UpdatedBy") && reader["UpdatedBy"] != DBNull.Value)
                duty.UpdatedBy = reader["UpdatedBy"].ToString();
            if (columns.Contains("CreatedDate") && reader["CreatedDate"] != DBNull.Value)
                duty.CreatedDate = Convert.ToDateTime( reader["CreatedDate"].ToString());
            if (columns.Contains("UpdatedDate") && reader["UpdatedDate"] != DBNull.Value)
                duty.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"].ToString());
            if (columns.Contains("DutyDate") && reader["DutyDate"] != DBNull.Value)
                duty.DutyDate = Convert.ToDateTime(reader["DutyDate"].ToString());

            if (columns.Contains("DutyStatus") && reader["DutyStatus"] != DBNull.Value)
                duty.DutyStatus =Convert.ToInt16( reader["DutyStatus"].ToString());

            if (columns.Contains("Longitude") && reader["Longitude"] != DBNull.Value)
                duty.Longtitude = (!String.IsNullOrEmpty(reader["Longitude"].ToString())) ? Convert.ToSingle(reader["Longitude"].ToString()) : 0f;


            if (columns.Contains("Latitude") && reader["Latitude"] != DBNull.Value)
                duty.Latitude = (!String.IsNullOrEmpty(reader["Latitude"].ToString())) ? Convert.ToSingle(reader["Latitude"].ToString()) : 0f;


            return duty;
        }
        public static Duty GetDutyByUserName(string userName, DateTime dutyDate, string dbConnection)
        {
            Duty duty = null;

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetDutyByUserName", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@UserName", SqlDbType.NVarChar));
                sqlCommand.Parameters["@UserName"].Value = userName;
                sqlCommand.Parameters.Add(new SqlParameter("@DutyDate", SqlDbType.DateTime));
                sqlCommand.Parameters["@DutyDate"].Value = dutyDate;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    duty = GetDutyFromSqlReader(reader);
                }
                connection.Close();
                reader.Close();
            }

            return duty;
        }

        public static List<Duty> GetDutyByUserNameAndDate(string userName, DateTime dutyDate, DateTime todutyDate, string dbConnection)
        {
            List<Duty> duty = new List<Duty>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetDutyByUserNameAndDate", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@UserName", SqlDbType.NVarChar));
                sqlCommand.Parameters["@UserName"].Value = userName;
                sqlCommand.Parameters.Add(new SqlParameter("@DutyDate", SqlDbType.DateTime));
                sqlCommand.Parameters["@DutyDate"].Value = dutyDate;
                sqlCommand.Parameters.Add(new SqlParameter("@ToDutyDate", SqlDbType.DateTime));
                sqlCommand.Parameters["@ToDutyDate"].Value = todutyDate;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    var dty = GetDutyFromSqlReader(reader);
                    duty.Add(dty);
                }
                connection.Close();
                reader.Close();
            }

            return duty;
        }
    }

}