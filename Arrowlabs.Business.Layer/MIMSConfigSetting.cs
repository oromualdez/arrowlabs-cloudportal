﻿namespace Arrowlabs.Business.Layer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Runtime.Serialization;
    using System.Data.SqlClient;
    using System.Data;
    using Arrowlabs.Mims.Audit.Models;

    [Serializable]
    [DataContract]
    public class MIMSConfigSetting
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string MIMSServerAddress { get; set; }
        [DataMember]
        public string MIMSMobileAddress { get; set; }
        [DataMember]
        public float MIMSMobileGPSInterval { get; set; }
        [DataMember]
        public DateTime? CreatedDate { get; set; }
        [DataMember]
        public DateTime? UpdatedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public string UpdatedBy { get; set; }
        [DataMember]
        public int MaxUploadSize { get; set; }
        [DataMember]
        public int MaxNumberAttachments { get; set; }

        [DataMember]
        public string FilePath { get; set; }
        [DataMember]
        public int LiveVideoFrameWidth { get; set; }
        [DataMember]
        public int LiveVideoFrameHeight { get; set; }
        [DataMember]
        public string UploadServiceUrl { get; set; }

        private static MIMSConfigSetting GetMIMSConfigSettingFromSqlReader(SqlDataReader reader)
        {
            var configSetting = new MIMSConfigSetting();
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
            if (reader["Id"] != DBNull.Value)
                configSetting.Id = Convert.ToInt32(reader["Id"].ToString());
            if (reader["MIMSServerAddress"] != DBNull.Value)
                configSetting.MIMSServerAddress = reader["MIMSServerAddress"].ToString();
            if (reader["MIMSMobileAddress"] != DBNull.Value)
                configSetting.MIMSMobileAddress = reader["MIMSMobileAddress"].ToString();
            if (reader["MIMSMobileGPSInterval"] != DBNull.Value)
                configSetting.MIMSMobileGPSInterval = Convert.ToSingle(reader["MIMSMobileGPSInterval"].ToString());
            if (reader["CreatedDate"] != DBNull.Value)
                configSetting.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            if (reader["Updateddate"] != DBNull.Value)
                configSetting.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
            if (reader["CreatedBy"] != DBNull.Value)
                configSetting.UpdatedBy = reader["CreatedBy"].ToString();
            if (reader["CreatedBy"] != DBNull.Value)
                configSetting.CreatedBy = reader["CreatedBy"].ToString();
            if (reader["MaxUploadSize"] != DBNull.Value)
                if (!string.IsNullOrEmpty(reader["MaxUploadSize"].ToString()))
                    configSetting.MaxUploadSize = Convert.ToInt32(reader["MaxUploadSize"].ToString());
            if (reader["MaxNumberAttachments"] != DBNull.Value)
                if (!string.IsNullOrEmpty(reader["MaxNumberAttachments"].ToString()))
                    configSetting.MaxNumberAttachments = Convert.ToInt32(reader["MaxNumberAttachments"].ToString());

            if (columns.Contains( "FilePath"))
            {
                if (reader["FilePath"] != DBNull.Value)
                    configSetting.FilePath = reader["FilePath"].ToString();
            }
       

            if (columns.Contains("LiveVideoFrameWidth") && reader["LiveVideoFrameWidth"] != DBNull.Value)
            {
                configSetting.LiveVideoFrameWidth = Convert.ToInt32(reader["LiveVideoFrameWidth"].ToString());
            }
            if (columns.Contains("LiveVideoFrameHeight") && reader["LiveVideoFrameHeight"] != DBNull.Value)
            {
                configSetting.LiveVideoFrameHeight = Convert.ToInt32(reader["LiveVideoFrameHeight"].ToString());
            }
            if (columns.Contains("UploadServiceUrl") && reader["UploadServiceUrl"] != DBNull.Value)
            {
                configSetting.UploadServiceUrl = reader["UploadServiceUrl"].ToString();
            }
            
            return configSetting;
        }

        public static MIMSConfigSetting GetMIMSConfigSettings(string dbConnection)
        {
            var configSetting = new MIMSConfigSetting();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetMIMSConfigSettings", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    configSetting = GetMIMSConfigSettingFromSqlReader(reader);
                }
                connection.Close();
                reader.Close();
            }
            return configSetting;
        }

        public static bool InsertorUpdateMIMSConfigSetting(MIMSConfigSetting configSetting, string dbConnection,
            string auditDbConnection = "", bool isAuditEnabled = true)
        {
            if (configSetting != null)
            {
                int id = configSetting.Id;
                var action = (id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate; 


                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateMIMSConfigSetting", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = configSetting.Id;

                    cmd.Parameters.Add(new SqlParameter("@MIMSServerAddress", SqlDbType.NVarChar));
                    cmd.Parameters["@MIMSServerAddress"].Value = configSetting.MIMSServerAddress;

                    cmd.Parameters.Add(new SqlParameter("@MIMSMobileAddress", SqlDbType.NVarChar));
                    cmd.Parameters["@MIMSMobileAddress"].Value = configSetting.MIMSMobileAddress;

                    cmd.Parameters.Add(new SqlParameter("@MIMSMobileGPSInterval", SqlDbType.Float));
                    cmd.Parameters["@MIMSMobileGPSInterval"].Value = configSetting.MIMSMobileGPSInterval;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = configSetting.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = configSetting.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = configSetting.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@UpdatedBy"].Value = configSetting.UpdatedBy;

                    cmd.Parameters.Add(new SqlParameter("@MaxUploadSize", SqlDbType.Int));
                    cmd.Parameters["@MaxUploadSize"].Value = configSetting.MaxUploadSize;

                    cmd.Parameters.Add(new SqlParameter("@MaxNumberAttachments", SqlDbType.Int));
                    cmd.Parameters["@MaxNumberAttachments"].Value = configSetting.MaxNumberAttachments;

                    cmd.Parameters.Add(new SqlParameter("@FilePath", SqlDbType.NVarChar));
                    cmd.Parameters["@FilePath"].Value = configSetting.FilePath;

                    cmd.Parameters.Add(new SqlParameter("@UploadServiceUrl", SqlDbType.NVarChar));
                    cmd.Parameters["@UploadServiceUrl"].Value = configSetting.UploadServiceUrl;
                    

                    var returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;
                 
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();
                    
                    if (id == 0)
                        id = (int)returnParameter.Value;

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            configSetting.Id = id;
                            var audit = new MimsConfigSettingAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, configSetting.UpdatedBy);
                            Reflection.CopyProperties(configSetting, audit);
                            MimsConfigSettingAudit.InsertMimsConfigSettingAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertorUpdateMIMSConfigSetting", ex, dbConnection);
                    }

                }
            }
            return true;
        }
    }
}
