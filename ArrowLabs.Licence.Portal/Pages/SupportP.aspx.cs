﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Arrowlabs.Business.Layer;
using System.Web.Services;
using System.Web.Configuration;
using System.Globalization;
using System.Text;
using PushSharp;
using PushSharp.Apple;
using PushSharp.Core;
using System.IO;
using System.Threading;
using ArrowLabs.Licence.Portal.Helpers;
using iTextSharp.text;
using iTextSharp.text.pdf;


namespace ArrowLabs.Licence.Portal.Pages
{
    public partial class SupportP : System.Web.UI.Page
    {
        static List<string> VideoExtensions = new List<string> { ".AVI", ".MP4", ".MKV", ".FLV" };

        static string dbConnection { get; set; }
        static string dbConnectionAudit { get; set; }
        protected string senderName;
        protected string senderName2;
        protected string grpOptionView;
        protected string customersDisplay;
        protected string tskOptionView;
        protected string resolvedPercent = "0";
        protected string pendingPercent;
        protected string inprogressPercent;
        protected string completedPercent;
        protected string pendingLabel;
        protected string inprogressLabel;
        protected string completedLabel;
        protected string totalLabel;
        int completedCount;
        int inprogressCount;
        int pendingCount;
        int total;
        protected string ipaddress;
        protected string userinfoDisplay;
        protected string senderName3;
        static ClientLicence getClientLic;
        protected string siteName;
        protected void Page_Load(object sender, EventArgs e)
        {
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/Default.aspx");
            }
            fpath = Server.MapPath("~/Uploads/");
            userinfoDisplay = "block";
            tskOptionView = "none";
            grpOptionView = "none";
            customersDisplay = "none";
            dbConnection = CommonUtility.dbConnection;
            dbConnectionAudit = CommonUtility.dbConnectionAudit;
            senderName = User.Identity.Name;
            senderName2 = Encrypt.EncryptData(senderName, true, dbConnection);
            var userinfo = Users.GetUserByName(senderName, dbConnection);
            try
            {
                senderName3 = userinfo.CustomerUName;
                var configtSettings = ConfigSettings.GetConfigSettings(dbConnection);
                ipaddress = configtSettings.ChatServerUrl + "/signalr";

                if (userinfo.SiteId > 0)
                {
                    siteName = "<a style='margin-left:0px;color:gray' href='#' class='fa fa-building fa-lg'></a><a style='font-size:smaller;color:gray;margin-right:5px'>" + userinfo.SiteName + "</a>";
                }

                getClientLic = ClientLicence.GetLicenseByClientID(CommonUtility.arrowlabsKey, dbConnection);
                if (userinfo.RoleId != (int)Role.SuperAdmin)
                {
                    userinfoDisplay = "none";

                    PushNotificationDevice.UpdatePushNotificationDeviceByUsername(userinfo.Username, userinfo.SiteId, dbConnection);

                    var modules = UserModules.GetAllModulesByUsername(userinfo.Username, dbConnection);
                    foreach (var mods in modules)
                    {
                        if (mods.ModuleId == (int)Accounts.ModuleTypes.Collaboration)
                        {
                            grpOptionView = "block";
                        }
                        if (mods.ModuleId == (int)Accounts.ModuleTypes.Tasks)
                        {

                            tskOptionView = "block";
                        }
                        if (mods.ModuleId == (int)Accounts.ModuleTypes.Customer)
                        {

                            customersDisplay = "block";
                        }
                    }

                }
                else
                {
                    tskOptionView = "block";
                    grpOptionView = "block";
                    customersDisplay = "block";
                }
                if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                    userinfoDisplay = "block";
                if (userinfo.RoleId == (int)Role.Director)
                    userinfoDisplay = "block";

                senderName3 = userinfo.CustomerUName;

                if (userinfo.RoleId != (int)Role.SuperAdmin)
                {
                    var modules = UserModules.GetAllModulesByUsername(userinfo.Username, dbConnection);
                    foreach (var mods in modules)
                    {
                        if (mods.ModuleId == (int)Accounts.ModuleTypes.Customer)
                        {

                            customersDisplay = "block";
                        }
                    }

                }
                else
                {
                    customersDisplay = "block";
                }

                var startlocations = new List<Location>();
                var locations = new List<Location>();
                var offenceCategory = new List<OffenceCategory>();
                var offenceTypes = new List<OffenceType>(); 
                var off = new OffenceCategory();
                off.Id = 0;
                off.Name = "Select Category";
                offenceCategory.Add(off);

                var offenceCategorys = OffenceCategory.GetAllOffenceCategoryByCId(1, dbConnection);
                offenceCategory.AddRange(offenceCategorys);

                if (userinfo.RoleId == (int)Role.CustomerSuperadmin || userinfo.RoleId == (int)Role.CustomerUser)
                {
                    locations.AddRange(Location.GetAllMainLocationByCustomerId(userinfo.CustomerInfoId, dbConnection));
                    locations.AddRange(Location.GetAllSubLocationByCustomerId(userinfo.CustomerInfoId, dbConnection));

                    offenceTypes = OffenceType.GetAllOffenceTypeByCId(userinfo.CustomerInfoId, dbConnection); 
                }
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                    offenceTypes = OffenceType.GetAllOffenceTypeByLevel5(userinfo.ID, dbConnection); 
                    locations.AddRange(Location.GetAllMainLocationByLevel5(userinfo.ID, dbConnection));
                    locations.AddRange(Location.GetAllSubLocationByLevel5(userinfo.ID, dbConnection)); 
                }
                else if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                {
                    offenceTypes = OffenceType.GetAllOffenceType(dbConnection);
                    locations.AddRange(Location.GetAllLocation(dbConnection));
                    locations.AddRange(Location.GetAllSubLocation(dbConnection)); 
                }
                else
                {
                    offenceTypes = OffenceType.GetAllOffenceTypeBySiteId(userinfo.SiteId, dbConnection);
                    locations.AddRange(Location.GetAllLocationBySiteId(userinfo.SiteId, dbConnection));
                    locations.AddRange(Location.GetAllSubLocationBySiteId(userinfo.SiteId, dbConnection)); 
                }

                var grouped = locations.GroupBy(item => item.ID);
                locations = grouped.Select(grp => grp.OrderBy(item => item.LocationDesc).First()).ToList();
                locations = locations.OrderBy(i => i.LocationDesc).ToList();

                var glocations = new List<Location>();
                var loc = new Location();
                loc.ID = 0;
                loc.LocationDesc = "Select Location";
                loc.LocationDesc_AR = "Select Location";
                loc.CreatedDate = CommonUtility.getDTNow();
                loc.CreatedBy = userinfo.Username;
                glocations.Add(loc);
                glocations.AddRange(locations);

                startlocations.AddRange(glocations.Where(i => i.LocationTypeId != 2).ToList());
  
                ticketCategorySelect.DataTextField = "Name";
                ticketCategorySelect.DataValueField = "Id";
                ticketCategorySelect.DataSource = offenceCategory;
                ticketCategorySelect.DataBind();

                ticketlocationSelect.DataTextField = "LOCATIONDESC";
                ticketlocationSelect.DataValueField = "Id";
                ticketlocationSelect.DataSource = startlocations;
                ticketlocationSelect.DataBind();

                typeSelect.DataTextField = "OffenceTypeDesc";
                typeSelect.DataValueField = "OffenceTypeId";
                typeSelect.DataSource = offenceTypes;
                typeSelect.DataBind();
                typeEditSelect.DataTextField = "OffenceTypeDesc";
                typeEditSelect.DataValueField = "OffenceTypeId";
                typeEditSelect.DataSource = offenceTypes;
                typeEditSelect.DataBind();
                  
                offenceCategorySelect.DataTextField = "Name";
                offenceCategorySelect.DataValueField = "Id";
                offenceCategorySelect.DataSource = offenceCategory;
                offenceCategorySelect.DataBind();

                editoffenceCategorySelect.DataTextField = "Name";
                editoffenceCategorySelect.DataValueField = "Id";
                editoffenceCategorySelect.DataSource = offenceCategory;
                editoffenceCategorySelect.DataBind();

            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Ticketing", "Page_Load", err, dbConnection, userinfo.SiteId);
            }

        }
        [WebMethod]
        public static List<string> getTicketingData(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                   var  allcustomEvents = CustomEvent.GetAllCustomEventByTypeAndCId((int)CustomEvent.EventTypes.DriverOffence, 1, dbConnection);
                    allcustomEvents = allcustomEvents.Where(i => i.UserName == userinfo.Username).ToList();
                foreach (var item in allcustomEvents)
                {

                    var gTicket = DriverOffence.GetDriverOffenceById(item.Identifier, dbConnection);
                    if (gTicket != null)
                    {
                        var displayDate = item.RecevieTime.Value.AddHours(userinfo.TimeZone);
                        if (gTicket.TicketStatus == (int)CustomEvent.IncidentActionStatus.Engage)
                        {
                            item.StatusName = "Inprogress";
                            displayDate = gTicket.StartDate.AddHours(userinfo.TimeZone); 
                        }
                        else if (gTicket.TicketStatus == (int)CustomEvent.IncidentActionStatus.Complete)
                        {
                            item.StatusName = "Completed";
                            displayDate = gTicket.EndDate.AddHours(userinfo.TimeZone); 
                        }
                        var returnstring = "<tr role='row' class='odd'><td class='sorting_1'>" + item.CustomerIncidentId + "</td><td>" + item.StatusName + "</td><td>" + gTicket.OffenceCategory + "</td><td>" + item.CustomerUName + "</td><td>" + displayDate.ToString() + "</td></tr>";//<td><a href='#' data-target='#ticketingViewCard'  data-toggle='modal' onclick='rowchoice(&apos;" + item.EventId + "&apos;) '><i class='fa fa-eye mr-1x'></i>View</a></td>
                        listy.Add(returnstring);
                    }
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Ticketing", "getTicketingData", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static List<string> getTableDataTicketResolved(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }
                var allcustomEvents = new List<CustomEvent>();
                if (userinfo.RoleId == (int)Role.Manager)
                {
                    var pclist = new List<string>();
                    allcustomEvents = CustomEvent.GetAllCustomEventByDateAndManagerIdHandled(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(365, 0, 0, 0)), userinfo.ID, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.Admin)
                {
                    var managerusers = DirectorManager.GetAllManagersByDirectorId(userinfo.ID, dbConnection);
                    allcustomEvents.AddRange(CustomEvent.GetAllCustomEventByDateAndDirectorIdHandled(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(365, 0, 0, 0)), userinfo.ID, dbConnection));
                    foreach (var manguser in managerusers)
                    {
                        allcustomEvents.AddRange(CustomEvent.GetAllCustomEventByDateAndManagerIdHandled(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(365, 0, 0, 0)), manguser, dbConnection));
                    }
                }
                else if (userinfo.RoleId == (int)Role.Director)
                {
                    allcustomEvents = CustomEvent.GetAllCustomEventByDateHandledAndSiteId(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(365, 0, 0, 0)), userinfo.SiteId, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                {
                    allcustomEvents = CustomEvent.GetAllCustomEventByDateHandledAndCId(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(365, 0, 0, 0)), userinfo.CustomerInfoId, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.CustomerUser)
                {
                    allcustomEvents = CustomEvent.GetAllCustomEventByDateHandledAndCId(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(365, 0, 0, 0)), userinfo.CustomerInfoId, dbConnection);
                    allcustomEvents = allcustomEvents.Where(i => i.UserName == userinfo.Username).ToList();
                }
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                    allcustomEvents.AddRange(CustomEvent.GetAllCustomEventByDateHandledAndLevel5(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(365, 0, 0, 0)), userinfo.ID, dbConnection));

                    var glist = CustomEvent.GetAllCustomEventByDateHandledAndLevel5(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(365, 0, 0, 0)), userinfo.ID, dbConnection);
                    allcustomEvents.AddRange(glist.Where(i => i.HandledBy == userinfo.Username).ToList());
                }
                else if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                {
                    allcustomEvents = CustomEvent.GetAllCustomEventByDateHandled(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(365, 0, 0, 0)), dbConnection);
                }
                allcustomEvents = allcustomEvents.Where(i => i.EventType == CustomEvent.EventTypes.DriverOffence && i.Handled).ToList();
                var imageclass = string.Empty;
                var grouped = allcustomEvents.GroupBy(item => item.EventId);
                allcustomEvents = grouped.Select(grp => grp.OrderBy(item => item.EventId).First()).ToList();
                foreach (var item in allcustomEvents)
                {

                    var gTicket = DriverOffence.GetDriverOffenceById(item.Identifier, dbConnection);
                    if (gTicket != null)
                    {
                        item.StatusName = "Resolved";
                        var returnstring = "<tr role='row' class='odd'><td class='sorting_1'>" + item.CustomerIncidentId + "</td><td>" + item.StatusName + "</td><td>" + gTicket.OffenceCategory + "</td><td>" + item.CustomerUName + "</td><td>" + item.RecevieTime.Value.AddHours(userinfo.TimeZone).ToString() + "</td><td><a href='#' data-target='#ticketingViewCard'  data-toggle='modal' onclick='rowchoice(&apos;" + item.EventId + "&apos;) '><i class='fa fa-eye mr-1x'></i>View</a></td></tr>";
                        listy.Add(returnstring);
                    }
                }
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("Incident", "getTableDataTicketResolved", er, dbConnection, userinfo.SiteId);
            }
            //<tr role="row" class="odd clickable-row clickable-row-links"><td><span class="circle-point-container"><span class="circle-point circle-point-orange"></span></span></td><td class="sorting_1">Gecko</td><td>Firefox 1.0</td><td>Win 98+ / OSX.2+</td><td>'++'</td><td><a href="#"><i class="fa fa-eye mr-1x"></i>View</a></td></tr>
            return listy;
        }

        [WebMethod]
        public static List<string> getOffenceData(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var alldata = new List<Offence>();

                if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                {
                    alldata = Offence.GetAllOffenceByCId(userinfo.CustomerInfoId, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                    alldata = Offence.GetAllOffenceByLevel5(userinfo.ID, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                {
                    alldata = Offence.GetAllOffence(dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.CustomerUser)
                {

                }
                else
                {
                    alldata = Offence.GetAllOffenceBySiteId(userinfo.SiteId, dbConnection);
                }

                foreach (var item in alldata)
                {
                    var action = "<a href='#' data-target='#editOffenceModal'  data-toggle='modal' onclick='rowchoiceOffenceType(&apos;" + item.ID + "&apos;,&apos;" + item.OffenceDesc + "&apos;,&apos;" + item.OffenceDesc_AR + "&apos;,&apos;" + item.OffenceTypeID + "&apos;,&apos;" + item.OffenceValue + "&apos;)'><i class='fa fa-pencil mr-1x'></i>Edit</a><a href='#' data-target='#deleteOffenceTypeModal'  data-toggle='modal' onclick='rowchoiceOffenceType(&apos;" + item.ID + "&apos;,&apos;" + item.OffenceDesc + "&apos;,&apos;" + item.OffenceDesc_AR + "&apos;,&apos;" + item.OffenceTypeID + "&apos;,&apos;" + item.OffenceValue + "&apos;)'><i class='fa fa-trash mr-1x'></i>Delete</a>";

                    if (userinfo.RoleId != (int)Role.SuperAdmin && userinfo.RoleId != (int)Role.CustomerSuperadmin)
                    {
                        if (userinfo.Username != item.CreatedBy)
                            action = "";
                    }

                    var returnstring = "<tr role='row' class='odd'><td>" + item.OffenceDesc + "</td><td>" + item.OffenceName + "</td><td>" + item.OffenceValue + "</td><td>" + action + "</td></tr>";
                    listy.Add(returnstring);
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Ticketing", "getOffenceData", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getOffenceTypeData(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var alldata = new List<OffenceType>();

                if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                {
                    alldata = OffenceType.GetAllOffenceTypeByCId(userinfo.CustomerInfoId, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                    alldata = OffenceType.GetAllOffenceTypeByLevel5(userinfo.ID, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                {
                    alldata = OffenceType.GetAllOffenceType(dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.CustomerUser)
                {

                }
                else
                {
                    alldata = OffenceType.GetAllOffenceTypeBySiteId(userinfo.SiteId, dbConnection);
                }


                foreach (var item in alldata)
                {
                    var action = "<a href='#' data-target='#EditOffenceCategoryModal'  data-toggle='modal' onclick='rowchoiceOffenceCategory(&apos;" + item.OffenceTypeId + "&apos;,&apos;" + item.OffenceTypeDesc + "&apos;,&apos;" + item.OffenceTypeDesc_AR + "&apos;,&apos;" + item.OffenceCategoryId + "&apos;)'><i class='fa fa-pencil mr-1x'></i>Edit</a><a href='#' data-target='#deleteOffenceCatModal'  data-toggle='modal' onclick='rowchoiceOffenceCategory(&apos;" + item.OffenceTypeId + "&apos;,&apos;" + item.OffenceTypeDesc + "&apos;,&apos;" + item.OffenceTypeDesc_AR + "&apos;,&apos;" + item.OffenceCategoryId + "&apos;)'><i class='fa fa-trash mr-1x'></i>Delete</a>";

                    if (userinfo.RoleId != (int)Role.SuperAdmin && userinfo.RoleId != (int)Role.CustomerSuperadmin)
                    {
                        if (userinfo.Username != item.CreatedBy)
                            action = "";
                    }

                    var returnstring = "<tr role='row' class='odd'><td>" + item.OffenceTypeDesc + "</td><td>" + item.OffenceTypeDesc_AR + "</td><td>" + item.OffenceCategory + "</td><td>" + action + "</td></tr>";
                    listy.Add(returnstring);
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Ticketing", "getOffenceTypeData", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getVehicleMakeData(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var alldata = new List<VehicleMake>();

                if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                {
                    alldata = VehicleMake.GetAllVehicleMakeByCId(userinfo.CustomerInfoId, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                    alldata = VehicleMake.GetAllVehicleMakeByLevel5(userinfo.ID, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                {
                    alldata = VehicleMake.GetAllVehicleMake(dbConnection);
                }
                else
                {
                    alldata = VehicleMake.GetAllVehicleMakeBySiteId(userinfo.SiteId, dbConnection);
                }

                foreach (var item in alldata)
                {
                    var action = "<a href='#' data-target='#editVehicleMakeModal'  data-toggle='modal' onclick='rowchoiceVehicleMake(&apos;" + item.ID + "&apos;,&apos;" + item.VehicleMakeDesc + "&apos;,&apos;" + item.VehicleMakeDesc_AR + "&apos;)'><i class='fa fa-pencil mr-1x'></i>Edit</a><a href='#' data-target='#deleteVehMakeModal'  data-toggle='modal' onclick='rowchoiceVehicleMake(&apos;" + item.ID + "&apos;,&apos;" + item.VehicleMakeDesc + "&apos;,&apos;" + item.VehicleMakeDesc_AR + "&apos;)'><i class='fa fa-trash mr-1x'></i>Delete</a>";

                    if (userinfo.RoleId != (int)Role.SuperAdmin && userinfo.RoleId != (int)Role.CustomerSuperadmin)
                    {
                        if (userinfo.Username != item.CreatedBy)
                            action = "";
                    }

                    var returnstring = "<tr role='row' class='odd'><td>" + item.VehicleMakeDesc + "</td><td>" + item.VehicleMakeDesc_AR + "</td><td>" + action + "</td></tr>";
                    listy.Add(returnstring);
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Ticketing", "getVehicleMakeData", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getVehicleColorData(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var alldata = new List<VehicleColor>();

                if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                {
                    alldata = VehicleColor.GetAllVehicleColorByCId(userinfo.CustomerInfoId, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                    alldata = VehicleColor.GetAllVehicleColorByLevel5(userinfo.ID, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                {
                    alldata = VehicleColor.GetAllVehicleColor(dbConnection);
                }
                else
                {
                    alldata = VehicleColor.GetAllVehicleColorBySiteId(userinfo.SiteId, dbConnection);
                }

                foreach (var item in alldata)
                {
                    var action = "<a href='#' data-target='#editVehicleColorModal'  data-toggle='modal' onclick='rowchoiceVehicleColor(&apos;" + item.ColorCode + "&apos;,&apos;" + item.ColorDesc + "&apos;,&apos;" + item.ColorDesc_AR + "&apos;)'><i class='fa fa-pencil mr-1x'></i>Edit</a><a href='#' data-target='#deleteVehColorModal'  data-toggle='modal' onclick='rowchoiceVehicleColor(&apos;" + item.ColorCode + "&apos;,&apos;" + item.ColorDesc + "&apos;,&apos;" + item.ColorDesc_AR + "&apos;)'><i class='fa fa-trash mr-1x'></i>Delete</a>";

                    if (userinfo.RoleId != (int)Role.SuperAdmin && userinfo.RoleId != (int)Role.CustomerSuperadmin)
                    {
                        if (userinfo.Username != item.CreatedBy)
                            action = "";
                    }

                    var returnstring = "<tr role='row' class='odd'><td>" + item.ColorDesc + "</td><td>" + item.ColorDesc_AR + "</td><td>" + action + "</td></tr>";
                    listy.Add(returnstring);
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Ticketing", "getVehicleColorData", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getPlateCodeData(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var alldata = new List<PlateCode>();

                if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                {
                    alldata = PlateCode.GetAllPlateCodeByCId(userinfo.CustomerInfoId, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                    alldata = PlateCode.GetAllPlateCodeByLevel5(userinfo.ID, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                {
                    alldata = PlateCode.GetAllPlateCode(dbConnection);
                }
                else
                {
                    alldata = PlateCode.GetAllPlateCodeBySiteId(userinfo.SiteId, dbConnection);
                }


                foreach (var item in alldata)
                {
                    var action = "<a href='#' data-target='#editPlateCodeModal'  data-toggle='modal' onclick='rowchoicePlateCode(&apos;" + item.ID + "&apos;,&apos;" + item.Description + "&apos;,&apos;" + item.Description_AR + "&apos;,&apos;" + item.PlateSource + "&apos;)'><i class='fa fa-pencil mr-1x'></i>Edit</a><a href='#' data-target='#deletePlateCode'  data-toggle='modal' onclick='rowchoicePlateCode(&apos;" + item.ID + "&apos;,&apos;" + item.Description + "&apos;,&apos;" + item.Description_AR + "&apos;)'><i class='fa fa-trash mr-1x'></i>Delete</a>";

                    if (userinfo.RoleId != (int)Role.SuperAdmin && userinfo.RoleId != (int)Role.CustomerSuperadmin)
                    {
                        if (userinfo.Username != item.CreatedBy)
                            action = "";
                    }

                    var returnstring = "<tr role='row' class='odd'><td>" + item.Description + "</td><td>" + item.Description_AR + "</td><td>" + item.PlateSource + "</td><td>" + action + "</td></tr>";
                    listy.Add(returnstring);
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Ticketing", "getPlateCodeData", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getPlateSourceData(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var alldata = new List<PlateSource>();

                if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                {
                    alldata = PlateSource.GetAllPlateSourceByCId(userinfo.CustomerInfoId, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                    alldata = PlateSource.GetAllPlateSourceByLevel5(userinfo.ID, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                {
                    alldata = PlateSource.GetAllPlateSource(dbConnection);
                }
                else
                {
                    alldata = PlateSource.GetAllPlateSourceBySiteId(userinfo.SiteId, dbConnection);
                }

                foreach (var item in alldata)
                {

                    var action = "<a href='#' data-target='#editPlateSourceModal'  data-toggle='modal' onclick='rowchoicePlateSource(&apos;" + item.ID + "&apos;,&apos;" + item.Description + "&apos;,&apos;" + item.Description_AR + "&apos;)'><i class='fa fa-pencil mr-1x'></i>Edit</a><a href='#' data-target='#deletePlateSouce'  data-toggle='modal' onclick='rowchoicePlateSource(&apos;" + item.ID + "&apos;,&apos;" + item.Description + "&apos;,&apos;" + item.Description_AR + "&apos;)'><i class='fa fa-trash mr-1x'></i>Delete</a>";

                    if (userinfo.RoleId != (int)Role.SuperAdmin && userinfo.RoleId != (int)Role.CustomerSuperadmin)
                    {
                        if (userinfo.Username != item.CreatedBy)
                            action = "";
                    }

                    var returnstring = "<tr role='row' class='odd'><td>" + item.Description + "</td><td>" + item.Description_AR + "</td><td>" + action + "</td></tr>";
                    listy.Add(returnstring);
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Ticketing", "getPlateSourceData", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static List<string> getOffenceCategoryData(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }


                var alldata = new List<OffenceCategory>();

                if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                {
                    alldata = OffenceCategory.GetAllOffenceCategoryByCId(userinfo.CustomerInfoId, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                    alldata = OffenceCategory.GetAllOffenceCategoryByLevel5(userinfo.ID, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                {
                    alldata = OffenceCategory.GetAllOffenceCategory(dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.CustomerUser)
                {

                }
                else
                {
                    alldata = OffenceCategory.GetAllOffenceCategoryBySiteId(userinfo.SiteId, dbConnection);
                }

                foreach (var item in alldata)
                {
                    var action = "<a href='#' data-target='#EditOffenceCategoryModal2'  data-toggle='modal' onclick='rowchoiceOffenceCategory2(&apos;" + item.Id + "&apos;,&apos;" + item.Name + "&apos;,&apos;" + item.Description + "&apos;)'><i class='fa fa-pencil mr-1x'></i>Edit</a><a href='#' data-target='#deleteOffenceCatModal2'  data-toggle='modal' onclick='rowchoiceOffenceCategory2(&apos;" + item.Id + "&apos;,&apos;" + item.Name + "&apos;,&apos;" + item.Description + "&apos;)'><i class='fa fa-trash mr-1x'></i>Delete</a>";

                    if (userinfo.RoleId != (int)Role.SuperAdmin && userinfo.RoleId != (int)Role.CustomerSuperadmin)
                    {
                        if (userinfo.Username != item.CreatedBy)
                            action = "";
                    }

                    var returnstring = "<tr role='row' class='odd'><td>" + item.Name + "</td><td>" + item.Description + "</td><td>" + action + "</td></tr>";
                    listy.Add(returnstring);
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Ticketing", "getOffenceCategoryData", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static List<string> getTableRowDataTicket(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var customData = CustomEvent.GetCustomEventById(id, dbConnection);

                if (customData != null)
                {
                    var offenceinfo = DriverOffence.GetDriverOffenceById(customData.Identifier, dbConnection);
                    if (offenceinfo != null)
                    {
                        listy.Add(offenceinfo.PlateNumber);
                        listy.Add(offenceinfo.PlateSource);
                        listy.Add(offenceinfo.PlateCode);
                        listy.Add(offenceinfo.VehicleMake);
                        listy.Add(offenceinfo.VehicleColor);
                        var ggUser = Users.GetUserByName(offenceinfo.UserName, dbConnection);
                        listy.Add(ggUser.CustomerUName);
                        var geolocation = ReverseGeocode.RetrieveFormatedAddress(customData.Latitude.ToString(), customData.Longtitude.ToString(), getClientLic);
                        listy.Add(geolocation);
                        listy.Add(offenceinfo.CreatedDate.AddHours(userinfo.TimeZone).ToString());
                        listy.Add(offenceinfo.OffenceCategory);
                        listy.Add(offenceinfo.OffenceType);
                        if (userinfo.RoleId == (int)Role.CustomerUser)
                        {
                            listy.Add("5");
                        }
                        else
                        {
                            if (customData.Handled)
                            {
                                listy.Add("5");
                            }
                            else
                                listy.Add(offenceinfo.TicketStatus.ToString());
                        }
                        listy.Add(offenceinfo.Comments);
                        if (!string.IsNullOrEmpty(offenceinfo.CusName))
                            listy.Add(offenceinfo.CusName);
                        else
                        {
                            listy.Add("N/A");
                        }
                        if (!string.IsNullOrEmpty(offenceinfo.ContractName))
                            listy.Add(offenceinfo.ContractName);
                        else
                        {
                            listy.Add("N/A");
                        }
                        listy.Add(offenceinfo.ContractId.ToString());
                        listy.Add(offenceinfo.CusId.ToString());

                        var usertask = UserTask.GetAllTasksByIncidentId(id, dbConnection);
                        if (usertask.Count > 0)
                        {
                            usertask = usertask.Where(i => i.Status != (int)TaskStatus.Completed && i.Status != (int)TaskStatus.Accepted).ToList();
                            if (usertask.Count > 0)
                            {
                                listy.Add("0");
                            }
                            else
                            {
                                listy.Add("1");
                            }
                        }
                        else
                        {
                            listy.Add("1");
                        }
                    }
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Ticketing", "getTableRowData", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getChecklistData(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var customData = CustomEvent.GetCustomEventById(id, dbConnection);
                if (customData != null)
                {
                    var offenceinfo = DriverOffence.GetDriverOffenceById(customData.Identifier, dbConnection);
                    if (!string.IsNullOrEmpty(offenceinfo.Offence1))
                        listy.Add(offenceinfo.Offence1);
                    if (!string.IsNullOrEmpty(offenceinfo.Offence2))
                        listy.Add(offenceinfo.Offence2);
                    if (!string.IsNullOrEmpty(offenceinfo.Offence3))
                        listy.Add(offenceinfo.Offence3);
                    if (!string.IsNullOrEmpty(offenceinfo.Offence4))
                        listy.Add(offenceinfo.Offence4);
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Ticketing", "getChecklistData", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getChecklistDataTicket(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var customData = CustomEvent.GetCustomEventById(id, dbConnection);
                if (customData != null)
                {
                    var offenceinfo = DriverOffence.GetDriverOffenceById(customData.Identifier, dbConnection);
                    if (!string.IsNullOrEmpty(offenceinfo.Offence1))
                        listy.Add(offenceinfo.Offence1);
                    if (!string.IsNullOrEmpty(offenceinfo.Offence2))
                        listy.Add(offenceinfo.Offence2);
                    if (!string.IsNullOrEmpty(offenceinfo.Offence3))
                        listy.Add(offenceinfo.Offence3);
                    if (!string.IsNullOrEmpty(offenceinfo.Offence4))
                        listy.Add(offenceinfo.Offence4);
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Ticketing", "getChecklistDataTicket", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static string getIncidentLocationData(int id, string uname)
        {
            var json = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            json += "[";
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                if (getClientLic != null)
                {
                    if (getClientLic.isLocation)
                    {
                        var cusEv = CustomEvent.GetCustomEventById(id, dbConnection);
                        if (cusEv != null)
                        {
                            var item = DriverOffence.GetDriverOffenceById(cusEv.Identifier, dbConnection);

                            json += "{ \"Username\" : \"Ticket\",\"Id\" : \"" + item.Identifier.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                            json = json.Substring(0, json.Length - 1);

                        }
                    }
                }
                json += "]";
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Ticketing", "getIncidentLocationData", err, dbConnection, userinfo.SiteId);
            }
            return json;
        }
        [WebMethod]
        public static string getAttachmentDataIconsTicket(int id, string uname)
        {
            var listy = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                var cusEv = CustomEvent.GetCustomEventById(id, dbConnection);
                var attachments = DriverOffence.GetDriverOffenceById(cusEv.Identifier, dbConnection);
                var settings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                var i = 1;
                //var imgstring = settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/OffenceImagePath.jpeg";
                var retstring2 = string.Empty;
                if (!string.IsNullOrEmpty(attachments.OffenceImagePath1))
                {
                    retstring2 = "<img src='" + attachments.OffenceImagePath1 + "' data-toggle='tab' data-target='#image-1-tab'/>";
                    listy += retstring2;
                    i++;
                }
                if (!string.IsNullOrEmpty(attachments.Image1Path))
                {
                    var imgstring = settings.MIMSMobileAddress + "/Images/videoicon.png";
                    retstring2 = "<img src='" + imgstring + "' data-toggle='tab' data-target='#video-0-tab'/>";
                    listy += retstring2;

                    imgstring = settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/ImagePath1.jpeg";
                    retstring2 = "<img src='" + imgstring + "' data-toggle='tab' data-target='#image-2-tab'/>";
                    listy += retstring2;

                    imgstring = settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/ImagePath2.jpeg";
                    retstring2 = "<img src='" + imgstring + "' data-toggle='tab' data-target='#image-3-tab'/>";
                    listy += retstring2;

                    imgstring = settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/ImagePath3.jpeg";
                    retstring2 = "<img src='" + imgstring + "' data-toggle='tab' data-target='#image-4-tab'/>";
                    listy += retstring2;

                    imgstring = settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/ImagePath4.jpeg";
                    retstring2 = "<img src='" + imgstring + "' data-toggle='tab' data-target='#image-5-tab'/>";
                    listy += retstring2;

                    imgstring = settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/ImagePath5.jpeg";
                    retstring2 = "<img src='" + imgstring + "' data-toggle='tab' data-target='#image-6-tab'/>";
                    listy += retstring2;

                    imgstring = settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/ImagePath6.jpeg";
                    retstring2 = "<img src='" + imgstring + "' data-toggle='tab' data-target='#image-7-tab'/>";
                    listy += retstring2;

                    i = 7;
                }
                var evattachments = MobileHotEventAttachment.GetMobileHotEventAttachmentByEventId(cusEv.EventId, dbConnection);
                if (evattachments.Count > 0)
                {
                    foreach (var item in evattachments)
                    {
                        var imgstring = string.Empty;
                        if (!string.IsNullOrEmpty(item.AttachmentPath))
                        {
                            var requiredString = item.AttachmentPath;//.Substring(index, (item.AttachmentPath.Length - index)); 
                            if (VideoExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))
                            {
                                var retstring = "<img src='../images/VLCMediaPlayer1.png' data-toggle='tab' onclick='play(" + i + ")' data-target='#video-" + i + "-tab'/>";
                                listy += retstring;
                                i++;
                            }
                            else if (CommonUtility.FileExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))//(System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".PDF")
                            {

                            }
                            else
                            {
                                var retstring = "<img src='" + requiredString + "' data-toggle='tab' data-target='#image-" + i + "-tab'/>";
                                listy += retstring;
                                i++;
                            }

                        }
                        else
                        {
                            if (item.Attachment != null)
                            {
                                var retstring = "<img src='" + item.AttachmentPath + "' data-toggle='tab' data-target='#image-" + i + "-tab'/>";
                                listy += retstring;
                                i++;
                            }
                        }
                    }
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Ticketing", "getAttachmentDataIcons", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static string getAttachmentDataTabTicket(int id, string uname)
        {
            var listy = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }
                var i = 1;
                var iTab = 1;
                var cusEv = CustomEvent.GetCustomEventById(id, dbConnection);
                listy += "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#ticketlocation-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-map-marker fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Location</p></div></div></div>";
                var attachments = DriverOffence.GetDriverOffenceById(cusEv.Identifier, dbConnection);
                if (!string.IsNullOrEmpty(attachments.OffenceImagePath1))
                {
                    listy += "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-1-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Image</p></div></div></div>";
                    i++;
                    iTab++;
                }
                if (!string.IsNullOrEmpty(attachments.Image1Path))
                {
                    listy += "<div class='row static-height-with-border clickable-row' data-toggle='tab' onclick='play(1)' data-target='#video-0-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-play-circle-o fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Video</p></div></div></div>";
                    listy += "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-2-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Snap Image 1</p></div></div></div>";
                    listy += "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-3-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Snap Image 2</p></div></div></div>";
                    listy += "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-4-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Snap Image 3</p></div></div></div>";
                    listy += "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-5-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Snap Image 4</p></div></div></div>";
                    listy += "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-6-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Snap Image 5</p></div></div></div>";
                    listy += "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-7-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Snap Image 6</p></div></div></div>";
                    i = 7;
                    iTab = 7;
                }

                var evattachments = MobileHotEventAttachment.GetMobileHotEventAttachmentByEventId(cusEv.EventId, dbConnection);

                if (evattachments.Count > 0)
                {
                    foreach (var item in evattachments)
                    {
                        var imgstring = string.Empty;
                        if (!string.IsNullOrEmpty(item.AttachmentPath))
                        {
                            // int index = item.AttachmentPath.IndexOf("HotEvents");
                            var requiredString = item.AttachmentPath;//.Substring(index, (item.AttachmentPath.Length - index));
                            // requiredString = requiredString.Replace("\\", "/");

                            if (VideoExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))
                            {
                                var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' onclick='play(" + iTab + ")' data-target='#video-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-play-circle-o fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceTicket(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                listy += retstring;
                                iTab++;
                            }
                            else if (CommonUtility.FileExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))//(System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".PDF")
                            {
                                //var mimssettings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                                //imgstring = String.Format(mimssettings.MIMSMobileAddress + "/Uploads/HotEvents/{0}", System.IO.Path.GetFileName(requiredString));

                                var attachmentName = CommonUtility.getAttachmentDisplayName(item.AttachmentName, i);
                                if (!string.IsNullOrEmpty(attachmentName))
                                    attachmentName = attachmentName.Split('-')[1];
                                var retstringExtra = "<div class='row static-height-with-border clickable-row' ><div class='col-md-12'><div onclick='window.open(&apos;" + requiredString + "&apos;);' class='inline-block mr-2x'><i class='fa fa-file-pdf-o fa-2x gray-bg red-color'></i></div><div class='inline-block' onclick='window.open(&apos;" + requiredString + "&apos;);'><p>" + attachmentName + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceTicket(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                listy += retstringExtra;
                            }
                            else
                            {
                                var attachmentName = CommonUtility.getAttachmentDisplayName(item.AttachmentName, i);
                                if (!string.IsNullOrEmpty(attachmentName))
                                    attachmentName = attachmentName.Split('-')[1];
                                var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>" + attachmentName + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceTicket(&apos;" + item.Id + "&apos;)'></i></div></div></div>";

                                //var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                listy += retstring;
                                iTab++;
                            }
                            i++;
                        }
                        else
                        {
                            if (item.Attachment != null)
                            {
                                var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceTicket(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                listy += retstring;
                                iTab++;
                                i++;
                            }
                        }
                    }
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Ticketing", "getAttachmentDataTab", err, dbConnection, userinfo.SiteId);
            }

            return listy;
        }
        [WebMethod]
        public static List<string> getAttachmentDataTicket(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var cusEv = CustomEvent.GetCustomEventById(id, dbConnection);
                var attachments = DriverOffence.GetDriverOffenceById(cusEv.Identifier, dbConnection);
                var settings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);

                var retstring = "<video id='Video0' width='100%' height='378px' autoplay='autoplay' type='video/ogg; codecs=theo' muted controls ><source src='" + settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/" + attachments.Identifier.ToString() + ".ogg' /></video>";
                //var retstring = "<video id='Video0' width='100%' height='378px' src='" + settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/" + attachments.Identifier.ToString() + ".ogg' type='video/ogg; codecs=theo' autoplay='autoplay' />";
                listy.Add(retstring);
                var i = 1;
                //var imgstring = settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/OffenceImagePath.jpeg";
                var retstring2 = string.Empty;
                if (!string.IsNullOrEmpty(attachments.OffenceImagePath1))
                {
                    retstring2 = "<img onclick='rotateMe(this);' src='" + attachments.OffenceImagePath1 + "' class='resized-filled-image' onload='loadMe(this);'/>";
                    listy.Add(retstring2);
                    i++;
                }
                if (!string.IsNullOrEmpty(attachments.Image1Path))
                {
                    var imgstring = settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/ImagePath1.jpeg";
                    retstring2 = "<img onclick='rotateMe(this);' src='" + imgstring + "' class='resized-filled-image' onload='loadMe(this);'/>";
                    listy.Add(retstring2);

                    imgstring = settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/ImagePath2.jpeg";
                    retstring2 = "<img onclick='rotateMe(this);' src='" + imgstring + "' class='resized-filled-image' onload='loadMe(this);'/>";
                    listy.Add(retstring2);

                    imgstring = settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/ImagePath3.jpeg";
                    retstring2 = "<img onclick='rotateMe(this);' src='" + imgstring + "' class='resized-filled-image' onload='loadMe(this);'/>";
                    listy.Add(retstring2);

                    imgstring = settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/ImagePath4.jpeg";
                    retstring2 = "<img onclick='rotateMe(this);' src='" + imgstring + "' class='resized-filled-image' onload='loadMe(this);'/>";
                    listy.Add(retstring2);

                    imgstring = settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/ImagePath5.jpeg";
                    retstring2 = "<img onclick='rotateMe(this);' src='" + imgstring + "' class='resized-filled-image' onload='loadMe(this);'/>";
                    listy.Add(retstring2);

                    imgstring = settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/ImagePath6.jpeg";
                    retstring2 = "<img onclick='rotateMe(this);' src='" + imgstring + "' class='resized-filled-image' onload='loadMe(this);'/>";
                    listy.Add(retstring2);

                    i = 7;
                }
                var evattachments = MobileHotEventAttachment.GetMobileHotEventAttachmentByEventId(cusEv.EventId, dbConnection);

                if (evattachments.Count > 0)
                {

                    foreach (var item in evattachments)
                    {
                        var imgstring = string.Empty;
                        if (!string.IsNullOrEmpty(item.AttachmentPath))
                        {
                            //int index = item.AttachmentPath.IndexOf("HotEvents");
                            var requiredString = item.AttachmentPath;//.Substring(index, (item.AttachmentPath.Length - index));
                            //requiredString = requiredString.Replace("\\", "/");
                            if (VideoExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))
                            {
                                //var retstring = "<iframe width='100%' height='378' frameborder='0' class='video-presenter' allowfullscreen></iframe><div class='video-link'>" + mimssettings.MIMSMobileAddress + "/Uploads/" + requiredString + "</div>";//"<img src='images/VLCMediaPlayer1.png' data-toggle='tab' data-target='#image-1-tab'/>";
                                retstring = "<video id='Video" + i + "' width='100%' height='378px' muted controls ><source src='" + requiredString + "' /></video>";
                                listy.Add(retstring);
                                i++;
                            }
                            else if (CommonUtility.FileExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))//(System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".PDF" || System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".DOCX")
                            {

                            }
                            else
                            {
                                //imgstring = mimssettings.MIMSMobileAddress + "/Uploads/" + requiredString;
                                retstring = "<img onclick='rotateMe(this);' src='" + requiredString + "' class='resized-filled-image' onload='loadMe(this);'/>";
                                listy.Add(retstring);
                                i++;
                            }

                        }
                        else
                        {
                            if (item.Attachment != null)
                            {
                                var base64 = Convert.ToBase64String(item.Attachment);
                                imgstring = String.Format("data:image/png;base64,{0}", base64);
                                retstring = "<img onclick='rotateMe(this);' src='" + imgstring + "' class='resized-filled-image' onload='loadMe(this);'/>";
                                listy.Add(retstring);
                                i++;
                            }
                        }
                    }
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Ticketing", "getAttachmentData", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static string editOffenceType(int id, string name, string nameAR, string type, string tvalue, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    var off = Offence.GetOffenceById(id, dbConnection);
                    if (off != null)
                    {
                        var oldValue = string.Empty;
                        var newValue = string.Empty;
                        if (off.OffenceDesc != name)
                        {
                            oldValue = off.OffenceDesc;
                            newValue = name;
                            off.OffenceDesc = name;
                            SystemLogger.SaveSystemLog(dbConnectionAudit, "Ticketing", newValue, oldValue, userinfo, "OFFENCE TYPE OffenceDesc-" + id);
                        }
                        if (off.OffenceDesc_AR != nameAR)
                        {
                            oldValue = off.OffenceDesc_AR;
                            newValue = nameAR;
                            off.OffenceDesc_AR = nameAR;
                            SystemLogger.SaveSystemLog(dbConnectionAudit, "Ticketing", newValue, oldValue, userinfo, "OFFENCE TYPE OffenceDesc_AR-" + id);
                        }
                        if (off.OffenceTypeID != Convert.ToInt32(type))
                        {
                            oldValue = off.OffenceTypeID.ToString();
                            newValue = type;
                            off.OffenceTypeID = Convert.ToInt32(type);
                            SystemLogger.SaveSystemLog(dbConnectionAudit, "Ticketing", newValue, oldValue, userinfo, "OFFENCE TYPE OffenceDesc_AR-" + id);
                        }
                        if (off.OffenceValue != tvalue)
                        {
                            oldValue = off.OffenceValue;
                            newValue = tvalue;
                            off.OffenceValue = tvalue;
                            SystemLogger.SaveSystemLog(dbConnectionAudit, "Ticketing", newValue, oldValue, userinfo, "OFFENCE TYPE OffenceValue-" + id);
                        }
                        off.UpdatedBy = userinfo.Username;
                        off.UpdatedDate = CommonUtility.getDTNow();
                        Offence.InsertorUpdateOffence(off, dbConnection, dbConnectionAudit, true);
                    }

                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Ticketing", "editOffenceType", err, dbConnection, userinfo.SiteId);
                    return err.Message;
                }
                return "SUCCESS";
            }
        }

        [WebMethod]
        public static string editVehMake(int id, string name, string nameAR, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    var off = VehicleMake.GetVehicleMakeById(id, dbConnection);
                    if (off != null)
                    {

                        var oldValue = string.Empty;
                        var newValue = string.Empty;
                        if (off.VehicleMakeDesc != name)
                        {
                            var getData2 = VehicleMake.GetVehicleMakeByNameAndCIdAndSiteId(name, userinfo.CustomerInfoId, userinfo.SiteId, dbConnection);

                            if (userinfo.RoleId == (int)Role.Director)
                            {
                                if (getData2 == null)
                                {
                                    getData2 = VehicleMake.GetVehicleMakeByNameAndCIdAndSiteId(name, userinfo.CustomerInfoId, 0, dbConnection);
                                }
                            }
                            if (getData2 == null)
                            {
                                oldValue = off.VehicleMakeDesc;
                                newValue = name;
                                off.VehicleMakeDesc = name;
                                SystemLogger.SaveSystemLog(dbConnectionAudit, "Ticketing", newValue, oldValue, userinfo, "VEHICLE MAKE VehicleMakeDesc-" + id);
                            }
                            else
                            {
                                return "Name already exists";
                            }
                        }
                        if (off.VehicleMakeDesc_AR != nameAR)
                        {
                            oldValue = off.VehicleMakeDesc_AR;
                            newValue = nameAR;
                            off.VehicleMakeDesc_AR = nameAR;
                            SystemLogger.SaveSystemLog(dbConnectionAudit, "Ticketing", newValue, oldValue, userinfo, "VEHICLE MAKE VehicleMakeDesc_AR-" + id);
                        }
                        off.UpdatedBy = userinfo.Username;
                        off.UpdatedDate = CommonUtility.getDTNow();
                        VehicleMake.InsertorUpdateVehicleMake(off, dbConnection, dbConnectionAudit, true);
                    }
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Ticketing", "editVehMake", err, dbConnection, userinfo.SiteId);
                    return err.Message;
                }
                return "SUCCESS";
            }
        }
        [WebMethod]
        public static string editVehColor(int id, string name, string nameAR, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    var off = VehicleColor.GetVehicleColorById(id, dbConnection);
                    if (off != null)
                    {
                        var oldValue = string.Empty;
                        var newValue = string.Empty;
                        if (off.ColorDesc != name)
                        {
                            var getData2 = VehicleColor.GetVehicleColorByNameAndCIdAndSiteId(name, userinfo.CustomerInfoId, userinfo.SiteId, dbConnection);

                            if (userinfo.RoleId == (int)Role.Director)
                            {
                                if (getData2 == null)
                                {
                                    getData2 = VehicleColor.GetVehicleColorByNameAndCIdAndSiteId(name, userinfo.CustomerInfoId, 0, dbConnection);
                                }
                            }
                            if (getData2 == null)
                            {
                                oldValue = off.ColorDesc;
                                newValue = name;
                                off.ColorDesc = name;
                                SystemLogger.SaveSystemLog(dbConnectionAudit, "Ticketing", newValue, oldValue, userinfo, "VEHICLE COLOR ColorDesc-" + id);
                            }
                            else
                            {
                                return "Name already exists";
                            }
                        }
                        if (off.ColorDesc_AR != nameAR)
                        {
                            oldValue = off.ColorDesc_AR;
                            newValue = nameAR;
                            off.ColorDesc_AR = nameAR;
                            SystemLogger.SaveSystemLog(dbConnectionAudit, "Ticketing", newValue, oldValue, userinfo, "VEHICLE COLOR ColorDesc_AR-" + id);
                        }
                        off.UpdatedBy = userinfo.Username;
                        off.UpdatedDate = CommonUtility.getDTNow();
                        VehicleColor.InsertorUpdateVehicleColor(off, dbConnection, dbConnectionAudit, true);
                    }
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Ticketing", "editVehColor", err, dbConnection, userinfo.SiteId);
                    return err.Message;
                }
                return "SUCCESS";
            }
        }
        [WebMethod]
        public static string editPlateCode(int id, string name, string nameAR, string type, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                try
                {

                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    var off = PlateCode.GetPlateCodeById(id, dbConnection);
                    {
                        var oldValue = string.Empty;
                        var newValue = string.Empty;
                        if (off.Description != name)
                        {
                            var getData2 = PlateCode.GetPlateCodeByNameAndCIdAndSiteId(name, userinfo.CustomerInfoId, userinfo.SiteId, dbConnection);

                            if (userinfo.RoleId == (int)Role.Director)
                            {
                                if (getData2 == null)
                                {
                                    getData2 = PlateCode.GetPlateCodeByNameAndCIdAndSiteId(name, userinfo.CustomerInfoId, 0, dbConnection);
                                }
                            }
                            if (getData2 == null)
                            {
                                oldValue = off.Description;
                                newValue = name;
                                off.Description = name;
                                SystemLogger.SaveSystemLog(dbConnectionAudit, "Ticketing", newValue, oldValue, userinfo, "PLATE CODE Description-" + id);
                            }
                            else
                            {
                                return "Name already exists";
                            }
                        }
                        if (off.Description_AR != nameAR)
                        {
                            oldValue = off.Description_AR;
                            newValue = nameAR;
                            off.Description_AR = nameAR;
                            SystemLogger.SaveSystemLog(dbConnectionAudit, "Ticketing", newValue, oldValue, userinfo, "PLATE CODE Description_AR-" + id);
                        }
                        if (off.PlateSource != type)
                        {
                            oldValue = off.PlateSource;
                            newValue = type;
                            off.PlateSource = type;
                            SystemLogger.SaveSystemLog(dbConnectionAudit, "Ticketing", newValue, oldValue, userinfo, "PLATE CODE PlateSource-" + id);
                        }
                        off.UpdatedBy = userinfo.Username;
                        off.UpdatedDate = CommonUtility.getDTNow();
                        PlateCode.InsertorUpdatePlateCode(off, dbConnection, dbConnectionAudit, true);
                    }
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Ticketing", "editPlateCode", err, dbConnection, userinfo.SiteId);
                    return err.Message;
                }
            }
            return "SUCCESS";
        }

        [WebMethod]
        public static string editPlateSource(int id, string name, string nameAR, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                try
                {

                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    var off = PlateSource.GetPlateSourceById(id, dbConnection);
                    if (off != null)
                    {
                        var oldValue = string.Empty;
                        var newValue = string.Empty;
                        if (off.Code != name)
                        {
                            var getData2 = PlateSource.GetPlateSourceByNameAndCIdAndSiteId(name, userinfo.CustomerInfoId, userinfo.SiteId, dbConnection);

                            if (userinfo.RoleId == (int)Role.Director)
                            {
                                if (getData2 == null)
                                {
                                    getData2 = PlateSource.GetPlateSourceByNameAndCIdAndSiteId(name, userinfo.CustomerInfoId, 0, dbConnection);
                                }
                            }
                            if (getData2 == null)
                            {
                                oldValue = off.Code;
                                newValue = name;
                                off.Code = name;
                                SystemLogger.SaveSystemLog(dbConnectionAudit, "Ticketing", newValue, oldValue, userinfo, "PLATE SOURCE Code-" + id);
                            }
                            else
                            {
                                return "Name already exists";
                            }
                        }
                        if (off.Description_AR != nameAR)
                        {
                            oldValue = off.Description_AR;
                            newValue = nameAR;
                            off.Description_AR = nameAR;
                            SystemLogger.SaveSystemLog(dbConnectionAudit, "Ticketing", newValue, oldValue, userinfo, "PLATE SOURCE Description_AR-" + id);
                        }
                        if (off.Description != name)
                        {
                            oldValue = off.Description;
                            newValue = name;
                            off.Description = name;
                            SystemLogger.SaveSystemLog(dbConnectionAudit, "Ticketing", newValue, oldValue, userinfo, "PLATE SOURCE Description-" + id);
                        }
                        off.UpdatedBy = userinfo.Username;
                        off.UpdatedDate = CommonUtility.getDTNow();
                        PlateSource.InsertorUpdatePlateSource(off, dbConnection, dbConnectionAudit, true);

                        var plateCs = PlateCode.GetAllPlateCode(dbConnection);
                        if (plateCs != null && plateCs.Count > 0)
                        {
                            plateCs = plateCs.Where(i => i.PlateSource.ToLower() == oldValue.ToLower()).ToList();
                            foreach (var item in plateCs)
                            {
                                item.PlateSource = name;
                                PlateCode.InsertorUpdatePlateCode(item, dbConnection, dbConnectionAudit, true);
                            }
                        }
                    }
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Ticketing", "editPlateSource", err, dbConnection, userinfo.SiteId);
                    return err.Message;
                }
            }
            return "SUCCESS";
        }
        [WebMethod]
        public static string addOffenceType(int id, string name, string nameAR, string type, string tvalue, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    var off = new Offence();
                    off.OffenceDesc = name;
                    off.OffenceDesc_AR = nameAR;
                    off.OffenceTypeID = Convert.ToInt32(type);
                    off.UpdatedBy = userinfo.Username;
                    off.UpdatedDate = CommonUtility.getDTNow();
                    off.CreatedBy = userinfo.Username;
                    off.CreateDate = CommonUtility.getDTNow();
                    off.OffenceValue = tvalue;
                    off.CustomerId = userinfo.CustomerInfoId;
                    off.SiteId = userinfo.SiteId;
                    Offence.InsertorUpdateOffence(off, dbConnection, dbConnectionAudit, true);
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Ticketing", "addOffenceType", err, dbConnection, userinfo.SiteId);
                    return err.Message;
                }
            }
            return "SUCCESS";
        }
        [WebMethod]
        public static string addOffenceCat(int id, string name, int cval, string nameAR, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }


                    var getData = OffenceType.GetOffenceTypeByNameAndCIdAndSiteId(name, userinfo.CustomerInfoId, userinfo.SiteId, dbConnection);

                    if (userinfo.RoleId == (int)Role.Director)
                    {
                        if (getData == null)
                        {
                            getData = OffenceType.GetOffenceTypeByNameAndCIdAndSiteId(name, userinfo.CustomerInfoId, 0, dbConnection);
                        }
                    }
                    if (getData == null)
                    {
                        var otlist = new List<Offence>();
                        if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                        {
                            var getTaskCategorys = OffenceType.GetAllOffenceTypeByCId(userinfo.CustomerInfoId, dbConnection);
                            if (getTaskCategorys.Count > 0)
                            {
                                foreach (var getcat in getTaskCategorys)
                                {
                                    if (getcat.OffenceTypeDesc.ToLower() == name.ToLower())
                                    {
                                        var otlist1 = Offence.GetAllOffenceByOffenceTypeId(getcat.OffenceTypeId, dbConnection);
                                        otlist.AddRange(otlist1);
                                        OffenceType.DeleteOffenceTypeById(getcat.OffenceTypeId, dbConnection);
                                    }
                                }
                            }
                        }

                        var off = new OffenceType();
                        off.OffenceCategoryId = cval;
                        off.OffenceTypeDesc = name;
                        off.OffenceTypeDesc_AR = nameAR;
                        off.UpdatedBy = userinfo.Username;
                        off.UpdatedDate = DateTime.Now;
                        off.CreatedBy = userinfo.Username;
                        off.CreatedDate = DateTime.Now;
                        off.CustomerId = userinfo.CustomerInfoId;
                        off.SiteId = userinfo.SiteId;
                        var rid = OffenceType.InsertorUpdateOffenceType(off, dbConnection, dbConnectionAudit, true);

                        foreach (var item in otlist)
                        {
                            item.OffenceTypeID = rid;
                            item.UpdatedBy = userinfo.Username;
                            item.UpdatedDate = CommonUtility.getDTNow();
                            Offence.InsertorUpdateOffence(item, dbConnection);
                        }
                    }
                    else
                    {
                        return "Name already exists";
                    }
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Ticketing", "addOffenceCat", err, dbConnection, userinfo.SiteId);
                    return err.Message;
                }
            }
            return "SUCCESS";
        }

        [WebMethod]
        public static string addVehMake(int id, string name, string nameAR, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    var getData = VehicleMake.GetVehicleMakeByNameAndCIdAndSiteId(name, userinfo.CustomerInfoId, userinfo.SiteId, dbConnection);

                    if (userinfo.RoleId == (int)Role.Director)
                    {
                        if (getData == null)
                        {
                            getData = VehicleMake.GetVehicleMakeByNameAndCIdAndSiteId(name, userinfo.CustomerInfoId, 0, dbConnection);
                        }
                    }
                    if (getData == null)
                    {

                        if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                        {
                            var getTaskCategorys = VehicleMake.GetAllVehicleMakeByCId(userinfo.CustomerInfoId, dbConnection);
                            if (getTaskCategorys.Count > 0)
                            {
                                foreach (var getcat in getTaskCategorys)
                                {
                                    if (getcat.VehicleMakeDesc.ToLower() == name.ToLower())
                                        VehicleMake.DeleteVehicleMakeById(getcat.ID, dbConnection);
                                }
                            }
                        }

                        var off = new VehicleMake();
                        off.UpdatedBy = userinfo.Username;
                        off.UpdatedDate = CommonUtility.getDTNow();
                        off.CreatedBy = userinfo.Username;
                        off.CreatedDate = CommonUtility.getDTNow();
                        off.VehicleMakeDesc = name;
                        off.VehicleMakeDesc_AR = nameAR;
                        off.CustomerId = userinfo.CustomerInfoId;
                        off.SiteId = userinfo.SiteId;
                        VehicleMake.InsertorUpdateVehicleMake(off, dbConnection, dbConnectionAudit, true);
                    }
                    else
                    {
                        return "Name already exists";
                    }
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Ticketing", "addVehMake", err, dbConnection, userinfo.SiteId);
                    return err.Message;
                }
            }
            return "SUCCESS";
        }
        [WebMethod]
        public static string addVehColor(int id, string name, string nameAR, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    var getData = VehicleColor.GetVehicleColorByNameAndCIdAndSiteId(name, userinfo.CustomerInfoId, userinfo.SiteId, dbConnection);

                    if (userinfo.RoleId == (int)Role.Director)
                    {
                        if (getData == null)
                        {
                            getData = VehicleColor.GetVehicleColorByNameAndCIdAndSiteId(name, userinfo.CustomerInfoId, 0, dbConnection);
                        }
                    }

                    if (getData == null)
                    {

                        if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                        {
                            var getTaskCategorys = VehicleColor.GetAllVehicleColorByCId(userinfo.CustomerInfoId, dbConnection);
                            if (getTaskCategorys.Count > 0)
                            {
                                foreach (var getcat in getTaskCategorys)
                                {
                                    if (getcat.ColorDesc.ToLower() == name.ToLower())
                                        VehicleColor.DeleteVehicleColorByColorCode(getcat.ColorCode, dbConnection);
                                }
                            }
                        }

                        var off = new VehicleColor();
                        off.CreatedBy = userinfo.Username;
                        off.CreatedDate = CommonUtility.getDTNow();
                        off.ColorDesc = name;
                        off.ColorDesc_AR = nameAR;
                        off.UpdatedBy = userinfo.Username;
                        off.UpdatedDate = CommonUtility.getDTNow();
                        off.CustomerId = userinfo.CustomerInfoId;
                        off.SiteId = userinfo.SiteId;
                        VehicleColor.InsertorUpdateVehicleColor(off, dbConnection, dbConnectionAudit, true);
                    }
                    else
                    {
                        return "Name already exists";
                    }
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Ticketing", "addVehColor", err, dbConnection, userinfo.SiteId);
                    return err.Message;
                }
            }
            return "SUCCESS";
        }
        [WebMethod]
        public static string addPlateCode(int id, string name, string nameAR, string type, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                try
                {

                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    var getData = PlateCode.GetPlateCodeByNameAndCIdAndSiteId(name, userinfo.CustomerInfoId, userinfo.SiteId, dbConnection);

                    if (userinfo.RoleId == (int)Role.Director)
                    {
                        if (getData == null)
                        {
                            getData = PlateCode.GetPlateCodeByNameAndCIdAndSiteId(name, userinfo.CustomerInfoId, 0, dbConnection);
                        }
                    }

                    if (getData == null)
                    {
                        if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                        {
                            var getTaskCategorys = PlateCode.GetAllPlateCodeByCId(userinfo.CustomerInfoId, dbConnection);
                            if (getTaskCategorys.Count > 0)
                            {
                                foreach (var getcat in getTaskCategorys)
                                {
                                    if (getcat.Description.ToLower() == name.ToLower())
                                    {
                                        PlateCode.DeletePlateCodeById(getcat.ID, dbConnection);
                                    }
                                }
                            }
                        }

                        var off = new PlateCode();
                        off.CreatedBy = userinfo.Username;
                        off.CreatedDate = CommonUtility.getDTNow();
                        off.PlateSource = type;
                        off.Description = name;
                        off.Description_AR = nameAR;
                        off.UpdatedBy = userinfo.Username;
                        off.UpdatedDate = CommonUtility.getDTNow();
                        off.CustomerId = userinfo.CustomerInfoId;
                        off.SiteId = userinfo.SiteId;
                        var retid = PlateCode.InsertorUpdatePlateCode(off, dbConnection, dbConnectionAudit, true);

                    }
                    else
                    {
                        return "Name already exists";
                    }
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Ticketing", "addPlateCode", err, dbConnection, userinfo.SiteId);
                    return err.Message;
                }
            }
            return "SUCCESS";
        }
        [WebMethod]
        public static string addPlateSource(int id, string name, string nameAR, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }
                    var getData = PlateSource.GetPlateSourceByNameAndCIdAndSiteId(name, userinfo.CustomerInfoId, userinfo.SiteId, dbConnection);

                    if (userinfo.RoleId == (int)Role.Director)
                    {
                        if (getData == null)
                        {
                            getData = PlateSource.GetPlateSourceByNameAndCIdAndSiteId(name, userinfo.CustomerInfoId, 0, dbConnection);
                        }
                    }
                    if (getData == null)
                    {
                        if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                        {
                            var getTaskCategorys = PlateSource.GetAllPlateSourceByCId(userinfo.CustomerInfoId, dbConnection);
                            if (getTaskCategorys.Count > 0)
                            {
                                foreach (var getcat in getTaskCategorys)
                                {
                                    if (getcat.Description.ToLower() == name.ToLower())
                                    {
                                        PlateSource.DeletePlateSourceById(getcat.ID, dbConnection);
                                    }
                                }
                            }
                        }

                        var off = new PlateSource();
                        off.CreatedBy = userinfo.Username;
                        off.CreatedDate = CommonUtility.getDTNow();
                        off.Code = name;
                        off.Description = name;
                        off.Description_AR = nameAR;
                        off.UpdatedBy = userinfo.Username;
                        off.UpdatedDate = CommonUtility.getDTNow();
                        off.CustomerId = userinfo.CustomerInfoId;
                        off.SiteId = userinfo.SiteId;
                        var retid = PlateSource.InsertorUpdatePlateSource(off, dbConnection, dbConnectionAudit, true);

                    }
                    else
                    {
                        return "Name already exists";
                    }
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Ticketing", "addPlateSource", err, dbConnection, userinfo.SiteId);
                    return err.Message;
                }
            }
            return "SUCCESS";
        }
        [WebMethod]
        public static string delOffenceType(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                try
                {

                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    Arrowlabs.Business.Layer.Offence.DeleteOffenceById(id, dbConnection);
                    SystemLogger.SaveSystemLog(dbConnectionAudit, "Ticketing", id.ToString(), id.ToString(), userinfo, "Delete Offence Type" + id);
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Ticketing", "delOffenceType", err, dbConnection, userinfo.SiteId);
                    return err.Message;
                }

            }
            return "SUCCESS";
        }
        [WebMethod]
        public static string delOffenceCat(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    var gPlateS = Arrowlabs.Business.Layer.OffenceType.GetAllOffenceType(dbConnection);
                    if (gPlateS != null && gPlateS.Count > 0)
                    {
                        gPlateS = gPlateS.Where(i => i.OffenceTypeId == id).ToList();
                        if (gPlateS != null && gPlateS.Count > 0)
                        {
                            var allcodes = Offence.GetAllOffence(dbConnection);
                            if (allcodes != null && allcodes.Count > 0)
                            {
                                allcodes = allcodes.Where(i => i.OffenceTypeID == gPlateS[0].OffenceTypeId).ToList();

                                foreach (var item in allcodes)
                                {
                                    Arrowlabs.Business.Layer.Offence.DeleteOffenceById(item.ID, dbConnection);
                                    SystemLogger.SaveSystemLog(dbConnectionAudit, "Ticketing", item.ID.ToString(), item.ID.ToString(), userinfo, "Delete Plate Source" + id);
                                }
                            }

                            Arrowlabs.Business.Layer.OffenceType.DeleteOffenceTypeById(id, dbConnection);
                            SystemLogger.SaveSystemLog(dbConnectionAudit, "Ticketing", id.ToString(), id.ToString(), userinfo, "Delete Offence Cat" + gPlateS[0].OffenceTypeDesc);
                        }
                        else
                        {
                            return "Offence Type already deleted";
                        }
                    }
                    else
                    {
                        return "Offence Type already deleted";
                    }
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Ticketing", "delOffenceCat", err, dbConnection, userinfo.SiteId);
                    return err.Message;
                }
            }
            return "SUCCESS";
        }
        [WebMethod]
        public static string delVehMake(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    Arrowlabs.Business.Layer.VehicleMake.DeleteVehicleMakeById(id, dbConnection);
                    SystemLogger.SaveSystemLog(dbConnectionAudit, "Ticketing", id.ToString(), id.ToString(), userinfo, "Delete Vehicle Make" + id);
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Ticketing", "delVehMake", err, dbConnection, userinfo.SiteId);
                }
            }
            return "SUCCESS";
        }
        [WebMethod]
        public static string delVehColor(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    Arrowlabs.Business.Layer.VehicleColor.DeleteVehicleColorByColorCode(id, dbConnection);
                    SystemLogger.SaveSystemLog(dbConnectionAudit, "Ticketing", id.ToString(), id.ToString(), userinfo, "Delete Vehicle Color" + id);
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Ticketing", "delVehColor", err, dbConnection, userinfo.SiteId);
                    return err.Message;
                }
            }
            return "SUCCESS";
        }
        [WebMethod]
        public static string delPlateCode(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    Arrowlabs.Business.Layer.PlateCode.DeletePlateCodeById(id, dbConnection);
                    SystemLogger.SaveSystemLog(dbConnectionAudit, "Ticketing", id.ToString(), id.ToString(), userinfo, "Delete Plate Code" + id);
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Ticketing", "delPlateCode", err, dbConnection, userinfo.SiteId);
                    return err.Message;
                }
            }
            return "SUCCESS";
        }
        [WebMethod]
        public static string delPlateSource(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    var gPlateS = Arrowlabs.Business.Layer.PlateSource.GetAllPlateSource(dbConnection);
                    if (gPlateS != null && gPlateS.Count > 0)
                    {
                        gPlateS = gPlateS.Where(i => i.ID == id).ToList();
                        if (gPlateS != null && gPlateS.Count > 0)
                        {
                            var allcodes = PlateCode.GetAllPlateCode(dbConnection);
                            if (allcodes != null && allcodes.Count > 0)
                            {
                                allcodes = allcodes.Where(i => i.PlateSource == gPlateS[0].Code).ToList();

                                foreach (var item in allcodes)
                                {
                                    Arrowlabs.Business.Layer.PlateCode.DeletePlateCodeById(item.ID, dbConnection);
                                    SystemLogger.SaveSystemLog(dbConnectionAudit, "Ticketing", item.ID.ToString(), item.ID.ToString(), userinfo, "Delete Plate Source" + id);
                                }
                            }

                            Arrowlabs.Business.Layer.PlateSource.DeletePlateSourceById(id, dbConnection);
                            SystemLogger.SaveSystemLog(dbConnectionAudit, "Ticketing", id.ToString(), id.ToString(), userinfo, "Delete Plate Source" + gPlateS[0].Description);
                        }
                        else
                        {
                            return "Plate Source already deleted";
                        }
                    }
                    else
                    {
                        return "Plate Source already deleted";
                    }
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Ticketing", "delPlateSource", err, dbConnection, userinfo.SiteId);
                    return err.Message;
                }
            }
            return "SUCCESS";
        }

        [WebMethod]
        public static List<string> handleTicket(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                listy.Add("LOGOUT");
            }
            else
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        listy.Add("LOGOUT");
                        return listy;
                    }

                    var cusData = CustomEvent.GetCustomEventById(id, dbConnection);
                    cusData.Handled = true;
                    cusData.HandledBy = userinfo.Username;
                    cusData.HandledTime = CommonUtility.getDTNow();
                    cusData.UpdatedBy = userinfo.Username;
                    cusData.IncidentStatus = CommonUtility.getIncidentStatusValue("resolve");
                    CustomEvent.InsertCustomEvent(cusData, dbConnection, dbConnectionAudit, true);
                    CustomEvent.CustomEventHandledById(id, true, userinfo.Username, dbConnection);

                    var EventHistoryEntry = new EventHistory();
                    EventHistoryEntry.CreatedDate = CommonUtility.getDTNow();
                    EventHistoryEntry.CreatedBy = userinfo.Username;
                    EventHistoryEntry.EventId = id;
                    EventHistoryEntry.IncidentAction = (int)CustomEvent.IncidentActionStatus.Resolve;
                    EventHistoryEntry.UserName = userinfo.Username;
                    //EventHistoryEntry.Remarks = ins;
                    EventHistoryEntry.SiteId = userinfo.SiteId;
                    EventHistoryEntry.CustomerId = userinfo.CustomerInfoId;
                    EventHistory.InsertEventHistory(EventHistoryEntry, dbConnection, dbConnectionAudit, true);

                    SystemLogger.SaveSystemLog(dbConnectionAudit, "Ticketing", id.ToString(), id.ToString(), userinfo, "Handled Ticket" + id);

                    var alltasks = UserTask.GetAllTaskByIncidentId(cusData.EventId, dbConnection);
                    foreach (var tsk in alltasks)
                    {
                        if (tsk.Status == (int)TaskStatus.Pending || tsk.Status == (int)TaskStatus.InProgress || tsk.Status == (int)TaskStatus.Completed)
                        {
                            var tskhistory = new TaskEventHistory();
                            tsk.Status = (int)TaskStatus.Accepted;
                            tskhistory.Action = (int)TaskAction.Accepted;
                            tskhistory.CreatedBy = userinfo.Username;
                            tskhistory.CreatedDate = CommonUtility.getDTNow();
                            tskhistory.TaskId = tsk.Id;
                            tskhistory.SiteId = userinfo.SiteId;
                            tskhistory.CustomerId = userinfo.CustomerInfoId;
                            TaskEventHistory.InsertTaskEventHistory(tskhistory, dbConnection, dbConnectionAudit, true);

                            if (tsk.TaskLinkId == 0)
                            {
                                var gtsklinks = UserTask.GetAllTasksByTaskLinkId(tsk.Id, dbConnection);
                                if (gtsklinks.Count > 0)
                                {
                                    foreach (var links in gtsklinks)
                                    {
                                        if (tsk.Status == (int)TaskStatus.Pending || tsk.Status == (int)TaskStatus.InProgress || tsk.Status == (int)TaskStatus.Completed)
                                        {
                                            links.Status = (int)TaskStatus.Accepted;
                                            links.UpdatedDate = CommonUtility.getDTNow();
                                            if (userinfo != null)
                                                links.UpdatedBy = userinfo.Username;
                                            UserTask.InsertorUpdateTask(links, dbConnection, dbConnectionAudit, true);

                                            var ltskhistory = new TaskEventHistory();
                                            ltskhistory.Action = (int)TaskAction.Accepted;
                                            ltskhistory.CreatedBy = userinfo.Username;
                                            ltskhistory.CreatedDate = CommonUtility.getDTNow();
                                            ltskhistory.TaskId = links.Id;
                                            ltskhistory.SiteId = userinfo.SiteId;
                                            tskhistory.CustomerId = userinfo.CustomerInfoId;
                                            TaskEventHistory.InsertTaskEventHistory(ltskhistory, dbConnection, dbConnectionAudit, true);
                                        }
                                    }
                                }
                            }
                            tsk.UpdatedDate = CommonUtility.getDTNow();
                            if (userinfo != null)
                                tsk.UpdatedBy = userinfo.Username;

                            var tId = UserTask.InsertorUpdateTask(tsk, dbConnection, dbConnectionAudit, true);

                            SystemLogger.SaveSystemLog(dbConnectionAudit, "Ticketing", tsk.Id.ToString(), tsk.Id.ToString(), userinfo, "Handled Tasks pertaining to ticket-" + id);

                        }
                    }
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("Ticketing", "handleTicket", ex, dbConnection, userinfo.SiteId);
                    listy.Add(ex.Message);
                }
                listy.Add("SUCCESS");

            }
            return listy;
        }

        [WebMethod]
        public static List<string> startTicket(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                listy.Add("LOGOUT");
            }
            else
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        listy.Add("LOGOUT");
                        return listy;
                    }

                    var cusData = CustomEvent.GetCustomEventById(id, dbConnection);
                    cusData.Handled = true;
                    cusData.HandledBy = userinfo.Username;
                    cusData.HandledTime = CommonUtility.getDTNow();
                    cusData.UpdatedBy = userinfo.Username;
                    cusData.IncidentStatus = CommonUtility.getIncidentStatusValue("resolve");
                    CustomEvent.InsertCustomEvent(cusData, dbConnection, dbConnectionAudit, true);
                    CustomEvent.CustomEventHandledById(id, true, userinfo.Username, dbConnection);
                    SystemLogger.SaveSystemLog(dbConnectionAudit, "Ticketing", id.ToString(), id.ToString(), userinfo, "Handled Ticket" + id);
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("Ticketing", "handleTicket", ex, dbConnection, userinfo.SiteId);
                    listy.Add(ex.Message);
                }
                listy.Add("SUCCESS");

            }
            return listy;
        }

        //User Profile
        [WebMethod]
        public static string changePW(int id, string password, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                System.Web.Security.FormsAuthentication.SignOut();
                //Response.Redirect("~/Default.aspx");
                return "LOGOUT";
            }
            else
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                var getuser = Users.GetUserById(userinfo.ID, dbConnection);
                var oldPw = getuser.Password;
                getuser.Password = Encrypt.EncryptData(password, true, dbConnection);
                getuser.UpdatedBy = userinfo.Username;
                getuser.UpdatedDate = CommonUtility.getDTNow();
                if (Users.InsertOrUpdateUsers(getuser, dbConnection, dbConnectionAudit, true))
                {
                    var oldValue = oldPw;
                    var newValue = Encrypt.EncryptData(password, true, dbConnection);
                    SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "Password change");
                    return id.ToString();
                }
                else
                    return "0";
            }
        }
        [WebMethod]
        public static int addUserProfile(int id, string username, string firstname, string lastname, string emailaddress, string phonenumber, string password, int devicetype, int supervisor, int role, string imgPath, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return 0;
                }

                if (userinfo.ID > 0)
                {
                    var getuser = userinfo;
                    getuser.FirstName = firstname;
                    getuser.LastName = lastname;
                    getuser.Email = emailaddress;

                    if (getuser.RoleId != role)
                    {
                        getuser.RoleId = role;
                        if (role == (int)Role.Manager)
                        {
                            var getMang = Users.GetAllFullManagersByUserId(getuser.ID, dbConnection);
                            if (getMang != null)
                                UserManager.DeleteManagersByUserIdAndMangId(getMang.ID, getuser.ID, dbConnection);
                        }
                        else if (role == (int)Role.Operator || role == (int)Role.UnassignedOperator)
                        {
                            var getdir = Accounts.GetDirectorByManagerName(getuser.Username, dbConnection);
                            if (getdir != null)
                                DirectorManager.DeleteManagersByDirectorIdAndMangName(getuser.Username, getdir.ID, dbConnection);
                        }
                        else
                        {
                            var getdir = Accounts.GetDirectorByManagerName(getuser.Username, dbConnection);
                            if (getdir != null)
                                DirectorManager.DeleteManagersByDirectorIdAndMangName(getuser.Username, getdir.ID, dbConnection);

                            var getMang = Users.GetAllFullManagersByUserId(getuser.ID, dbConnection);
                            if (getMang != null)
                                UserManager.DeleteManagersByUserIdAndMangId(getMang.ID, getuser.ID, dbConnection);
                        }
                    }
                    if (getuser.RoleId == (int)Role.Manager)
                    {

                        var dirUser = Users.GetUserById(supervisor, dbConnection);
                        var getdir = Accounts.GetDirectorByManagerName(getuser.Username, dbConnection);

                        if (getdir != null)
                            DirectorManager.DeleteManagersByDirectorIdAndMangName(getuser.Username, getdir.ID, dbConnection);

                        if (dirUser != null)
                        {
                            if (!string.IsNullOrEmpty(dirUser.Username))
                            {
                                List<DirectorManager> userManagerList = new List<DirectorManager>() { new DirectorManager() 
                                            { 
                                                DirectorId = dirUser.ID, 
                                                ManagerId = getuser.ID,
                                                CreatedBy = dirUser.Username,
                                                CreatedDate = CommonUtility.getDTNow(),
                                                UpdatedBy = dirUser.Username,
                                                UpdatedDate = CommonUtility.getDTNow(),
                                                ManagerName = getuser.Username,
                                                ManagerAccountName = getuser.AccountName,
                                                SiteId = dirUser.SiteId,
                                                CustomerId = userinfo.CustomerInfoId                            
                                            }};
                                DirectorManager.InsertDirectorManager(userManagerList, dbConnection, dbConnectionAudit, true);
                            }
                        }
                    }
                    else if (getuser.RoleId == (int)Role.Operator)
                    {
                        if (supervisor > 0)
                        {
                            var manUser = Users.GetUserById(supervisor, dbConnection);
                            List<UserManager> userManagerList = new List<UserManager>() { new UserManager() { ManagerId = supervisor, UserId = getuser.ID, SiteId = manUser.SiteId, CustomerId = manUser.CustomerInfoId } };

                            UserManager.InsertUserManagers(userManagerList, dbConnection, dbConnectionAudit, true);
                        }
                    }
                    else if (getuser.RoleId == (int)Role.UnassignedOperator)
                    {
                        var getMang = Users.GetAllFullManagersByUserId(getuser.ID, dbConnection);
                        if (getMang != null)
                            UserManager.DeleteManagersByUserIdAndMangId(getMang.ID, getuser.ID, dbConnection);
                    }
                    if (Users.InsertOrUpdateUsers(getuser, dbConnection, dbConnectionAudit, true))
                    {
                        return userinfo.ID;
                    }
                    else
                        return 0;
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Ticketing", "addUserProfile", err, dbConnection, userinfo.SiteId);
            }
            return userinfo.ID;
        }

        [WebMethod]
        public static List<string> getUserProfileData(int id, string uname)
        {
            var listy = new List<string>();
            var customData = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(customData.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }
                var supervisorId = 0;
                if (customData != null)
                {
                    listy.Add(customData.Username);
                    listy.Add(customData.FirstName + " " + customData.LastName);
                    listy.Add(customData.Telephone);
                    listy.Add(customData.Email);
                    var geoLoc = ReverseGeocode.RetrieveFormatedAddress(customData.Latitude.ToString(), customData.Longitude.ToString(), getClientLic);
                    listy.Add(geoLoc);
                    listy.Add(CommonUtility.getUserRoleName(customData.RoleId));
                    if (customData.RoleId == (int)Role.Operator)
                    {
                        var usermanager = Users.GetAllFullManagersByUserId(customData.ID, dbConnection);
                        if (usermanager != null)
                        {
                            listy.Add(usermanager.CustomerUName);
                            supervisorId = usermanager.ID;
                        }
                        else
                            listy.Add("Unassigned");
                    }
                    else if (customData.RoleId == (int)Role.Manager)
                    {
                        var getdir = Accounts.GetDirectorByManagerName(customData.Username, dbConnection);
                        if (getdir != null)
                        {
                            listy.Add(getdir.CustomerUName);
                            supervisorId = getdir.ID;
                        }
                        else
                            listy.Add("Unassigned");
                    }
                    else if (customData.RoleId == (int)Role.UnassignedOperator)
                    {
                        listy.Add("Unassigned");
                    }
                    else
                    {
                        listy.Add(" ");
                    }
                    listy.Add("Group Name");
                    listy.Add(customData.Status);
                    listy.Add("circle-point " + CommonUtility.getImgUserStatus(customData.Status));
                    var imgSrc = CommonUtility.getUserPhotoUrl(customData.ID, dbConnection);
                    //var userImg = UserImage.GetUserImageByUserId(customData.ID, dbConnection);
                    //var imgSrc = "../images/icon-user-default.png";//images / custom - images / user - 1.png;
                    //if (userImg != null)
                    //{
                    //    var base64 = Convert.ToBase64String(userImg.ImageFile);
                    //    imgSrc = String.Format("data:image/png;base64,{0}", base64);
                    //}
                    var fontstyle = string.Empty;
                    if (customData.Active == (int)Arrowlabs.Business.Layer.HealthCheck.DeviceStatus.Online)
                    {
                        fontstyle = "style='color:lime;'";
                    }
                    //   var pushDev = PushNotificationDevice.GetPushNotificationDeviceByUsername(customData.Username, dbConnection);
                    var monitor = string.Empty;
                    //if (customData.RoleId == (int)Role.Admin || customData.RoleId == (int)Role.Manager || customData.RoleId == (int)Role.Director || customData.RoleId == (int)Role.Regional || customData.RoleId == (int)Role.ChiefOfficer)
                    if (customData.RoleId != (int)Role.SuperAdmin)
                    {
                        if (customData.IsServerPortal)
                        {
                            monitor = "<i " + fontstyle + " class='fa fa-laptop fa-2x mr-2x'></i>";
                            fontstyle = "";
                        }
                        else
                        {
                            monitor = "<i class='fa fa-laptop fa-2x mr-2x'></i>";
                        }
                        //   if (pushDev != null)
                        //   {
                        //      if (!string.IsNullOrEmpty(pushDev.Username))
                        //      {
                        //          if (pushDev.IsServerPortal)
                        //           {
                        //               monitor = "<i " + fontstyle + " class='fa fa-laptop fa-2x mr-2x'></i>";
                        //               fontstyle = "";
                        //        }
                        //        else
                        //        {
                        //            monitor = "<i class='fa fa-laptop fa-2x mr-2x'></i>";
                        //        }
                        //    }
                        //    else
                        //    {
                        //        monitor = "<i " + fontstyle + " class='fa fa-laptop fa-2x mr-2x'></i>";
                        //        fontstyle = "";
                        //    }
                        //}
                    }
                    listy.Add(imgSrc);
                    listy.Add(CommonUtility.getUserDeviceType(customData.DeviceType, fontstyle, monitor));
                    listy.Add(CommonUtility.getRoleSupervisor(customData.RoleId));
                    listy.Add(customData.FirstName);
                    listy.Add(customData.LastName);
                    listy.Add(supervisorId.ToString());
                    listy.Add(Decrypt.DecryptData(customData.Password, true, dbConnection));
                    listy.Add(customData.Latitude.ToString());
                    listy.Add(customData.Longitude.ToString());

                    var userSiteDisplay = customData == null ? "N/A" : customData.SiteName;
                    if (customData.RoleId != (int)Role.Regional)
                    {
                        if (customData.SiteId == 0)
                            listy.Add("N/A");
                        else
                            listy.Add(userSiteDisplay);
                    }
                    else
                    {
                        listy.Add("Multiple");
                    }



                    listy.Add(customData.Gender);
                    listy.Add(customData.EmployeeID);
                }
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("Incident", "getUserProfileData", er, dbConnection, customData.SiteId);
            }
            return listy;
        }
        protected string GetIPAddress()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }

            return context.Request.ServerVariables["REMOTE_ADDR"];
        }
        protected void forceLogoutButton_Click(object sender, EventArgs e)
        {
            System.Web.Security.FormsAuthentication.SignOut();
            Response.Redirect("~/Default.aspx");
        }
        protected void LogoutButton_Click(object sender, EventArgs e)
        {
            var customData = Users.GetUserByName(User.Identity.Name, dbConnection);
            CommonUtility.LogoutUser(customData, System.Web.HttpContext.Current.Session.SessionID, GetIPAddress());
            System.Web.Security.FormsAuthentication.SignOut();
            Response.Redirect("~/Default.aspx");
        }


        [WebMethod]
        public static string delOffenceCat2(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    var gPlateS = Arrowlabs.Business.Layer.OffenceCategory.GetAllOffenceCategory(dbConnection);
                    if (gPlateS != null && gPlateS.Count > 0)
                    {
                        gPlateS = gPlateS.Where(i => i.Id == id).ToList();
                        var gAllTypes = Arrowlabs.Business.Layer.OffenceType.GetAllOffenceType(dbConnection);
                        gAllTypes = gAllTypes.Where(i => i.OffenceCategoryId == id).ToList();
                        if (gAllTypes != null && gAllTypes.Count > 0)
                        {
                            foreach (var gtypes in gAllTypes)
                            {
                                if (gPlateS != null && gPlateS.Count > 0)
                                {
                                    var allcodes = Offence.GetAllOffence(dbConnection);
                                    if (allcodes != null && allcodes.Count > 0)
                                    {
                                        allcodes = allcodes.Where(i => i.OffenceTypeID == gtypes.OffenceTypeId).ToList();

                                        foreach (var item in allcodes)
                                        {
                                            Arrowlabs.Business.Layer.Offence.DeleteOffenceById(item.ID, dbConnection);
                                            SystemLogger.SaveSystemLog(dbConnectionAudit, "Ticketing", item.ID.ToString(), item.ID.ToString(), userinfo, "Delete Plate Source" + id);
                                        }
                                    }

                                    Arrowlabs.Business.Layer.OffenceType.DeleteOffenceTypeById(gtypes.OffenceTypeId, dbConnection);
                                    SystemLogger.SaveSystemLog(dbConnectionAudit, "Ticketing", gtypes.OffenceTypeId.ToString(), gtypes.OffenceTypeId.ToString(), userinfo, "Delete Offence Type" + gtypes.OffenceTypeDesc);
                                }
                            }
                        }
                        Arrowlabs.Business.Layer.OffenceCategory.DeleteOffenceCategoryById(id, dbConnection);
                        SystemLogger.SaveSystemLog(dbConnectionAudit, "Ticketing", id.ToString(), id.ToString(), userinfo, "Delete Offence Cat" + gPlateS[0].Name);
                    }
                    else
                    {
                        return "Offence Category already deleted";
                    }
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Ticketing", "delOffenceCat2", err, dbConnection, userinfo.SiteId);
                    return err.Message;
                }
            }
            return "SUCCESS";
        }
        [WebMethod]
        public static string editOffenceCat2(int id, string name, string nameAR, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    var getData = OffenceCategory.GetOffenceCategoryById(id, dbConnection);
                    if (getData != null)
                    {

                        var oldValue = string.Empty;
                        var newValue = string.Empty;
                        if (getData.Name.ToLower() != name.ToLower())
                        {
                            var getData2 = OffenceCategory.GetOffenceCategoryByNameAndCIdAndSiteId(name, userinfo.CustomerInfoId, userinfo.SiteId, dbConnection);

                            if (userinfo.RoleId == (int)Role.Director)
                            {
                                if (getData2 == null)
                                {
                                    getData2 = OffenceCategory.GetOffenceCategoryByNameAndCIdAndSiteId(name, userinfo.CustomerInfoId, 0, dbConnection);
                                }
                            }
                            if (getData2 == null)
                            {
                                oldValue = getData.Name;
                                newValue = name;
                                getData.Name = name;
                                SystemLogger.SaveSystemLog(dbConnectionAudit, "Ticketing", newValue, oldValue, userinfo, "OFFENCE CATEGORY OffenceTypeDesc-" + id);
                            }
                            else
                            {
                                return "Name already exists";
                            }

                        }
                        if (getData.Description != nameAR)
                        {
                            oldValue = getData.Description;
                            newValue = nameAR;
                            getData.Description = nameAR;
                            SystemLogger.SaveSystemLog(dbConnectionAudit, "Ticketing", newValue, oldValue, userinfo, "OFFENCE CATEGORY OffenceTypeDesc_AR-" + id);
                        }
                        getData.UpdatedBy = userinfo.Username;
                        getData.UpdatedDate = CommonUtility.getDTNow();
                        OffenceCategory.InsertorUpdateOffenceCategory(getData, dbConnection, dbConnectionAudit, true);

                    }
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Ticketing", "editOffenceCat2", err, dbConnection, userinfo.SiteId);
                    return err.Message;
                }
                return "SUCCESS";
            }
        }

        [WebMethod]
        public static string editOffenceCat(int id, string name, int cval, string nameAR, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                try
                {
                    var getData = OffenceType.GetOffenceTypeById(id, dbConnection);
                    if (getData != null)
                    {

                        var oldValue = string.Empty;
                        var newValue = string.Empty;
                        if (getData.OffenceTypeDesc != name)
                        {

                            var getData2 = OffenceType.GetOffenceTypeByNameAndCIdAndSiteId(name, userinfo.CustomerInfoId, userinfo.SiteId, dbConnection);

                            if (userinfo.RoleId == (int)Role.Director)
                            {
                                if (getData2 == null)
                                {
                                    getData2 = OffenceType.GetOffenceTypeByNameAndCIdAndSiteId(name, userinfo.CustomerInfoId, 0, dbConnection);
                                }
                            }
                            if (getData2 == null)
                            {
                                oldValue = getData.OffenceTypeDesc;
                                newValue = name;
                                getData.OffenceTypeDesc = name;
                                SystemLogger.SaveSystemLog(dbConnectionAudit, "Ticketing", newValue, oldValue, userinfo, "OFFENCE TYPE OffenceTypeDesc-" + id);
                            }
                            else
                            {
                                return "Name already exists";
                            }
                        }
                        if (getData.OffenceTypeDesc_AR != nameAR)
                        {
                            oldValue = getData.OffenceTypeDesc_AR;
                            newValue = nameAR;
                            getData.OffenceTypeDesc_AR = nameAR;
                            SystemLogger.SaveSystemLog(dbConnectionAudit, "Ticketing", newValue, oldValue, userinfo, "OFFENCE TYPE OffenceTypeDesc_AR-" + id);
                        }
                        getData.OffenceCategoryId = cval;
                        getData.UpdatedBy = userinfo.Username;
                        getData.UpdatedDate = CommonUtility.getDTNow();
                        OffenceType.InsertorUpdateOffenceType(getData, dbConnection, dbConnectionAudit, true);
                    }

                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Ticketing", "editOffenceCat", err, dbConnection, userinfo.SiteId);
                    return err.Message;
                }
                return "SUCCESS";
            }
        }

        [WebMethod]
        public static string addOffenceCat2(int id, string name, string nameAR, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    var getData = OffenceCategory.GetOffenceCategoryByNameAndCIdAndSiteId(name, userinfo.CustomerInfoId, userinfo.SiteId, dbConnection);

                    if (userinfo.RoleId == (int)Role.Director)
                    {
                        if (getData == null)
                        {
                            getData = OffenceCategory.GetOffenceCategoryByNameAndCIdAndSiteId(name, userinfo.CustomerInfoId, 0, dbConnection);
                        }
                    }
                    if (getData == null)
                    {

                        var otlist = new List<OffenceType>();
                        if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                        {
                            var getTaskCategorys = OffenceCategory.GetAllOffenceCategoryByCId(userinfo.CustomerInfoId, dbConnection);
                            if (getTaskCategorys.Count > 0)
                            {
                                foreach (var getcat in getTaskCategorys)
                                {
                                    if (getcat.Name.ToLower() == name.ToLower())
                                    {
                                        var olist = OffenceType.GetAllOffenceTypeByOffenceCategoryId(getcat.Id, dbConnection);
                                        otlist.AddRange(olist);
                                        OffenceCategory.DeleteOffenceCategoryById(getcat.Id, dbConnection);
                                    }
                                }
                            }
                        }

                        var off = new OffenceCategory();
                        off.Name = name;
                        off.Description = nameAR;
                        off.UpdatedBy = userinfo.Username;
                        off.UpdatedDate = CommonUtility.getDTNow();
                        off.CreatedBy = userinfo.Username;
                        off.CreatedDate = CommonUtility.getDTNow();
                        off.CustomerId = userinfo.CustomerInfoId;
                        off.SiteId = userinfo.SiteId;
                        var rid = OffenceCategory.InsertorUpdateOffenceCategory(off, dbConnection, dbConnectionAudit, true);
                        foreach (var item in otlist)
                        {
                            item.OffenceCategoryId = rid;
                            item.UpdatedBy = userinfo.Username;
                            item.UpdatedDate = CommonUtility.getDTNow();
                            OffenceType.InsertorUpdateOffenceType(item, dbConnection);
                        }

                    }
                    else
                    {
                        return "Name already exists";
                    }
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Ticketing", "addOffenceCat", err, dbConnection, userinfo.SiteId);
                    return err.Message;
                }
            }
            return "SUCCESS";
        }

        [WebMethod]
        public static List<string> getServerData(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }
                else
                {
                    var licenseInfo = ClientLicence.GetLicenseByClientID(CommonUtility.arrowlabsKey, dbConnection);
                    var getalldevs = Device.GetClientDeviceByClientID(CommonUtility.arrowlabsKey, dbConnection);
                    var devinuse = 0;
                    foreach (var dev in getalldevs)
                    {
                        if (dev.State == Arrowlabs.Business.Layer.Device.DeviceState.Enable.ToString())
                            devinuse++;
                    }
                    var users = Users.GetAllUsersByCustomerId(userinfo.CustomerInfoId, dbConnection).Count;//Users.GetAllMobileOnlineUsers(dbConnection);//LoginSession.GetAllMimsMobileOnlineByDeviceType(1, dbConnection);
                    var cInfo = CustomerInfo.GetCustomerInfoById(userinfo.CustomerInfoId, dbConnection);
                    if (licenseInfo != null)
                    {
                        var remaining = (cInfo.TotalUser - users).ToString();
                        listy.Add("N/A");
                        listy.Add("N/A");
                        listy.Add("N/A");
                        listy.Add("N/A");
                        listy.Add("N/A");
                        listy.Add("N/A");
                        listy.Add("N/A"); //Remaining Client
                        listy.Add("N/A"); //Total Client
                        listy.Add("N/A"); //Used Client
                        listy.Add(remaining); // Remaining Mobile
                        listy.Add(cInfo.TotalUser.ToString());//licenseInfo.TotalMobileUsers); //Total Mobile
                        listy.Add(users.ToString()); // Used
                        //licenseInfo.RemainingMobileUsers = remaining;
                        //licenseInfo.UsedMobileUsers = users.ToString();

                        var modules = cInfo.Modules.Split('?');

                        //listy.Add(licenseInfo.isSurveillance.ToString().ToLower());
                        var isSurv = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Surveillance).ToString()).ToList();
                        if (isSurv != null && isSurv.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isNotification.ToString().ToLower());//CHANGED TO MESAGEBOARD
                        var isNoti = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.MessageBoard).ToString()).ToList();
                        if (isNoti != null && isNoti.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isLocation.ToString().ToLower()); //CHANGED TO MESAGEBOARD
                        var isLoc = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Contract).ToString()).ToList();
                        if (isLoc != null && isLoc.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isTicketing.ToString().ToLower());
                        var isTicket = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Ticketing).ToString()).ToList();
                        if (isTicket != null && isTicket.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isTask.ToString().ToLower());
                        var isTask = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Task).ToString()).ToList();
                        if (isTask != null && isTask.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isIncident.ToString().ToLower());
                        var isInci = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Incident).ToString()).ToList();
                        if (isInci != null && isInci.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isWarehouse.ToString().ToLower());
                        var isWare = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Warehouse).ToString()).ToList();
                        if (isWare != null && isWare.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");


                        //listy.Add(licenseInfo.isChat.ToString().ToLower());
                        var isChat = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Chat).ToString()).ToList();
                        if (isChat != null && isChat.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isCollaboration.ToString().ToLower());
                        var isCollab = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Collaboration).ToString()).ToList();
                        if (isCollab != null && isCollab.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isLostandFound.ToString().ToLower());
                        var isLF = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.LostandFound).ToString()).ToList();
                        if (isLF != null && isLF.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");


                        //listy.Add(licenseInfo.isDutyRoster.ToString().ToLower());
                        var isDR = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.DutyRoster).ToString()).ToList();
                        if (isDR != null && isDR.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isPostOrder.ToString().ToLower());
                        var isPO = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.PostOrder).ToString()).ToList();
                        if (isPO != null && isPO.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isVerification.ToString().ToLower());
                        var isVeri = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Verification).ToString()).ToList();
                        if (isVeri != null && isVeri.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isRequest.ToString().ToLower());
                        var isRequest = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Request).ToString()).ToList();
                        if (isRequest != null && isRequest.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");


                        //listy.Add(licenseInfo.isDispatch.ToString().ToLower());
                        var isDisp = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Dispatch).ToString()).ToList();
                        if (isDisp != null && isDisp.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        var isAct = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Activity).ToString()).ToList();
                        if (isAct != null && isAct.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //ClientLicence.InsertClientLicence(licenseInfo, dbConnection, dbConnectionAudit, true);
                        listy.Add(cInfo.Country);
                    }
                }
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("Devices", "getServerData", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static string saveTZ(string id, string uname)
        {
            var json = string.Empty;
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {
                    var cInfo = CustomerInfo.GetCustomerInfoById(userinfo.CustomerInfoId, dbConnection);
                    if (cInfo != null)
                    {
                        var split = id.Split('(');
                        var cname = split[0];
                        var spli2 = split[1].Split(')');
                        var doubles = spli2[0];
                        var ctimezone = Convert.ToDouble(doubles.Split(':')[0]);

                        cInfo.Country = cname;
                        cInfo.TimeZone = ctimezone;
                        var latlng = ReverseGeocode.RetrieveFormatedGeo(cname);
                        if (latlng.Count > 0)
                        {
                            cInfo.Lati = latlng[0];
                            cInfo.Long = latlng[1];
                        }
                        CustomerInfo.InsertorUpdateCustomerInfo(cInfo, dbConnection);

                        return "SUCCESS";
                    }
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("Tasks.aspx", "deleteSystemType", ex, dbConnection, userinfo.SiteId);
                    return ex.Message;
                }
                return json;
            }
        }

        [WebMethod]
        public static List<ContractInfo> getContractListByCustomerId(int id, string uname)
        {
            var fullcollection = new List<ContractInfo>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                if (userinfo != null)
                {
                    var cust = new ContractInfo();
                    cust.Id = 0; cust.Id = 0;
                    cust.ContractRef = "Select Type";
                    cust.CreatedDate = CommonUtility.getDTNow();
                    cust.CreatedBy = userinfo.Username;
                    fullcollection.Add(cust);
                    if (userinfo.RoleId != (int)Role.CustomerUser)
                    {

                        fullcollection = ContractInfo.GetContractInfosByCustomerId(id, dbConnection);


                    }
                    else
                    {
                        fullcollection.Add(ContractInfo.GetContractInfoById(userinfo.ContractLinkId, dbConnection));
                    }
                    fullcollection = fullcollection.Where(i => i.Active == true).ToList();
                    fullcollection = fullcollection.Where(i => i.EndDate.Value.Date >= CommonUtility.getDTNow().Date).ToList();
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Ticketing", "getContractListByCustomerId", ex, dbConnection, userinfo.SiteId);
            }
            //<tr role="row" class="odd clickable-row clickable-row-links"><td><span class="circle-point-container"><span class="circle-point circle-point-orange"></span></span></td><td class="sorting_1">Gecko</td><td>Firefox 1.0</td><td>Win 98+ / OSX.2+</td><td>'++'</td><td><a href="#"><i class="fa fa-eye mr-1x"></i>View</a></td></tr>
            return fullcollection;
        }
        [WebMethod]
        public static List<Project> getProjectListByCustomerId(int id, string uname)
        {
            var fullcollection = new List<Project>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                if (userinfo != null)
                {
                    if (id > 0)
                    {
                        fullcollection.AddRange(Project.GetProjectByCustomerId(id, dbConnection));
                        fullcollection = fullcollection.Where(i => i.UserId == userinfo.ID || i.CreatedBy == userinfo.Username).ToList();
                        fullcollection = fullcollection.Where(i => i.EndDate.Value.Date >= CommonUtility.getDTNow().Date).ToList();
                    }
                    else
                    {

                        if (userinfo.RoleId == (int)Role.Admin || userinfo.RoleId == (int)Role.Manager || userinfo.RoleId == (int)Role.Director)
                        {
                            fullcollection.AddRange(Project.GetAllProjectsBySiteId(userinfo.SiteId, dbConnection));
                        }
                        else if (userinfo.RoleId == (int)Role.Regional)
                        {
                            fullcollection.AddRange(Project.GetAllProjectsByLevel5(userinfo.ID, dbConnection));
                        }
                        else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                        {
                            fullcollection.AddRange(Project.GetAllProjectByCustomerInfoId(userinfo.CustomerInfoId, dbConnection));
                        }
                        else
                        {
                            fullcollection.AddRange(Project.GetAllProject(dbConnection));
                        }
                        fullcollection = fullcollection.Where(i => i.UserId == userinfo.ID || i.CreatedBy == userinfo.Username).ToList();
                        fullcollection = fullcollection.Where(i => i.EndDate.Value.Date >= CommonUtility.getDTNow().Date).ToList();

                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Ticketing", "getProjectListByCustomerId", ex, dbConnection, userinfo.SiteId);
            }
            return fullcollection;
        }

        [WebMethod]
        public static string getLocationData(int id, string uname)
        {
            var json = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                var locs = new List<Location>();
                if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                {
                    locs.AddRange(Location.GetAllLocation(dbConnection));
                    locs.AddRange(Location.GetAllSubLocation(dbConnection));
                }
                else if (userinfo.RoleId == (int)Role.Admin || userinfo.RoleId == (int)Role.Manager || userinfo.RoleId == (int)Role.Director)
                    locs.AddRange(Location.GetAllLocationBySiteId(userinfo.SiteId, dbConnection));
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                    // var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                    // foreach (var site in sites)
                    locs.AddRange(Location.GetAllLocationByLevel5(userinfo.ID, dbConnection));
                }
                else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                {
                    locs.AddRange(Location.GetAllLocationByCustomerId(userinfo.CustomerInfoId, dbConnection));
                }

                json += "[";
                if (getClientLic != null)
                {
                    if (getClientLic.isLocation)
                    {
                        if (getClientLic != null)
                        {
                            if (getClientLic.isLocation)
                            {
                                foreach (var item in locs)
                                {
                                    json += "{ \"Username\" : \"" + item.LocationDesc + "\",\"Id\" : \"" + item.ID.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                                }
                            }
                        }
                    }
                }
                json = json.Substring(0, json.Length - 1);
                json += "]";
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Tasks", "getLocationData", ex, dbConnection, userinfo.SiteId);
            }
            return json;
        }

        [WebMethod]
        public static List<string> getLocationById(string id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                if (!string.IsNullOrEmpty(id))
                {
                    var getloc = Location.GetLocationById(Convert.ToInt32(id), dbConnection);
                    listy.Add(getloc.Longitude.ToString());
                    listy.Add(getloc.Latitude.ToString());
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Tasks", "getLocationById", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static List<Customer> getCustomerByProjectId(int id, string uname)
        {
            var fullcollection = new List<Customer>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                if (userinfo != null)
                {
                    if (id > 0)
                    {

                        var cust = new Customer();
                        cust.Id = 0; cust.Id = 0;
                        cust.ClientName = "Select Account";
                        cust.CreatedDate = CommonUtility.getDTNow();
                        cust.CreatedBy = userinfo.Username;
                        fullcollection.Add(cust);
                        var pproj = Project.GetProjectById(id, dbConnection);
                        if (pproj != null)
                        {
                            var custt = Customer.GetCustomerById(pproj.CustomerId, dbConnection);
                            if (custt != null)
                            {
                                if (custt.Status)
                                    fullcollection.Add(custt);
                            }
                            else
                            {
                            }
                        }
                        else
                        {
                        }
                    }
                    else
                    {
                        var cust = new Customer();
                        cust.Id = 0; cust.Id = 0;
                        cust.ClientName = "Select Account";
                        cust.CreatedDate = CommonUtility.getDTNow();
                        cust.CreatedBy = userinfo.Username;
                        var ffullcollection = new List<Customer>();
                        if (userinfo.RoleId == (int)Role.Admin || userinfo.RoleId == (int)Role.Manager || userinfo.RoleId == (int)Role.Director)
                        {
                            ffullcollection.AddRange(Customer.GetAllCustomersBySiteId(userinfo.SiteId, dbConnection));
                        }
                        else if (userinfo.RoleId == (int)Role.Regional)
                        {
                            ffullcollection.AddRange(Customer.GetAllCustomersByLevel5(userinfo.ID, dbConnection));
                        }
                        else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                        {
                            ffullcollection.AddRange(Customer.GetAllCustomersByCustomerInfoId(userinfo.CustomerInfoId, dbConnection));
                        }
                        else if (userinfo.RoleId == (int)Role.CustomerUser)
                        {
                            ffullcollection.Add(Customer.GetCustomerById(userinfo.CustomerLinkId, dbConnection));
                        }
                        else
                        {
                            ffullcollection.AddRange(Customer.GetAllCustomers(dbConnection));
                        }

                        ffullcollection = ffullcollection.Where(i => i.Status == true).ToList();
                        fullcollection.Add(cust);
                        fullcollection.AddRange(ffullcollection);
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Ticketing", "getProjectListByCustomerId", ex, dbConnection, userinfo.SiteId);
            }
            return fullcollection;
        }

        [WebMethod]
        public static List<string> getAssignUserTableData(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var allusers = new List<Users>();
                var allgroups = new List<Group>();
                if (userinfo.RoleId == (int)Role.Manager)
                {
                    allusers = Users.GetAllFullUsersByManagerId(userinfo.ID, dbConnection);
                    allgroups = Group.GetAllGroupByCreator(userinfo.Username, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                {
                    allusers.AddRange(Users.GetAllUsersByCustomerIdNotIncludeCustomerUser(userinfo.CustomerInfoId, dbConnection));
                    allusers = allusers.Where(i => i.RoleId != (int)Role.CustomerSuperadmin).ToList();
                    allgroups.AddRange(Group.GetAllGroupByCreator(userinfo.Username, dbConnection));
                }
                else if (userinfo.RoleId == (int)Role.Admin)
                {
                    allgroups = Group.GetAllGroupByCreator(userinfo.Username, dbConnection);
                    var sessions = DirectorManager.GetAllFullManagersByDirectorId(userinfo.ID, dbConnection);
                    foreach (var usr in sessions)
                    {
                        allusers.Add(usr);
                        var getallUsers = Users.GetAllFullUsersByManagerIdForDirector(usr.ID, dbConnection);
                        foreach (var subsubuser in getallUsers)
                        {
                            allusers.Add(subsubuser);
                        }
                    }
                    var unassigned = Users.GetAllUnassignedOperators(dbConnection);
                    unassigned = unassigned.Where(i => i.SiteId == (int)userinfo.SiteId).ToList();
                    foreach (var subsubuser in unassigned)
                    {
                        allusers.Add(subsubuser);
                    }

                }
                else if (userinfo.RoleId == (int)Role.Director)
                {
                    allusers = Users.GetAllUsersBySiteId(userinfo.SiteId, dbConnection);
                    allgroups = Group.GetAllGroupByCreator(userinfo.Username, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                    //    var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                    //  foreach (var site in sites)
                    //  {
                    allusers.AddRange(Users.GetAllUsersByLevel5(userinfo.ID, dbConnection));
                    //  }
                    allgroups = Group.GetAllGroupByCreator(userinfo.Username, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.ChiefOfficer)
                {
                    allusers = Users.GetAllUsers(dbConnection);
                    allusers = allusers.Where(i => i.RoleId != (int)Role.ChiefOfficer).ToList();
                    allgroups = Group.GetAllGroupByCreator(userinfo.Username, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.SuperAdmin)
                {
                    allusers = Users.GetAllUsers(dbConnection);
                    allgroups = Group.GetAllGroup(dbConnection);
                }
                allusers = allusers.Where(item => item.Username != userinfo.Username).ToList();
                allusers = allusers.Where(i => i.RoleId != (int)Role.MessageBoardUser && i.RoleId != (int)Role.CustomerUser).ToList();
                var grouped = allusers.GroupBy(item => item.ID);
                allusers = grouped.Select(grp => grp.OrderBy(item => item.ID).First()).ToList();

                foreach (var item in allusers)
                {
                    //if (item.DeviceType == (int)Users.DeviceTypes.Mobile || item.DeviceType == (int)Users.DeviceTypes.MobileAndClient || item.RoleId == (int)Role.Manager)
                    //{
                        var returnstring = "<tr role='row' class='odd'><td>" + item.CustomerUName + "</td><td class='sorting_1'>" + item.Status + "</td><td><a href='#' style='color:#b2163b' class='red-color' id=" + item.ID + "-User onclick='dispatchUserchoiceTable(&apos;" + item.ID + "&apos;,&apos;" + item.CustomerUName + "&apos;,&apos;User&apos;)'><i class='fa fa-plus red-color' ></i>ADD</a></td></tr>";
                        listy.Add(returnstring);
                    //}
                }
                foreach (var grp in allgroups)
                {
                    var newgrpName = grp.Name.Replace(" ", "_");
                    var returnstring = "<tr role='row' class='odd'><td>" + grp.Name + "</td><td class='sorting_1'>Group</td><td><a style='color:#b2163b' href='#' class='red-color' id=" + grp.Id + "-Group onclick='dispatchUserchoiceTable(&apos;" + grp.Id + "&apos;,&apos;" + newgrpName + "&apos;,&apos;Group&apos;)'><i class='fa fa-plus red-color' ></i>ADD</a></td></tr>";
                    listy.Add(returnstring);
                }
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("Incident", "getAssignUserTableData", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getTaskListDataTicket(int id, string uname)
        {

            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }
                var chklisty = new List<string>();
                var usertask = UserTask.GetAllTasksByIncidentId(id, dbConnection);
                foreach (var item in usertask)
                {
                    var stringCheck = string.Empty;
                    if (item.Status == (int)TaskStatus.Completed || item.Status == (int)TaskStatus.Accepted)
                        stringCheck = "Checked";
                    else
                    {
                        stringCheck = "Unchecked";
                    }

                    chklisty.Add(item.Name + "|" + stringCheck + "|" + item.Id.ToString() + "|" + item.StatusDescription);

                    var linktasks = UserTask.GetAllTasksByTaskLinkId(item.Id, dbConnection);
                    if (linktasks.Count > 0)
                    {
                        foreach (var item2 in linktasks)
                        {
                            stringCheck = string.Empty;
                            if (item2.Status == (int)TaskStatus.Completed || item.Status == (int)TaskStatus.Accepted)
                                stringCheck = "Checked";
                            else
                            {
                                stringCheck = "Unchecked";
                            }

                            chklisty.Add(item2.Name + "|" + stringCheck + "|" + item2.Id.ToString() + "|" + item2.StatusDescription);
                        }
                    }
                }
                listy.AddRange(chklisty);
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("Ticketing", "getTaskListDataTicket", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static List<string> InsertTaskOnly(string name, string desc, string assignType
    , string assignId, string assigneeName, string longi, string lati, string startdate
    , string priority, string checklistid, string recurring, string lbrecurring
    , string uname, string tasktype, string linkId, string projectid, string contractId, string customerId, string startT, string issig, string ticketId)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            var listy = new List<string>();
            if (!retVal)
            {
                listy.Add("LOGOUT");
            }
            else
            {
                try
                {

                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        listy.Add("LOGOUT");
                        return listy;
                    }

                    List<string> calculatedDates = null;
                    var recuringVal = string.Empty;
                    //var selectedTaskStartDate = startdate + " 00:00";
                    //DateTime selectedTaskStartDateTime = DateTime.ParseExact(selectedTaskStartDate, "MM/dd/yyyy HH:mm",
                    //               System.Globalization.CultureInfo.InvariantCulture);

                    var selectedTaskStartDateTime = DateTime.Parse(startdate + "," + startT);
                    var num = userinfo.TimeZone;
                    if (num < 0)
                    {
                        num = num + num + num;
                    }
                    else if (num > 0)
                    {
                        num = num - num - num;
                    }

                    selectedTaskStartDateTime = selectedTaskStartDateTime.AddHours(num);
                    var dtnow = CommonUtility.getDTNow();
                    if (!string.IsNullOrEmpty(recurring))
                    {
                        if (Convert.ToBoolean(recurring))
                        {
                            recuringVal = lbrecurring;
                            calculatedDates = CommonUtility.GetDatesForRecurringTask(recuringVal, selectedTaskStartDateTime);
                        }
                    }

                    var loggedInManager = userinfo;
                    var newTask = new UserTask();
                    newTask.Name = name;
                    newTask.Description = desc;


                    //var project = Project.GetProjectById((string.IsNullOrEmpty(projectid)) ? 0 : Convert.ToInt16(projectid), dbConnection);
                    //if (project != null)
                    //{
                    //    newTask.Systype = project.SystypeId;

                    //}
                    newTask.CustId = (string.IsNullOrEmpty(customerId)) ? 0 : Convert.ToInt16(customerId);
                    newTask.ContractId = (string.IsNullOrEmpty(contractId)) ? 0 : Convert.ToInt16(contractId);
                    newTask.ProjectId = (string.IsNullOrEmpty(projectid)) ? 0 : Convert.ToInt16(projectid);

                    newTask.AssigneeId = Convert.ToInt32(assignId);

                    if (assignType == TaskAssigneeType.User.ToString())
                    {
                        newTask.AssigneeType = (int)TaskAssigneeType.User;
                        var aUser = Users.GetUserById(newTask.AssigneeId, dbConnection);
                        if (aUser != null)
                            newTask.AssigneeName = aUser.Username;
                        else
                            newTask.AssigneeName = assigneeName;

                        if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                        {
                            if (newTask.CustId > 0)
                            {
                                var gCustomer = Customer.GetCustomerById(newTask.CustId, dbConnection);
                                if (gCustomer.SiteId > 0 && gCustomer.SiteId != aUser.SiteId)
                                {
                                    listy.Add("Selected user is from a different site than the specified customer");
                                    return listy;
                                }
                            }
                            if (newTask.ContractId > 0)
                            {
                                var gCustomer = ContractInfo.GetContractInfoById(newTask.ContractId, dbConnection);
                                if (gCustomer.SiteId > 0 && gCustomer.SiteId != aUser.SiteId)
                                {
                                    listy.Add("Selected user is from a different site than the specified contract");
                                    return listy;
                                }
                            }
                            if (newTask.ProjectId > 0)
                            {
                                var gCustomer = Project.GetProjectById(newTask.ProjectId, dbConnection);
                                if (gCustomer.SiteId > 0 && gCustomer.SiteId != aUser.SiteId)
                                {
                                    listy.Add("Selected user is from a different site than the specified project");
                                    return listy;
                                }
                            }
                        }
                    }
                    else if (assignType == TaskAssigneeType.Group.ToString())
                    {
                        newTask.AssigneeType = (int)TaskAssigneeType.Group;

                        var aUser = Group.GetGroupById(newTask.AssigneeId, dbConnection);
                        if (aUser != null)
                            newTask.AssigneeName = aUser.Name;
                        else
                            newTask.AssigneeName = assigneeName;

                        if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                        {
                            if (newTask.CustId > 0)
                            {
                                var gCustomer = Customer.GetCustomerById(newTask.CustId, dbConnection);
                                if (gCustomer.SiteId > 0 && gCustomer.SiteId != aUser.SiteId)
                                {
                                    listy.Add("Selected group is from a different site than the specified customer");
                                    return listy;
                                }
                            }
                            if (newTask.ContractId > 0)
                            {
                                var gCustomer = ContractInfo.GetContractInfoById(newTask.ContractId, dbConnection);
                                if (gCustomer.SiteId > 0 && gCustomer.SiteId != aUser.SiteId)
                                {
                                    listy.Add("Selected group is from a different site than the specified contract");
                                    return listy;
                                }
                            }
                            if (newTask.ProjectId > 0)
                            {
                                var gCustomer = Project.GetProjectById(newTask.ProjectId, dbConnection);
                                if (gCustomer.SiteId > 0 && gCustomer.SiteId != aUser.SiteId)
                                {
                                    listy.Add("Selected group is from a different site than the specified project");
                                    return listy;
                                }
                            }
                        }
                    }
                    assigneeName = newTask.AssigneeName;

                    newTask.CreateDate = CommonUtility.getDTNow();
                    newTask.CreatedBy = userinfo.Username;
                    newTask.ManagerName = userinfo.Username;
                    newTask.ManagerId = loggedInManager.ID;
                    newTask.TaskTypeId = Convert.ToInt32(tasktype);
                    newTask.IsDeleted = false;
                    newTask.UpdatedDate = CommonUtility.getDTNow();
                    if (!string.IsNullOrEmpty(longi))
                        newTask.Longitude = Convert.ToDouble(longi);
                    else
                        newTask.Longitude = 0;
                    if (!string.IsNullOrEmpty(lati))
                        newTask.Latitude = Convert.ToDouble(lati);
                    else
                        newTask.Latitude = 0;

                    newTask.StartDate = selectedTaskStartDateTime;
                    newTask.EndDate = CommonUtility.getDTNow();

                    if (priority == TaskPiority.Severe.ToString())
                        newTask.Priority = (int)TaskPiority.Severe;
                    else if (priority == TaskPiority.High.ToString())
                        newTask.Priority = (int)TaskPiority.High;
                    else if (priority == TaskPiority.Medium.ToString())
                        newTask.Priority = (int)TaskPiority.Medium;
                    else if (priority == TaskPiority.Low.ToString())
                        newTask.Priority = (int)TaskPiority.Low;

                    newTask.IsTaskTemplate = false;
                    newTask.TemplateCheckListId = Convert.ToInt32(checklistid);

                    if (calculatedDates != null)
                    {
                        if (Convert.ToBoolean(recurring))
                        {
                            newTask.RecurringParentId = 0;
                            newTask.IsRecurring = true;
                            newTask.RejectionNotes = lbrecurring;
                        }
                    }
                    newTask.SiteId = userinfo.SiteId;

                    if (userinfo.RoleId == (int)Role.Regional)
                    {
                        if (assignType == TaskAssigneeType.User.ToString())
                        {
                            var uinfo = Users.GetUserById(Convert.ToInt32(assignId), dbConnection);
                            newTask.SiteId = uinfo.SiteId;
                        }
                        else if (assignType == TaskAssigneeType.Group.ToString())
                        {
                            var ginfo = Group.GetGroupById(Convert.ToInt32(assignId), dbConnection);
                            newTask.SiteId = ginfo.SiteId;
                        }
                    }
                    newTask.CustomerId = userinfo.CustomerInfoId;

                    if (!string.IsNullOrEmpty(linkId))
                    {
                        if (CommonUtility.isNumeric(linkId))
                        {
                            newTask.TaskLinkId = Convert.ToInt32(linkId);
                            var gTask = UserTask.GetTaskById(newTask.TaskLinkId, dbConnection);
                            if (gTask != null)
                            {
                                gTask.Status = (int)TaskStatus.InProgress;
                                gTask.FollowUp = false;
                                gTask.UpdatedBy = userinfo.Username;
                                gTask.UpdatedDate = CommonUtility.getDTNow();
                                UserTask.InsertorUpdateTask(gTask, dbConnection, dbConnectionAudit, true);
                            }
                        }
                    }
                    if (!string.IsNullOrEmpty(ticketId))
                    {
                        if (CommonUtility.isNumeric(ticketId))
                        {
                            newTask.IncidentId = Convert.ToInt32(ticketId);
                        }
                    }


                    if (!string.IsNullOrEmpty(issig))
                    {
                        //isSign = Convert.ToBoolean(issig);

                        if (issig.ToLower() == "true")
                            newTask.IsSignature = true;
                    }

                    var recurrenceParentTaskId = UserTask.InsertorUpdateTask(newTask, dbConnection, dbConnectionAudit, true);

                    if (newTask.IsSignature)
                        UserTask.UpdateTaskSignature(recurrenceParentTaskId, true, dbConnection);

                    var tskhistory = new TaskEventHistory();
                    tskhistory.Action = (int)TaskAction.Pending;
                    tskhistory.CreatedBy = userinfo.Username;
                    tskhistory.CreatedDate = CommonUtility.getDTNow();
                    tskhistory.TaskId = recurrenceParentTaskId;
                    tskhistory.Remarks = assigneeName;
                    tskhistory.SiteId = userinfo.SiteId;
                    tskhistory.CustomerId = userinfo.CustomerInfoId;
                    TaskEventHistory.InsertTaskEventHistory(tskhistory, dbConnection, dbConnectionAudit, true);

                    var allTemplateCheckListItems = TemplateCheckListItem.GetAllTemplateCheckListItems(dbConnection);
                    List<TemplateCheckListItem> selectedTemplateCheckListItems = null;

                    if (allTemplateCheckListItems != null && allTemplateCheckListItems.Count > 0)
                    {
                        selectedTemplateCheckListItems = allTemplateCheckListItems.Where(i => i.ParentCheckListId == newTask.TemplateCheckListId).ToList();
                        foreach (var templateCheckListItem in selectedTemplateCheckListItems)
                        {
                            var taskCheckList = new TaskCheckList();
                            taskCheckList.IsChecked = templateCheckListItem.IsChecked;
                            taskCheckList.TaskId = recurrenceParentTaskId;
                            taskCheckList.UpdatedDate = CommonUtility.getDTNow();
                            taskCheckList.CreatedDate = CommonUtility.getDTNow();
                            taskCheckList.CreatedBy = userinfo.Username;
                            taskCheckList.TemplateCheckListItemId = templateCheckListItem.Id;
                            taskCheckList.SiteId = userinfo.SiteId;
                            taskCheckList.CustomerId = userinfo.CustomerInfoId;
                            TaskCheckList.InsertorUpdateTaskCheckListItem(taskCheckList, dbConnection, dbConnectionAudit, true);
                        }
                    }

                    if (calculatedDates != null && Convert.ToBoolean(recurring))
                    {
                        var recurringTask = newTask;

                        /* run your code here */
                        foreach (var rday in calculatedDates)
                        {
                            if ((recuringVal.Equals("Daily") && Convert.ToDateTime(rday).DayOfWeek.ToString() != "Friday" &&
                                    Convert.ToDateTime(rday).DayOfWeek.ToString() != "Saturday")
                                || recuringVal.Equals("Weekly") || recuringVal.Equals("Monthly"))
                            {
                                recurringTask.StartDate = Convert.ToDateTime(rday);
                                recurringTask.AccountName = "";
                                recurringTask.Id = 0;
                                recurringTask.IsRecurring = true;
                                recurringTask.RecurringParentId = recurrenceParentTaskId;
                                recurringTask.RejectionNotes = "";
                                recurringTask.CustomerId = userinfo.CustomerInfoId;
                                //recurringTask.SiteId = userinfo.SiteId;
                                var recurringTaskId = UserTask.InsertorUpdateTask(recurringTask, dbConnection, dbConnectionAudit, true);

                                if (recurringTask.IsSignature)
                                    UserTask.UpdateTaskSignature(recurringTaskId, true, dbConnection);

                                var rectskhistory = new TaskEventHistory();
                                rectskhistory.Action = (int)TaskAction.Pending;
                                rectskhistory.CreatedBy = userinfo.Username;
                                rectskhistory.CreatedDate = CommonUtility.getDTNow();
                                rectskhistory.TaskId = recurringTaskId;
                                rectskhistory.Remarks = assigneeName;
                                rectskhistory.SiteId = userinfo.SiteId;
                                tskhistory.CustomerId = userinfo.CustomerInfoId;
                                TaskEventHistory.InsertTaskEventHistory(rectskhistory, dbConnection, dbConnectionAudit, true);

                                foreach (var templateCheckListItem in selectedTemplateCheckListItems)
                                {
                                    var taskCheckList = new TaskCheckList();
                                    taskCheckList.IsChecked = templateCheckListItem.IsChecked;
                                    taskCheckList.TaskId = recurringTaskId;
                                    taskCheckList.UpdatedDate = CommonUtility.getDTNow();
                                    taskCheckList.CreatedDate = CommonUtility.getDTNow();
                                    taskCheckList.CreatedBy = userinfo.Username;
                                    taskCheckList.TemplateCheckListItemId = templateCheckListItem.Id;
                                    taskCheckList.SiteId = userinfo.SiteId;
                                    taskCheckList.CustomerId = userinfo.CustomerInfoId;
                                    TaskCheckList.InsertorUpdateTaskCheckListItem(taskCheckList, dbConnection, dbConnectionAudit, true);
                                }
                            }
                        }
                    }

                    if (DateTime.Parse(startdate).Date == CommonUtility.getDTNow().Date)
                    {

                        if (newTask.AssigneeType == (int)TaskAssigneeType.Group)
                        {
                            var thread = new Thread(() => CommonUtility.sendPushNotificationGroup(newTask.AssigneeId, "Task: " + newTask.Name, dbConnection));
                            thread.Start();
                        }

                        else if (newTask.AssigneeType == (int)TaskAssigneeType.User)
                        {
                            var pushDevInfo = PushNotificationDevice.GetPushNotificationDeviceByUsername(newTask.AssigneeName, dbConnection);
                            if (!string.IsNullOrEmpty(pushDevInfo.DeviceToken) && pushDevInfo.DeviceType == (int)PushNotificationDeviceType.Apple)
                            {
                                Thread thread = new Thread(() => PushNotificationClient.SendtoAppleUserNotification("Task: " + newTask.Name, newTask.AssigneeName, dbConnection));

                                thread.Start();
                            }
                            else if (!string.IsNullOrEmpty(pushDevInfo.DeviceToken) && pushDevInfo.DeviceType == (int)PushNotificationDeviceType.Android)
                            {
                                Thread thread = new Thread(() => PushNotificationAndroid.SendNotification(newTask.AssigneeName, CommonUtility.androidpushApplicationID, CommonUtility.androidpushSenderId, pushDevInfo.DeviceToken, "Task", newTask.Name, dbConnection, (int)PushNotificationDevice.PushNotificationType.TaskNotification));

                                thread.Start();
                            }
                        }
                    }
                    listy.Add("SUCCESS");
                    listy.Add(newTask.AssigneeName);
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("Ticketing", "InsertTaskOnly", ex, dbConnection, userinfo.SiteId);
                    listy.Add(ex.Message);
                }


            }
            return listy;
        }

        [WebMethod]
        public static List<string> getTableRowDataTask(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var customData = UserTask.GetTaskById(Convert.ToInt32(id), dbConnection);

                if (customData != null)
                {
                    listy.Add(customData.CustomerUName);
                    listy.Add(customData.CreateDate.Value.AddHours(userinfo.TimeZone).ToString());
                    listy.Add(customData.ACustomerUName);
                    listy.Add(customData.StatusDescription);
                    var geolocation = ReverseGeocode.RetrieveFormatedAddress(customData.Latitude.ToString(), customData.Longitude.ToString(), getClientLic);
                    listy.Add(geolocation);
                    listy.Add(customData.Name);
                    listy.Add(customData.Name);
                    listy.Add(customData.Name);
                    listy.Add(customData.Description);
                    listy.Add(customData.Notes);
                    listy.Add(customData.Name);
                    listy.Add(customData.StartDate.Value.AddHours(userinfo.TimeZone).ToString());
                    listy.Add(customData.CheckListNotes);
                    listy.Add("circle-point " + CommonUtility.getImgStatus(customData.StatusDescription));
                    var parentTasks = TemplateCheckList.GetAllTemplateCheckListById(customData.TemplateCheckListId.ToString(), dbConnection);
                    if (parentTasks != null)
                        listy.Add(parentTasks.Name);
                    else
                        listy.Add("None");

                    listy.Add(customData.TaskTypeName);

                    if (customData.IncidentId > 0)
                    {
                        var getCus = CustomEvent.GetCustomEventById(customData.IncidentId, dbConnection);
                        if (getCus != null)
                        {
                            if (getCus.EventType == CustomEvent.EventTypes.MobileHotEvent)
                            {
                                var mobEv = MobileHotEvent.GetMobileHotEventByGuid(getCus.Identifier, dbConnection);
                                if (mobEv != null)
                                {
                                    if (string.IsNullOrEmpty(mobEv.EventTypeName))
                                    {

                                    }
                                    else
                                        getCus.Name = mobEv.EventTypeName;
                                }
                            }
                            else if (getCus.EventType == CustomEvent.EventTypes.HotEvent || getCus.EventType == CustomEvent.EventTypes.Request)
                            {
                                var reqEv = HotEvent.GetHotEventById(getCus.Identifier, dbConnection);
                                if (reqEv != null)
                                {
                                    if (CommonUtility.getPCName(reqEv) != "MIMS MOBILE")
                                        getCus.UserName = CommonUtility.getPCName(reqEv);

                                    if (getCus.EventType == CustomEvent.EventTypes.Request)
                                    {
                                        var splitName = reqEv.Name.Split('^');
                                        if (splitName.Length > 1)
                                            getCus.Name = splitName[1];
                                    }
                                }
                            }
                            else if (getCus.EventType == CustomEvent.EventTypes.DriverOffence)
                            {
                                var doffence = DriverOffence.GetDriverOffenceById(getCus.Identifier, dbConnection);
                                if (doffence != null)
                                {
                                    getCus.Name = doffence.OffenceCategory;
                                }
                            }
                            listy.Add(getCus.Name + "|" + customData.IncidentId);


                        }
                        else
                        {
                            listy.Add("None");
                        }
                    }
                    else
                    {
                        if (customData.TaskLinkId > 0)
                        {
                            var gt = UserTask.GetTaskById(customData.TaskLinkId, dbConnection);
                            if (gt != null)
                            {
                                if (gt.IncidentId > 0)
                                {
                                    var getCus2 = CustomEvent.GetCustomEventById(customData.IncidentId, dbConnection);
                                    if (getCus2 != null)
                                    {
                                        if (getCus2.EventType == CustomEvent.EventTypes.MobileHotEvent)
                                        {
                                            var mobEv = MobileHotEvent.GetMobileHotEventByGuid(getCus2.Identifier, dbConnection);
                                            if (mobEv != null)
                                            {
                                                if (string.IsNullOrEmpty(mobEv.EventTypeName))
                                                {

                                                }
                                                else
                                                    getCus2.Name = mobEv.EventTypeName;
                                            }
                                        }
                                        else if (getCus2.EventType == CustomEvent.EventTypes.HotEvent || getCus2.EventType == CustomEvent.EventTypes.Request)
                                        {
                                            var reqEv = HotEvent.GetHotEventById(getCus2.Identifier, dbConnection);
                                            if (reqEv != null)
                                            {
                                                if (CommonUtility.getPCName(reqEv) != "MIMS MOBILE")
                                                    getCus2.UserName = CommonUtility.getPCName(reqEv);

                                                if (getCus2.EventType == CustomEvent.EventTypes.Request)
                                                {
                                                    var splitName = reqEv.Name.Split('^');
                                                    if (splitName.Length > 1)
                                                        getCus2.Name = splitName[1];
                                                }
                                            }
                                        }
                                        else if (getCus2.EventType == CustomEvent.EventTypes.DriverOffence)
                                        {
                                            var doffence = DriverOffence.GetDriverOffenceById(getCus2.Identifier, dbConnection);
                                            if (doffence != null)
                                            {
                                                getCus2.Name = doffence.OffenceCategory;
                                            }
                                        }
                                        listy.Add(getCus2.Name + "|" + customData.IncidentId);
                                    }
                                    else
                                    {
                                        listy.Add("None");
                                    }
                                }
                                else
                                {
                                    listy.Add("None");
                                }
                            }
                            else
                            {
                                listy.Add("None");
                            }
                        }
                        else
                            listy.Add("None");
                    }
                    listy.Add(string.IsNullOrEmpty(customData.ClientName) ? "N/A" : customData.ClientName);
                    listy.Add(string.IsNullOrEmpty(customData.ProjectName) ? "N/A" : customData.ProjectName);
                    listy.Add(string.IsNullOrEmpty(customData.ContractName) ? "N/A" : customData.ContractName);

                }
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("Incident", "getTableRowDataTask", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static List<string> getTaskHistoryData(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var eventData = UserTask.GetTaskById(Convert.ToInt32(id), dbConnection);//EventHistory.GetEventHistoryByEventId(Convert.ToInt32(id), dbConnection); //new List<CustomEvent>();
                var taskeventhistory = TaskEventHistory.GetTaskEventHistoryByTaskId(eventData.Id, dbConnection);

                var tasklinks = UserTask.GetAllTasksByTaskLinkId(Convert.ToInt32(id), dbConnection);
                if (tasklinks.Count > 0)
                {
                    foreach (var link in tasklinks)
                    {
                        var forlink = TaskEventHistory.GetTaskEventHistoryByTaskId(link.Id, dbConnection);
                        foreach (var forl in forlink)
                        {
                            forl.isSubTask = true;
                            taskeventhistory.Add(forl);
                        }
                    }
                    taskeventhistory = taskeventhistory.OrderByDescending(i => i.CreatedDate).ToList();
                }

                var completedBy = string.Empty;
                var inprogressby = string.Empty;
                var acceptedData = string.Empty;
                var rejectedData = string.Empty;
                var assignedData = string.Empty;
                foreach (var task in taskeventhistory)
                {
                    var taskn = "Task";
                    if (task.isSubTask)
                    {
                        taskn = "Sub-task";
                    }
                    if (eventData.AssigneeType == (int)TaskAssigneeType.Group)
                    {
                        if (task.Action == (int)TaskAction.Update)
                        {
                            acceptedData = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                                + "</p><p><span class='red-color'>" + task.CustomerUName + "</span> updated<span class='red-color'>" + taskn
                                + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                            listy.Add(acceptedData);
                        }
                    }

                    if (task.Action == (int)TaskAction.Complete)
                    {
                        completedBy = task.CustomerUName;
                        var compLocation = string.Empty;
                        var geoLoc = ReverseGeocode.RetrieveFormatedAddress(eventData.EndLatitude.ToString(), eventData.EndLongitude.ToString(), getClientLic);
                        if (!string.IsNullOrEmpty(geoLoc))
                            compLocation = " at " + geoLoc;
                        var compInfo = "completed";
                        var compstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                            + "</p><p><span class='red-color'>" + completedBy + "</span>" + compInfo + "<span class='red-color'>" + taskn
                            + "</span>" + compLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(compstring);
                    }
                    else if (task.Action == (int)TaskAction.InProgress)
                    {
                        inprogressby = task.CustomerUName;
                        var pendingLocation = string.Empty;
                        var geoLoc2 = ReverseGeocode.RetrieveFormatedAddress(eventData.StartLatitude.ToString(), eventData.StartLongitude.ToString(), getClientLic);
                        if (!string.IsNullOrEmpty(geoLoc2))
                            pendingLocation = " at " + geoLoc2;
                        var pendingInfo = "started";
                        var returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                            + "</p><p><span class='red-color'>" + inprogressby + "</span>" + pendingInfo + "<span class='red-color'>" + taskn
                            + "</span>" + pendingLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (task.Action == (int)TaskAction.Accepted)
                    {
                        acceptedData = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                            + "</p><p><span class='red-color'>" + task.CustomerUName + "</span> accepted<span class='red-color'>" + taskn
                            + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(acceptedData);
                    }
                    else if (task.Action == (int)TaskAction.Rejected)
                    {
                        rejectedData = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                            + "</p><p><span class='red-color'>" + task.CustomerUName + "</span> rejected<span class='red-color'>" + taskn
                            + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(rejectedData);
                    }
                    else if (task.Action == (int)TaskAction.Assigned)
                    {
                        assignedData = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                            + "</p><p><span class='red-color'>" + task.CustomerUName + "</span> assigned<span class='red-color'>" + taskn
                            + "</span> to " + task.ACustomerUName + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(assignedData);
                    }
                    else if (task.Action == (int)TaskAction.Pending)
                    {
                        var incidentLocation = string.Empty;
                        var geoLoc3 = ReverseGeocode.RetrieveFormatedAddress(eventData.Latitude.ToString(), eventData.Longitude.ToString(), getClientLic);
                        if (!string.IsNullOrEmpty(geoLoc3))
                            incidentLocation = " at " + geoLoc3;
                        var actioninfo = "created";

                        var gUser = Users.GetUserByName(eventData.CreatedBy, dbConnection);

                        var dataString = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                            + "</p><p><span class='red-color'>" + gUser.CustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + taskn
                            + "</span>for " + task.ACustomerUName + " " + incidentLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(dataString);
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Tasks", "getTaskEventHistoryData", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static string taskgetAttachmentDataIcons(int id, string uname)
        {
            var listy = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                var attachments = UserTaskAttachment.GetTaskAttachmentsByTaskId(Convert.ToInt32(id), dbConnection);
                var i = 1;
                if (attachments.Count > 0)
                {
                    foreach (var item in attachments)
                    {
                        if (!string.IsNullOrEmpty(item.DocumentPath))
                        {
                            if (VideoExtensions.Contains(System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant()))
                            {
                                var retstring = "<img src='../images/VLCMediaPlayer1.png' data-toggle='tab' onclick='play(" + i + ")' data-target='#video-" + i + "-tab'/>";
                                listy += retstring;
                            }
                            else
                            {
                                //var mimssettings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                                //var imgstring = String.Format(mimssettings.MIMSMobileAddress + "/Uploads/Tasks/" + id + "/{0}", System.IO.Path.GetFileName(item.DocumentPath));
                                var retstring = "<img src='" + item.DocumentPath + "' data-toggle='tab' data-target='#image-" + i + "-tab'/>";
                                listy += retstring;
                            }
                            i++;
                        }
                    }
                }
            }
            catch (Exception er)
            {
                MIMSLog.MIMSLogSave("Incident", "taskgetAttachmentDataIcons", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static List<string> taskgetAttachmentData(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var attachments = UserTaskAttachment.GetTaskAttachmentsByTaskId(Convert.ToInt32(id), dbConnection);
                var i = 1;
                if (attachments.Count > 0)
                {
                    foreach (var item in attachments)
                    {
                        if (!string.IsNullOrEmpty(item.DocumentPath))
                        {
                            //var mimssettings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                            if (VideoExtensions.Contains(System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant()))
                            {
                                var retstring = "<video id='Video" + i + " width='100%' height='378px' muted controls ><source src='" + item.DocumentPath + "' /></video>";
                                listy.Add(retstring);
                            }
                            else if (System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant() == ".PDF")
                            {

                            }
                            else
                            {
                                //var imgstring = String.Format(mimssettings.MIMSMobileAddress + "/Uploads/Tasks/" + id + "/{0}", System.IO.Path.GetFileName(item.DocumentPath));
                                var retstring = "<img onclick='rotateMe(this);' src='" + item.DocumentPath + "' class='resized-filled-image' onload='loadMe(this);'/>";
                                listy.Add(retstring);
                            }
                            i++;
                        }
                    }
                }
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("Incident", "taskgetAttachmentData", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static string taskgetAttachmentDataTab(int id, string uname)
        {
            var listy = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                var attachments = UserTaskAttachment.GetTaskAttachmentsByTaskId(Convert.ToInt32(id), dbConnection);
                listy += "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#tasklocation-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-map-marker fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Incident Location</p></div></div></div>";
                var i = 1;
                if (attachments.Count > 0)
                {
                    foreach (var item in attachments)
                    {
                        if (!string.IsNullOrEmpty(item.DocumentPath))
                        {
                            if (VideoExtensions.Contains(System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant()))
                            {
                                var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' onclick='play(" + i + ")' data-target='#video-" + i + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-play-circle-o fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div></div></div>";
                                listy += retstring;
                            }
                            else if (System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant() == ".PDF")
                            {

                            }
                            else
                            {
                                var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-" + i + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div></div></div>";
                                listy += retstring;
                            }
                            i++;
                        }
                    }
                }
            }
            catch (Exception er)
            {
                MIMSLog.MIMSLogSave("Incident", "taskgetAttachmentDataTab", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> taskgetChecklistData(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var chklisty = new List<string>();
                var isAllChecked = true;
                var sessions = TaskCheckList.GetTaskCheckListItemsByTaskId(Convert.ToInt32(id), dbConnection);
                foreach (var item in sessions)
                {
                    var stringCheck = string.Empty;
                    if (item.IsChecked)
                        stringCheck = "Checked";
                    else
                    {
                        stringCheck = "Unchecked";
                        isAllChecked = false;
                    }
                    if (item.CheckListItemType == 4 || item.CheckListItemType == 5)
                    {
                        chklisty.Add(item.Name + "|" + stringCheck + "|True");
                    }
                    else if (item.CheckListItemType == 3)
                    {
                        if (!string.IsNullOrEmpty(item.TemplateCheckListItemNote))
                            chklisty.Add(item.Name + "|" + stringCheck + "|3|" + item.TemplateCheckListItemNote);
                        else
                            chklisty.Add(item.Name + "|" + stringCheck + "|False");
                    }
                    else
                    {
                        chklisty.Add(item.Name + "|" + stringCheck + "|False");
                    }
                    if (item.ChildCheckList != null)
                    {
                        foreach (var child in item.ChildCheckList)
                        {
                            if (child.IsChecked)
                                stringCheck = "Checked";
                            else
                            {
                                stringCheck = "Unchecked";
                                isAllChecked = false;
                            }
                            chklisty.Add(child.Name + "|" + stringCheck + "|False");
                        }
                    }
                }
                listy.Add(isAllChecked.ToString());
                listy.AddRange(chklisty);
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Incident", "taskgetChecklistData", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static List<string> getChecklistNotesData(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var sessions = TaskCheckList.GetTaskCheckListItemsByTaskId(Convert.ToInt32(id), dbConnection);
                foreach (var item in sessions)
                {
                    if (item.CheckListItemType == 5)
                    {
                        if (item.ChildCheckList != null)
                        {
                            foreach (var child in item.ChildCheckList)
                            {
                                if (!child.IsChecked)
                                    listy.Add(child.Name + "|" + child.TemplateCheckListItemNote);
                            }
                        }
                    }
                    else
                    {
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Incidents", "getChecklistNotesData", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getCanvasNotesData(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }
                var sessions = UserTaskAttachment.GetTaskAttachmentsByTaskId(Convert.ToInt32(id), dbConnection);
                var count = 1;
                foreach (var item in sessions)
                {
                    if (!string.IsNullOrEmpty(item.DocumentPath))
                    {
                        if (item.ImageNote != "Task Attachment" && !string.IsNullOrEmpty(item.ImageNote))
                        {
                            listy.Add("Attachment " + count + "|" + item.ImageNote);
                        }
                        count++;    //listy.Add(child.Name + "|" + child.TemplateCheckListItemNote);
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Incidents", "getCanvasNotesData", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static string getTaskLocationData(int id, string uname)
        {
            var json = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                if (id > 0)
                {
                    var item = UserTask.GetTaskById(Convert.ToInt32(id), dbConnection);
                    json += "[";
                    if (getClientLic != null)
                    {
                        if (getClientLic.isLocation)
                        {
                            if (item != null)
                            {
                                if (item.StatusDescription == "Pending")
                                {
                                    json += "{ \"Username\" : \"" + item.StatusDescription + "\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                                }
                                else if (item.StatusDescription == "InProgress")
                                {
                                    if (item.StartLatitude > 0 && item.StartLongitude > 0)
                                        json += "{ \"Username\" : \"" + item.StatusDescription + "\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.StartLongitude.ToString() + "\",\"Lat\" : \"" + item.StartLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                                    json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                                }
                                else if (item.StatusDescription == "Completed" || item.StatusDescription == "Accepted")
                                {
                                    if (item.EndLatitude > 0 && item.EndLongitude > 0)
                                        json += "{ \"Username\" : \"" + item.StatusDescription + "\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.EndLongitude.ToString() + "\",\"Lat\" : \"" + item.EndLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"GREEN\",\"Logs\" : \"Retrieve\"},";

                                    if (item.StartLatitude > 0 && item.StartLongitude > 0)
                                        json += "{ \"Username\" : \"InProgress\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.StartLongitude.ToString() + "\",\"Lat\" : \"" + item.StartLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                                    json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                                }
                                //else
                                //{
                                //    json += "{ \"Username\" : \"" + item.StatusDescription + "\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                                //}
                            }
                        }
                    }
                    json = json.Substring(0, json.Length - 1);
                    json += "]";
                }
            }
            catch (Exception er)
            {
                MIMSLog.MIMSLogSave("Incident", "getTaskLocationData", er, dbConnection, userinfo.SiteId);
            }
            return json;
        }

        [WebMethod]
        public static string attachFileToTicket(int id, string filepath, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var json = string.Empty;
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }
                var cusEv = CustomEvent.GetCustomEventById(id, dbConnection);
                if (cusEv != null)
                {
                    var newObj = new MobileHotEventAttachment();

                    var newimgPath = filepath.Replace('|', '\\');
                    if (File.Exists(newimgPath))
                    {
                        Stream fs = File.OpenRead(newimgPath);
                        var getFileName = Path.GetFileName(newimgPath);
                        getFileName = Guid.NewGuid().ToString().Split('-')[0] + "-" + getFileName;
                        var savestring = CommonUtility.CloudUploadFile(userinfo.CustomerInfoId, getFileName, fs);
                        if (savestring != "FAIL")
                        {
                            newObj.AttachmentPath = savestring;
                            newObj.AttachmentName = System.IO.Path.GetFileName(savestring);
                            //newObj.SiteId = cusEv.SiteId;
                            newObj.UpdatedBy = userinfo.Username;
                            newObj.UpdatedDate = CommonUtility.getDTNow();
                            newObj.CreatedBy = userinfo.Username;
                            newObj.CreatedDate = CommonUtility.getDTNow();
                            newObj.EventId = id;
                            newObj.IsPortal = true;
                            newObj.SiteId = userinfo.SiteId;
                            newObj.CustomerId = userinfo.CustomerInfoId;
                            MobileHotEventAttachment.InsertOrUpdateIncidentMobileAttachment(newObj, dbConnection, dbConnectionAudit, true);
                            json = "Successfully attached file";
                        }
                        else
                        {
                            MIMSLog.MIMSLogSave("Incident", "Failed to upload file to cloud.", new Exception(), dbConnection, userinfo.SiteId);
                            json = "Failed to upload file to cloud.";
                        }
                        if (fs != null)
                        {
                            fs.Close();
                        }
                        if (File.Exists(newimgPath))
                            File.Delete(newimgPath);
                    }
                    else
                    {
                        MIMSLog.MIMSLogSave("Incident", "File trying to upload doesn't exist.", new Exception(), dbConnection, userinfo.SiteId);
                        json = "File trying to upload doesn't exist.";
                    }
                }
                else
                    json = "Problem was faced trying to attach file.";
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Lost", "attachFileToTask", err, dbConnection, userinfo.SiteId);
                json = err.Message;
            }
            return json;
        }

        [WebMethod]
        public static string UpdateTicketStatus(int id, string status, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var json = string.Empty;
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }
                var cusEv = CustomEvent.GetCustomEventById(id, dbConnection);
                if (cusEv != null)
                {
                    if (!string.IsNullOrEmpty(status))
                    {
                        if (status.ToUpper() == "START")
                        {


                            //cusEv.IncidentStatus = CommonUtility.getIncidentStatusValue("dispatch");
                            //cusEv.Handled = false;
                            ////incidentinfo.SiteId = userinfo.SiteId;
                            //cusEv.SessionId = System.Web.HttpContext.Current.Session.SessionID;
                            //cusEv.Password = Encrypt.EncryptData(userinfo.Password, true, dbConnection);
                            //var returnV = CommonUtility.CreateIncident(cusEv);
                            //if (returnV != "")
                            //{
                            DriverOffence.UpdateStartDriverOffence(cusEv.Identifier, CommonUtility.getDTNow(), userinfo.Username, dbConnection);
                            var EventHistoryEntry = new EventHistory();
                            EventHistoryEntry.CreatedDate = CommonUtility.getDTNow();
                            EventHistoryEntry.CreatedBy = userinfo.Username;
                            EventHistoryEntry.EventId = id;
                            EventHistoryEntry.IncidentAction = (int)CustomEvent.IncidentActionStatus.Engage;
                            EventHistoryEntry.UserName = userinfo.Username;
                            //EventHistoryEntry.Remarks = ins;
                            EventHistoryEntry.SiteId = userinfo.SiteId;
                            EventHistoryEntry.CustomerId = userinfo.CustomerInfoId;
                            EventHistory.InsertEventHistory(EventHistoryEntry, dbConnection, dbConnectionAudit, true);
                            json = "Successfully started work on ticket";
                            //}
                        }
                        else if (status.ToUpper() == "END")
                        {
                            //cusEv.IncidentStatus = CommonUtility.getIncidentStatusValue("complete");
                            //cusEv.Handled = false;
                            ////incidentinfo.SiteId = userinfo.SiteId;
                            //cusEv.SessionId = System.Web.HttpContext.Current.Session.SessionID;
                            //cusEv.Password = Encrypt.EncryptData(userinfo.Password, true, dbConnection);
                            //var returnV = CommonUtility.CreateIncident(cusEv);
                            //if (returnV != "")
                            //{
                            DriverOffence.UpdateEndDriverOffence(cusEv.Identifier, CommonUtility.getDTNow(), userinfo.Username, dbConnection);
                            var EventHistoryEntry = new EventHistory();
                            EventHistoryEntry.CreatedDate = CommonUtility.getDTNow();
                            EventHistoryEntry.CreatedBy = userinfo.Username;
                            EventHistoryEntry.EventId = id;
                            EventHistoryEntry.IncidentAction = (int)CustomEvent.IncidentActionStatus.Complete;
                            EventHistoryEntry.UserName = userinfo.Username;
                            //EventHistoryEntry.Remarks = ins;
                            EventHistoryEntry.SiteId = userinfo.SiteId;
                            EventHistoryEntry.CustomerId = userinfo.CustomerInfoId;
                            EventHistory.InsertEventHistory(EventHistoryEntry, dbConnection, dbConnectionAudit, true);
                            json = "Successfully ended work on ticket";
                            //}
                        }
                    }
                }
                else
                    json = "Problem was faced trying to attach file.";
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Ticketing", "UpdateTicketStatus", err, dbConnection, userinfo.SiteId);
                json = err.Message;
            }
            return json;
        }

        [WebMethod]
        public static List<string> geTicketHistoryData(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var allEventHistory = EventHistory.GetEventHistoryByEventId(Convert.ToInt32(id), dbConnection); //new List<CustomEvent>();
                var alltasks = UserTask.GetAllTaskByIncidentId(Convert.ToInt32(id), dbConnection);
                if (alltasks.Count > 0)
                {
                    foreach (var item in alltasks)
                    {
                        var alltaskhistory = TaskEventHistory.GetTaskEventHistoryByTaskId(item.Id, dbConnection);
                        if (alltaskhistory.Count > 0)
                        {
                            foreach (var task in alltaskhistory)
                            {
                                if (task.Action != (int)TaskAction.Update)
                                {
                                    var evhistory = new EventHistory();

                                    evhistory.isTask = true;
                                    evhistory.TaskName = task.TaskName;
                                    if (task.Action == (int)TaskAction.Pending)
                                        evhistory.IncidentAction = (int)CustomEvent.IncidentActionStatus.Pending;
                                    else if (task.Action == (int)TaskAction.InProgress)
                                        evhistory.IncidentAction = (int)CustomEvent.IncidentActionStatus.Engage;
                                    else if (task.Action == (int)TaskAction.Complete)
                                        evhistory.IncidentAction = (int)CustomEvent.IncidentActionStatus.Complete;
                                    else if (task.Action == (int)TaskAction.Accepted)
                                        evhistory.IncidentAction = (int)CustomEvent.IncidentActionStatus.Resolve;
                                    else if (task.Action == (int)TaskAction.Rejected)
                                        evhistory.IncidentAction = (int)CustomEvent.IncidentActionStatus.Reject;

                                    evhistory.CustomerUName = task.CustomerUName;
                                    evhistory.ACustomerUName = task.CustomerUName;
                                    evhistory.CreatedDate = task.CreatedDate;
                                    evhistory.Latitude = task.Latitude;
                                    evhistory.Longtitude = task.Longtitude;
                                    allEventHistory.Add(evhistory);
                                }
                                if (item.AssigneeType == (int)TaskAssigneeType.Group)
                                {
                                    if (task.Action == (int)TaskAction.Update)
                                    {
                                        var evhistory = new EventHistory();
                                        evhistory.isTask = true;
                                        evhistory.TaskName = task.TaskName;
                                        evhistory.IncidentAction = (int)CustomEvent.IncidentActionStatus.Updated;
                                        evhistory.CustomerUName = task.CustomerUName;
                                        evhistory.ACustomerUName = task.CustomerUName;
                                        evhistory.CreatedDate = task.CreatedDate;
                                        evhistory.Latitude = task.Latitude;
                                        evhistory.Longtitude = task.Longtitude;
                                        allEventHistory.Add(evhistory);
                                    }
                                }
                            }
                        }
                    }
                }
                allEventHistory = allEventHistory.OrderByDescending(i => i.CreatedDate).ToList();
                foreach (var rem in allEventHistory)
                {
                    var remname = string.Empty;
                    if (rem.EventId > 0)
                    {
                        var gethotevent = CustomEvent.GetCustomEventById(rem.EventId, dbConnection);

                        if (gethotevent != null)
                            gethotevent.Name = "Ticket";

                        remname = gethotevent.Name;
                    }
                    if (rem.isTask)
                    {
                        if (!string.IsNullOrEmpty(rem.TaskName))
                            remname = rem.TaskName;
                        else
                            remname = "Task";

                    }
                    var actioninfo = string.Empty;
                    var returnstring = string.Empty;
                    var incidentLocation = string.Empty;
                    if (!string.IsNullOrEmpty(rem.Longtitude) && !string.IsNullOrEmpty(rem.Latitude))
                        incidentLocation = "from " + ReverseGeocode.RetrieveFormatedAddress(rem.Latitude.ToString(), rem.Longtitude.ToString(), getClientLic);

                    if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Pending)
                    {
                        actioninfo = "created ";
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.CustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + remname + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Complete)
                    {
                        actioninfo = "completed ";
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.ACustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + remname + "</span>" + incidentLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Dispatch)
                    {
                        actioninfo = "dispatched ";
                        incidentLocation = "to " + rem.ACustomerUName;
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.CustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + remname + "</span>" + incidentLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Resolve)
                    {
                        actioninfo = "resolved ";
                        incidentLocation = "";
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.CustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + remname + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Reject)
                    {
                        actioninfo = "rejected ";
                        incidentLocation = "";
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.CustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + remname + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Park)
                    {
                        actioninfo = "parked ";
                        incidentLocation = "";
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.CustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + remname + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Engage)
                    {
                        actioninfo = "started ";
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.ACustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + remname + "</span>" + incidentLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Release)
                    {
                        actioninfo = "released ";
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.ACustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + remname + "</span>" + incidentLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Arrived)
                    {
                        actioninfo = "arrived to incident ";

                        incidentLocation = "at " + ReverseGeocode.RetrieveFormatedAddress(rem.Latitude.ToString(), rem.Longtitude.ToString(), getClientLic);

                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.ACustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + remname + "</span>" + incidentLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Escalated)
                    {
                        actioninfo = "escalated ";
                        incidentLocation = "";
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.CustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + remname + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Updated)
                    {
                        actioninfo = "updated ";
                        incidentLocation = "";
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.CustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + remname + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                }
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("Ticketing", "geTicketHistoryData", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static List<string> getTicketRemarksData(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var projinfo = CustomEvent.GetCustomEventById(id, dbConnection);
                if (projinfo != null)
                {
                    var remarksList = CustomEventRemarks.GetCustomEventRemarksByEventId(id, dbConnection);


                    if (remarksList.Count > 0)
                    {
                        remarksList = remarksList.OrderByDescending(i => i.CreatedDate).ToList();
                        var count = 0;
                        foreach (var task in remarksList)
                        {
                            if (count == 3)
                            {
                                break;
                            }
                            count++;
                            var OGremarks = task.Remarks;
                            if (!string.IsNullOrEmpty(task.Remarks) && task.Remarks.Length > 50)
                            {
                                task.Remarks = task.Remarks.Remove(50);
                                task.Remarks = task.Remarks + "...";
                            }
                            var acceptedData = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + task.CustomerUName + " wrote :</span>" + task.Remarks + "</p></div><i class='fa fa-sticky-note absoult-center-left' style='color:#bbbbbb;margin-top:-1px;' onmouseover='style=&apos;cursor: pointer;margin-top:-1px;color:#bbbbbb;&apos;' onclick='showRemarks(&apos;" + OGremarks + "&apos;)'></i>";
                            listy.Add(acceptedData);
                        }
                        if (remarksList.Count > 3)
                        {
                            var seeall = "<h5><span  class='line-center' onmouseover='style=&apos;cursor: pointer;' onclick='showAllTicketingRemarks(&apos;" + id + "&apos;)'>SEE ALL</span></h5>";
                            listy.Add(seeall);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Ticketing", "getTicketRemarksData", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static List<string> getTicketRemarksData2(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var projinfo = CustomEvent.GetCustomEventById(id, dbConnection);
                if (projinfo != null)
                {
                    var remarksList = CustomEventRemarks.GetCustomEventRemarksByEventId(id, dbConnection);


                    if (remarksList.Count > 0)
                    {
                        remarksList = remarksList.OrderByDescending(i => i.CreatedDate).ToList();
                        foreach (var task in remarksList)
                        {
                            var OGremarks = task.Remarks;
                            var acceptedData = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + task.CustomerUName + " wrote :</span>" + task.Remarks + "</p></div><i class='fa fa-sticky-note absoult-center-left' style='color:#bbbbbb;margin-top:-1px;' onmouseover='style=&apos;cursor: pointer;margin-top:-1px;color:#bbbbbb;&apos;' onclick='showRemarks(&apos;" + OGremarks + "&apos;)'></i>";
                            listy.Add(acceptedData);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Ticketing", "getTicketRemarksData2", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static string addNewTicketingRemarks(int id, string notes, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var json = string.Empty;
            try
            {
                var projinfo = CustomEvent.GetCustomEventById(id, dbConnection);
                if (projinfo != null)
                {
                    var newRemarks = new CustomEventRemarks();
                    newRemarks.CreatedBy = userinfo.Username;
                    newRemarks.CreatedDate = CommonUtility.getDTNow();
                    newRemarks.EventId = id;
                    newRemarks.Remarks = notes;
                    newRemarks.UpdatedBy = userinfo.Username;
                    newRemarks.UpdatedDate = CommonUtility.getDTNow();
                    newRemarks.CustomerInfoId = userinfo.CustomerInfoId;
                    newRemarks.SiteId = userinfo.SiteId;
                    var retV = CustomEventRemarks.InsertOrUpdateCustomEventRemarks(newRemarks, dbConnection, dbConnectionAudit, true);
                    if (retV > 0)
                    {
                        json = "SUCCESS";
                    }
                    else
                    {
                        json = "Problem faced trying to save notes";
                    }
                }
                else
                {
                    json = "Problem faced trying to get ticket and save notes";
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Ticketing", "addNewTicketingRemarks", err, dbConnection, userinfo.SiteId);
                json = err.Message;
            }
            return json;
        }

        [WebMethod]
        public static List<OffenceType> getTicketTypeByCategory(int id, string uname)
        {
            var fullcollection = new List<OffenceType>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                if (userinfo != null)
                {
                    if (id > 0)
                    {
                        fullcollection.AddRange(OffenceType.GetAllOffenceTypeByOffenceCategoryId(id, dbConnection));
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Ticketing", "getTicketTypeByCategory", ex, dbConnection, userinfo.SiteId);
            }
            return fullcollection;
        }

        [WebMethod]
        public static List<Offence> getTicketItemByType(int id, string uname)
        {
            var fullcollection = new List<Offence>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                if (userinfo != null)
                {
                    if (id > 0)
                    {
                        fullcollection.AddRange(Offence.GetAllOffenceByOffenceTypeId(id, dbConnection));
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Ticketing", "getTicketItemByType", ex, dbConnection, userinfo.SiteId);
            }
            return fullcollection;
        }


        [WebMethod]
        public static string deleteAttachmentDataTicket(int id, string uname)
        {
            var listy = string.Empty;
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    var ret = Arrowlabs.Business.Layer.MobileHotEventAttachment.GetMobileHotEventAttachmentById(id, dbConnection);
                    if (ret != null)
                    {
                        ret = ret.Where(i => i.Id == id).ToList();
                        if (ret.Count > 0)
                        {
                            MobileHotEventAttachment.DeleteMobileHotEventAttachmentById(id, dbConnection);

                            SystemLogger.SaveSystemLog(dbConnectionAudit, "Attachment-" + id, id.ToString(), "MobileHotEventAttachment", userinfo, "Attachment was deleted on_" + CommonUtility.getDTNow());

                            CommonUtility.CloudDeleteFile(ret[0].AttachmentPath);

                            listy = "Successfully deleted entry";
                        }
                        else
                            listy = "Failed to delete entry";
                    }
                    else
                        listy = "Failed to delete entry";
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Tasks", "deleteAttachmentData", err, dbConnection, userinfo.SiteId);
                    listy = err.Message;
                }
                return listy;
            }
        }

        [WebMethod]
        public static string InsertTicketOnly(string name, string desc, string ticketCat,
            string ticketType, string[] ticketItems,
            string longi, string lati, string uname, string[] imgp, string contractid, string cusid)
        {
            string json = string.Empty;
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    var driverOffence = new Arrowlabs.Business.Layer.DriverOffence();
                    driverOffence.Identifier = Guid.NewGuid();
                    driverOffence.OffenceDate = CommonUtility.getDTNow().ToShortDateString();
                    driverOffence.OffenceTime = CommonUtility.getDTNow().ToShortTimeString();
                    driverOffence.UserName = userinfo.Username;
                    driverOffence.Comments = desc;

                    if (!string.IsNullOrEmpty(longi) && CommonUtility.isNumeric(longi))
                        driverOffence.Longitude = Convert.ToDouble(longi);
                    if (!string.IsNullOrEmpty(lati) && CommonUtility.isNumeric(lati))
                        driverOffence.Latitude = Convert.ToDouble(lati);

                    driverOffence.OffenceCategory = ticketCat;

                    if (!string.IsNullOrEmpty(contractid) && CommonUtility.isNumeric(contractid))
                    {
                        driverOffence.ContractId = Convert.ToInt32(contractid);
                    }

                    if (!string.IsNullOrEmpty(cusid) && CommonUtility.isNumeric(cusid))
                    {
                        driverOffence.CusId = Convert.ToInt32(cusid);
                    }


                    driverOffence.CreatedDate = CommonUtility.getDTNow();
                    driverOffence.Status = 0;

                    if (ticketType != "Select Type")
                        driverOffence.OffenceType = ticketType;

                    for (var i = 0; i < ticketItems.Length; i++)
                    {
                        if (!string.IsNullOrEmpty(ticketItems[i]))
                        {
                            if (ticketItems[i].Split('-').Length > 1)
                            {
                                var gOffence = Offence.GetOffenceById(Convert.ToInt32(ticketItems[i].Split('-')[1]), dbConnection);
                                if (i == 0)
                                    driverOffence.Offence1 = gOffence.OffenceDesc;
                                if (i == 1)
                                    driverOffence.Offence2 = gOffence.OffenceDesc;
                                if (i == 2)
                                    driverOffence.Offence3 = gOffence.OffenceDesc;
                                if (i == 3)
                                    driverOffence.Offence4 = gOffence.OffenceDesc;
                            }
                        }
                    }
                    driverOffence.SiteId = 0;//userinfo.SiteId;
                    driverOffence.CustomerId = 1;//userinfo.CustomerInfoId;
                    driverOffence.SessionId = System.Web.HttpContext.Current.Session.SessionID;
                    driverOffence.Password = Arrowlabs.Business.Layer.Encrypt.EncryptData(userinfo.Password, true, dbConnection);
                    var result = CommonUtility.CreateTicket(driverOffence);
                    if (result)
                    {
                        json = "SUCCESS";
                        if (userinfo.RoleId == (int)Role.CustomerUser)
                        {
                            driverOffence.CusId = userinfo.CustomerLinkId;
                            driverOffence.ContractId = userinfo.ContractLinkId;
                        }
                        DriverOffence.UpdateCustomerInfoForDriverOffence(driverOffence.Identifier, driverOffence.CusId, driverOffence.ContractId, dbConnection);

                        var cusEv = CustomEvent.GetAllCustomHotEventByIdentifier(driverOffence.Identifier, dbConnection);
                        if (cusEv != null)
                        {
                            var x = CommonUtility.sendEmail(userinfo.Username, userinfo.FirstName + " " + userinfo.LastName, "Ticket successfully created", cusEv.CustomerIncidentId, "MIMS Support Ticket", "Support");
                            
                            for (var i = 0; i < imgp.Length; i++)
                            {
                                if (!string.IsNullOrEmpty(imgp[i]))
                                {
                                    var newimgPath = imgp[i].Replace('|', '\\');
                                    if (File.Exists(newimgPath))
                                    {
                                        Stream fs = File.OpenRead(newimgPath);
                                        var getFileName = Path.GetFileName(newimgPath);
                                        getFileName = Guid.NewGuid().ToString().Split('-')[0] + "-" + getFileName;
                                        var savestring = CommonUtility.CloudUploadFile(userinfo.CustomerInfoId, getFileName, fs);
                                        if (savestring != "FAIL")
                                        {
                                            var newObj = new MobileHotEventAttachment();
                                            newObj.AttachmentPath = savestring;
                                            newObj.AttachmentName = System.IO.Path.GetFileName(savestring);
                                            //newObj.SiteId = cusEv.SiteId;
                                            newObj.UpdatedBy = userinfo.Username;
                                            newObj.UpdatedDate = CommonUtility.getDTNow();
                                            newObj.CreatedBy = userinfo.Username;
                                            newObj.CreatedDate = CommonUtility.getDTNow();
                                            newObj.EventId = cusEv.EventId;
                                            newObj.SiteId = userinfo.SiteId;
                                            newObj.IsPortal = true;
                                            newObj.CustomerId = userinfo.CustomerInfoId;
                                            MobileHotEventAttachment.InsertOrUpdateIncidentMobileAttachment(newObj, dbConnection, dbConnectionAudit, true);
                                        }
                                        else
                                        {
                                            MIMSLog.MIMSLogSave("Incident", "Failed to upload file to cloud.", new Exception(), dbConnection, userinfo.SiteId);
                                            json = "Failed to upload file to cloud.";
                                        }
                                        if (fs != null)
                                        {
                                            fs.Close();
                                        }
                                        if (File.Exists(newimgPath))
                                            File.Delete(newimgPath);
                                    }
                                    else
                                    {
                                        MIMSLog.MIMSLogSave("Incident", "File trying to upload doesn't exist.", new Exception(), dbConnection, userinfo.SiteId);
                                        json = "File trying to upload doesn't exist.";
                                    } 
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("Ticketing", "InsertTicketOnly", ex, dbConnection, userinfo.SiteId);
                    json = ex.Message;
                }
            }
            return json;
        }

        static string fpath;
        [WebMethod]
        public static string CreatePDFTicket(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    if (id > 0)
                    {
                        var cusevent = CustomEvent.GetCustomEventById(id, dbConnection);
                        if (cusevent != null)
                            return GeneratePDFTicket(cusevent, userinfo);
                    }
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("Ticket", "CreatePDFTicket", ex, dbConnection, userinfo.SiteId);
                }
                return "Failed to generate report!";
            }
        }

        private static string GeneratePDFTicket(CustomEvent cusevent, Users userinfo)
        {
            string fileName = string.Empty;
            try
            {
                iTextSharp.text.Document doc = new iTextSharp.text.Document(PageSize.LETTER, 25F, 25F, 50F, 25F);

                doc.SetMargins(25, 25, 65, 35);

                fileName = CommonUtility.getDTNow().Day.ToString() + "-" + CommonUtility.getDTNow().Month.ToString() + "-" + CommonUtility.getDTNow().Year.ToString() + "-" + CommonUtility.getDTNow().Hour.ToString() + "-" + CommonUtility.getDTNow().Minute.ToString() + "-" + CommonUtility.getDTNow().Second.ToString() + ".pdf";
                var path = fpath + fileName;

                PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(path, FileMode.Create));
                writer.PageEvent = new ITextEvents()
                {
                    cid = userinfo.CustomerInfoId
                };

                doc.Open();
                BaseFont f_cn = BaseFont.CreateFont(Environment.GetFolderPath(Environment.SpecialFolder.Fonts) + "\\verdana.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                BaseFont h_cn = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                BaseFont z_cn = BaseFont.CreateFont(BaseFont.ZAPFDINGBATS, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                Font times = new Font(f_cn, 12, Font.NORMAL, Color.BLACK);
                Font btimes = new Font(f_cn, 12, Font.BOLD, Color.BLACK);

                iTextSharp.text.Color arrowred = new iTextSharp.text.Color(162, 0, 46);

                Font eleventimes = new Font(f_cn, 11, Font.NORMAL, Color.BLACK);
                Font weleventimes = new Font(f_cn, 11, Font.NORMAL, Color.WHITE);

                Font gweb = new Font(z_cn, 11, Font.NORMAL, Color.GREEN);
                Font rweb = new Font(z_cn, 11, Font.NORMAL, arrowred);

                Font ueleventimes = new Font(f_cn, 11, Font.UNDERLINE, Color.BLACK);

                Font beleventimes = new Font(f_cn, 11, Font.BOLD, Color.BLACK);
                Font bredeleventimes = new Font(f_cn, 11, Font.BOLD, arrowred);
                Font redeleventimes = new Font(f_cn, 11, Font.NORMAL, arrowred);
                Font twelvetimes = new Font(f_cn, 12, Font.ITALIC, Color.BLACK);

                Font eleventimesI = new Font(f_cn, 11, Font.ITALIC, Color.BLACK);

                Font mbtimes = new Font(h_cn, 18, Font.BOLD, arrowred);
                Font cbtimes = new Font(h_cn, 13, Font.BOLD, arrowred);

                float[] columnWidthsH = new float[] { 30, 60, 10 };

                var mainheadertable = new PdfPTable(3);
                mainheadertable.DefaultCell.Border = Rectangle.NO_BORDER;
                mainheadertable.WidthPercentage = 100;
                mainheadertable.SetWidths(columnWidthsH);
                var newTRIlist = new List<TimelineReportItems>();

                var headerMain = new PdfPCell();
                if (userinfo.CustomerInfoId == 87 || userinfo.CustomerInfoId == 45)
                {
                    headerMain.AddElement(new Phrase("G4S TICKET REPORT", mbtimes));
                }
                else
                {
                    headerMain.AddElement(new Phrase("MIMS TICKET REPORT", mbtimes));
                }
                headerMain.PaddingBottom = 10;
                headerMain.HorizontalAlignment = 1;
                headerMain.Border = Rectangle.NO_BORDER;
                PdfPCell pdfCell1 = new PdfPCell();
                pdfCell1.Border = Rectangle.NO_BORDER;
                PdfPCell pdfCell2 = new PdfPCell();
                pdfCell2.Border = Rectangle.NO_BORDER;
                mainheadertable.AddCell(pdfCell1);
                mainheadertable.AddCell(headerMain);
                mainheadertable.AddCell(pdfCell2);

                var taskdataTable = new PdfPTable(2);
                var taskdataTableA = new PdfPTable(2);
                var taskdataTableB = new PdfPTable(2);
                taskdataTable.DefaultCell.Border = Rectangle.NO_BORDER;
                taskdataTable.WidthPercentage = 100;

                taskdataTableA.DefaultCell.Border = Rectangle.NO_BORDER;
                taskdataTableB.DefaultCell.Border = Rectangle.NO_BORDER;

                taskdataTableA.WidthPercentage = 100;
                taskdataTableB.WidthPercentage = 100;

                float[] columnWidths2575 = new float[] { 25, 75 };
                float[] columnWidths3070 = new float[] { 30, 70 };
                float[] columnWidths3565 = new float[] { 35, 65 };
                float[] columnWidths4060 = new float[] { 40, 60 };
                taskdataTableA.SetWidths(columnWidths4060);

                taskdataTableB.SetWidths(columnWidths4060);
                var driverOffence = DriverOffence.GetDriverOffenceById(cusevent.Identifier, dbConnection);
                if (driverOffence != null)
                {

                }
                //SIDE A

                if (driverOffence.CusId > 0)
                {
                    var header0 = new PdfPCell();
                    header0.AddElement(new Phrase("Account:", beleventimes));
                    header0.Border = Rectangle.NO_BORDER;
                    taskdataTableA.AddCell(header0);

                    var header0a = new PdfPCell();
                    header0a.AddElement(new Phrase(driverOffence.CusName, eleventimes));
                    header0a.Border = Rectangle.NO_BORDER;
                    taskdataTableA.AddCell(header0a);
                }
                if (driverOffence.ContractId > 0)
                {
                    var header00 = new PdfPCell();
                    header00.AddElement(new Phrase("Contract:", beleventimes));
                    header00.Border = Rectangle.NO_BORDER;
                    taskdataTableA.AddCell(header00);

                    var header00a = new PdfPCell();
                    header00a.AddElement(new Phrase(driverOffence.ContractName, eleventimes));
                    header00a.Border = Rectangle.NO_BORDER;
                    taskdataTableA.AddCell(header00a);
                }
                var header1 = new PdfPCell();
                header1.AddElement(new Phrase("Category:", beleventimes));
                header1.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header1);

                var header1a = new PdfPCell();
                header1a.AddElement(new Phrase(driverOffence.OffenceCategory, eleventimes));
                header1a.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header1a);

                var header2 = new PdfPCell();
                header2.AddElement(new Phrase("Type:", beleventimes));
                header2.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header2);

                if (string.IsNullOrEmpty(driverOffence.OffenceType))
                    driverOffence.OffenceType = "N/A";

                var header2a = new PdfPCell();
                header2a.AddElement(new Phrase(driverOffence.OffenceType, eleventimes));
                header2a.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header2a);

                var subNameTable = new PdfPTable(1);
                var header000 = new PdfPCell();
                header000.AddElement(new Phrase("Item:", beleventimes));
                header000.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header000);

                if (!string.IsNullOrEmpty(driverOffence.Offence1))
                {
                    var header000a = new PdfPCell();
                    header000a.AddElement(new Phrase(driverOffence.Offence1, eleventimes));
                    header000a.Border = Rectangle.NO_BORDER;
                    subNameTable.AddCell(header000a);
                }
                if (!string.IsNullOrEmpty(driverOffence.Offence2))
                {
                    var header000a = new PdfPCell();
                    header000a.AddElement(new Phrase(driverOffence.Offence2, eleventimes));
                    header000a.Border = Rectangle.NO_BORDER;
                    subNameTable.AddCell(header000a);
                }
                if (!string.IsNullOrEmpty(driverOffence.Offence3))
                {
                    var header000a = new PdfPCell();
                    header000a.AddElement(new Phrase(driverOffence.Offence3, eleventimes));
                    header000a.Border = Rectangle.NO_BORDER;
                    subNameTable.AddCell(header000a);
                }
                if (!string.IsNullOrEmpty(driverOffence.Offence4))
                {
                    var header000a = new PdfPCell();
                    header000a.AddElement(new Phrase(driverOffence.Offence4, eleventimes));
                    header000a.Border = Rectangle.NO_BORDER;
                    subNameTable.AddCell(header000a);
                }
                taskdataTableA.AddCell(subNameTable);

                var header3 = new PdfPCell();
                header3.AddElement(new Phrase("Created Date:", beleventimes));
                header3.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header3);

                var header3a = new PdfPCell();
                header3a.AddElement(new Phrase(cusevent.RecevieTime.Value.AddHours(userinfo.TimeZone).ToString(), eleventimes));
                header3a.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header3a);

                var header4 = new PdfPCell();
                header4.AddElement(new Phrase("Created by:", beleventimes));
                header4.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header4);

                var header4a = new PdfPCell();
                header4a.AddElement(new Phrase(cusevent.CustomerFullName, eleventimes));
                header4a.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header4a);

                var header8 = new PdfPCell();
                header8.AddElement(new Phrase("Description:", beleventimes));
                header8.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header8);

                var header8a = new PdfPCell();
                header8a.AddElement(new Phrase(driverOffence.Comments, eleventimes));
                header8a.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header8a);

                //SIDE B

                var headerB2 = new PdfPCell();
                headerB2.AddElement(new Phrase("Status:", beleventimes));
                headerB2.Border = Rectangle.NO_BORDER;
                taskdataTableB.AddCell(headerB2);
                if (cusevent.Handled)
                {
                    cusevent.StatusName = "Resolved";
                }
                else if (driverOffence.TicketStatus == (int)CustomEvent.IncidentActionStatus.Engage)
                {
                    cusevent.StatusName = "Inprogress";
                }
                else if (driverOffence.TicketStatus == (int)CustomEvent.IncidentActionStatus.Complete)
                {
                    cusevent.StatusName = "Completed";
                }

                var headerB2a = new PdfPCell();
                headerB2a.AddElement(new Phrase(cusevent.StatusName, eleventimes));
                headerB2a.Border = Rectangle.NO_BORDER;
                taskdataTableB.AddCell(headerB2a);

                var headerB1 = new PdfPCell();
                headerB1.AddElement(new Phrase("Business Unit:", beleventimes));
                headerB1.Border = Rectangle.NO_BORDER;
                taskdataTableB.AddCell(headerB1);


                var siteinfo = Arrowlabs.Business.Layer.Site.GetSiteById(cusevent.SiteId, dbConnection);
                if (siteinfo != null)
                {
                    var headerB1a = new PdfPCell();
                    headerB1a.AddElement(new Phrase(siteinfo.Name, eleventimes));
                    headerB1a.Border = Rectangle.NO_BORDER;
                    taskdataTableB.AddCell(headerB1a);
                }
                else
                {
                    var headerB1a = new PdfPCell();
                    headerB1a.AddElement(new Phrase("N/A", eleventimes));
                    headerB1a.Border = Rectangle.NO_BORDER;
                    taskdataTableB.AddCell(headerB1a);
                }

                if (cusevent.Handled)
                {
                    var headerB4 = new PdfPCell();
                    headerB4.AddElement(new Phrase("Resolved By:", beleventimes));
                    headerB4.Border = Rectangle.NO_BORDER;
                    headerB4.PaddingBottom = 20;
                    taskdataTableB.AddCell(headerB4);

                    var headerB4a = new PdfPCell();
                    headerB4a.AddElement(new Phrase(cusevent.CustomerUName, eleventimes));
                    headerB4a.Border = Rectangle.NO_BORDER;
                    taskdataTableB.AddCell(headerB4a);

                    var headerB5 = new PdfPCell();
                    headerB5.AddElement(new Phrase("Resolved Notes:", beleventimes));
                    headerB5.Border = Rectangle.NO_BORDER;
                    headerB5.PaddingBottom = 20;
                    taskdataTableB.AddCell(headerB5);

                    var evsHistory = EventHistory.GetEventHistoryByEventId(cusevent.EventId, dbConnection);
                    var isEsca = evsHistory.Where(x => x.IncidentAction == (int)CustomEvent.IncidentActionStatus.Resolve);
                    if (isEsca != null)
                    {
                        foreach (var esca in isEsca)
                        {
                            var headerB5a = new PdfPCell();
                            headerB5a.AddElement(new Phrase(esca.Remarks, eleventimes));
                            headerB5a.Border = Rectangle.NO_BORDER;
                            taskdataTableB.AddCell(headerB5a);
                            break;
                        }
                    }
                    else
                    {
                        var headerB5a = new PdfPCell();
                        headerB5a.AddElement(new Phrase("N/A", eleventimes));
                        headerB5a.Border = Rectangle.NO_BORDER;
                        taskdataTableB.AddCell(headerB5a);
                    }
                }

                taskdataTable.AddCell(taskdataTableA);
                taskdataTable.AddCell(taskdataTableB);

                var maptextdataTable = new PdfPTable(1);
                maptextdataTable.DefaultCell.Border = Rectangle.NO_BORDER;
                maptextdataTable.WidthPercentage = 100;

                var geolocation = ReverseGeocode.RetrieveFormatedAddress(cusevent.Latitude.ToString(), cusevent.Longtitude.ToString(), getClientLic);
                var geofence = GeofenceLocation.GetAllGeofenceLocationbyIncidentId(dbConnection, cusevent.EventId);
                if (string.IsNullOrEmpty(geolocation))
                {
                    if (geofence.Count > 0)
                    {
                        geolocation = ReverseGeocode.RetrieveFormatedAddress(geofence[0].Latitude.ToString(), geofence[0].Longitude.ToString(), getClientLic);
                        cusevent.Longtitude = geofence[0].Longitude.ToString();
                        cusevent.Latitude = geofence[0].Latitude.ToString();

                    }
                }

                if (string.IsNullOrEmpty(geolocation))
                    geolocation = "Not Specified";

                var mapheader = new PdfPCell();
                mapheader.AddElement(new Phrase("Location:" + geolocation, beleventimes));
                mapheader.Border = Rectangle.NO_BORDER;
                maptextdataTable.AddCell(mapheader);

                //var mapdataTable = new PdfPTable(2);
                //mapdataTable.DefaultCell.Border = Rectangle.NO_BORDER;
                //mapdataTable.WidthPercentage = 100;
                //float[] columnWidths7030 = new float[] { 70, 30 };
                //mapdataTable.SetWidths(columnWidths7030);
                //MAP

                //outer.AddCell(table);
                var latComp = string.Empty;
                var longComp = string.Empty;
                var latEng = string.Empty;
                var longEng = string.Empty;
                var arrived = string.Empty;
                var vargeopath = string.Empty; //&path=color:0xff0000ff|weight:5|25.09773,55.16316|25.0978512224338,55.1634863298386
                var tracebackpath = string.Empty; //&path=color:blue|weight:3|25.09773,55.16316|25.0978512224338,55.1634863298386


                var incidenteventhistory = EventHistory.GetEventHistoryByEventId(cusevent.EventId, dbConnection);
                var completedBy = string.Empty;
                var inprogressby = string.Empty;
                var acceptedData = string.Empty;
                var rejectedData = string.Empty;
                var assignedData = string.Empty;
                var utask = UserTask.GetAllTaskByIncidentId(cusevent.EventId, dbConnection);
                if (utask.Count > 0)
                {
                    foreach (var item in utask)
                    {
                        var alltaskhistory = TaskEventHistory.GetTaskEventHistoryByTaskId(item.Id, dbConnection);
                        if (alltaskhistory.Count > 0)
                        {
                            foreach (var task in alltaskhistory)
                            {
                                if (task.Action != (int)TaskAction.Update)
                                {
                                    var evhistory = new EventHistory();

                                    evhistory.isTask = true;
                                    evhistory.TaskName = task.TaskName;
                                    if (task.Action == (int)TaskAction.Pending)
                                        evhistory.IncidentAction = (int)CustomEvent.IncidentActionStatus.Pending;
                                    else if (task.Action == (int)TaskAction.InProgress)
                                        evhistory.IncidentAction = (int)CustomEvent.IncidentActionStatus.Engage;
                                    else if (task.Action == (int)TaskAction.Complete)
                                        evhistory.IncidentAction = (int)CustomEvent.IncidentActionStatus.Complete;
                                    else if (task.Action == (int)TaskAction.Accepted)
                                        evhistory.IncidentAction = (int)CustomEvent.IncidentActionStatus.Resolve;
                                    else if (task.Action == (int)TaskAction.Rejected)
                                        evhistory.IncidentAction = (int)CustomEvent.IncidentActionStatus.Reject;

                                    evhistory.CustomerFullName = task.CustomerFullName;
                                    evhistory.ACustomerFullName = task.CustomerFullName;
                                    evhistory.CreatedDate = task.CreatedDate;
                                    evhistory.Latitude = task.Latitude;
                                    evhistory.Longtitude = task.Longtitude;
                                    incidenteventhistory.Add(evhistory);
                                }
                                if (item.AssigneeType == (int)TaskAssigneeType.Group)
                                {
                                    if (task.Action == (int)TaskAction.Update)
                                    {
                                        var evhistory = new EventHistory();
                                        evhistory.isTask = true;
                                        evhistory.TaskName = task.TaskName;
                                        evhistory.IncidentAction = (int)CustomEvent.IncidentActionStatus.Updated;
                                        evhistory.CustomerFullName = task.CustomerFullName;
                                        evhistory.ACustomerFullName = task.CustomerFullName;
                                        evhistory.CreatedDate = task.CreatedDate;
                                        evhistory.Latitude = task.Latitude;
                                        evhistory.Longtitude = task.Longtitude;
                                        incidenteventhistory.Add(evhistory);
                                    }
                                }
                            }
                        }
                    }
                }
                incidenteventhistory = incidenteventhistory.OrderByDescending(i => i.CreatedDate).ToList();
                foreach (var rem in incidenteventhistory)
                {
                    var timelineItem = new TimelineReportItems();

                    //if (cusevent.EventType == CustomEvent.EventTypes.MobileHotEvent)
                    //{
                    //    cusevent.Name = cusevent.EventTypeName;
                    //}
                    //else if (cusevent.EventType == CustomEvent.EventTypes.Request)
                    //{
                    //    var splitName = cusevent.RequestName.Split('^');
                    //    if (splitName.Length > 1)
                    //    {
                    //        cusevent.Name = splitName[1];
                    //    }
                    //}
                    cusevent.Name = driverOffence.OffenceCategory;

                    if (rem.isTask)
                    {
                        if (!string.IsNullOrEmpty(rem.TaskName))
                            cusevent.Name = "task " + rem.TaskName;
                        else
                            cusevent.Name = "Task";

                    }


                    var actioninfo = string.Empty;
                    var returnstring = string.Empty;
                    var incidentLocation = string.Empty;
                    if (!string.IsNullOrEmpty(rem.Longtitude) && !string.IsNullOrEmpty(rem.Latitude))
                        incidentLocation = "from " + ReverseGeocode.RetrieveFormatedAddress(rem.Latitude.ToString(), rem.Longtitude.ToString(), getClientLic);

                    if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Pending)
                    {
                        actioninfo = "created ";
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.CustomerFullName + " " + actioninfo + " " + cusevent.Name;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Complete)
                    {
                        actioninfo = "completed ";
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.ACustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Dispatch)
                    {
                        actioninfo = "dispatched ";
                        incidentLocation = "to " + rem.ACustomerFullName;
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.CustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Resolve)
                    {
                        actioninfo = "resolved ";
                        incidentLocation = "";
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.CustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Reject)
                    {
                        actioninfo = "rejected ";
                        incidentLocation = "";
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.CustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Park)
                    {
                        actioninfo = "parked ";
                        incidentLocation = "";
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.CustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Engage)
                    {
                        actioninfo = "started ";
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.ACustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Release)
                    {
                        actioninfo = "released ";
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.ACustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Arrived)
                    {
                        actioninfo = "arrived to incident ";
                        incidentLocation = "at " + ReverseGeocode.RetrieveFormatedAddress(rem.Latitude.ToString(), rem.Longtitude.ToString(), getClientLic);
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.ACustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Escalated)
                    {
                        actioninfo = "escalated ";
                        incidentLocation = "";
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.CustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Updated)
                    {
                        actioninfo = "updated ";
                        incidentLocation = "";
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.CustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    newTRIlist.Add(timelineItem);
                }


                //if (geofence.Count > 0)
                //{
                //    vargeopath = "&path=color:0xff0000ff|weight:5|";
                //    foreach (var geo in geofence)
                //    {
                //        vargeopath += geo.Latitude + "," + geo.Longitude + "|";
                //    }
                //    vargeopath = vargeopath.Substring(0, vargeopath.Length - 1);
                //}
                //var assigneeList = new List<int>();
                //var notification = Arrowlabs.Business.Layer.Notification.GetAllNotificationsByIncidentId(cusevent.EventId, dbConnection);

                //var colorCount = 0;
                //var legendTraceback = new List<string>();
                //foreach (var noti in notification)
                //{
                //    var tbColor = string.Empty;
                //    if (assigneeList.IndexOf(noti.AssigneeId) != -1)
                //    {

                //    }
                //    else
                //    {
                //        tbColor = CommonUtility.getColorName(colorCount);
                //        assigneeList.Add(noti.AssigneeId);
                //        legendTraceback.Add(tbColor + " Path:  " + noti.ACustomerUName + " route");
                //        colorCount++;
                //    }
                //    var tracebackhistory = TraceBackHistory.GetTracBackHistoryByIncidentId(noti.Id, dbConnection);
                //    var tbCount = 0;
                //    var tbRemovedPointsCount = 0;

                //    var tbContinueCountStart = 0;
                //    if (tracebackhistory.Count > CommonUtility.tracebackreportmaxcount)
                //    {
                //        tbRemovedPointsCount = tracebackhistory.Count - CommonUtility.tracebackreportmaxcount;
                //        tbContinueCountStart = (CommonUtility.tracebackreportmaxcount / 2) + tbRemovedPointsCount;
                //    }
                //    if (tracebackhistory.Count > 0)
                //    {
                //        tracebackpath += "&path=color:blue|weight:5|";
                //        tracebackpath += latEng + "," + longEng + "|";
                //        foreach (var trace in tracebackhistory)
                //        {
                //            if (tracebackhistory.Count > CommonUtility.tracebackreportmaxcount)
                //            {
                //                tbCount++;
                //                if (tbCount < (CommonUtility.tracebackreportmaxcount / 2))
                //                    tracebackpath += trace.Latitude + "," + trace.Longitude + "|";
                //                else if (tbCount > tbContinueCountStart)
                //                    tracebackpath += trace.Latitude + "," + trace.Longitude + "|";
                //            }
                //            else
                //                tracebackpath += trace.Latitude + "," + trace.Longitude + "|";
                //        }
                //        tracebackpath += latComp + "," + longComp;
                //    }
                //}
                //var maplat = cusevent.Latitude;
                //var maplong = cusevent.Longtitude;
                //var taskLocation = string.Empty;
                //if (cusevent.Latitude != "0" && cusevent.Longtitude != "0")
                //{
                //    taskLocation = "&markers=color:red%7Clabel:T%7C" + cusevent.Latitude + "," + cusevent.Longtitude;
                //}
                //else
                //{
                //    maplat = latEng;
                //    maplong = longEng;
                //}
                //Map style roadMap
                //                String mapURL = "https://maps.googleapis.com/maps/api/staticmap?" +
                //"center=" + cusevent.Latitude + "," + cusevent.Longtitude + "&" +
                //"size=700x360" + tracebackpath + vargeopath + arrived + "&markers=color:green%7Clabel:C%7C" + latComp + "," + longComp + "&markers=color:yellow%7Clabel:E%7C" + latEng + "," + longEng + taskLocation + "&zoom=17" + "&maptype=png" + "&sensor=true";
                //                iTextSharp.text.Image LocationImage = iTextSharp.text.Image.GetInstance(mapURL);
                //                LocationImage.ScaleToFit(380, 320);
                //                LocationImage.Alignment = iTextSharp.text.Image.UNDERLYING;
                //                LocationImage.SetAbsolutePosition(0, 0);
                //                PdfPCell locationcell = new PdfPCell(LocationImage);
                //                locationcell.PaddingTop = 5;
                //                locationcell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right 
                //                locationcell.Border = 0;
                //                mapdataTable.AddCell(locationcell);


                //                PdfPTable legendtable = new PdfPTable(1);
                //                legendtable.DefaultCell.Border = Rectangle.NO_BORDER;
                //                legendtable.HorizontalAlignment = 0;
                //                Font redfont = new Font();
                //                redfont.Size = 11;
                //                Font yellowfont = new Font();
                //                yellowfont.Size = 11;
                //                Font greenfont = new Font();
                //                greenfont.Size = 11;
                //                Font bluefont = new Font();
                //                bluefont.Size = 11;
                //                redfont.Color = Color.RED;
                //                legendtable.AddCell(new Phrase("Legend:", ueleventimes));
                //                legendtable.AddCell(new Phrase("I:  Incident Location", redfont));
                //                yellowfont.Color = new Color(204, 204, 0);
                //                legendtable.AddCell(new Phrase("E:  Engaged Location", yellowfont));
                //                bluefont.Color = iTextSharp.text.Color.BLUE;
                //                legendtable.AddCell(new Phrase("A:  Arrived Location", bluefont));
                //                greenfont.Color = new Color(0, 204, 0);
                //                legendtable.AddCell(new Phrase("C:  Completed Location", greenfont));
                //                var pathCell = new PdfPCell(new Phrase("Red Path:  Geofence", eleventimes));
                //                pathCell.Border = iTextSharp.text.Rectangle.NO_BORDER;
                //                legendtable.AddCell(pathCell);
                //                foreach (var legendtrace in legendTraceback)
                //                {
                //                    var traceCell = new PdfPCell(new Phrase(legendtrace, eleventimes));
                //                    traceCell.Border = iTextSharp.text.Rectangle.NO_BORDER;
                //                    legendtable.AddCell(traceCell);
                //                }
                //                mapdataTable.AddCell(legendtable);

                iTextSharp.text.Paragraph paragraphTable = new iTextSharp.text.Paragraph();
                paragraphTable.SpacingBefore = 120f;
                Paragraph p = new Paragraph();
                p.IndentationLeft = 10;

                p.Add(mainheadertable);
                p.Add(taskdataTable);
                p.Add(maptextdataTable);
                //p.Add(mapdataTable);

                PdfPTable table = new PdfPTable(1);
                table.DefaultCell.Border = Rectangle.NO_BORDER;

                //Map style roadMap

                doc.Add(paragraphTable);

                doc.Add(p);

                //Attachments

                var listycanvasnotes = new List<string>();

                var attachments = new List<MobileHotEventAttachment>();


                //if (cusevent.EventType == CustomEvent.EventTypes.MobileHotEvent)
                //{
                //    var mobEv = MobileHotEvent.GetMobileHotEventByGuid(cusevent.Identifier, dbConnection);
                //    if (mobEv != null)
                //        attachments.AddRange(MobileHotEventAttachment.GetMobileHotEventAttachmentByHotEventId(mobEv.Id, dbConnection));


                //}

                attachments.AddRange(MobileHotEventAttachment.GetMobileHotEventAttachmentByEventId(cusevent.EventId, dbConnection));

                var videoCount = 0;
                var imgCount = 0;
                var pdfCount = 0;

                PdfPTable taskimagetable = new PdfPTable(2);
                taskimagetable.DefaultCell.Border = Rectangle.NO_BORDER;
                taskimagetable.WidthPercentage = 100;

                var atheader1 = new PdfPCell();
                atheader1.AddElement(new Phrase("Attachments", cbtimes));
                atheader1.Border = Rectangle.NO_BORDER;
                atheader1.PaddingTop = 10;
                atheader1.PaddingBottom = 10;
                atheader1.Colspan = 2;
                taskimagetable.AddCell(atheader1);
                taskimagetable.KeepTogether = true;
                if (attachments.Count > 0)
                {
                    var attachcount = 0;
                    var canvasattachcount = 0;
                    var attachlist = new List<MobileHotEventAttachment>();
                    var videosList = new List<MobileHotEventAttachment>();

                    foreach (var item in attachments)
                    {
                        if (!string.IsNullOrEmpty(item.AttachmentPath))
                        {
                            if (VideoExtensions.Contains(System.IO.Path.GetExtension(item.AttachmentPath).ToUpperInvariant()))
                            {
                                videoCount++;
                            }
                            else if (CommonUtility.FileExtensions.Contains(System.IO.Path.GetExtension(item.AttachmentPath).ToUpperInvariant()))//(System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".PDF" || System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".DOCX")
                            {
                                pdfCount++;
                            }
                            else
                            {
                                attachlist.Add(item);
                                attachcount++;
                                imgCount++;
                                listycanvasnotes.Add("Image " + attachcount);
                            }
                        }
                    }
                    taskimagetable.DefaultCell.Border = Rectangle.NO_BORDER;
                    var countz = 0;
                    foreach (var item in attachlist)
                    {
                        if (VideoExtensions.Contains(System.IO.Path.GetExtension(item.AttachmentPath).ToUpperInvariant()))
                        {

                        }
                        else if (CommonUtility.FileExtensions.Contains(System.IO.Path.GetExtension(item.AttachmentPath).ToUpperInvariant()))//(System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".PDF" || System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".DOCX")
                        {

                        }
                        else
                        {
                            iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(item.AttachmentPath);

                            if (image.Height > image.Width)
                            {
                                //Maximum height is 800 pixels.
                                float percentage = 0.0f;
                                percentage = 155 / image.Height;
                                image.ScalePercent(percentage * 100);
                            }
                            else
                            {
                                //Maximum width is 600 pixels.
                                float percentage = 0.0f;
                                percentage = 155 / image.Width;
                                image.ScalePercent(percentage * 100);
                            }

                            //image.ScaleAbsolute(120f, 155.25f);
                            image.Border = iTextSharp.text.Rectangle.NO_BORDER;

                            iTextSharp.text.pdf.PdfPCell imgCell1 = new iTextSharp.text.pdf.PdfPCell();
                            if (!string.IsNullOrEmpty(listycanvasnotes[countz]))
                            {
                                Paragraph pcell = new Paragraph(listycanvasnotes[countz].ToUpper());
                                pcell.Alignment = Element.ALIGN_LEFT;
                                imgCell1.AddElement(pcell);
                            }
                            imgCell1.Border = iTextSharp.text.Rectangle.NO_BORDER;
                            imgCell1.AddElement(new Chunk(image, 0, 0));
                            imgCell1.PaddingTop = 10;
                            taskimagetable.AddCell(imgCell1);
                            countz++;
                        }
                    }
                    taskimagetable.CompleteRow();
                }

                var extraattachment = new PdfPTable(3);
                extraattachment.DefaultCell.Border = Rectangle.NO_BORDER;
                extraattachment.WidthPercentage = 100;
                var phrase2 = new Phrase();
                phrase2.Add(
                    new Chunk("Files Attached:  ", btimes)
                );
                phrase2.Add(new Chunk(pdfCount.ToString(), times));

                var zheaderA3a = new PdfPCell();
                zheaderA3a.AddElement(phrase2);
                zheaderA3a.Border = Rectangle.NO_BORDER;
                zheaderA3a.PaddingTop = 20;
                zheaderA3a.PaddingBottom = 20;
                extraattachment.AddCell(zheaderA3a);

                var phrase32 = new Phrase();
                phrase32.Add(
                    new Chunk("Videos Attached:  ", btimes)
                );
                phrase32.Add(new Chunk(videoCount.ToString(), times));

                var aheaderA3a = new PdfPCell();
                aheaderA3a.AddElement(phrase32);
                aheaderA3a.Border = Rectangle.NO_BORDER;
                aheaderA3a.PaddingTop = 20;
                aheaderA3a.PaddingBottom = 20;
                extraattachment.AddCell(aheaderA3a);

                var phrase323 = new Phrase();
                phrase323.Add(
                    new Chunk("Images Attached:  ", btimes)
                );
                phrase323.Add(new Chunk(imgCount.ToString(), times));

                var aaheaderA3a = new PdfPCell();
                aaheaderA3a.AddElement(phrase323);
                aaheaderA3a.Border = Rectangle.NO_BORDER;
                aaheaderA3a.PaddingTop = 20;
                aaheaderA3a.PaddingBottom = 20;
                extraattachment.AddCell(aaheaderA3a);

                //doc.NewPage();

                doc.Add(taskimagetable);

                doc.Add(extraattachment);


                PdfPTable timetable = new PdfPTable(2);
                timetable.WidthPercentage = 100;
                timetable.DefaultCell.Border = Rectangle.NO_BORDER;
                timetable.SetWidths(columnWidths2575);

                if (newTRIlist.Count > 0)
                {
                    var headeventCell = new PdfPCell(new Phrase("Timeline", cbtimes));
                    headeventCell.Border = Rectangle.NO_BORDER;
                    headeventCell.PaddingTop = 10;
                    headeventCell.PaddingBottom = 10;
                    headeventCell.Colspan = 2;
                    timetable.AddCell(headeventCell);

                    foreach (var lit in newTRIlist)
                    {
                        var dateCell = new PdfPCell(new Phrase(lit.DatetimeN, redeleventimes));
                        dateCell.Border = Rectangle.NO_BORDER;
                        timetable.AddCell(dateCell);

                        var eventCell = new PdfPCell(new Phrase(lit.TimelineActivity, eleventimes));
                        eventCell.Border = Rectangle.NO_BORDER;
                        timetable.AddCell(eventCell);
                    }
                }
                doc.Add(timetable);

                //TASKS LOOP

                //var utask = UserTask.GetAllTaskByIncidentId(cusevent.EventId, dbConnection);
                if (utask.Count > 0)
                {
                    int count = 1;

                    foreach (var task in utask)
                    {
                        var tskheadertable = new PdfPTable(2);
                        tskheadertable.DefaultCell.Border = Rectangle.NO_BORDER;
                        tskheadertable.WidthPercentage = 100;
                        float[] columnWidths2080 = new float[] { 20, 80 };
                        tskheadertable.SetWidths(columnWidths2080);

                        var tskheader0 = new PdfPCell();
                        //tskheader0.AddElement(new Phrase("Task #:" + count, cbtimes));

                        tskheader0.AddElement(new Phrase("Attached Task", cbtimes));
                        tskheader0.Border = Rectangle.NO_BORDER;
                        tskheader0.Colspan = 2;
                        tskheadertable.AddCell(tskheader0);

                        var tskheader1 = new PdfPCell();
                        tskheader1.AddElement(new Phrase("Task Name:", beleventimes));
                        tskheader1.Border = Rectangle.NO_BORDER;
                        tskheadertable.AddCell(tskheader1);

                        var tskheader1a = new PdfPCell();
                        tskheader1a.AddElement(new Phrase(task.Name, eleventimes));
                        tskheader1a.Border = Rectangle.NO_BORDER;
                        tskheadertable.AddCell(tskheader1a);

                        var tskheader2 = new PdfPCell();
                        tskheader2.AddElement(new Phrase("Task Description:", beleventimes));
                        tskheader2.Border = Rectangle.NO_BORDER;
                        tskheadertable.AddCell(tskheader2);

                        var tskheader2a = new PdfPCell();
                        tskheader2a.AddElement(new Phrase(task.Description, eleventimes));
                        tskheader2a.Border = Rectangle.NO_BORDER;
                        tskheadertable.AddCell(tskheader2a);

                        var chklistName = "None";
                        var newTCLlist = new List<TaskChecklistReportItems>();
                        var parentTasks = TemplateCheckList.GetAllTemplateCheckListById(task.TemplateCheckListId.ToString(), dbConnection);
                        if (parentTasks != null)
                        {
                            chklistName = parentTasks.Name;

                            var sessions = TaskCheckList.GetTaskCheckListItemsByTaskId(task.Id, dbConnection);

                            foreach (var item in sessions)
                            {
                                var newTCL = new TaskChecklistReportItems();
                                newTCL.isParent = true;

                                if (item.CheckListItemType != (int)CheckListItemType.Type4 && item.CheckListItemType != (int)CheckListItemType.Type5)
                                {
                                    var ncaheaderA = new PdfPCell();
                                    ncaheaderA.AddElement(new Phrase(item.Name, eleventimes));
                                    ncaheaderA.Border = Rectangle.NO_BORDER;
                                    newTCL.itemName = ncaheaderA;
                                }
                                else
                                {
                                    var ncaheaderA = new PdfPCell();
                                    ncaheaderA.AddElement(new Phrase(item.Name, beleventimes));
                                    ncaheaderA.Border = Rectangle.NO_BORDER;
                                    newTCL.itemName = ncaheaderA;
                                }
                                newTCL.Notes = item.TemplateCheckListItemNote;

                                var myattachments = UserTaskAttachment.GetTaskAttachmentsByChecklistId(item.Id, dbConnection);
                                newTCL.Attachments = myattachments;


                                var stringCheck = string.Empty;
                                var itemNotes = string.Empty;

                                if (item.IsChecked)
                                {
                                    iTextSharp.text.pdf.PdfPCell imgCell1 = new iTextSharp.text.pdf.PdfPCell();
                                    imgCell1.AddElement(new Phrase("3", gweb));
                                    imgCell1.Border = Rectangle.NO_BORDER;
                                    newTCL.Status = imgCell1;
                                }
                                else
                                {
                                    List myList = new ZapfDingbatsList(54);

                                    iTextSharp.text.pdf.PdfPCell imgCell1 = new iTextSharp.text.pdf.PdfPCell();
                                    imgCell1.AddElement(new Phrase("6", rweb));
                                    imgCell1.Border = Rectangle.NO_BORDER;
                                    newTCL.Status = imgCell1;

                                    //if (item.CheckListItemType != (int)CheckListItemType.Type4 && item.CheckListItemType != (int)CheckListItemType.Type5)
                                    //    chkCounter++;
                                }

                                newTCLlist.Add(newTCL);

                                if (item.ChildCheckList != null)
                                {
                                    if (item.ChildCheckList.Count > 0)
                                    {
                                        foreach (var child in item.ChildCheckList)
                                        {

                                            var cnewTCL = new TaskChecklistReportItems();
                                            cnewTCL.isParent = false;

                                            var ncaheaderA = new PdfPCell();
                                            ncaheaderA.AddElement(new Phrase(child.Name, eleventimesI));
                                            ncaheaderA.Border = Rectangle.NO_BORDER;
                                            cnewTCL.itemName = ncaheaderA;

                                            cnewTCL.Notes = child.TemplateCheckListItemNote;

                                            var mycattachments = UserTaskAttachment.GetTaskAttachmentsByChecklistId(child.Id, dbConnection);
                                            cnewTCL.Attachments = mycattachments;

                                            if (item.IsChecked)
                                            {
                                                //gweb rweb
                                                iTextSharp.text.pdf.PdfPCell imgCell1 = new iTextSharp.text.pdf.PdfPCell();
                                                imgCell1.AddElement(new Phrase("3", gweb));
                                                imgCell1.Border = Rectangle.NO_BORDER;
                                                cnewTCL.Status = imgCell1;

                                            }
                                            else
                                            {
                                                iTextSharp.text.pdf.PdfPCell imgCell1 = new iTextSharp.text.pdf.PdfPCell();
                                                imgCell1.AddElement(new Phrase("6", rweb));
                                                imgCell1.Border = Rectangle.NO_BORDER;
                                                cnewTCL.Status = imgCell1;

                                                //chkCounter++;
                                            }

                                            newTCLlist.Add(cnewTCL);
                                        }
                                    }
                                }
                            }
                        }
                        var tskheader3 = new PdfPCell();
                        tskheader3.AddElement(new Phrase("Checklist Name:", beleventimes));
                        tskheader3.Border = Rectangle.NO_BORDER;
                        tskheadertable.AddCell(tskheader3);

                        var tskheader3a = new PdfPCell();
                        tskheader3a.AddElement(new Phrase(chklistName, eleventimes));
                        tskheader3a.Border = Rectangle.NO_BORDER;
                        tskheadertable.AddCell(tskheader3a);

                        doc.Add(tskheadertable);

                        var newchecklistTable = new PdfPTable(4);
                        newchecklistTable.DefaultCell.Border = Rectangle.NO_BORDER;
                        newchecklistTable.WidthPercentage = 100;
                        float[] columnWidthsCHK = new float[] { 30, 10, 30, 30 };
                        newchecklistTable.SetWidths(columnWidthsCHK);

                        var cheader1 = new PdfPCell();
                        cheader1.AddElement(new Phrase("Checklist", cbtimes));
                        cheader1.Border = Rectangle.NO_BORDER;
                        cheader1.PaddingTop = 10;
                        cheader1.PaddingBottom = 10;
                        cheader1.Colspan = 4;
                        newchecklistTable.AddCell(cheader1);

                        var ncaheader1 = new PdfPCell();
                        ncaheader1.AddElement(new Phrase("Item Name", weleventimes));
                        ncaheader1.Border = Rectangle.NO_BORDER;
                        ncaheader1.PaddingLeft = 5;
                        ncaheader1.PaddingBottom = 5;
                        ncaheader1.BackgroundColor = Color.DARK_GRAY;

                        newchecklistTable.AddCell(ncaheader1);

                        var ncaheader2 = new PdfPCell();
                        ncaheader2.AddElement(new Phrase("Status", weleventimes));
                        ncaheader2.Border = Rectangle.NO_BORDER;
                        ncaheader2.BackgroundColor = Color.DARK_GRAY;
                        newchecklistTable.AddCell(ncaheader2);

                        var ncaheader3 = new PdfPCell();
                        ncaheader3.AddElement(new Phrase("Notes", weleventimes));
                        ncaheader3.Border = Rectangle.NO_BORDER;
                        ncaheader3.BackgroundColor = Color.DARK_GRAY;
                        newchecklistTable.AddCell(ncaheader3);

                        var ncaheader4 = new PdfPCell();
                        ncaheader4.AddElement(new Phrase("Attachments", weleventimes));
                        ncaheader4.Border = Rectangle.NO_BORDER;
                        ncaheader4.BackgroundColor = Color.DARK_GRAY;
                        newchecklistTable.AddCell(ncaheader4);

                        foreach (var item in newTCLlist)
                        {
                            newchecklistTable.AddCell(item.itemName);

                            newchecklistTable.AddCell(item.Status);

                            var ncaheaderAB = new PdfPCell();
                            ncaheaderAB.AddElement(new Phrase(item.Notes, eleventimes));
                            ncaheaderAB.Border = Rectangle.NO_BORDER;
                            newchecklistTable.AddCell(ncaheaderAB);

                            if (item.Attachments.Count > 0)
                            {
                                PdfPTable imgTable = new PdfPTable(1);
                                imgTable.DefaultCell.Border = Rectangle.NO_BORDER;
                                foreach (var attach in item.Attachments)
                                {
                                    if (VideoExtensions.Contains(System.IO.Path.GetExtension(attach.DocumentPath).ToUpperInvariant()))
                                    {
                                        imgTable.AddCell(new Phrase("Video Attached", eleventimes));
                                    }
                                    else
                                    {
                                        //var imgstring = String.Format(mimssettings.MIMSMobileAddress + "/Uploads/Tasks/" + task.Id + "/{0}", System.IO.Path.GetFileName(attach.DocumentPath));

                                        iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(attach.DocumentPath);

                                        if (image.Height > image.Width)
                                        {
                                            //Maximum height is 800 pixels.
                                            float percentage = 0.0f;
                                            percentage = 155 / image.Height;
                                            image.ScalePercent(percentage * 100);
                                        }
                                        else
                                        {
                                            //Maximum width is 600 pixels.
                                            float percentage = 0.0f;
                                            percentage = 155 / image.Width;
                                            image.ScalePercent(percentage * 100);
                                        }

                                        //image.ScaleAbsolute(120f, 155.25f);
                                        image.Border = Rectangle.NO_BORDER;
                                        iTextSharp.text.pdf.PdfPCell imgCell1l = new iTextSharp.text.pdf.PdfPCell();
                                        imgCell1l.Border = Rectangle.NO_BORDER;
                                        imgCell1l.AddElement(new Chunk(image, 0, 0));
                                        imgTable.AddCell(imgCell1l);
                                        if (!string.IsNullOrEmpty(attach.ImageNote) && attach.ImageNote != "Task Attachment")
                                            imgTable.AddCell(new Phrase("Img Note:" + attach.ImageNote, eleventimes));
                                    }
                                }
                                newchecklistTable.AddCell(imgTable);
                            }
                            else
                            {
                                newchecklistTable.AddCell(new Phrase(" ", eleventimes));
                            }
                        }

                        doc.Add(newchecklistTable);

                        var tsklistycanvasnotes = new List<string>();
                        var tskattachments = UserTaskAttachment.GetTaskAttachmentsByTaskId(task.Id, dbConnection);
                        var tskvideoCount = 0;
                        var tskimgCount = 0;
                        var tskpdfCount = 0;

                        PdfPTable tsktaskimagetable = new PdfPTable(2);
                        tsktaskimagetable.DefaultCell.Border = Rectangle.NO_BORDER;
                        tsktaskimagetable.WidthPercentage = 100;

                        var tskatheader1 = new PdfPCell();
                        tskatheader1.AddElement(new Phrase("Task Attachments", cbtimes));
                        tskatheader1.Border = Rectangle.NO_BORDER;
                        tskatheader1.PaddingTop = 10;
                        tskatheader1.PaddingBottom = 10;
                        tskatheader1.Colspan = 2;
                        tsktaskimagetable.AddCell(tskatheader1);

                        if (tskattachments.Count > 0)
                        {
                            var attachcount = 0;
                            var canvasattachcount = 0;
                            var attachlist = new List<UserTaskAttachment>();

                            foreach (var item in tskattachments)
                            {
                                if (!string.IsNullOrEmpty(item.DocumentPath) && item.ChecklistId == 0)
                                {
                                    if (VideoExtensions.Contains(System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant()))
                                    {
                                        tskvideoCount++;
                                    }
                                    else if (System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant() == ".PDF")
                                    {
                                        tskpdfCount++;
                                    }
                                    else
                                    {
                                        attachlist.Add(item);
                                        attachcount++;
                                        tskimgCount++;
                                        if (item.ImageNote != "Task Attachment" && !string.IsNullOrEmpty(item.ImageNote))
                                        {
                                            tsklistycanvasnotes.Add("Canvas Notes Image " + attachcount + ":" + item.ImageNote);
                                            canvasattachcount++;
                                        }
                                        else
                                        {
                                            tsklistycanvasnotes.Add("Image " + attachcount);
                                            canvasattachcount++;
                                        }
                                    }
                                }
                            }

                            var countz = 0;
                            foreach (var item in attachlist)
                            {
                                if (VideoExtensions.Contains(System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant()))
                                {

                                }
                                else if (System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant() == ".PDF")
                                {

                                }
                                else
                                {
                                    iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(item.DocumentPath);

                                    if (image.Height > image.Width)
                                    {
                                        //Maximum height is 800 pixels.
                                        float percentage = 0.0f;
                                        percentage = 155 / image.Height;
                                        image.ScalePercent(percentage * 100);
                                    }
                                    else
                                    {
                                        //Maximum width is 600 pixels.
                                        float percentage = 0.0f;
                                        percentage = 155 / image.Width;
                                        image.ScalePercent(percentage * 100);
                                    }

                                    //image.ScaleAbsolute(120f, 155.25f);
                                    image.Border = iTextSharp.text.Rectangle.NO_BORDER;

                                    iTextSharp.text.pdf.PdfPCell imgCell1 = new iTextSharp.text.pdf.PdfPCell();
                                    if (!string.IsNullOrEmpty(tsklistycanvasnotes[countz]))
                                    {
                                        Paragraph pcell = new Paragraph(tsklistycanvasnotes[countz].ToUpper());
                                        pcell.Alignment = Element.ALIGN_LEFT;
                                        imgCell1.AddElement(pcell);
                                    }
                                    imgCell1.Border = iTextSharp.text.Rectangle.NO_BORDER;
                                    imgCell1.AddElement(new Chunk(image, 0, 0));
                                    imgCell1.PaddingTop = 10;
                                    tsktaskimagetable.AddCell(imgCell1);
                                    countz++;
                                }
                            }
                            tsktaskimagetable.CompleteRow();
                        }

                        var tskextraattachment = new PdfPTable(3);
                        tskextraattachment.DefaultCell.Border = Rectangle.NO_BORDER;
                        tskextraattachment.WidthPercentage = 100;
                        var tskphrase2 = new Phrase();
                        tskphrase2.Add(
                            new Chunk("PDF Attached:  ", btimes)
                        );
                        tskphrase2.Add(new Chunk(tskpdfCount.ToString(), times));

                        var tskzheaderA3a = new PdfPCell();
                        tskzheaderA3a.AddElement(tskphrase2);
                        tskzheaderA3a.Border = Rectangle.NO_BORDER;
                        tskzheaderA3a.PaddingTop = 20;
                        tskzheaderA3a.PaddingBottom = 20;
                        tskextraattachment.AddCell(tskzheaderA3a);

                        var tskphrase32 = new Phrase();
                        tskphrase32.Add(
                            new Chunk("Videos Attached:  ", btimes)
                        );
                        tskphrase32.Add(new Chunk(tskvideoCount.ToString(), times));

                        var tskaheaderA3a = new PdfPCell();
                        tskaheaderA3a.AddElement(tskphrase32);
                        tskaheaderA3a.Border = Rectangle.NO_BORDER;
                        tskaheaderA3a.PaddingTop = 20;
                        tskaheaderA3a.PaddingBottom = 20;
                        tskextraattachment.AddCell(tskaheaderA3a);

                        var tskphrase323 = new Phrase();
                        tskphrase323.Add(
                            new Chunk("Images Attached:  ", btimes)
                        );
                        tskphrase323.Add(new Chunk(tskimgCount.ToString(), times));

                        var tskaaheaderA3a = new PdfPCell();
                        tskaaheaderA3a.AddElement(tskphrase323);
                        tskaaheaderA3a.Border = Rectangle.NO_BORDER;
                        tskaaheaderA3a.PaddingTop = 20;
                        tskaaheaderA3a.PaddingBottom = 20;
                        tskextraattachment.AddCell(tskaaheaderA3a);

                        doc.Add(tsktaskimagetable);

                        doc.Add(tskextraattachment);

                        var taskeventhistory = TaskEventHistory.GetTaskEventHistoryByTaskId(task.Id, dbConnection);

                        taskeventhistory = taskeventhistory.OrderByDescending(i => i.CreatedDate).ToList();

                        var tsknewTRIlist = new List<TimelineReportItems>();

                        foreach (var taskEv in taskeventhistory)
                        {
                            var timelineItem = new TimelineReportItems();
                            var taskn = "Task";
                            if (taskEv.isSubTask)
                            {
                                taskn = "Sub-task";
                            }

                            if (taskEv.Action == (int)TaskAction.Complete)
                            {
                                completedBy = taskEv.CustomerFullName;
                                var compLocation = string.Empty;
                                var geoLoc = ReverseGeocode.RetrieveFormatedAddress(task.EndLatitude.ToString(), task.EndLongitude.ToString(), getClientLic);
                                if (!string.IsNullOrEmpty(geoLoc))
                                    compLocation = " at " + geoLoc;
                                var compInfo = "completed";
                                latComp = task.EndLatitude.ToString();
                                longComp = task.EndLongitude.ToString();
                                timelineItem.DatetimeN = taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                                //listy.Add(taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());

                                var compstring = completedBy + " " + compInfo + " " + taskn + " " + compLocation;

                                timelineItem.TimelineActivity = compstring;
                                //listy.Add(compstring);
                            }
                            else if (taskEv.Action == (int)TaskAction.InProgress)
                            {
                                inprogressby = taskEv.CustomerFullName;
                                var pendingLocation = string.Empty;
                                var geoLoc2 = ReverseGeocode.RetrieveFormatedAddress(task.StartLatitude.ToString(), task.StartLongitude.ToString(), getClientLic);
                                if (!string.IsNullOrEmpty(geoLoc2))
                                    pendingLocation = " at " + geoLoc2;
                                var pendingInfo = "started";
                                // listy.Add(taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());
                                timelineItem.DatetimeN = taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();

                                var returnstring = inprogressby + " " + pendingInfo + " " + taskn + " " + pendingLocation;
                                //listy.Add(returnstring);
                                timelineItem.TimelineActivity = returnstring;

                                latEng = task.StartLatitude.ToString();
                                longEng = task.StartLongitude.ToString();
                            }
                            else if (taskEv.Action == (int)TaskAction.Accepted)
                            {
                                timelineItem.DatetimeN = taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                                //listy.Add(taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());
                                acceptedData = taskEv.CustomerFullName + " accepted " + taskn;
                                //listy.Add(acceptedData);
                                timelineItem.TimelineActivity = acceptedData;
                            }
                            else if (taskEv.Action == (int)TaskAction.Rejected)
                            {
                                //listy.Add(taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());
                                timelineItem.DatetimeN = taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();

                                rejectedData = taskEv.CustomerFullName + " rejected Task " + taskn;

                                timelineItem.TimelineActivity = rejectedData;
                                //listy.Add(rejectedData);
                            }
                            else if (taskEv.Action == (int)TaskAction.Assigned)
                            {
                                //listy.Add(taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());
                                timelineItem.DatetimeN = taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();

                                assignedData = taskEv.CustomerFullName + " assigned " + taskn + " to " + taskEv.ACustomerFullName;

                                timelineItem.TimelineActivity = assignedData;

                                //listy.Add(assignedData);
                            }
                            else if (taskEv.Action == (int)TaskAction.Pending)
                            {
                                var incidentLocation = string.Empty;
                                var geoLoc3 = ReverseGeocode.RetrieveFormatedAddress(task.Latitude.ToString(), task.Longitude.ToString(), getClientLic);
                                if (!string.IsNullOrEmpty(geoLoc3))
                                    incidentLocation = " at " + geoLoc3;
                                var actioninfo = "created";
                                //listy.Add(taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());
                                timelineItem.DatetimeN = taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                                actioninfo = task.CustomerFullName + " " + actioninfo + " " + taskn + " for " + taskEv.ACustomerFullName + " " + incidentLocation;
                                timelineItem.TimelineActivity = actioninfo;
                                //listy.Add(actioninfo);
                            }
                            tsknewTRIlist.Add(timelineItem);
                        }

                        PdfPTable tsktimetable = new PdfPTable(2);
                        tsktimetable.WidthPercentage = 100;
                        tsktimetable.DefaultCell.Border = Rectangle.NO_BORDER;
                        tsktimetable.SetWidths(columnWidths2575);

                        if (tsknewTRIlist.Count > 0)
                        {
                            var headeventCell = new PdfPCell(new Phrase("Task Timeline", cbtimes));
                            headeventCell.Border = Rectangle.NO_BORDER;
                            headeventCell.PaddingTop = 10;
                            headeventCell.PaddingBottom = 10;
                            headeventCell.Colspan = 2;
                            tsktimetable.AddCell(headeventCell);

                            foreach (var lit in tsknewTRIlist)
                            {
                                var dateCell = new PdfPCell(new Phrase(lit.DatetimeN, redeleventimes));
                                dateCell.Border = Rectangle.NO_BORDER;
                                tsktimetable.AddCell(dateCell);

                                var eventCell = new PdfPCell(new Phrase(lit.TimelineActivity, eleventimes));
                                eventCell.Border = Rectangle.NO_BORDER;
                                tsktimetable.AddCell(eventCell);
                            }
                        }
                        doc.Add(tsktimetable);

                    }
                }
                //END OF TASKS



                Font geleventimes = new Font(f_cn, 11, Font.NORMAL, Color.GRAY);
                var endTable = new PdfPTable(1);
                endTable.DefaultCell.Border = Rectangle.NO_BORDER;
                endTable.WidthPercentage = 100;

                var endheader = new PdfPCell();
                endheader.AddElement(new Phrase("Report generated: " + CommonUtility.getDTNow().AddHours(userinfo.TimeZone).ToString() + " 	by: " + userinfo.Username, geleventimes));
                endheader.Border = Rectangle.NO_BORDER;
                endTable.AddCell(endheader);
                doc.Add(endTable);

                writer.Flush();
                doc.Close();
                if (File.Exists(path))
                {
                    using (StreamReader sr = new StreamReader(path))
                    {
                        var vpath = CommonUtility.CloudUploadFile(userinfo.CustomerInfoId, fileName, sr.BaseStream);
                        return vpath;
                    }
                }
            }

            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("Incident", "GeneratePDFIncident", exp, dbConnection, userinfo.SiteId);
                return "Failed to generate report!";
            }
            return "Failed to generate report!";
        }

        [WebMethod]
        public static List<string> getManualData(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var gAllFiles = MimsSupport.GetAllMimsSupport(dbConnection);
                foreach (var f in gAllFiles)
                {
                    if (!string.IsNullOrEmpty(f.VideoPath))
                    {
                        if (!string.IsNullOrEmpty(f.DocumentPath))
                            listy.Add(f.Name + "|" + f.VideoPath + "|2|" + f.DocumentPath);
                        else
                            listy.Add(f.Name + "|" + f.VideoPath + "|1");
                    }
                    else
                    {
                        listy.Add(f.Name + "|" + f.DocumentPath + "|0");
                    }
                }
                //var strings = "Task - How to create checklist|https://livemimslob.blob.core.windows.net/45blob/f1ffa9b9-3.jpg";
                //listy.Add(strings);
                //listy.Add(strings);
                //listy.Add(strings);
                //listy.Add(strings);
                //listy.Add(strings);
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Ticketing", "getTicketRemarksData2", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static string saveWifi(string ssid, string pw, string uname)
        {
            var listy = string.Empty;
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    return "SUCCESS";
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Tasks", "deleteAttachmentData", err, dbConnection, userinfo.SiteId);
                    listy = err.Message;
                }
                return listy;
            }
        }
    }
}