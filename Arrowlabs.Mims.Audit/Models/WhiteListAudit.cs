﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class WhiteListAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public string MACAddress { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int SiteId { get; set; }
        private static WhiteListAudit GetWhiteListAuditFromSqlReader(SqlDataReader reader)
        {
            var whiteListAudit = new WhiteListAudit();
            whiteListAudit.BaseAudit = BaseAudit.GetBaseAuditFromSqlReader(reader);
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

            if (columns.Contains("MACAddress") && reader["MACAddress"] != DBNull.Value)
                whiteListAudit.MACAddress = reader["MACAddress"].ToString();
            if (columns.Contains("CreatedBy") && reader["CreatedBy"] != DBNull.Value)
                whiteListAudit.CreatedBy = reader["CreatedBy"].ToString();
            if (columns.Contains("CreatedDate") && reader["CreatedDate"] != DBNull.Value)
                whiteListAudit.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                whiteListAudit.SiteId = Convert.ToInt32(reader["SiteId"].ToString());

            return whiteListAudit;

        }
        public static List<WhiteListAudit> GetAllWhiteListAudit(string dbConnection)
        {
            var whiteList = new List<WhiteListAudit>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllWhiteListAudit", connection);
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var whiteListAudit = GetWhiteListAuditFromSqlReader(reader);
                    whiteList.Add(whiteListAudit);
                }
                connection.Close();
                reader.Close();
            }
            return whiteList;
        }
        public static bool InsertWhiteListAudit(WhiteListAudit whiteListAudit, string dbConnection)
        {
            if (whiteListAudit != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertWhiteListAudit", connection);

                    var baseAuditId = BaseAudit.InsertBaseAudit(whiteListAudit.BaseAudit, dbConnection);
                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;

                    cmd.Parameters.Add("@MACAddress", SqlDbType.NVarChar).Value = whiteListAudit.MACAddress;

                    cmd.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = whiteListAudit.CreatedBy;

                    cmd.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = whiteListAudit.CreatedDate;
                    cmd.Parameters.Add("@SiteId", SqlDbType.Int).Value = whiteListAudit.SiteId;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }
     
    }
}
