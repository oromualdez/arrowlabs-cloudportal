﻿using Arrowlabs.Mims.Audit.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Business.Layer
{
    public class MilestoneCamera
    {
        public Guid Id { get; set; }
        public string CameraName { get; set; }

        public string ServerIP { get; set; }

        public int Port { get; set; }

        public string Password { get; set; }

        public string Username { get; set; }

        public string CamDetails { get; set; }

        public int AuthenticationType { get; set; }

        private static MilestoneCamera GetMilestoneCameraFromSqlReader(SqlDataReader reader)
        {
            var milestoneCamera = new MilestoneCamera();
            if (reader["Id"] != DBNull.Value)
                milestoneCamera.Id = Guid.Parse(reader["Id"].ToString());
            if (reader["CameraName"] != DBNull.Value)
                milestoneCamera.CameraName = reader["CameraName"].ToString();
            if (reader["ServerIP"] != DBNull.Value)
                milestoneCamera.ServerIP = reader["ServerIP"].ToString();
            if (reader["Port"] != DBNull.Value)
                milestoneCamera.Port =Convert.ToInt32(reader["Port"].ToString());
            if (reader["Password"] != DBNull.Value)
                milestoneCamera.Password = reader["Password"].ToString();
            if (reader["Username"] != DBNull.Value)
                milestoneCamera.Username = reader["Username"].ToString();
            if (reader["AuthenticationType"] != DBNull.Value)
                milestoneCamera.AuthenticationType = Convert.ToInt32(reader["AuthenticationType"].ToString());

            milestoneCamera.CamDetails = string.Format("{0}╦{1}╦{2}╦{3}╦{4}╦{5}", milestoneCamera.ServerIP, milestoneCamera.Port, milestoneCamera.Username, milestoneCamera.Password, milestoneCamera.AuthenticationType, milestoneCamera.CameraName);

            return milestoneCamera;
        }

        public static List<MilestoneCamera> GetAllMilestoneCamera(string dbConnection)
        {
            var milestoneCameras = new List<MilestoneCamera>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllMilestoneCamera", connection);            
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    var milestoneCamera = GetMilestoneCameraFromSqlReader(reader);
                    milestoneCameras.Add(milestoneCamera);
                }
                connection.Close();
                reader.Close();
            }
            return milestoneCameras;
        }
        public static bool InsertorUpdateMilestoneCamera(MilestoneCamera milestoneCamera, string dbConnection,
string auditDbConnection = "", bool isAuditEnabled = true)
        {

            if (milestoneCamera != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateMilestoneCamera", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.UniqueIdentifier));
                    cmd.Parameters["@Id"].Value = milestoneCamera.Id;

                    cmd.Parameters.Add(new SqlParameter("@CameraName", SqlDbType.NVarChar));
                    cmd.Parameters["@CameraName"].Value = milestoneCamera.CameraName;

                    cmd.Parameters.Add(new SqlParameter("@ServerIP", SqlDbType.NVarChar));
                    cmd.Parameters["@ServerIP"].Value = milestoneCamera.ServerIP;

                    cmd.Parameters.Add(new SqlParameter("@Port", SqlDbType.Int));
                    cmd.Parameters["@Port"].Value = milestoneCamera.Port;

                    cmd.Parameters.Add(new SqlParameter("@Password", SqlDbType.NVarChar));
                    cmd.Parameters["@Password"].Value = milestoneCamera.Password;

                    cmd.Parameters.Add(new SqlParameter("@Username", SqlDbType.NVarChar));
                    cmd.Parameters["@Username"].Value = milestoneCamera.Username;

                    cmd.Parameters.Add(new SqlParameter("@AuthenticationType", SqlDbType.Int));
                    cmd.Parameters["@AuthenticationType"].Value = milestoneCamera.AuthenticationType;


                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();
                    
                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            var audit = new MilestoneCameraAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, Arrowlabs.Mims.Audit.Enums.Action.Create, milestoneCamera.Username);
                            Reflection.CopyProperties(milestoneCamera, audit);
                            MilestoneCameraAudit.InsertorUpdateMilestoneCamera(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertOrUpdateMilestoneCamera", ex, dbConnection);
                    }
                }
            }
            return true;
        }

    }
}
