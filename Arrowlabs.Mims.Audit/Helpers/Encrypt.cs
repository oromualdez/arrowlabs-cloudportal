﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;

namespace Arrowlabs.Mims.Audit
{
     /// <summary>
     /// Encrypt class
     /// </summary>
    public class Encrypt
    {
        static TripleDESCryptoServiceProvider tripleDes = null;

        private static TripleDESCryptoServiceProvider TripleDesCrypto(string dbConnection)
        {


            tripleDes = new TripleDESCryptoServiceProvider();
            //tripleDes.Key = TruncateHash("ArrowlabsFZCLLCSecuritySolutions2013", tripleDes.KeySize / 8);
            tripleDes.Key = TruncateHash(Arrowlabs.Mims.Audit.Models.Miscellaneous.FinalResult(dbConnection), tripleDes.KeySize / 8);
            tripleDes.IV = TruncateHash("", tripleDes.BlockSize / 8);
            return tripleDes;

        }


        private static byte[] TruncateHash(string key, int length)
        {
            SHA1CryptoServiceProvider sha1 = new SHA1CryptoServiceProvider();
            // Hash the key.
            byte[] keyBytes = System.Text.Encoding.Unicode.GetBytes(key);
            byte[] hash = sha1.ComputeHash(keyBytes);

            // Truncate or pad the hash.
            Array.Resize(ref hash, length);
            return hash;
        }
        public static string EncryptData(string plaintext, bool useHashing, string dbConnection = "")
        {

            byte[] plaintextBytes = System.Text.Encoding.Unicode.GetBytes(plaintext);


            System.IO.MemoryStream ms = new System.IO.MemoryStream();
            var tripleDesCrypto = TripleDesCrypto(dbConnection);
            CryptoStream encStream = new CryptoStream(ms, tripleDesCrypto.CreateEncryptor(), System.Security.Cryptography.CryptoStreamMode.Write);


            encStream.Write(plaintextBytes, 0, plaintextBytes.Length);
            encStream.FlushFinalBlock();


            return Convert.ToBase64String(ms.ToArray());
        }

    }
}
