﻿using Arrowlabs.Mims.Audit.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Business.Layer
{
    public class CustomerAttachment
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public string AttachmentPath { get; set; }

        public string DocumentName { get; set; }

        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int SiteId { get; set; }
        public int CustomerInfoId { get; set; }
        private static CustomerAttachment GetCustomerAttachmentFromSqlReader(SqlDataReader reader)
        {
            var customerAttachment = new CustomerAttachment();
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
            if (reader["Id"] != DBNull.Value)
                customerAttachment.Id = Convert.ToInt32(reader["Id"].ToString());



            if (reader["CustomerId"] != DBNull.Value)
                customerAttachment.CustomerId = Convert.ToInt32(reader["CustomerId"].ToString());

            if (reader["CreatedBy"] != DBNull.Value)
                customerAttachment.CreatedBy = reader["CreatedBy"].ToString();
            if (reader["CreatedDate"] != DBNull.Value)
                customerAttachment.CreatedDate = Convert.ToDateTime(reader["CreatedDate"]);
            if (reader["UpdatedBy"] != DBNull.Value)
                customerAttachment.UpdatedBy = reader["UpdatedBy"].ToString();
            if (reader["UpdatedDate"] != DBNull.Value)
                customerAttachment.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"]);
            if (reader["AttachmentPath"] != DBNull.Value)
                customerAttachment.AttachmentPath = reader["AttachmentPath"].ToString();

            if (columns.Contains( "SiteId") & reader["SiteId"] != DBNull.Value)
                customerAttachment.SiteId = Convert.ToInt32(reader["SiteId"].ToString());

            if (columns.Contains( "CustomerInfoId") & reader["CustomerInfoId"] != DBNull.Value)
                customerAttachment.CustomerInfoId = Convert.ToInt32(reader["CustomerInfoId"].ToString());

            if (columns.Contains( "DocumentName") & reader["DocumentName"] != DBNull.Value)
                customerAttachment.DocumentName = reader["DocumentName"].ToString();
            

            return customerAttachment;
        }

        public static List<CustomerAttachment> GetAllCustomerAttachments(string dbConnection)
        {
            var customerAttachments = new List<CustomerAttachment>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetAllCustomerAttachments";
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var custAttach = GetCustomerAttachmentFromSqlReader(reader);
                    customerAttachments.Add(custAttach);
                }
                connection.Close();
                reader.Close();
            }
            return customerAttachments;
        }

        public static CustomerAttachment GetCustomerAttachmentById(int id, string dbConnection)
        {
            CustomerAttachment customerAttachment = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetCustomerAttachmentById";
                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    customerAttachment = GetCustomerAttachmentFromSqlReader(reader);
                    
                }
                connection.Close();
                reader.Close();
            }
            return customerAttachment;
        }

        public static bool DeleteCustomerAttachmentById(int locationId, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var cmd = new SqlCommand("DeleteCustomerAttachmentById", connection);

                cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                cmd.Parameters["@Id"].Value = locationId;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.ExecuteNonQuery();
            }
            return true;
        }


        public static List<CustomerAttachment> GetCustomerAttachmentByCustomerId(int customerId, string dbConnection)
        {
            var customerAttachments = new List<CustomerAttachment>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetCustomerAttachmentByCustomerId";
                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlCommand.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                sqlCommand.Parameters["@CustomerId"].Value = customerId;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var customerAttachment = GetCustomerAttachmentFromSqlReader(reader);
                    customerAttachments.Add(customerAttachment);
                }
                connection.Close();
                reader.Close();
            }
            return customerAttachments;
        }
        public static int InsertOrUpdateCustomerAttachment(CustomerAttachment customerAttachment, string dbConnection
            , string auditDbConnection = "", bool isAuditEnabled = true)
        {
            int id = customerAttachment.Id;
            var action = (id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;

            if (customerAttachment != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateCustomerAttachment", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = customerAttachment.Id;


                    cmd.Parameters.Add(new SqlParameter("@CustomerInfoId", SqlDbType.Int));
                                        cmd.Parameters["@CustomerInfoId"].Value = customerAttachment.CustomerInfoId;
                    

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = customerAttachment.CustomerId;

                    cmd.Parameters.Add(new SqlParameter("@AttachmentPath", SqlDbType.NVarChar));
                    cmd.Parameters["@AttachmentPath"].Value = customerAttachment.AttachmentPath;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = customerAttachment.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    if (customerAttachment.Id == 0)
                    {
                        cmd.Parameters["@CreatedDate"].Value = DateTime.Now;
                    }
                    else
                    {
                        cmd.Parameters["@CreatedDate"].Value = customerAttachment.CreatedDate;
                    }

                    cmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@UpdatedBy"].Value = customerAttachment.UpdatedBy;

                    cmd.Parameters.Add(new SqlParameter("@DocumentName", SqlDbType.NVarChar));
                    cmd.Parameters["@DocumentName"].Value = customerAttachment.DocumentName;
                    
                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = DateTime.Now;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = customerAttachment.SiteId;

                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();

                    if (id == 0)
                        id = (int)returnParameter.Value;
                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            customerAttachment.Id = id;
                            var audit = new CustomerAttachmentAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, customerAttachment.UpdatedBy);
                            Reflection.CopyProperties(customerAttachment, audit);
                            CustomerAttachmentAudit.InsertCustomerAttachmentAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertCustomerAttachmentAudit", ex, dbConnection);
                    }
                }
            }
            return id;
        }
    }
}
