﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Runtime.Serialization;
using Arrowlabs.Mims.Audit.Models;
using System.Linq;
namespace Arrowlabs.Business.Layer
{

    public class TaskCheckList
    {
        public int Id { get; set; }
        public int TaskId { get; set; }

        public int SiteId { get; set; }
        
        public int CustomerId { get; set; }

        public int TemplateCheckListItemId { get; set; }
        public string UpdatedBy { get; set; }
        public string TemplateCheckListItemNote { get; set; }        
        public bool IsChecked { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string IsCheck { get; set; }

        public string IsCameraDisplay { get; set; }

        public string IsValueDisplay { get; set; }

        public string IsSingleItem { get; set; }
        
        public string IsLocationDisplay { get; set; }
     
        // Mapping fields
        public string CheckListLocation { get; set; }
        public bool IsCamera { get; set; }
        public string Name { get; set; }
        public string DocumentPath { get; set; }
        
        public int Quantity { get; set; }
        public int EntryQuantity { get; set; }
        public int ExitQuantity { get; set; }
        public int CheckListItemType { get; set; }
        public int ParentCheckListId { get; set; }
        public int ChildCheckListId { get; set; }
        public List<TaskCheckList> ChildCheckList { get; set; }
        public static TaskCheckList GetTaskCheckListItemById(int id, string dbConnection)
        {
            var taskCheckListItem = new TaskCheckList();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetTaskCheckListItemById", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    taskCheckListItem = GetTaskCheckListItemFromSqlReader(reader);
                }
                connection.Close();
                reader.Close();
            }
            return taskCheckListItem;
        } 
        public static List<TaskCheckList> GetTaskCheckListItemsByTaskId(int taskId, string dbConnection)
        {
            var taskCheckListItems = new List<TaskCheckList>();
            List<TaskCheckList> parentCheckListItems = new List<TaskCheckList>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetTaskCheckListItemByTaskId", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@TaskId", SqlDbType.Int));
                sqlCommand.Parameters["@TaskId"].Value = taskId;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var taskCheckListItem = GetTaskCheckListItemFromSqlReader(reader);
                    taskCheckListItems.Add(taskCheckListItem);
                }

                if (taskCheckListItems.Count > 0)
                { 
                    foreach (var taskCheckListItem in taskCheckListItems)
                    {
                        if (taskCheckListItem.ChildCheckListId == 0)
                        {
                            taskCheckListItem.ChildCheckList = new List<TaskCheckList>();
                            
                            foreach (var taskCheckLstItem in taskCheckListItems)
                            {
                                if (taskCheckLstItem.ChildCheckListId == taskCheckListItem.TemplateCheckListItemId)
                                {
                                    taskCheckListItem.ChildCheckList.Add(taskCheckLstItem);
                                }
                            }

                            if (taskCheckListItem.ChildCheckList.Count > 0)
                            {
                                taskCheckListItem.IsSingleItem = "disabled";
                                taskCheckListItem.IsValueDisplay = "style=display:none;";
                                taskCheckListItem.IsLocationDisplay = "style=display:none;";
                                taskCheckListItem.IsCameraDisplay = "display:none;";
                            }

                            parentCheckListItems.Add(taskCheckListItem);
                        }
                    }
                }


                connection.Close();
                reader.Close();
            }
            return parentCheckListItems;
        }

        public static TaskCheckList GetTaskCheckListItemByTaskIdAndTemplateCheckListItemId(int taskId,int id ,string dbConnection)
        {
            var taskCheckListItems = new TaskCheckList();
            List<TaskCheckList> parentCheckListItems = new List<TaskCheckList>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetTaskCheckListItemByTaskIdAndTemplateCheckListItemId", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@TaskId", SqlDbType.Int));
                sqlCommand.Parameters["@TaskId"].Value = taskId;

                sqlCommand.Parameters.Add(new SqlParameter("@TemplateCheckListItemId", SqlDbType.Int));
                sqlCommand.Parameters["@TemplateCheckListItemId"].Value = id;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    taskCheckListItems = GetTaskCheckListItemFromSqlReader(reader);
                } 
                connection.Close();
                reader.Close();
            }
            return taskCheckListItems;
        }

        public static List<TaskCheckList> GetTaskCheckListItemByTaskIdAndChildCheckListId(int taskId, int id, string dbConnection)
        {
            var taskCheckListItems = new List<TaskCheckList>();
            List<TaskCheckList> parentCheckListItems = new List<TaskCheckList>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetTaskCheckListItemByTaskIdAndChildCheckListId", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@TaskId", SqlDbType.Int));
                sqlCommand.Parameters["@TaskId"].Value = taskId;

                sqlCommand.Parameters.Add(new SqlParameter("@ChildCheckListId", SqlDbType.Int));
                sqlCommand.Parameters["@ChildCheckListId"].Value = id;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var taskCheckListItem = GetTaskCheckListItemFromSqlReader(reader);
                    taskCheckListItems.Add(taskCheckListItem);
                }

                if (taskCheckListItems.Count > 0)
                {
                    foreach (var taskCheckListItem in taskCheckListItems)
                    {
                        //if (taskCheckListItem.ChildCheckListId == 0)
                       // {
                          //  taskCheckListItem.ChildCheckList = new List<TaskCheckList>();

                          //  foreach (var taskCheckLstItem in taskCheckListItems)
                          //  {
                           //     if (taskCheckLstItem.ChildCheckListId == taskCheckListItem.TemplateCheckListItemId)
                            //    {
                           //         taskCheckListItem.ChildCheckList.Add(taskCheckLstItem);
                            //    }
                            //}
                            parentCheckListItems.Add(taskCheckListItem);
                       // }
                    }
                }


                connection.Close();
                reader.Close();
            }
            return parentCheckListItems;
        }

        private static TaskCheckList GetTaskCheckListItemFromSqlReader(SqlDataReader reader)
        {
            var taskCheckListItem = new TaskCheckList();
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
            if (reader["Id"] != DBNull.Value)
                taskCheckListItem.Id = Convert.ToInt32(reader["Id"].ToString());
            if (reader["TaskId"] != DBNull.Value)
                taskCheckListItem.TaskId = Convert.ToInt32(reader["TaskId"].ToString());

            if (reader["SiteId"] != DBNull.Value)
                taskCheckListItem.SiteId = Convert.ToInt32(reader["SiteId"].ToString());

            if (reader["CustomerId"] != DBNull.Value)
                taskCheckListItem.CustomerId = Convert.ToInt32(reader["CustomerId"].ToString());

            if (reader["TemplateCheckListItemId"] != DBNull.Value)
                taskCheckListItem.TemplateCheckListItemId = Convert.ToInt32(reader["TemplateCheckListItemId"].ToString());
            if (reader["ParentCheckListId"] != DBNull.Value)
                taskCheckListItem.ParentCheckListId = Convert.ToInt32(reader["ParentCheckListId"].ToString());
            if (reader["ChildCheckListId"] != DBNull.Value)
                taskCheckListItem.ChildCheckListId = (String.IsNullOrEmpty(reader["ChildCheckListId"].ToString())) ? 0: Convert.ToInt32(reader["ChildCheckListId"].ToString());
            if (reader["TemplateCheckListItemNote"] != DBNull.Value)
                taskCheckListItem.TemplateCheckListItemNote = reader["TemplateCheckListItemNote"].ToString();
            if (reader["IsChecked"] != DBNull.Value)
                taskCheckListItem.IsChecked = Convert.ToBoolean(reader["IsChecked"].ToString().ToLower());
            if (reader["CreatedDate"] != DBNull.Value)
                taskCheckListItem.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            if (reader["Updateddate"] != DBNull.Value)
                taskCheckListItem.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
            if (reader["CreatedBy"] != DBNull.Value)
                taskCheckListItem.CreatedBy = reader["CreatedBy"].ToString();
            //
            // Mapping Fields
            if (reader["Name"] != DBNull.Value)
                taskCheckListItem.Name = reader["Name"].ToString();
            if (reader["Quantity"] != DBNull.Value)
                taskCheckListItem.Quantity = Convert.ToInt32(reader["Quantity"].ToString());
            if (reader["EntryQuantity"] != DBNull.Value)
                taskCheckListItem.EntryQuantity = Convert.ToInt32(reader["EntryQuantity"].ToString());
            if (reader["ExitQuantity"] != DBNull.Value)
                taskCheckListItem.ExitQuantity = Convert.ToInt32(reader["ExitQuantity"].ToString());
            if (reader["CheckListItemType"] != DBNull.Value)
                taskCheckListItem.CheckListItemType = Convert.ToInt32(reader["CheckListItemType"].ToString());

            if (columns.Contains("CheckListLocation") && reader["CheckListLocation"] != DBNull.Value)
            {
                if (!string.IsNullOrEmpty(reader["CheckListLocation"].ToString()))
                    taskCheckListItem.CheckListLocation = reader["CheckListLocation"].ToString();
            }
            if (columns.Contains("DocumentPath") && reader["DocumentPath"] != DBNull.Value)
            {
                if (!string.IsNullOrEmpty(reader["DocumentPath"].ToString()))
                    taskCheckListItem.DocumentPath = reader["DocumentPath"].ToString();
            } 
            if (columns.Contains("IsCamera") && reader["IsCamera"] != DBNull.Value)
            {
                if (!string.IsNullOrEmpty(reader["IsCamera"].ToString()))
                    taskCheckListItem.IsCamera = Convert.ToBoolean(reader["IsCamera"].ToString());
            }
            taskCheckListItem.IsCameraDisplay = "";
            if (!taskCheckListItem.IsCamera)
            {
                taskCheckListItem.IsCameraDisplay = "display:none;";
            }
            taskCheckListItem.IsValueDisplay = "";
            taskCheckListItem.IsLocationDisplay = "";
            if (taskCheckListItem.Quantity == 0)
            {
                taskCheckListItem.IsValueDisplay = "style=display:none;";
            }
            if (string.IsNullOrEmpty(taskCheckListItem.CheckListLocation))
            {
                taskCheckListItem.IsLocationDisplay = "style=display:none;";
            }
            if (taskCheckListItem.IsChecked)
            {
                taskCheckListItem.IsCheck = "checked";
            }
            
            return taskCheckListItem;
        }

        public static bool InsertorUpdateTaskCheckListItem(TaskCheckList taskCheckListItem, string dbConnection,
            string auditDbConnection = "", bool isAuditEnabled = true)
        {
            if (taskCheckListItem != null)
            {
                int id = taskCheckListItem.Id;
                var action = (id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate; 

                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateTaskCheckList", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = taskCheckListItem.Id;

                    cmd.Parameters.Add(new SqlParameter("@TaskId", SqlDbType.Int));
                    cmd.Parameters["@TaskId"].Value = taskCheckListItem.TaskId;

                                        cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = taskCheckListItem.CustomerId;
                                        cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = taskCheckListItem.SiteId;

                     

                    cmd.Parameters.Add(new SqlParameter("@TemplateCheckListItemId", SqlDbType.Int));
                    cmd.Parameters["@TemplateCheckListItemId"].Value = taskCheckListItem.TemplateCheckListItemId;

                    if (taskCheckListItem.CheckListItemType != 5)
                    {
                        cmd.Parameters.Add(new SqlParameter("@TemplateCheckListItemNote", SqlDbType.NVarChar));
                        cmd.Parameters["@TemplateCheckListItemNote"].Value = taskCheckListItem.TemplateCheckListItemNote;
                    }
                    else
                    {
                        var checklistchilditems = TaskCheckList.GetTaskCheckListItemById(taskCheckListItem.Id, dbConnection);

                        if (!string.IsNullOrEmpty(checklistchilditems.TemplateCheckListItemNote))
                        {
                            cmd.Parameters.Add(new SqlParameter("@TemplateCheckListItemNote", SqlDbType.NVarChar));
                            cmd.Parameters["@TemplateCheckListItemNote"].Value = checklistchilditems.TemplateCheckListItemNote;
                        }
                    }
                    cmd.Parameters.Add(new SqlParameter("@IsChecked", SqlDbType.Bit));
                    cmd.Parameters["@IsChecked"].Value = taskCheckListItem.IsChecked;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = taskCheckListItem.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = taskCheckListItem.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = taskCheckListItem.CreatedBy;

                    var returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                    if (id == 0)
                        id = (int)returnParameter.Value;

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            taskCheckListItem.Id = id;
                            var audit = new TaskCheckListAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, taskCheckListItem.UpdatedBy);
                            Reflection.CopyProperties(taskCheckListItem, audit);
                            TaskCheckListAudit.InsertTaskCheckListItem(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertOrUpdateTaskCheckList", ex, dbConnection);
                    }
                }
            }
            return true;
        }

        public static bool InsertorUpdateTaskCheckListItem4(TaskCheckList taskCheckListItem, string dbConnection,
    string auditDbConnection = "", bool isAuditEnabled = true)
        {
            if (taskCheckListItem != null)
            {
                int id = taskCheckListItem.Id;
                var action = (id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;

                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateTaskCheckList", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = taskCheckListItem.Id;

                    cmd.Parameters.Add(new SqlParameter("@TaskId", SqlDbType.Int));
                    cmd.Parameters["@TaskId"].Value = taskCheckListItem.TaskId;

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = taskCheckListItem.CustomerId;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = taskCheckListItem.SiteId;

                    cmd.Parameters.Add(new SqlParameter("@TemplateCheckListItemId", SqlDbType.Int));
                    cmd.Parameters["@TemplateCheckListItemId"].Value = taskCheckListItem.TemplateCheckListItemId;

                    cmd.Parameters.Add(new SqlParameter("@TemplateCheckListItemNote", SqlDbType.NVarChar));
                    cmd.Parameters["@TemplateCheckListItemNote"].Value = taskCheckListItem.TemplateCheckListItemNote;

                    cmd.Parameters.Add(new SqlParameter("@IsChecked", SqlDbType.Bit));
                    cmd.Parameters["@IsChecked"].Value = taskCheckListItem.IsChecked;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = taskCheckListItem.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = taskCheckListItem.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = taskCheckListItem.CreatedBy;

                    var returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                    if (id == 0)
                        id = (int)returnParameter.Value;

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            taskCheckListItem.Id = id;
                            var audit = new TaskCheckListAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, taskCheckListItem.UpdatedBy);
                            Reflection.CopyProperties(taskCheckListItem, audit);
                            TaskCheckListAudit.InsertTaskCheckListItem(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertOrUpdateTaskCheckList", ex, auditDbConnection);
                    }
                }
            }
            return true;
        }

        public static List<TaskCheckList> GetAllTaskChecklist(string dbConnection)
        {
            var taskCheckListItems = new List<TaskCheckList>();
            List<TaskCheckList> parentCheckListItems = new List<TaskCheckList>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllTaskChecklist", connection);

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var taskCheckListItem = GetTaskCheckListItemFromSqlReader(reader);
                    taskCheckListItems.Add(taskCheckListItem);
                }

                if (taskCheckListItems.Count > 0)
                { 
                    foreach (var taskCheckListItem in taskCheckListItems)
                    {
                        if (taskCheckListItem.ChildCheckListId == 0)
                        {
                            taskCheckListItem.ChildCheckList = new List<TaskCheckList>();
                            
                            foreach (var taskCheckLstItem in taskCheckListItems)
                            {
                                if (taskCheckLstItem.ChildCheckListId == taskCheckListItem.TemplateCheckListItemId)
                                {
                                    taskCheckListItem.ChildCheckList.Add(taskCheckLstItem);
                                }
                            }
                            parentCheckListItems.Add(taskCheckListItem);
                        }
                    }
                }


                connection.Close();
                reader.Close();
            }
            return parentCheckListItems;
        }
        
    }
}
