﻿<%@ Page Title="Log In" EnableEventValidation="false" Language="C#" AutoEventWireup="true"
    CodeBehind="Login.aspx.cs" Inherits="ArrowLabs.Licence.Portal.Account.Login" %>
<head>
     <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="initial-scale=1,minimum-scale=1,maximum-scale=1,width=device-width,height=device-height,user-scalable=yes">

  <title>Signin Page</title>
        <link rel="apple-touch-icon" sizes="57x57" href="https://testportalcdn.azureedge.net/Images/favicons/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="https://testportalcdn.azureedge.net/Images/favicons/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="https://testportalcdn.azureedge.net/Images/favicons/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="https://testportalcdn.azureedge.net/Images/favicons/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="https://testportalcdn.azureedge.net/Images/favicons/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="https://testportalcdn.azureedge.net/Images/favicons/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="https://testportalcdn.azureedge.net/Images/favicons/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="https://testportalcdn.azureedge.net/Images/favicons/apple-EnterKeyFiltertouch-icon-152x152.png">
    <link rel="icon" type="image/png" href="https://testportalcdn.azureedge.net/Images/favicons/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="https://testportalcdn.azureedge.net/Images/favicons/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="https://testportalcdn.azureedge.net/Images/favicons/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="https://testportalcdn.azureedge.net/Images/favicons/manifest.json">
    <meta name="msapplication-TileColor" content="#2d89ef">
    <meta name="msapplication-TileImage" content="https://testportalcdn.azureedge.net/Images/favicons/mstile-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="https://testportalcdn.azureedge.net/styles/bootstrap-select.css">
    <link rel="stylesheet" href="https://testportalcdn.azureedge.net/styles/bootstrap.css">
   <link rel="stylesheet" href="https://testportalcdn.azureedge.net/styles/dependencies.css">
    <link rel="stylesheet" href="https://testportalcdn.azureedge.net/styles/wrapkit.css">
    <link rel="stylesheet" href="https://testportalcdn.azureedge.net/styles/components.css">
    <link rel="stylesheet" href="https://testportalcdn.azureedge.net/styles/styles.css">
    <link rel="stylesheet" href="https://testportalcdn.azureedge.net/styles/styles-media.css">
    <link rel="stylesheet" href="https://testportalcdn.azureedge.net/styles/sweetalert.css">
    <script src="https://testportalcdn.azureedge.net/Scripts/jquery-2.1.4.min.js"></script>
          <style>

              #pswd_info {
    position:absolute;
    bottom:-130px;
    bottom: -115px\9; /* IE Specific */
    right:55px;
    width:250px;
    padding:15px;
    background:#fefefe;
    font-size:.875em;
    border-radius:5px;
    box-shadow:0 1px 3px #ccc;
    border:1px solid #ddd;
}
              #pswd_info h4 {
    margin:0 0 10px 0;
    padding:0;
    font-weight:normal;
}
              #pswd_info::before {
    content: "\25B2";
    position:absolute;
    top:-12px;
    left:45%;
    font-size:14px;
    line-height:14px;
    color:#ddd;
    text-shadow:none;
    display:block;
}
#pswd_info {
    display:none;
}
              .invalid {
    /*background:url(../images/invalid.png) no-repeat 0 50%;*/
    padding-left:22px;
    line-height:24px;
    color:#ec3f41;
}
.valid {
    /*background:url(../images/valid.png) no-repeat 0 50%;*/
    padding-left:22px;
    line-height:24px;
    color:#3a7d34;
}

    .modal {
        display: none;
        position: fixed;
        z-index: 1000;
        top: 0;
        left: 0;
        height: 100%;
        width: 100%;
        background: rgba( 255, 255, 255, .8 ) url('https://testportalcdn.azureedge.net/FhHRx.gif') 50% 50% no-repeat;
    }

    body.loading {
        overflow: hidden;
    }

        body.loading .modal {
            display: block;
        }
</style>
</head>
<script type="text/javascript">

    function isInvalidEmail(inputtxt) {
        try {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if(re.test(inputtxt))
            {
                return false;
            }
            else
            {
                showAlert('Is invalid email address');
                return true;
            }

        }
        catch (err) {
            //alert(err)
        }
    }
    function isEmptyOrSpaces(str) {
        return str === null || str.match(/^ *$/) !== null;
    }
    function isNumeric(obj) {
        return !jQuery.isArray(obj) && (obj - parseFloat(obj) + 1) >= 0;
    }

    function changePassword() {
        var confPw = document.getElementById("newPwInput").value;
        jQuery.ajax({
            type: "POST",
            url: "Login.aspx/forgotPW",
            data: "{'password':'" + confPw + "'}",
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d =="SUCCESS") {
                    jQuery('#forgotModal').modal('hide');
                    
                    showAlert("Successfully sent password to " + confPw);

                    document.getElementById("newPwInput").value = "";
                }
                else {
                    showAlert(data.d);
                }
            }
        });
    }
    jQuery(document).ready(function () {
        localStorage.removeItem("activeTabDev");
        localStorage.removeItem("activeTabInci");
        localStorage.removeItem("activeTabMessage");
        localStorage.removeItem("activeTabTask");
        localStorage.removeItem("activeTabTick");
        localStorage.removeItem("activeTabUB");
        localStorage.removeItem("activeTabVer");
        localStorage.removeItem("activeTabLost");
    });
    function showAlert(message) {
        swal({
            title: "Information",
            text: message,
            confirmButtonText: "OK",
            customClass: 'twitter'
        });
    }
    function nextClick() {
        var isErr = false;
        if (isEmptyOrSpaces(document.getElementById('addUserNameInput').value)) {
            isErr = true;
            showAlert('Provide your first name');
        }
        if (!isErr) {
            if (isEmptyOrSpaces(document.getElementById("addLastNameInput").value)) {
                isErr = true;
                showAlert('Provide your last name');
            }
        }
        if (!isErr) {
            if (isEmptyOrSpaces(document.getElementById('addAddressInput').value)) {
                isErr = true;
                showAlert('Provide your address');
            }
        }
        if (!isErr) {
            if (document.getElementById('countrySelect').value == "Country") {
                isErr = true;
                showAlert('Please select country');
            }
        }
        if (!isErr) {
            if (!isEmptyOrSpaces(document.getElementById('addEmailAddInput').value)) {
                if (isInvalidEmail(document.getElementById('addEmailAddInput').value)) {
                    isErr = true;
                }
            }
            else {
                isErr = true;
                showAlert('Provide email address');
            }
        }
        if (!isErr) {
            if (!isEmptyOrSpaces(document.getElementById('addPhoneNoInput').value)) {
                if (!isNumeric(document.getElementById('addPhoneNoInput').value)) {
                    isErr = true;
                    showAlert('Provide numeric phone number');
                }
            }
            else {
                isErr = true;
                showAlert('Provide phone number');
            }
        }

        if (!isErr) {
            if (isEmptyOrSpaces(document.getElementById('addPassInput').value)) {
                isErr = true;
                showAlert('Provide your password');
            }
        }

        if (!isErr) {

            if (!letterGood) {
                showAlert('Password does not contain letter');
                isErr = true;
            }
            if (!isErr) {
                if (!capitalGood) {
                    showAlert('Password does not contain capital letter');
                    isErr = true;
                }
            }
            if (!isErr) {
                if (!numGood) {
                    showAlert('Password does not contain number');
                    isErr = true;
                }
            }
            if (!isErr) {
                if (!lengthGood) {
                    showAlert('Password length not enough');
                    isErr = true;
                }
            }
        }

        if (!isErr) {
            if (isEmptyOrSpaces(document.getElementById('addConfPInput').value)) {
                isErr = true;
                showAlert('Provide your matching password');
            }
        }
        if (!isErr) {
            if (document.getElementById('addPassInput').value != document.getElementById('addConfPInput').value) {
                isErr = true;
                showAlert('Password does not match');
            }
        }
        if (!isErr) {
            if (!isEmptyOrSpaces(document.getElementById('addTotalInput').value)) {
                if (!isNumeric(document.getElementById('addTotalInput').value)) {
                    isErr = true;
                    showAlert('Provide numeric total user');
                }
            }
            else {
                isErr = true;
                showAlert('Provide total user');
            }
        }
        if (!isErr) {
            document.getElementById('step1UL').style.display = "none";
            document.getElementById('step1DIV').style.display = "none";
            document.getElementById('step2UL').style.display = "block";
            document.getElementById('step2DIV').style.display = "block";
        }
    }
    function backClick() {
        document.getElementById('step1UL').style.display = "block";
        document.getElementById('step1DIV').style.display = "block";
        document.getElementById('step2UL').style.display = "none";
        document.getElementById('step2DIV').style.display = "none";
    }
    function saveInfo() {
        try {
            //isEmptyOrSpaces
            var isErr = false;
            if (isEmptyOrSpaces(document.getElementById('addUserNameInput').value)) {
                isErr = true;
                showAlert('Provide your first name');
            }
            if (isEmptyOrSpaces(document.getElementById("addLastNameInput").value)) {
                isErr = true;
                showAlert('Provide your last name');
            }
            if (!isErr) {
                if (isEmptyOrSpaces(document.getElementById('addAddressInput').value)) {
                    isErr = true;
                    showAlert('Provide your address');
                }
            }
            if (!isErr) {
                if (document.getElementById('countrySelect').value == "Country") {
                    isErr = true;
                    showAlert('Please select country');
                }
            }
            if (!isErr) {
                if (!isEmptyOrSpaces(document.getElementById('addEmailAddInput').value)) {
                    if (isInvalidEmail(document.getElementById('addEmailAddInput').value)) {
                        isErr = true;
                    }
                }
                else {
                    isErr = true;
                    showAlert('Provide email address');
                }
            }
            if (!isErr) {
                if (!isEmptyOrSpaces(document.getElementById('addPhoneNoInput').value)) {
                    if (!isNumeric(document.getElementById('addPhoneNoInput').value)) {
                        isErr = true;
                        showAlert('Provide numeric phone number');
                    }
                }
                else {
                    isErr = true;
                    showAlert('Provide phone number');
                }
            }

            if (!isErr) {
                if (isEmptyOrSpaces(document.getElementById('addPassInput').value)) {
                    isErr = true;
                    showAlert('Provide your password');
                }
            } 
            if (!isErr) {
                if (isEmptyOrSpaces(document.getElementById('addConfPInput').value)) {
                    isErr = true;
                    showAlert('Provide your matching password');
                }
            }
             

            if (!isErr) {
                if (!isEmptyOrSpaces(document.getElementById('addTotalInput').value)) {
                    if (!isNumeric(document.getElementById('addTotalInput').value)) {
                        isErr = true;
                        showAlert('Provide numeric total user');
                    }
                }
                else {
                    isErr = true;
                    showAlert('Provide total user');
                }
            }
            if (!isErr) {
                if (document.getElementById('addPassInput').value != document.getElementById('addConfPInput').value) {
                    isErr = true;
                    showAlert('Password does not match');
                }
            }

            if (!isErr) {
                jQuery.ajax({
                    type: "POST",
                    url: "Login.aspx/saveInfo",
                    data: "{'name':'" + document.getElementById('addUserNameInput').value + "','address':'" + document.getElementById('addAddressInput').value
                            + "','country':'" + document.getElementById('countrySelect').value + "','email':'" + document.getElementById('addEmailAddInput').value
                            + "','phoneno':'" + document.getElementById('addPhoneNoInput').value + "','total':'" + document.getElementById('addTotalInput').value
                            + "','notimodule':'" + document.getElementById("notificationModule").checked + "','incimodule':'" + document.getElementById("incidentModule").checked
                            + "','taskmodule':'" + document.getElementById("taskModule").checked + "','ticketmodule':'" + document.getElementById("ticketModule").checked
                            + "','postmodule':'" + document.getElementById("postOrderModule").checked + "','verimodule':'false','chatmodule':'" + document.getElementById("chatModule").checked + "','drmodule':'" + document.getElementById("DRModule").checked
                            + "','lfmodule':'" + document.getElementById("LostFoundModule").checked + "','survmodule':'" + document.getElementById("surveilModule").checked + "','reqmodule':'" + document.getElementById("requestModule").checked + "','collabmodule':'" + document.getElementById("collabModule").checked
                            + "','dispatchmodule':'" + document.getElementById("dispatchModule").checked + "','locationmodule':'" + document.getElementById("locationModule").checked
                            + "','waremodule':'" + document.getElementById("warehouseModule").checked + "','password':'" + document.getElementById("addConfPInput").value
                            + "','lastname':'" + document.getElementById("addLastNameInput").value + "','activity':'" + document.getElementById("activityModule").checked
                            + "'}", 
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d == "SUCCESS") {
                            showAlert('Successfully registered.');
                            jQuery('#newUser').modal('hide');
                        }
                        else if (data.d == "A confirmation email will be sent to you shortly.") {
                            showAlert(data.d);
                            jQuery('#newUser').modal('hide');
                        }
                        else
                            showAlert(data.d);
                    }
                });
            }
        }
        catch(err){alert(err)}
    }
    var sCount = 0;
    function thisImgClick(e) {
        if (e.style.filter == "grayscale(1)") {
            e.style.filter = "none";
            e.setAttribute("style", "-webkit-filter:grayscale(0)");
            e.style.width = "90px";
            document.getElementById(e.id + "Module").checked = true;
            sCount++;
            if (sCount > 0)
                document.getElementById("selectDIV").style.display = "block";
                
            document.getElementById("selectCount").innerHTML = sCount;
            
        }
        else {
            e.style.filter = "gray";
            e.setAttribute("style", "-webkit-filter:grayscale(1)");
            e.style.width = "90px";
            document.getElementById(e.id + "Module").checked = false;
            sCount = sCount-1;
            if(sCount == 0)
                document.getElementById("selectDIV").style.display = "none";

            document.getElementById("selectCount").innerHTML = sCount;
        }
    }
    var firstClick = false;
    var lengthGood = false;
    var letterGood = false;
    var capitalGood = false;
    var numGood = false;
    function clearFields() {
        document.getElementById("newPwInput").value = "";
        if (!firstClick) {
            $('input[type=password]').keyup(function () {
                // keyup event code here
                var pswd = $(this).val();
                if (pswd.length < 8) {
                    $('#length').removeClass('valid').addClass('invalid');
                    lengthGood = false;
                } else {
                    $('#length').removeClass('invalid').addClass('valid');
                    lengthGood = true;
                }
                //validate letter
                if (pswd.match(/[A-z]/)) {
                    $('#letter').removeClass('invalid').addClass('valid');
                    letterGood = true;
                    
                } else {
                    $('#letter').removeClass('valid').addClass('invalid');
                    letterGood = false;
                }

                //validate capital letter
                if (pswd.match(/[A-Z]/)) {
                    $('#capital').removeClass('invalid').addClass('valid');
                    capitalGood = true;
                } else {
                    $('#capital').removeClass('valid').addClass('invalid');
                    
                    capitalGood = false;
                }

                //validate number
                if (pswd.match(/\d/)) {
                    $('#number').removeClass('invalid').addClass('valid');
                    numGood = true;
                   
                } else {
                    $('#number').removeClass('valid').addClass('invalid');
                    numGood = false;
                }
            });
            $('input[type=password]').focus(function () {
                // focus code here
                $('#pswd_info').show();
            });
            $('input[type=password]').blur(function () {
                // blur code here
                $('#pswd_info').hide();
            });
            firstClick = true;
        }
        document.getElementById("selectDIV").style.display = "none";

        sCount = 0;

        document.getElementById("notification").setAttribute("style", "-webkit-filter:grayscale(1)");
        document.getElementById("incident").setAttribute("style", "-webkit-filter:grayscale(1)");
        document.getElementById("task").setAttribute("style", "-webkit-filter:grayscale(1)");
        document.getElementById("ticket").setAttribute("style", "-webkit-filter:grayscale(1)");
        document.getElementById("postOrder").setAttribute("style", "-webkit-filter:grayscale(1)");

        document.getElementById("activity").setAttribute("style", "-webkit-filter:grayscale(1)");
        document.getElementById("activityModule").checked = false;
        document.getElementById("activity").style.width = "90px";
        

        document.getElementById("postOrderModule").checked = false;

        document.getElementById("ticketModule").checked = false;

        document.getElementById("taskModule").checked = false;

        document.getElementById("incidentModule").checked = false;

        document.getElementById("notificationModule").checked = false;

        document.getElementById("notification").style.width = "90px";
        document.getElementById("incident").style.width = "90px";
        document.getElementById("task").style.width = "90px";
        document.getElementById("ticket").style.width = "90px";
        document.getElementById("postOrder").style.width = "90px";

        document.getElementById("DR").setAttribute("style", "-webkit-filter:grayscale(1)");
        document.getElementById("DR").style.width = "90px";

        document.getElementById("DRModule").checked = false;

        document.getElementById("LostFoundModule").checked = false;

        document.getElementById("locationModule").checked = false;

        document.getElementById("requestModule").checked = false;

        document.getElementById("collabModule").checked = false;

        document.getElementById("LostFound").setAttribute("style", "-webkit-filter:grayscale(1)");
        document.getElementById("LostFound").style.width = "90px";

        document.getElementById("location").setAttribute("style", "-webkit-filter:grayscale(1)");
        document.getElementById("location").style.width = "90px";

        document.getElementById("request").setAttribute("style", "-webkit-filter:grayscale(1)");
        document.getElementById("request").style.width = "90px";

        document.getElementById("collab").setAttribute("style", "-webkit-filter:grayscale(1)");
        document.getElementById("collab").style.width = "90px";

        document.getElementById("surveil").setAttribute("style", "-webkit-filter:grayscale(1)");
        document.getElementById("surveil").style.width = "90px";
         
        document.getElementById("surveilModule").checked = false;
         
        document.getElementById("dispatchModule").checked = false;

        document.getElementById("warehouseModule").checked = false;

        document.getElementById("chatModule").checked = false;

        document.getElementById("dispatch").setAttribute("style", "-webkit-filter:grayscale(1)");
        document.getElementById("dispatch").style.width = "90px";

        document.getElementById("warehouse").setAttribute("style", "-webkit-filter:grayscale(1)");
        document.getElementById("warehouse").style.width = "90px";

        document.getElementById("chat").setAttribute("style", "-webkit-filter:grayscale(1)");
        document.getElementById("chat").style.width = "90px";
        
        document.getElementById('addUserNameInput').value = "";
        document.getElementById('addAddressInput').value = "";
        document.getElementById('countrySelect').value = "Country";
        document.getElementById('addEmailAddInput').value = "";
        document.getElementById('addPhoneNoInput').value = "";
        document.getElementById('addTotalInput').value = "";

        document.getElementById('addPassInput').value = "";
        document.getElementById('addConfPInput').value = "";
        
        backClick();


        document.getElementById("notificationModule").checked = false;

        document.getElementById("incidentModule").checked = false;
        
        document.getElementById("taskModule").checked = false;

        document.getElementById("ticketModule").checked = false;
        
        document.getElementById("postOrderModule").checked = false;
        
        document.getElementById("verifierModule").checked = false;
        
        document.getElementById("chatModule").checked = false;
        
        document.getElementById("DRModule").checked = false;
        
        document.getElementById("LostFoundModule").checked = false;
        
        document.getElementById("surveilModule").checked = false;
        
        document.getElementById("requestModule").checked = false;
        
        document.getElementById("collabModule").checked = false;
        
        document.getElementById("dispatchModule").checked = false;
        
        document.getElementById("locationModule").checked = false;
        
        document.getElementById("warehouseModule").checked = false;
        
    }
<%--    function ddl_changed(ddl) {
        //alert(ddl.value);
        var myHidden = document.getElementById('<%= languageHidden.ClientID %>');

        if (myHidden)//checking whether it is found on DOM, but not necessary
        {
            myHidden.value = ddl.value;
        }
    }--%>
    </script>
<body class="login">
    <form runat="server">
  <!--[if lt IE 9]>
  <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
  <![endif]-->

  <main class="signin-wrapper">
    <div class="tab-content">
      <p class="text-center p-4x">
        <img src="https://testportalcdn.azureedge.net/Images/custom-images/site-login-logo.png" alt="" height="71px">
      </p>
      <div class="tab-pane fade in active" id="signin">
            <asp:Login ID="LoginUser" runat="server" EnableViewState="false" 
        RenderOuterTable="false" onauthenticate="LoginUser_Authenticate" DisplayRememberMe="true"
         >
        <LayoutTemplate>
            <span class="failureNotification">
                <asp:Literal ID="FailureText" runat="server"></asp:Literal>
            </span>
            <asp:ValidationSummary ID="LoginUserValidationSummary" runat="server" CssClass="failureNotification" 
                 ValidationGroup="LoginUserValidationGroup"/>
<%--          <p class="login-header">SIGN INTO MIMS</p>--%>
            <div class="form-group">
            <div class="input-group input-group-in mb-2x">
              <span class="input-group-addon"><i class="icon-user"></i></span>
                        <asp:TextBox style="border:none;width:95%;height:40px;" ID="UserName" runat="server" CssClass="textEntry" AutoCompleteType="Disabled" placeholder="Username"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName" 
                             CssClass="failureNotification" ErrorMessage="User Name is required." ToolTip="User Name is required." 
                             ValidationGroup="LoginUserValidationGroup">*</asp:RequiredFieldValidator>
            </div>
          </div><!-- /.form-group -->
          <div class="form-group">
            <div class="input-group input-group-in mb-2x">
              <span class="input-group-addon"><i class="icon-lock"></i></span>
                        <asp:TextBox ID="Password" style="border:none;width:95%;height:40px;" runat="server" CssClass="passwordEntry" placeholder="Password" TextMode="Password"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password" 
                             CssClass="failureNotification" ErrorMessage="Password is required." ToolTip="Password is required." 
                             ValidationGroup="LoginUserValidationGroup">*</asp:RequiredFieldValidator>
          </div>
          </div><!-- /.form-group -->
                      <div class="form-group" style="display:none;">
            <div class="input-group input-group-in mb-2x">
              <span class="input-group-addon"><i class="fa fa-globe"></i></span>
                     <asp:DropDownList style="border:none;width:98%;height:40px;"  onchange="ddl_changed(this)" id="languageSelect" runat="server">
                     </asp:DropDownList>
          </div>
          </div><!-- /.form-group -->


            <div class="row" style="margin-left:-12px;">
                <div style="margin-top:10px;" class="form-group clearfix col-md-6">
                    <div class="animated-hue text-center">
                    <asp:Button class="btn btn-primary" ID="LoginButton" runat="server" CommandName="Login" Text="SIGN IN" 
                        ValidationGroup="LoginUserValidationGroup"/>
                    </div>
             </div><!-- /.form-group -->
                            <div style="margin-top:10px;" class="form-group clearfix col-md-6">
                    <div class="animated-hue text-center">
                    <asp:Button class="btn btn-primary"  ID="RegisterButton" runat="server" onclientclick="clearFields();jQuery('#newUser').modal('show');return false;" Text="REGISTER" 
                        />
                    </div>
             </div><!-- /.form-group -->
                </div>
            <div class="row" style="
    margin-top: 15px;margin-left:5px;
">
<div class="col-md-6">
            <div style="margin-left:-7px;"><asp:CheckBox ID="RememberMe"  runat="server" Text="Remember me" /></div>
</div>
<div class="col-md-6" style="margin-top:3px;margin-left:0px;">
<a style="text-decoration:underline;" href="#" onclick="clearFields();jQuery('#forgotModal').modal('show');">Forgot Password</a>
</div>
            
            </div>
<%--                                        <div style="margin-top:10px;" class="form-group clearfix">
                    <div class="animated-hue text-center">
                    <asp:Button class="btn btn-primary"  ID="ForgotButton" runat="server" onclientclick="clearFields();jQuery('#forgotModal').modal('show');return false;" Text="FORGOT PASSWORD" 
                        />
                    </div>
                <%--<p class="pull-right"><a href="#recoverAccount" data-toggle="modal">Can't Sign In?</a></p>--%>
             <%--</div>--%> 
        </LayoutTemplate>
    </asp:Login>
          <asp:HiddenField ID="languageHidden" Value="Please Select Language" runat="server"/>
      </div><!-- /.tab-pane -->

    </div><!-- /.tab-content -->
<%--    <p class="slogin">Magic is something <span class="red-color">you make</span></p>--%>
  </main><!--/#wrapper-->
        <div aria-hidden="true" aria-labelledby="newUser" role="dialog" tabindex="-1" id="newUser" class="modal fade" style="display: none;">
                <div class="modal-dialog modal-md">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button aria-hidden="true" data-dismiss="modal" class="close" type="button" onclick="clearFields()"><i class="icon_close fa-lg"></i></button>
                      <%--<h4 class="modal-title">REGISTRATION FORM</h4>--%>
                        <img style="height:75px;" src="../Images/site-logo-coloredsmall.png"/>
                    </div>
                    <div class="modal-body">
                        <div class="row" id="step1DIV">
                            <div class="col-md-6 border-right">
                                <div class="row">
                                    <div class="col-md-12">
                                      <%--  <p style="margin-left:5px;" class="red-color">*First Name:</p>--%>
                                        <input placeholder="First Name" id="addUserNameInput" class="form-control">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                 <%--       <p style="margin-left:5px;" class="red-color">*Last Name:</p>--%>
                                        <input placeholder="Last Name" id="addLastNameInput" class="form-control">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                             <%--           <p style="margin-left:5px;" class="red-color">*Address:</p>--%>
                                        <input placeholder="Address" id="addAddressInput" class="form-control">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                    <%--   <p style="margin-left:5px;" class="red-color">*Country:</p>--%>
										<label class="select select-o" >
                                           <select id="countrySelect" class="selectpicker form-control"  data-live-search="true">

<option>Country</option>
<option>Afghanistan (+4:00)</option>
<option>Albania (+1:00)</option>
<option>Algeria (+1:00)</option>
<option>Andorra (+1:00)</option>
<option>Angola (+1:00)</option>
<option>Antigua & Deps (-4:00)</option>
<option>Argentina (-3:00)</option>
<option>Armenia (+4:00)</option>
 
<option>Australia (+10:00)</option>      
                                                                      
<option>Austria (+1:00)</option>
<option>Azerbaijan (+4:00)</option>
<option>Bahamas (-5:00)</option>
<option>Bahrain (+3:00)</option>
<option>Bangladesh (+6:00)</option>
<option>Barbados (−04:00)</option>
<option>Belarus (+03:00) </option>
<option>Belgium (+01:00) </option>
<option>Belize (−06:00)</option>
<option>Benin (+01:00)</option>
<option>Bhutan(+06:00)</option>
<option>Bolivia (−04:00)</option>
<option>Bosnia Herzegovina (+01:00)</option>
<option>Botswana(+02:00)</option>
<option>Brazil(−02:00)</option>
<option>Brunei (+08:00)</option>
<option>Bulgaria (+02:00)</option>
<option>Burkina (+02:00)</option>
<option>Burundi (+02:00)</option>
<option>Cambodia (+07:00)</option>
<option>Cameroon (+01:00)</option>
<option>Canada (−05:00)</option>
<option>Cape Verde (−01:00)</option>
<option>Central African Rep (+01:00)</option>
<option>Chad (+01:00)</option>
<option>Chile (−04:00)</option>
<option>China (+08:00)</option>
<option>Colombia (−05:00)</option>
<option>Comoros (+03:00)</option>
<option>Congo (+01:00)</option>
<option>Costa Rica (−06:00)</option>
<option>Croatia (+01:00)</option>
<option>Cuba (−05:00)</option>
<option>Cyprus (+02:00)</option>
<option>Czech Republic (+01:00)</option>
<option>Denmark (+01:00)</option>
<option>Djibouti (+03:00)</option>
<option>Dominica (−04:00)</option>
<option>Dominican Republic (−04:00)</option>
<option>East Timor (+09:00)</option>
<option>Ecuador (−05:00)</option>
<option>Egypt (+02:00)</option>
<option>El Salvador (−06:00)</option>
<option>Equatorial Guinea (+01:00)</option>
<option>Eritrea (+03:00)</option>
<option>Estonia (+02:00)</option>
<option>Ethiopia (+03:00)</option>
<option>Fiji (+12:00)</option>
<option>Finland (+02:00)</option>
<option>France (+01:00)</option>
<option>Gabon (+01:00)</option>
<option>Gambia (+00:00)</option>
<option>Georgia (+04:00)</option>
<option>Germany (+01:00)</option>
<option>Ghana (+00:00)</option>
<option>Greece (+02:00)</option>
<option>Grenada (−04:00)</option>
<option>Guatemala (−06:00)</option>
<option>Guinea (+00:00)</option>
<option>Guinea-Bissau (+00:00)</option>
<option>Guyana (−04:00)</option>
<option>Haiti (−05:00)</option>
<option>Honduras (−06:00)</option>
<option>Hong Kong(+08:00)</option>
<option>Hungary (+01:00)</option>
<option>Iceland (+00:00)</option>
<option>India (+05:00)</option>
<option>Indonesia (+07:00)</option>
<option>Iran (+03:00)</option>
<option>Iraq (+03:00)</option>
<option>Ireland {Republic} (+00:00)</option>
<option>Israel (+02:00)</option>
<option>Italy (+01:00)</option>
<option>Jamaica (−05:00)</option>
<option>Japan (+09:00)</option>
<option>Jordan (+02:00)</option>
<option>Kazakhstan (+06:00)</option>
<option>Kenya (+03:00)</option>
<option>Kiribati (+12:00)</option>
<option>Korea North (+08:00)</option>
<option>Korea South (+09:00)</option>
<option>Kosovo (+01:00)</option>
<option>Kuwait (+03:00)</option>
<option>Kyrgyzstan (+06:00)</option>
<option>Laos (+07:00)</option>
<option>Latvia (+02:00)</option>
<option>Lebanon (+02:00)</option>
<option>Lesotho (+02:00)</option>
<option>Liberia (+00:00)</option>
<option>Libya (+02:00)</option>
<option>Liechtenstein (+01:00)</option>
<option>Lithuania (02:00)</option>
<option>Luxembourg (+01:00)</option>
<option>Macedonia (+01:00)</option>
<option>Madagascar (+03:00)</option>
<option>Malawi (+02:00)</option>
<option>Malaysia (+08:00)</option>
<option>Maldives (+05:00)</option>
<option>Mali (+00:00)</option>
<option>Malta (+01:00)</option>
<option>Marshall Islands (+12:00)</option>
<option>Mauritania (+00:00)</option>
<option>Mauritius (+04:00)</option>
<option>Mexico (−06:00 )</option>
<option>Moldova (+02:00)</option>
<option>Monaco (+01:00)</option>
<option>Mongolia (+08:00)</option>
<option>Montenegro (+01:00)</option>
<option>Morocco (+00:00)</option>
<option>Mozambique (+02:00)</option>
<option>Myanmar (+06:00)</option>
<option>Namibia (+01:00)</option>
<option>Nauru (+12:00)</option>
<option>Nepal (+05:45 )</option>
<option>Netherlands (+01:00)</option>
<option>New Zealand (+12:00)</option>
<option>Nicaragua (−06:00)</option>
<option>Niger (+01:00)</option>
<option>Nigeria (+01:00)</option>
<option>Norway (+01:00)</option>
<option>Oman (04:00)</option>
<option>Pakistan (+05:00)</option>
<option>Palau (+09:00)</option>
<option>Panama (−05:00)</option>
<option>Papua New Guinea (+10:00)</option>
<option>Paraguay (−04:00)</option>
<option>Peru (−05:00)</option>
<option>Philippines (+08:00)</option>
<option>Poland (+01:00)</option>
<option>Portugal (+00:00)</option>
<option>Qatar (+03:00)</option>
<option>Romania (+02:00)</option>
<option>Russian Federation (+03:00)</option>
<option>Rwanda (+02:00)</option>
<option>St Kitts & Nevis (04:00)</option>
<option>St Lucia (−04:00)</option>
<option>Saint Vincent & the Grenadines (−04:00)</option>
<option>Samoa (+13:00)</option>
<option>San Marino (+01:00)</option>
<option>Saudi Arabia (03:00)</option>
<option>Senegal (+00:00)</option>
<option>Serbia (+01:00)</option>
<option>Seychelles (+04:00 )</option>
<option>Sierra Leone (+00:00)</option>
<option>Singapore (+08:00)</option>
<option>Slovakia (+01:00)</option>
<option>Slovenia (+01:00)</option>
<option>Solomon Islands (+11:00)</option>
<option>Somalia (+03:00)</option>
<option>South Africa (+02:00)</option>
<option>South Sudan (+03:00)</option>
<option>Spain (+00:00)</option>
<option>Sri Lanka (+05:00)</option>
<option>Sudan (+03:00)</option>
<option>Suriname (−03:00)</option>
<option>Swaziland (+02:00)</option>
<option>Sweden (+01:00)</option>
<option>Switzerland (+01:00)</option>
<option>Syria (+02:00)</option>
<option>Taiwan (+08:00)</option>
<option>Tajikistan (+05:00)</option>
<option>Tanzania (03:00)</option>
<option>Thailand (+07:00)</option>
<option>Togo (+00:00)</option>
<option>Tonga (+13:00)</option>
<option>Trinidad & Tobago (04:00)</option>
<option>Tunisia (+01:00)</option>
<option>Turkey (+03:00)</option>
<option>Turkmenistan (+05:00)</option>
<option>Tuvalu (+12:00)</option>
<option>Uganda (+03:00)</option>
<option>Ukraine (+02:00</option>
<option>United Arab Emirates (+04:00)</option>
<option>United Kingdom (+00:00)</option>
<option>United States (-05:00) </option>
<option>Uruguay (−03:00)</option>
<option>Uzbekistan (+05:00)</option>
<option>Vanuatu (+11:00)</option>
<option>Vatican City (+01:00)</option>
<option>Venezuela (−04:00)</option>
<option>Vietnam (+07:00)</option>
<option>Yemen (+03:00)</option>
<option>Zambia (+02:00)</option>
<option>Zimbabwe (+02:00 )</option>
											 
											
											</select>
										 </label>
                                    </div>
                                </div>
     
                                                                                                           
                            </div>
                            <div class="col-md-6 ">
                                <div class="row">
                                    <div class="col-md-12">
                                        <%--<p style="margin-left:5px;" class="red-color">*Email Address:</p>--%>
                                        <input placeholder="Email Address" id="addEmailAddInput" class="form-control">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                       <%-- <p style="margin-left:5px;" class="red-color">*Phone Number:</p>--%>
                                        <input placeholder="Phone Number"  id="addPhoneNoInput" class="form-control">
                                    </div>
                                </div>    
                                <div class="row">
                                    <div class="col-md-12">
                                        <%--<p style="margin-left:5px;" class="red-color">*Password:</p>--%>
                                        <input type="password" placeholder="Password"  id="addPassInput" class="form-control"> 
                                    </div>
                                </div> 
                                <div class="row">
                                    <div class="col-md-12">
                                        <%--<p style="margin-left:5px;" class="red-color">*Confirm Password:</p>--%>
                                        <input type="password" placeholder="Confirm Password"  id="addConfPInput" class="form-control">
                                    </div>
                                </div>  
                                                       
                            </div>
                                           <div class="row">
                                    <div class="col-md-12">
                                     <%--   <p style="margin-left:5px;" class="red-color">*Total User License:</p>--%>
                                        <input  placeholder="Total User License"  id="addTotalInput" class="form-control"> 
                                    </div>
                                </div>     
                            <div class="row">
                                <div id="pswd_info">
    <h4>Password must meet the following requirements:</h4>
    <ul>
        <li id="letter" class="invalid">At least <strong>one letter</strong></li>
        <li id="capital" class="invalid">At least <strong>one capital letter</strong></li>
        <li id="number" class="invalid">At least <strong>one number</strong></li>
        <li id="length" class="invalid">Be at least <strong>8 characters</strong></li>
    </ul>
</div>
                            </div>
                        </div>

                        <div class="row" style="display:none;" id="step2DIV"> 
 
                            <div class="row text-center"> 
                               <div class="col-md-2" style="width:20%;padding:0px;"> 
                                   <img style="height:90px;" id="notification" onclick="thisImgClick(this)" src="../Images/Notification.png"/>
                               </div>
                                <div class="col-md-2" style="width:20%;padding:0px;">
                                    <img style="height:90px;" id="incident" onclick="thisImgClick(this)"  src="../Images/IncidentModule.png"/>
                               </div>
                                <div class="col-md-2" style="width:20%;padding:0px;">
                                    <img style="height:90px;" id="task" onclick="thisImgClick(this)" src="../Images/Tasking.png"/>
                               </div>
                                <div class="col-md-2" style="width:20%;padding:0px;">
                                    <img style="height:90px;" id="ticket" onclick="thisImgClick(this)" src="../Images/Ticketing.png"/>
                               </div>
                                <div class="col-md-2" style="width:20%;padding:0px;">
                                    <img style="height:90px;" id="postOrder" onclick="thisImgClick(this)" src="../Images/PostOrder.png"/>
                               </div>

                            </div>
                            <div class="row text-center"  >
                               <div class="col-md-2" style="width:20%;padding:0px;">
                                   <img style="height:90px;" id="DR" onclick="thisImgClick(this)" src="../Images/DutyRoster.png"/>
                               </div>
                                <div class="col-md-2" style="width:20%;padding:0px;">
                                    <img style="height:90px;" id="LostFound" onclick="thisImgClick(this)" src="../Images/LostFound.png"/>
                               </div>
                                <div class="col-md-2" style="width:20%;padding:0px;">
                                    <img style="height:90px;" id="location" onclick="thisImgClick(this)" src="../Images/Contract.png"/>
                               </div>
                                <div class="col-md-2" style="width:20%;padding:0px;">
                                    <img style="height:90px;" id="request" onclick="thisImgClick(this)" src="../Images/Request.png"/>
                               </div>
                                  <div class="col-md-2" style="width:20%;padding:0px;">
           <img style="height:90px;" id="surveil" onclick="thisImgClick(this)" src="../Images/Location.png"/>
                               </div> 
                                
                            </div>
                            <div class="row text-center">
                               <div class="col-md-2" style="width:20%;padding:0px;">
                                    <img style="height:90px;" id="activity" onclick="thisImgClick(this)" src="../Images/Activity.png"/>
                               </div>
                               <div class="col-md-2" style="width:20%;padding:0px;">
                                   <img style="height:90px;" id="chat" onclick="thisImgClick(this)" src="../Images/Chat.png"/>
                               </div>
                                <div class="col-md-2" style="width:20%;padding:0px;">
                                    <img style="height:90px;" id="warehouse" onclick="thisImgClick(this)" src="../Images/Warehouse.png"/>
                               </div>
                                <div class="col-md-2" style="width:20%;padding:0px;">
                                    <img style="height:90px;" id="dispatch" onclick="thisImgClick(this)" src="../Images/Dispatch.png"/>
                               </div>
                                <div class="col-md-2" style="width:20%;padding:0px;">
                                    <div class="row" id="selectDIV" style="display:none;"> 
                                    <p style="margin-left:5px;" class="red-color">SELECTED</p>
                                        <p style="margin-left:5px;font-size:35px" class="red-color" id="selectCount">0</p>
                                    </div>
                               </div>
                              
                            </div>
                         <div class="row text-center" style="display:none;">
                             <div class="col-md-2" style="width:20%;padding:0px;">
                                    <img style="height:90px;" id="collab" onclick="thisImgClick(this)" src="../Images/Collaboration.png"/>
                               </div>
                                                            <div class="col-md-2" style="width:20%;padding:0px;">
                 
                               </div>
                               <div class="col-md-2" style="width:20%;padding:0px;">
                                    
                               </div>
                                <div class="col-md-2" style="width:20%;padding:0px;">
                            
                               </div>
                                <div class="col-md-2" style="width:20%;padding:0px;">
                                    
                               </div>

                                 
                             </div>
              <div class="row" id="mobilemodulesDIV" style="display:none;">
                                    <div class="col-md-12" >
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-12">
                                          <h3 class="capitalize-text no-margin">MODULES</h3>
                                                </div>
                                                </div>
                                            <div class="row">
                                                <div class="col-md-4">                                              
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input style="display:none;" type="checkbox" id="notificationModule" name="niceCheck">
                                            <label for="notificationModule" style="font-size:20px;"><i class="fa fa-bell-o"></i>Message Board</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" id="incidentModule" name="niceCheck">
                                            <label for="incidentModule" style="font-size:20px;"><i class="fa fa-exclamation-circle"></i>Incidents</label>
                                          </div><!--/nice-checkbox-->
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" id="taskModule" name="niceCheck">
                                            <label for="taskModule" style="font-size:20px;"><i class="fa fa-stack-overflow"></i>Task</label>
                                          </div><!--/nice-checkbox-->
                                            </div>
                                            </div>
                                            <div class="row">
                                            <div class="col-md-4 ">                                     
                                                <div class="nice-checkbox inline-block no-vmargine">
                                                                            <input type="checkbox" id="ticketModule" name="niceCheck">
                                                                            <label for="ticketModule" style="font-size:20px;"><i class="fa fa-file-text-o"></i>Ticketing</label>
                                                                            </div><!--/nice-checkbox-->   
                                                                            </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" id="postOrderModule" name="niceCheck">
                                            <label for="postOrderModule" style="font-size:20px;"><i class="fa fa-stack-overflow"></i>Post Order</label>
                                          </div><!--/nice-checkbox-->   
                                          </div>
  <div class="col-md-4 ">                                     <div class="nice-checkbox inline-block no-vmargine">
                                                                            <input type="checkbox" id="warehouseModule" name="niceCheck">
                                                                            <label for="warehouseModule" style="font-size:20px;"><i class="fa fa-industry"></i>Warehouse</label>
                                                                            </div><!--/nice-checkbox-->   
                                                                            </div>
                                          </div>
                                            <div class="row">
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" id="chatModule" name="niceCheck">
                                            <label for="chatModule" style="font-size:20px;"><i class="fa fa-comments-o"></i>Chat</label>
                                          </div><!--/nice-checkbox-->   
                                          </div>
                                          <div class="col-md-4">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" id="DRModule" name="niceCheck">
                                            <label for="DRModule" style="font-size:20px;"><i class="fa fa-stack-overflow"></i>Duty Roster</label>
                                          </div><!--/nice-checkbox-->
                                            </div>
                                        <div class="col-md-4" >                                              
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" id="LostFoundModule" name="niceCheck">
                                            <label for="LostFoundModule" style="font-size:20px;"><i class="fa fa-search"></i>Lost&Found</label>
                                          </div><!--/nice-checkbox-->       
                                        </div>
                                        </div>
                                            <div class="row">
                                                  <div class="col-md-4 ">                                     <div class="nice-checkbox inline-block no-vmargine">
                                                                            <input type="checkbox"  id="locationModule" name="niceCheck">
                                                                            <label for="locationModule" style="font-size:20px;"><i class="fa fa-map-o"></i>Customer</label>
                                                                            </div><!--/nice-checkbox-->   
                                                                            </div>
                                                                                                                        
                                                                                                                                                                        <div class="col-md-4 ">                                     <div class="nice-checkbox inline-block no-vmargine">
                                                                            <input type="checkbox" id="requestModule" name="niceCheck">
                                                                            <label for="requestModule" style="font-size:20px;"><i class="fa fa-stack-overflow"></i>Request</label>
                                                                            </div><!--/nice-checkbox-->   
                                                                            </div>
                                                                            
                                             <div class="col-md-4 "> 
                                                                                                                      <div class="nice-checkbox inline-block no-vmargine">
                                                                            <input type="checkbox" id="collabModule" name="niceCheck">
                                                                            <label for="collabModule" style="font-size:20px;"><i class="fa fa-group"></i>Collaboration</label>
                                                                            </div><!--/nice-checkbox-->   
                                                                            </div>    
                                            </div>
                                            <div class="row">
                                                                                            <div class="col-md-4">  
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" id="dispatchModule" name="niceCheck">
                                            <label for="dispatchModule" style="font-size:20px;"><i class="fa fa-stack-overflow"></i>Dispatch</label>
                                          </div><!--/nice-checkbox-->   
                                          </div>
                                                                                                                      
                                                                                                        <div class="col-md-4 " style="display:none;">  
                                                                                                                    <div class="nice-checkbox inline-block no-vmargine">
                                                                            <input type="checkbox" id="surveilModule" name="niceCheck">
                                                                            <label for="surveilModule" style="font-size:20px;"><i class="fa fa-camera"></i>Surveillance</label>
                                                                            </div><!--/nice-checkbox-->   
                                                                            </div>
																			
																			
																			                                          <div class="col-md-4" style="display:none;">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" id="verifierModule" name="niceCheck">
                                            <label for="verifierModule">Verifier</label>
                                          </div><!--/nice-checkbox-->   
                                          </div>   
                                          <div class="col-md-4" style="display:none;">    
                                          <div class="nice-checkbox inline-block no-vmargine">
                                            <input type="checkbox" id="activityModule" name="niceCheck">
                                            <label for="activityModule">Activity</label>
                                          </div><!--/nice-checkbox-->   
                                          </div>                                  
                                                </div>
                                        </div>
                                    </div>
                                </div>   
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="row horizontal-navigation">
                            <div class="panel-control">
                                <ul class="nav nav-tabs" id="step1UL">
                                     <li class="active"><a href="#" data-dismiss="modal" onclick="clearFields()">CLOSE</a>
                                    </li>  
                                    <li class="active"><a href="#" onclick="nextClick()">NEXT</a> 
                                    </li>                                    
                                </ul>
                                <ul class="nav nav-tabs" style="display:none;" id="step2UL">
                                    <li class="active"><a href="#" onclick="backClick()">BACK</a>
                                    </li>    
                                    <li class="active"><a href="#" onclick="saveInfo()">REGISTER</a>
                                    </li>   
                                </ul>
                                <!-- /.nav -->
                            </div>
                        </div>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
             </div>

        <div aria-hidden="true" aria-labelledby="forgotModal" role="dialog" tabindex="-1" id="forgotModal" class="modal fade" style="display: none;">
               <div class="modal-dialog modal-sm">
                  <div class="modal-content">
                     <div class="modal-header">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i class="icon_close fa-lg"></i></button>
                        <h4 class="modal-title capitalize-text">FORGOT PASSWORD</h4>
                     </div>
                     <div class="modal-body">
                        <form role="form">
                                                       <div class="row">
                              <div class="col-md-12">
                                 <input class="form-control" placeholder="Email address" id="newPwInput"/> 
                              </div>
                           </div>
                        </form>
                     </div>
                     <div class="modal-footer">
                        <div class="row horizontal-navigation">
                           <div class="panel-control">
                              <ul class="nav nav-tabs">
                                 <li><a href="#" data-dismiss="modal">CANCEL</a>
                                 </li>
                                 <li class="active"><a href="#" onclick="changePassword()" >SUBMIT</a>
                                 </li>
                              </ul>
                              <!-- /.nav -->
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- /.modal-content -->
               </div>
               <!-- /.modal-dialog -->
            </div>
<div class="signin-cr text-light">
<%--  <ul>
  <li>
    <a href="#">
        Home
    </a>
  </li>
  <li>
    <a href="#">
        Support
    </a>
  </li>
  <li>
    <a href="#">
        Security
    </a>
  </li>
  <li>
    <a href="#">
        Privacy
    </a>
  </li>

  <li>
    <a href="#">
        Terms
    </a>
  </li>

  <li>
    <a href="#">
        Contact
    </a>
  </li>


  </ul>--%>
<p style="color:#b2163b">Copyright &copy; 2018 Arrow Labs Security Solutions LLC. All Rights Reserved</p>
</div>
  <!-- Modal Recover -->
<%--  <div class="modal fade" id="recoverAccount" tabindex="-1" role="dialog" aria-labelledby="recoverAccountLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <form id="recoverForm" action="index.html">
          <div class="modal-header">
            <h4 class="modal-title" id="recoverAccountLabel">Reset Password</h4>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <div class="input-group input-group-in">
                <span class="input-group-addon"><i class="fa fa-envelope-o text-muted"></i></span>
                <input type="email" name="recoverEmail" id="recoverEmail" class="form-control" placeholder="Enter your email address">
              </div>
            </div><!-- /.form-group -->
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Send reset link</button>
          </div>
        </form><!-- /#recoverForm -->
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /#recoverAccount -->--%>


  <!-- VENDORS : jQuery & Bootstrap -->
<%--    <script src="<%= Page.ResolveClientUrl("~/Scripts/vendor.js") %>"></script>--%>
            <script src="https://testportalcdn.azureedge.net/Scripts/vendor.js"></script>
  <!-- END VENDORS -->

  <!-- DEPENDENCIES : Required plugins -->
<%--    <script src="<%= Page.ResolveClientUrl("~/Scripts/dependencies.js") %>"></script>--%>
            <script src="https://testportalcdn.azureedge.net/Scripts/dependencies.js"></script>
  <!-- END DEPENDENCIES -->

<%--        <script src="<%= Page.ResolveClientUrl("~/Scripts/sweetalert.js")%>"></script>--%>
            <script src="https://testportalcdn.azureedge.net/Scripts/sweetalert.js"</script>
  <!-- PLUGIN SETUPS: vendors & dependencies setups -->
<%--    <script src="<%= Page.ResolveClientUrl("~/Scripts/plugin-setups.js") %>"></script>--%>
            <script src="https://testportalcdn.azureedge.net/Scripts/plugin-setups.js"></script>
  <!-- END PLUGIN SETUPS -->
<%--        	<script src="<%= Page.ResolveClientUrl("~/Scripts/script.js") %>"></script>--%>
        	<script src="https://testportalcdn.azureedge.net/Scripts/script.js"></script>
        </form>
    </body>