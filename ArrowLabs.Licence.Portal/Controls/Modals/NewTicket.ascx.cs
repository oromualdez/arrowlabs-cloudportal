﻿using Arrowlabs.Business.Layer;
using ArrowLabs.Licence.Portal.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ArrowLabs.Licence.Portal.Controls.Modals
{
    public partial class NewTicket : System.Web.UI.UserControl
    {
        protected string customersDisplay;
        protected void Page_Load(object sender, EventArgs e)
        {            

            var dbConnection = CommonUtility.dbConnection;
            var user = HttpContext.Current.User.Identity;
            customersDisplay = "none";
            if (user != null)
            {
                var userinfo = Users.GetUserByName(user.Name, dbConnection);

                if (userinfo.RoleId != (int)Role.SuperAdmin)
                {
                    var modules = UserModules.GetAllModulesByUsername(userinfo.Username, dbConnection);
                    foreach (var mods in modules)
                    { 
                        if (mods.ModuleId == (int)Accounts.ModuleTypes.Customer)
                        {

                            customersDisplay = "block";
                        }
                    }

                }
                else
                { 
                    customersDisplay = "block";
                }

                var startlocations = new List<Location>();
                var locations = new List<Location>();
                var offenceCategory = new List<OffenceCategory>();
                var off = new OffenceCategory();
                off.Id = 0;
                off.Name = "Select Category";
                offenceCategory.Add(off);
                if (userinfo.RoleId == (int)Role.CustomerSuperadmin || userinfo.RoleId == (int)Role.CustomerUser)
                {
                    locations.AddRange(Location.GetAllMainLocationByCustomerId(userinfo.CustomerInfoId, dbConnection));
                    locations.AddRange(Location.GetAllSubLocationByCustomerId(userinfo.CustomerInfoId, dbConnection));
                    var offenceCategorys = OffenceCategory.GetAllOffenceCategoryByCId(userinfo.CustomerInfoId, dbConnection);
                    offenceCategory.AddRange(offenceCategorys);
                }
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                    locations.AddRange(Location.GetAllMainLocationByLevel5(userinfo.ID, dbConnection));
                    locations.AddRange(Location.GetAllSubLocationByLevel5(userinfo.ID, dbConnection));
                    var offenceCategorys = OffenceCategory.GetAllOffenceCategoryByLevel5(userinfo.ID, dbConnection);
                    offenceCategory.AddRange(offenceCategorys);
                }
                else if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                {
                    locations.AddRange(Location.GetAllLocation(dbConnection));
                    locations.AddRange(Location.GetAllSubLocation(dbConnection));
                    var offenceCategorys = OffenceCategory.GetAllOffenceCategory(dbConnection);
                    offenceCategory.AddRange(offenceCategorys);
                } 
                else
                {
                    locations.AddRange(Location.GetAllLocationBySiteId(userinfo.SiteId, dbConnection));
                    locations.AddRange(Location.GetAllSubLocationBySiteId(userinfo.SiteId, dbConnection));
                    var offenceCategorys = OffenceCategory.GetAllOffenceCategoryBySiteId(userinfo.SiteId, dbConnection);
                    offenceCategory.AddRange(offenceCategorys);
                }

                var grouped = locations.GroupBy(item => item.ID);
                locations = grouped.Select(grp => grp.OrderBy(item => item.LocationDesc).First()).ToList();
                locations = locations.OrderBy(i => i.LocationDesc).ToList();

                var glocations = new List<Location>();
                var loc = new Location();
                loc.ID = 0;
                loc.LocationDesc = "Select Location";
                loc.LocationDesc_AR = "Select Location";
                loc.CreatedDate = CommonUtility.getDTNow();
                loc.CreatedBy = userinfo.Username;
                glocations.Add(loc);
                glocations.AddRange(locations);

                startlocations.AddRange(glocations.Where(i => i.LocationTypeId != 2).ToList());

                ticketCategorySelect.DataTextField = "Name";
                ticketCategorySelect.DataValueField = "Id";
                ticketCategorySelect.DataSource = offenceCategory;
                ticketCategorySelect.DataBind();

                ticketlocationSelect.DataTextField = "LOCATIONDESC";
                ticketlocationSelect.DataValueField = "Id";
                ticketlocationSelect.DataSource = startlocations;
                ticketlocationSelect.DataBind();
            }
        }
    }
}