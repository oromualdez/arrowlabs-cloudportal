﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Arrowlabs.Business.Layer;
using System.Text;
using System.Configuration;
using System.Web.UI.DataVisualization.Charting;
using System.Web.Services;
using ArrowLabs.Licence.Portal.Helpers;
using System.Threading;
using iTextSharp.text.pdf;
using iTextSharp.text;

namespace ArrowLabs.Licence.Portal
{
    public partial class OthersDash : System.Web.UI.Page
    {
        static string dbConnection { get; set; }
        static string dbConnectionAudit { get; set; }
        protected string locID;
        protected string incidentName;
        protected string incidentIns;
        protected string incidentDesc;
        protected string incidentRecBy;
        protected string incidentLong;
        protected string incidentLat;
        protected string incidentTaskId;
        protected string incidentAction;
        protected string IsTask;
        protected string incidentType;
        protected string resolvedPercent="0";
        protected string pendingPercent = "0";
        protected string HandledPercentage = "0";
        protected string inprogressPercent = "0";
        protected string completedPercent = "0";
        protected string datetoday;
        protected string hot1Percent = "0";
        protected string hot2Percent = "0";
        protected string hot3Percent = "0";
        protected string hot4Percent = "0";
        protected string hot5Percent = "0";
        protected string day1;
        protected string day2;
        protected string day3;
        protected string day4;
        protected string day5;
        protected string day6;
        protected string day7;
        protected string loggedInId;
        protected string VeriRequestCount = "0";
        protected string VeriEnrolledCount = "0";
        protected string VeriTotalCount = "0";
        protected string VeriRequestPercent = "0";
        protected string VeriEnrolledPercent = "0";
        protected string userinfoDisplay = "block";
        protected string msbDisplay;
        protected int ActualDemo = 0;
        protected int PlannedDemo = 0;

        protected string ActualDemoCount = "0";
        protected string PlannedDemoCount = "0";

        protected int taskUser1Avg = 0;
        protected int taskUser2Avg = 0;
        protected int taskUser3Avg = 0;
        protected int taskUser4Avg = 0;
        protected int taskUser5Avg = 0;

        protected string taskUser1 = " ";
        protected string taskUser2 = " ";
        protected string taskUser3 = " ";
        protected string taskUser4 = " ";
        protected string taskUser5 = " ";

        protected string AvgTotal = "0";

        protected string Top1Ins = "0";
        protected string Top2Ins = "0";
        protected string Top3Ins = "0";
        protected string Top4Ins = "0";
        protected string Top5Ins = "0";

        protected string Top1InsCount = "0";
        protected string Top2InsCount = "0";
        protected string Top3InsCount = "0";
        protected string Top4InsCount = "0";
        protected string Top5InsCount = "0";

        protected string Top1InsName = " ";
        protected string Top2InsName = " ";
        protected string Top3InsName = " ";
        protected string Top4InsName = " ";
        protected string Top5InsName = " ";

        protected string Top1TicCount = "0";
        protected string Top2TicCount = "0";
        protected string Top3TicCount = "0";
        protected string Top4TicCount = "0";
        protected string Top5TicCount = "0";

        protected string Top1TicName = "";
        protected string Top2TicName = "";
        protected string Top3TicName = "";
        protected string Top4TicName = "";
        protected string Top5TicName = "";

        protected string Top1Tic = "0";
        protected string Top2Tic = "0";
        protected string Top3Tic = "0";
        protected string Top4Tic = "0";
        protected string Top5Tic = "0";

        protected int SkillSet1Week1Count = 0;
        protected int SkillSet1Week2Count = 0;
        protected int SkillSet1Week3Count = 0;
        protected int SkillSet1Week4Count = 0;

        protected int SkillSet2Week1Count = 0;
        protected int SkillSet2Week2Count = 0;
        protected int SkillSet2Week3Count = 0;
        protected int SkillSet2Week4Count = 0;

        protected int SkillSet3Week1Count = 0;
        protected int SkillSet3Week2Count = 0;
        protected int SkillSet3Week3Count = 0;
        protected int SkillSet3Week4Count = 0;

        protected int SkillSet4Week1Count = 0;
        protected int SkillSet4Week2Count = 0;
        protected int SkillSet4Week3Count = 0;
        protected int SkillSet4Week4Count = 0;

        protected int SkillSet5Week1Count = 0;
        protected int SkillSet5Week2Count = 0;
        protected int SkillSet5Week3Count = 0;
        protected int SkillSet5Week4Count = 0;


        protected string SkillSet1Name = " ";
        protected string SkillSet2Name = " ";
        protected string SkillSet3Name = " ";
        protected string SkillSet4Name = " ";
        protected string SkillSet5Name = " ";

        protected string demoWeek1 = "1";
        protected string demoWeek2 = "2";
        protected string demoWeek3 = "3";
        protected string demoWeek4 = "4";
        protected string demoWeek5 = "5";
        

        protected int day1week1;
        protected int day2week1;
        protected int day3week1;
        protected int day4week1;
        protected int day5week1;
        protected int day6week1;
        protected int day7week1;

        protected int day1week2;
        protected int day2week2;
        protected int day3week2;
        protected int day4week2;
        protected int day5week2;
        protected int day6week2;
        protected int day7week2;

        protected int day1week3;
        protected int day2week3;
        protected int day3week3;
        protected int day4week3;
        protected int day5week3;
        protected int day6week3;
        protected int day7week3;

        protected int day1week4;
        protected int day2week4;
        protected int day3week4;
        protected int day4week4;
        protected int day5week4;
        protected int day6week4;
        protected int day7week4;

        protected int ticketday1week1;
        protected int ticketday2week1;
        protected int ticketday3week1;
        protected int ticketday4week1;
        protected int ticketday5week1;
        protected int ticketday6week1;
        protected int ticketday7week1;

        protected int ticketday1week2;
        protected int ticketday2week2;
        protected int ticketday3week2;
        protected int ticketday4week2;
        protected int ticketday5week2;
        protected int ticketday6week2;
        protected int ticketday7week2;

        protected int ticketday1week3;
        protected int ticketday2week3;
        protected int ticketday3week3;
        protected int ticketday4week3;
        protected int ticketday5week3;
        protected int ticketday6week3;
        protected int ticketday7week3;

        protected int ticketday1week4;
        protected int ticketday2week4;
        protected int ticketday3week4;
        protected int ticketday4week4;
        protected int ticketday5week4;
        protected int ticketday6week4;
        protected int ticketday7week4;
        protected int notificationCount1;
        protected int notificationCount2;
        protected int notificationCount3;
        protected int notificationCount4;
        protected string cusEvId { get; set; }
        protected string incidentId;
        protected string longi;
        protected string lati;
        protected string alarmMSG;
        protected string senderName2;
        protected string senderName;
        protected string username;
        protected string ipaddress;
        protected string sendToManager;
        protected string assignedTo;
        protected string sendToTag;
        protected string sendMangId;

        protected string pendingPercentDemo;
        protected string pendingPercentDemoCount;

        protected string inprogressPercentDemo;
        protected string inprogressPercentDemoCount;

        protected string completedPercentDemo;
        protected string completedPercentDemoCount;

        protected string acceptedPercent;
        protected string acceptedPercentCount;

        protected string totalTasksCount;

        protected string handledTeamTasks;

        int completedCount;
        int inprogressCount;
        int pendingCount;
        int total;
        protected int onlinecount;
        protected int offlinecount;
        protected int idlecount;

        protected string hot1display;
        protected string hot2display;
        protected string hot3display;
        protected string hot4display;
        protected string hot5display;
        static ClientLicence getClientLic;
        static List<string> VideoExtensions = new List<string> { ".AVI", ".MP4", ".MKV", ".FLV" };

        protected string sourceLat;
        protected string sourceLon;
        protected string currentlocation;

        protected string incidentDisplay;
        protected string taskDisplay;
        protected string senderName3;
        protected string siteName;
        protected void Page_Load(object sender, EventArgs e)
        {
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/Default.aspx");
            }

            fpath = Server.MapPath("~/Uploads/");

            dbConnection = CommonUtility.dbConnection;
            dbConnectionAudit = CommonUtility.dbConnectionAudit;
            var dtNow = CommonUtility.getDTNow();
            datetoday = dtNow.ToString("MMM dd, yyyy");
            senderName = User.Identity.Name;
            senderName2 = Encrypt.EncryptData(senderName, true, dbConnection);
            var userinfo = Users.GetUserByName(senderName, dbConnection);
            sourceLat = "25.2049808502197";
            sourceLon = "55.2707939147949";
            currentlocation = "U.A.E";
            msbDisplay = "none";
            if (userinfo != null)
            {
                if (userinfo.SiteId > 0)
                {
                    siteName = "<a style='margin-left:0px;color:gray' href='#' class='fa fa-building fa-lg'></a><a style='font-size:smaller;color:gray;margin-right:5px'>" + userinfo.SiteName + "</a>";
                }
            }
            try
            {
                var settings = ConfigSettings.GetConfigSettings(CommonUtility.dbConnection);
                ipaddress = settings.ChatServerUrl + "/signalr";
                var firstDayOfMonth = new DateTime(dtNow.Year, dtNow.Month, 1);
                day1 = firstDayOfMonth.ToString("yyyy-MM-dd");
                day2 = firstDayOfMonth.AddDays(1).ToString("yyyy-MM-dd");
                day3 = firstDayOfMonth.AddDays(2).ToString("yyyy-MM-dd");
                day4 = firstDayOfMonth.AddDays(3).ToString("yyyy-MM-dd");
                day5 = firstDayOfMonth.AddDays(4).ToString("yyyy-MM-dd");
                day6 = firstDayOfMonth.AddDays(5).ToString("yyyy-MM-dd");
                day7 = firstDayOfMonth.AddDays(6).ToString("yyyy-MM-dd");


                getClientLic = ClientLicence.GetLicenseByClientID(CommonUtility.arrowlabsKey, dbConnection);
                if (userinfo != null)
                {
                    senderName3 = userinfo.CustomerUName;
                    incidentDisplay = "none";
                    taskDisplay = "block";

                    if (userinfo.RoleId == (int)Role.CustomerSuperadmin || userinfo.RoleId == (int)Role.CustomerUser || userinfo.RoleId == (int)Role.Regional)
                    {
                        if (userinfo.CustomerInfoId > 0)
                        {
                            var gSite = Arrowlabs.Business.Layer.CustomerInfo.GetCustomerInfoById(userinfo.CustomerInfoId, dbConnection);
                            if (gSite != null)
                            {
                                sourceLat = gSite.Lati;
                                sourceLon = gSite.Long;
                                if (!string.IsNullOrEmpty(gSite.Country))
                                    currentlocation = gSite.Country.ToUpper();
                                else
                                    currentlocation = "N/A";
                            }
                        }
                    }
                    else
                    {
                        if (userinfo.SiteId > 0)
                        {
                            var gSite = Arrowlabs.Business.Layer.Site.GetSiteById(userinfo.SiteId, dbConnection);
                            if (gSite != null)
                            {
                                sourceLat = gSite.Lati;
                                sourceLon = gSite.Long;
                                currentlocation = ReverseGeocode.RetrieveFormatedAddress(sourceLat, sourceLon, getClientLic).ToUpper();
                            }
                        }
                    }

                    if (userinfo.RoleId != (int)Role.SuperAdmin)
                    {
                        var uModules = UserModules.GetAllModulesByUsername(userinfo.Username, dbConnection);
                        if (uModules != null)
                        {
                            if (uModules.Count > 0)
                            {
                                var isIncident = uModules.Where(i => i.ModuleId == (int)Accounts.ModuleTypes.Alarms).ToList();
                                if (isIncident.Count > 0)
                                {
                                    incidentDisplay = "block";
                                }
                                var isTask = uModules.Where(i => i.ModuleId == (int)Accounts.ModuleTypes.Tasks).ToList();
                                if (isTask.Count == 0)
                                {
                                    taskDisplay = "none";
                                }

                                var isMB = uModules.Where(i => i.ModuleId == (int)Accounts.ModuleTypes.MessageBoard).ToList();
                                if (isMB.Count >0)
                                {
                                    msbDisplay = "block";
                                }
                            }
                            else
                            {
                                Response.Redirect("~/Pages/UsersDB.aspx");
                            }
                        }
                        else
                        {
                            Response.Redirect("~/Pages/UsersDB.aspx");
                        }
                    }
                    else
                    {
                        incidentDisplay = "block";
                    }

                    loggedInId = userinfo.ID.ToString();
                    if (userinfo.RoleId == (int)Role.SuperAdmin)
                    {
                        userinfoDisplay = "block";
                        msbDisplay = "block";
                    }
                    else
                    {
                        if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                            userinfoDisplay = "block";
                        //var pushDev = PushNotificationDevice.GetPushNotificationDeviceByUsername(userinfo.Username, dbConnection);
                        //if (pushDev != null)
                        //{
                        //    if (!string.IsNullOrEmpty(pushDev.Username))
                        //    {
                        //        pushDev.IsServerPortal = true;
                        //        pushDev.SiteId = userinfo.SiteId;
                        //        PushNotificationDevice.InsertorUpdatePushNotificationDevice(pushDev, dbConnection, dbConnectionAudit,true);
                        //    }
                        //}
                        PushNotificationDevice.UpdatePushNotificationDeviceByUsername(userinfo.Username, userinfo.SiteId, dbConnection);
                        userinfoDisplay = "none";
                    }

                    getClientLic = ClientLicence.GetLicenseByClientID(CommonUtility.arrowlabsKey, dbConnection);
                    if (getClientLic != null)
                    {
                        if (!getClientLic.isTask)
                            taskDisplay = "none";
                        if (!getClientLic.isIncident)
                            incidentDisplay = "none";
                    }
                }

                var allmanager = new List<Users>();
                var alldirectors = new List<Users>();
                var unassigned = new Users();
                unassigned.Username = "Unassigned";
                unassigned.ID = 0;
                allmanager.Add(unassigned);
                alldirectors.Add(unassigned);
                allmanager.AddRange(Users.GetAllManagers(dbConnection));
                alldirectors.AddRange(Users.GetAllAdmins(dbConnection));

                editmanagerpickerSelect.DataTextField = "Username";
                editmanagerpickerSelect.DataValueField = "ID";
                editmanagerpickerSelect.DataSource = allmanager;
                editmanagerpickerSelect.DataBind();

                editdirpickerSelect.DataTextField = "Username";
                editdirpickerSelect.DataValueField = "ID";
                editdirpickerSelect.DataSource = alldirectors;
                editdirpickerSelect.DataBind();


                incidentName = Request.QueryString["IncName"];
                incidentAction = "Dispatch";
                cusEvId = "0";
               
                IsTask = "false";
                incidentId = "0";
                {
                    //getOthersInfo(userinfo);
                    getOnlineUsers(userinfo);
                    //getEventStatus(userinfo);
                }
            }
            catch(Exception ex)
            {
                MIMSLog.MIMSLogSave("OthersDash.aspx", "PageLoad", ex, dbConnection, userinfo.SiteId);
            }
        }
        public void getOnlineUsers(Users userinfo)
        {
            try
            {
                var users = new List<Users>();
                var devices = new List<HealthCheck>();
                onlinecount = 0;
                offlinecount = 0;
                idlecount = 0;
                if (userinfo != null)
                {
                    if (userinfo.RoleId == (int)Role.Manager)
                    {
                        users = Users.GetAllFullUsersByManagerId(userinfo.ID, dbConnection);
                    }
                    else if (userinfo.RoleId == (int)Role.Admin)
                    {
                        var sessions = DirectorManager.GetAllFullManagersByDirectorId(userinfo.ID, dbConnection);
                        foreach (var usr in sessions)
                        {
                            users.Add(usr);
                            var getallUsers = Users.GetAllFullUsersByManagerIdForDirector(usr.ID, dbConnection);
                            foreach (var subsubuser in getallUsers)
                            {
                                users.Add(subsubuser);
                            }
                        }
                        var unassigned = Users.GetAllUnassignedOperators(dbConnection);
                        unassigned = unassigned.Where(i => i.SiteId == (int)userinfo.SiteId).ToList();
                        foreach (var subsubuser in unassigned)
                        {
                            users.Add(subsubuser);
                        } 
                    }
                    else if (userinfo.RoleId == (int)Role.Regional)
                    {
                        //var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                        //foreach (var site in sites)
                        //{
                        users.AddRange(Users.GetAllUsersByLevel5(userinfo.ID, dbConnection));
                        //}
                        //devices = HealthCheck.GetAllDevicesHealthCheck(dbConnection);
                    }
                    else if (userinfo.RoleId == (int)Role.Director)
                    {
                        users = Users.GetAllUsersBySiteId(userinfo.SiteId, dbConnection);
                        users = users.Where(i => i.RoleId != (int)Role.Director).ToList();
                        //devices = HealthCheck.GetAllDevicesHealthCheck(dbConnection);
                    }
                    else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                    {
                        users = Users.GetAllUsersByCustomerId(userinfo.CustomerInfoId, dbConnection);
                        users = users.Where(i => i.RoleId != (int)Role.CustomerSuperadmin).ToList();
                    }
                    else if (userinfo.RoleId == (int)Role.ChiefOfficer)
                    {
                        users = Users.GetAllUsers(dbConnection);
                        users = users.Where(i => i.RoleId != (int)Role.ChiefOfficer).ToList();
                    }
                    else if (userinfo.RoleId == (int)Role.SuperAdmin)
                    {
                        users = Users.GetAllUsers(dbConnection);
                        devices = HealthCheck.GetAllDevicesHealthCheck(dbConnection);
                    }

                    var grouped = users.GroupBy(item => item.ID);
                    users = grouped.Select(grp => grp.OrderBy(item => item.ID).First()).ToList();

                    foreach (var item in users)
                    {
                        if (item.Active == (int)Arrowlabs.Business.Layer.HealthCheck.DeviceStatus.Online)
                        {
                            onlinecount++;
                        }
                        else if (item.Active == (int)Arrowlabs.Business.Layer.HealthCheck.DeviceStatus.Error)
                        {
                            idlecount++;
                        }
                        else if (item.Active == (int)Arrowlabs.Business.Layer.HealthCheck.DeviceStatus.Offline)
                        {
                            offlinecount++;
                        }
                    }
                    foreach (var dev in devices)
                    {
                        if (dev.Status == 1)
                        {
                            onlinecount++;
                        }
                        else
                        {
                            offlinecount++;
                        }
                    }
                    lbOnlineCount.Text = onlinecount.ToString();
                    lbOfflineCount.Text = offlinecount.ToString();
                    lbIdleCount.Text = idlecount.ToString();
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("OthersDash.aspx", "getOnlineUsers", ex, dbConnection, userinfo.SiteId);
            }
        }



        public void getEventStatus(Users userinfo)
        {
            try
            {
                var cusEvents = new List<CustomEvent>();
                var resolvedIncidents = new List<CustomEvent>();
                var preresolvedIncidents = new List<CustomEvent>();
                pendingCount = 0;
                inprogressCount = 0;
                completedCount = 0;
                total = 0;

                ticketday1week1 = 0;
                ticketday2week1 = 0;
                ticketday3week1 = 0;
                ticketday4week1 = 0;
                ticketday5week1 = 0;
                ticketday6week1 = 0;
                ticketday7week1 = 0;

                ticketday1week2 = 0;
                ticketday2week2 = 0;
                ticketday3week2 = 0;
                ticketday4week2 = 0;
                ticketday5week2 = 0;
                ticketday6week2 = 0;
                ticketday7week2 = 0;

                ticketday1week3 = 0;
                ticketday2week3 = 0;
                ticketday3week3 = 0;
                ticketday4week3 = 0;
                ticketday5week3 = 0;
                ticketday6week3 = 0;
                ticketday7week3 = 0;

                ticketday1week4 = 0;
                ticketday2week4 = 0;
                ticketday3week4 = 0;
                ticketday4week4 = 0;
                ticketday5week4 = 0;
                ticketday6week4 = 0;
                ticketday7week4 = 0;


                var enrollReq = new List<Verifier>();
                var enrolled = new List<Enrollment>();
                //if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                //{
                //    enrollReq = Verifier.GetAllVerifierNoBytesByRequest((int)Verifier.RequestTypes.Verify, dbConnection);
                //    enrolled = Enrollment.GetAllEnrollments(dbConnection);
                //}
                //else if (userinfo.RoleId == (int)Role.Manager)
                //{
                //    enrollReq = Verifier.GetAllVerifierNoBytesByManagerIdAndRequest(userinfo.ID, (int)Verifier.RequestTypes.Verify, dbConnection);
                //    enrolled = Enrollment.GetAllEnrollmentByCreatedBy(userinfo.Username, dbConnection);
                //}
                //else if (userinfo.RoleId == (int)Role.Director)
                //{
                //    enrollReq = Verifier.GetAllVerifierNoBytesByRequestAndSite((int)Verifier.RequestTypes.Verify, userinfo.SiteId, dbConnection);
                //    enrolled = Enrollment.GetAllEnrollmentBySite(userinfo.SiteId, dbConnection);
                //}
                //else if (userinfo.RoleId == (int)Role.Regional)
                //{
                //    var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                //    foreach (var site in sites)
                //    {
                //        enrollReq.AddRange(Verifier.GetAllVerifierNoBytesByRequestAndSite((int)Verifier.RequestTypes.Verify, site.SiteId, dbConnection));
                //        enrolled.AddRange(Enrollment.GetAllEnrollmentBySite(site.SiteId, dbConnection));
                //    }
                //}
                //else if (userinfo.RoleId == (int)Role.Admin)
                //{
                //    enrollReq = Verifier.GetAllVerifierNoBytesByManagerIdAndRequest(userinfo.ID, (int)Verifier.RequestTypes.Verify, dbConnection);
                //    enrolled = Enrollment.GetAllEnrollmentByCreatedBy(userinfo.Username, dbConnection);
                //    var xsessions = DirectorManager.GetAllFullManagersByDirectorId(userinfo.ID, dbConnection);
                //    foreach (var item in xsessions)
                //    {
                //        enrollReq.AddRange(Verifier.GetAllVerifierNoBytesByManagerIdAndRequest(item.ID, (int)Verifier.RequestTypes.Verify, dbConnection));
                //        enrolled.AddRange(Enrollment.GetAllEnrollmentByCreatedBy(item.Username, dbConnection));
                //    }
                //    var grouped = enrollReq.GroupBy(item => item.Id);
                //    enrollReq = grouped.Select(grp => grp.OrderBy(item => item.Id).First()).ToList();

                //    var groupedd = enrolled.GroupBy(item => item.Id);
                //    enrolled = groupedd.Select(grp => grp.OrderBy(item => item.Id).First()).ToList();
                //}
                VeriRequestCount = enrollReq.Count.ToString();
                VeriEnrolledCount = enrolled.Count.ToString();
                VeriTotalCount = (enrolled.Count + enrollReq.Count).ToString();
                VeriRequestPercent = ((int)Math.Round((float)enrollReq.Count / (float)(enrolled.Count + enrollReq.Count) * (float)100)).ToString();
                VeriEnrolledPercent = ((int)Math.Round((float)enrolled.Count / (float)(enrolled.Count + enrollReq.Count) * (float)100)).ToString();
                //if (userinfo != null)
                //{
                //    //if (userinfo.RoleId == (int)Role.Manager)
                //    //{
                //    //    cusEvents = CustomEvent.GetAllCustomEventsByManagerId(userinfo.ID, dbConnection);
                //    //    preresolvedIncidents = CustomEvent.GetAllCustomEventByDateAndManagerIdHandledBySiteId(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(365, 0, 0, 0)), userinfo.ID, userinfo.SiteId, dbConnection);
                //    //}
                //    //else if (userinfo.RoleId == (int)Role.Admin)
                //    //{
                //    //    var managerusers = DirectorManager.GetAllManagersByDirectorId(userinfo.ID, dbConnection);
                //    //    cusEvents.AddRange(CustomEvent.GetAllCustomEventsByManagerId(userinfo.ID, dbConnection));
                //    //    preresolvedIncidents.AddRange(CustomEvent.GetAllCustomEventByDateAndDirectorIdHandledBySiteId(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(365, 0, 0, 0)), userinfo.ID, userinfo.SiteId, dbConnection));
                //    //    foreach (var manguser in managerusers)
                //    //    {
                //    //        cusEvents.AddRange(CustomEvent.GetAllCustomEventsByManagerId(manguser, dbConnection));

                //    //        preresolvedIncidents.AddRange(CustomEvent.GetAllCustomEventByDateAndManagerIdHandledBySiteId(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(365, 0, 0, 0)), manguser, userinfo.SiteId, dbConnection));
                //    //    }
                //    //}
                //    //else if (userinfo.RoleId == (int)Role.Director)
                //    //{
                //    //    cusEvents = CustomEvent.GetAllCustomEventsBySiteId(userinfo.SiteId,dbConnection);
                        
                //    //    preresolvedIncidents = CustomEvent.GetAllCustomEventByDateHandledAndSiteId(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(365, 0, 0, 0)), userinfo.SiteId,dbConnection);
                //    //}
                //    if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                //    {
                //        cusEvents = CustomEvent.GetAllCustomEvents(dbConnection);
                //        preresolvedIncidents = CustomEvent.GetAllCustomEventByDateHandled(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(365, 0, 0, 0)), dbConnection);
                //    } 
                //    else if (userinfo.RoleId == (int)Role.CustomerUser)
                //    {
                //        cusEvents = CustomEvent.GetAllCustomEventByCId(userinfo.CustomerInfoId, dbConnection);
                //        preresolvedIncidents = CustomEvent.GetAllCustomEventByDateHandled(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(365, 0, 0, 0)), dbConnection);
                //        preresolvedIncidents = preresolvedIncidents.Where(i => i.CustomerId == userinfo.CustomerInfoId).ToList();
                //        preresolvedIncidents = preresolvedIncidents.Where(i => i.UserName == userinfo.Username).ToList();
                //        cusEvents = cusEvents.Where(i => i.UserName == userinfo.Username).ToList();
                //    }
                //    else 
                //    {
                //        cusEvents = CustomEvent.GetAllCustomEventByCId(userinfo.CustomerInfoId, dbConnection);
                //        preresolvedIncidents = CustomEvent.GetAllCustomEventByDateHandled(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(365, 0, 0, 0)), dbConnection);
                //        preresolvedIncidents = preresolvedIncidents.Where(i => i.CustomerId == userinfo.CustomerInfoId).ToList();
                //    }
                //    //else if (userinfo.RoleId == (int)Role.Regional)
                //    //{
                //    //    //var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                //    //   // foreach (var site in sites)
                //    //   // {
                //    //        cusEvents.AddRange(CustomEvent.GetAllCustomEventsByLevel5(userinfo.ID, dbConnection));
                //    //        preresolvedIncidents.AddRange(CustomEvent.GetAllCustomEventByDateHandledAndLevel5(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(365, 0, 0, 0)), userinfo.ID, dbConnection));

                //    //    //}
                //    //    cusEvents.AddRange(CustomEvent.GetAllCustomEventsByManagerId(userinfo.ID, dbConnection));
                //    //}
                //    //else if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                //    //{
                //    //    cusEvents = CustomEvent.GetAllCustomEvents(dbConnection);
                //    //    preresolvedIncidents = CustomEvent.GetAllCustomEventByDateHandled(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(365, 0, 0, 0)), dbConnection);
                //    //}
                    

                //    var userList = new List<string>();
                //    var offenceuserList = new List<string>();
                //    if (cusEvents.Count > 0)
                //    {
                //        var grouped = cusEvents.GroupBy(item => item.EventId);
                //        cusEvents = grouped.Select(grp => grp.OrderBy(item => item.EventId).First()).ToList();

                //        foreach (var evs in cusEvents)
                //        {
                //            if (evs.EventType == (int)CustomEvent.EventTypes.DriverOffence)
                //            {
                //                if (!evs.Handled)
                //                {
                //                    var gTicket = DriverOffence.GetDriverOffenceById(evs.Identifier, dbConnection);
                //                    if (gTicket != null)
                //                    {
                //                        if (gTicket.TicketStatus == (int)CustomEvent.IncidentActionStatus.Engage)
                //                        {
                //                            inprogressCount++;
                //                        }
                //                        else if (gTicket.TicketStatus == (int)CustomEvent.IncidentActionStatus.Complete)
                //                        {
                //                            completedCount++;
                //                        }
                //                        else
                //                        {
                //                            pendingCount++;
                //                        }
                //                    }
                //                }
                //                else
                //                {
                //                    //resolvedIncidents.Add(evs);
                //                }
                //                //if (!evs.Handled)//evs.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Pending)
                //                //{
                //                //    pendingCount++; //pending count
                //                //}
                //                //else if (evs.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Complete)
                //                //{
                //                //    completedCount++; //complete count
                //                //}
                //                //else if (evs.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Resolve)
                //                //{

                //                //}
                //                //else
                //                //    inprogressCount++;

                //                if (evs.EventType == CustomEvent.EventTypes.HotEvent || evs.EventType == CustomEvent.EventTypes.Request)
                //                {
                //                    var reqEv = HotEvent.GetHotEventById(evs.Identifier, dbConnection);
                //                    if (reqEv != null)
                //                    {
                //                        if (CommonUtility.getPCName(reqEv).ToLower() != "mims mobile")
                //                        {
                //                            var retname = CommonUtility.getPCName(reqEv).ToLower();
                //                            if (!string.IsNullOrEmpty(retname))
                //                                userList.Add(retname);
                //                        }
                //                        else
                //                            userList.Add(evs.UserName.ToLower());
                //                    }
                //                }
                //                if (evs.EventType == CustomEvent.EventTypes.MobileHotEvent)
                //                {
                //                    userList.Add(evs.UserName.ToLower());
                //                }
                //                offenceuserList.Add(evs.CustomerUName.ToLower());
                //                if (evs.RecevieTime.Value.Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 1))
                //                {
                //                    ticketday1week1++;
                //                }
                //                else if (evs.RecevieTime.Value.Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 2))
                //                {
                //                    ticketday2week1++;
                //                }
                //                else if (evs.RecevieTime.Value.Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 3))
                //                {
                //                    ticketday3week1++;
                //                }
                //                else if (evs.RecevieTime.Value.Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 4))
                //                {
                //                    ticketday4week1++;
                //                }
                //                else if (evs.RecevieTime.Value.Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 5))
                //                {
                //                    ticketday5week1++;
                //                }
                //                else if (evs.RecevieTime.Value.Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 6))
                //                {
                //                    ticketday6week1++;
                //                }
                //                else if (evs.RecevieTime.Value.Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 7))
                //                {
                //                    ticketday7week1++;
                //                }
                //                else if (evs.RecevieTime.Value.Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 8))
                //                {
                //                    ticketday1week2++;
                //                }
                //                else if (evs.RecevieTime.Value.Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 9))
                //                {
                //                    ticketday2week2++;
                //                }
                //                else if (evs.RecevieTime.Value.Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 10))
                //                {
                //                    ticketday3week2++;
                //                }
                //                else if (evs.RecevieTime.Value.Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 11))
                //                {
                //                    ticketday4week2++;
                //                }
                //                else if (evs.RecevieTime.Value.Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 12))
                //                {
                //                    ticketday5week2++;
                //                }
                //                else if (evs.RecevieTime.Value.Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 13))
                //                {
                //                    ticketday6week2++;
                //                }
                //                else if (evs.RecevieTime.Value.Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 14))
                //                {
                //                    ticketday7week2++;
                //                }
                //                else if (evs.RecevieTime.Value.Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 15))
                //                {
                //                    ticketday1week3++;
                //                }
                //                else if (evs.RecevieTime.Value.Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 16))
                //                {
                //                    ticketday2week3++;
                //                }
                //                else if (evs.RecevieTime.Value.Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 17))
                //                {
                //                    ticketday3week3++;
                //                }
                //                else if (evs.RecevieTime.Value.Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 18))
                //                {
                //                    ticketday4week3++;
                //                }
                //                else if (evs.RecevieTime.Value.Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 19))
                //                {
                //                    ticketday5week3++;
                //                }
                //                else if (evs.RecevieTime.Value.Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 20))
                //                {
                //                    ticketday6week3++;
                //                }
                //                else if (evs.RecevieTime.Value.Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 21))
                //                {
                //                    ticketday7week3++;
                //                }
                //                else if (evs.RecevieTime.Value.Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 22))
                //                {
                //                    ticketday1week4++;
                //                }
                //                else if (evs.RecevieTime.Value.Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 23))
                //                {
                //                    ticketday2week4++;
                //                }
                //                else if (evs.RecevieTime.Value.Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 24))
                //                {
                //                    ticketday3week4++;
                //                }
                //                else if (evs.RecevieTime.Value.Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 25))
                //                {
                //                    ticketday4week4++;
                //                }
                //                else if (evs.RecevieTime.Value.Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 26))
                //                {
                //                    ticketday5week4++;
                //                }
                //                else if (evs.RecevieTime.Value.Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 27))
                //                {
                //                    ticketday6week4++;
                //                }
                //                else if (evs.RecevieTime.Value.Date >= new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 28) && evs.RecevieTime.Value.Date < new DateTime(CommonUtility.getDTNow().Year, (CommonUtility.getDTNow().Month + 1), 1))
                //                {
                //                    ticketday7week4++;
                //                }
                //            } 
                //        }
                //    }
                //    //total = cusEvents.Count;

                //    var groupedpa = preresolvedIncidents.GroupBy(item => item.EventId);
                //    preresolvedIncidents = groupedpa.Select(grp => grp.OrderBy(item => item.EventId).First()).ToList();

                //    foreach (var resolve in preresolvedIncidents)
                //    {
                //        if (resolve.EventType == (int)CustomEvent.EventTypes.DriverOffence)
                //            resolvedIncidents.Add(resolve);
                //    }
                //    total = pendingCount + inprogressCount + completedCount + resolvedIncidents.Count;
                //    Dictionary<string, int> counts = userList.GroupBy(x => x)
                //          .ToDictionary(g => g.Key,
                //                        g => g.Count());
                //    var items = from pair in counts
                //                orderby pair.Value descending
                //                select pair;

                //    Dictionary<string, int> offencecounts = offenceuserList.GroupBy(x => x)
                //    .ToDictionary(g => g.Key,
                //        g => g.Count());
                //    var offenceitems = from pair in offencecounts
                //                       orderby pair.Value descending
                //                       select pair;
                //    var offenceusercounter = 0;

                //    if (offenceuserList.Count > 0)
                //    {
                //        foreach (KeyValuePair<string, int> pair in offenceitems)
                //        {
                //            if (offenceusercounter == 0)
                //            {
                //                // lbTop1OffenceEvUser.Text = pair.Key.ToString();
                //                Top1TicName = pair.Key.ToString();
                //                Top1TicCount = pair.Value.ToString();
                //            }
                //            else if (offenceusercounter == 1)
                //            {
                //                // lbTop2OffenceEvUser.Text = pair.Key.ToString();
                //                Top2TicName = pair.Key.ToString();
                //                Top2TicCount = pair.Value.ToString();
                //            }
                //            else if (offenceusercounter == 2)
                //            {
                //                //lbTop3OffenceEvUser.Text = pair.Key.ToString();
                //                Top3TicName = pair.Key.ToString();
                //                Top3TicCount = pair.Value.ToString();
                //            }
                //            else if (offenceusercounter == 3)
                //            {
                //                // lbTop4OffenceEvUser.Text = pair.Key.ToString();
                //                Top4TicName = pair.Key.ToString();
                //                Top4TicCount = pair.Value.ToString();
                //            }
                //            else if (offenceusercounter == 4)
                //            {
                //                // lbTop5OffenceEvUser.Text = pair.Key.ToString();
                //                Top5TicName = pair.Key.ToString();
                //                Top5TicCount = pair.Value.ToString();
                //                break;
                //            }
                //            offenceusercounter++;
                //        }
                //    }
                //    var ticTotal = Convert.ToInt32(Top1TicCount) + Convert.ToInt32(Top2TicCount) + Convert.ToInt32(Top3TicCount) + Convert.ToInt32(Top4TicCount) + Convert.ToInt32(Top5TicCount);
                //    Top1Tic = ((int)Math.Round((float)Convert.ToInt32(Top1TicCount) / (float)ticTotal * (float)100)).ToString() + "%";
                //    Top2Tic = ((int)Math.Round((float)Convert.ToInt32(Top2TicCount) / (float)ticTotal * (float)100)).ToString() + "%";
                //    Top3Tic = ((int)Math.Round((float)Convert.ToInt32(Top3TicCount) / (float)ticTotal * (float)100)).ToString() + "%";
                //    Top4Tic = ((int)Math.Round((float)Convert.ToInt32(Top4TicCount) / (float)ticTotal * (float)100)).ToString() + "%";
                //    Top5Tic = ((int)Math.Round((float)Convert.ToInt32(Top5TicCount) / (float)ticTotal * (float)100)).ToString() + "%";

                //    var hoteventusercounter = 0;
                //    var hotTotal = 0;
                //    var hot1Count = 0;
                //    var hot2Count = 0;
                //    var hot3Count = 0;
                //    var hot4Count = 0;
                //    var hot5Count = 0;
                //    if (userList.Count > 0)
                //    {
                //        foreach (KeyValuePair<string, int> pair in items)
                //        {
                //            if (hoteventusercounter == 0)
                //            {
                //                // lbTop1HotEvCount.Text = pair.Value.ToString();
                //                //  lbTop1HotEvUser.Text = pair.Key.ToString();
                //                hotTotal = hotTotal + pair.Value;
                //                hot1Count = pair.Value;
                //            }
                //            else if (hoteventusercounter == 1)
                //            {
                //                // lbTop2HotEvCount.Text = pair.Value.ToString();
                //                // lbTop2HotEvUser.Text = pair.Key.ToString();
                //                hotTotal = hotTotal + pair.Value;
                //                hot2Count = pair.Value;
                //            }
                //            else if (hoteventusercounter == 2)
                //            {
                //                // lbTop3HotEvCount.Text = pair.Value.ToString();
                //                // lbTop3HotEvUser.Text = pair.Key.ToString();
                //                hotTotal = hotTotal + pair.Value;
                //                hot3Count = pair.Value;
                //            }
                //            else if (hoteventusercounter == 3)
                //            {
                //                //lbTop4HotEvCount.Text = pair.Value.ToString();
                //                //lbTop4HotEvUser.Text = pair.Key.ToString();
                //                hotTotal = hotTotal + pair.Value;
                //                hot4Count = pair.Value;
                //            }
                //            else if (hoteventusercounter == 4)
                //            {
                //                //lbTop5HotEvCount.Text = pair.Value.ToString();
                //                //lbTop5HotEvUser.Text = pair.Key.ToString();
                //                hotTotal = hotTotal + pair.Value;
                //                hot5Count = pair.Value;
                //                break;
                //            }
                //            hoteventusercounter++;
                //        }
                //    }
                //    if (hot1Count > 0)
                //    {
                //        hot1Percent = ((int)Math.Round((float)hot1Count / (float)hotTotal * (float)100)).ToString() + "%";
                //        hot1display = "block";
                //    }
                //    else
                //    {
                //        hot1Percent = "0%";
                //        hot1display = "none";
                //    }
                //    if (hot2Count > 0)
                //    {
                //        hot2Percent = ((int)Math.Round((float)hot2Count / (float)hotTotal * (float)100)).ToString() + "%";
                //        hot2display = "block";
                //    }
                //    else
                //    {
                //        hot2Percent = "0%";
                //        hot2display = "none";
                //    }
                //    if (hot3Count > 0)
                //    {
                //        hot3Percent = ((int)Math.Round((float)hot3Count / (float)hotTotal * (float)100)).ToString() + "%";
                //        hot3display = "block";
                //    }
                //    else
                //    {
                //        hot3Percent = "0%";
                //        hot3display = "none";
                //    }
                //    if (hot4Count > 0)
                //    {
                //        hot4Percent = ((int)Math.Round((float)hot4Count / (float)hotTotal * (float)100)).ToString() + "%";
                //        hot4display = "block";
                //    }
                //    else
                //    {
                //        hot4Percent = "0%";
                //        hot4display = "none";
                //    }
                //    if (hot5Count > 0)
                //    {
                //        hot5Percent = ((int)Math.Round((float)hot5Count / (float)hotTotal * (float)100)).ToString() + "%";
                //        hot5display = "block";
                //    }
                //    else
                //    {
                //        hot5Percent = "0%";
                //        hot5display = "none";
                //    }
                //    //inprogressCount = total - (pendingCount + completedCount);
                //    var pendingPercentage = (int)Math.Round((float)pendingCount / (float)total * (float)100);
                //    var inprogressPercentage = (int)Math.Round((float)inprogressCount / (float)total * (float)100);
                //    var completedPercentage = (int)Math.Round((float)completedCount / (float)total * (float)100);
                //    var resolvedPercentage = (int)Math.Round((float)resolvedIncidents.Count / (float)total * (float)100);

                //    if (resolvedIncidents.Count > 0)
                //    {
                //        resolvedPercent = resolvedPercentage.ToString();
                //        lbresolvedPercent.Text = resolvedIncidents.Count.ToString();
                //    }
                //    else
                //    {
                //        resolvedPercent = "0";
                //        lbresolvedPercent.Text = "0";
                //    }
                //    var newCount = inprogressCount + completedCount + resolvedIncidents.Count;

                //    if (newCount > 0)
                //        HandledPercentage = ((int)Math.Round((float)newCount / (float)total * (float)100)).ToString();
                //    else
                //        HandledPercentage = "0";

                //    //lbHandledAlarms.Text = newCount.ToString();//(total - pendingCount).ToString();
                //    if (pendingCount > 0)
                //    {
                //        pendingPercent = pendingPercentage.ToString();
                //        //lbPendingAlarms.Text = pendingCount.ToString();
                //        lbPending.Text = pendingCount.ToString();
                //        lbPendingpercent.Text = pendingPercentage.ToString();
                //    }
                //    else
                //    {
                //        pendingPercent = "0";
                //        lbPending.Text = "0";
                //        lbPendingpercent.Text = "0";
                //        //lbPendingAlarms.Text = "0";
                //    }
                //    if (inprogressCount > 0)
                //    {
                //        inprogressPercent = inprogressPercentage.ToString();
                //        lbInprogresspercent.Text = inprogressPercentage.ToString();
                //        lbInprogress.Text = inprogressCount.ToString();
                //    }
                //    else
                //    {
                //        lbInprogress.Text = "0";
                //        lbInprogresspercent.Text = "0";
                //        inprogressPercent = "0";
                //    }
                //    if (completedCount > 0)
                //    {
                //        completedPercent = completedPercentage.ToString();
                //        lbCompletedpercent.Text = completedPercentage.ToString();
                //        lbCompleted.Text = completedCount.ToString();
                //    }
                //    else
                //    {
                //        completedPercent = "0";
                //        lbCompletedpercent.Text = "0";
                //        lbCompleted.Text = "0";
                //    }

                //    lbTotalAlarms.Text = total.ToString();
                //    //lbTotalEvents.Text = total.ToString();

                //}
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("OthersDash.aspx", "getEventStatus", ex, dbConnection, userinfo.SiteId);
            }
        }

        [WebMethod]
        public static List<string> getEventStatusRetrieve(string fromdate, string todate, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var fromDt = Convert.ToDateTime(fromdate);
                var toDt = Convert.ToDateTime(todate);
                toDt = toDt.Add(new TimeSpan(23, 59, 0));
                var intList = new List<int>();
                var cusEvents = new List<CustomEvent>();
                var resolvedIncidents = new List<CustomEvent>();
                var preresolvedIncidents = new List<CustomEvent>();
                var pendingCount = 0;
                var inprogressCount = 0;
                var completedCount = 0;
                var total = 0;
                if (fromDt < toDt)
                {
                    if (userinfo != null)
                    {
                        pendingCount = 0;
                        inprogressCount = 0;
                        completedCount = 0;
                        total = 0;

                        var zticketday1week1 = 0;
                        var zticketday2week1 = 0;
                        var zticketday3week1 = 0;
                        var zticketday4week1 = 0;
                        var zticketday5week1 = 0;
                        var zticketday6week1 = 0;
                        var zticketday7week1 = 0;

                        var zticketday1week2 = 0;
                        var zticketday2week2 = 0;
                        var zticketday3week2 = 0;
                        var zticketday4week2 = 0;
                        var zticketday5week2 = 0;
                        var zticketday6week2 = 0;
                        var zticketday7week2 = 0;

                        var zticketday1week3 = 0;
                        var zticketday2week3 = 0;
                        var zticketday3week3 = 0;
                        var zticketday4week3 = 0;
                        var zticketday5week3 = 0;
                        var zticketday6week3 = 0;
                        var zticketday7week3 = 0;

                        var zticketday1week4 = 0;
                        var zticketday2week4 = 0;
                        var zticketday3week4 = 0;
                        var zticketday4week4 = 0;
                        var zticketday5week4 = 0;
                        var zticketday6week4 = 0;
                        var zticketday7week4 = 0;



                        if (userinfo != null)
                        {
                            if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                            {
                                cusEvents = CustomEvent.GetAllCustomEvents(dbConnection);
                                preresolvedIncidents = CustomEvent.GetAllCustomEventByDateHandled(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(365, 0, 0, 0)), dbConnection);
                            }
                            else if (userinfo.RoleId == (int)Role.CustomerUser)
                            {
                                cusEvents = CustomEvent.GetAllCustomEventByCId(userinfo.CustomerInfoId, dbConnection);
                                preresolvedIncidents = CustomEvent.GetAllCustomEventByDateHandled(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(365, 0, 0, 0)), dbConnection);
                                preresolvedIncidents = preresolvedIncidents.Where(i => i.CustomerId == userinfo.CustomerInfoId).ToList();
                                preresolvedIncidents = preresolvedIncidents.Where(i => i.UserName == userinfo.Username).ToList();
                                cusEvents = cusEvents.Where(i => i.UserName == userinfo.Username).ToList();
                            }
                            else
                            {
                                cusEvents = CustomEvent.GetAllCustomEventByCId(userinfo.CustomerInfoId, dbConnection);
                                preresolvedIncidents = CustomEvent.GetAllCustomEventByDateHandled(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(365, 0, 0, 0)), dbConnection);
                                preresolvedIncidents = preresolvedIncidents.Where(i => i.CustomerId == userinfo.CustomerInfoId).ToList();
                            }


                            //cusEvents = cusEvents.Where(i => (i.RecevieTime.Value.Month == toDt.Month && i.RecevieTime.Value.Year == toDt.Year) || (i.RecevieTime.Value.Month == fromDt.Month && i.RecevieTime.Value.Year == fromDt.Year)).ToList();

                            //preresolvedIncidents = preresolvedIncidents.Where(i => (i.RecevieTime.Value.Month == toDt.Month && i.RecevieTime.Value.Year == toDt.Year) || (i.RecevieTime.Value.Month == fromDt.Month && i.RecevieTime.Value.Year == fromDt.Year)).ToList();

                            cusEvents = cusEvents.Where(i => i.RecevieTime.Value.AddHours(userinfo.TimeZone).Date <= toDt.Date && i.RecevieTime.Value.AddHours(userinfo.TimeZone).Date >= fromDt.Date).ToList();

                            preresolvedIncidents = preresolvedIncidents.Where(i => i.RecevieTime.Value.AddHours(userinfo.TimeZone).Date <= toDt.Date && i.RecevieTime.Value.AddHours(userinfo.TimeZone).Date >= fromDt.Date).ToList();

                            var userList = new List<string>();
                            var offenceuserList = new List<string>();
                            if (cusEvents.Count > 0)
                            {
                                var grouped = cusEvents.GroupBy(item => item.EventId);
                                cusEvents = grouped.Select(grp => grp.OrderBy(item => item.EventId).First()).ToList();

                                foreach (var evs in cusEvents)
                                {
                                    if (evs.EventType == (int)CustomEvent.EventTypes.DriverOffence)
                                    {
                                        if (!evs.Handled)
                                        {
                                            var gTicket = DriverOffence.GetDriverOffenceById(evs.Identifier, dbConnection);
                                            if (gTicket != null)
                                            {
                                                if (gTicket.TicketStatus == (int)CustomEvent.IncidentActionStatus.Engage)
                                                {
                                                    inprogressCount++;
                                                }
                                                else if (gTicket.TicketStatus == (int)CustomEvent.IncidentActionStatus.Complete)
                                                {
                                                    completedCount++;
                                                }
                                                else
                                                {
                                                    pendingCount++;
                                                }
                                            }
                                        }
                                        else
                                        {

                                        }

                                        if (evs.EventType == CustomEvent.EventTypes.HotEvent || evs.EventType == CustomEvent.EventTypes.Request)
                                        {
                                            var reqEv = HotEvent.GetHotEventById(evs.Identifier, dbConnection);
                                            if (reqEv != null)
                                            {
                                                if (CommonUtility.getPCName(reqEv).ToLower() != "mims mobile")
                                                {
                                                    var retname = CommonUtility.getPCName(reqEv).ToLower();
                                                    if (!string.IsNullOrEmpty(retname))
                                                        userList.Add(retname);
                                                }
                                                else
                                                    userList.Add(evs.UserName.ToLower());
                                            }
                                        }
                                        if (evs.EventType == CustomEvent.EventTypes.MobileHotEvent)
                                        {
                                            userList.Add(evs.UserName.ToLower());
                                        }
                                        offenceuserList.Add(evs.CustomerUName.ToLower());
                                        if (evs.RecevieTime.Value.Date == new DateTime(fromDt.Year, fromDt.Month, 1))
                                        {
                                            zticketday1week1++;
                                        }
                                        else if (evs.RecevieTime.Value.Date == new DateTime(fromDt.Year, fromDt.Month, 2))
                                        {
                                            zticketday2week1++;
                                        }
                                        else if (evs.RecevieTime.Value.Date == new DateTime(fromDt.Year, fromDt.Month, 3))
                                        {
                                            zticketday3week1++;
                                        }
                                        else if (evs.RecevieTime.Value.Date == new DateTime(fromDt.Year, fromDt.Month, 4))
                                        {
                                            zticketday4week1++;
                                        }
                                        else if (evs.RecevieTime.Value.Date == new DateTime(fromDt.Year, fromDt.Month, 5))
                                        {
                                            zticketday5week1++;
                                        }
                                        else if (evs.RecevieTime.Value.Date == new DateTime(fromDt.Year, fromDt.Month, 6))
                                        {
                                            zticketday6week1++;
                                        }
                                        else if (evs.RecevieTime.Value.Date == new DateTime(fromDt.Year, fromDt.Month, 7))
                                        {
                                            zticketday7week1++;
                                        }
                                        else if (evs.RecevieTime.Value.Date == new DateTime(fromDt.Year, fromDt.Month, 8))
                                        {
                                            zticketday1week2++;
                                        }
                                        else if (evs.RecevieTime.Value.Date == new DateTime(fromDt.Year, fromDt.Month, 9))
                                        {
                                            zticketday2week2++;
                                        }
                                        else if (evs.RecevieTime.Value.Date == new DateTime(fromDt.Year, fromDt.Month, 10))
                                        {
                                            zticketday3week2++;
                                        }
                                        else if (evs.RecevieTime.Value.Date == new DateTime(fromDt.Year, fromDt.Month, 11))
                                        {
                                            zticketday4week2++;
                                        }
                                        else if (evs.RecevieTime.Value.Date == new DateTime(fromDt.Year, fromDt.Month, 12))
                                        {
                                            zticketday5week2++;
                                        }
                                        else if (evs.RecevieTime.Value.Date == new DateTime(fromDt.Year, fromDt.Month, 13))
                                        {
                                            zticketday6week2++;
                                        }
                                        else if (evs.RecevieTime.Value.Date == new DateTime(fromDt.Year, fromDt.Month, 14))
                                        {
                                            zticketday7week2++;
                                        }
                                        else if (evs.RecevieTime.Value.Date == new DateTime(fromDt.Year, fromDt.Month, 15))
                                        {
                                            zticketday1week3++;
                                        }
                                        else if (evs.RecevieTime.Value.Date == new DateTime(fromDt.Year, fromDt.Month, 16))
                                        {
                                            zticketday2week3++;
                                        }
                                        else if (evs.RecevieTime.Value.Date == new DateTime(fromDt.Year, fromDt.Month, 17))
                                        {
                                            zticketday3week3++;
                                        }
                                        else if (evs.RecevieTime.Value.Date == new DateTime(fromDt.Year, fromDt.Month, 18))
                                        {
                                            zticketday4week3++;
                                        }
                                        else if (evs.RecevieTime.Value.Date == new DateTime(fromDt.Year, fromDt.Month, 19))
                                        {
                                            zticketday5week3++;
                                        }
                                        else if (evs.RecevieTime.Value.Date == new DateTime(fromDt.Year, fromDt.Month, 20))
                                        {
                                            zticketday6week3++;
                                        }
                                        else if (evs.RecevieTime.Value.Date == new DateTime(fromDt.Year, fromDt.Month, 21))
                                        {
                                            zticketday7week3++;
                                        }
                                        else if (evs.RecevieTime.Value.Date == new DateTime(fromDt.Year, fromDt.Month, 22))
                                        {
                                            zticketday1week4++;
                                        }
                                        else if (evs.RecevieTime.Value.Date == new DateTime(fromDt.Year, fromDt.Month, 23))
                                        {
                                            zticketday2week4++;
                                        }
                                        else if (evs.RecevieTime.Value.Date == new DateTime(fromDt.Year, fromDt.Month, 24))
                                        {
                                            zticketday3week4++;
                                        }
                                        else if (evs.RecevieTime.Value.Date == new DateTime(fromDt.Year, fromDt.Month, 25))
                                        {
                                            zticketday4week4++;
                                        }
                                        else if (evs.RecevieTime.Value.Date == new DateTime(fromDt.Year, fromDt.Month, 26))
                                        {
                                            zticketday5week4++;
                                        }
                                        else if (evs.RecevieTime.Value.Date == new DateTime(fromDt.Year, fromDt.Month, 27))
                                        {
                                            zticketday6week4++;
                                        }
                                        else if (evs.RecevieTime.Value.Date >= new DateTime(fromDt.Year, fromDt.Month, 28) && evs.RecevieTime.Value.Date < new DateTime(fromDt.Year, (fromDt.Month + 1), 1))
                                        {
                                            zticketday7week4++;
                                        }
                                    }
                                }
                            }

                            var groupedpa = preresolvedIncidents.GroupBy(item => item.EventId);
                            preresolvedIncidents = groupedpa.Select(grp => grp.OrderBy(item => item.EventId).First()).ToList();

                            foreach (var resolve in preresolvedIncidents)
                            {
                                if (resolve.EventType == (int)CustomEvent.EventTypes.DriverOffence)
                                    resolvedIncidents.Add(resolve);
                            }
                            total = pendingCount + inprogressCount + completedCount + resolvedIncidents.Count;
                            Dictionary<string, int> counts = userList.GroupBy(x => x)
                                  .ToDictionary(g => g.Key,
                                                g => g.Count());
                            var items = from pair in counts
                                        orderby pair.Value descending
                                        select pair;

                            Dictionary<string, int> offencecounts = offenceuserList.GroupBy(x => x)
                            .ToDictionary(g => g.Key,
                                g => g.Count());
                            var offenceitems = from pair in offencecounts
                                               orderby pair.Value descending
                                               select pair;
                            var offenceusercounter = 0;

                            var zTop1TicName = string.Empty;
                            var zTop2TicName = string.Empty;
                            var zTop3TicName = string.Empty;
                            var zTop4TicName = string.Empty;
                            var zTop5TicName = string.Empty;
                            var zTop1TicCount = "0";
                            var zTop2TicCount = "0";
                            var zTop3TicCount = "0";
                            var zTop4TicCount = "0";
                            var zTop5TicCount = "0";

                            if (offenceuserList.Count > 0)
                            {
                                foreach (KeyValuePair<string, int> pair in offenceitems)
                                {
                                    if (offenceusercounter == 0)
                                    {
                                        // lbTop1OffenceEvUser.Text = pair.Key.ToString();
                                        zTop1TicName = pair.Key.ToString();
                                        zTop1TicCount = pair.Value.ToString();
                                    }
                                    else if (offenceusercounter == 1)
                                    {
                                        // lbTop2OffenceEvUser.Text = pair.Key.ToString();
                                        zTop2TicName = pair.Key.ToString();
                                        zTop2TicCount = pair.Value.ToString();
                                    }
                                    else if (offenceusercounter == 2)
                                    {
                                        //lbTop3OffenceEvUser.Text = pair.Key.ToString();
                                        zTop3TicName = pair.Key.ToString();
                                        zTop3TicCount = pair.Value.ToString();
                                    }
                                    else if (offenceusercounter == 3)
                                    {
                                        // lbTop4OffenceEvUser.Text = pair.Key.ToString();
                                        zTop4TicName = pair.Key.ToString();
                                        zTop4TicCount = pair.Value.ToString();
                                    }
                                    else if (offenceusercounter == 4)
                                    {
                                        // lbTop5OffenceEvUser.Text = pair.Key.ToString();
                                        zTop5TicName = pair.Key.ToString();
                                        zTop5TicCount = pair.Value.ToString();
                                        break;
                                    }
                                    offenceusercounter++;
                                }
                            }
                            var ticTotal = Convert.ToInt32(zTop1TicCount) + Convert.ToInt32(zTop2TicCount) + Convert.ToInt32(zTop3TicCount) + Convert.ToInt32(zTop4TicCount) + Convert.ToInt32(zTop5TicCount);
                            var zTop1Tic = ((int)Math.Round((float)Convert.ToInt32(zTop1TicCount) / (float)ticTotal * (float)100)).ToString() + "%";
                            var zTop2Tic = ((int)Math.Round((float)Convert.ToInt32(zTop2TicCount) / (float)ticTotal * (float)100)).ToString() + "%";
                            var zTop3Tic = ((int)Math.Round((float)Convert.ToInt32(zTop3TicCount) / (float)ticTotal * (float)100)).ToString() + "%";
                            var zTop4Tic = ((int)Math.Round((float)Convert.ToInt32(zTop4TicCount) / (float)ticTotal * (float)100)).ToString() + "%";
                            var zTop5Tic = ((int)Math.Round((float)Convert.ToInt32(zTop5TicCount) / (float)ticTotal * (float)100)).ToString() + "%";

                            if (Convert.ToInt32(zTop1TicCount) <= 0)
                            {
                                zTop1Tic = "0%";
                            }
                            if (Convert.ToInt32(zTop2TicCount) <= 0)
                            {
                                zTop2Tic = "0%";
                            }
                            if (Convert.ToInt32(zTop3TicCount) <= 0)
                            {
                                zTop3Tic = "0%";
                            }
                            if (Convert.ToInt32(zTop4TicCount) <= 0)
                            {
                                zTop4Tic = "0%";
                            }
                            if (Convert.ToInt32(zTop5TicCount) <= 0)
                            {
                                zTop5Tic = "0%";
                            }

                            var hoteventusercounter = 0;
                            var hotTotal = 0;
                            var hot1Count = 0;
                            var hot2Count = 0;
                            var hot3Count = 0;
                            var hot4Count = 0;
                            var hot5Count = 0;
                            if (userList.Count > 0)
                            {
                                foreach (KeyValuePair<string, int> pair in items)
                                {
                                    if (hoteventusercounter == 0)
                                    {
                                        // lbTop1HotEvCount.Text = pair.Value.ToString();
                                        //  lbTop1HotEvUser.Text = pair.Key.ToString();
                                        hotTotal = hotTotal + pair.Value;
                                        hot1Count = pair.Value;
                                    }
                                    else if (hoteventusercounter == 1)
                                    {
                                        // lbTop2HotEvCount.Text = pair.Value.ToString();
                                        // lbTop2HotEvUser.Text = pair.Key.ToString();
                                        hotTotal = hotTotal + pair.Value;
                                        hot2Count = pair.Value;
                                    }
                                    else if (hoteventusercounter == 2)
                                    {
                                        // lbTop3HotEvCount.Text = pair.Value.ToString();
                                        // lbTop3HotEvUser.Text = pair.Key.ToString();
                                        hotTotal = hotTotal + pair.Value;
                                        hot3Count = pair.Value;
                                    }
                                    else if (hoteventusercounter == 3)
                                    {
                                        //lbTop4HotEvCount.Text = pair.Value.ToString();
                                        //lbTop4HotEvUser.Text = pair.Key.ToString();
                                        hotTotal = hotTotal + pair.Value;
                                        hot4Count = pair.Value;
                                    }
                                    else if (hoteventusercounter == 4)
                                    {
                                        //lbTop5HotEvCount.Text = pair.Value.ToString();
                                        //lbTop5HotEvUser.Text = pair.Key.ToString();
                                        hotTotal = hotTotal + pair.Value;
                                        hot5Count = pair.Value;
                                        break;
                                    }
                                    hoteventusercounter++;
                                }
                            }
                            //var zhot1Percent = "0%";
                            //var zhot2Percent = "0%";
                            //var zhot3Percent = "0%";
                            //var zhot4Percent = "0%";
                            //var zhot5Percent = "0%";

                            //if (hot1Count > 0)
                            //{
                            //    zhot1Percent = ((int)Math.Round((float)hot1Count / (float)hotTotal * (float)100)).ToString() + "%";

                            //}
                            //if (hot2Count > 0)
                            //{
                            //    zhot2Percent = ((int)Math.Round((float)hot2Count / (float)hotTotal * (float)100)).ToString() + "%";

                            //}
                            //if (hot3Count > 0)
                            //{
                            //    zhot3Percent = ((int)Math.Round((float)hot3Count / (float)hotTotal * (float)100)).ToString() + "%";

                            //}
                            //if (hot4Count > 0)
                            //{
                            //    zhot4Percent = ((int)Math.Round((float)hot4Count / (float)hotTotal * (float)100)).ToString() + "%";

                            //}
                            //if (hot5Count > 0)
                            //{
                            //    zhot5Percent = ((int)Math.Round((float)hot5Count / (float)hotTotal * (float)100)).ToString() + "%";

                            //}
                            //inprogressCount = total - (pendingCount + completedCount);
                            var pendingPercentage = (int)Math.Round((float)pendingCount / (float)total * (float)100);
                            var inprogressPercentage = (int)Math.Round((float)inprogressCount / (float)total * (float)100);
                            var completedPercentage = (int)Math.Round((float)completedCount / (float)total * (float)100);
                            var resolvedPercentage = (int)Math.Round((float)resolvedIncidents.Count / (float)total * (float)100);

                            var zresolvedPercent = "0";
                            var zlbresolvedPercent = "0";

                            if (resolvedIncidents.Count > 0)
                            {
                                zresolvedPercent = resolvedPercentage.ToString();
                                zlbresolvedPercent = resolvedIncidents.Count.ToString();
                            }
                            var newCount = inprogressCount + completedCount + resolvedIncidents.Count;

                            //if (newCount > 0)
                            //    HandledPercentage = ((int)Math.Round((float)newCount / (float)total * (float)100)).ToString();
                            //else
                            //    HandledPercentage = "0";

                            //lbHandledAlarms.Text = newCount.ToString();//(total - pendingCount).ToString();

                            var zlbHandledAlarms = "0";

                            zlbHandledAlarms = newCount.ToString();
                            var zpendingPercent = "0";
                            var zlbPending = "0";
                            var zlbPendingpercent = "0";
                            var zlbPendingAlarms = "0";

                            var zlbInprogress = "0";
                            var zlbInprogresspercent = "0";
                            var zinprogressPercent = "0";

                            var zcompletedPercent = "0";
                            var zlbCompletedpercent = "0";
                            var zlbCompleted = "0";

                            if (pendingCount > 0)
                            {
                                zpendingPercent = pendingPercentage.ToString();
                                //lbPendingAlarms.Text = pendingCount.ToString();
                                zlbPending = pendingCount.ToString();
                                zlbPendingpercent = pendingPercentage.ToString();
                            }
                            if (inprogressCount > 0)
                            {
                                zinprogressPercent = inprogressPercentage.ToString();
                                zlbInprogresspercent = inprogressPercentage.ToString();
                                zlbInprogress = inprogressCount.ToString();
                            }
                            if (completedCount > 0)
                            {
                                zcompletedPercent = completedPercentage.ToString();
                                zlbCompletedpercent = completedPercentage.ToString();
                                zlbCompleted = completedCount.ToString();
                            }

                            var zlbTotalAlarms = "0";
                            zlbTotalAlarms = total.ToString();

                            listy.Add(zlbresolvedPercent);
                            listy.Add(zresolvedPercent);

                            listy.Add(zlbCompleted);
                            listy.Add(zlbCompletedpercent);

                            listy.Add(zlbInprogress);
                            listy.Add(zlbInprogresspercent);

                            listy.Add(zlbPending);
                            listy.Add(zlbPendingpercent);

                            listy.Add(zlbTotalAlarms);

                            var firstDayOfMonth = new DateTime(fromDt.Year, fromDt.Month, 1);
                            var zday1 = firstDayOfMonth.ToString("yyyy-MM-dd");
                            var zday2 = firstDayOfMonth.AddDays(1).ToString("yyyy-MM-dd");
                            var zday3 = firstDayOfMonth.AddDays(2).ToString("yyyy-MM-dd");
                            var zday4 = firstDayOfMonth.AddDays(3).ToString("yyyy-MM-dd");
                            var zday5 = firstDayOfMonth.AddDays(4).ToString("yyyy-MM-dd");
                            var zday6 = firstDayOfMonth.AddDays(5).ToString("yyyy-MM-dd");
                            var zday7 = firstDayOfMonth.AddDays(6).ToString("yyyy-MM-dd");

                            listy.Add(zday1);
                            listy.Add(zday2);
                            listy.Add(zday3);
                            listy.Add(zday4);
                            listy.Add(zday5);
                            listy.Add(zday6);
                            listy.Add(zday7);

                            listy.Add(zticketday1week1.ToString());
                            listy.Add(zticketday1week2.ToString());
                            listy.Add(zticketday1week3.ToString());
                            listy.Add(zticketday1week4.ToString());

                            listy.Add(zticketday2week1.ToString());
                            listy.Add(zticketday2week2.ToString());
                            listy.Add(zticketday2week3.ToString());
                            listy.Add(zticketday2week4.ToString());

                            listy.Add(zticketday3week1.ToString());
                            listy.Add(zticketday3week2.ToString());
                            listy.Add(zticketday3week3.ToString());
                            listy.Add(zticketday3week4.ToString());

                            listy.Add(zticketday4week1.ToString());
                            listy.Add(zticketday4week2.ToString());
                            listy.Add(zticketday4week3.ToString());
                            listy.Add(zticketday4week4.ToString());

                            listy.Add(zticketday5week1.ToString());
                            listy.Add(zticketday5week2.ToString());
                            listy.Add(zticketday5week3.ToString());
                            listy.Add(zticketday5week4.ToString());

                            listy.Add(zticketday6week1.ToString());
                            listy.Add(zticketday6week2.ToString());
                            listy.Add(zticketday6week3.ToString());
                            listy.Add(zticketday6week4.ToString());

                            listy.Add(zticketday7week1.ToString());
                            listy.Add(zticketday7week2.ToString());
                            listy.Add(zticketday7week3.ToString());
                            listy.Add(zticketday7week4.ToString());


                            var tenthDayOfMonth = firstDayOfMonth.Add(new TimeSpan(6, 0, 0, 0));
                            var fifteenDayOfMonth = tenthDayOfMonth.Add(new TimeSpan(7, 0, 0, 0));
                            var twentyDayOfMonth = fifteenDayOfMonth.Add(new TimeSpan(7, 0, 0, 0));
                            var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);

                            listy.Add(firstDayOfMonth.ToShortDateString());
                            listy.Add(tenthDayOfMonth.ToShortDateString());
                            listy.Add(fifteenDayOfMonth.ToShortDateString());
                            listy.Add(twentyDayOfMonth.ToShortDateString());
                            listy.Add(lastDayOfMonth.ToShortDateString());

                            var znotificationCount1 = 0;
                            var znotificationCount2 = 0;
                            var znotificationCount3 = 0;
                            var znotificationCount4 = 0;
                            var notifications = new List<Notification>();

                            if (userinfo.RoleId == (int)Role.Manager)
                            {
                                notifications = Notification.GetAllNotificationsByCreator(userinfo.Username, dbConnection);
                            }
                            else if (userinfo.RoleId == (int)Role.Admin)
                            {

                                notifications = Notification.GetAllNotificationsByCreator(userinfo.Username, dbConnection);
                                var sessions = DirectorManager.GetAllFullManagersByDirectorId(userinfo.ID, dbConnection);
                                foreach (var usr in sessions)
                                {
                                    notifications.AddRange(Notification.GetAllNotificationsByCreator(usr.Username, dbConnection));
                                } 
                            }
                            else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                            {
                                notifications = Notification.GetAllNotificationsByCId(userinfo.CustomerInfoId, dbConnection); 
                            }
                            else if (userinfo.RoleId == (int)Role.Director)
                            {
                                notifications = Notification.GetAllNotificationsBySiteId(userinfo.SiteId, dbConnection);
                                notifications = notifications.Where(i => i.SiteId == userinfo.SiteId).ToList();
                            }
                            else if (userinfo.RoleId == (int)Role.Regional)
                            {
                                var notis = Notification.GetAllNotificationsByLevel5(userinfo.ID, dbConnection);

                                notifications.AddRange(notis);
                            }
                            else if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                                notifications = Notification.GetAllNotifications(dbConnection);

                            //notifications = notifications.Where(i => (i.CreatedDate.Value.Month == toDt.Month && i.CreatedDate.Value.Year == toDt.Year) || (i.CreatedDate.Value.Month == fromDt.Month && i.CreatedDate.Value.Year == fromDt.Year)).ToList();

                            notifications = notifications.Where(i => i.CreatedDate.Value.AddHours(userinfo.TimeZone).Date <= toDt.Date && i.CreatedDate.Value.AddHours(userinfo.TimeZone).Date >= fromDt.Date).ToList();

                            var groupednotification = notifications.GroupBy(item => item.Id);
                            notifications = groupednotification.Select(grp => grp.OrderBy(item => item.Id).First()).ToList();

                            foreach (var noti in notifications)
                            {
                                if (noti.CreatedDate.Value.Date >= firstDayOfMonth.Date && noti.CreatedDate < tenthDayOfMonth.Date)
                                {
                                    znotificationCount1++;
                                }
                                else if (noti.CreatedDate.Value.Date >= tenthDayOfMonth.Date && noti.CreatedDate < fifteenDayOfMonth.Date)
                                {
                                    znotificationCount2++;
                                }
                                else if (noti.CreatedDate.Value.Date >= fifteenDayOfMonth.Date && noti.CreatedDate < twentyDayOfMonth.Date)
                                {
                                    znotificationCount3++;
                                }
                                else if (noti.CreatedDate.Value.Date >= twentyDayOfMonth.Date && noti.CreatedDate < lastDayOfMonth.Date)
                                {
                                    znotificationCount4++;
                                }
                            }

                            listy.Add(znotificationCount1.ToString());
                            listy.Add(znotificationCount2.ToString());
                            listy.Add(znotificationCount3.ToString());
                            listy.Add(znotificationCount4.ToString());

                            listy.Add(zTop1Tic);
                            listy.Add(zTop2Tic);
                            listy.Add(zTop3Tic);
                            listy.Add(zTop4Tic);
                            listy.Add(zTop5Tic);


                            listy.Add(zTop1TicName);
                            listy.Add(zTop2TicName);
                            listy.Add(zTop3TicName);
                            listy.Add(zTop4TicName);
                            listy.Add(zTop5TicName);

                            listy.Add(zTop1TicCount);
                            listy.Add(zTop2TicCount);
                            listy.Add(zTop3TicCount);
                            listy.Add(zTop4TicCount);
                            listy.Add(zTop5TicCount);
                        }
                    }
                }
                else
                {
                    listy.Add("ERROR");
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("OthersDash", "getEventStatusRetrieve", ex, dbConnection, userinfo.SiteId);
                listy.Clear();
                listy.Add("ERROR");
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getEventStatusTotal(string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var cusEvents = new List<CustomEvent>();
                var resolvedIncidents = new List<CustomEvent>();
                var preresolvedIncidents = new List<CustomEvent>();
                var pendingCount = 0;
                var inprogressCount = 0;
                var completedCount = 0;
                var total = 0;

                var fromDt = new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 1);
                var toDt = fromDt.AddMonths(1).AddDays(-1);
                toDt = toDt.Add(new TimeSpan(23, 59, 0));

                if (userinfo != null)
                {
                    pendingCount = 0;
                    inprogressCount = 0;
                    completedCount = 0;
                    total = 0;

                    var zticketday1week1 = 0;
                    var zticketday2week1 = 0;
                    var zticketday3week1 = 0;
                    var zticketday4week1 = 0;
                    var zticketday5week1 = 0;
                    var zticketday6week1 = 0;
                    var zticketday7week1 = 0;

                    var zticketday1week2 = 0;
                    var zticketday2week2 = 0;
                    var zticketday3week2 = 0;
                    var zticketday4week2 = 0;
                    var zticketday5week2 = 0;
                    var zticketday6week2 = 0;
                    var zticketday7week2 = 0;

                    var zticketday1week3 = 0;
                    var zticketday2week3 = 0;
                    var zticketday3week3 = 0;
                    var zticketday4week3 = 0;
                    var zticketday5week3 = 0;
                    var zticketday6week3 = 0;
                    var zticketday7week3 = 0;

                    var zticketday1week4 = 0;
                    var zticketday2week4 = 0;
                    var zticketday3week4 = 0;
                    var zticketday4week4 = 0;
                    var zticketday5week4 = 0;
                    var zticketday6week4 = 0;
                    var zticketday7week4 = 0;


 
                                       if (userinfo != null)
                    {
                        if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                        {
                            cusEvents = CustomEvent.GetAllCustomEvents(dbConnection);
                            preresolvedIncidents = CustomEvent.GetAllCustomEventByDateHandled(toDt, fromDt, dbConnection);
                        }
                        else if (userinfo.RoleId == (int)Role.CustomerUser)
                        {
                            cusEvents = CustomEvent.GetAllCustomEventByCId(userinfo.CustomerInfoId, dbConnection);
                            preresolvedIncidents = CustomEvent.GetAllCustomEventByDateHandled(toDt, fromDt, dbConnection);
                            preresolvedIncidents = preresolvedIncidents.Where(i => i.CustomerId == userinfo.CustomerInfoId).ToList();
                            preresolvedIncidents = preresolvedIncidents.Where(i => i.UserName == userinfo.Username).ToList();
                            cusEvents = cusEvents.Where(i => i.UserName == userinfo.Username).ToList();
                        }
                        else
                        {
                            cusEvents = CustomEvent.GetAllCustomEventByCId(userinfo.CustomerInfoId, dbConnection);
                            preresolvedIncidents = CustomEvent.GetAllCustomEventByDateHandled(toDt, fromDt, dbConnection);
                            preresolvedIncidents = preresolvedIncidents.Where(i => i.CustomerId == userinfo.CustomerInfoId).ToList();
                        }


                        cusEvents = cusEvents.Where(i => (i.RecevieTime.Value.Month == toDt.Month && i.RecevieTime.Value.Year == toDt.Year) || (i.RecevieTime.Value.Month == fromDt.Month && i.RecevieTime.Value.Year == fromDt.Year)).ToList();

                        preresolvedIncidents = preresolvedIncidents.Where(i => (i.RecevieTime.Value.Month == toDt.Month && i.RecevieTime.Value.Year == toDt.Year) || (i.RecevieTime.Value.Month == fromDt.Month && i.RecevieTime.Value.Year == fromDt.Year)).ToList();

                        cusEvents = cusEvents.Where(i => i.RecevieTime.Value.AddHours(userinfo.TimeZone).Date <= toDt.Date && i.RecevieTime.Value.AddHours(userinfo.TimeZone).Date >= fromDt.Date).ToList();

                        preresolvedIncidents = preresolvedIncidents.Where(i => i.RecevieTime.Value.AddHours(userinfo.TimeZone).Date <= toDt.Date && i.RecevieTime.Value.AddHours(userinfo.TimeZone).Date >= fromDt.Date).ToList();
 
                        var userList = new List<string>();
                        var offenceuserList = new List<string>();
                        if (cusEvents.Count > 0)
                        {
                            var grouped = cusEvents.GroupBy(item => item.EventId);
                            cusEvents = grouped.Select(grp => grp.OrderBy(item => item.EventId).First()).ToList();

                            foreach (var evs in cusEvents)
                            {
                                if (evs.EventType == (int)CustomEvent.EventTypes.DriverOffence)
                                {
                                    if (!evs.Handled)
                                    {
                                        var gTicket = DriverOffence.GetDriverOffenceById(evs.Identifier, dbConnection);
                                        if (gTicket != null)
                                        {
                                            if (gTicket.TicketStatus == (int)CustomEvent.IncidentActionStatus.Engage)
                                            {
                                                inprogressCount++;
                                            }
                                            else if (gTicket.TicketStatus == (int)CustomEvent.IncidentActionStatus.Complete)
                                            {
                                                completedCount++;
                                            }
                                            else
                                            {
                                                pendingCount++;
                                            }
                                        }
                                    }
                                    else
                                    { 

                                    } 

                                    if (evs.EventType == CustomEvent.EventTypes.HotEvent || evs.EventType == CustomEvent.EventTypes.Request)
                                    {
                                        var reqEv = HotEvent.GetHotEventById(evs.Identifier, dbConnection);
                                        if (reqEv != null)
                                        {
                                            if (CommonUtility.getPCName(reqEv).ToLower() != "mims mobile")
                                            {
                                                var retname = CommonUtility.getPCName(reqEv).ToLower();
                                                if (!string.IsNullOrEmpty(retname))
                                                    userList.Add(retname);
                                            }
                                            else
                                                userList.Add(evs.UserName.ToLower());
                                        }
                                    }
                                    if (evs.EventType == CustomEvent.EventTypes.MobileHotEvent)
                                    {
                                        userList.Add(evs.UserName.ToLower());
                                    }
                                    offenceuserList.Add(evs.CustomerUName.ToLower());
                                    if (evs.RecevieTime.Value.Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 1))
                                    {
                                        zticketday1week1++;
                                    }
                                    else if (evs.RecevieTime.Value.Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 2))
                                    {
                                        zticketday2week1++;
                                    }
                                    else if (evs.RecevieTime.Value.Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 3))
                                    {
                                        zticketday3week1++;
                                    }
                                    else if (evs.RecevieTime.Value.Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 4))
                                    {
                                        zticketday4week1++;
                                    }
                                    else if (evs.RecevieTime.Value.Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 5))
                                    {
                                        zticketday5week1++;
                                    }
                                    else if (evs.RecevieTime.Value.Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 6))
                                    {
                                        zticketday6week1++;
                                    }
                                    else if (evs.RecevieTime.Value.Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 7))
                                    {
                                        zticketday7week1++;
                                    }
                                    else if (evs.RecevieTime.Value.Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 8))
                                    {
                                        zticketday1week2++;
                                    }
                                    else if (evs.RecevieTime.Value.Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 9))
                                    {
                                        zticketday2week2++;
                                    }
                                    else if (evs.RecevieTime.Value.Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 10))
                                    {
                                        zticketday3week2++;
                                    }
                                    else if (evs.RecevieTime.Value.Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 11))
                                    {
                                        zticketday4week2++;
                                    }
                                    else if (evs.RecevieTime.Value.Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 12))
                                    {
                                        zticketday5week2++;
                                    }
                                    else if (evs.RecevieTime.Value.Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 13))
                                    {
                                        zticketday6week2++;
                                    }
                                    else if (evs.RecevieTime.Value.Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 14))
                                    {
                                        zticketday7week2++;
                                    }
                                    else if (evs.RecevieTime.Value.Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 15))
                                    {
                                        zticketday1week3++;
                                    }
                                    else if (evs.RecevieTime.Value.Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 16))
                                    {
                                        zticketday2week3++;
                                    }
                                    else if (evs.RecevieTime.Value.Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 17))
                                    {
                                        zticketday3week3++;
                                    }
                                    else if (evs.RecevieTime.Value.Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 18))
                                    {
                                        zticketday4week3++;
                                    }
                                    else if (evs.RecevieTime.Value.Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 19))
                                    {
                                        zticketday5week3++;
                                    }
                                    else if (evs.RecevieTime.Value.Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 20))
                                    {
                                        zticketday6week3++;
                                    }
                                    else if (evs.RecevieTime.Value.Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 21))
                                    {
                                        zticketday7week3++;
                                    }
                                    else if (evs.RecevieTime.Value.Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 22))
                                    {
                                        zticketday1week4++;
                                    }
                                    else if (evs.RecevieTime.Value.Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 23))
                                    {
                                        zticketday2week4++;
                                    }
                                    else if (evs.RecevieTime.Value.Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 24))
                                    {
                                        zticketday3week4++;
                                    }
                                    else if (evs.RecevieTime.Value.Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 25))
                                    {
                                        zticketday4week4++;
                                    }
                                    else if (evs.RecevieTime.Value.Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 26))
                                    {
                                        zticketday5week4++;
                                    }
                                    else if (evs.RecevieTime.Value.Date == new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 27))
                                    {
                                        zticketday6week4++;
                                    }
                                    else if (evs.RecevieTime.Value.Date >= new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 28) && evs.RecevieTime.Value.Date < new DateTime(CommonUtility.getDTNow().Year, (CommonUtility.getDTNow().Month + 1), 1))
                                    {
                                        zticketday7week4++;
                                    }
                                }
                            }
                        }

                        var groupedpa = preresolvedIncidents.GroupBy(item => item.EventId);
                        preresolvedIncidents = groupedpa.Select(grp => grp.OrderBy(item => item.EventId).First()).ToList();

                        foreach (var resolve in preresolvedIncidents)
                        {
                            if (resolve.EventType == (int)CustomEvent.EventTypes.DriverOffence)
                                resolvedIncidents.Add(resolve);
                        }
                        total = pendingCount + inprogressCount + completedCount + resolvedIncidents.Count;
                        Dictionary<string, int> counts = userList.GroupBy(x => x)
                              .ToDictionary(g => g.Key,
                                            g => g.Count());
                        var items = from pair in counts
                                    orderby pair.Value descending
                                    select pair;

                        Dictionary<string, int> offencecounts = offenceuserList.GroupBy(x => x)
                        .ToDictionary(g => g.Key,
                            g => g.Count());
                        var offenceitems = from pair in offencecounts
                                           orderby pair.Value descending
                                           select pair;
                        var offenceusercounter = 0;

                        var zTop1TicName = string.Empty;
                        var zTop2TicName = string.Empty;
                        var zTop3TicName = string.Empty;
                        var zTop4TicName = string.Empty;
                        var zTop5TicName = string.Empty;
                        var zTop1TicCount = "0";
                        var zTop2TicCount = "0";
                        var zTop3TicCount = "0";
                        var zTop4TicCount = "0";
                        var zTop5TicCount = "0";

                        if (offenceuserList.Count > 0)
                        {
                            foreach (KeyValuePair<string, int> pair in offenceitems)
                            {
                                if (offenceusercounter == 0)
                                {
                                    // lbTop1OffenceEvUser.Text = pair.Key.ToString();
                                    zTop1TicName = pair.Key.ToString();
                                    zTop1TicCount = pair.Value.ToString();
                                }
                                else if (offenceusercounter == 1)
                                {
                                    // lbTop2OffenceEvUser.Text = pair.Key.ToString();
                                    zTop2TicName = pair.Key.ToString();
                                    zTop2TicCount = pair.Value.ToString();
                                }
                                else if (offenceusercounter == 2)
                                {
                                    //lbTop3OffenceEvUser.Text = pair.Key.ToString();
                                    zTop3TicName = pair.Key.ToString();
                                    zTop3TicCount = pair.Value.ToString();
                                }
                                else if (offenceusercounter == 3)
                                {
                                    // lbTop4OffenceEvUser.Text = pair.Key.ToString();
                                    zTop4TicName = pair.Key.ToString();
                                    zTop4TicCount = pair.Value.ToString();
                                }
                                else if (offenceusercounter == 4)
                                {
                                    // lbTop5OffenceEvUser.Text = pair.Key.ToString();
                                    zTop5TicName = pair.Key.ToString();
                                    zTop5TicCount = pair.Value.ToString();
                                    break;
                                }
                                offenceusercounter++;
                            }
                        }
                        var ticTotal = Convert.ToInt32(zTop1TicCount) + Convert.ToInt32(zTop2TicCount) + Convert.ToInt32(zTop3TicCount) + Convert.ToInt32(zTop4TicCount) + Convert.ToInt32(zTop5TicCount);
                        var zTop1Tic = ((int)Math.Round((float)Convert.ToInt32(zTop1TicCount) / (float)ticTotal * (float)100)).ToString() + "%";
                        var zTop2Tic = ((int)Math.Round((float)Convert.ToInt32(zTop2TicCount) / (float)ticTotal * (float)100)).ToString() + "%";
                        var zTop3Tic = ((int)Math.Round((float)Convert.ToInt32(zTop3TicCount) / (float)ticTotal * (float)100)).ToString() + "%";
                        var zTop4Tic = ((int)Math.Round((float)Convert.ToInt32(zTop4TicCount) / (float)ticTotal * (float)100)).ToString() + "%";
                        var zTop5Tic = ((int)Math.Round((float)Convert.ToInt32(zTop5TicCount) / (float)ticTotal * (float)100)).ToString() + "%";

                        if (Convert.ToInt32(zTop1TicCount) <= 0)
                        {
                            zTop1Tic = "0%";
                        }
                        if (Convert.ToInt32(zTop2TicCount) <= 0)
                        {
                            zTop2Tic = "0%";
                        }
                        if (Convert.ToInt32(zTop3TicCount) <= 0)
                        {
                            zTop3Tic = "0%";
                        }
                        if (Convert.ToInt32(zTop4TicCount) <= 0)
                        {
                            zTop4Tic = "0%";
                        }
                        if (Convert.ToInt32(zTop5TicCount) <= 0)
                        {
                            zTop5Tic = "0%";
                        }
                        var hoteventusercounter = 0;
                        var hotTotal = 0;
                        var hot1Count = 0;
                        var hot2Count = 0;
                        var hot3Count = 0;
                        var hot4Count = 0;
                        var hot5Count = 0;
                        if (userList.Count > 0)
                        {
                            foreach (KeyValuePair<string, int> pair in items)
                            {
                                if (hoteventusercounter == 0)
                                {
                                    // lbTop1HotEvCount.Text = pair.Value.ToString();
                                    //  lbTop1HotEvUser.Text = pair.Key.ToString();
                                    hotTotal = hotTotal + pair.Value;
                                    hot1Count = pair.Value;
                                }
                                else if (hoteventusercounter == 1)
                                {
                                    // lbTop2HotEvCount.Text = pair.Value.ToString();
                                    // lbTop2HotEvUser.Text = pair.Key.ToString();
                                    hotTotal = hotTotal + pair.Value;
                                    hot2Count = pair.Value;
                                }
                                else if (hoteventusercounter == 2)
                                {
                                    // lbTop3HotEvCount.Text = pair.Value.ToString();
                                    // lbTop3HotEvUser.Text = pair.Key.ToString();
                                    hotTotal = hotTotal + pair.Value;
                                    hot3Count = pair.Value;
                                }
                                else if (hoteventusercounter == 3)
                                {
                                    //lbTop4HotEvCount.Text = pair.Value.ToString();
                                    //lbTop4HotEvUser.Text = pair.Key.ToString();
                                    hotTotal = hotTotal + pair.Value;
                                    hot4Count = pair.Value;
                                }
                                else if (hoteventusercounter == 4)
                                {
                                    //lbTop5HotEvCount.Text = pair.Value.ToString();
                                    //lbTop5HotEvUser.Text = pair.Key.ToString();
                                    hotTotal = hotTotal + pair.Value;
                                    hot5Count = pair.Value;
                                    break;
                                }
                                hoteventusercounter++;
                            }
                        }
                        //var zhot1Percent = "0%";
                        //var zhot2Percent = "0%";
                        //var zhot3Percent = "0%";
                        //var zhot4Percent = "0%";
                        //var zhot5Percent = "0%";

                        //if (hot1Count > 0)
                        //{
                        //    zhot1Percent = ((int)Math.Round((float)hot1Count / (float)hotTotal * (float)100)).ToString() + "%";
 
                        //} 
                        //if (hot2Count > 0)
                        //{
                        //    zhot2Percent = ((int)Math.Round((float)hot2Count / (float)hotTotal * (float)100)).ToString() + "%";
                         
                        //} 
                        //if (hot3Count > 0)
                        //{
                        //    zhot3Percent = ((int)Math.Round((float)hot3Count / (float)hotTotal * (float)100)).ToString() + "%";
         
                        //} 
                        //if (hot4Count > 0)
                        //{
                        //    zhot4Percent = ((int)Math.Round((float)hot4Count / (float)hotTotal * (float)100)).ToString() + "%";
                    
                        //} 
                        //if (hot5Count > 0)
                        //{
                        //    zhot5Percent = ((int)Math.Round((float)hot5Count / (float)hotTotal * (float)100)).ToString() + "%";
 
                        //} 
                        //inprogressCount = total - (pendingCount + completedCount);
                        var pendingPercentage = (int)Math.Round((float)pendingCount / (float)total * (float)100);
                        var inprogressPercentage = (int)Math.Round((float)inprogressCount / (float)total * (float)100);
                        var completedPercentage = (int)Math.Round((float)completedCount / (float)total * (float)100);
                        var resolvedPercentage = (int)Math.Round((float)resolvedIncidents.Count / (float)total * (float)100);

                        var zresolvedPercent = "0";
                        var zlbresolvedPercent = "0";

                        if (resolvedIncidents.Count > 0)
                        {
                            zresolvedPercent = resolvedPercentage.ToString();
                            zlbresolvedPercent = resolvedIncidents.Count.ToString();
                        } 
                        var newCount = inprogressCount + completedCount + resolvedIncidents.Count;

                        //if (newCount > 0)
                        //    HandledPercentage = ((int)Math.Round((float)newCount / (float)total * (float)100)).ToString();
                        //else
                        //    HandledPercentage = "0";

                        //lbHandledAlarms.Text = newCount.ToString();//(total - pendingCount).ToString();

                        var zlbHandledAlarms = "0";

                        zlbHandledAlarms = newCount.ToString();
                        var zpendingPercent = "0";
                        var zlbPending = "0";
                        var zlbPendingpercent = "0";
                        var zlbPendingAlarms = "0";

                        var zlbInprogress = "0";
                        var zlbInprogresspercent = "0";
                        var zinprogressPercent = "0";

                        var zcompletedPercent = "0";
                        var zlbCompletedpercent = "0";
                        var zlbCompleted = "0";

                        if (pendingCount > 0)
                        {
                            zpendingPercent = pendingPercentage.ToString();
                            //lbPendingAlarms.Text = pendingCount.ToString();
                            zlbPending = pendingCount.ToString();
                            zlbPendingpercent = pendingPercentage.ToString();
                        } 
                        if (inprogressCount > 0)
                        {
                            zinprogressPercent = inprogressPercentage.ToString();
                            zlbInprogresspercent = inprogressPercentage.ToString();
                            zlbInprogress = inprogressCount.ToString();
                        }
                        if (completedCount > 0)
                        {
                            zcompletedPercent = completedPercentage.ToString();
                            zlbCompletedpercent = completedPercentage.ToString();
                            zlbCompleted = completedCount.ToString();
                        }

                        var zlbTotalAlarms = "0";
                        zlbTotalAlarms = total.ToString();

                        listy.Add(zlbresolvedPercent);
                        listy.Add(zresolvedPercent);

                        listy.Add(zlbCompleted);
                        listy.Add(zlbCompletedpercent);

                        listy.Add(zlbInprogress);
                        listy.Add(zlbInprogresspercent);

                        listy.Add(zlbPending);
                        listy.Add(zlbPendingpercent);

                        listy.Add(zlbTotalAlarms);

                        var firstDayOfMonth = new DateTime(fromDt.Year, fromDt.Month, 1);
                        var zday1 = firstDayOfMonth.ToString("yyyy-MM-dd");
                        var zday2 = firstDayOfMonth.AddDays(1).ToString("yyyy-MM-dd");
                        var zday3 = firstDayOfMonth.AddDays(2).ToString("yyyy-MM-dd");
                        var zday4 = firstDayOfMonth.AddDays(3).ToString("yyyy-MM-dd");
                        var zday5 = firstDayOfMonth.AddDays(4).ToString("yyyy-MM-dd");
                        var zday6 = firstDayOfMonth.AddDays(5).ToString("yyyy-MM-dd");
                        var zday7 = firstDayOfMonth.AddDays(6).ToString("yyyy-MM-dd");

                        listy.Add(zday1); 
                        listy.Add(zday2);
                        listy.Add(zday3); 
                        listy.Add(zday4);
                        listy.Add(zday5); 
                        listy.Add(zday6);
                        listy.Add(zday7);

                        listy.Add(zticketday1week1.ToString());
                        listy.Add(zticketday1week2.ToString());
                        listy.Add(zticketday1week3.ToString());
                        listy.Add(zticketday1week4.ToString());

                        listy.Add(zticketday2week1.ToString());
                        listy.Add(zticketday2week2.ToString());
                        listy.Add(zticketday2week3.ToString());
                        listy.Add(zticketday2week4.ToString());

                        listy.Add(zticketday3week1.ToString());
                        listy.Add(zticketday3week2.ToString());
                        listy.Add(zticketday3week3.ToString());
                        listy.Add(zticketday3week4.ToString());

                        listy.Add(zticketday4week1.ToString());
                        listy.Add(zticketday4week2.ToString());
                        listy.Add(zticketday4week3.ToString());
                        listy.Add(zticketday4week4.ToString());
                                            
                        listy.Add(zticketday5week1.ToString());
                        listy.Add(zticketday5week2.ToString());
                        listy.Add(zticketday5week3.ToString());
                        listy.Add(zticketday5week4.ToString());

                        listy.Add(zticketday6week1.ToString());
                        listy.Add(zticketday6week2.ToString());
                        listy.Add(zticketday6week3.ToString());
                        listy.Add(zticketday6week4.ToString());

                        listy.Add(zticketday7week1.ToString());
                        listy.Add(zticketday7week2.ToString());
                        listy.Add(zticketday7week3.ToString());
                        listy.Add(zticketday7week4.ToString());

                                            
                        var tenthDayOfMonth = firstDayOfMonth.Add(new TimeSpan(6, 0, 0, 0));
                        var fifteenDayOfMonth = tenthDayOfMonth.Add(new TimeSpan(7, 0, 0, 0));
                        var twentyDayOfMonth = fifteenDayOfMonth.Add(new TimeSpan(7, 0, 0, 0));
                        var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);

                        listy.Add(firstDayOfMonth.ToShortDateString());
                        listy.Add(tenthDayOfMonth.ToShortDateString());
                        listy.Add(fifteenDayOfMonth.ToShortDateString());
                        listy.Add(twentyDayOfMonth.ToShortDateString());
                        listy.Add(lastDayOfMonth.ToShortDateString());

                        var znotificationCount1 = 0;
                        var znotificationCount2 = 0;
                        var znotificationCount3 = 0;
                        var znotificationCount4 = 0;
                        var notifications = new List<Notification>();

                        if (userinfo.RoleId == (int)Role.Manager)
                        {
                            notifications = Notification.GetAllNotificationsByCreator(userinfo.Username, dbConnection);
                        }
                        else if (userinfo.RoleId == (int)Role.Admin)
                        {

                            notifications = Notification.GetAllNotificationsByCreator(userinfo.Username, dbConnection);
                            var sessions = DirectorManager.GetAllFullManagersByDirectorId(userinfo.ID, dbConnection);
                            foreach (var usr in sessions)
                            {
                                notifications.AddRange(Notification.GetAllNotificationsByCreator(usr.Username, dbConnection));
                            }

                        }
                        else if (userinfo.RoleId == (int)Role.Director)
                        {
                            notifications = Notification.GetAllNotificationsBySiteId(userinfo.SiteId, dbConnection);
                            notifications = notifications.Where(i => i.SiteId == userinfo.SiteId).ToList();
                        }
                        else if (userinfo.RoleId == (int)Role.Regional)
                        {
                            var notis = Notification.GetAllNotificationsByLevel5(userinfo.ID, dbConnection);

                            notifications.AddRange(notis);
                        }
                        else if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                            notifications = Notification.GetAllNotifications(dbConnection);

                        notifications = notifications.Where(i => (i.CreatedDate.Value.Month == toDt.Month && i.CreatedDate.Value.Year == toDt.Year) || (i.CreatedDate.Value.Month == fromDt.Month && i.CreatedDate.Value.Year == fromDt.Year)).ToList();

                        notifications = notifications.Where(i => i.CreatedDate.Value.AddHours(userinfo.TimeZone).Date <= toDt.Date && i.CreatedDate.Value.AddHours(userinfo.TimeZone).Date >= fromDt.Date).ToList();
 
                        var groupednotification = notifications.GroupBy(item => item.Id);
                        notifications = groupednotification.Select(grp => grp.OrderBy(item => item.Id).First()).ToList();

                        foreach (var noti in notifications)
                        {
                            if (noti.CreatedDate.Value.Date >= firstDayOfMonth.Date && noti.CreatedDate < tenthDayOfMonth.Date)
                            {
                                znotificationCount1++;
                            }
                            else if (noti.CreatedDate.Value.Date >= tenthDayOfMonth.Date && noti.CreatedDate < fifteenDayOfMonth.Date)
                            {
                                znotificationCount2++;
                            }
                            else if (noti.CreatedDate.Value.Date >= fifteenDayOfMonth.Date && noti.CreatedDate < twentyDayOfMonth.Date)
                            {
                                znotificationCount3++;
                            }
                            else if (noti.CreatedDate.Value.Date >= twentyDayOfMonth.Date && noti.CreatedDate < lastDayOfMonth.Date)
                            {
                                znotificationCount4++;
                            }
                        } 

                        listy.Add(znotificationCount1.ToString());
                        listy.Add(znotificationCount2.ToString());
                        listy.Add(znotificationCount3.ToString());
                        listy.Add(znotificationCount4.ToString());

                        listy.Add(zTop1Tic);
                        listy.Add(zTop2Tic);
                        listy.Add(zTop3Tic);
                        listy.Add(zTop4Tic);
                        listy.Add(zTop5Tic);
                                            

                        listy.Add(zTop1TicName);
                        listy.Add(zTop2TicName);
                        listy.Add(zTop3TicName);
                        listy.Add(zTop4TicName);
                        listy.Add(zTop5TicName);

                        listy.Add(zTop1TicCount);
                        listy.Add(zTop2TicCount);
                        listy.Add(zTop3TicCount);
                        listy.Add(zTop4TicCount);
                        listy.Add(zTop5TicCount);
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("OthersDash", "getEventStatusTotal", ex, dbConnection, userinfo.SiteId);
                listy.Clear();
                listy.Add("ERROR");
            }
            return listy;
        }

        public void getOthersInfo(Users userinfo)
        {
            try
            {
                var firstDayOfMonth = new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 1);
                var tenthDayOfMonth = firstDayOfMonth.Add(new TimeSpan(6, 0, 0, 0));
                var fifteenDayOfMonth = tenthDayOfMonth.Add(new TimeSpan(7, 0, 0, 0));
                var twentyDayOfMonth = fifteenDayOfMonth.Add(new TimeSpan(7, 0, 0, 0));
                var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);

                var notifications = new List<Notification>();

                if (userinfo.RoleId == (int)Role.Manager)
                {
                    notifications = Notification.GetAllNotificationsByCreator(userinfo.Username, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.Admin)
                {

                    notifications = Notification.GetAllNotificationsByCreator(userinfo.Username, dbConnection);
                    var sessions = DirectorManager.GetAllFullManagersByDirectorId(userinfo.ID, dbConnection);
                    foreach (var usr in sessions)
                    {
                        notifications.AddRange(Notification.GetAllNotificationsByCreator(usr.Username, dbConnection));
                    }

                }
                else if (userinfo.RoleId == (int)Role.Director)
                {
                    notifications = Notification.GetAllNotificationsBySiteId(userinfo.SiteId, dbConnection);
                    notifications = notifications.Where(i => i.SiteId == userinfo.SiteId).ToList();
                }
                else if (userinfo.RoleId == (int)Role.Regional)
                { 
                        var notis = Notification.GetAllNotificationsByLevel5(userinfo.ID, dbConnection);
 
                        notifications.AddRange(notis); 
                }
                else if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                    notifications = Notification.GetAllNotifications(dbConnection);

                var grouped = notifications.GroupBy(item => item.Id);
                notifications = grouped.Select(grp => grp.OrderBy(item => item.Id).First()).ToList();
                
                foreach (var noti in notifications)
                {
                    if (noti.CreatedDate.Value.Date >= firstDayOfMonth.Date && noti.CreatedDate < tenthDayOfMonth.Date)
                    {
                        notificationCount1++;
                    }
                    else if (noti.CreatedDate.Value.Date >= tenthDayOfMonth.Date && noti.CreatedDate < fifteenDayOfMonth.Date)
                    {
                        notificationCount2++;
                    }
                    else if (noti.CreatedDate.Value.Date >= fifteenDayOfMonth.Date && noti.CreatedDate < twentyDayOfMonth.Date)
                    {
                        notificationCount3++;
                    }
                    else if (noti.CreatedDate.Value.Date >= twentyDayOfMonth.Date && noti.CreatedDate < lastDayOfMonth.Date)
                    {
                        notificationCount4++;
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("OthersDash.aspx", "getOthersInfo", ex, dbConnection, userinfo.SiteId);
            }
        }

        [WebMethod]
        public static List<string> getActivityReportData(int id,string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var sessions = new List<CustomEvent>();
                var hotEvent6 = 0;
                var hotEvent12 = 0;
                var hotEvent18 = 0;
                var hotEvent24 = 0;
                var mobEvent6 = 0;
                var mobEvent12 = 0;
                var mobEvent18 = 0;
                var mobEvent24 = 0;
                if (userinfo != null)
                {
                    if (userinfo.RoleId != (int)Role.SuperAdmin)
                    {
                        sessions = CustomEvent.GetAllCustomEventByDateAndManagerIdUNHandledBySiteId(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(1, 0, 0)), userinfo.ID,userinfo.SiteId ,dbConnection);
                    }
                    else
                    {
                        var session = CustomEvent.GetAllCustomEventByDateUNHandled(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(24, 0, 0)), dbConnection);
                        foreach (var tem in session)
                        {
                            if (tem.EventType == CustomEvent.EventTypes.HotEvent)
                            {
                                if ((CommonUtility.getDTNow().Subtract(new TimeSpan(6, 0, 0)) < tem.RecevieTime) && (tem.RecevieTime < CommonUtility.getDTNow()))
                                {
                                    hotEvent6++;
                                }
                                if ((CommonUtility.getDTNow().Subtract(new TimeSpan(12, 0, 0)) < tem.RecevieTime) && (tem.RecevieTime < CommonUtility.getDTNow()) && (CommonUtility.getDTNow().Subtract(new TimeSpan(6, 0, 0)) > tem.RecevieTime))
                                {
                                    hotEvent12++;
                                }
                                if ((CommonUtility.getDTNow().Subtract(new TimeSpan(18, 0, 0)) < tem.RecevieTime) && (tem.RecevieTime < CommonUtility.getDTNow()) && (CommonUtility.getDTNow().Subtract(new TimeSpan(12, 0, 0)) > tem.RecevieTime))
                                {
                                    hotEvent18++;
                                }
                                if ((CommonUtility.getDTNow().Subtract(new TimeSpan(24, 0, 0)) < tem.RecevieTime) && (tem.RecevieTime < CommonUtility.getDTNow()) && (CommonUtility.getDTNow().Subtract(new TimeSpan(18, 0, 0)) > tem.RecevieTime))
                                {
                                    hotEvent24++;
                                }

                            }
                            else if (tem.EventType == CustomEvent.EventTypes.MobileHotEvent)
                            {
                                if ((CommonUtility.getDTNow().Subtract(new TimeSpan(6, 0, 0)) < tem.RecevieTime) && (tem.RecevieTime < CommonUtility.getDTNow()))
                                {
                                    mobEvent6++;
                                }
                                if ((CommonUtility.getDTNow().Subtract(new TimeSpan(12, 0, 0)) < tem.RecevieTime) && (tem.RecevieTime < CommonUtility.getDTNow()) && (CommonUtility.getDTNow().Subtract(new TimeSpan(6, 0, 0)) > tem.RecevieTime))
                                {
                                    mobEvent12++;
                                }
                                if ((CommonUtility.getDTNow().Subtract(new TimeSpan(18, 0, 0)) < tem.RecevieTime) && (tem.RecevieTime < CommonUtility.getDTNow()) && (CommonUtility.getDTNow().Subtract(new TimeSpan(12, 0, 0)) > tem.RecevieTime))
                                {
                                    mobEvent18++;
                                }
                                if ((CommonUtility.getDTNow().Subtract(new TimeSpan(24, 0, 0)) < tem.RecevieTime) && (tem.RecevieTime < CommonUtility.getDTNow()) && (CommonUtility.getDTNow().Subtract(new TimeSpan(18, 0, 0)) > tem.RecevieTime))
                                {
                                    mobEvent24++;
                                }

                            }
                        }
                    }
                    listy.Add(mobEvent24.ToString());
                    listy.Add(hotEvent24.ToString());

                    listy.Add(mobEvent18.ToString());
                    listy.Add(hotEvent18.ToString());

                    listy.Add(mobEvent12.ToString());
                    listy.Add(hotEvent12.ToString());

                    listy.Add(mobEvent6.ToString());
                    listy.Add(hotEvent6.ToString());

                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("OthersDash.aspx", "getActivityReportData", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        
        [WebMethod]
        public static List<string> getOffenceChart(int id,string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var firstDayOfMonth = new DateTime(CommonUtility.getDTNow().Year, CommonUtility.getDTNow().Month, 1);
                var tenthDayOfMonth = firstDayOfMonth.Add(new TimeSpan(6, 0, 0, 0));
                var fifteenDayOfMonth = tenthDayOfMonth.Add(new TimeSpan(7, 0, 0, 0));
                var twentyDayOfMonth = fifteenDayOfMonth.Add(new TimeSpan(7, 0, 0, 0));
                var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                listy.Add(firstDayOfMonth.ToString("MMM dd, yyyy"));
                listy.Add(tenthDayOfMonth.ToString("MMM dd, yyyy"));
                listy.Add(fifteenDayOfMonth.ToString("MMM dd, yyyy"));
                listy.Add(twentyDayOfMonth.ToString("MMM dd, yyyy"));
                listy.Add(lastDayOfMonth.ToString("MMM dd, yyyy"));
                var offences = new List<CustomEvent>();
                if (userinfo.RoleId == (int)Role.Manager || userinfo.RoleId == (int)Role.Admin)
                {
                    offences = CustomEvent.GetAllCustomEventByDateAndManagerIdUNHandledBySiteId(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(30, 0, 0, 0)), userinfo.ID,userinfo.SiteId ,dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.Director)
                {
                    offences = CustomEvent.GetAllCustomEventByDateUNHandledAndSite(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(30, 0, 0, 0)),userinfo.SiteId ,dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                    //var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                    //foreach (var site in sites)
                    offences.AddRange(CustomEvent.GetAllCustomEventByDateUNHandledAndLevel5(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(30, 0, 0, 0)), userinfo.ID, dbConnection));
                }
                else if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                {
                    offences = CustomEvent.GetAllCustomEventByDateUNHandled(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(30, 0, 0, 0)), dbConnection);
                }
                var week1userList = new List<string>();
                var week2userList = new List<string>();
                var week3userList = new List<string>();
                var week4userList = new List<string>();
                foreach (var off in offences)
                {
                    if (off.EventType == CustomEvent.EventTypes.DriverOffence)
                    {
                        if (off.RecevieTime.Value.Ticks > firstDayOfMonth.Ticks && off.RecevieTime.Value.Ticks < tenthDayOfMonth.Ticks)
                        {
                            week1userList.Add(off.UserName);
                        }
                        else if (off.RecevieTime.Value.Ticks > tenthDayOfMonth.Ticks && off.RecevieTime.Value.Ticks < fifteenDayOfMonth.Ticks)
                        {
                            week2userList.Add(off.UserName);
                        }
                        else if (off.RecevieTime.Value.Ticks > fifteenDayOfMonth.Ticks && off.RecevieTime.Value.Ticks < twentyDayOfMonth.Ticks)
                        {
                            week3userList.Add(off.UserName);
                        }
                        else if (off.RecevieTime.Value.Ticks > twentyDayOfMonth.Ticks && off.RecevieTime.Value.Ticks < lastDayOfMonth.Ticks)
                        {
                            week4userList.Add(off.UserName);
                        }
                    }
                }
                Dictionary<string, int> week1counts = week1userList.GroupBy(x => x)
                          .ToDictionary(g => g.Key,
                                        g => g.Count());
                Dictionary<string, int> week2counts = week2userList.GroupBy(x => x)
                          .ToDictionary(g => g.Key,
                                        g => g.Count());
                Dictionary<string, int> week3counts = week3userList.GroupBy(x => x)
                          .ToDictionary(g => g.Key,
                                        g => g.Count());
                Dictionary<string, int> week4counts = week4userList.GroupBy(x => x)
                          .ToDictionary(g => g.Key,
                                        g => g.Count());

                var week1items = from pair in week1counts
                                 orderby pair.Value descending
                                 select pair;
                var week2items = from pair in week2counts
                                 orderby pair.Value descending
                                 select pair;
                var week3items = from pair in week3counts
                                 orderby pair.Value descending
                                 select pair;
                var week4items = from pair in week4counts
                                 orderby pair.Value descending
                                 select pair;
                int count = 0;
                foreach (KeyValuePair<string, int> pair in week1items)
                {
                    if (count < 5)
                    {
                        //listy.Add(pair.Key.ToString());
                        listy.Add(pair.Value.ToString());
                        count++;
                    }
                    else
                    {
                        break;
                    }
                }
                if (count < 5)
                {
                    var i = 0;
                    for (i = 5; i > count; count++)
                    {
                        //listy.Add("0");
                        listy.Add("0");
                    }
                }
                count = 0;
                foreach (KeyValuePair<string, int> pair in week2items)
                {
                    if (count < 5)
                    {
                        //listy.Add(pair.Key.ToString());
                        listy.Add(pair.Value.ToString());
                        count++;
                    }
                    else
                    {
                        break;
                    }
                }
                if (count < 5)
                {
                    var i = 0;
                    for (i = 5; i > count; count++)
                    {
                        //listy.Add("0");
                        listy.Add("0");
                    }
                }
                count = 0;
                foreach (KeyValuePair<string, int> pair in week3items)
                {
                    if (count < 5)
                    {
                        //listy.Add(pair.Key.ToString());
                        listy.Add(pair.Value.ToString());
                        count++;
                    }
                    else
                    {
                        break;
                    }
                }
                if (count < 5)
                {
                    var i = 0;
                    for (i = 5; i > count; count++)
                    {
                        //listy.Add("0");
                        listy.Add("0");
                    }
                }
                count = 0;
                foreach (KeyValuePair<string, int> pair in week4items)
                {
                    if (count < 5)
                    {
                        //listy.Add(pair.Key.ToString());
                        listy.Add(pair.Value.ToString());
                        count++;
                    }
                    else
                    {
                        break;
                    }
                }
                if (count < 5)
                {
                    var i = 0;
                    for (i = 5; i > count; count++)
                    {
                        //listy.Add("0");
                        listy.Add("0");
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("OthersDash.aspx", "getOffenceChart", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static string deleteReminder(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }
                Reminders.DeleteReminderById(id, dbConnection);
                SystemLogger.SaveSystemLog(dbConnectionAudit, "OthersDash", id.ToString(), id.ToString(), userinfo, "Delete Reminder" + id);
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("OthersDash.aspx", "deleteReminder", ex, dbConnection, userinfo.SiteId);
                return ex.Message;
            }
            return "SUCCESS";
        }
        [WebMethod]
        public static List<string> getReminders(int id,string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                //if(userinfo)
                //var getreminders = Reminders.GetAllReminders(dbConnection);
                var getreminders = Reminders.GetAllRemindersByDateAndCreator(CommonUtility.getDTNow().Date, userinfo.Username, dbConnection);
                //json = ;//'<time style="margin-left:15px">8:00 am, December 31, 2015</time><p style="margin-left:15px">Send new updates to Orry sasdasssssssss ssssssssssssss ssssssssssssssssss ssssssssssss</p><div style="margin-left:-5px" class="vertical-line green-border"></div>';
                var remindercount = 0;
                foreach (var rem in getreminders)
                {
                    if (remindercount == 6)
                        break;
                    //var convertdatetimeformat = rem.ReminderDate.ToString('')//8:00 am, December 31, 2015
                    //<time style='margin-left:15px'>" + rem.ReminderDate.ToString("MMM dd, yyyy") + "</time>
                    if (!rem.IsCompleted)
                    {
                        var colorcode = string.Empty;
                        var colorcode2 = string.Empty;
                        if (rem.ColorCode == (int)Reminders.ColorCodes.Green)
                        {
                            colorcode = "green-border";
                            colorcode2 = "green";
                        }
                        else if (rem.ColorCode == (int)Reminders.ColorCodes.Blue)
                        {
                            colorcode = "blue-border";
                            colorcode2 = "blue";
                        }
                        else if (rem.ColorCode == (int)Reminders.ColorCodes.Yellow)
                        {
                            colorcode = "yellow-border";
                            colorcode2 = "yellow";
                        }
                        else if (rem.ColorCode == (int)Reminders.ColorCodes.Red)
                        {
                            colorcode = "red-border";
                            colorcode2 = "red";
                        }

                        var date = rem.FromDate + "-" + rem.ToDate + " " + rem.ReminderDate.Value.ToString("dddd MMMM dd, yyyy");

                        var jsonstring = "<div onclick='reminderChoice(&apos;" + rem.ID + "&apos;,&apos;" + date + "&apos;,&apos;" + rem.ReminderNote + "&apos;,&apos;" + rem.ReminderTopic + "&apos;,&apos;" + colorcode2 + "&apos;)' href='#' data-target='#" + colorcode2 + "reminderboxs' data-toggle='modal'><time style='margin-left:15px'>" + rem.FromDate + "-" + rem.ToDate + "</time><p style='margin-left:15px'>" + rem.ReminderTopic + "</p><div style='margin-left:-5px' class='vertical-line " + colorcode + "'></div></div>";
                        listy.Add(jsonstring);
                        remindercount++;
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("OthersDash.aspx", "getReminders", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getRecentActivity(int id,string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var getrecenthistory = EventHistory.GetTopEventHistoryBySiteId(userinfo.SiteId,dbConnection);

            var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

            if (isValidSession == null)
            {
                listy.Add("LOGOUT");
                return listy;
            }

            foreach (var rem in getrecenthistory)
            {
                //var convertdatetimeformat = rem.ReminderDate.ToString('')//8:00 am, December 31, 2015
                var gethotevent = CustomEvent.GetCustomEventById(rem.EventId, dbConnection);
                
                if (gethotevent != null)
                {
                    if (gethotevent.EventType == CustomEvent.EventTypes.MobileHotEvent)
                    {
                        gethotevent.Name = gethotevent.EventTypeName;
                        //var mobEv = MobileHotEvent.GetMobileHotEventByGuid(gethotevent.Identifier, dbConnection);
                        //if (mobEv != null)
                        //{
                        //    if (string.IsNullOrEmpty(mobEv.EventTypeName))
                        //        gethotevent.Name = "None";
                        //    else
                        //        gethotevent.Name = mobEv.EventTypeName;

                        //}
                    }

                    var incidentType = gethotevent.Name;

                    var incidentLocation = string.Empty;

                    var actioninfo = string.Empty;
                    if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Pending)
                    {
                        incidentLocation = "from " + ReverseGeocode.RetrieveFormatedAddress(gethotevent.Latitude.ToString(), gethotevent.Longtitude.ToString(), getClientLic);

                        actioninfo = "created";

                        var recTimeMinusNow = CommonUtility.getDTNow().Subtract(rem.CreatedDate.Value.AddHours(userinfo.TimeZone));

                        var jsonstring = "<div class='col-md-2'><p>" + recTimeMinusNow.Minutes.ToString() + "M</p></div><div class='col-md-2'><span class='bullhorn-circle'><i class='fa fa-bullhorn'></i></span></div><div class='col-md-8'><p><span>" + rem.CreatedBy + "</span></br>" + actioninfo + "<span>" + incidentType + "</span></br>" + incidentLocation + "</p></div><div class='vertical-line'></div>";

                        listy.Add(jsonstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Complete)
                    {
                        actioninfo = "completed";

                        incidentLocation = "from " + ReverseGeocode.RetrieveFormatedAddress(rem.Latitude.ToString(), rem.Longtitude.ToString(), getClientLic);

                        var recTimeMinusNow = CommonUtility.getDTNow().Subtract(rem.CreatedDate.Value.AddHours(userinfo.TimeZone));

                        var jsonstring = "<div class='col-md-2'><p>" + recTimeMinusNow.Minutes.ToString() + "M</p></div><div class='col-md-2'><span class='bullhorn-circle'><i class='fa fa-bullhorn'></i></span></div><div class='col-md-8'><p><span>" + rem.UserName + "</span></br>" + actioninfo + "<span>" + incidentType + "</span>" + incidentLocation + "</p></div><div class='vertical-line'></div>";

                        listy.Add(jsonstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Dispatch)
                    {
                        actioninfo = "dispatched";

                        incidentLocation = "to " + rem.UserName;

                        var recTimeMinusNow = CommonUtility.getDTNow().Subtract(rem.CreatedDate.Value.AddHours(userinfo.TimeZone));

                        var jsonstring = "<div class='col-md-2'><p>" + recTimeMinusNow.Minutes.ToString() + "M</p></div><div class='col-md-2'><span class='bullhorn-circle'><i class='fa fa-bullhorn'></i></span></div><div class='col-md-8'><p><span>" + rem.CreatedBy + "</span></br>" + actioninfo + "<span>" + incidentType + "</span>" + incidentLocation + "</p></div><div class='vertical-line'></div>";

                        listy.Add(jsonstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Resolve)
                    {
                        actioninfo = "resolved";

                        var recTimeMinusNow = CommonUtility.getDTNow().Subtract(rem.CreatedDate.Value.AddHours(userinfo.TimeZone));

                        var jsonstring = "<div class='col-md-2'><p>" + recTimeMinusNow.Minutes.ToString() + "M</p></div><div class='col-md-2'><span class='bullhorn-circle'><i class='fa fa-bullhorn'></i></span></div><div class='col-md-8'><p><span>" + rem.CreatedBy + "</span></br>" + actioninfo + "<span>" + incidentType + "</span>" + incidentLocation + "</p></div><div class='vertical-line'></div>";

                        listy.Add(jsonstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Reject)
                    {
                        actioninfo = "rejected";

                        var recTimeMinusNow = CommonUtility.getDTNow().Subtract(rem.CreatedDate.Value.AddHours(userinfo.TimeZone));

                        var jsonstring = "<div class='col-md-2'><p>" + recTimeMinusNow.Minutes.ToString() + "M</p></div><div class='col-md-2'><span class='bullhorn-circle'><i class='fa fa-bullhorn'></i></span></div><div class='col-md-8'><p><span>" + rem.CreatedBy + "</span></br>" + actioninfo + "<span>" + incidentType + "</span>" + incidentLocation + "</p></div><div class='vertical-line'></div>";

                        listy.Add(jsonstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Park)
                    {
                        actioninfo = "parked";

                        var recTimeMinusNow = CommonUtility.getDTNow().Subtract(rem.CreatedDate.Value.AddHours(userinfo.TimeZone));

                        var jsonstring = "<div class='col-md-2'><p>" + recTimeMinusNow.Minutes.ToString() + "M</p></div><div class='col-md-2'><span class='bullhorn-circle'><i class='fa fa-bullhorn'></i></span></div><div class='col-md-8'><p><span>" + rem.CreatedBy + "</span></br>" + actioninfo + "<span>" + incidentType + "</span>" + incidentLocation + "</p></div><div class='vertical-line'></div>";

                        listy.Add(jsonstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Engage)
                    {
                        incidentLocation = "from " + ReverseGeocode.RetrieveFormatedAddress(rem.Latitude.ToString(), rem.Longtitude.ToString(), getClientLic);

                        actioninfo = "engaged";

                        var recTimeMinusNow = CommonUtility.getDTNow().Subtract(rem.CreatedDate.Value.AddHours(userinfo.TimeZone));

                        var jsonstring = "<div class='col-md-2'><p>" + recTimeMinusNow.Minutes.ToString() + "M</p></div><div class='col-md-2'><span class='bullhorn-circle'><i class='fa fa-bullhorn'></i></span></div><div class='col-md-8'><p><span>" + rem.UserName + "</span></br>" + actioninfo + "<span>" + incidentType + "</span></br>" + incidentLocation + "</p></div><div class='vertical-line'></div>";

                        listy.Add(jsonstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Release)
                    {
                        actioninfo = "released";

                        var recTimeMinusNow = CommonUtility.getDTNow().Subtract(rem.CreatedDate.Value.AddHours(userinfo.TimeZone));

                        var jsonstring = "<div class='col-md-2'><p>" + recTimeMinusNow.Minutes.ToString() + "M</p></div><div class='col-md-2'><span class='bullhorn-circle'><i class='fa fa-bullhorn'></i></span></div><div class='col-md-8'><p><span>" + rem.UserName + "</span></br>" + actioninfo + "<span>" + incidentType + "</span>" + incidentLocation + "</p></div><div class='vertical-line'></div>";

                        listy.Add(jsonstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Arrived)
                    {
                        actioninfo = "arrived ";

                        var recTimeMinusNow = CommonUtility.getDTNow().Subtract(rem.CreatedDate.Value.AddHours(userinfo.TimeZone));

                        var jsonstring = "<div class='col-md-2'><p>" + recTimeMinusNow.Minutes.ToString() + "M</p></div><div class='col-md-2'><span class='bullhorn-circle'><i class='fa fa-bullhorn'></i></span></div><div class='col-md-8'><p><span>" + rem.UserName + "</span></br>" + actioninfo + "<span>" + incidentType + "</span>" + incidentLocation + "</p></div><div class='vertical-line'></div>";

                        listy.Add(jsonstring);
                    }
                }
            }
            return listy;
        }

        [WebMethod]
        public static string getTimerDuration(int id)
        {
            var settings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
            return settings.MIMSMobileGPSInterval.ToString();
        }
        
        [WebMethod]
        public static string getGPSData(int id,string uname)
        {
            var json = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var cusEvents = new List<CustomEvent>();
            json += "[";

            var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

            if (isValidSession == null)
            {
                return "LOGOUT";
            }

            if (getClientLic != null)
            {
                if (getClientLic.isLocation)
                { 
                    if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                    {
                        cusEvents = CustomEvent.GetAllCustomEventByType((int)CustomEvent.EventTypes.DriverOffence, dbConnection);
                    }
                    else if (userinfo.RoleId == (int)Role.CustomerUser)
                    {
                        cusEvents = CustomEvent.GetAllCustomEventByTypeAndCId((int)CustomEvent.EventTypes.DriverOffence, userinfo.CustomerInfoId, dbConnection);
                        cusEvents = cusEvents.Where(i => i.UserName == userinfo.Username).ToList();
                    }
                    else
                    {
                        cusEvents = CustomEvent.GetAllCustomEventByTypeAndCId((int)CustomEvent.EventTypes.DriverOffence, userinfo.CustomerInfoId, dbConnection);
                    }


                    foreach (var item in cusEvents)
                    {
                        if (item.EventType == CustomEvent.EventTypes.DriverOffence)
                        {
                            var getDoFF = DriverOffence.GetDriverOffenceById(item.Identifier, dbConnection);
                            if (getDoFF != null)
                                json += "{ \"Username\" : \"" + getDoFF.OffenceCategory + "-" + item.CustomerIncidentId + "\",\"Id\" : \"" + item.EventId + "\",\"Long\" : \"" + item.Longtitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + item.RecevieTime.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"YELLOW\",\"Logs\" : \"INCIDENT\",\"DName\" : \"" + getDoFF.OffenceCategory + "-" + item.CustomerIncidentId + "\"},";
                        }
                    } 
                }
            }
            json = json.Substring(0, json.Length - 1);
            json += "]";
            return json;
        }

        [WebMethod]
        public static string getGPSDataTasks(int id,string uname)
        {
            var json = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

            if (isValidSession == null)
            {
                return "LOGOUT";
            }


            json += "[";
            var fullcollection = new List<UserTask>();

            if (userinfo.RoleId == (int)Role.SuperAdmin)
                fullcollection = UserTask.GetAllTaskByDateExcludingRecurringTaskParent(CommonUtility.getDTNow().Date, dbConnection);
            else
                fullcollection = UserTask.GetAllTasksOfTeamByManagerId(userinfo.ID, dbConnection);

            foreach (var item in fullcollection)
            {
                if (item.Status != (int)TaskStatus.Accepted && item.Status != (int)TaskStatus.Rejected && item.Status != (int)TaskStatus.Cancelled)
                {
                    if (item.Status == (int)TaskStatus.Pending)
                        json += "{ \"Username\" : \"" + item.Name + "-" + item.Id + "\",\"Id\" : \"" + item.Id + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + item.CreateDate + "\",\"State\" : \"PURPLE\",\"Logs\" : \"Task\"},";
                    else if (item.Status == (int)TaskStatus.InProgress)
                        json += "{ \"Username\" : \"" + item.Name + "-" + item.Id + "\",\"Id\" : \"" + item.Id + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + item.CreateDate + "\",\"State\" : \"YELLOW\",\"Logs\" : \"Task\"},";
                    else if (item.Status == (int)TaskStatus.Completed)
                        json += "{ \"Username\" : \"" + item.Name + "-" + item.Id + "\",\"Id\" : \"" + item.Id + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + item.CreateDate + "\",\"State\" : \"GREEN\",\"Logs\" : \"Task\"},";

                }
            }

            json = json.Substring(0, json.Length - 1);
            json += "]";
            return json;
        }

        [WebMethod]
        public static string getGPSDataOthers(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var cusEvents = new List<CustomEvent>();
            var json = string.Empty;
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                json += "[";

                if (userinfo.RoleId == (int)Role.Manager)
                {
                    cusEvents = CustomEvent.GetAllCustomEventsByManagerId(userinfo.ID, dbConnection);

                }
                else if (userinfo.RoleId == (int)Role.Admin)
                {
                    var managerusers = DirectorManager.GetAllManagersByDirectorId(userinfo.ID, dbConnection);
                    cusEvents.AddRange(CustomEvent.GetAllCustomEventsByManagerId(userinfo.ID, dbConnection));
                    foreach (var manguser in managerusers)
                    {
                        cusEvents.AddRange(CustomEvent.GetAllCustomEventsByManagerId(manguser, dbConnection));
                    }
                }
                else
                {
                    cusEvents = CustomEvent.GetAllCustomEvents(dbConnection);
                }
                foreach (var item in cusEvents)
                {
                    if (item.EventType == CustomEvent.EventTypes.DriverOffence)
                        json += "{ \"Username\" : \"" + item.Name + "-" + item.EventId + "\",\"Id\" : \"" + item.EventId + "\",\"Long\" : \"" + item.Longtitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + item.RecevieTime.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"YELLOW\",\"Logs\" : \"INCIDENT\"},";
                }
                var sessions = new List<Verifier>();

                if (userinfo.RoleId == (int)Role.SuperAdmin)
                    sessions = Verifier.GetAllVerifierNoBytesByRequest((int)Verifier.RequestTypes.Verify, dbConnection);
                else
                    sessions = Verifier.GetAllVerifierNoBytesByManagerIdAndRequest(userinfo.ID, (int)Verifier.RequestTypes.Verify, dbConnection);

                foreach (var item in sessions)
                {
                    json += "{ \"Username\" : \"" + item.TypeName + "-" + item.Id + "\",\"Id\" : \"" + item.Id + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + item.CreatedDate + "\",\"State\" : \"YELLOW\",\"Logs\" : \"Verify\"},";

                }
                json = json.Substring(0, json.Length - 1);
                json += "]";
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("OthersDash.aspx", "getGPSDataOthers", ex, dbConnection, userinfo.SiteId);
            }
            return json;
        }

        

        [WebMethod]
        public static List<string> getusershealtcheck(int id, int onlinecount, int offlinecount, int idlecount, string uname)
        {
            var healthtotal = onlinecount + offlinecount + idlecount;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var lp15 = (int)Math.Round((float)onlinecount / (float)healthtotal * (float)100);
                var lp30 = (int)Math.Round((float)offlinecount / (float)healthtotal * (float)100);
                var lp60 = (int)Math.Round((float)idlecount / (float)healthtotal * (float)100);
                if (lp15 <= 1)
                    listy.Add("1%");
                else
                    listy.Add(lp15.ToString() + "%");

                if (lp30 <= 1)
                    listy.Add("1%");
                else
                    listy.Add(lp30.ToString() + "%");

                if (lp60 <= 1)
                    listy.Add("1%");
                else
                    listy.Add(lp60.ToString() + "%");

            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("OthersDash.aspx", "getusershealtcheck", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static float getTimer(int id)
        {
            var settings = ConfigSettings.GetConfigSettings(dbConnection);
            return settings.MIMSMobileGPSInterval;
        }

        [WebMethod]
        public static List<string> getTop5Values(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var hotAlarm = new List<CustomEvent>();
                                 if (userinfo.RoleId == (int)Role.Manager || userinfo.RoleId == (int)Role.Admin)
                {
                    hotAlarm = CustomEvent.GetAllCustomEventByDateAndManagerIdUNHandledBySiteId(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(30, 0, 0, 0)), userinfo.ID,userinfo.SiteId ,dbConnection);
                }
                else
                {
                    hotAlarm = CustomEvent.GetAllCustomEventByDateUNHandled(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(30, 0, 0, 0)), dbConnection);
                }

                var userList = new List<string>();
                foreach (var hot in hotAlarm)
                {
                    if (hot.EventType == CustomEvent.EventTypes.HotEvent || hot.EventType == CustomEvent.EventTypes.MobileHotEvent || hot.EventType == CustomEvent.EventTypes.Request)
                    {

                        userList.Add(hot.UserName);

                    }
                }
                Dictionary<string, int> counts = userList.GroupBy(x => x)
                                      .ToDictionary(g => g.Key,
                                                    g => g.Count());
                var items = from pair in counts
                            orderby pair.Value descending
                            select pair;
                int count = 0;
                foreach (KeyValuePair<string, int> pair in items)
                {

                    if (count < 5)
                    {
                        listy.Add(pair.Key.ToString());
                        listy.Add(pair.Value.ToString());
                        count++;
                    }
                    else
                    {
                        break;
                    }

                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("OthersDash.aspx", "getTop5Values", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static List<string> getTop5OffenceValues(int id,string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var prevDate = CommonUtility.getDTNow().Subtract(new TimeSpan(31, 0, 0, 0));
                var todayDate = CommonUtility.getDTNow();
                listy.Add(prevDate.Year.ToString() + "-" + prevDate.Month.ToString() + "-" + prevDate.Day.ToString());
                listy.Add(todayDate.Year.ToString() + "-" + todayDate.Month.ToString() + "-" + todayDate.Day.ToString());
                listy.Add(prevDate.Year.ToString() + "-" + prevDate.Month.ToString());
                var offences = new List<CustomEvent>();
                if (userinfo.RoleId == (int)Role.Manager || userinfo.RoleId == (int)Role.Admin)
                {
                    offences = CustomEvent.GetAllCustomEventByDateAndManagerIdUNHandledBySiteId(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(30, 0, 0, 0)), userinfo.ID,userinfo.SiteId ,dbConnection);
                }
                else
                {
                    offences = CustomEvent.GetAllCustomEventByDateUNHandled(CommonUtility.getDTNow(), CommonUtility.getDTNow().Subtract(new TimeSpan(30, 0, 0, 0)), dbConnection);
                }
                var userList = new List<string>();
                foreach (var off in offences)
                {
                    if (off.EventType == CustomEvent.EventTypes.DriverOffence)
                    {

                        userList.Add(off.UserName);

                    }
                }
                Dictionary<string, int> counts = userList.GroupBy(x => x)
                                      .ToDictionary(g => g.Key,
                                                    g => g.Count());
                var items = from pair in counts
                            orderby pair.Value descending
                            select pair;
                int count = 0;
                foreach (KeyValuePair<string, int> pair in items)
                {
                    if (count < 5)
                    {
                        listy.Add(pair.Key.ToString());
                        listy.Add(pair.Value.ToString());
                        count++;
                    }
                    else
                    {
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("OthersDash.aspx", "getTop5OffenceValues", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getTableData(int id,string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);

            var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

            if (isValidSession == null)
            {
                listy.Add("LOGOUT");
                return listy;
            }

            var allcustomEvents = new List<CustomEvent>();

            if (userinfo.RoleId == (int)Role.Manager)
            {
                allcustomEvents = CustomEvent.GetAllCustomEventsByManagerId(userinfo.ID, dbConnection);

            }
            else if (userinfo.RoleId == (int)Role.Admin)
            {
                var managerusers = DirectorManager.GetAllManagersByDirectorId(userinfo.ID, dbConnection);
                allcustomEvents.AddRange(CustomEvent.GetAllCustomEventsByManagerId(userinfo.ID, dbConnection));
                foreach (var manguser in managerusers)
                {
                    allcustomEvents.AddRange(CustomEvent.GetAllCustomEventsByManagerId(manguser, dbConnection));
                }
            }
            else
            {
                allcustomEvents = CustomEvent.GetAllCustomEvents(dbConnection);
            }


            var imageclass = string.Empty;
            foreach(var item in allcustomEvents)
            {
                if (item.EventType != CustomEvent.EventTypes.DriverOffence)
                {
                    if (item.EventType == CustomEvent.EventTypes.MobileHotEvent)
                    {
                        item.Name = item.EventTypeName;
                        //var mobEv = MobileHotEvent.GetMobileHotEventByGuid(item.Identifier, dbConnection);
                        //if (string.IsNullOrEmpty(mobEv.EventTypeName))
                        //    item.Name = "None";
                        //else
                        //    item.Name = mobEv.EventTypeName;
                        //imageclass = "circle-point-red";
                    }
                    else if (item.EventType == CustomEvent.EventTypes.HotEvent || item.EventType == CustomEvent.EventTypes.Request)
                    {
                        //var reqEv = HotEvent.GetHotEventById(item.Identifier, dbConnection);
                        //if (reqEv != null)
                        //{
                        //    if (CommonUtility.getPCName(reqEv) != "MIMS MOBILE")
                        //        item.UserName = CommonUtility.getPCName(reqEv);
                        //}
                    }
                    if (item.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Pending)
                        imageclass = "circle-point-red";
                    else if (item.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Complete)
                        imageclass = "circle-point-green";
                    else
                        imageclass = "circle-point-yellow";

                    var returnstring = "<tr role='row' class='odd'><td><span class='circle-point-container'><span class='circle-point " + imageclass + "'></span></span></td><td class='sorting_1'>" + item.StatusName + "</td><td>" + item.Name + "</td><td>" + item.RecevieTime.Value.AddHours(userinfo.TimeZone).ToString() + "</td><td>" + item.CustomerUName + "</td><td><a href='#' data-target='#ticketingViewCard'  data-toggle='modal' onclick='rowchoice(&apos;" + item.EventId + "&apos;)'><i class='fa fa-eye mr-1x'></i>View</a></td></tr>";
                    listy.Add(returnstring);
                }
            }
            return listy;
        }
        [WebMethod]
        public static string getIncidentLocationData(int id, string uname)
        {
            var json = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            json += "[";
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                if (getClientLic != null)
                {
                    if (getClientLic.isLocation)
                    {
                        var cusEv = CustomEvent.GetCustomEventById(id, dbConnection);
                        if (cusEv != null)
                        {
                            var item = DriverOffence.GetDriverOffenceById(cusEv.Identifier, dbConnection);

                            json += "{ \"Username\" : \"Ticket\",\"Id\" : \"" + item.Identifier.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                            json = json.Substring(0, json.Length - 1);

                        }
                    }
                }
                json += "]";
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Ticketing", "getIncidentLocationData", err, dbConnection, userinfo.SiteId);
            }
            return json;
        }
        [WebMethod]
        public static string getGPSDataUsers(int id, string uname)
        {
            var json = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                var users = new List<Users>();
                var devices = new List<Arrowlabs.Business.Layer.HealthCheck>();
                var vehs = new List<ClientManager>();
                if (getClientLic != null)
                {
                    if (getClientLic.isLocation)
                    {
                        if (userinfo.RoleId == (int)Role.Manager)
                        {
                            users = Users.GetAllFullUsersByManagerId(userinfo.ID, dbConnection);
                        }
                        else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                        {
                            users = Users.GetAllUsersByCustomerId(userinfo.CustomerInfoId, dbConnection);
                            users = users.Where(i => i.RoleId != (int)Role.CustomerSuperadmin).ToList();
                        }
                        else if (userinfo.RoleId == (int)Role.Admin)
                        {
                            var sessions = DirectorManager.GetAllFullManagersByDirectorId(userinfo.ID, dbConnection);
                            foreach (var usr in sessions)
                            {
                                users.Add(usr);
                                var getallUsers = Users.GetAllFullUsersByManagerIdForDirector(usr.ID, dbConnection);
                                foreach (var subsubuser in getallUsers)
                                {
                                    users.Add(subsubuser);
                                }
                            }
                            var unassigned = Users.GetAllUnassignedOperators(dbConnection);
                            unassigned = unassigned.Where(i => i.SiteId == (int)userinfo.SiteId).ToList();
                            foreach (var subsubuser in unassigned)
                            {
                                users.Add(subsubuser);
                            } 
                        }
                        else if (userinfo.RoleId == (int)Role.Director)
                        {
                            users = Users.GetAllUsersBySiteId(userinfo.SiteId, dbConnection);
                        }
                        else if (userinfo.RoleId == (int)Role.Regional)
                        {
                           // var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                           // foreach (var site in sites)
                            users.AddRange(Users.GetAllUsersByLevel5(userinfo.ID, dbConnection));

                        }
                        else if (userinfo.RoleId == (int)Role.ChiefOfficer)
                        {
                            users = Users.GetAllUsers(dbConnection);
                            users = users.Where(i => i.RoleId != (int)Role.ChiefOfficer).ToList();
                        }
                        else if (userinfo.RoleId == (int)Role.SuperAdmin)
                        {
                            users = Users.GetAllUsers(dbConnection);
                            devices = Arrowlabs.Business.Layer.HealthCheck.GetAllDevicesHealthCheck(dbConnection);
                        }
                    }
                }
                json += "[";


                var grouped = users.GroupBy(item => item.ID);
                users = grouped.Select(grp => grp.OrderBy(item => item.ID).First()).ToList();

                foreach (var item in users)
                {
                    if (item.Username != userinfo.Username)
                    {
                        var newLoginDate = item.LastLogin.Value.AddHours(userinfo.TimeZone).ToString("d MMM  hh:mm tt");
                        if (item.Active == (int)Arrowlabs.Business.Layer.HealthCheck.DeviceStatus.Online)
                        {
                            if (item.RoleId == (int)Role.Manager || item.RoleId == (int)Role.Admin)
                            {
                               // var pushDev = PushNotificationDevice.GetPushNotificationDeviceByUsername(item.Username, dbConnection);
                               // if (pushDev != null)
                              //  {
                                 //   if (!string.IsNullOrEmpty(pushDev.Username))
                                //    {
                                        if (item.IsServerPortal)
                                        {
                                            json += "{ \"Username\" : \"" + item.Username + "\",\"Id\" : \"" + item.ID.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + newLoginDate + "\",\"State\" : \"OFFUSER\",\"Logs\" : \"User\"},";
                                        }
                                        else
                                        {
                                            json += "{ \"Username\" : \"" + item.Username + "\",\"Id\" : \"" + item.ID.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + newLoginDate + "\",\"State\" : \"GREEN\",\"Logs\" : \"User\"},";
                                        }
                                 //   }
                                //    else
                                //    {
                                 //       json += "{ \"Username\" : \"" + item.Username + "\",\"Id\" : \"" + item.ID.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + newLoginDate + "\",\"State\" : \"GREEN\",\"Logs\" : \"User\"},";
                                //    }
                               // }
                               // else
                              //  {
                               //     json += "{ \"Username\" : \"" + item.Username + "\",\"Id\" : \"" + item.ID.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + newLoginDate + "\",\"State\" : \"GREEN\",\"Logs\" : \"User\"},";
                               // }
                            }
                            else
                            {
                                json += "{ \"Username\" : \"" + item.Username + "\",\"Id\" : \"" + item.ID.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + newLoginDate + "\",\"State\" : \"GREEN\",\"Logs\" : \"User\"},";
                            }
                        }
                        else if (item.Active == (int)Arrowlabs.Business.Layer.HealthCheck.DeviceStatus.Error)
                        {
                            json += "{ \"Username\" : \"" + item.Username + "\",\"Id\" : \"" + item.ID.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + newLoginDate + "\",\"State\" : \"YELLOW\",\"Logs\" : \"User\"},";
                        }
                        else if (item.Active == (int)Arrowlabs.Business.Layer.HealthCheck.DeviceStatus.Offline)
                        {
                            json += "{ \"Username\" : \"" + item.Username + "\",\"Id\" : \"" + item.ID.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + newLoginDate + "\",\"State\" : \"OFFUSER\",\"Logs\" : \"User\"},";
                        }
                    }
                }
                foreach (var dev in devices)
                {
                    var camString = "";
                    var camList = ClientCamera.GetAllClientCamerasByMacAddress(dev.MacAddress, dbConnection);
                    var camCounter = 0;

                    foreach (var cam in camList)
                    {
                        camString += "CameraName" + camCounter + "=" + cam.CameraName + "&";
                        camCounter++;
                    }
                    if (camCounter == 0)
                        camString = "NO CAM";

                    if (dev.Status == 1)
                    {
                        var newDev = Device.GetClientDeviceByMacAddress(dev.MacAddress, dbConnection);
                        //var LoginDate = dev.LastLoginDate.ToString("d MMM  hh:mm tt");
                        json += "{ \"Username\" : \"" + dev.PCName + "\",\"Id\" : \"" + dev.PCName + "\",\"Long\" : \"" + newDev.Longitude.ToString() + "\",\"Lat\" : \"" + newDev.Latitude.ToString() + "\",\"LastLog\" : \"" + dev.LastLoginDate + "\",\"State\" : \"BLUE\",\"Logs\" : \"User\",\"Cams\" : \"" + camString + "\"},";
                    }
                    else if (dev.Status == 0)
                    {
                        var newDev = Device.GetClientDeviceByMacAddress(dev.MacAddress, dbConnection);
                        //var LoginDate = dev.LastLoginDate.ToString("d MMM  hh:mm tt");
                        json += "{ \"Username\" : \"" + dev.PCName + "\",\"Id\" : \"" + dev.PCName + "\",\"Long\" : \"" + newDev.Longitude.ToString() + "\",\"Lat\" : \"" + newDev.Latitude.ToString() + "\",\"LastLog\" : \"" + dev.LastLoginDate + "\",\"State\" : \"OFFCLIENT\",\"Logs\" : \"User\",\"Cams\" : \"" + camString + "\"},";

                    }
                    else
                    {
                        var newDev = Device.GetClientDeviceByMacAddress(dev.MacAddress, dbConnection);
                        //var LoginDate = dev.LastLoginDate.ToString("d MMM  hh:mm tt");
                        json += "{ \"Username\" : \"" + dev.PCName + "\",\"Id\" : \"" + dev.PCName + "\",\"Long\" : \"" + newDev.Longitude.ToString() + "\",\"Lat\" : \"" + newDev.Latitude.ToString() + "\",\"LastLog\" : \"" + dev.LastLoginDate + "\",\"State\" : \"" + dev.Status + "\",\"Logs\" : \"User\",\"Cams\" : \"" + camString + "\"},";

                    }

                }
                if (id > 0)
                {
                    var item2 = CustomEvent.GetCustomEventById(id, dbConnection);
                    if (item2 != null)
                    {
                        json += "{ \"Username\" : \"" + item2.Name + "\",\"Id\" : \"" + item2.EventId.ToString() + "\",\"Long\" : \"" + item2.Longtitude.ToString() + "\",\"Lat\" : \"" + item2.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"User\"},";
                    }
                }
                json = json.Substring(0, json.Length - 1);
                json += "]";
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("OthersDash.aspx", "getGPSDataUsers", ex, dbConnection, userinfo.SiteId);
            }
            return json;
        }
        [WebMethod]
        public static List<string> getAssignUserTableData(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var allusers = new List<Users>();
                var devices = new List<Arrowlabs.Business.Layer.HealthCheck>();
                if (userinfo.RoleId == (int)Role.Manager)
                {
                    allusers = Users.GetAllFullUsersByManagerId(userinfo.ID, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.Admin)
                {
                    var sessions = DirectorManager.GetAllFullManagersByDirectorId(userinfo.ID, dbConnection);
                    foreach (var usr in sessions)
                    {
                        allusers.Add(usr);
                        var getallUsers = Users.GetAllFullUsersByManagerIdForDirector(usr.ID, dbConnection);
                        foreach (var subsubuser in getallUsers)
                        {
                            allusers.Add(subsubuser);
                        }
                    }
                    var unassigned = Users.GetAllUnassignedOperators(dbConnection);
                    unassigned = unassigned.Where(i => i.SiteId == (int)userinfo.SiteId).ToList();
                    foreach (var subsubuser in unassigned)
                    {
                        allusers.Add(subsubuser);
                    } 
                }
                else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                {
                    allusers = Users.GetAllUsersByCustomerId(userinfo.CustomerInfoId, dbConnection);
                    allusers = allusers.Where(i => i.RoleId != (int)Role.CustomerSuperadmin).ToList();
                }
                else if (userinfo.RoleId == (int)Role.Director)
                {
                    allusers = Users.GetAllUsersBySiteId(userinfo.SiteId, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                   // var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                  //  foreach (var site in sites)
                  //  {
                    allusers.AddRange(Users.GetAllUsersByLevel5(userinfo.ID, dbConnection));
                   // }
                }
                else if (userinfo.RoleId == (int)Role.ChiefOfficer)
                {
                    allusers = Users.GetAllUsers(dbConnection);
                    allusers = allusers.Where(i => i.RoleId != (int)Role.ChiefOfficer).ToList();
                }
                else if (userinfo.RoleId == (int)Role.SuperAdmin)
                {
                    allusers = Users.GetAllUsers(dbConnection);
                    devices = Arrowlabs.Business.Layer.HealthCheck.GetAllDevicesHealthCheck(dbConnection);
                }

                var grouped = allusers.GroupBy(item => item.ID);
                allusers = grouped.Select(grp => grp.OrderBy(item => item.ID).First()).ToList();
                foreach (var item in allusers)
                {
                    //if (item.DeviceType == (int)Users.DeviceTypes.Mobile || item.DeviceType == (int)Users.DeviceTypes.MobileAndClient || item.RoleId == (int)Role.Manager)
                   // {
                        if (item.Username != userinfo.Username)
                        {
                            var returnstring = "<tr role='row' class='odd'><td>" + item.Username + "</td><td class='sorting_1'>" + item.Status + "</td><td><a href='#' class='red-color' id=" + item.ID + item.Username + " onclick='dispatchUserchoiceTable(&apos;" + item.ID + "&apos;,&apos;" + item.Username + "&apos;)'><i class='fa fa-plus red-color'></i>ADD</a></td></tr>";
                            listy.Add(returnstring);
                        }
                   // }
                }
                foreach (var dev in devices)
                {
                    if (dev.Status == 1)
                    {
                        var newDev = Device.GetClientDeviceByMacAddress(dev.MacAddress, dbConnection);
                        var returnstring = "<tr role='row' class='odd'><td>" + newDev.PCName + "</td><td class='sorting_1'>Online</td><td><a href='#' class='red-color' id=" + newDev.PCName + newDev.PCName + " onclick='dispatchUserchoiceTable(&apos;" + newDev.PCName + "&apos;,&apos;" + newDev.PCName + "&apos;)'><i class='fa fa-plus red-color'></i>ADD</a></td></tr>";
                        listy.Add(returnstring);
                        //<td><a href='#' ><i class='fa fa-eye mr-1x'></i>View Location</a></td>
                    }
                    else
                    {
                        var newDev = Device.GetClientDeviceByMacAddress(dev.MacAddress, dbConnection);
                        var returnstring = "<tr role='row' class='odd'><td>" + newDev.PCName + "</td><td class='sorting_1'>Offline</td><td><a href='#' class='red-color' id=" + newDev.PCName + newDev.PCName + " onclick='dispatchUserchoiceTable(&apos;" + newDev.PCName + "&apos;,&apos;" + newDev.PCName + "&apos;)'><i class='fa fa-plus red-color'></i>ADD</a></td></tr>";
                        listy.Add(returnstring);
                        //<td><a href='#' ><i class='fa fa-eye mr-1x'></i>View Location</a></td>
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("OtherDash.aspx", "getAssignUserTableData", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getTableRowData(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var customData = CustomEvent.GetCustomEventById(id, dbConnection);

                if (customData != null)
                {
                    var offenceinfo = DriverOffence.GetDriverOffenceById(customData.Identifier, dbConnection);
                    if (offenceinfo != null)
                    {
                        listy.Add(offenceinfo.PlateNumber);
                        listy.Add(offenceinfo.PlateSource);
                        listy.Add(offenceinfo.PlateCode);
                        listy.Add(offenceinfo.VehicleMake);
                        listy.Add(offenceinfo.VehicleColor);
                        var ggUser = Users.GetUserByName(offenceinfo.UserName, dbConnection);
                        listy.Add(ggUser.CustomerUName);
                        var geolocation = ReverseGeocode.RetrieveFormatedAddress(customData.Latitude.ToString(), customData.Longtitude.ToString(), getClientLic);
                        listy.Add(geolocation);
                        listy.Add(offenceinfo.CreatedDate.AddHours(userinfo.TimeZone).ToString());
                        listy.Add(offenceinfo.OffenceCategory);
                        listy.Add(offenceinfo.OffenceType);
                        listy.Add(offenceinfo.TicketStatus.ToString());
                        listy.Add(offenceinfo.Comments);
                    }
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Ticketing", "getTableRowData", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static List<string> getEventHistoryData(int id,string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);

            var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

            if (isValidSession == null)
            {
                listy.Add("LOGOUT");
                return listy;
            }

            var allEventHistory = EventHistory.GetEventHistoryByEventId(Convert.ToInt32(id), dbConnection); //new List<CustomEvent>();
            foreach (var rem in allEventHistory)
            {
                var gethotevent = CustomEvent.GetCustomEventById(rem.EventId, dbConnection);

                if (gethotevent.EventType == CustomEvent.EventTypes.MobileHotEvent)
                {
                    gethotevent.Name = gethotevent.EventTypeName;
                    //var mobEv = MobileHotEvent.GetMobileHotEventByGuid(gethotevent.Identifier, dbConnection);
                    //if (mobEv != null)
                    //{
                    //    if (string.IsNullOrEmpty(mobEv.EventTypeName))
                    //        gethotevent.Name = "None";
                    //    else
                    //        gethotevent.Name = mobEv.EventTypeName;
                    //}
                }

                var actioninfo = string.Empty;
                var returnstring = string.Empty;
                var incidentLocation = string.Empty;
                if (!string.IsNullOrEmpty(rem.Longtitude) && !string.IsNullOrEmpty(rem.Latitude))
                    incidentLocation = "from " + ReverseGeocode.RetrieveFormatedAddress(rem.Latitude.ToString(), rem.Longtitude.ToString(), getClientLic);

                if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Pending)
                {
                    actioninfo = "created ";
                    returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.CreatedBy + "</span>" + actioninfo + "<span class='red-color'>" + gethotevent.Name + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                    listy.Add(returnstring);
                }
                else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Complete)
                {
                    actioninfo = "completed ";
                    returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.UserName + "</span>" + actioninfo + "<span class='red-color'>" + gethotevent.Name + "</span>" + incidentLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                    listy.Add(returnstring);
                }
                else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Dispatch)
                {
                    //var incidentNotification2 = Arrowlabs.Business.Layer.Notification.GetAllNotificationsByIncidentIdAndByHandled(gethotevent.EventId, false, dbConnection);
                    //foreach (var item in incidentNotification2)
                    // {
                    actioninfo = "dispatched ";
                    incidentLocation = "to " + rem.UserName;
                    returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.CreatedBy + "</span>" + actioninfo + "<span class='red-color'>" + gethotevent.Name + "</span>" + incidentLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                    listy.Add(returnstring);
                    //}
                }
                else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Resolve)
                {
                    actioninfo = "resolved ";
                    incidentLocation = "";
                    returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.CreatedBy + "</span>" + actioninfo + "<span class='red-color'>" + gethotevent.Name + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                    listy.Add(returnstring);
                }
                else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Reject)
                {
                    actioninfo = "rejected ";
                    incidentLocation = "";
                    returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.CreatedBy + "</span>" + actioninfo + "<span class='red-color'>" + gethotevent.Name + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                    listy.Add(returnstring);
                }
                else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Park)
                {
                    actioninfo = "parked ";
                    incidentLocation = "";
                    returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.CreatedBy + "</span>" + actioninfo + "<span class='red-color'>" + gethotevent.Name + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                    listy.Add(returnstring);
                }
                else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Engage)
                {
                    actioninfo = "engaged ";
                    returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.UserName + "</span>" + actioninfo + "<span class='red-color'>" + gethotevent.Name + "</span>" + incidentLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                    listy.Add(returnstring);
                }
                else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Release)
                {
                    actioninfo = "released ";
                    returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.UserName + "</span>" + actioninfo + "<span class='red-color'>" + gethotevent.Name + "</span>" + incidentLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                    listy.Add(returnstring);
                }


            }
            return listy;
        }

        [WebMethod]
        public static string getAttachmentDataIcons(int id, string uname)
        {
            var listy = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                var cusEv = CustomEvent.GetCustomEventById(id, dbConnection);
                var attachments = DriverOffence.GetDriverOffenceById(cusEv.Identifier, dbConnection);
                var settings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                var i = 0;
                //var imgstring = settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/OffenceImagePath.jpeg";
                var retstring2 = string.Empty;
                if (!string.IsNullOrEmpty(attachments.OffenceImagePath1))
                {
                    retstring2 = "<img src='" + attachments.OffenceImagePath1 + "' data-toggle='tab' data-target='#image-" + i + "-tab'/>";
                    listy += retstring2;
                    i++;
                }
                if (!string.IsNullOrEmpty(attachments.Image1Path))
                {
                    var imgstring = settings.MIMSMobileAddress + "/Images/videoicon.png";
                    retstring2 = "<img src='" + imgstring + "' data-toggle='tab' data-target='#video-0-tab'/>";
                    listy += retstring2;

                    imgstring = settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/ImagePath1.jpeg";
                    retstring2 = "<img src='" + imgstring + "' data-toggle='tab' data-target='#image-2-tab'/>";
                    listy += retstring2;

                    imgstring = settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/ImagePath2.jpeg";
                    retstring2 = "<img src='" + imgstring + "' data-toggle='tab' data-target='#image-3-tab'/>";
                    listy += retstring2;

                    imgstring = settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/ImagePath3.jpeg";
                    retstring2 = "<img src='" + imgstring + "' data-toggle='tab' data-target='#image-4-tab'/>";
                    listy += retstring2;

                    imgstring = settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/ImagePath4.jpeg";
                    retstring2 = "<img src='" + imgstring + "' data-toggle='tab' data-target='#image-5-tab'/>";
                    listy += retstring2;

                    imgstring = settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/ImagePath5.jpeg";
                    retstring2 = "<img src='" + imgstring + "' data-toggle='tab' data-target='#image-6-tab'/>";
                    listy += retstring2;

                    imgstring = settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/ImagePath6.jpeg";
                    retstring2 = "<img src='" + imgstring + "' data-toggle='tab' data-target='#image-7-tab'/>";
                    listy += retstring2;

                    i = 7;
                }
                var evattachments = MobileHotEventAttachment.GetMobileHotEventAttachmentByEventId(cusEv.EventId, dbConnection);
                if (evattachments.Count > 0)
                {
                    foreach (var item in evattachments)
                    {
                        var imgstring = string.Empty;
                        if (!string.IsNullOrEmpty(item.AttachmentPath))
                        {

                            if (i == 3)
                            {
                                var retstring = "<img src='../images/more.png' data-toggle='tab' data-target='#ticketattachments-tab' />";
                                listy += retstring;
                                i++;
                                break;
                            }
                            else
                            {
                                var requiredString = item.AttachmentPath;//.Substring(index, (item.AttachmentPath.Length - index)); 
                                if (VideoExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))
                                {
                                    var retstring = "<img src='../images/VLCMediaPlayer1.png' data-toggle='tab' onclick='play(" + i + ")' data-target='#video-" + i + "-tab'/>";
                                    listy += retstring;
                                    i++;
                                }
                                else if (CommonUtility.FileExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))//(System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".PDF")
                                {

                                }
                                else
                                {
                                    var retstring = "<img src='" + requiredString + "' data-toggle='tab' data-target='#image-" + i + "-tab'/>";
                                    listy += retstring;
                                    i++;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Ticketing", "getAttachmentDataIcons", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static string getAttachmentDataTab(int id, string uname)
        {
            var listy = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }
                var i = 0;
                var iTab = 0;
                var cusEv = CustomEvent.GetCustomEventById(id, dbConnection);
                listy += "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#ticketlocation-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-map-marker fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Location</p></div></div></div>";
                var attachments = DriverOffence.GetDriverOffenceById(cusEv.Identifier, dbConnection);
                if (!string.IsNullOrEmpty(attachments.OffenceImagePath1))
                {
                    listy += "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-" + i + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Image</p></div></div></div>";
                    i++;
                    iTab++;
                }
                if (!string.IsNullOrEmpty(attachments.Image1Path))
                {
                    listy += "<div class='row static-height-with-border clickable-row' data-toggle='tab' onclick='play(1)' data-target='#video-0-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-play-circle-o fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Video</p></div></div></div>";
                    listy += "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-2-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Snap Image 1</p></div></div></div>";
                    listy += "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-3-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Snap Image 2</p></div></div></div>";
                    listy += "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-4-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Snap Image 3</p></div></div></div>";
                    listy += "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-5-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Snap Image 4</p></div></div></div>";
                    listy += "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-6-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Snap Image 5</p></div></div></div>";
                    listy += "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-7-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Snap Image 6</p></div></div></div>";
                    i = 7;
                    iTab = 7;
                }

                var evattachments = MobileHotEventAttachment.GetMobileHotEventAttachmentByEventId(cusEv.EventId, dbConnection);

                if (evattachments.Count > 0)
                {
                    foreach (var item in evattachments)
                    {
                        var imgstring = string.Empty;
                        if (!string.IsNullOrEmpty(item.AttachmentPath))
                        {
                            // int index = item.AttachmentPath.IndexOf("HotEvents");
                            var requiredString = item.AttachmentPath;//.Substring(index, (item.AttachmentPath.Length - index));
                            // requiredString = requiredString.Replace("\\", "/");

                            if (VideoExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))
                            {
                                var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' onclick='play(" + iTab + ")' data-target='#video-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-play-circle-o fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceTicket(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                listy += retstring;
                                iTab++;
                            }
                            else if (CommonUtility.FileExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))//(System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".PDF")
                            {
                                //var mimssettings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                                //imgstring = String.Format(mimssettings.MIMSMobileAddress + "/Uploads/HotEvents/{0}", System.IO.Path.GetFileName(requiredString));

                                var attachmentName = CommonUtility.getAttachmentDisplayName(item.AttachmentName, i);
                                //if (!string.IsNullOrEmpty(attachmentName))
                                //    attachmentName = attachmentName.Split('-')[1];
                                var retstringExtra = "<div class='row static-height-with-border clickable-row' ><div class='col-md-12'><div onclick='window.open(&apos;" + requiredString + "&apos;);' class='inline-block mr-2x'><i class='fa fa-file-pdf-o fa-2x gray-bg red-color'></i></div><div class='inline-block' onclick='window.open(&apos;" + requiredString + "&apos;);'><p>" + attachmentName + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceTicket(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                listy += retstringExtra;
                            }
                            else
                            {
                                var attachmentName = CommonUtility.getAttachmentDisplayName(item.AttachmentName, i);
                                //if (!string.IsNullOrEmpty(attachmentName))
                                //    attachmentName = attachmentName.Split('-')[1];
                                var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>" + attachmentName + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceTicket(&apos;" + item.Id + "&apos;)'></i></div></div></div>";

                                //var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                listy += retstring;
                                iTab++;
                            }
                            i++;
                        }
                        else
                        {
                            if (item.Attachment != null)
                            {
                                var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#image-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceTicket(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                listy += retstring;
                                iTab++;
                                i++;
                            }
                        }
                    }
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Ticketing", "getAttachmentDataTab", err, dbConnection, userinfo.SiteId);
            }

            return listy;
        }
        [WebMethod]
        public static List<string> getAttachmentData(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var cusEv = CustomEvent.GetCustomEventById(id, dbConnection);
                var attachments = DriverOffence.GetDriverOffenceById(cusEv.Identifier, dbConnection);
                var settings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);

                var retstring = string.Empty;//"<video id='Video0' width='100%' height='380px' autoplay='autoplay' type='video/ogg; codecs=theo' muted controls ><source src='" + settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/" + attachments.Identifier.ToString() + ".ogg' /></video>";
                //var retstring = "<video id='Video0' width='100%' height='380px' src='" + settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/" + attachments.Identifier.ToString() + ".ogg' type='video/ogg; codecs=theo' autoplay='autoplay' />";
                //listy.Add(retstring);
                var i = 0;
                //var imgstring = settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/OffenceImagePath.jpeg";
                var retstring2 = string.Empty;
                if (!string.IsNullOrEmpty(attachments.OffenceImagePath1))
                {
                    retstring2 = "<img onclick='rotateMe(this);' src='" + attachments.OffenceImagePath1 + "' class='resized-filled-image' onload='loadMe(this);'/>";
                    listy.Add(retstring2);
                    i++;
                }
                if (!string.IsNullOrEmpty(attachments.Image1Path))
                {
                    var imgstring = settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/ImagePath1.jpeg";
                    retstring2 = "<img onclick='rotateMe(this);' src='" + imgstring + "' class='resized-filled-image' onload='loadMe(this);'/>";
                    listy.Add(retstring2);

                    imgstring = settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/ImagePath2.jpeg";
                    retstring2 = "<img onclick='rotateMe(this);' src='" + imgstring + "' class='resized-filled-image' onload='loadMe(this);'/>";
                    listy.Add(retstring2);

                    imgstring = settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/ImagePath3.jpeg";
                    retstring2 = "<img onclick='rotateMe(this);' src='" + imgstring + "' class='resized-filled-image' onload='loadMe(this);'/>";
                    listy.Add(retstring2);

                    imgstring = settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/ImagePath4.jpeg";
                    retstring2 = "<img onclick='rotateMe(this);' src='" + imgstring + "' class='resized-filled-image' onload='loadMe(this);'/>";
                    listy.Add(retstring2);

                    imgstring = settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/ImagePath5.jpeg";
                    retstring2 = "<img onclick='rotateMe(this);' src='" + imgstring + "' class='resized-filled-image' onload='loadMe(this);'/>";
                    listy.Add(retstring2);

                    imgstring = settings.MIMSMobileAddress + "/Uploads/Video/" + attachments.Identifier.ToString() + "/ImagePath6.jpeg";
                    retstring2 = "<img onclick='rotateMe(this);' src='" + imgstring + "' class='resized-filled-image' onload='loadMe(this);'/>";
                    listy.Add(retstring2);

                    i = 7;
                }
                var evattachments = MobileHotEventAttachment.GetMobileHotEventAttachmentByEventId(cusEv.EventId, dbConnection);

                if (evattachments.Count > 0)
                {

                    foreach (var item in evattachments)
                    {
                        var imgstring = string.Empty;
                        if (!string.IsNullOrEmpty(item.AttachmentPath))
                        {
                            //int index = item.AttachmentPath.IndexOf("HotEvents");
                            var requiredString = item.AttachmentPath;//.Substring(index, (item.AttachmentPath.Length - index));
                            //requiredString = requiredString.Replace("\\", "/");
                            if (VideoExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))
                            {
                                //var retstring = "<iframe width='100%' height='378' frameborder='0' class='video-presenter' allowfullscreen></iframe><div class='video-link'>" + mimssettings.MIMSMobileAddress + "/Uploads/" + requiredString + "</div>";//"<img src='images/VLCMediaPlayer1.png' data-toggle='tab' data-target='#image-1-tab'/>";
                                retstring = "<video id='Video" + i + "' width='100%' height='380px' muted controls ><source src='" + requiredString + "' /></video>";
                                listy.Add(retstring);
                                i++;
                            }
                            else if (CommonUtility.FileExtensions.Contains(System.IO.Path.GetExtension(requiredString).ToUpperInvariant()))//(System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".PDF" || System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".DOCX")
                            {

                            }
                            else
                            {
                                //imgstring = mimssettings.MIMSMobileAddress + "/Uploads/" + requiredString;
                                retstring = "<img onclick='rotateMe(this);' src='" + requiredString + "' class='resized-filled-image' onload='loadMe(this);'/>";
                                listy.Add(retstring);
                                i++;
                            }

                        }
                        else
                        {
                            if (item.Attachment != null)
                            {
                                var base64 = Convert.ToBase64String(item.Attachment);
                                imgstring = String.Format("data:image/png;base64,{0}", base64);
                                retstring = "<img onclick='rotateMe(this);' src='" + imgstring + "' class='resized-filled-image' onload='loadMe(this);'/>";
                                listy.Add(retstring);
                                i++;
                            }
                        }
                    }
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Ticketing", "getAttachmentData", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static string getLocationData(int id,string uname)
        {
            var json = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var item = CustomEvent.GetCustomEventById(Convert.ToInt32(id), dbConnection);

            var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

            if (isValidSession == null)
            {
                return "LOGOUT";
            }

            //var tracebackhistory = TraceBackHistory.GetTracBackHistoryByIncidentId(item.EventId, dbConnection);
            json += "[";
            if (item != null)
            {
                if (getClientLic != null)
                {
                    if (getClientLic.isLocation)
                    {
                        if (!string.IsNullOrEmpty(item.Longtitude) && !string.IsNullOrEmpty(item.Longtitude))
                        {
                            json += "{ \"Username\" : \"Ticket\",\"Id\" : \"" + item.EventId.ToString() + "\",\"Long\" : \"" + item.Longtitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                        }
                    }
                }
            }
            
            json = json.Substring(0, json.Length - 1);
            json += "]";
            return json;
        }

        [WebMethod]
        public static string isUser(string id)
        {
            var result = "false";
            {
                var getuser = Users.GetUserByName(id, dbConnection);
                if (getuser != null)
                    result = "true";
            }
            return result;
        }

        [WebMethod]
        public static string getAssigneeType(string id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

            if (isValidSession == null)
            {
                return "LOGOUT";
            }

            if (CommonUtility.isNumeric(id))
                return "User";
            else
                return "Device";
        }

        [WebMethod]
        public static string changeIncidentState(string id, string state,string ins,string uname)
        {
            int taskId1 = 0;
            var retVal = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);

            var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

            if (isValidSession == null)
            {
                return "LOGOUT";
            }

            if (!string.IsNullOrEmpty(id))
            {
                var incidentinfo = CustomEvent.GetCustomEventById(Convert.ToInt32(id), dbConnection);
                incidentinfo.UpdatedBy = userinfo.Username;
                incidentinfo.Instructions = ins;
                incidentinfo.IncidentStatus = CommonUtility.getIncidentStatusValue(state.ToLower());
                //incidentinfo.SiteId = userinfo.SiteId;
                incidentinfo.SessionId = System.Web.HttpContext.Current.Session.SessionID;
                incidentinfo.Password = Encrypt.EncryptData(userinfo.Password, true, dbConnection);
                var returnV = CommonUtility.CreateIncident(incidentinfo);
                if (returnV != "")
                {
                    taskId1 = Convert.ToInt32(returnV);
                    //taskId1 = CustomEvent.InsertorUpdateIncident(incidentinfo, dbConnection);
                    retVal = incidentinfo.Name;
                    if (incidentinfo.EventType == CustomEvent.EventTypes.MobileHotEvent)
                    {
                        retVal = incidentinfo.EventTypeName;
                        //var getMobName = MobileHotEvent.GetMobileHotEventByGuid(incidentinfo.Identifier, dbConnection);
                        //{
                        //    if (string.IsNullOrEmpty(getMobName.EventTypeName))
                        //        retVal = "None";
                        //    else
                        //        retVal = getMobName.EventTypeName;
                        //}
                    }
                    if (taskId1 < 1)
                    {
                        taskId1 = Convert.ToInt32(id);
                    }
                    if (incidentinfo.IncidentStatus != (int)CustomEvent.IncidentActionStatus.Dispatch)
                    {
                        var EventHistoryEntry = new EventHistory();
                        EventHistoryEntry.CreatedDate = CommonUtility.getDTNow();
                        EventHistoryEntry.CreatedBy = userinfo.Username;
                        EventHistoryEntry.EventId = taskId1;
                        EventHistoryEntry.IncidentAction = incidentinfo.IncidentStatus;
                        EventHistoryEntry.UserName = userinfo.Username;
                        EventHistoryEntry.SiteId = userinfo.SiteId;
                        EventHistoryEntry.CustomerId = userinfo.CustomerInfoId;
                        EventHistory.InsertEventHistory(EventHistoryEntry, dbConnection, dbConnectionAudit, true);
                        
                    }
                    if (incidentinfo.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Resolve)
                    {
                        incidentinfo.Handled = true;
                        incidentinfo.HandledTime = CommonUtility.getDTNow();
                        incidentinfo.HandledBy = userinfo.Username;
                        incidentinfo.SiteId = userinfo.SiteId;
                        taskId1 = CustomEvent.InsertorUpdateIncident(incidentinfo, dbConnection, dbConnectionAudit,true);
                        if (taskId1 < 1)
                        {
                            taskId1 = Convert.ToInt32(id);
                        }
                        var notifications = Arrowlabs.Business.Layer.Notification.GetAllNotificationsByIncidentId(taskId1, dbConnection);
                        foreach (var noti in notifications)
                        {
                            if (noti.AssigneeType == (int)TaskAssigneeType.User)
                            {
                                var taskuser = Users.GetUserById(noti.AssigneeId, dbConnection);
                                taskuser.Engaged = false;
                                taskuser.EngagedIn = string.Empty;
                                Users.InsertOrUpdateUsers(taskuser, dbConnection, dbConnectionAudit,true);
                            }
                            else if (noti.AssigneeType == (int)TaskAssigneeType.Group)
                            {
                                var groupusers = Users.GetAllUserByGroupId(noti.AssigneeId, dbConnection);
                                foreach (var taskuser in groupusers)
                                {
                                    var splitinfo = taskuser.EngagedIn.Split('-');
                                    if (splitinfo.Length > 1)
                                    {
                                        if (splitinfo[1] == noti.Id.ToString())
                                        {
                                            taskuser.Engaged = false;
                                            taskuser.EngagedIn = string.Empty;
                                            Users.InsertOrUpdateUsers(taskuser, dbConnection, dbConnectionAudit,true);
                                        }
                                    }
                                }
                            }
                        }

                        Arrowlabs.Business.Layer.Notification.UpdateNotificationStatusByIncidentId(taskId1, true, dbConnection);
                        var taskinfo = UserTask.GetAllTaskByIncidentId(taskId1, dbConnection);
                        foreach (var task in taskinfo)
                        {
                            task.Status = (int)TaskStatus.Cancelled;

                            var tId = UserTask.InsertorUpdateTask(task, dbConnection, dbConnectionAudit, true);
                            task.Id = tId;
                            //UserTask.InsertorUpdateTask(task, dbConnection);
                        }
                        CustomEvent.InsertorUpdateIncident(incidentinfo, dbConnection, dbConnectionAudit,true);
                        CustomEvent.CustomEventHandledById(incidentinfo.EventId, true, userinfo.Username, dbConnection);
                    }
                    else if (incidentinfo.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Release)
                    {
                        var notifications = Arrowlabs.Business.Layer.Notification.GetAllNotificationsByIncidentId(taskId1, dbConnection);
                        foreach (var noti in notifications)
                        {
                            if (noti.AssigneeType == (int)TaskAssigneeType.User)
                            {
                                var taskuser = Users.GetUserById(noti.AssigneeId, dbConnection);
                                taskuser.Engaged = false;
                                taskuser.EngagedIn = string.Empty;
                                Users.InsertOrUpdateUsers(taskuser, dbConnection, dbConnectionAudit,true);
                            }
                            else if (noti.AssigneeType == (int)TaskAssigneeType.Group)
                            {
                                var groupusers = Users.GetAllUserByGroupId(noti.AssigneeId, dbConnection);
                                foreach (var taskuser in groupusers)
                                {
                                    var splitinfo = taskuser.EngagedIn.Split('-');
                                    if (splitinfo.Length > 1)
                                    {
                                        if (splitinfo[1] == noti.Id.ToString())
                                        {
                                            taskuser.Engaged = false;
                                            taskuser.EngagedIn = string.Empty;
                                            Users.InsertOrUpdateUsers(taskuser, dbConnection, dbConnectionAudit,true);
                                        }
                                    }
                                }
                            }
                        }

                        Arrowlabs.Business.Layer.Notification.UpdateNotificationStatusByIncidentId(taskId1, true, dbConnection);
                        var taskinfo = UserTask.GetAllTaskByIncidentId(taskId1, dbConnection);
                        foreach (var task in taskinfo)
                        {
                            task.Status = (int)TaskStatus.Cancelled;

                            var tId = UserTask.InsertorUpdateTask(task, dbConnection, dbConnectionAudit, true);
                            task.Id= tId;
                            
                        }
                    }
                    else if (incidentinfo.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Reject)
                    {
                        var notifications = Arrowlabs.Business.Layer.Notification.GetAllNotificationsByIncidentId(incidentinfo.EventId, dbConnection);
                        foreach (var noti in notifications)
                        {
                            if (noti.AssigneeType == (int)TaskAssigneeType.User)
                            {
                                var taskuser = Users.GetUserById(noti.AssigneeId, dbConnection);
                                taskuser.Engaged = false;
                                taskuser.EngagedIn = string.Empty;
                                Users.InsertOrUpdateUsers(taskuser, dbConnection, dbConnectionAudit,true);
                            }
                            else if (noti.AssigneeType == (int)TaskAssigneeType.Group)
                            {
                                var groupusers = Users.GetAllUserByGroupId(noti.AssigneeId, dbConnection);
                                foreach (var taskuser in groupusers)
                                {
                                    var splitinfo = taskuser.EngagedIn.Split('-');
                                    if (splitinfo.Length > 1)
                                    {
                                        if (splitinfo[1] == noti.Id.ToString())
                                        {
                                            taskuser.Engaged = false;
                                            taskuser.EngagedIn = string.Empty;
                                            Users.InsertOrUpdateUsers(taskuser, dbConnection, dbConnectionAudit,true);
                                        }
                                    }
                                }
                            }
                        }

                        Arrowlabs.Business.Layer.Notification.UpdateNotificationStatusByIncidentId(incidentinfo.EventId, true, dbConnection);
                        var taskinfo = UserTask.GetAllTaskByIncidentId(incidentinfo.EventId, dbConnection);
                        foreach (var task in taskinfo)
                        {
                            task.Status = (int)TaskStatus.Cancelled;

                            var tId = UserTask.InsertorUpdateTask(task, dbConnection, dbConnectionAudit, true);
                            taskId1 = tId;
                            
                        }
                    }
                }
            }
            return retVal;
        }

        [WebMethod]
        public static string sendpushMultipleNotification(string[] userIds, string msg,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                Thread thread = new Thread(() => CommonUtility.sendPushNotificationAlarm(userIds, msg, dbConnection));
                thread.Start();
                //Thread thread = new Thread(() => PushNotificationClient.SendtoAppleMultiUserNotification("Alarm: " + msg, userIds, dbConnection));
                //thread.Start();
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("OthersDash.aspx", "sendpushMultipleNotification", ex, dbConnection, userinfo.SiteId);
                return "FAIL";
            }
            return "SUCCESS";
        }
        [WebMethod]
        public static string getCurrentLocation(string  lat, string lng,string uname)
        {
            var retVal = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                retVal = ReverseGeocode.RetrieveFormatedAddress(lat, lng, getClientLic);
                var split = retVal.Split('-');
                retVal = split[split.Length-1];
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("OthersDash.aspx", "getCurrentLocation", ex, dbConnection, userinfo.SiteId);
            }
            return retVal;
        }
        [WebMethod]
        public static string insertNewReminder(string name, string date, string reminder,int colorcode,string todate,string fromdate,string uname)
        {
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }


                    var newReminder = new Reminders();
                    newReminder.ReminderTopic = name;
                    newReminder.ReminderNote = reminder;
                    newReminder.ToDate = todate;
                    newReminder.FromDate = fromdate;
                    newReminder.ColorCode = colorcode;
                    newReminder.ReminderDate = Convert.ToDateTime(date);
                    newReminder.CreatedBy = userinfo.Username;
                    newReminder.CreatedDate = CommonUtility.getDTNow();
                    newReminder.UpdatedBy = userinfo.Username;
                    newReminder.UpdatedDate = CommonUtility.getDTNow();
                    newReminder.SiteId = userinfo.SiteId;

                    newReminder.CustomerId = userinfo.CustomerInfoId;
                    Reminders.InsertorUpdateReminder(newReminder, dbConnection, dbConnectionAudit, true);
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("OthersDash.aspx", "insertNewReminder", ex, dbConnection, userinfo.SiteId);
                    return ex.Message;
                }
                return "SUCCESS";
            }
        }
        [WebMethod]
        public static string inserttask(string id, string assigneetype, string assigneename, string assigneeid, string templatename, string longi, string lati, int incidentId,string uname)
        {
            int taskId1 = 0;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);

            var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

            if (isValidSession == null)
            {
                return "LOGOUT";
            }

            if (Convert.ToBoolean(id))
            {
                var temptasks = UserTask.GetTaskTemplateById(Convert.ToInt32(Convert.ToInt32(templatename)), dbConnection);
                var newTasks = new UserTask();
                newTasks.Name = temptasks.Name;
                newTasks.Description = temptasks.Description;
                newTasks.IncidentId = Convert.ToInt32(incidentId);
                if (assigneetype == TaskAssigneeType.Location.ToString())
                {
                    newTasks.AssigneeName = assigneename;
                    newTasks.AssigneeId = Convert.ToInt32(assigneeid);//Convert.ToInt32(tbUserID.Value);
                    newTasks.AssigneeType = (int)TaskAssigneeType.Location;
                }
                else if (assigneetype == TaskAssigneeType.Group.ToString())
                {
                    newTasks.AssigneeName = assigneename;
                    newTasks.AssigneeId = Convert.ToInt32(assigneeid);//Convert.ToInt32(tbUserID.Value);
                    newTasks.AssigneeType = (int)TaskAssigneeType.Group;
                }
                else if (assigneetype == TaskAssigneeType.User.ToString())
                {
                    newTasks.AssigneeType = (int)TaskAssigneeType.User;
                    newTasks.AssigneeName = assigneename;
                    newTasks.AssigneeId = Convert.ToInt32(assigneeid);//Convert.ToInt32(tbUserID.Value);

                }
                else if (assigneetype == TaskAssigneeType.Device.ToString())
                {
                    newTasks.AssigneeType = (int)TaskAssigneeType.Device;
                    newTasks.AssigneeName = assigneename;
                    newTasks.AssigneeId = 0;

                }
                newTasks.CreateDate = CommonUtility.getDTNow();
                newTasks.CreatedBy = userinfo.Username;
                newTasks.ManagerName = userinfo.Username;
                newTasks.ManagerId = userinfo.ID;

                newTasks.IsDeleted = false;
                newTasks.UpdatedDate = CommonUtility.getDTNow();

                if (!string.IsNullOrEmpty(longi))
                    newTasks.Longitude = Convert.ToDouble(longi);
                else
                    newTasks.Longitude = 0;

                if (!string.IsNullOrEmpty(lati))
                    newTasks.Latitude = Convert.ToDouble(lati);
                else
                    newTasks.Latitude = 0;

                newTasks.StartDate = CommonUtility.getDTNow().Date;
                newTasks.EndDate = CommonUtility.getDTNow().Date;
                newTasks.Priority = temptasks.Priority;
                newTasks.TemplateCheckListId = temptasks.TemplateCheckListId;
                newTasks.SiteId = userinfo.SiteId;
                taskId1 = UserTask.InsertorUpdateTask(newTasks, dbConnection, dbConnectionAudit, true);
                newTasks.Id = taskId1;
                
                var tskhistory = new TaskEventHistory();
                tskhistory.Action = (int)TaskAction.Pending;
                tskhistory.CreatedBy = userinfo.Username;
                tskhistory.CreatedDate = CommonUtility.getDTNow();
                tskhistory.TaskId = taskId1;
                tskhistory.SiteId = userinfo.SiteId;
                tskhistory.CustomerId = userinfo.CustomerInfoId;
                TaskEventHistory.InsertTaskEventHistory(tskhistory, dbConnection, dbConnectionAudit, true);
                
                var allTemplateCheckListItems = TemplateCheckListItem.GetAllTemplateCheckListItems(dbConnection);
                if (allTemplateCheckListItems != null && allTemplateCheckListItems.Count > 0)
                {
                    var selectedTemplateCheckListItems = allTemplateCheckListItems.Where(i => i.ParentCheckListId == newTasks.TemplateCheckListId).ToList();
                    if (selectedTemplateCheckListItems.Count > 0)
                    {
                        foreach (var templateCheckListItem in selectedTemplateCheckListItems)
                        {
                            var taskCheckList = new TaskCheckList();
                            taskCheckList.IsChecked = templateCheckListItem.IsChecked;
                            taskCheckList.TaskId = taskId1;
                            taskCheckList.UpdatedDate = CommonUtility.getDTNow();
                            taskCheckList.CreatedDate = CommonUtility.getDTNow();
                            taskCheckList.CreatedBy = Environment.UserName;
                            taskCheckList.TemplateCheckListItemId = templateCheckListItem.Id;
                            taskCheckList.SiteId = userinfo.SiteId;
                            taskCheckList.CustomerId = userinfo.CustomerInfoId;
                            TaskCheckList.InsertorUpdateTaskCheckListItem(taskCheckList, dbConnection, dbConnectionAudit,true);
                        }
                    }
                }
            }
            return taskId1.ToString();
        }
        [WebMethod]
        public static string insertNewIncident(string name, string desc, string locationid, string incidenttype, string notificationid, string taskid, string receivedby, string longi, string lati, string status, string instructions, string msgtask, string incidentId,string uname)
        {
            int taskId1 = 0;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);

            var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

            if (isValidSession == null)
            {
                return "LOGOUT";
            }

            CustomEvent newincident = null;
            var dtnow = CommonUtility.getDTNow();

            if (!string.IsNullOrEmpty(incidentId))
                if (Convert.ToInt32(incidentId) > 0)
                    newincident = CustomEvent.GetCustomEventById(Convert.ToInt32(incidentId), dbConnection);
                else
                {
                    newincident = new CustomEvent();
                    newincident.Identifier = Guid.NewGuid();
                    newincident.CreatedBy = userinfo.Username;
                }

            if (newincident != null)
            {
                newincident.UserName = userinfo.Username;

                newincident.RecevieTime = dtnow;
                newincident.EventType = CustomEvent.EventTypes.Incident;
                newincident.Name = name;
                newincident.Description = desc;
                newincident.LocationId = Convert.ToInt32(locationid);
                newincident.IncidentType = Convert.ToInt32(incidenttype);
                newincident.ReceivedBy = receivedby;

                newincident.CreatedDate = dtnow;
                newincident.Longtitude = longi;
                newincident.Latitude = lati;
                newincident.Instructions = instructions;

                if (Convert.ToBoolean(msgtask))
                    newincident.TemplateTaskId = Convert.ToInt32(taskid);
                else
                    newincident.TemplateTaskId = 0;

                newincident.IncidentStatus = (int)CustomEvent.IncidentActionStatus.Dispatch;
                newincident.SiteId = userinfo.SiteId;
                taskId1 = CustomEvent.InsertorUpdateIncident(newincident, dbConnection, dbConnectionAudit,true);



                if (taskId1 < 1)
                {
                    taskId1 = Convert.ToInt32(incidentId);
                }
                else
                {
                    var EventHistoryEntry = new EventHistory();
                    EventHistoryEntry.CreatedDate = dtnow;
                    EventHistoryEntry.CreatedBy = userinfo.Username;
                    EventHistoryEntry.EventId = taskId1;
                    EventHistoryEntry.IncidentAction = (int)CustomEvent.IncidentActionStatus.Pending;
                    EventHistoryEntry.UserName = receivedby;
                    EventHistoryEntry.SiteId = userinfo.SiteId;
                    EventHistoryEntry.CustomerId = userinfo.CustomerInfoId;
                    EventHistory.InsertEventHistory(EventHistoryEntry, dbConnection, dbConnectionAudit, true);
                    
                }

            }

            return taskId1.ToString();
        }
        //User Profile
        [WebMethod]
        public static int changePW(int id, string password,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                System.Web.Security.FormsAuthentication.SignOut();
                return 0;
            }
            else
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return 0;
                }

                var getuser = Users.GetUserById(userinfo.ID, dbConnection);
                var oldPw = getuser.Password;
                getuser.Password = Encrypt.EncryptData(password, true, dbConnection);
                getuser.UpdatedBy = userinfo.Username;
                getuser.UpdatedDate = CommonUtility.getDTNow();
                if (Users.InsertOrUpdateUsers(getuser, dbConnection, dbConnectionAudit, true))
                {
                    var oldValue = oldPw;
                    var newValue = Encrypt.EncryptData(password, true, dbConnection);
                    SystemLogger.SaveSystemLog(dbConnectionAudit, "Devices", newValue, oldValue, userinfo, "Password change");
                    return id;
                }
                else
                    return 0;
            }
        }
        [WebMethod]
        public static int addUserProfile(int id, string username, string firstname, string lastname, string emailaddress, string phonenumber, string password, int devicetype, int supervisor, int role, string imgPath,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            if(id > 1)
            {
                var getuser = Users.GetUserById(id, dbConnection);
                getuser.FirstName = firstname;
                getuser.LastName = lastname;
                getuser.Email = emailaddress;

                if (getuser.RoleId != role)
                {
                    getuser.RoleId = role;
                    if (role == (int)Role.Manager)
                    {
                        var getMang = Users.GetAllFullManagersByUserId(getuser.ID, dbConnection);
                        if (getMang != null)
                            UserManager.DeleteManagersByUserIdAndMangId(getMang.ID, getuser.ID, dbConnection);
                    }
                    else if (role == (int)Role.Operator || role == (int)Role.UnassignedOperator)
                    {
                        var getdir = Accounts.GetDirectorByManagerName(getuser.Username, dbConnection);
                        if (getdir != null)
                            DirectorManager.DeleteManagersByDirectorIdAndMangName(getuser.Username, getdir.ID, dbConnection);
                    }
                    else
                    {
                        var getdir = Accounts.GetDirectorByManagerName(getuser.Username, dbConnection);
                        if (getdir != null)
                            DirectorManager.DeleteManagersByDirectorIdAndMangName(getuser.Username, getdir.ID, dbConnection);

                        var getMang = Users.GetAllFullManagersByUserId(getuser.ID, dbConnection);
                        if (getMang != null)
                            UserManager.DeleteManagersByUserIdAndMangId(getMang.ID, getuser.ID, dbConnection);
                    }
                }
                if (getuser.RoleId == (int)Role.Manager)
                {

                    var dirUser = Users.GetUserById(supervisor, dbConnection);
                    var getdir = Accounts.GetDirectorByManagerName(getuser.Username, dbConnection);

                    if (getdir != null)
                        DirectorManager.DeleteManagersByDirectorIdAndMangName(getuser.Username, getdir.ID, dbConnection);

                    if (dirUser != null)
                    {
                        if (!string.IsNullOrEmpty(dirUser.Username))
                        {
                            List<DirectorManager> userManagerList = new List<DirectorManager>() { new DirectorManager() 
                                            { 
                                                DirectorId = dirUser.ID, 
                                                ManagerId = getuser.ID,
                                                CreatedBy = dirUser.Username,
                                                CreatedDate = CommonUtility.getDTNow(),
                                                UpdatedBy = dirUser.Username,
                                                UpdatedDate = CommonUtility.getDTNow(),
                                                ManagerName = getuser.Username,
                                                ManagerAccountName = getuser.AccountName   ,
                                                SiteId = dirUser.SiteId,
                                                CustomerId = userinfo.CustomerInfoId                         
                                            }};
                            DirectorManager.InsertDirectorManager(userManagerList, dbConnection, dbConnectionAudit, true);
                        }
                    }
                }
                else if (getuser.RoleId == (int)Role.Operator)
                {
                    if (supervisor > 0)
                    {
                        var manUser = Users.GetUserById(supervisor, dbConnection);
                        List<UserManager> userManagerList = new List<UserManager>() { new UserManager() { ManagerId = supervisor, UserId = getuser.ID, SiteId = manUser.SiteId, CustomerId = manUser.CustomerInfoId } };

                        UserManager.InsertUserManagers(userManagerList, dbConnection, dbConnectionAudit, true);
                    }
                }
                else if (getuser.RoleId == (int)Role.UnassignedOperator)
                {
                    var getMang = Users.GetAllFullManagersByUserId(getuser.ID, dbConnection);
                    if (getMang != null)
                        UserManager.DeleteManagersByUserIdAndMangId(getMang.ID, getuser.ID, dbConnection);
                }
                if (Users.InsertOrUpdateUsers(getuser, dbConnection, dbConnectionAudit,true))
                {
                    return id;
                }
                else
                    return 0;
            }
            else if (userinfo.ID > 0)
            {
                var getuser = userinfo;
                getuser.FirstName = firstname;
                getuser.LastName = lastname;
                getuser.Email = emailaddress;

                if (getuser.RoleId != role)
                {
                    getuser.RoleId = role;
                    if (role == (int)Role.Manager)
                    {
                        var getMang = Users.GetAllFullManagersByUserId(getuser.ID, dbConnection);
                        if (getMang != null)
                            UserManager.DeleteManagersByUserIdAndMangId(getMang.ID, getuser.ID, dbConnection);
                    }
                    else if (role == (int)Role.Operator || role == (int)Role.UnassignedOperator)
                    {
                        var getdir = Accounts.GetDirectorByManagerName(getuser.Username, dbConnection);
                        if (getdir != null)
                            DirectorManager.DeleteManagersByDirectorIdAndMangName(getuser.Username, getdir.ID, dbConnection);
                    }
                    else
                    {
                        var getdir = Accounts.GetDirectorByManagerName(getuser.Username, dbConnection);
                        if (getdir != null)
                            DirectorManager.DeleteManagersByDirectorIdAndMangName(getuser.Username, getdir.ID, dbConnection);

                        var getMang = Users.GetAllFullManagersByUserId(getuser.ID, dbConnection);
                        if (getMang != null)
                            UserManager.DeleteManagersByUserIdAndMangId(getMang.ID, getuser.ID, dbConnection);
                    }
                }
                if (getuser.RoleId == (int)Role.Manager)
                {

                    var dirUser = Users.GetUserById(supervisor, dbConnection);
                    var getdir = Accounts.GetDirectorByManagerName(getuser.Username, dbConnection);

                    if (getdir != null)
                        DirectorManager.DeleteManagersByDirectorIdAndMangName(getuser.Username, getdir.ID, dbConnection);

                    if (dirUser != null)
                    {
                        if (!string.IsNullOrEmpty(dirUser.Username))
                        {
                            List<DirectorManager> userManagerList = new List<DirectorManager>() { new DirectorManager() 
                                            { 
                                                DirectorId = dirUser.ID, 
                                                ManagerId = getuser.ID,
                                                CreatedBy = dirUser.Username,
                                                CreatedDate = CommonUtility.getDTNow(),
                                                UpdatedBy = dirUser.Username,
                                                UpdatedDate = CommonUtility.getDTNow(),
                                                ManagerName = getuser.Username,
                                                ManagerAccountName = getuser.AccountName,
                                                SiteId = dirUser.SiteId   ,
                                                CustomerId = userinfo.CustomerInfoId                         
                                            }};
                            DirectorManager.InsertDirectorManager(userManagerList, dbConnection, dbConnectionAudit, true);
                        }
                    }
                }
                else if (getuser.RoleId == (int)Role.Operator)
                {
                    if (supervisor > 0)
                    {
                        var manUser = Users.GetUserById(supervisor, dbConnection);
                        List<UserManager> userManagerList = new List<UserManager>() { new UserManager() { ManagerId = supervisor, UserId = getuser.ID, SiteId = manUser.SiteId, CustomerId = manUser.CustomerInfoId } };

                        UserManager.InsertUserManagers(userManagerList, dbConnection, dbConnectionAudit, true);
                    }
                }
                else if (getuser.RoleId == (int)Role.UnassignedOperator)
                {
                    var getMang = Users.GetAllFullManagersByUserId(getuser.ID, dbConnection);
                    if (getMang != null)
                        UserManager.DeleteManagersByUserIdAndMangId(getMang.ID, getuser.ID, dbConnection);
                }
                if (Users.InsertOrUpdateUsers(getuser, dbConnection, dbConnectionAudit,true))
                {
                    return 1;
                }
                else
                    return 0;
            }
            return 0;
        }

        [WebMethod]
        public static List<string> getUserProfileData(int id,string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);

            var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

            if (isValidSession == null)
            {
                listy.Add("LOGOUT");
                return listy;
            }

            var customData = Users.GetUserById(id, dbConnection);
            var supervisorId = 0;
            if (customData != null)
            {
                listy.Add(customData.Username);
                listy.Add(customData.FirstName + " " + customData.LastName);
                listy.Add(customData.Telephone);
                listy.Add(customData.Email);
                var geoLoc = ReverseGeocode.RetrieveFormatedAddress(customData.Latitude.ToString(), customData.Longitude.ToString(), getClientLic);
                listy.Add(geoLoc);
                listy.Add(CommonUtility.getUserRoleName(customData.RoleId));
                if (customData.RoleId == (int)Role.Operator)
                {
                    var usermanager = Users.GetAllFullManagersByUserId(customData.ID, dbConnection);
                    if (usermanager != null)
                    {
                        listy.Add(usermanager.CustomerUName);
                        supervisorId = usermanager.ID;
                    }
                    else
                        listy.Add("Unassigned");
                }
                else if (customData.RoleId == (int)Role.Manager)
                {
                    var getdir = Accounts.GetDirectorByManagerName(customData.Username, dbConnection);
                    if (getdir != null)
                    {
                        listy.Add(getdir.CustomerUName);
                        supervisorId = getdir.ID;
                    }
                    else
                        listy.Add("Unassigned");
                }
                else if (customData.RoleId == (int)Role.UnassignedOperator)
                {
                    listy.Add("Unassigned");
                }
                else
                {
                    listy.Add(" ");
                }
                listy.Add("Group Name");
                listy.Add(customData.Status);
                listy.Add("circle-point " + CommonUtility.getImgUserStatus(customData.Status));
                //var userImg = UserImage.GetUserImageByUserId(customData.ID, dbConnection);
                var imgSrc = CommonUtility.getUserPhotoUrl(customData.ID, dbConnection);// "Images/icon-user-default.png";//images / custom - images / user - 1.png;
                //if (userImg != null)
                //{
                //    var base64 = Convert.ToBase64String(userImg.ImageFile);
                //    imgSrc = String.Format("data:image/png;base64,{0}", base64);
                //}
                var fontstyle = string.Empty;
                if (customData.Active == (int)Arrowlabs.Business.Layer.HealthCheck.DeviceStatus.Online)
                {
                    fontstyle = "style='color:lime;'";
                }
              //  var pushDev = PushNotificationDevice.GetPushNotificationDeviceByUsername(customData.Username, dbConnection);
                var monitor = string.Empty;
                //if (customData.RoleId == (int)Role.Admin || customData.RoleId == (int)Role.Manager || customData.RoleId == (int)Role.Director || customData.RoleId == (int)Role.Regional || customData.RoleId == (int)Role.ChiefOfficer)
                if (customData.RoleId != (int)Role.SuperAdmin)
                {
                  //  if (pushDev != null)
                  //  {
                  //      if (!string.IsNullOrEmpty(pushDev.Username))
                  //      {
                    if (customData.IsServerPortal)
                            {
                                monitor = "<i " + fontstyle + " class='fa fa-laptop fa-2x mr-2x'></i>";
                                fontstyle = "";
                            }
                            else
                            {
                                monitor = "<i class='fa fa-laptop fa-2x mr-2x'></i>";
                            }
                 //       }
                 //       else
                 //       {
                 //           monitor = "<i " + fontstyle + " class='fa fa-laptop fa-2x mr-2x'></i>";
                 //           fontstyle = "";
                   //     }
                 //   }
                }
                listy.Add(imgSrc);
                listy.Add(CommonUtility.getUserDeviceType(customData.DeviceType, fontstyle,monitor));
                listy.Add(CommonUtility.getRoleSupervisor(customData.RoleId));
                listy.Add(customData.FirstName);
                listy.Add(customData.LastName);
                listy.Add(supervisorId.ToString());
                listy.Add(Decrypt.DecryptData(customData.Password, true, dbConnection));
                listy.Add(customData.Latitude.ToString());
                listy.Add(customData.Longitude.ToString());

                var userSiteDisplay = customData == null ? "N/A" : customData.SiteName;
                if (customData.RoleId != (int)Role.Regional)
                {
                    if (customData.SiteId == 0)
                        listy.Add("N/A");
                    else
                        listy.Add(userSiteDisplay);
                }
                else
                {
                    listy.Add("Multiple");
                }



                listy.Add(customData.Gender);
                listy.Add(customData.EmployeeID);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getUserRecentActivity(int id,string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);

            var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

            if (isValidSession == null)
            {
                listy.Add("LOGOUT");
                return listy;
            }

            var getrecenthistory = new List<EventHistory>();

            if(userinfo.RoleId ==(int)Role.SuperAdmin)
                getrecenthistory = EventHistory.GetTopEventHistory(dbConnection);
            else
                getrecenthistory = EventHistory.GetTopEventHistoryBySiteId(userinfo.SiteId, dbConnection);

            var incidentLocation = string.Empty;
            var actioninfo = string.Empty;
            var entryCount = 0;
            var userDetails = Users.GetUserById(id, dbConnection);
            foreach (var rem in getrecenthistory)
            {
                if (entryCount == 10)
                    break;

                if (rem.CreatedBy == userDetails.Username || rem.UserName == userDetails.Username)
                {
                    var gethotevent = CustomEvent.GetCustomEventById(rem.EventId, dbConnection);
                    if (gethotevent != null)
                    {
                        if (gethotevent.EventType == CustomEvent.EventTypes.MobileHotEvent)
                        {
                            gethotevent.Name = gethotevent.EventTypeName;
                            //var mobEv = MobileHotEvent.GetMobileHotEventByGuid(gethotevent.Identifier, dbConnection);
                            //if (mobEv != null)
                            //{
                            //    if (string.IsNullOrEmpty(mobEv.EventTypeName))
                            //        gethotevent.Name = "None";
                            //    else
                            //        gethotevent.Name = mobEv.EventTypeName;

                            //}
                        }

                        var incidentType = gethotevent.Name;


                        if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Pending)
                        {
                            incidentLocation = "from " + ReverseGeocode.RetrieveFormatedAddress(gethotevent.Latitude.ToString(), gethotevent.Longtitude.ToString(), getClientLic);

                            actioninfo = "created";

                            //var recTimeMinusNow = CommonUtility.getDTNow().Subtract(rem.CreatedDate.Value);

                            var jsonstring = "<div class='col-md-2'><p>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p></div><div class='col-md-2'><span class='bullhorn-circle'><i class='fa fa-bullhorn'></i></span></div><div class='col-md-8'><p><span>" + rem.CreatedBy + "</span></br>" + actioninfo + "<span>" + incidentType + "</span></br>" + incidentLocation + "</p></div><div class='vertical-line'></div>";

                            listy.Add(jsonstring);

                            entryCount++;
                        }
                        else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Complete)
                        {
                            actioninfo = "completed";

                            incidentLocation = "from " + ReverseGeocode.RetrieveFormatedAddress(rem.Latitude.ToString(), rem.Longtitude.ToString(), getClientLic);

                            //var recTimeMinusNow = CommonUtility.getDTNow().Subtract(rem.CreatedDate.Value);

                            var jsonstring = "<div class='col-md-2'><p>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p></div><div class='col-md-2'><span class='bullhorn-circle'><i class='fa fa-bullhorn'></i></span></div><div class='col-md-8'><p><span>" + rem.UserName + "</span></br>" + actioninfo + "<span>" + incidentType + "</span>" + incidentLocation + "</p></div><div class='vertical-line'></div>";

                            listy.Add(jsonstring);

                            entryCount++;
                        }
                        else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Dispatch)
                        {
                            actioninfo = "dispatched";

                            incidentLocation = "to " + rem.UserName;

                            //var recTimeMinusNow = CommonUtility.getDTNow().Subtract(rem.CreatedDate.Value);

                            var jsonstring = "<div class='col-md-2'><p>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p></div><div class='col-md-2'><span class='bullhorn-circle'><i class='fa fa-bullhorn'></i></span></div><div class='col-md-8'><p><span>" + rem.CreatedBy + "</span></br>" + actioninfo + "<span>" + incidentType + "</span>" + incidentLocation + "</p></div><div class='vertical-line'></div>";

                            listy.Add(jsonstring);

                            entryCount++;
                        }
                        else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Resolve)
                        {
                            actioninfo = "resolved";

                            //var recTimeMinusNow = CommonUtility.getDTNow().Subtract(rem.CreatedDate.Value);

                            var jsonstring = "<div class='col-md-2'><p>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p></div><div class='col-md-2'><span class='bullhorn-circle'><i class='fa fa-bullhorn'></i></span></div><div class='col-md-8'><p><span>" + rem.CreatedBy + "</span></br>" + actioninfo + "<span>" + incidentType + "</span>" + incidentLocation + "</p></div><div class='vertical-line'></div>";

                            listy.Add(jsonstring);

                            entryCount++;
                        }
                        else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Reject)
                        {
                            actioninfo = "rejected";

                            //var recTimeMinusNow = CommonUtility.getDTNow().Subtract(rem.CreatedDate.Value);

                            var jsonstring = "<div class='col-md-2'><p>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p></div><div class='col-md-2'><span class='bullhorn-circle'><i class='fa fa-bullhorn'></i></span></div><div class='col-md-8'><p><span>" + rem.CreatedBy + "</span></br>" + actioninfo + "<span>" + incidentType + "</span>" + incidentLocation + "</p></div><div class='vertical-line'></div>";

                            listy.Add(jsonstring);

                            entryCount++;
                        }
                        else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Park)
                        {
                            actioninfo = "parked";

                            //var recTimeMinusNow = CommonUtility.getDTNow().Subtract(rem.CreatedDate.Value);

                            var jsonstring = "<div class='col-md-2'><p>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p></div><div class='col-md-2'><span class='bullhorn-circle'><i class='fa fa-bullhorn'></i></span></div><div class='col-md-8'><p><span>" + rem.CreatedBy + "</span></br>" + actioninfo + "<span>" + incidentType + "</span>" + incidentLocation + "</p></div><div class='vertical-line'></div>";

                            listy.Add(jsonstring);

                            entryCount++;
                        }
                        else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Engage)
                        {
                            incidentLocation = "from " + ReverseGeocode.RetrieveFormatedAddress(rem.Latitude.ToString(), rem.Longtitude.ToString(), getClientLic);

                            actioninfo = "engaged";

                            //var recTimeMinusNow = CommonUtility.getDTNow().Subtract(rem.CreatedDate.Value);

                            var jsonstring = "<div class='col-md-2'><p>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p></div><div class='col-md-2'><span class='bullhorn-circle'><i class='fa fa-bullhorn'></i></span></div><div class='col-md-8'><p><span>" + rem.UserName + "</span></br>" + actioninfo + "<span>" + incidentType + "</span></br>" + incidentLocation + "</p></div><div class='vertical-line'></div>";

                            listy.Add(jsonstring);

                            entryCount++;
                        }
                        else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Release)
                        {
                            actioninfo = "released";

                            //var recTimeMinusNow = CommonUtility.getDTNow().Subtract(rem.CreatedDate.Value);

                            var jsonstring = "<div class='col-md-2'><p>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p></div><div class='col-md-2'><span class='bullhorn-circle'><i class='fa fa-bullhorn'></i></span></div><div class='col-md-8'><p><span>" + rem.UserName + "</span></br>" + actioninfo + "<span>" + incidentType + "</span>" + incidentLocation + "</p></div><div class='vertical-line'></div>";

                            listy.Add(jsonstring);

                            entryCount++;
                        }
                    }
                }
            }
            var getMyTasks = UserTask.GetAllInCompleteTasksByAssigneeTypeAndAssigneeIdAndStatus((int)TaskAssigneeType.User, id, (int)TaskStatus.Completed, dbConnection);
            getMyTasks.AddRange(UserTask.GetAllInCompleteTasksByAssigneeTypeAndAssigneeIdAndStatus((int)TaskAssigneeType.User, id, (int)TaskStatus.InProgress, dbConnection));
            getMyTasks.AddRange(UserTask.GetAllInCompleteTasksByAssigneeTypeAndAssigneeIdAndStatus((int)TaskAssigneeType.User, id, (int)TaskStatus.Pending, dbConnection));

            List<UserTask> SortedList = getMyTasks.OrderByDescending(o => o.CreateDate).ToList();

            foreach (var tsk in SortedList)
            {
                if (entryCount == 10)
                    break;

                if (tsk.Status == (int)TaskStatus.Pending)
                {
                    incidentLocation = "to " + ReverseGeocode.RetrieveFormatedAddress(tsk.Latitude.ToString(), tsk.Longitude.ToString(), getClientLic);

                    actioninfo = "got assigned task";

                    var jsonstring = "<div class='col-md-2'><p>" + tsk.StartDate.Value.ToString() + "</p></div><div class='col-md-2'><span class='bullhorn-circle'><i class='fa fa-bullhorn'></i></span></div><div class='col-md-8'><p><span>" + tsk.AssigneeName + "</span></br>" + actioninfo + "<span>" + tsk.Name + "</span>" + incidentLocation + "</p></div><div class='vertical-line'></div>";

                    listy.Add(jsonstring);

                    entryCount++;
                }
                else if (tsk.Status == (int)TaskStatus.InProgress)
                {


                    incidentLocation = "from " + ReverseGeocode.RetrieveFormatedAddress(tsk.StartLatitude.ToString(), tsk.StartLongitude.ToString(), getClientLic);

                    actioninfo = "started progress on";

                    var jsonstring = "<div class='col-md-2'><p>" + tsk.ActualStartDate.Value.ToString() + "</p></div><div class='col-md-2'><span class='bullhorn-circle'><i class='fa fa-bullhorn'></i></span></div><div class='col-md-8'><p><span>" + tsk.AssigneeName + "</span></br>" + actioninfo + "<span>" + tsk.Name + "</span>" + incidentLocation + "</p></div><div class='vertical-line'></div>";

                    listy.Add(jsonstring);

                    entryCount++;

                    incidentLocation = "to " + ReverseGeocode.RetrieveFormatedAddress(tsk.Latitude.ToString(), tsk.Longitude.ToString(), getClientLic);

                    actioninfo = "got assigned task";

                    jsonstring = "<div class='col-md-2'><p>" + tsk.StartDate.Value.ToString() + "</p></div><div class='col-md-2'><span class='bullhorn-circle'><i class='fa fa-bullhorn'></i></span></div><div class='col-md-8'><p><span>" + tsk.AssigneeName + "</span></br>" + actioninfo + "<span>" + tsk.Name + "</span>" + incidentLocation + "</p></div><div class='vertical-line'></div>";

                    listy.Add(jsonstring);

                    entryCount++;
                }
                else if (tsk.Status == (int)TaskStatus.Completed)
                {
                    incidentLocation = "from " + ReverseGeocode.RetrieveFormatedAddress(tsk.EndLatitude.ToString(), tsk.EndLongitude.ToString(), getClientLic);

                    actioninfo = "completed";

                    var jsonstring = "<div class='col-md-2'><p>" + tsk.ActualEndDate.Value.ToString() + "</p></div><div class='col-md-2'><span class='bullhorn-circle'><i class='fa fa-bullhorn'></i></span></div><div class='col-md-8'><p><span>" + tsk.AssigneeName + "</span></br>" + actioninfo + "<span>" + tsk.Name + "</span>" + incidentLocation + "</p></div><div class='vertical-line'></div>";

                    listy.Add(jsonstring);

                    entryCount++;

                    incidentLocation = "from " + ReverseGeocode.RetrieveFormatedAddress(tsk.StartLatitude.ToString(), tsk.StartLongitude.ToString(), getClientLic);

                    actioninfo = "started progress on";

                    jsonstring = "<div class='col-md-2'><p>" + tsk.ActualStartDate.ToString() + "</p></div><div class='col-md-2'><span class='bullhorn-circle'><i class='fa fa-bullhorn'></i></span></div><div class='col-md-8'><p><span>" + tsk.AssigneeName + "</span></br>" + actioninfo + "<span>" + tsk.Name + "</span>" + incidentLocation + "</p></div><div class='vertical-line'></div>";

                    listy.Add(jsonstring);

                    entryCount++;

                    incidentLocation = "to " + ReverseGeocode.RetrieveFormatedAddress(tsk.Latitude.ToString(), tsk.Longitude.ToString(), getClientLic);

                    actioninfo = "got assigned task";

                    jsonstring = "<div class='col-md-2'><p>" + tsk.StartDate.Value.ToString() + "</p></div><div class='col-md-2'><span class='bullhorn-circle'><i class='fa fa-bullhorn'></i></span></div><div class='col-md-8'><p><span>" + tsk.AssigneeName + "</span></br>" + actioninfo + "<span>" + tsk.Name + "</span>" + incidentLocation + "</p></div><div class='vertical-line'></div>";

                    listy.Add(jsonstring);

                    entryCount++;




                }
            }
            return listy;
        }
        protected string GetIPAddress()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }

            return context.Request.ServerVariables["REMOTE_ADDR"];
        }
        protected void forceLogoutButton_Click(object sender, EventArgs e)
        {
            System.Web.Security.FormsAuthentication.SignOut();
            Response.Redirect("~/Default.aspx");
        }
        protected void LogoutButton_Click(object sender, EventArgs e)
        {
            var customData = Users.GetUserByName(User.Identity.Name, dbConnection);
            CommonUtility.LogoutUser(customData, System.Web.HttpContext.Current.Session.SessionID, GetIPAddress());
            System.Web.Security.FormsAuthentication.SignOut();
            Response.Redirect("~/Default.aspx");
        }
        [WebMethod]
        public static List<string> getGroupDataFromUser(int id,string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);

            var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

            if (isValidSession == null)
            {
                listy.Add("LOGOUT");
                return listy;
            }

            var usergroups = GroupUser.GetGroupsByUserId(id, dbConnection);
            foreach (var group in usergroups)
            {
                var groupInfo = Group.GetGroupById(group.GroupId, dbConnection);
                if (groupInfo != null)
                {
                    listy.Add(AddGroupToUserProfileList(groupInfo));
                    listy.Add(groupInfo.Id.ToString());
                }
                //href='#groups-view-tab' onclick='getGroupView(&apos;" + item.Id + "&apos;,&apos;ALL&apos;)' data-toggle='tab'
                //listy.Add(groupInfo.Name);
            }

            return listy;
        }
        private static string AddGroupToUserProfileList(Group item)
        {
            var members = Users.GetAllUserByGroupId(item.Id, dbConnection);
            var colorcode = string.Empty;
            if (item.ColorCode == (int)Reminders.ColorCodes.Green)
                colorcode = "green-column";
            else if (item.ColorCode == (int)Reminders.ColorCodes.Blue)
                colorcode = "blue-column";
            else if (item.ColorCode == (int)Reminders.ColorCodes.Yellow)
                colorcode = "yellow-column";
            else if (item.ColorCode == (int)Reminders.ColorCodes.Red)
                colorcode = "red-column";

            var retstring = "<div class='inline-block mr-2x inherited-vertical-align'><span class='" + colorcode + " column-diemention extra-height'></span></div><div class='inline-block mr-2x inherited-vertical-align'><h4 class='no-margin-top blue-color'>" + item.Name + "</h4><h6 class='mb-4x' id='" + item.Name + "-" + item.Id + "'>MEMBERS(" + members.Count + ")</h6><a href='#groups-view-tab' onclick='getGroupView(&apos;" + item.Id + "&apos;,&apos;ALL&apos;)' data-toggle='tab' class='light-gray mr-2x'><i class='fa fa-eye mr-1x'></i>View</a></div>";

            return retstring;
        }
        [WebMethod]
        public static List<string> getReminderSelectorDates(int id)
        {
            var listy = new List<string>();
            // Set the start time (00:00 means 12:00 AM)
            DateTime StartTime = CommonUtility.getDTNow().Date.Add(new TimeSpan(7, 0, 0));
            // Set the end time (23:55 means 11:55 PM)
            DateTime EndTime = CommonUtility.getDTNow().Date.Add(new TimeSpan(17, 0, 0));
            //Set 5 minutes interval
            TimeSpan Interval = new TimeSpan(1, 0, 0);
            //To set 1 hour interval
            //TimeSpan Interval = new TimeSpan(1, 0, 0);           

            while (StartTime <= EndTime)
            {
                listy.Add(StartTime.ToString("HH:mm"));
                StartTime = StartTime.Add(Interval);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getDateRangeReminder(string date)
        {
            var listy = new List<string>();
            var split = date.Split(':');
            // Set the start time (00:00 means 12:00 AM)
            DateTime StartTime = CommonUtility.getDTNow().Date.Add(new TimeSpan(Convert.ToInt32(split[0]), 0, 0));
            // Set the end time (23:55 means 11:55 PM)
            DateTime EndTime = CommonUtility.getDTNow().Date.Add(new TimeSpan(18, 0, 0));
            //Set 5 minutes interval
            TimeSpan Interval = new TimeSpan(1, 0, 0);
            //To set 1 hour interval
            //TimeSpan Interval = new TimeSpan(1, 0, 0);           
            //fromReminderDate.Items.Clear();
            //toReminderDate.Items.Clear();
            while (StartTime <= EndTime)
            {
                listy.Add(StartTime.ToString("HH:mm"));
                StartTime = StartTime.Add(Interval);
            }
            return listy;
        }
        // Task
        [WebMethod]
        public static string taskgetAttachmentDataIcons(int id, string uname)
        {
            var listy = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                var attachments = UserTaskAttachment.GetTaskAttachmentsByTaskId(Convert.ToInt32(id), dbConnection);
                var i = 1;
                if (attachments.Count > 0)
                {
                    foreach (var item in attachments)
                    {
                        if (!string.IsNullOrEmpty(item.DocumentPath))
                        {
                            if (i == 4)
                            {
                                var retstring = "<img src='../images/more.png' data-toggle='tab' data-target='#taskattachments-tab' onclick='hideTaskplay()'/>";
                                listy += retstring;
                                i++;
                                break;
                            }
                            else
                            {
                                if (VideoExtensions.Contains(System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant()))
                                {
                                    var retstring = "<img src='../images/VLCMediaPlayer1.png' data-toggle='tab' onclick='play(" + i + ")' data-target='#video-" + i + "-tab'/>";
                                    listy += retstring;
                                    i++;
                                }
                                else if (System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant() == ".MP3")
                                {
                                    if (item.ChecklistId == 0)
                                    {
                                        //var retstring = "<img src='../images/VLCMediaPlayer1.png' data-toggle='tab' onclick='play(" + i + ")' data-target='#video-" + i + "-tab'/>";
                                        var retstring = "<img src='../images/music-note.png' data-toggle='tab' data-target='#tasklocation-tab' onclick='audiotaskplay(&apos;" + item.DocumentPath + "&apos;)'/>";

                                        listy += retstring;
                                        i++;
                                    }
                                }
                                else if (System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant() == ".PDF")
                                {

                                }
                                else
                                {
                                    //var mimssettings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                                    //var imgstring = String.Format(mimssettings.MIMSMobileAddress + "/Uploads/Tasks/" + id + "/{0}", System.IO.Path.GetFileName(item.DocumentPath));
                                    var retstring = "<img src='" + item.DocumentPath + "' data-toggle='tab' data-target='#image-" + i + "-tab'/>";
                                    listy += retstring;
                                    i++;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception er)
            {
                MIMSLog.MIMSLogSave("Incident", "taskgetAttachmentDataIcons", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static List<string> taskgetAttachmentData(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var attachments = UserTaskAttachment.GetTaskAttachmentsByTaskId(Convert.ToInt32(id), dbConnection);
                var i = 1;
                if (attachments.Count > 0)
                {
                    foreach (var item in attachments)
                    {
                        if (!string.IsNullOrEmpty(item.DocumentPath))
                        {
                            //var mimssettings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                            if (VideoExtensions.Contains(System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant()))
                            {
                                var retstring = "<video id='Video" + i + " width='100%' height='380px' muted controls ><source src='" + item.DocumentPath + "' /></video>";
                                listy.Add(retstring);
                                i++;
                            }
                            else if (System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant() == ".MP3")
                            {
                                //if (item.ChecklistId == 0)
                                //{
                                //    var retstring = "<audio id='Video" + i + "' width='100%' height='380px' controls ><source src='" + item.DocumentPath + "' type='audio/mpeg' /></audio>";
                                //    listy.Add(retstring);
                                //    i++;
                                //}
                            }
                            else if (System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant() == ".PDF")
                            {

                            }
                            else
                            {
                                //var imgstring = String.Format(mimssettings.MIMSMobileAddress + "/Uploads/Tasks/" + id + "/{0}", System.IO.Path.GetFileName(item.DocumentPath));
                                var retstring = "<img onclick='rotateMe(this);' src='" + item.DocumentPath + "' class='resized-filled-image' onload='loadMe(this);'/>";
                                listy.Add(retstring);
                                i++;
                            }
                        }
                    }
                }
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("Incident", "taskgetAttachmentData", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static string taskgetAttachmentDataTab(int id, string uname)
        {
            var listy = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var attachments = UserTaskAttachment.GetTaskAttachmentsByTaskId(Convert.ToInt32(id), dbConnection);
                listy += "<div class='row static-height-with-border clickable-row' data-toggle='tab' onclick='hideTaskplay()' data-target='#tasklocation-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-map-marker fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Task Location</p></div></div></div>";
                var i = 1;
                var iTab = 1;
                if (attachments.Count > 0)
                {
                    foreach (var item in attachments)
                    {
                        if (!string.IsNullOrEmpty(item.DocumentPath))
                        {
                            if (VideoExtensions.Contains(System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant()))
                            {
                                var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' onclick='hideTaskplay();play(" + iTab + ")' data-target='#video-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-play-circle-o fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                listy += retstring;
                                iTab++;
                            }
                            else if (System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant() == ".MP3")
                            {
                                if (item.ChecklistId == 0)
                                {
                                    //var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' onclick='play(" + iTab + ")' data-target='#video-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-play-circle-o fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                    var retstring = "<div class='row static-height-with-border clickable-row' data-toggle='tab' data-target='#tasklocation-tab' onclick='audiotaskplay(&apos;" + item.DocumentPath + "&apos;)' ><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-music fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>Attachment " + i + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";

                                    listy += retstring;
                                    iTab++;
                                }
                            }
                            else if (System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant() == ".PDF")
                            {

                                var imgstring = String.Format(item.DocumentPath);

                                var attachmentName = CommonUtility.getAttachmentDisplayName(item.DocumentPath, i);

                                var retstringExtra = "<div class='row static-height-with-border clickable-row' onclick='hideTaskplay()'><div class='col-md-12'><div onclick='window.open(&apos;" + imgstring + "&apos;);' class='inline-block mr-2x'><i class='fa fa-file-pdf-o fa-2x gray-bg red-color'></i></div><div class='inline-block' onclick='window.open(&apos;" + imgstring + "&apos;);'><p>" + attachmentName + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                listy += retstringExtra;

                            }
                            else
                            {
                                var attachmentName = CommonUtility.getAttachmentDisplayName(item.DocumentPath, i);

                                var retstring = "<div class='row static-height-with-border clickable-row' onclick='hideTaskplay()' data-toggle='tab' data-target='#image-" + iTab + "-tab'><div class='col-md-12'><div class='inline-block mr-2x'><i class='fa fa-image fa-2x gray-bg red-color'></i></div><div class='inline-block'><p>" + attachmentName + "</p></div><div style='float:right' class='inline-block'><i style='margin-top:3px;color:#b2163b;' class='fa fa-close fa-1x' onclick='deleteAttachmentChoiceAsset(&apos;" + item.Id + "&apos;)'></i></div></div></div>";
                                listy += retstring;
                                iTab++;
                            }
                            i++;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Tasks", "getAttachmentDataTab", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> taskgetChecklistData(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var chklisty = new List<string>();
                var isAllChecked = true;
                var sessions = TaskCheckList.GetTaskCheckListItemsByTaskId(Convert.ToInt32(id), dbConnection);
                var mimssettings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);


                var groupedlist = new List<TaskCheckList>();
                var groupedItems = sessions.GroupBy(x => x.Id);
                var groupedlistItems = groupedItems.Select(grp => grp.OrderBy(x => x.Id).First()).ToList();

                foreach (var child in groupedlistItems)
                {
                    var attachfiles = sessions.Where(i => i.Id == child.Id).ToList();
                    var mp3files = attachfiles.Where(i => !string.IsNullOrEmpty(i.DocumentPath) && System.IO.Path.GetExtension(i.DocumentPath).ToUpperInvariant() == ".MP3").ToList();
                    if (mp3files.Count > 0)
                    {
                        //Yes
                        groupedlist.Add(mp3files[0]);
                    }
                    else
                    {
                        groupedlist.Add(attachfiles[0]);
                    }
                }

                foreach (var item in groupedlist)
                {
                    var stringCheck = string.Empty;
                    if (item.IsChecked)
                        stringCheck = "Checked";
                    else
                    {
                        stringCheck = "Unchecked";
                        isAllChecked = false;
                    }
                    var imgsrc = string.Empty;
                    if (!string.IsNullOrEmpty(item.DocumentPath) && System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant() == ".MP3")
                    {

                        imgsrc = "<i style='margin-left:5px;' onmouseover='style=&apos;cursor: pointer;margin-left:5px;&apos;' onclick='audiotaskplay(&apos;" + item.DocumentPath + "&apos;)' class='fa fa-music'></i>";
                    }

                    if (item.CheckListItemType == 4 || item.CheckListItemType == 5)
                    {
                        chklisty.Add(item.Name + "|" + stringCheck + "|True|" + imgsrc);
                    }
                    else if (item.CheckListItemType == 3)
                    {
                        if (!string.IsNullOrEmpty(item.TemplateCheckListItemNote))
                            chklisty.Add(item.Name + "|" + stringCheck + "|3|" + item.TemplateCheckListItemNote + "|" + imgsrc);
                        else
                            chklisty.Add(item.Name + "|" + stringCheck + "|False|" + imgsrc);
                    }
                    else
                    {
                        chklisty.Add(item.Name + "|" + stringCheck + "|False|" + imgsrc);
                    }
                    if (item.ChildCheckList != null)
                    {
                        //var audiofiles = item.ChildCheckList.Where(i => System.IO.Path.GetExtension(i.DocumentPath).ToUpperInvariant() == ".MP3").ToList();
                        //var glist = item.ChildCheckList.Where(i => System.IO.Path.GetExtension(i.DocumentPath).ToUpperInvariant() != ".MP3").ToList();

                        var childgroupedlist = new List<TaskCheckList>();
                        var childgroupedItems = item.ChildCheckList.GroupBy(x => x.Id);
                        var childgroupedlistItems = childgroupedItems.Select(grp => grp.OrderBy(x => x.Id).First()).ToList();

                        foreach (var child in childgroupedlistItems)
                        {
                            var attachfiles = item.ChildCheckList.Where(i => i.Id == child.Id).ToList();
                            var mp3files = attachfiles.Where(i => !string.IsNullOrEmpty(i.DocumentPath) && System.IO.Path.GetExtension(i.DocumentPath).ToUpperInvariant() == ".MP3").ToList();
                            if (mp3files.Count > 0)
                            {
                                //Yes
                                childgroupedlist.Add(mp3files[0]);
                            }
                            else
                            {
                                childgroupedlist.Add(attachfiles[0]);
                            }
                        }

                        foreach (var child in childgroupedlist)
                        {
                            var imgsrc2 = string.Empty;
                            if (!string.IsNullOrEmpty(child.DocumentPath) && System.IO.Path.GetExtension(child.DocumentPath).ToUpperInvariant() == ".MP3")
                            {

                                imgsrc2 = "<i style='margin-left:5px;' onmouseover='style=&apos;cursor: pointer;margin-left:5px;&apos;' onclick='audiotaskplay(&apos;" + child.DocumentPath + "&apos;)' class='fa fa-music'></i>";
                            }

                            if (child.IsChecked)
                                stringCheck = "Checked";
                            else
                            {
                                stringCheck = "Unchecked";
                                isAllChecked = false;
                            }
                            chklisty.Add(child.Name + "|" + stringCheck + "|False|" + imgsrc2);
                        }
                    }
                }
                listy.Add(isAllChecked.ToString());
                listy.AddRange(chklisty);
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Tasks", "getChecklistData", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getChecklistNotesData(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var sessions = TaskCheckList.GetTaskCheckListItemsByTaskId(Convert.ToInt32(id), dbConnection);
                foreach (var item in sessions)
                {
                    if (item.CheckListItemType == 5)
                    {
                        if (item.ChildCheckList != null)
                        {
                            foreach (var child in item.ChildCheckList)
                            {
                                if (!child.IsChecked)
                                    listy.Add(child.Name + "|" + child.TemplateCheckListItemNote);
                            }
                        }
                    }
                    else
                    {
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Incidents", "getChecklistNotesData", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getCanvasNotesData(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }
                var sessions = UserTaskAttachment.GetTaskAttachmentsByTaskId(Convert.ToInt32(id), dbConnection);
                var count = 1;
                foreach (var item in sessions)
                {
                    if (!string.IsNullOrEmpty(item.DocumentPath))
                    {
                        if (item.ImageNote != "Task Attachment" && !string.IsNullOrEmpty(item.ImageNote))
                        {
                            listy.Add("Attachment " + count + "|" + item.ImageNote);
                        }
                        count++;    //listy.Add(child.Name + "|" + child.TemplateCheckListItemNote);
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Incidents", "getCanvasNotesData", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getTableRowDataTask(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var customData = UserTask.GetTaskById(Convert.ToInt32(id), dbConnection);

                if (customData != null)
                {
                    listy.Add(customData.CustomerUName);
                    listy.Add(customData.CreateDate.Value.AddHours(userinfo.TimeZone).ToString());
                    listy.Add(customData.ACustomerUName);
                    listy.Add(customData.StatusDescription);
                    var geolocation = ReverseGeocode.RetrieveFormatedAddress(customData.Latitude.ToString(), customData.Longitude.ToString(), getClientLic);
                    listy.Add(geolocation);
                    listy.Add(customData.Name);
                    listy.Add(customData.Name);
                    listy.Add(customData.Name);
                    listy.Add(customData.Description);
                    listy.Add(customData.Notes);
                    listy.Add(customData.Name);
                    listy.Add(customData.StartDate.Value.AddHours(userinfo.TimeZone).ToString());
                    listy.Add(customData.CheckListNotes);
                    listy.Add("circle-point " + CommonUtility.getImgStatus(customData.StatusDescription));
                    var parentTasks = TemplateCheckList.GetAllTemplateCheckListById(customData.TemplateCheckListId.ToString(), dbConnection);
                    if (parentTasks != null)
                        listy.Add(parentTasks.Name);
                    else
                        listy.Add("None");

                    listy.Add(customData.TaskTypeName);

                    if (customData.IncidentId > 0)
                    {
                        var getCus = CustomEvent.GetCustomEventById(customData.IncidentId, dbConnection);
                        if (getCus != null)
                        {
                            if (getCus.EventType == CustomEvent.EventTypes.MobileHotEvent)
                            {
                                var mobEv = MobileHotEvent.GetMobileHotEventByGuid(getCus.Identifier, dbConnection);
                                if (mobEv != null)
                                {
                                    if (string.IsNullOrEmpty(mobEv.EventTypeName))
                                    {

                                    }
                                    else
                                        getCus.Name = mobEv.EventTypeName;
                                }
                            }
                            else if (getCus.EventType == CustomEvent.EventTypes.HotEvent || getCus.EventType == CustomEvent.EventTypes.Request)
                            {
                                var reqEv = HotEvent.GetHotEventById(getCus.Identifier, dbConnection);
                                if (reqEv != null)
                                {
                                    if (CommonUtility.getPCName(reqEv) != "MIMS MOBILE")
                                        getCus.UserName = CommonUtility.getPCName(reqEv);

                                    if (getCus.EventType == CustomEvent.EventTypes.Request)
                                    {
                                        var splitName = reqEv.Name.Split('^');
                                        if (splitName.Length > 1)
                                            getCus.Name = splitName[1];
                                    }
                                }
                            }
                            else if (getCus.EventType == CustomEvent.EventTypes.DriverOffence)
                            {
                                var doffence = DriverOffence.GetDriverOffenceById(getCus.Identifier, dbConnection);
                                if (doffence != null)
                                {
                                    getCus.Name = doffence.OffenceCategory;
                                }
                            }
                            listy.Add(getCus.Name + "|" + customData.IncidentId);


                        }
                        else
                            listy.Add("None");
                    }
                    else
                    {
                        if (customData.TaskLinkId > 0)
                        {
                            var gt = UserTask.GetTaskById(customData.TaskLinkId, dbConnection);
                            if (gt != null)
                            {
                                if (gt.IncidentId > 0)
                                {
                                    var getCus2 = CustomEvent.GetCustomEventById(customData.IncidentId, dbConnection);
                                    if (getCus2 != null)
                                    {
                                        if (getCus2.EventType == CustomEvent.EventTypes.MobileHotEvent)
                                        {
                                            var mobEv = MobileHotEvent.GetMobileHotEventByGuid(getCus2.Identifier, dbConnection);
                                            if (mobEv != null)
                                            {
                                                if (string.IsNullOrEmpty(mobEv.EventTypeName))
                                                {

                                                }
                                                else
                                                    getCus2.Name = mobEv.EventTypeName;
                                            }
                                        }
                                        else if (getCus2.EventType == CustomEvent.EventTypes.HotEvent || getCus2.EventType == CustomEvent.EventTypes.Request)
                                        {
                                            var reqEv = HotEvent.GetHotEventById(getCus2.Identifier, dbConnection);
                                            if (reqEv != null)
                                            {
                                                if (CommonUtility.getPCName(reqEv) != "MIMS MOBILE")
                                                    getCus2.UserName = CommonUtility.getPCName(reqEv);

                                                if (getCus2.EventType == CustomEvent.EventTypes.Request)
                                                {
                                                    var splitName = reqEv.Name.Split('^');
                                                    if (splitName.Length > 1)
                                                        getCus2.Name = splitName[1];
                                                }
                                            }
                                        }
                                        else if (getCus2.EventType == CustomEvent.EventTypes.DriverOffence)
                                        {
                                            var doffence = DriverOffence.GetDriverOffenceById(getCus2.Identifier, dbConnection);
                                            if (doffence != null)
                                            {
                                                getCus2.Name = doffence.OffenceCategory;
                                            }
                                        }
                                        listy.Add(getCus2.Name + "|" + customData.IncidentId);
                                    }
                                    else
                                    {
                                        listy.Add("None");
                                    }
                                }
                                else
                                {
                                    listy.Add("None");
                                }
                            }
                            else
                            {
                                listy.Add("None");
                            }
                        }
                        else
                            listy.Add("None");
                    }

                    listy.Add(string.IsNullOrEmpty(customData.ClientName) ? "N/A" : customData.ClientName);
                    listy.Add(string.IsNullOrEmpty(customData.ProjectName) ? "N/A" : customData.ProjectName);
                    listy.Add(string.IsNullOrEmpty(customData.ContractName) ? "N/A" : customData.ContractName);

                }
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("Incident", "getTableRowDataTask", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getTaskHistoryData(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var eventData = UserTask.GetTaskById(Convert.ToInt32(id), dbConnection);//EventHistory.GetEventHistoryByEventId(Convert.ToInt32(id), dbConnection); //new List<CustomEvent>();
                var taskeventhistory = TaskEventHistory.GetTaskEventHistoryByTaskId(eventData.Id, dbConnection);

                var tasklinks = UserTask.GetAllTasksByTaskLinkId(Convert.ToInt32(id), dbConnection);
                if (tasklinks.Count > 0)
                {
                    foreach (var link in tasklinks)
                    {
                        var forlink = TaskEventHistory.GetTaskEventHistoryByTaskId(link.Id, dbConnection);
                        foreach (var forl in forlink)
                        {
                            forl.isSubTask = true;
                            taskeventhistory.Add(forl);
                        }
                    }
                    taskeventhistory = taskeventhistory.OrderByDescending(i => i.CreatedDate).ToList();
                }

                var completedBy = string.Empty;
                var inprogressby = string.Empty;
                var acceptedData = string.Empty;
                var rejectedData = string.Empty;
                var assignedData = string.Empty;
                foreach (var task in taskeventhistory)
                {
                    var taskn = "Task";
                    if (task.isSubTask)
                    {
                        taskn = "Sub-task";
                    }
                    if (eventData.AssigneeType == (int)TaskAssigneeType.Group)
                    {
                        if (task.Action == (int)TaskAction.Update)
                        {
                            acceptedData = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                                + "</p><p><span class='red-color'>" + task.CustomerUName + "</span> updated<span class='red-color'>" + taskn
                                + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                            listy.Add(acceptedData);
                        }
                    }

                    if (task.Action == (int)TaskAction.Complete)
                    {
                        completedBy = task.CustomerUName;
                        var compLocation = string.Empty;
                        var geoLoc = ReverseGeocode.RetrieveFormatedAddress(eventData.EndLatitude.ToString(), eventData.EndLongitude.ToString(), getClientLic);
                        if (!string.IsNullOrEmpty(geoLoc))
                            compLocation = " at " + geoLoc;
                        var compInfo = "completed";
                        var compstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                            + "</p><p><span class='red-color'>" + completedBy + "</span>" + compInfo + "<span class='red-color'>" + taskn
                            + "</span>" + compLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(compstring);
                    }
                    else if (task.Action == (int)TaskAction.InProgress)
                    {
                        inprogressby = task.CustomerUName;
                        var pendingLocation = string.Empty;
                        var geoLoc2 = ReverseGeocode.RetrieveFormatedAddress(eventData.StartLatitude.ToString(), eventData.StartLongitude.ToString(), getClientLic);
                        if (!string.IsNullOrEmpty(geoLoc2))
                            pendingLocation = " at " + geoLoc2;
                        var pendingInfo = "started";
                        var returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                            + "</p><p><span class='red-color'>" + inprogressby + "</span>" + pendingInfo + "<span class='red-color'>" + taskn
                            + "</span>" + pendingLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (task.Action == (int)TaskAction.OnRoute)
                    {
                        inprogressby = task.CustomerUName;
                        var pendingLocation = string.Empty;
                        var geoLoc2 = ReverseGeocode.RetrieveFormatedAddress(eventData.StartLatitude.ToString(), eventData.StartLongitude.ToString(), getClientLic);
                        if (!string.IsNullOrEmpty(geoLoc2))
                            pendingLocation = " at " + geoLoc2;
                        var pendingInfo = "on route";
                        var returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                            + "</p><p><span class='red-color'>" + inprogressby + "</span>" + pendingInfo + "<span class='red-color'>" + taskn
                            + "</span>" + pendingLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (task.Action == (int)TaskAction.Pause)
                    {
                        inprogressby = task.CustomerUName;
                        var pendingLocation = string.Empty;
                        var geoLoc2 = ReverseGeocode.RetrieveFormatedAddress(eventData.StartLatitude.ToString(), eventData.StartLongitude.ToString(), getClientLic);
                        if (!string.IsNullOrEmpty(geoLoc2))
                            pendingLocation = " at " + geoLoc2;
                        var pendingInfo = "paused";
                        var returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                            + "</p><p><span class='red-color'>" + inprogressby + "</span>" + pendingInfo + "<span class='red-color'>" + taskn
                            + "</span>" + pendingLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (task.Action == (int)TaskAction.Resume)
                    {
                        inprogressby = task.CustomerUName;
                        var pendingLocation = string.Empty;
                        var geoLoc2 = ReverseGeocode.RetrieveFormatedAddress(eventData.StartLatitude.ToString(), eventData.StartLongitude.ToString(), getClientLic);
                        if (!string.IsNullOrEmpty(geoLoc2))
                            pendingLocation = " at " + geoLoc2;
                        var pendingInfo = "resumed";
                        var returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                            + "</p><p><span class='red-color'>" + inprogressby + "</span>" + pendingInfo + "<span class='red-color'>" + taskn
                            + "</span>" + pendingLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (task.Action == (int)TaskAction.Accepted)
                    {
                        acceptedData = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                            + "</p><p><span class='red-color'>" + task.CustomerUName + "</span> accepted<span class='red-color'>" + taskn
                            + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(acceptedData);
                    }
                    else if (task.Action == (int)TaskAction.Rejected)
                    {
                        rejectedData = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                            + "</p><p><span class='red-color'>" + task.CustomerUName + "</span> rejected<span class='red-color'>" + taskn
                            + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(rejectedData);
                    }
                    else if (task.Action == (int)TaskAction.Assigned)
                    {
                        assignedData = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                            + "</p><p><span class='red-color'>" + task.CustomerUName + "</span> assigned<span class='red-color'>" + taskn
                            + "</span> to " + task.ACustomerUName + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(assignedData);
                    }
                    else if (task.Action == (int)TaskAction.Pending)
                    {
                        var incidentLocation = string.Empty;
                        var geoLoc3 = ReverseGeocode.RetrieveFormatedAddress(eventData.Latitude.ToString(), eventData.Longitude.ToString(), getClientLic);
                        if (!string.IsNullOrEmpty(geoLoc3))
                            incidentLocation = " at " + geoLoc3;
                        var actioninfo = "created";

                        var gUser = Users.GetUserByName(eventData.CreatedBy, dbConnection);

                        var dataString = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString()
                            + "</p><p><span class='red-color'>" + gUser.CustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + taskn
                            + "</span>for " + task.ACustomerUName + " " + incidentLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(dataString);
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Ticketing", "getTaskHistoryData", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static string UpdateTicketStatus(int id, string status, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var json = string.Empty;
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }
                var cusEv = CustomEvent.GetCustomEventById(id, dbConnection);
                if (cusEv != null)
                {
                    if (!string.IsNullOrEmpty(status))
                    {
                        if (status.ToUpper() == "START")
                        {


                            //cusEv.IncidentStatus = CommonUtility.getIncidentStatusValue("dispatch");
                            //cusEv.Handled = false;
                            ////incidentinfo.SiteId = userinfo.SiteId;
                            //cusEv.SessionId = System.Web.HttpContext.Current.Session.SessionID;
                            //cusEv.Password = Encrypt.EncryptData(userinfo.Password, true, dbConnection);
                            //var returnV = CommonUtility.CreateIncident(cusEv);
                            //if (returnV != "")
                            //{
                            DriverOffence.UpdateStartDriverOffence(cusEv.Identifier, CommonUtility.getDTNow(), userinfo.Username, dbConnection);
                            var EventHistoryEntry = new EventHistory();
                            EventHistoryEntry.CreatedDate = CommonUtility.getDTNow();
                            EventHistoryEntry.CreatedBy = userinfo.Username;
                            EventHistoryEntry.EventId = id;
                            EventHistoryEntry.IncidentAction = (int)CustomEvent.IncidentActionStatus.Engage;
                            EventHistoryEntry.UserName = userinfo.Username;
                            //EventHistoryEntry.Remarks = ins;
                            EventHistoryEntry.SiteId = userinfo.SiteId;
                            EventHistoryEntry.CustomerId = userinfo.CustomerInfoId;
                            EventHistory.InsertEventHistory(EventHistoryEntry, dbConnection, dbConnectionAudit, true);
                            json = "Successfully started work on ticket";
                            //}
                        }
                        else if (status.ToUpper() == "END")
                        {
                            //cusEv.IncidentStatus = CommonUtility.getIncidentStatusValue("complete");
                            //cusEv.Handled = false;
                            ////incidentinfo.SiteId = userinfo.SiteId;
                            //cusEv.SessionId = System.Web.HttpContext.Current.Session.SessionID;
                            //cusEv.Password = Encrypt.EncryptData(userinfo.Password, true, dbConnection);
                            //var returnV = CommonUtility.CreateIncident(cusEv);
                            //if (returnV != "")
                            //{
                            DriverOffence.UpdateEndDriverOffence(cusEv.Identifier, CommonUtility.getDTNow(), userinfo.Username, dbConnection);
                            var EventHistoryEntry = new EventHistory();
                            EventHistoryEntry.CreatedDate = CommonUtility.getDTNow();
                            EventHistoryEntry.CreatedBy = userinfo.Username;
                            EventHistoryEntry.EventId = id;
                            EventHistoryEntry.IncidentAction = (int)CustomEvent.IncidentActionStatus.Complete;
                            EventHistoryEntry.UserName = userinfo.Username;
                            //EventHistoryEntry.Remarks = ins;
                            EventHistoryEntry.SiteId = userinfo.SiteId;
                            EventHistoryEntry.CustomerId = userinfo.CustomerInfoId;
                            EventHistory.InsertEventHistory(EventHistoryEntry, dbConnection, dbConnectionAudit, true);
                            json = "Successfully ended work on ticket";
                            var gCus = CustomerInfo.GetCustomerInfoById(userinfo.CustomerInfoId, dbConnection);

                            if (gCus != null && gCus.SendEmail)
                            {
                                var projinfo = DriverOffence.GetDriverOffenceById(cusEv.Identifier, dbConnection);
                                if (projinfo != null)
                                {
                                    try
                                    {
                                        CommonUtility.sendEmailNotification(projinfo.UserName, projinfo.CustomerFullName, "Ticket", userinfo.FirstName + " " + userinfo.LastName, projinfo.EndDate.AddHours(userinfo.TimeZone).ToString(), projinfo.OffenceCategory, cusEv.CustomerIncidentId, projinfo.StartDate.AddHours(userinfo.TimeZone).ToString(), "End",userinfo);
                                    }
                                    catch (Exception ex)
                                    {
                                        MIMSLog.MIMSLogSave("Ticketing", "UpdateTicketStatus-sendEmailNotification", ex, dbConnection, userinfo.SiteId);
                                    }
                                }
                            }
                            //}
                        }
                    }
                }
                else
                    json = "Problem was faced trying to attach file.";
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Ticketing", "UpdateTicketStatus", err, dbConnection, userinfo.SiteId);
                json = err.Message;
            }
            return json;
        }
        [WebMethod]
        public static List<string> getChecklistData(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var customData = CustomEvent.GetCustomEventById(id, dbConnection);
                if (customData != null)
                {
                    var offenceinfo = DriverOffence.GetDriverOffenceById(customData.Identifier, dbConnection);
                    if (!string.IsNullOrEmpty(offenceinfo.Offence1))
                        listy.Add(offenceinfo.Offence1);
                    if (!string.IsNullOrEmpty(offenceinfo.Offence2))
                        listy.Add(offenceinfo.Offence2);
                    if (!string.IsNullOrEmpty(offenceinfo.Offence3))
                        listy.Add(offenceinfo.Offence3);
                    if (!string.IsNullOrEmpty(offenceinfo.Offence4))
                        listy.Add(offenceinfo.Offence4);
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("OthersDash", "getChecklistData", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> handleTicket(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                listy.Add("LOGOUT");
            }
            else
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        listy.Add("LOGOUT");
                        return listy;
                    }

                    var cusData = CustomEvent.GetCustomEventById(id, dbConnection);
                    cusData.Handled = true;
                    cusData.HandledBy = userinfo.Username;
                    cusData.HandledTime = CommonUtility.getDTNow();
                    cusData.UpdatedBy = userinfo.Username;
                    cusData.IncidentStatus = CommonUtility.getIncidentStatusValue("resolve");
                    CustomEvent.InsertCustomEvent(cusData, dbConnection, dbConnectionAudit, true);
                    CustomEvent.CustomEventHandledById(id, true, userinfo.Username, dbConnection);

                    var EventHistoryEntry = new EventHistory();
                    EventHistoryEntry.CreatedDate = CommonUtility.getDTNow();
                    EventHistoryEntry.CreatedBy = userinfo.Username;
                    EventHistoryEntry.EventId = id;
                    EventHistoryEntry.IncidentAction = (int)CustomEvent.IncidentActionStatus.Resolve;
                    EventHistoryEntry.UserName = userinfo.Username;
                    //EventHistoryEntry.Remarks = ins;
                    EventHistoryEntry.SiteId = userinfo.SiteId;
                    EventHistoryEntry.CustomerId = userinfo.CustomerInfoId;
                    EventHistory.InsertEventHistory(EventHistoryEntry, dbConnection, dbConnectionAudit, true);

                    SystemLogger.SaveSystemLog(dbConnectionAudit, "Ticketing", id.ToString(), id.ToString(), userinfo, "Handled Ticket" + id);

                    var alltasks = UserTask.GetAllTaskByIncidentId(cusData.EventId, dbConnection);
                    foreach (var tsk in alltasks)
                    {
                        if (tsk.Status == (int)TaskStatus.Pending || tsk.Status == (int)TaskStatus.InProgress || tsk.Status == (int)TaskStatus.Completed)
                        {
                            var tskhistory = new TaskEventHistory();
                            tsk.Status = (int)TaskStatus.Accepted;
                            tskhistory.Action = (int)TaskAction.Accepted;
                            tskhistory.CreatedBy = userinfo.Username;
                            tskhistory.CreatedDate = CommonUtility.getDTNow();
                            tskhistory.TaskId = tsk.Id;
                            tskhistory.SiteId = userinfo.SiteId;
                            tskhistory.CustomerId = userinfo.CustomerInfoId;
                            TaskEventHistory.InsertTaskEventHistory(tskhistory, dbConnection, dbConnectionAudit, true);

                            if (tsk.TaskLinkId == 0)
                            {
                                var gtsklinks = UserTask.GetAllTasksByTaskLinkId(tsk.Id, dbConnection);
                                if (gtsklinks.Count > 0)
                                {
                                    foreach (var links in gtsklinks)
                                    {
                                        if (tsk.Status == (int)TaskStatus.Pending || tsk.Status == (int)TaskStatus.InProgress || tsk.Status == (int)TaskStatus.Completed)
                                        {
                                            links.Status = (int)TaskStatus.Accepted;
                                            links.UpdatedDate = CommonUtility.getDTNow();
                                            if (userinfo != null)
                                                links.UpdatedBy = userinfo.Username;
                                            UserTask.InsertorUpdateTask(links, dbConnection, dbConnectionAudit, true);

                                            var ltskhistory = new TaskEventHistory();
                                            ltskhistory.Action = (int)TaskAction.Accepted;
                                            ltskhistory.CreatedBy = userinfo.Username;
                                            ltskhistory.CreatedDate = CommonUtility.getDTNow();
                                            ltskhistory.TaskId = links.Id;
                                            tskhistory.CustomerId = userinfo.CustomerInfoId;
                                            ltskhistory.SiteId = userinfo.SiteId;
                                            TaskEventHistory.InsertTaskEventHistory(ltskhistory, dbConnection, dbConnectionAudit, true);
                                        }
                                    }
                                }
                            }
                            tsk.UpdatedDate = CommonUtility.getDTNow();
                            if (userinfo != null)
                                tsk.UpdatedBy = userinfo.Username;

                            var tId = UserTask.InsertorUpdateTask(tsk, dbConnection, dbConnectionAudit, true);

                            SystemLogger.SaveSystemLog(dbConnectionAudit, "Ticketing", tsk.Id.ToString(), tsk.Id.ToString(), userinfo, "Handled Tasks pertaining to ticket-" + id);

                        }
                    }
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("Ticketing", "handleTicket", ex, dbConnection, userinfo.SiteId);
                    listy.Add(ex.Message);
                }
                listy.Add("SUCCESS");

            }
            return listy;
        }
        [WebMethod]
        public static List<string> getTaskListData(int id, string uname)
        {

            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }
                var chklisty = new List<string>();
                var usertask = UserTask.GetAllTasksByIncidentId(id, dbConnection);
                foreach (var item in usertask)
                {
                    var stringCheck = string.Empty;
                    if (item.Status == (int)TaskStatus.Completed)
                        stringCheck = "Checked";
                    else
                    {
                        stringCheck = "Unchecked";
                    }

                    if (item.Status == (int)TaskStatus.OnRoute || item.Status == (int)TaskStatus.InProgress)
                    {
                        item.StatusDescription = "InProgress";
                    }

                    chklisty.Add(item.Name + "|" + stringCheck + "|" + item.Id.ToString() + "|" + item.StatusDescription);
                }
                listy.AddRange(chklisty);
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("Ticketing", "getTaskListData2", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getTaskListDataTicket(int id, string uname)
        {

            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }
                var chklisty = new List<string>();
                var usertask = UserTask.GetAllTasksByIncidentId(id, dbConnection);

                var module = "false";

                var gUserMod = UserModules.GetUserModuleMapByUserIdAndModuleId(userinfo.ID, (int)Accounts.ModuleTypes.Tasks, dbConnection);

                if (gUserMod.Count > 0)
                    module = "true";

                if (userinfo.RoleId == (int)Role.SuperAdmin)
                {
                    module = "true";
                }

                foreach (var item in usertask)
                {
                    var stringCheck = string.Empty;
                    if (item.Status == (int)TaskStatus.Completed || item.Status == (int)TaskStatus.Accepted)
                        stringCheck = "Checked";
                    else
                    {
                        stringCheck = "Unchecked";
                    }

                    if (item.Status == (int)TaskStatus.OnRoute || item.Status == (int)TaskStatus.InProgress)
                    {
                        item.StatusDescription = "InProgress";
                    }

                    chklisty.Add(item.Name + "|" + stringCheck + "|" + item.Id.ToString() + "|" + item.StatusDescription + "|" + module);

                    var linktasks = UserTask.GetAllTasksByTaskLinkId(item.Id, dbConnection);
                    if (linktasks.Count > 0)
                    {
                        foreach (var item2 in linktasks)
                        {
                            stringCheck = string.Empty;
                            if (item2.Status == (int)TaskStatus.Completed || item.Status == (int)TaskStatus.Accepted)
                                stringCheck = "Checked";
                            else
                            {
                                stringCheck = "Unchecked";
                            }

                            if (item2.Status == (int)TaskStatus.OnRoute || item2.Status == (int)TaskStatus.InProgress)
                            {
                                item2.StatusDescription = "InProgress";
                            }

                            chklisty.Add(item2.Name + "|" + stringCheck + "|" + item2.Id.ToString() + "|" + item2.StatusDescription + "|" + module);
                        }
                    }
                }
                listy.AddRange(chklisty);
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("Ticketing", "getTaskListDataTicket", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static string getTaskLocationData(int id, string uname)
        {
            var json = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                if (id > 0)
                {
                    var item = UserTask.GetTaskById(Convert.ToInt32(id), dbConnection);
                    var traceback = TraceBackHistory.GetTracBackHistoryBytaskId(Convert.ToInt32(id), dbConnection);
                    json += "[";
                    if (getClientLic != null)
                    {
                        if (getClientLic.isLocation)
                        {
                            if (item.StatusDescription == "Pending")
                            {
                                json += "{ \"Username\" : \"" + item.StatusDescription + "\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                            }
                            else if (item.StatusDescription == "InProgress" || item.StatusDescription == "Pause")
                            {
                                if (item.StartLatitude > 0 && item.StartLongitude > 0)
                                    json += "{ \"Username\" : \"InProgress\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.StartLongitude.ToString() + "\",\"Lat\" : \"" + item.StartLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                                if (item.OnRouteLatitude > 0 && item.OnRouteLongitude > 0)
                                    json += "{ \"Username\" : \"OnRoute\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.OnRouteLongitude.ToString() + "\",\"Lat\" : \"" + item.OnRouteLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                                json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                            }
                            else if (item.StatusDescription == "OnRoute")
                            {
                                if (item.OnRouteLatitude > 0 && item.OnRouteLongitude > 0)
                                    json += "{ \"Username\" : \"OnRoute\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.OnRouteLongitude.ToString() + "\",\"Lat\" : \"" + item.OnRouteLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";


                                json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                            }
                            else if (item.StatusDescription == "Completed" || item.StatusDescription == "Accepted" || item.StatusDescription == "Rejected")
                            {
                                if (item.EndLatitude > 0 && item.EndLongitude > 0)
                                    json += "{ \"Username\" : \"Completed\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.EndLongitude.ToString() + "\",\"Lat\" : \"" + item.EndLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"GREEN\",\"Logs\" : \"Retrieve\"},";
                                if (item.StartLatitude > 0 && item.StartLongitude > 0)
                                    json += "{ \"Username\" : \"InProgress\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.StartLongitude.ToString() + "\",\"Lat\" : \"" + item.StartLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                                if (item.OnRouteLatitude > 0 && item.OnRouteLongitude > 0)
                                    json += "{ \"Username\" : \"OnRoute\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.OnRouteLongitude.ToString() + "\",\"Lat\" : \"" + item.OnRouteLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";


                                json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                            }
                        }
                    }
                    json = json.Substring(0, json.Length - 1);
                    json += "]";
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Tasks", "getTaskLocationData", ex, dbConnection, userinfo.SiteId);
            }
            return json;
        }
        [WebMethod]
        public static List<string> getTableRowDataTicket(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var customData = CustomEvent.GetCustomEventById(id, dbConnection);

                if (customData != null)
                {
                    var offenceinfo = DriverOffence.GetDriverOffenceById(customData.Identifier, dbConnection);
                    if (offenceinfo != null)
                    {
                        listy.Add(offenceinfo.PlateNumber);
                        listy.Add(offenceinfo.PlateSource);
                        listy.Add(offenceinfo.PlateCode);
                        listy.Add(offenceinfo.VehicleMake);
                        listy.Add(offenceinfo.VehicleColor);
                        var ggUser = Users.GetUserByName(offenceinfo.UserName, dbConnection);
                        listy.Add(ggUser.CustomerUName);
                        var geolocation = ReverseGeocode.RetrieveFormatedAddress(customData.Latitude.ToString(), customData.Longtitude.ToString(), getClientLic);
                        listy.Add(geolocation);
                        listy.Add(offenceinfo.CreatedDate.AddHours(userinfo.TimeZone).ToString());
                        listy.Add(offenceinfo.OffenceCategory);
                        listy.Add(offenceinfo.OffenceType);
                        if (userinfo.RoleId == (int)Role.CustomerUser)
                        {
                            if (offenceinfo.TicketStatus == 4)
                            {
                                listy.Add("5");
                            }
                            else
                                listy.Add("6");
                        }
                        else
                        {
                            if (customData.Handled)
                            {
                                listy.Add("5");
                            }
                            else
                                listy.Add(offenceinfo.TicketStatus.ToString());
                        }

                        listy.Add(offenceinfo.Comments);
                        if (!string.IsNullOrEmpty(offenceinfo.CusName))
                            listy.Add(offenceinfo.CusName);
                        else
                        {
                            listy.Add("N/A");
                        }
                        if (!string.IsNullOrEmpty(offenceinfo.ContractName))
                            listy.Add(offenceinfo.ContractName);
                        else
                        {
                            listy.Add("N/A");
                        }

                        listy.Add(offenceinfo.ContractId.ToString());
                        listy.Add(offenceinfo.CusId.ToString());

                        var usertask = UserTask.GetAllTasksByIncidentId(id, dbConnection);
                        if (usertask.Count > 0)
                        {
                            usertask = usertask.Where(i => i.Status != (int)TaskStatus.Completed && i.Status != (int)TaskStatus.Accepted).ToList();
                            if (usertask.Count > 0)
                            {
                                listy.Add("0");
                            }
                            else
                            {
                                listy.Add("1");
                            }
                        }
                        else
                        {
                            listy.Add("1");
                        }

                        if (offenceinfo.TicketStatus == (int)CustomEvent.IncidentActionStatus.Engage)
                        {
                            customData.StatusName = "Inprogress";
                        }
                        else if (offenceinfo.TicketStatus == (int)CustomEvent.IncidentActionStatus.Complete)
                        {
                            customData.StatusName = "Completed";
                        }

                        if (customData.Handled)
                            customData.StatusName = "Resolved";

                        listy.Add(customData.StatusName);
                    }
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Ticketing", "getTableRowData", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }
        [WebMethod]
        public static string getDispatchUserList(int id, string ttype)
        {
            var json = string.Empty;
            var assigneeList = new List<int>();
            if (id > 0)
            {
                if (ttype == "task")
                {
                    var item = UserTask.GetTaskById(Convert.ToInt32(id), dbConnection);
                    json += "<a href='#' style='color:" + CommonUtility.getHexColor(0) + "' ><i  class='fa fa-check-square-o'></i>" + item.ACustomerUName + "</a>";
                }
                else
                {
                    var colorCount = 0;
                    var notification = Arrowlabs.Business.Layer.Notification.GetAllNotificationsByIncidentId(Convert.ToInt32(id), dbConnection);
                    foreach (var noti in notification)
                    {

                        var tbColor = string.Empty;
                        if (assigneeList.IndexOf(noti.AssigneeId) != -1)
                        {

                        }
                        else
                        {
                            tbColor = CommonUtility.getHexColor(colorCount);
                            assigneeList.Add(noti.AssigneeId);
                            colorCount++;
                            json += "<a href='#' style='color:" + tbColor + ";font-size:16px;' onclick='tracebackOnUser(&apos;" + id + "&apos;,&apos;" + ttype + "&apos;,&apos;" + noti.AssigneeId + "&apos;)'><i id='" + id + noti.AssigneeId + "'  class='fa fa-check-square-o'></i>" + noti.ACustomerUName + "</a>|";

                        }
                    }
                }
            }
            return json;
        }

        [WebMethod]
        public static string getTracebackLocationData(int id, string ttype, string uname)
        {
            var json = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var assigneeList = new List<int>();
            if (id > 0)
            {
                json += "[";
                if (ttype == "task")
                {
                    var item = UserTask.GetTaskById(Convert.ToInt32(id), dbConnection);
                    var traceback = TraceBackHistory.GetTracBackHistoryBytaskId(Convert.ToInt32(id), dbConnection);
                    json += "[";
                    var geteventhistorys = TaskEventHistory.GetTaskEventHistoryByTaskId(Convert.ToInt32(id), dbConnection);
                    var rejecteds = geteventhistorys.Where(i => i.Action == (int)TaskAction.Rejected).ToList();

                    var gotrejected = false;

                    if (rejecteds.Count > 0)
                    {
                        gotrejected = true;
                    }

                    var inprohistory = new TaskEventHistory();

                    if (!gotrejected)
                    {
                        if (item.StatusDescription == "Pending")
                        {
                            if (item.Longitude > 0 && item.Latitude > 0)
                                json += "{ \"Username\" : \"" + item.StatusDescription + "\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                        }
                        else if (item.StatusDescription == "InProgress" || item.StatusDescription == "Pause")
                        {
                            if (item.StartLatitude > 0 && item.StartLongitude > 0)
                                json += "{ \"Username\" : \"InProgress\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.StartLongitude.ToString() + "\",\"Lat\" : \"" + item.StartLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                            if (item.OnRouteLatitude > 0 && item.OnRouteLongitude > 0)
                                json += "{ \"Username\" : \"OnRoute\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.OnRouteLongitude.ToString() + "\",\"Lat\" : \"" + item.OnRouteLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                            if (item.Longitude > 0 && item.Latitude > 0)
                                json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                        }
                        else if (item.StatusDescription == "OnRoute")
                        {
                            if (item.OnRouteLatitude > 0 && item.OnRouteLongitude > 0)
                                json += "{ \"Username\" : \"" + item.StatusDescription + "\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.OnRouteLongitude.ToString() + "\",\"Lat\" : \"" + item.OnRouteLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";


                            json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                        }
                        else if (item.StatusDescription == "Completed" || item.StatusDescription == "Accepted")
                        {
                            if (item.EndLongitude > 0 && item.EndLatitude > 0)
                                json += "{ \"Username\" : \"" + item.StatusDescription + "\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.EndLongitude.ToString() + "\",\"Lat\" : \"" + item.EndLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"GREEN\",\"Logs\" : \"Retrieve\"},";

                            if (item.StartLatitude > 0 && item.StartLongitude > 0)
                                json += "{ \"Username\" : \"InProgress\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.StartLongitude.ToString() + "\",\"Lat\" : \"" + item.StartLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                            if (item.OnRouteLatitude > 0 && item.OnRouteLongitude > 0)
                                json += "{ \"Username\" : \"OnRoute\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.OnRouteLongitude.ToString() + "\",\"Lat\" : \"" + item.OnRouteLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                            if (item.Longitude > 0 && item.Latitude > 0)
                                json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                        }
                    }
                    else
                    {
                        if (item.Longitude > 0 && item.Latitude > 0)
                            json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                        foreach (var ev in geteventhistorys)
                        {
                            if (ev.Action == (int)TaskAction.OnRoute)
                            {
                                if (!string.IsNullOrEmpty(ev.Longtitude) && !string.IsNullOrEmpty(ev.Latitude))
                                    json += "{ \"Username\" : \"OnRoute\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + ev.Longtitude + "\",\"Lat\" : \"" + ev.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                                if (inprohistory.Id == 0)
                                {
                                    if (ev.CreatedDate.Value < item.ActualStartDate.Value)
                                    {
                                        inprohistory = ev;
                                    }
                                }
                            }
                            else if (ev.Action == (int)TaskAction.InProgress)
                            {
                                if (!string.IsNullOrEmpty(ev.Longtitude) && !string.IsNullOrEmpty(ev.Latitude))
                                    json += "{ \"Username\" : \"InProgress\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + ev.Longtitude + "\",\"Lat\" : \"" + ev.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                                if (inprohistory.Id == 0)
                                {
                                    if (ev.CreatedDate.Value < item.ActualStartDate.Value)
                                    {
                                        inprohistory = ev;
                                    }
                                }
                            }
                            else if (ev.Action == (int)TaskAction.Complete)
                            {
                                if (!string.IsNullOrEmpty(ev.Longtitude) && !string.IsNullOrEmpty(ev.Latitude))
                                {
                                    if (ev.CreatedDate.Value < item.ActualEndDate.Value)
                                        json += "{ \"Username\" : \"Completed\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + ev.Longtitude + "\",\"Lat\" : \"" + ev.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"X\",\"Logs\" : \"Retrieve\"},";
                                    else
                                        json += "{ \"Username\" : \"Completed\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + ev.Longtitude + "\",\"Lat\" : \"" + ev.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"GREEN\",\"Logs\" : \"Retrieve\"},";
                                }
                            }
                        }
                    }
                    if (item.StatusDescription == "Completed" || item.StatusDescription == "Accepted" || gotrejected)
                    {
                        if (traceback.Count > 0)
                        {
                            if (gotrejected)
                            {
                                if (inprohistory.Id == 0)
                                {
                                    if (!string.IsNullOrEmpty(inprohistory.Longtitude) && !string.IsNullOrEmpty(inprohistory.Latitude))
                                        json += "{ \"Username\" : \"\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + inprohistory.Longtitude.ToString() + "\",\"Lat\" : \"" + inprohistory.Latitude.ToString() + "\",\"LastLog\" :  \"" + item.ActualStartDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                                }
                                else
                                {
                                    if (item.StartLatitude > 0 && item.StartLongitude > 0)
                                        json += "{ \"Username\" : \"\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.StartLongitude.ToString() + "\",\"Lat\" : \"" + item.StartLatitude.ToString() + "\",\"LastLog\" :  \"" + item.ActualStartDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                                }
                            }
                            else
                            {
                                if (item.StartLatitude > 0 && item.StartLongitude > 0)
                                    json += "{ \"Username\" : \"\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.StartLongitude.ToString() + "\",\"Lat\" : \"" + item.StartLatitude.ToString() + "\",\"LastLog\" :  \"" + item.ActualStartDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                            }
                            foreach (var tb in traceback)
                            {
                                var getUser = Users.GetUserById(tb.UserId, dbConnection);
                                if (tb.Longitude > 0 && tb.Latitude > 0)
                                    json += "{ \"Username\" : \"" + getUser.Username + "\",\"Id\" : \"" + tb.Id.ToString() + "\",\"Long\" : \"" + tb.Longitude.ToString() + "\",\"Lat\" : \"" + tb.Latitude.ToString() + "\",\"LastLog\" :  \"" + tb.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                            }
                            if (item.EndLongitude > 0 && item.EndLatitude > 0)
                                json += "{ \"Username\" : \"\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.EndLongitude.ToString() + "\",\"Lat\" : \"" + item.EndLatitude.ToString() + "\",\"LastLog\" :  \"" + item.ActualEndDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                        }
                    } 
                }
                else
                {
                    var item = CustomEvent.GetCustomEventById(id, dbConnection);
                    if (item.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Complete)
                    {
                        if (item.EventType == CustomEvent.EventTypes.Incident)
                        {
                            var geofence = GeofenceLocation.GetAllGeofenceLocationbyIncidentId(dbConnection, id);
                            if (geofence.Count > 0)
                            {
                                foreach (var geo in geofence)
                                {
                                    json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.EventId.ToString() + "\",\"Long\" : \"" + geo.Longitude + "\",\"Lat\" : \"" + geo.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                                }
                            }
                            else
                            {
                                json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.EventId.ToString() + "\",\"Long\" : \"" + item.Longtitude + "\",\"Lat\" : \"" + item.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                            }
                        }
                        else
                        {
                            json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.EventId.ToString() + "\",\"Long\" : \"" + item.Longtitude + "\",\"Lat\" : \"" + item.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                        }
                        var colorCount = 0;
                        var notification = Arrowlabs.Business.Layer.Notification.GetAllNotificationsByIncidentId(Convert.ToInt32(id), dbConnection);
                        foreach (var noti in notification)
                        {
                            var tbColor = string.Empty;
                            if (assigneeList.IndexOf(noti.AssigneeId) != -1)
                            {

                            }
                            else
                            {
                                tbColor = CommonUtility.getHexColor(colorCount);
                                assigneeList.Add(noti.AssigneeId);
                                colorCount++;
                            }
                            var traceback = TraceBackHistory.GetTracBackHistoryByIncidentId(noti.Id, dbConnection);

                            if (traceback.Count > 0)
                            {

                                json += "{ \"Username\" : \"Engaged\",\"Id\" : \"" + id + "\",\"Long\" : \"" + traceback[0].Longitude.ToString() + "\",\"Lat\" : \"" + traceback[0].Latitude.ToString() + "\",\"LastLog\" :  \"" + traceback[0].CreatedDate.ToString() + "\",\"State\" : \"ENG\",\"Logs\" : \"Retrieve\"},";
                                foreach (var tb in traceback)
                                {
                                    var getUser = Users.GetUserById(tb.UserId, dbConnection);
                                    json += "{ \"Username\" : \"" + getUser.CustomerUName + "\",\"Id\" : \"" + tb.Id.ToString() + "\",\"Long\" : \"" + tb.Longitude.ToString() + "\",\"Lat\" : \"" + tb.Latitude.ToString() + "\",\"LastLog\" :  \"" + tb.CreatedDate.ToString() + "\",\"State\" : \"PINK" + noti.Id + "\",\"Logs\" : \"" + tbColor + "\"},";
                                }
                                json += "{ \"Username\" : \"Completed\",\"Id\" : \"" + id + "\",\"Long\" : \"" + traceback[traceback.Count - 1].Longitude.ToString() + "\",\"Lat\" : \"" + traceback[traceback.Count - 1].Latitude.ToString() + "\",\"LastLog\" :  \"" + traceback[traceback.Count - 1].CreatedDate.ToString() + "\",\"State\" : \"COM\",\"Logs\" : \"Retrieve\"},";
                            }
                        }
                    }
                }
                //json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                json = json.Substring(0, json.Length - 1);
                json += "]";
            }
            return json;
        }
        [WebMethod]
        public static string getTracebackLocationByDurationData(int id, int duration, string ttype,string uname)
        {
            var json = string.Empty;
            var assigneeList = new List<int>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            if (id > 0)
            {
                json += "[";
                if (ttype == "task")
                {
                    var item = UserTask.GetTaskById(Convert.ToInt32(id), dbConnection);
                    var traceback = TraceBackHistory.GetTracBackHistoryBytaskId(Convert.ToInt32(id), dbConnection);
                    json += "[";
                    var geteventhistorys = TaskEventHistory.GetTaskEventHistoryByTaskId(Convert.ToInt32(id), dbConnection);
                    var rejecteds = geteventhistorys.Where(i => i.Action == (int)TaskAction.Rejected).ToList();

                    var gotrejected = false;

                    if (rejecteds.Count > 0)
                    {
                        gotrejected = true;
                    }
                    if (!gotrejected)
                    {
                        if (item.StatusDescription == "Pending")
                        {
                            if (item.Longitude > 0 && item.Latitude > 0)
                                json += "{ \"Username\" : \"" + item.StatusDescription + "\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                        }
                        else if (item.StatusDescription == "InProgress" || item.StatusDescription == "Pause")
                        {
                            if (item.StartLatitude > 0 && item.StartLongitude > 0)
                                json += "{ \"Username\" : \"InProgress\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.StartLongitude.ToString() + "\",\"Lat\" : \"" + item.StartLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                            if (item.OnRouteLatitude > 0 && item.OnRouteLongitude > 0)
                                json += "{ \"Username\" : \"OnRoute\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.OnRouteLongitude.ToString() + "\",\"Lat\" : \"" + item.OnRouteLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";


                            if (item.Longitude > 0 && item.Latitude > 0)
                                json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                        }
                        else if (item.StatusDescription == "OnRoute")
                        {
                            if (item.OnRouteLatitude > 0 && item.OnRouteLongitude > 0)
                                json += "{ \"Username\" : \"" + item.StatusDescription + "\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.OnRouteLongitude.ToString() + "\",\"Lat\" : \"" + item.OnRouteLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                            json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                        }
                        else if (item.StatusDescription == "Completed" || item.StatusDescription == "Accepted")
                        {
                            if (item.EndLongitude > 0 && item.EndLatitude > 0)
                                json += "{ \"Username\" : \"" + item.StatusDescription + "\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.EndLongitude.ToString() + "\",\"Lat\" : \"" + item.EndLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"GREEN\",\"Logs\" : \"Retrieve\"},";

                            if (item.StartLatitude > 0 && item.StartLongitude > 0)
                                json += "{ \"Username\" : \"InProgress\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.StartLongitude.ToString() + "\",\"Lat\" : \"" + item.StartLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                            if (item.OnRouteLatitude > 0 && item.OnRouteLongitude > 0)
                                json += "{ \"Username\" : \"OnRoute\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.OnRouteLongitude.ToString() + "\",\"Lat\" : \"" + item.OnRouteLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";


                            if (item.Longitude > 0 && item.Latitude > 0)
                                json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                        }
                    }
                    else
                    {
                        if (item.Longitude > 0 && item.Latitude > 0)
                            json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                        foreach (var ev in geteventhistorys)
                        {
                            if (ev.Action == (int)TaskAction.OnRoute)
                            {
                                if (!string.IsNullOrEmpty(ev.Longtitude) && !string.IsNullOrEmpty(ev.Latitude))
                                    json += "{ \"Username\" : \"OnRoute\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + ev.Longtitude + "\",\"Lat\" : \"" + ev.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                            }
                            else if (ev.Action == (int)TaskAction.InProgress)
                            {
                                if (!string.IsNullOrEmpty(ev.Longtitude) && !string.IsNullOrEmpty(ev.Latitude))
                                    json += "{ \"Username\" : \"InProgress\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + ev.Longtitude + "\",\"Lat\" : \"" + ev.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";
                            }
                            else if (ev.Action == (int)TaskAction.Complete)
                            {
                                if (!string.IsNullOrEmpty(ev.Longtitude) && !string.IsNullOrEmpty(ev.Latitude))
                                {
                                    if (ev.CreatedDate.Value < item.ActualEndDate.Value)
                                        json += "{ \"Username\" : \"Completed\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + ev.Longtitude + "\",\"Lat\" : \"" + ev.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"X\",\"Logs\" : \"Retrieve\"},";
                                    else
                                        json += "{ \"Username\" : \"Completed\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + ev.Longtitude + "\",\"Lat\" : \"" + ev.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"GREEN\",\"Logs\" : \"Retrieve\"},";
                                }
                            }
                        }
                    }

                    var curTime = CommonUtility.getDTNow();
                    var firstTime = false;
                    if (item.StatusDescription == "Completed" || item.StatusDescription == "Accepted")
                    {
                        if (traceback.Count > 0)
                        {
                            json += "{ \"Username\" : \"\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.StartLongitude.ToString() + "\",\"Lat\" : \"" + item.StartLatitude.ToString() + "\",\"LastLog\" :  \"" + item.ActualStartDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                            foreach (var tb in traceback)
                            {
                                var getUser = Users.GetUserById(tb.UserId, dbConnection);
                                if (!firstTime)
                                {
                                    curTime = tb.CreatedDate.Value.AddHours(userinfo.TimeZone);
                                    firstTime = true;
                                    json += "{ \"Username\" : \"" + getUser.Username + "\",\"Id\" : \"" + tb.Id.ToString() + "\",\"Long\" : \"" + tb.Longitude.ToString() + "\",\"Lat\" : \"" + tb.Latitude.ToString() + "\",\"LastLog\" :  \"" + tb.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";

                                }
                                else
                                {
                                    if (tb.CreatedDate.Value >= curTime.Add(new TimeSpan(0, duration, 0)))
                                    {
                                        json += "{ \"Username\" : \"" + getUser.Username + "\",\"Id\" : \"" + tb.Id.ToString() + "\",\"Long\" : \"" + tb.Longitude.ToString() + "\",\"Lat\" : \"" + tb.Latitude.ToString() + "\",\"LastLog\" :  \"" + tb.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                                        curTime = tb.CreatedDate.Value.AddHours(userinfo.TimeZone);
                                    }
                                }
                            }
                            json += "{ \"Username\" : \"\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.EndLongitude.ToString() + "\",\"Lat\" : \"" + item.EndLatitude.ToString() + "\",\"LastLog\" :  \"" + item.ActualEndDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                        }
                    } 
                }
                else
                {
                    var item = CustomEvent.GetCustomEventById(id, dbConnection);

                    if (item.EventType == CustomEvent.EventTypes.Incident)
                    {
                        var geofence = GeofenceLocation.GetAllGeofenceLocationbyIncidentId(dbConnection, id);
                        if (geofence.Count > 0)
                        {
                            foreach (var geo in geofence)
                            {
                                json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.EventId.ToString() + "\",\"Long\" : \"" + geo.Longitude + "\",\"Lat\" : \"" + geo.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                            }
                        }
                        else
                        {
                            json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.EventId.ToString() + "\",\"Long\" : \"" + item.Longtitude + "\",\"Lat\" : \"" + item.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                        }
                    }
                    else
                    {
                        json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.EventId.ToString() + "\",\"Long\" : \"" + item.Longtitude + "\",\"Lat\" : \"" + item.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                    }
                    var notification = Arrowlabs.Business.Layer.Notification.GetAllNotificationsByIncidentId(Convert.ToInt32(id), dbConnection);
                    var colorCount = 0;
                    foreach (var noti in notification)
                    {
                        var tbColor = string.Empty;
                        if (assigneeList.IndexOf(noti.AssigneeId) != -1)
                        {

                        }
                        else
                        {
                            tbColor = CommonUtility.getHexColor(colorCount);
                            assigneeList.Add(noti.AssigneeId);
                            colorCount++;
                        }
                        var traceback = TraceBackHistory.GetTracBackHistoryByIncidentId(noti.Id, dbConnection);

                        var curTime = CommonUtility.getDTNow();
                        var firstTime = false;
                        if (traceback.Count > 0)
                        {

                            json += "{ \"Username\" : \"Engaged\",\"Id\" : \"" + id + "\",\"Long\" : \"" + traceback[0].Longitude.ToString() + "\",\"Lat\" : \"" + traceback[0].Latitude.ToString() + "\",\"LastLog\" :  \"" + traceback[0].CreatedDate.ToString() + "\",\"State\" : \"ENG\",\"Logs\" : \"Retrieve\"},";
                            foreach (var tb in traceback)
                            {
                                var getUser = Users.GetUserById(tb.UserId, dbConnection);
                                if (!firstTime)
                                {
                                    curTime = tb.CreatedDate.Value;
                                    firstTime = true;
                                    json += "{ \"Username\" : \"" + getUser.CustomerUName + "\",\"Id\" : \"" + tb.Id.ToString() + "\",\"Long\" : \"" + tb.Longitude.ToString() + "\",\"Lat\" : \"" + tb.Latitude.ToString() + "\",\"LastLog\" :  \"" + tb.CreatedDate.ToString() + "\",\"State\" : \"PINK" + noti.Id + "\",\"Logs\" : \"" + tbColor + "\"},";

                                }
                                else
                                {
                                    if (tb.CreatedDate.Value >= curTime.Add(new TimeSpan(0, duration, 0)))
                                    {
                                        json += "{ \"Username\" : \"" + getUser.CustomerUName + "\",\"Id\" : \"" + tb.Id.ToString() + "\",\"Long\" : \"" + tb.Longitude.ToString() + "\",\"Lat\" : \"" + tb.Latitude.ToString() + "\",\"LastLog\" :  \"" + tb.CreatedDate.ToString() + "\",\"State\" : \"PINK" + noti.Id + "\",\"Logs\" : \"" + tbColor + "\"},";
                                        curTime = tb.CreatedDate.Value;
                                    }
                                }
                            }
                            json += "{ \"Username\" : \"Completed\",\"Id\" : \"" + id + "\",\"Long\" : \"" + traceback[traceback.Count - 1].Longitude.ToString() + "\",\"Lat\" : \"" + traceback[traceback.Count - 1].Latitude.ToString() + "\",\"LastLog\" :  \"" + traceback[traceback.Count - 1].CreatedDate.ToString() + "\",\"State\" : \"COM\",\"Logs\" : \"Retrieve\"},";
                        }
                    }

                }
                json = json.Substring(0, json.Length - 1);
                json += "]";
            }
            return json;
        }

        [WebMethod]
        public static string getTracebackLocationDataByUser(int id, int duration, string ttype, int[] userIds,string uname)
        {
            var json = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var assigneeList = new List<int>();
            if (id > 0)
            {
                json += "[";
                if (ttype == "task")
                {
                    var item = UserTask.GetTaskById(Convert.ToInt32(id), dbConnection);
                    var traceback = TraceBackHistory.GetTracBackHistoryBytaskId(Convert.ToInt32(id), dbConnection);
                    json += "[";
                    var geteventhistorys = TaskEventHistory.GetTaskEventHistoryByTaskId(Convert.ToInt32(id), dbConnection);
                    var rejecteds = geteventhistorys.Where(i => i.Action == (int)TaskAction.Rejected).ToList();

                    var gotrejected = false;

                    if (rejecteds.Count > 0)
                    {
                        gotrejected = true;
                    }
                    if (!gotrejected)
                    {
                        if (item.StatusDescription == "Pending")
                        {
                            if (item.Longitude > 0 && item.Latitude > 0)
                                json += "{ \"Username\" : \"" + item.StatusDescription + "\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                        }
                        else if (item.StatusDescription == "InProgress" || item.StatusDescription == "Pause")
                        {
                            if (item.StartLatitude > 0 && item.StartLongitude > 0)
                                json += "{ \"Username\" : \"InProgress\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.StartLongitude.ToString() + "\",\"Lat\" : \"" + item.StartLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                            if (item.OnRouteLatitude > 0 && item.OnRouteLongitude > 0)
                                json += "{ \"Username\" : \"OnRoute\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.OnRouteLongitude.ToString() + "\",\"Lat\" : \"" + item.OnRouteLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";


                            if (item.Longitude > 0 && item.Latitude > 0)
                                json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                        }
                        else if (item.StatusDescription == "OnRoute")
                        {
                            if (item.OnRouteLatitude > 0 && item.OnRouteLongitude > 0)
                                json += "{ \"Username\" : \"" + item.StatusDescription + "\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.OnRouteLongitude.ToString() + "\",\"Lat\" : \"" + item.OnRouteLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                            json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                        }
                        else if (item.StatusDescription == "Completed" || item.StatusDescription == "Accepted")
                        {
                            if (item.EndLongitude > 0 && item.EndLatitude > 0)
                                json += "{ \"Username\" : \"" + item.StatusDescription + "\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.EndLongitude.ToString() + "\",\"Lat\" : \"" + item.EndLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"GREEN\",\"Logs\" : \"Retrieve\"},";

                            if (item.StartLatitude > 0 && item.StartLongitude > 0)
                                json += "{ \"Username\" : \"InProgress\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.StartLongitude.ToString() + "\",\"Lat\" : \"" + item.StartLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                            if (item.OnRouteLatitude > 0 && item.OnRouteLongitude > 0)
                                json += "{ \"Username\" : \"OnRoute\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.OnRouteLongitude.ToString() + "\",\"Lat\" : \"" + item.OnRouteLatitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";


                            if (item.Longitude > 0 && item.Latitude > 0)
                                json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                        }
                    }
                    else
                    {
                        if (item.Longitude > 0 && item.Latitude > 0)
                            json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                        foreach (var ev in geteventhistorys)
                        {
                            if (ev.Action == (int)TaskAction.OnRoute)
                            {
                                if (!string.IsNullOrEmpty(ev.Longtitude) && !string.IsNullOrEmpty(ev.Latitude))
                                    json += "{ \"Username\" : \"OnRoute\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + ev.Longtitude + "\",\"Lat\" : \"" + ev.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";

                            }
                            else if (ev.Action == (int)TaskAction.InProgress)
                            {
                                if (!string.IsNullOrEmpty(ev.Longtitude) && !string.IsNullOrEmpty(ev.Latitude))
                                    json += "{ \"Username\" : \"InProgress\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + ev.Longtitude + "\",\"Lat\" : \"" + ev.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"BLUE\",\"Logs\" : \"Retrieve\"},";
                            }
                            else if (ev.Action == (int)TaskAction.Complete)
                            {
                                if (!string.IsNullOrEmpty(ev.Longtitude) && !string.IsNullOrEmpty(ev.Latitude))
                                {
                                    if (ev.CreatedDate.Value < item.ActualEndDate.Value)
                                        json += "{ \"Username\" : \"Completed\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + ev.Longtitude + "\",\"Lat\" : \"" + ev.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"X\",\"Logs\" : \"Retrieve\"},";
                                    else
                                        json += "{ \"Username\" : \"Completed\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + ev.Longtitude + "\",\"Lat\" : \"" + ev.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"GREEN\",\"Logs\" : \"Retrieve\"},";
                                }
                            }
                        }
                    }

                    var curTime = CommonUtility.getDTNow();
                    var firstTime = false;
                    if (item.StatusDescription == "Completed" || item.StatusDescription == "Accepted")
                    {
                        if (traceback.Count > 0)
                        {
                            json += "{ \"Username\" : \"\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.StartLongitude.ToString() + "\",\"Lat\" : \"" + item.StartLatitude.ToString() + "\",\"LastLog\" :  \"" + item.ActualStartDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                            foreach (var tb in traceback)
                            {
                                var getUser = Users.GetUserById(tb.UserId, dbConnection);
                                if (!firstTime)
                                {
                                    curTime = tb.CreatedDate.Value.AddHours(userinfo.TimeZone);
                                    firstTime = true;
                                    json += "{ \"Username\" : \"" + getUser.Username + "\",\"Id\" : \"" + tb.Id.ToString() + "\",\"Long\" : \"" + tb.Longitude.ToString() + "\",\"Lat\" : \"" + tb.Latitude.ToString() + "\",\"LastLog\" :  \"" + tb.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";

                                }
                                else
                                {
                                    if (tb.CreatedDate.Value >= curTime.Add(new TimeSpan(0, duration, 0)))
                                    {
                                        json += "{ \"Username\" : \"" + getUser.Username + "\",\"Id\" : \"" + tb.Id.ToString() + "\",\"Long\" : \"" + tb.Longitude.ToString() + "\",\"Lat\" : \"" + tb.Latitude.ToString() + "\",\"LastLog\" :  \"" + tb.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                                        curTime = tb.CreatedDate.Value.AddHours(userinfo.TimeZone);
                                    }
                                }
                            }
                            json += "{ \"Username\" : \"\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.EndLongitude.ToString() + "\",\"Lat\" : \"" + item.EndLatitude.ToString() + "\",\"LastLog\" :  \"" + item.ActualEndDate.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                        }
                    }
                }
                else
                {
                    var item = CustomEvent.GetCustomEventById(id, dbConnection);
                    if (item.EventType == CustomEvent.EventTypes.Incident)
                    {
                        var geofence = GeofenceLocation.GetAllGeofenceLocationbyIncidentId(dbConnection, id);
                        if (geofence.Count > 0)
                        {
                            foreach (var geo in geofence)
                            {
                                json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.EventId.ToString() + "\",\"Long\" : \"" + geo.Longitude + "\",\"Lat\" : \"" + geo.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                            }
                        }
                        else
                        {
                            json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.EventId.ToString() + "\",\"Long\" : \"" + item.Longtitude + "\",\"Lat\" : \"" + item.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";

                        }
                    }
                    else
                    {
                        json += "{ \"Username\" : \"" + item.Name + "\",\"Id\" : \"" + item.EventId.ToString() + "\",\"Long\" : \"" + item.Longtitude + "\",\"Lat\" : \"" + item.Latitude + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                    }
                    var colorCount = 0;
                    var notification = Arrowlabs.Business.Layer.Notification.GetAllNotificationsByIncidentId(Convert.ToInt32(id), dbConnection);
                    foreach (var noti in notification)
                    {
                        var tbColor = string.Empty;
                        if (assigneeList.IndexOf(noti.AssigneeId) != -1)
                        {

                        }
                        else
                        {
                            tbColor = CommonUtility.getHexColor(colorCount);
                            assigneeList.Add(noti.AssigneeId);
                            colorCount++;
                        }
                        if (!userIds.Contains(noti.AssigneeId))//(noti.AssigneeId != userIds[0])
                        {
                            var traceback = TraceBackHistory.GetTracBackHistoryByIncidentId(noti.Id, dbConnection);

                            var curTime = CommonUtility.getDTNow();
                            var firstTime = false;
                            if (traceback.Count > 0)
                            {

                                json += "{ \"Username\" : \"Engaged\",\"Id\" : \"" + id + "\",\"Long\" : \"" + traceback[0].Longitude.ToString() + "\",\"Lat\" : \"" + traceback[0].Latitude.ToString() + "\",\"LastLog\" :  \"" + traceback[0].CreatedDate.ToString() + "\",\"State\" : \"ENG\",\"Logs\" : \"Retrieve\"},";
                                foreach (var tb in traceback)
                                {
                                    var getUser = Users.GetUserById(tb.UserId, dbConnection);
                                    if (!firstTime)
                                    {
                                        curTime = tb.CreatedDate.Value;
                                        firstTime = true;
                                        json += "{ \"Username\" : \"" + getUser.CustomerUName + "\",\"Id\" : \"" + tb.Id.ToString() + "\",\"Long\" : \"" + tb.Longitude.ToString() + "\",\"Lat\" : \"" + tb.Latitude.ToString() + "\",\"LastLog\" :  \"" + tb.CreatedDate.ToString() + "\",\"State\" : \"PINK" + noti.Id + "\",\"Logs\" : \"" + tbColor + "\"},";

                                    }
                                    else
                                    {
                                        if (tb.CreatedDate.Value >= curTime.Add(new TimeSpan(0, duration, 0)))
                                        {
                                            json += "{ \"Username\" : \"" + getUser.CustomerUName + "\",\"Id\" : \"" + tb.Id.ToString() + "\",\"Long\" : \"" + tb.Longitude.ToString() + "\",\"Lat\" : \"" + tb.Latitude.ToString() + "\",\"LastLog\" :  \"" + tb.CreatedDate.ToString() + "\",\"State\" : \"PINK" + noti.Id + "\",\"Logs\" : \"" + tbColor + "\"},";
                                            curTime = tb.CreatedDate.Value;
                                        }
                                    }
                                }
                                json += "{ \"Username\" : \"Completed\",\"Id\" : \"" + id + "\",\"Long\" : \"" + traceback[traceback.Count - 1].Longitude.ToString() + "\",\"Lat\" : \"" + traceback[traceback.Count - 1].Latitude.ToString() + "\",\"LastLog\" :  \"" + traceback[traceback.Count - 1].CreatedDate.ToString() + "\",\"State\" : \"COM\",\"Logs\" : \"Retrieve\"},";
                            }
                        }
                    }
                }
                //json += "{ \"Username\" : \"Pending\",\"Id\" : \"" + item.Id.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"PURPLE\",\"Logs\" : \"Retrieve\"},";
                json = json.Substring(0, json.Length - 1);
                json += "]";
            }
            return json;
        }

        [WebMethod]
        public static string attachFileToTicket(int id, string filepath, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var json = string.Empty;
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }
                var cusEv = CustomEvent.GetCustomEventById(id, dbConnection);
                if (cusEv != null)
                {
                    var newObj = new MobileHotEventAttachment();

                    var newimgPath = filepath.Replace('|', '\\');
                    if (File.Exists(newimgPath))
                    {
                        Stream fs = File.OpenRead(newimgPath);
                        var getFileName = Path.GetFileName(newimgPath);
                        getFileName = Guid.NewGuid().ToString().Split('-')[0] + "-" + getFileName;
                        var savestring = CommonUtility.CloudUploadFile(userinfo.CustomerInfoId, getFileName, fs);
                        if (savestring != "FAIL")
                        {
                            newObj.AttachmentPath = savestring;
                            newObj.AttachmentName = System.IO.Path.GetFileName(savestring);
                            //newObj.SiteId = cusEv.SiteId;
                            newObj.UpdatedBy = userinfo.Username;
                            newObj.UpdatedDate = CommonUtility.getDTNow();
                            newObj.CreatedBy = userinfo.Username;
                            newObj.CreatedDate = CommonUtility.getDTNow();
                            newObj.EventId = id;
                            newObj.CustomerId = userinfo.CustomerInfoId;
                            newObj.SiteId = userinfo.SiteId;
                            newObj.IsPortal = true;
                            MobileHotEventAttachment.InsertOrUpdateIncidentMobileAttachment(newObj, dbConnection, dbConnectionAudit, true);
                            json = "Successfully attached file";
                        }
                        else
                        {
                            MIMSLog.MIMSLogSave("Incident", "Failed to upload file to cloud.", new Exception(), dbConnection, userinfo.SiteId);
                            json = "Failed to upload file to cloud.";
                        }
                        if (fs != null)
                        {
                            fs.Close();
                        }
                        if (File.Exists(newimgPath))
                            File.Delete(newimgPath);
                    }
                    else
                    {
                        MIMSLog.MIMSLogSave("Incident", "File trying to upload doesn't exist.", new Exception(), dbConnection, userinfo.SiteId);
                        json = "File trying to upload doesn't exist.";
                    }
                }
                else
                    json = "Problem was faced trying to attach file.";
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Lost", "attachFileToTask", err, dbConnection, userinfo.SiteId);
                json = err.Message;
            }
            return json;
        }

        //DEMO EDI


        public int Sum(List<int> customerssalary)
        {
            int result = 0;

            for (int i = 0; i < customerssalary.Count; i++)
            {
                result += customerssalary[i];
            }

            return result;
        }



        [WebMethod]
        public static List<string> getTableDataTeamTasks(int id,string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);

            var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

            if (isValidSession == null)
            {
                listy.Add("LOGOUT");
                return listy;
            }

            var fullcollection = new List<UserTask>();
            if (userinfo != null)
            {
                if (userinfo.RoleId == (int)Role.SuperAdmin)
                    fullcollection = UserTask.GetAllTaskByDateExcludingRecurringTaskParent(CommonUtility.getDTNow().Date, dbConnection);
                else
                    fullcollection = UserTask.GetAllTasksOfTeamByManagerId(userinfo.ID, dbConnection);

                var imageclass = string.Empty;
                foreach (var item in fullcollection)
                {
                    //if(item.Status == "")
                    if (item.StartDate <= CommonUtility.getDTNow().Date)
                    {
                        if (item.Status != (int)TaskStatus.Accepted && item.Status != (int)TaskStatus.Rejected && item.Status != (int)TaskStatus.Cancelled)
                        {
                            if (item.Status == (int)TaskStatus.RejectedSaved)
                                item.StatusDescription = "Rejected";

                            imageclass = CommonUtility.getImgStatusPriority(item.Priority);
                            var returnstring = "<tr role='row' class='odd'><td><span class='circle-point-container'><span class='circle-point " + imageclass + "'></span></span></td><td class='sorting_1'>" + item.StatusDescription + "</td><td>" + item.Name + "</td><td>" + item.CreateDate.ToString() + "</td><td>" + item.AssigneeName + "</td><td><a href='#' data-target='#taskDocument'  data-toggle='modal' onclick='showTaskDocument(&apos;" + item.Id + "&apos;)'><i class='fa fa-eye mr-1x'></i>View</a></td></tr>";
                            listy.Add(returnstring);
                        }
                    }
                }
                //<tr role="row" class="odd clickable-row clickable-row-links"><td><span class="circle-point-container"><span class="circle-point circle-point-orange"></span></span></td><td class="sorting_1">Gecko</td><td>Firefox 1.0</td><td>Win 98+ / OSX.2+</td><td>'++'</td><td><a href="#"><i class="fa fa-eye mr-1x"></i>View</a></td></tr>
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getTableDataTeamTasksByDate(string date,string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);

            var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

            if (isValidSession == null)
            {
                listy.Add("LOGOUT");
                return listy;
            }

            var fullcollection = new List<UserTask>();
            if (!string.IsNullOrEmpty(date))
            {
                var dtfilter = Convert.ToDateTime(date);
                if (userinfo != null)
                {
                    if (userinfo.RoleId == (int)Role.SuperAdmin)
                        fullcollection = UserTask.GetAllTaskByDateExcludingRecurringTaskParent(dtfilter.Date, dbConnection);
                    else
                        fullcollection = UserTask.GetAllTasksOfTeamByManagerId(userinfo.ID, dbConnection);

                    var imageclass = string.Empty;
                    foreach (var item in fullcollection)
                    {
                        //if(item.Status == "")
                        if (item.StartDate <= dtfilter.Date)
                        {
                            if (item.Status != (int)TaskStatus.Accepted && item.Status != (int)TaskStatus.Rejected && item.Status != (int)TaskStatus.Cancelled)
                            {
                                if (item.Status == (int)TaskStatus.RejectedSaved)
                                    item.StatusDescription = "Rejected";

                                imageclass = CommonUtility.getImgStatusPriority(item.Priority);
                                var returnstring = "<tr role='row' class='odd'><td><span class='circle-point-container'><span class='circle-point " + imageclass + "'></span></span></td><td class='sorting_1'>" + item.StatusDescription + "</td><td>" + item.Name + "</td><td>" + item.CreateDate.ToString() + "</td><td>" + item.AssigneeName + "</td><td><a href='#' data-target='#taskDocument'  data-toggle='modal' onclick='showTaskDocument(&apos;" + item.Id + "&apos;)'><i class='fa fa-eye mr-1x'></i>View</a></td></tr>";
                                listy.Add(returnstring);
                            }
                        }
                    }
                }
                //<tr role="row" class="odd clickable-row clickable-row-links"><td><span class="circle-point-container"><span class="circle-point circle-point-orange"></span></span></td><td class="sorting_1">Gecko</td><td>Firefox 1.0</td><td>Win 98+ / OSX.2+</td><td>'++'</td><td><a href="#"><i class="fa fa-eye mr-1x"></i>View</a></td></tr>
            }
            return listy;
        }
 
        [WebMethod]
        public static List<string> getRecentActivity3(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();

            var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

            if (isValidSession == null)
            {
                listy.Add("LOGOUT");
                return listy;
            }

            var usersStringList = new List<string>();
            var events = new List<EventHistory>();
            //if (userinfo.RoleId == (int)Role.Manager)
            //{
            //    events = EventHistory.Top10RecentActivityofVerifierAndCustomEventBySiteId((int)CustomEvent.EventTypes.DriverOffence, (int)Verifier.RequestTypes.Verify,userinfo.SiteId ,dbConnection);
            //    var userlist = Users.GetAllFullUsersByManagerId(userinfo.ID, dbConnection);
            //    foreach (var user in userlist)
            //    {
            //        usersStringList.Add(user.Username.ToLower());
            //    }
            //}
            //else if (userinfo.RoleId == (int)Role.Admin)
            //{
            //    events = EventHistory.Top10RecentActivityofVerifierAndCustomEventBySiteId((int)CustomEvent.EventTypes.DriverOffence, (int)Verifier.RequestTypes.Verify, userinfo.SiteId, dbConnection);
            //    var sessions = DirectorManager.GetAllFullManagersByDirectorId(userinfo.ID, dbConnection);
            //    foreach (var usr in sessions)
            //    {
            //        usersStringList.Add(usr.Username.ToLower());
            //        var getallUsers = Users.GetAllFullUsersByManagerIdForDirector(usr.ID, dbConnection);
            //        foreach (var subsubuser in getallUsers)
            //        {
            //            usersStringList.Add(subsubuser.Username.ToLower());
            //        }
            //    }
            //    var unassigned = Users.GetAllUnassignedOperators(dbConnection);
            //    unassigned = unassigned.Where(i => i.SiteId == (int)userinfo.SiteId).ToList();
            //    foreach (var subsubuser in unassigned)
            //    {
            //        usersStringList.Add(subsubuser.Username.ToLower());
            //    } 
            //}
            //else if(userinfo.RoleId == (int)Role.Director)
            //    events = EventHistory.Top10RecentActivityofVerifierAndCustomEventBySiteId((int)CustomEvent.EventTypes.DriverOffence, (int)Verifier.RequestTypes.Verify, userinfo.SiteId, dbConnection);
            //else if (userinfo.RoleId == (int)Role.CustomerSuperadmin){
            //    events = EventHistory.Top10RecentActivityofVerifierAndCustomEventByCId((int)CustomEvent.EventTypes.DriverOffence, (int)Verifier.RequestTypes.Verify, userinfo.CustomerInfoId, dbConnection);
            //else if (userinfo.RoleId == (int)Role.Regional)
            //{
            //   // var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
            //   // foreach (var site in sites)
            //    //{
            //    events.AddRange(EventHistory.Top10RecentActivityofVerifierAndCustomEventByLevel5((int)CustomEvent.EventTypes.DriverOffence, (int)Verifier.RequestTypes.Verify, userinfo.ID, dbConnection));
            //    //}
            //}
            //else 
            if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
            {
                events = EventHistory.Top10RecentActivityofVerifierAndCustomEvent((int)CustomEvent.EventTypes.DriverOffence, (int)Verifier.RequestTypes.Verify, dbConnection);
            }
            else if (userinfo.RoleId == (int)Role.CustomerUser)
            {
                events = EventHistory.Top10RecentActivityofVerifierAndCustomEventByCId((int)CustomEvent.EventTypes.DriverOffence, (int)Verifier.RequestTypes.Verify, userinfo.CustomerInfoId, dbConnection);
                events = events.Where(i => i.UserName == userinfo.Username).ToList();
            }
            else
            {
                events = EventHistory.Top10RecentActivityofVerifierAndCustomEventByCId((int)CustomEvent.EventTypes.DriverOffence, (int)Verifier.RequestTypes.Verify, userinfo.CustomerInfoId, dbConnection);
            }
            foreach (var item in events)
            {
                if (usersStringList.Count > 0)
                {
                    item.UserName = System.Text.RegularExpressions.Regex.Replace(item.UserName, @"\s+", "");
                    if (usersStringList.Contains(item.UserName.ToLower()))
                    {
                        if (item.CreatedDate.Value >= CommonUtility.getDTNow().Subtract(new TimeSpan(1,0,0)))
                        {
                            var incidentLocation = "from " + ReverseGeocode.RetrieveFormatedAddress(item.Latitude.ToString(), item.Longtitude.ToString(), getClientLic); ;

                            var actioninfo = "created";

                            var recTimeMinusNow = CommonUtility.getDTNow().Subtract(item.CreatedDate.Value);

                            var name = "Ticket";

                            var linkdisplay = "data-target='#ticketingViewCard' data-toggle='modal' onclick='rowchoice(" + item.EventId + ")'";

                            if (item.Id > 0)
                            {
                                name = "FRS";
                                linkdisplay = "data-target='#verificationDocument' data-toggle='modal' onclick='assignVerifierId(" + item.EventId + ")'";
                            }

                            var jsonstring = "<div class='col-md-2'><p>" + recTimeMinusNow.Minutes + "M</p></div><div class='col-md-2' " + linkdisplay + "><span class='bullhorn-circle'><i class='fa fa-bullhorn'></i></span></div><div class='col-md-8'><p><span>" + item.CustomerUName + "</span></br>" + actioninfo + "<span>" + name + "</span>" + incidentLocation + "</p></div><div class='vertical-line'></div>";

                            listy.Add(jsonstring);
                        }
                    }
                }
                else
                {
                    if (item.CreatedDate.Value >= CommonUtility.getDTNow().Subtract(new TimeSpan(1, 0, 0)))
                    {
                        var incidentLocation = "from " + ReverseGeocode.RetrieveFormatedAddress(item.Latitude.ToString(), item.Longtitude.ToString(), getClientLic); ;

                        var actioninfo = "created";

                        var recTimeMinusNow = CommonUtility.getDTNow().Subtract(item.CreatedDate.Value);

                        var name = "Ticket";

                        var linkdisplay = "data-target='#ticketingViewCard' data-toggle='modal' onclick='rowchoice(" + item.EventId + ")'";

                        if (item.Id > 0)
                        {
                            name = "FRS";
                            linkdisplay = "data-target='#verificationDocument' data-toggle='modal' onclick='assignVerifierId(" + item.EventId + ")'";
                        }

                        var jsonstring = "<div class='col-md-2'><p>" + recTimeMinusNow.Minutes + "M</p></div><div class='col-md-2' " + linkdisplay + "><span class='bullhorn-circle'><i class='fa fa-bullhorn'></i></span></div><div class='col-md-8'><p><span>" + item.CustomerUName + "</span></br>" + actioninfo + "<span>" + name + "</span>" + incidentLocation + "</p></div><div class='vertical-line'></div>";

                        listy.Add(jsonstring);
                    }
                }
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getTableDataOther(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            if (userinfo != null)
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var allcustomEvents = new List<CustomEvent>();
                //if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                //    allcustomEvents = CustomEvent.GetAllCustomEventByType((int)CustomEvent.EventTypes.DriverOffence, dbConnection);
                //else if (userinfo.RoleId == (int)Role.Director)
                //    allcustomEvents = CustomEvent.GetAllCustomEventByTypeBySiteId((int)CustomEvent.EventTypes.DriverOffence, userinfo.SiteId, dbConnection);
                //else if (userinfo.RoleId == (int)Role.Regional)
                //{
                //    allcustomEvents.AddRange(CustomEvent.GetAllCustomEventByTypeByLevel5((int)CustomEvent.EventTypes.DriverOffence, userinfo.ID, dbConnection));
                //}
                //else if (userinfo.RoleId == (int)Role.Manager)
                //{

                //    allcustomEvents = CustomEvent.GetAllCustomEventByTypeAndManagerId((int)CustomEvent.EventTypes.DriverOffence, userinfo.ID, dbConnection);
                //    var pclist = new List<string>();
                //    var customeventlist = CustomEvent.GetAllCustomEventByType((int)CustomEvent.EventTypes.DriverOffence, dbConnection);


                //    foreach (var custom in customeventlist)
                //    {
                //        if (pclist.Contains(custom.UserName))
                //            allcustomEvents.Add(custom);
                //    }

                //}
                //else if (userinfo.RoleId == (int)Role.Admin)
                //{
                //    var managerusers = DirectorManager.GetAllManagersByDirectorId(userinfo.ID, dbConnection);
                //    allcustomEvents.AddRange(CustomEvent.GetAllCustomEventByTypeAndManagerId((int)CustomEvent.EventTypes.DriverOffence, userinfo.ID, dbConnection));
                //    foreach (var manguser in managerusers)
                //    {
                //        allcustomEvents.AddRange(CustomEvent.GetAllCustomEventByTypeAndManagerId((int)CustomEvent.EventTypes.DriverOffence, manguser, dbConnection));

                //        var pclist = new List<string>();
                //        var customeventlist = CustomEvent.GetAllCustomEventByType((int)CustomEvent.EventTypes.DriverOffence, dbConnection);

                //        foreach (var custom in customeventlist)
                //        {
                //            if (pclist.Contains(custom.UserName))
                //                allcustomEvents.Add(custom);
                //        }
                //    }
                //}
                //else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                //{
                //    allcustomEvents = CustomEvent.GetAllCustomEventByTypeAndCId((int)CustomEvent.EventTypes.DriverOffence, userinfo.CustomerInfoId, dbConnection);
                //}
                //else if (userinfo.RoleId == (int)Role.CustomerUser)
                //{
                //    allcustomEvents = CustomEvent.GetAllCustomEventByTypeAndCId((int)CustomEvent.EventTypes.DriverOffence, userinfo.CustomerInfoId, dbConnection);
                //    allcustomEvents = allcustomEvents.Where(i => i.UserName == userinfo.Username).ToList();
                //}

                if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                {
                    allcustomEvents = CustomEvent.GetAllCustomEventByType((int)CustomEvent.EventTypes.DriverOffence, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.CustomerUser)
                {
                    allcustomEvents = CustomEvent.GetAllCustomEventByTypeAndCId((int)CustomEvent.EventTypes.DriverOffence, userinfo.CustomerInfoId, dbConnection);
                    allcustomEvents = allcustomEvents.Where(i => i.UserName == userinfo.Username).ToList();
                }
                else
                {
                    allcustomEvents = CustomEvent.GetAllCustomEventByTypeAndCId((int)CustomEvent.EventTypes.DriverOffence, userinfo.CustomerInfoId, dbConnection);
                }

                foreach (var item in allcustomEvents)
                {

                    var gTicket = DriverOffence.GetDriverOffenceById(item.Identifier, dbConnection);
                    if (gTicket != null)
                    {
                        if (gTicket.TicketStatus == (int)CustomEvent.IncidentActionStatus.Engage)
                        {
                            item.StatusName = "Inprogress";
                        }
                        else if (gTicket.TicketStatus == (int)CustomEvent.IncidentActionStatus.Complete)
                        {
                            item.StatusName = "Completed";
                        }
                        var returnstring = "<tr role='row' class='odd'><td class='sorting_1'>" + item.CustomerIncidentId + "</td><td>" + item.StatusName + "</td><td>" + gTicket.OffenceCategory + "</td><td>" + item.CustomerUName + "</td><td>" + item.RecevieTime.Value.AddHours(userinfo.TimeZone).ToString() + "</td><td><a href='#' data-target='#ticketingViewCard'  data-toggle='modal' onclick='rowchoice(&apos;" + item.EventId + "&apos;) '><i class='fa fa-eye mr-1x'></i>View</a></td></tr>";
                        listy.Add(returnstring);
                    }
                }
            }
            return listy;
        }
        [WebMethod]
        public static List<string> getTableDataOther2(int id,string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            if (userinfo != null)
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var sessions = new List<Verifier>();
                if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                {
                    sessions = Verifier.GetAllVerifierNoBytesByRequest((int)Verifier.RequestTypes.Verify, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.Director)
                {
                    sessions = Verifier.GetAllVerifierNoBytesByRequestAndSite((int)Verifier.RequestTypes.Verify, userinfo.SiteId, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                    var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                    foreach (var site in sites)
                        sessions.AddRange(Verifier.GetAllVerifierNoBytesByRequestAndSite((int)Verifier.RequestTypes.Verify, site.SiteId, dbConnection));
                }
                else if (userinfo.RoleId == (int)Role.Manager)
                {
                    sessions = Verifier.GetAllVerifierNoBytesByManagerIdAndRequest(userinfo.ID, (int)Verifier.RequestTypes.Verify, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.Admin)
                {
                    sessions = Verifier.GetAllVerifierNoBytesByManagerIdAndRequest(userinfo.ID, (int)Verifier.RequestTypes.Verify, dbConnection);
                    var xsessions = DirectorManager.GetAllFullManagersByDirectorId(userinfo.ID, dbConnection);
                    foreach (var usr in xsessions)
                    {
                        sessions.AddRange(Verifier.GetAllVerifierNoBytesByManagerIdAndRequest(usr.ID, (int)Verifier.RequestTypes.Verify, dbConnection));
                    }
                    var grouped = sessions.GroupBy(item => item.Id);
                    sessions = grouped.Select(grp => grp.OrderBy(item => item.Id).First()).ToList();
                }
                foreach (var item in sessions)
                {
                    var returnstring = "<tr role='row' class='odd'><td>" + item.TypeName + "</td><td>" + item.CreatedBy + "</td><td>" + item.CreatedDate.ToString() + "</td><td><a href='#' data-target='#verificationDocument'  data-toggle='modal' onclick='assignVerifierId(&apos;" + item.Id + "&apos;) '><i class='fa fa-eye mr-1x'></i>View</a></td></tr>";
                    listy.Add(returnstring);
                }
            }
            return listy;
        }
        //VERIFIER PART FOR EDI DEMO
        [WebMethod]
        public static string getVerifyLocationData(int id)
        {
            var json = string.Empty;
            json += "[";
            if (id > 0)
            {
                if (getClientLic != null)
                {
                    if (getClientLic.isLocation)
                    {
                        var customData = Verifier.GetVerifierById(id, dbConnection);

                        if (customData != null)
                        {
                            json += "{ \"Username\" : \"" + customData.TypeName + "\",\"Id\" : \"" + customData.Id.ToString() + "\",\"Long\" : \"" + customData.Longitude.ToString() + "\",\"Lat\" : \"" + customData.Latitude.ToString() + "\",\"LastLog\" : \"\",\"State\" : \"RED\",\"Logs\" : \"Retrieve\"},";
                        }
                    }
                }
            }
            json = json.Substring(0, json.Length - 1);
            json += "]";
            return json;
        }
        [WebMethod]
        public static List<string> getVerifierRequestData(int id,string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var customData = Verifier.GetVerifierById(id, dbConnection);
                var caseInfo = new Enrollment();
                var runVerify = VerifierResults.GetVerifierResultByVerifierId(id, dbConnection);
                if (runVerify.Count > 0)
                {

                    caseInfo = Enrollment.GetAllEnrollmentByCaseId(runVerify[0].CaseId, dbConnection);

                }
                //var caseInfo = Enrollment.GetAllEnrollmentByCaseId(customData.CaseId, dbConnection);
                var newCaseId = new List<int>();
                if (caseInfo != null)
                {
                    listy.Add(caseInfo.CreatedBy);
                }
                else
                    listy.Add(customData.CreatedBy);

                listy.Add(customData.TypeName);
                listy.Add(customData.CaseId.ToString());
                listy.Add(customData.CreatedDate.ToString());
                var geoLoc = ReverseGeocode.RetrieveFormatedAddress(customData.Latitude.ToString(), customData.Longitude.ToString(), getClientLic);
                listy.Add(geoLoc);

                if (caseInfo != null)
                {
                    if (!string.IsNullOrEmpty(caseInfo.Reason))
                        listy.Add(caseInfo.Reason);
                    else
                        listy.Add("N/A");
                    listy.Add(caseInfo.YearOfBirth);
                    listy.Add(caseInfo.Ethnicity);
                    listy.Add(caseInfo.PID);
                    listy.Add(caseInfo.Gender);
                    listy.Add(caseInfo.ListName);
                }
                else if (customData.EnrollmentResult != null)
                {
                    listy.Add(customData.EnrollmentResult.Reason);
                    listy.Add(customData.EnrollmentResult.YearOfBirth);
                    listy.Add(customData.EnrollmentResult.Ethnicity);
                    listy.Add(customData.EnrollmentResult.PID);
                    listy.Add(customData.EnrollmentResult.Gender);
                    listy.Add(customData.EnrollmentResult.ListName);
                }
                else
                {
                    listy.Add(customData.Reason);
                    listy.Add("N/A");
                    listy.Add("N/A");
                    listy.Add("N/A");
                    listy.Add("N/A");
                    listy.Add("N/A");
                }
                //if (customData.SentImageFile != null)
                //{
                //    string base64String2 = Convert.ToBase64String(customData.SentImageFile, 0, customData.SentImageFile.Length);
                //    listy.Add("data:image/png;base64," + base64String2);
                //}

                var settings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);
                var imgstring = settings.MIMSMobileAddress + "/Uploads/FaceRecognition/" + customData.Id.ToString() + ".jpeg";
                listy.Add(imgstring);

                if (customData.Request == (int)Verifier.RequestTypes.Verify)
                {
                    var resultImg = Verifier.GetOriginalImageByCase(customData.CaseId, dbConnection);
                    if (resultImg != null)
                    {
                        string base64String1 = Convert.ToBase64String(resultImg, 0, resultImg.Length);
                        listy.Add("data:image/png;base64," + base64String1);
                    }
                    //var runVerify = VerifierResults.GetVerifierResultByVerifierId(customData.Id, dbConnection);
                    //var runVerify = Verifier.VerifierMultipleResult(customData,dbConnection);
                    foreach (var verify in runVerify)
                    {
                        newCaseId.Add(verify.CaseId);
                        imgstring = settings.MIMSMobileAddress + "/Uploads/FaceRecognition/" + customData.Id.ToString() + "/" + verify.Percent + ".jpeg";
                        listy.Add(imgstring);

                        listy.Add(((int)Math.Round(Convert.ToSingle(verify.Percent) * (float)100)).ToString());
                    }
                }
                if (newCaseId[0] > 0)
                {
                    var newcaseInfo = Verifier.GetCaseInformationByCaseId(newCaseId[0], dbConnection);
                    if (!string.IsNullOrEmpty(newcaseInfo))
                    {
                        listy.Add(newcaseInfo);
                    }
                    else
                    {
                        listy.Add("N/A");
                    }
                    foreach (var caseid in newCaseId)
                    {
                        var casename = Verifier.GetCaseInformationByCaseId(caseid, dbConnection);

                        if (!string.IsNullOrEmpty(casename))
                        {
                            listy.Add(casename);
                            listy.Add(caseid.ToString());
                        }
                        else
                        {
                            listy.Add("N/A");
                            listy.Add(caseid.ToString());
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                MIMSLog.MIMSLogSave("GetAllVerifierService", "GetAllVerifierService", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static List<string> getEnrolledData(int id)
        {
            var listy = new List<string>();
            var customData = Enrollment.GetAllEnrollmentByCaseId(id, dbConnection);
            listy.Add(customData.CreatedBy);
            listy.Add(customData.TypeName);
            listy.Add(customData.CaseId.ToString());
            listy.Add(customData.CreatedDate.ToString());
            if (!string.IsNullOrEmpty(customData.Reason))
                listy.Add(customData.Reason);
            else
                listy.Add("N/A");
            listy.Add(customData.PID);
            listy.Add(customData.YearOfBirth);
            listy.Add(customData.Gender);
            listy.Add(customData.Ethnicity);
            listy.Add(customData.ListName);
            var resultImg = Verifier.GetOriginalImageByCase(customData.CaseId, dbConnection);
            if (resultImg != null)
            {
                string base64String1 = Convert.ToBase64String(resultImg, 0, resultImg.Length);
                listy.Add("data:image/png;base64," + base64String1);
            }
            else
            {
                listy.Add("Images/icon-user-default.png");
            }
            listy.Add(customData.Name);
            return listy;
        }

        [WebMethod]
        public static string rejectReDispatch(string id, string ins,string uname)
        {
            int taskId1 = 0;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var retVal = string.Empty;
            if (!string.IsNullOrEmpty(id))
            {
                var incidentinfo = CustomEvent.GetCustomEventById(Convert.ToInt32(id), dbConnection);
                incidentinfo.UpdatedBy = userinfo.Username;
                incidentinfo.IncidentStatus = CommonUtility.getIncidentStatusValue("reject");
                //incidentinfo.SiteId = userinfo.SiteId;
                incidentinfo.SessionId = System.Web.HttpContext.Current.Session.SessionID;
                incidentinfo.Password = Encrypt.EncryptData(userinfo.Password, true, dbConnection);
                var returnV = CommonUtility.CreateIncident(incidentinfo);
                if (returnV != "")
                {
                    taskId1 = Convert.ToInt32(returnV);
                    retVal = incidentinfo.Name;

                    if (incidentinfo.EventType == CustomEvent.EventTypes.MobileHotEvent)
                    {
                        //retVal = incidentinfo.EventTypeName;
                        var getMobName = MobileHotEvent.GetMobileHotEventByGuid(incidentinfo.Identifier, dbConnection);
                        {
                            if (string.IsNullOrEmpty(getMobName.EventTypeName))
                                retVal = "None";
                            else
                                retVal = getMobName.EventTypeName;
                        }
                    }

                    if (taskId1 < 1)
                    {
                        taskId1 = Convert.ToInt32(id);
                    }
                    var EventHistoryEntry = new EventHistory();
                    EventHistoryEntry.CreatedDate = CommonUtility.getDTNow();
                    EventHistoryEntry.CreatedBy = userinfo.Username;
                    EventHistoryEntry.EventId = taskId1;
                    EventHistoryEntry.IncidentAction = incidentinfo.IncidentStatus;
                    EventHistoryEntry.UserName = userinfo.Username;
                    EventHistoryEntry.Remarks = ins;
                    EventHistoryEntry.SiteId = userinfo.SiteId;
                    EventHistoryEntry.CustomerId = userinfo.CustomerInfoId;
                    EventHistory.InsertEventHistory(EventHistoryEntry, dbConnection, dbConnectionAudit, true);
                    
                    if (incidentinfo.IncidentStatus == (int)CustomEvent.IncidentActionStatus.Reject)
                    {
                        var notifications = Arrowlabs.Business.Layer.Notification.GetAllNotificationsByIncidentId(incidentinfo.EventId, dbConnection);
                        foreach (var noti in notifications)
                        {
                            if (noti.AssigneeType == (int)TaskAssigneeType.User)
                            {
                                var taskuser = Users.GetUserById(noti.AssigneeId, dbConnection);
                                taskuser.Engaged = false;
                                taskuser.EngagedIn = string.Empty;
                                Users.InsertOrUpdateUsers(taskuser, dbConnection, dbConnectionAudit,true);
                            }
                            else if (noti.AssigneeType == (int)TaskAssigneeType.Group)
                            {
                                var groupusers = Users.GetAllUserByGroupId(noti.AssigneeId, dbConnection);
                                foreach (var taskuser in groupusers)
                                {
                                    var splitinfo = taskuser.EngagedIn.Split('-');
                                    if (splitinfo.Length > 1)
                                    {
                                        if (splitinfo[1] == noti.Id.ToString())
                                        {
                                            taskuser.Engaged = false;
                                            taskuser.EngagedIn = string.Empty;
                                            Users.InsertOrUpdateUsers(taskuser, dbConnection, dbConnectionAudit,true);
                                        }
                                    }
                                }
                            }
                        }
                        Arrowlabs.Business.Layer.Notification.UpdateNotificationStatusByIncidentId(incidentinfo.EventId, false, dbConnection);
                        var taskinfo = UserTask.GetAllTaskByIncidentId(incidentinfo.EventId, dbConnection);
                        foreach (var task in taskinfo)
                        {
                            task.Status = (int)TaskStatus.Pending;
                            task.RejectionNotes = ins;
                            UserTask.InsertorUpdateTask(task, dbConnection, dbConnectionAudit, true);
                        }
                    }
                    incidentinfo = CustomEvent.GetCustomEventById(Convert.ToInt32(id), dbConnection);
                    incidentinfo.UpdatedBy = userinfo.Username;
                    incidentinfo.IncidentStatus = CommonUtility.getIncidentStatusValue("dispatch");
                    incidentinfo.SiteId = userinfo.SiteId;
                    incidentinfo.CustomerId = userinfo.CustomerInfoId;
                    incidentinfo.SessionId = System.Web.HttpContext.Current.Session.SessionID;
                    incidentinfo.Password = Encrypt.EncryptData(userinfo.Password, true, dbConnection);
                    returnV = CommonUtility.CreateIncident(incidentinfo);
                    if (returnV != "")
                    {
                        EventHistoryEntry.CreatedDate = CommonUtility.getDTNow();
                        EventHistoryEntry.CreatedBy = userinfo.Username;
                        EventHistoryEntry.EventId = taskId1;
                        EventHistoryEntry.IncidentAction = incidentinfo.IncidentStatus;
                        EventHistoryEntry.UserName = userinfo.Username;
                        EventHistoryEntry.Remarks = ins;
                        EventHistoryEntry.SiteId = userinfo.SiteId;
                        EventHistoryEntry.CustomerId = userinfo.CustomerInfoId;
                        EventHistory.InsertEventHistory(EventHistoryEntry, dbConnection, dbConnectionAudit, true);
                        
                        retVal = incidentinfo.Name + " has been redispatched!";
                    }
                    else
                    {
                        retVal = "";
                    }
                }
            }
            return retVal;
        }

        [WebMethod]
        public static string markReminderAsRead(int id,string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                var reminderInfo = Reminders.GetReminderById(id, dbConnection);
                reminderInfo.IsCompleted = true;
                Reminders.InsertorUpdateReminder(reminderInfo, dbConnection, dbConnectionAudit,true);
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("OthersDash.aspx", "markReminderAsRead", ex, dbConnection, userinfo.SiteId);
                return ex.Message;
            }
            return "SUCCESS";
        }

        [WebMethod]
        public static List<string> getServerData(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }
                else
                {
                    var licenseInfo = ClientLicence.GetLicenseByClientID(CommonUtility.arrowlabsKey, dbConnection);
                    var getalldevs = Device.GetClientDeviceByClientID(CommonUtility.arrowlabsKey, dbConnection);
                    var devinuse = 0;
                    foreach (var dev in getalldevs)
                    {
                        if (dev.State == Arrowlabs.Business.Layer.Device.DeviceState.Enable.ToString())
                            devinuse++;
                    }
                    var users = Users.GetAllUsersByCustomerId(userinfo.CustomerInfoId, dbConnection).Count;//Users.GetAllMobileOnlineUsers(dbConnection);//LoginSession.GetAllMimsMobileOnlineByDeviceType(1, dbConnection);
                    var cInfo = CustomerInfo.GetCustomerInfoById(userinfo.CustomerInfoId, dbConnection);
                    if (licenseInfo != null)
                    {
                        var remaining = (cInfo.TotalUser - users).ToString();
                        listy.Add("N/A");
                        listy.Add("N/A");
                        listy.Add("N/A");
                        listy.Add("N/A");
                        listy.Add("N/A");
                        listy.Add("N/A");
                        listy.Add("N/A"); //Remaining Client
                        listy.Add("N/A"); //Total Client
                        listy.Add("N/A"); //Used Client
                        listy.Add(remaining); // Remaining Mobile
                        listy.Add(cInfo.TotalUser.ToString());//licenseInfo.TotalMobileUsers); //Total Mobile
                        listy.Add(users.ToString()); // Used
                        //licenseInfo.RemainingMobileUsers = remaining;
                        //licenseInfo.UsedMobileUsers = users.ToString();

                        var modules = cInfo.Modules.Split('?');

                        //listy.Add(licenseInfo.isSurveillance.ToString().ToLower());
                        var isSurv = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Surveillance).ToString()).ToList();
                        if (isSurv != null && isSurv.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isNotification.ToString().ToLower());//CHANGED TO MESAGEBOARD
                        var isNoti = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.MessageBoard).ToString()).ToList();
                        if (isNoti != null && isNoti.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isLocation.ToString().ToLower()); //CHANGED TO MESAGEBOARD
                        var isLoc = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Contract).ToString()).ToList();
                        if (isLoc != null && isLoc.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isTicketing.ToString().ToLower());
                        var isTicket = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Ticketing).ToString()).ToList();
                        if (isTicket != null && isTicket.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isTask.ToString().ToLower());
                        var isTask = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Task).ToString()).ToList();
                        if (isTask != null && isTask.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isIncident.ToString().ToLower());
                        var isInci = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Incident).ToString()).ToList();
                        if (isInci != null && isInci.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isWarehouse.ToString().ToLower());
                        var isWare = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Warehouse).ToString()).ToList();
                        if (isWare != null && isWare.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");


                        //listy.Add(licenseInfo.isChat.ToString().ToLower());
                        var isChat = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Chat).ToString()).ToList();
                        if (isChat != null && isChat.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isCollaboration.ToString().ToLower());
                        var isCollab = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Collaboration).ToString()).ToList();
                        if (isCollab != null && isCollab.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isLostandFound.ToString().ToLower());
                        var isLF = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.LostandFound).ToString()).ToList();
                        if (isLF != null && isLF.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");


                        //listy.Add(licenseInfo.isDutyRoster.ToString().ToLower());
                        var isDR = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.DutyRoster).ToString()).ToList();
                        if (isDR != null && isDR.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isPostOrder.ToString().ToLower());
                        var isPO = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.PostOrder).ToString()).ToList();
                        if (isPO != null && isPO.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isVerification.ToString().ToLower());
                        var isVeri = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Verification).ToString()).ToList();
                        if (isVeri != null && isVeri.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isRequest.ToString().ToLower());
                        var isRequest = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Request).ToString()).ToList();
                        if (isRequest != null && isRequest.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");


                        //listy.Add(licenseInfo.isDispatch.ToString().ToLower());
                        var isDisp = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Dispatch).ToString()).ToList();
                        if (isDisp != null && isDisp.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        var isAct = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Activity).ToString()).ToList();
                        if (isAct != null && isAct.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //ClientLicence.InsertClientLicence(licenseInfo, dbConnection, dbConnectionAudit, true);
                        listy.Add(cInfo.Country);
                    }
                }
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("Devices", "getServerData", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static string saveTZ(string id, string uname)
        {
            var json = string.Empty;
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {
                    var cInfo = CustomerInfo.GetCustomerInfoById(userinfo.CustomerInfoId, dbConnection);
                    if (cInfo != null)
                    {
                        var split = id.Split('(');
                        var cname = split[0];
                        var spli2 = split[1].Split(')');
                        var doubles = spli2[0];
                        var ctimezone = Convert.ToDouble(doubles.Split(':')[0]);

                        cInfo.Country = cname;
                        cInfo.TimeZone = ctimezone;
                        var latlng = ReverseGeocode.RetrieveFormatedGeo(cname);
                        if (latlng.Count > 0)
                        {
                            cInfo.Lati = latlng[0];
                            cInfo.Long = latlng[1];
                        }
                        CustomerInfo.InsertorUpdateCustomerInfo(cInfo, dbConnection);

                        return "SUCCESS";
                    }
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("Tasks.aspx", "deleteSystemType", ex, dbConnection, userinfo.SiteId);
                    return ex.Message;
                }
                return json;
            }
        }
         

        [WebMethod]
        public static List<string> geTicketHistoryData(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                var allEventHistory = EventHistory.GetEventHistoryByEventId(Convert.ToInt32(id), dbConnection); //new List<CustomEvent>();
                var alltasks = UserTask.GetAllTaskByIncidentId(Convert.ToInt32(id), dbConnection);
                if (alltasks.Count > 0)
                {
                    foreach (var item in alltasks)
                    {
                        var alltaskhistory = TaskEventHistory.GetTaskEventHistoryByTaskId(item.Id, dbConnection);

                        var taskeventhistory = new List<TaskEventHistory>();

                        var tasklinks = UserTask.GetAllTasksByTaskLinkId(Convert.ToInt32(item.Id), dbConnection);
                        if (tasklinks.Count > 0)
                        {
                            foreach (var link in tasklinks)
                            {
                                var forlink = TaskEventHistory.GetTaskEventHistoryByTaskId(link.Id, dbConnection);
                                foreach (var forl in forlink)
                                {
                                    forl.isSubTask = true;
                                    taskeventhistory.Add(forl);
                                }
                            }
                            taskeventhistory = taskeventhistory.OrderByDescending(i => i.CreatedDate).ToList();
                        }

                        alltaskhistory.AddRange(taskeventhistory);

                        if (alltaskhistory.Count > 0)
                        {
                            foreach (var task in alltaskhistory)
                            {
                                if (task.Action != (int)TaskAction.Update)
                                {
                                    var evhistory = new EventHistory();
                                    if (task.isSubTask)
                                    {
                                        evhistory.isSubTask = true;
                                    }
                                    else
                                        evhistory.isTask = true;
                                    evhistory.TaskName = task.TaskName;
                                    if (task.Action == (int)TaskAction.Pending)
                                        evhistory.IncidentAction = (int)CustomEvent.IncidentActionStatus.Pending;
                                    else if (task.Action == (int)TaskAction.InProgress)
                                        evhistory.IncidentAction = (int)CustomEvent.IncidentActionStatus.Engage;
                                    else if (task.Action == (int)TaskAction.OnRoute)
                                        evhistory.IncidentAction = (int)CustomEvent.IncidentActionStatus.OnRoute;
                                    else if (task.Action == (int)TaskAction.Pause)
                                        evhistory.IncidentAction = (int)CustomEvent.IncidentActionStatus.Pause;
                                    else if (task.Action == (int)TaskAction.Resume)
                                        evhistory.IncidentAction = (int)CustomEvent.IncidentActionStatus.Resume;
                                    else if (task.Action == (int)TaskAction.Complete)
                                        evhistory.IncidentAction = (int)CustomEvent.IncidentActionStatus.Complete;
                                    else if (task.Action == (int)TaskAction.Accepted)
                                        evhistory.IncidentAction = (int)CustomEvent.IncidentActionStatus.Resolve;
                                    else if (task.Action == (int)TaskAction.Rejected)
                                        evhistory.IncidentAction = (int)CustomEvent.IncidentActionStatus.Reject;

                                    evhistory.CustomerUName = task.CustomerUName;
                                    evhistory.ACustomerUName = task.CustomerUName;
                                    evhistory.CreatedDate = task.CreatedDate;
                                    evhistory.Latitude = task.Latitude;
                                    evhistory.Longtitude = task.Longtitude;
                                    allEventHistory.Add(evhistory);
                                }
                                if (item.AssigneeType == (int)TaskAssigneeType.Group)
                                {
                                    if (task.Action == (int)TaskAction.Update)
                                    {
                                        var evhistory = new EventHistory();
                                        if (task.isSubTask)
                                        {
                                            evhistory.isSubTask = true;
                                        }
                                        else
                                            evhistory.isTask = true;

                                        evhistory.TaskName = task.TaskName;
                                        evhistory.IncidentAction = (int)CustomEvent.IncidentActionStatus.Updated;
                                        evhistory.CustomerUName = task.CustomerUName;
                                        evhistory.ACustomerUName = task.CustomerUName;
                                        evhistory.CreatedDate = task.CreatedDate;
                                        evhistory.Latitude = task.Latitude;
                                        evhistory.Longtitude = task.Longtitude;
                                        allEventHistory.Add(evhistory);
                                    }
                                }
                            }
                        }
                    }
                }
                allEventHistory = allEventHistory.OrderByDescending(i => i.CreatedDate).ToList();

                foreach (var rem in allEventHistory)
                {
                    var remname = string.Empty;
                    if (rem.EventId > 0)
                    {
                        var gethotevent = CustomEvent.GetCustomEventById(rem.EventId, dbConnection);

                        if (gethotevent != null)
                            gethotevent.Name = "Ticket";

                        remname = gethotevent.Name;
                    }
                    if (rem.isTask)
                    {
                        if (!string.IsNullOrEmpty(rem.TaskName))
                            remname = rem.TaskName;
                        else
                            remname = "Task";

                    }
                    else if (rem.isSubTask)
                    {
                        if (!string.IsNullOrEmpty(rem.TaskName))
                            remname = rem.TaskName;
                        else
                            remname = "Sub Task";
                    }
                    var actioninfo = string.Empty;
                    var returnstring = string.Empty;
                    var incidentLocation = string.Empty;
                    if (!string.IsNullOrEmpty(rem.Longtitude) && !string.IsNullOrEmpty(rem.Latitude))
                        incidentLocation = "from " + ReverseGeocode.RetrieveFormatedAddress(rem.Latitude.ToString(), rem.Longtitude.ToString(), getClientLic);

                    if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Pending)
                    {
                        actioninfo = "created ";
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.CustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + remname + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Complete)
                    {
                        actioninfo = "completed ";
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.ACustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + remname + "</span>" + incidentLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Dispatch)
                    {
                        actioninfo = "dispatched ";
                        incidentLocation = "to " + rem.ACustomerUName;
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.CustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + remname + "</span>" + incidentLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Resolve)
                    {
                        actioninfo = "resolved ";
                        incidentLocation = "";
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.CustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + remname + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Reject)
                    {
                        actioninfo = "rejected ";
                        incidentLocation = "";
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.CustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + remname + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Park)
                    {
                        actioninfo = "parked ";
                        incidentLocation = "";
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.CustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + remname + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.OnRoute)
                    {
                        actioninfo = "onroute ";
                        incidentLocation = "";
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.CustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + remname + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Pause)
                    {
                        actioninfo = "paused ";
                        incidentLocation = "";
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.CustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + remname + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Resume)
                    {
                        actioninfo = "resumed ";
                        incidentLocation = "";
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.CustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + remname + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Engage)
                    {
                        actioninfo = "started ";
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.ACustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + remname + "</span>" + incidentLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Release)
                    {
                        actioninfo = "released ";
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.ACustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + remname + "</span>" + incidentLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Arrived)
                    {
                        actioninfo = "arrived to incident ";

                        incidentLocation = "at " + ReverseGeocode.RetrieveFormatedAddress(rem.Latitude.ToString(), rem.Longtitude.ToString(), getClientLic);

                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.ACustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + remname + "</span>" + incidentLocation + "</p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Escalated)
                    {
                        actioninfo = "escalated ";
                        incidentLocation = "";
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.CustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + remname + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Updated)
                    {
                        actioninfo = "updated ";
                        incidentLocation = "";
                        returnstring = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + rem.CustomerUName + "</span>" + actioninfo + "<span class='red-color'>" + remname + "</span></p></div><div class='vertical-activity-line'></div><i class='fa fa-circle absoult-center-left green-color'></i>";
                        listy.Add(returnstring);
                    }
                }
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("Ticketing", "geTicketHistoryData", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static List<string> getTicketRemarksData(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var projinfo = CustomEvent.GetCustomEventById(id, dbConnection);
                if (projinfo != null)
                {
                    var remarksList = CustomEventRemarks.GetCustomEventRemarksByEventId(id, dbConnection);


                    if (remarksList.Count > 0)
                    {
                        remarksList = remarksList.OrderByDescending(i => i.CreatedDate).ToList();
                        var count = 0;
                        foreach (var task in remarksList)
                        {
                            if (count == 3)
                            {
                                break;
                            }
                            count++;
                            var OGremarks = task.Remarks;
                            if (!string.IsNullOrEmpty(task.Remarks) && task.Remarks.Length > 50)
                            {
                                task.Remarks = task.Remarks.Remove(50);
                                task.Remarks = task.Remarks + "...";
                            }
                            var acceptedData = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + task.CustomerUName + " wrote :</span>" + task.Remarks + "</p></div><i class='fa fa-sticky-note absoult-center-left' style='color:#bbbbbb;margin-top:-1px;' onmouseover='style=&apos;cursor: pointer;margin-top:-1px;color:#bbbbbb;&apos;' onclick='showRemarks(&apos;" + OGremarks + "&apos;)'></i>";
                            listy.Add(acceptedData);
                        }
                        if (remarksList.Count > 3)
                        {
                            var seeall = "<h5><span  class='line-center' onmouseover='style=&apos;cursor: pointer;' onclick='showAllTicketingRemarks(&apos;" + id + "&apos;)'>SEE ALL</span></h5>";
                            listy.Add(seeall);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Ticketing", "getTicketRemarksData", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static List<string> getTicketRemarksData2(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var projinfo = CustomEvent.GetCustomEventById(id, dbConnection);
                if (projinfo != null)
                {
                    var remarksList = CustomEventRemarks.GetCustomEventRemarksByEventId(id, dbConnection);


                    if (remarksList.Count > 0)
                    {
                        remarksList = remarksList.OrderByDescending(i => i.CreatedDate).ToList();
                        foreach (var task in remarksList)
                        {
                            var OGremarks = task.Remarks;
                            var acceptedData = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + task.CustomerUName + " wrote :</span>" + task.Remarks + "</p></div><i class='fa fa-sticky-note absoult-center-left' style='color:#bbbbbb;margin-top:-1px;' onmouseover='style=&apos;cursor: pointer;margin-top:-1px;color:#bbbbbb;&apos;' onclick='showRemarks(&apos;" + OGremarks + "&apos;)'></i>";
                            listy.Add(acceptedData);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Ticketing", "getTicketRemarksData2", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static string addNewTicketingRemarks(int id, string notes, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var json = string.Empty;
            try
            {
                var projinfo = CustomEvent.GetCustomEventById(id, dbConnection);
                if (projinfo != null)
                {
                    var newRemarks = new CustomEventRemarks();
                    newRemarks.CreatedBy = userinfo.Username;
                    newRemarks.CreatedDate = CommonUtility.getDTNow();
                    newRemarks.EventId = id;
                    newRemarks.Remarks = notes;
                    newRemarks.UpdatedBy = userinfo.Username;
                    newRemarks.UpdatedDate = CommonUtility.getDTNow();
                    newRemarks.CustomerInfoId = userinfo.CustomerInfoId;
                    newRemarks.SiteId = userinfo.SiteId;
                    var retV = CustomEventRemarks.InsertOrUpdateCustomEventRemarks(newRemarks, dbConnection, dbConnectionAudit, true);
                    if (retV > 0)
                    {
                        json = "SUCCESS";
                    }
                    else
                    {
                        json = "Problem faced trying to save notes";
                    }
                }
                else
                {
                    json = "Problem faced trying to get ticket and save notes";
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Ticketing", "addNewTicketingRemarks", err, dbConnection, userinfo.SiteId);
                json = err.Message;
            }
            return json;
        }
        [WebMethod]
        public static string deleteAttachmentDataTicket(int id, string uname)
        {
            var listy = string.Empty;
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    var ret = Arrowlabs.Business.Layer.MobileHotEventAttachment.GetMobileHotEventAttachmentById(id, dbConnection);
                    if (ret != null)
                    {
                        ret = ret.Where(i => i.Id == id).ToList();
                        if (ret.Count > 0)
                        {
                            MobileHotEventAttachment.DeleteMobileHotEventAttachmentById(id, dbConnection);

                            SystemLogger.SaveSystemLog(dbConnectionAudit, "Attachment-" + id, id.ToString(), "MobileHotEventAttachment", userinfo, "Attachment was deleted on_" + CommonUtility.getDTNow());

                            CommonUtility.CloudDeleteFile(ret[0].AttachmentPath);

                            listy = "Successfully deleted entry";
                        }
                        else
                            listy = "Failed to delete entry";
                    }
                    else
                        listy = "Failed to delete entry";
                }
                catch (Exception err)
                {
                    MIMSLog.MIMSLogSave("Tasks", "deleteAttachmentData", err, dbConnection, userinfo.SiteId);
                    listy = err.Message;
                }
                return listy;
            }
        }

        [WebMethod]
        public static string CreatePDFTicket(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    if (id > 0)
                    {
                        var cusevent = CustomEvent.GetCustomEventById(id, dbConnection);
                        if (cusevent != null)
                            return GeneratePDFTicket(cusevent, userinfo);
                    }
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("Ticket", "CreatePDFTicket", ex, dbConnection, userinfo.SiteId);
                }
                return "Failed to generate report!";
            }
        }
        static string fpath;
        private static string GeneratePDFTicket(CustomEvent cusevent, Users userinfo)
        {
            string fileName = string.Empty;
            try
            {
                iTextSharp.text.Document doc = new iTextSharp.text.Document(PageSize.LETTER, 25F, 25F, 50F, 25F);

                doc.SetMargins(25, 25, 65, 35);

                fileName = CommonUtility.getDTNow().Day.ToString() + "-" + CommonUtility.getDTNow().Month.ToString() + "-" + CommonUtility.getDTNow().Year.ToString() + "-" + CommonUtility.getDTNow().Hour.ToString() + "-" + CommonUtility.getDTNow().Minute.ToString() + "-" + CommonUtility.getDTNow().Second.ToString() + ".pdf";
                var path = fpath + fileName;

                PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(path, FileMode.Create));
                writer.PageEvent = new ITextEvents()
                {
                    cid = userinfo.CustomerInfoId
                };

                doc.Open();
                BaseFont f_cn = BaseFont.CreateFont(Environment.GetFolderPath(Environment.SpecialFolder.Fonts) + "\\verdana.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                BaseFont h_cn = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                BaseFont z_cn = BaseFont.CreateFont(BaseFont.ZAPFDINGBATS, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                Font times = new Font(f_cn, 12, Font.NORMAL, Color.BLACK);
                Font btimes = new Font(f_cn, 12, Font.BOLD, Color.BLACK);

                iTextSharp.text.Color arrowred = new iTextSharp.text.Color(162, 0, 46);

                Font eleventimes = new Font(f_cn, 11, Font.NORMAL, Color.BLACK);
                Font weleventimes = new Font(f_cn, 11, Font.NORMAL, Color.WHITE);

                Font gweb = new Font(z_cn, 11, Font.NORMAL, Color.GREEN);
                Font rweb = new Font(z_cn, 11, Font.NORMAL, arrowred);

                Font ueleventimes = new Font(f_cn, 11, Font.UNDERLINE, Color.BLACK);

                Font beleventimes = new Font(f_cn, 11, Font.BOLD, Color.BLACK);
                Font bredeleventimes = new Font(f_cn, 11, Font.BOLD, arrowred);
                Font redeleventimes = new Font(f_cn, 11, Font.NORMAL, arrowred);
                Font twelvetimes = new Font(f_cn, 12, Font.ITALIC, Color.BLACK);

                Font eleventimesI = new Font(f_cn, 11, Font.ITALIC, Color.BLACK);

                Font mbtimes = new Font(h_cn, 18, Font.BOLD, arrowred);
                Font cbtimes = new Font(h_cn, 13, Font.BOLD, arrowred);

                float[] columnWidthsH = new float[] { 30, 60, 10 };

                var mainheadertable = new PdfPTable(3);
                mainheadertable.DefaultCell.Border = Rectangle.NO_BORDER;
                mainheadertable.WidthPercentage = 100;
                mainheadertable.SetWidths(columnWidthsH);
                var newTRIlist = new List<TimelineReportItems>();

                var headerMain = new PdfPCell();
                if (userinfo.CustomerInfoId == 87 || userinfo.CustomerInfoId == 45)
                {
                    headerMain.AddElement(new Phrase("G4S TICKET REPORT", mbtimes));
                }
                else
                {
                    headerMain.AddElement(new Phrase("MIMS TICKET REPORT", mbtimes));
                }
                headerMain.PaddingBottom = 10;
                headerMain.HorizontalAlignment = 1;
                headerMain.Border = Rectangle.NO_BORDER;
                PdfPCell pdfCell1 = new PdfPCell();
                pdfCell1.Border = Rectangle.NO_BORDER;
                PdfPCell pdfCell2 = new PdfPCell();
                pdfCell2.Border = Rectangle.NO_BORDER;
                mainheadertable.AddCell(pdfCell1);
                mainheadertable.AddCell(headerMain);
                mainheadertable.AddCell(pdfCell2);

                var taskdataTable = new PdfPTable(2);
                var taskdataTableA = new PdfPTable(2);
                var taskdataTableB = new PdfPTable(2);
                taskdataTable.DefaultCell.Border = Rectangle.NO_BORDER;
                taskdataTable.WidthPercentage = 100;

                taskdataTableA.DefaultCell.Border = Rectangle.NO_BORDER;
                taskdataTableB.DefaultCell.Border = Rectangle.NO_BORDER;

                taskdataTableA.WidthPercentage = 100;
                taskdataTableB.WidthPercentage = 100;

                float[] columnWidths2575 = new float[] { 25, 75 };
                float[] columnWidths3070 = new float[] { 30, 70 };
                float[] columnWidths3565 = new float[] { 35, 65 };
                float[] columnWidths4060 = new float[] { 40, 60 };
                taskdataTableA.SetWidths(columnWidths4060);

                taskdataTableB.SetWidths(columnWidths4060);
                var driverOffence = DriverOffence.GetDriverOffenceById(cusevent.Identifier, dbConnection);
                if (driverOffence != null)
                {

                }
                //SIDE A

                if (driverOffence.CusId > 0)
                {
                    var header0 = new PdfPCell();
                    header0.AddElement(new Phrase("Account:", beleventimes));
                    header0.Border = Rectangle.NO_BORDER;
                    taskdataTableA.AddCell(header0);

                    var header0a = new PdfPCell();
                    header0a.AddElement(new Phrase(driverOffence.CusName, eleventimes));
                    header0a.Border = Rectangle.NO_BORDER;
                    taskdataTableA.AddCell(header0a);
                }
                if (driverOffence.ContractId > 0)
                {
                    var header00 = new PdfPCell();
                    header00.AddElement(new Phrase("Contract:", beleventimes));
                    header00.Border = Rectangle.NO_BORDER;
                    taskdataTableA.AddCell(header00);

                    var header00a = new PdfPCell();
                    header00a.AddElement(new Phrase(driverOffence.ContractName, eleventimes));
                    header00a.Border = Rectangle.NO_BORDER;
                    taskdataTableA.AddCell(header00a);
                }
                var header1 = new PdfPCell();
                header1.AddElement(new Phrase("Category:", beleventimes));
                header1.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header1);

                var header1a = new PdfPCell();
                header1a.AddElement(new Phrase(driverOffence.OffenceCategory, eleventimes));
                header1a.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header1a);

                var header2 = new PdfPCell();
                header2.AddElement(new Phrase("Type:", beleventimes));
                header2.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header2);

                if (string.IsNullOrEmpty(driverOffence.OffenceType))
                    driverOffence.OffenceType = "N/A";

                var header2a = new PdfPCell();
                header2a.AddElement(new Phrase(driverOffence.OffenceType, eleventimes));
                header2a.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header2a);

                var subNameTable = new PdfPTable(1);
                var header000 = new PdfPCell();
                header000.AddElement(new Phrase("Item:", beleventimes));
                header000.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header000);

                if (!string.IsNullOrEmpty(driverOffence.Offence1))
                {
                    var header000a = new PdfPCell();
                    header000a.AddElement(new Phrase(driverOffence.Offence1, eleventimes));
                    header000a.Border = Rectangle.NO_BORDER;
                    subNameTable.AddCell(header000a);
                }
                if (!string.IsNullOrEmpty(driverOffence.Offence2))
                {
                    var header000a = new PdfPCell();
                    header000a.AddElement(new Phrase(driverOffence.Offence2, eleventimes));
                    header000a.Border = Rectangle.NO_BORDER;
                    subNameTable.AddCell(header000a);
                }
                if (!string.IsNullOrEmpty(driverOffence.Offence3))
                {
                    var header000a = new PdfPCell();
                    header000a.AddElement(new Phrase(driverOffence.Offence3, eleventimes));
                    header000a.Border = Rectangle.NO_BORDER;
                    subNameTable.AddCell(header000a);
                }
                if (!string.IsNullOrEmpty(driverOffence.Offence4))
                {
                    var header000a = new PdfPCell();
                    header000a.AddElement(new Phrase(driverOffence.Offence4, eleventimes));
                    header000a.Border = Rectangle.NO_BORDER;
                    subNameTable.AddCell(header000a);
                }
                taskdataTableA.AddCell(subNameTable);

                var header3 = new PdfPCell();
                header3.AddElement(new Phrase("Created Date:", beleventimes));
                header3.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header3);

                var header3a = new PdfPCell();
                header3a.AddElement(new Phrase(cusevent.RecevieTime.Value.AddHours(userinfo.TimeZone).ToString(), eleventimes));
                header3a.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header3a);

                var header4 = new PdfPCell();
                header4.AddElement(new Phrase("Created by:", beleventimes));
                header4.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header4);

                var header4a = new PdfPCell();
                header4a.AddElement(new Phrase(cusevent.CustomerFullName, eleventimes));
                header4a.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header4a);

                var header8 = new PdfPCell();
                header8.AddElement(new Phrase("Description:", beleventimes));
                header8.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header8);

                var header8a = new PdfPCell();
                header8a.AddElement(new Phrase(driverOffence.Comments, eleventimes));
                header8a.Border = Rectangle.NO_BORDER;
                taskdataTableA.AddCell(header8a);

                //SIDE B

                var headerB2 = new PdfPCell();
                headerB2.AddElement(new Phrase("Status:", beleventimes));
                headerB2.Border = Rectangle.NO_BORDER;
                taskdataTableB.AddCell(headerB2);
                if (cusevent.Handled)
                {
                    cusevent.StatusName = "Resolved";
                }
                else if (driverOffence.TicketStatus == (int)CustomEvent.IncidentActionStatus.Engage)
                {
                    cusevent.StatusName = "Inprogress";
                }
                else if (driverOffence.TicketStatus == (int)CustomEvent.IncidentActionStatus.Complete)
                {
                    cusevent.StatusName = "Completed";
                }

                var headerB2a = new PdfPCell();
                headerB2a.AddElement(new Phrase(cusevent.StatusName, eleventimes));
                headerB2a.Border = Rectangle.NO_BORDER;
                taskdataTableB.AddCell(headerB2a);

                var headerB1 = new PdfPCell();
                headerB1.AddElement(new Phrase("Business Unit:", beleventimes));
                headerB1.Border = Rectangle.NO_BORDER;
                taskdataTableB.AddCell(headerB1);


                var siteinfo = Arrowlabs.Business.Layer.Site.GetSiteById(cusevent.SiteId, dbConnection);
                if (siteinfo != null)
                {
                    var headerB1a = new PdfPCell();
                    headerB1a.AddElement(new Phrase(siteinfo.Name, eleventimes));
                    headerB1a.Border = Rectangle.NO_BORDER;
                    taskdataTableB.AddCell(headerB1a);
                }
                else
                {
                    var headerB1a = new PdfPCell();
                    headerB1a.AddElement(new Phrase("N/A", eleventimes));
                    headerB1a.Border = Rectangle.NO_BORDER;
                    taskdataTableB.AddCell(headerB1a);
                }

                if (cusevent.Handled)
                {
                    var headerB4 = new PdfPCell();
                    headerB4.AddElement(new Phrase("Resolved By:", beleventimes));
                    headerB4.Border = Rectangle.NO_BORDER;
                    headerB4.PaddingBottom = 20;
                    taskdataTableB.AddCell(headerB4);

                    var headerB4a = new PdfPCell();
                    headerB4a.AddElement(new Phrase(cusevent.CustomerFullName, eleventimes));
                    headerB4a.Border = Rectangle.NO_BORDER;
                    taskdataTableB.AddCell(headerB4a);

                }

                taskdataTable.AddCell(taskdataTableA);
                taskdataTable.AddCell(taskdataTableB);

                var maptextdataTable = new PdfPTable(1);
                maptextdataTable.DefaultCell.Border = Rectangle.NO_BORDER;
                maptextdataTable.WidthPercentage = 100;

                var geolocation = ReverseGeocode.RetrieveFormatedAddress(cusevent.Latitude.ToString(), cusevent.Longtitude.ToString(), getClientLic);
                var geofence = GeofenceLocation.GetAllGeofenceLocationbyIncidentId(dbConnection, cusevent.EventId);
                if (string.IsNullOrEmpty(geolocation))
                {
                    if (geofence.Count > 0)
                    {
                        geolocation = ReverseGeocode.RetrieveFormatedAddress(geofence[0].Latitude.ToString(), geofence[0].Longitude.ToString(), getClientLic);
                        cusevent.Longtitude = geofence[0].Longitude.ToString();
                        cusevent.Latitude = geofence[0].Latitude.ToString();

                    }
                }

                if (string.IsNullOrEmpty(geolocation))
                    geolocation = "Not Specified";

                var mapheader = new PdfPCell();
                mapheader.AddElement(new Phrase("Location:" + geolocation, beleventimes));
                mapheader.Border = Rectangle.NO_BORDER;
                maptextdataTable.AddCell(mapheader);

                var latComp = string.Empty;
                var longComp = string.Empty;
                var latEng = string.Empty;
                var longEng = string.Empty;
                var arrived = string.Empty;
                var vargeopath = string.Empty; //&path=color:0xff0000ff|weight:5|25.09773,55.16316|25.0978512224338,55.1634863298386
                var tracebackpath = string.Empty; //&path=color:blue|weight:3|25.09773,55.16316|25.0978512224338,55.1634863298386


                var incidenteventhistory = EventHistory.GetEventHistoryByEventId(cusevent.EventId, dbConnection);
                var completedBy = string.Empty;
                var inprogressby = string.Empty;
                var acceptedData = string.Empty;
                var rejectedData = string.Empty;
                var assignedData = string.Empty;
                var utask = UserTask.GetAllTaskByIncidentId(cusevent.EventId, dbConnection);
                if (utask.Count > 0)
                {
                    foreach (var item in utask)
                    {
                        var alltaskhistory = TaskEventHistory.GetTaskEventHistoryByTaskId(item.Id, dbConnection);
                        if (alltaskhistory.Count > 0)
                        {
                            foreach (var task in alltaskhistory)
                            {
                                if (task.Action != (int)TaskAction.Update)
                                {
                                    var evhistory = new EventHistory();

                                    evhistory.isTask = true;
                                    evhistory.TaskName = task.TaskName;
                                    if (task.Action == (int)TaskAction.Pending)
                                        evhistory.IncidentAction = (int)CustomEvent.IncidentActionStatus.Pending;
                                    else if (task.Action == (int)TaskAction.InProgress)
                                        evhistory.IncidentAction = (int)CustomEvent.IncidentActionStatus.Engage;
                                    else if (task.Action == (int)TaskAction.OnRoute)
                                        evhistory.IncidentAction = (int)CustomEvent.IncidentActionStatus.OnRoute;
                                    else if (task.Action == (int)TaskAction.Pause)
                                        evhistory.IncidentAction = (int)CustomEvent.IncidentActionStatus.Pause;
                                    else if (task.Action == (int)TaskAction.Resume)
                                        evhistory.IncidentAction = (int)CustomEvent.IncidentActionStatus.Resume;
                                    else if (task.Action == (int)TaskAction.Complete)
                                        evhistory.IncidentAction = (int)CustomEvent.IncidentActionStatus.Complete;
                                    else if (task.Action == (int)TaskAction.Accepted)
                                        evhistory.IncidentAction = (int)CustomEvent.IncidentActionStatus.Resolve;
                                    else if (task.Action == (int)TaskAction.Rejected)
                                        evhistory.IncidentAction = (int)CustomEvent.IncidentActionStatus.Reject;

                                    evhistory.CustomerFullName = task.CustomerFullName;
                                    evhistory.ACustomerFullName = task.CustomerFullName;
                                    evhistory.CreatedDate = task.CreatedDate;
                                    evhistory.Latitude = task.Latitude;
                                    evhistory.Longtitude = task.Longtitude;
                                    incidenteventhistory.Add(evhistory);
                                }
                                if (item.AssigneeType == (int)TaskAssigneeType.Group)
                                {
                                    if (task.Action == (int)TaskAction.Update)
                                    {
                                        var evhistory = new EventHistory();
                                        evhistory.isTask = true;
                                        evhistory.TaskName = task.TaskName;
                                        evhistory.IncidentAction = (int)CustomEvent.IncidentActionStatus.Updated;
                                        evhistory.CustomerFullName = task.CustomerFullName;
                                        evhistory.ACustomerFullName = task.CustomerFullName;
                                        evhistory.CreatedDate = task.CreatedDate;
                                        evhistory.Latitude = task.Latitude;
                                        evhistory.Longtitude = task.Longtitude;
                                        incidenteventhistory.Add(evhistory);
                                    }
                                }
                            }
                        }
                    }
                }
                incidenteventhistory = incidenteventhistory.OrderByDescending(i => i.CreatedDate).ToList();
                foreach (var rem in incidenteventhistory)
                {
                    var timelineItem = new TimelineReportItems();

                    cusevent.Name = driverOffence.OffenceCategory;

                    if (rem.isTask)
                    {
                        if (!string.IsNullOrEmpty(rem.TaskName))
                            cusevent.Name = "task " + rem.TaskName;
                        else
                            cusevent.Name = "Task";

                    }


                    var actioninfo = string.Empty;
                    var returnstring = string.Empty;
                    var incidentLocation = string.Empty;
                    if (!string.IsNullOrEmpty(rem.Longtitude) && !string.IsNullOrEmpty(rem.Latitude))
                        incidentLocation = "from " + ReverseGeocode.RetrieveFormatedAddress(rem.Latitude.ToString(), rem.Longtitude.ToString(), getClientLic);

                    if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Pending)
                    {
                        actioninfo = "created ";
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.CustomerFullName + " " + actioninfo + " " + cusevent.Name;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Complete)
                    {
                        actioninfo = "completed ";
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.ACustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Dispatch)
                    {
                        actioninfo = "dispatched ";
                        incidentLocation = "to " + rem.ACustomerFullName;
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.CustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Resolve)
                    {
                        actioninfo = "resolved ";
                        incidentLocation = "";
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.CustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Reject)
                    {
                        actioninfo = "rejected ";
                        incidentLocation = "";
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.CustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Park)
                    {
                        actioninfo = "parked ";
                        incidentLocation = "";
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.CustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Engage)
                    {
                        actioninfo = "started ";
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.ACustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.OnRoute)
                    {
                        actioninfo = "onroute ";
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.ACustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Pause)
                    {
                        actioninfo = "paused ";
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.ACustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Resume)
                    {
                        actioninfo = "resumed ";
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.ACustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Release)
                    {
                        actioninfo = "released ";
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.ACustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Arrived)
                    {
                        actioninfo = "arrived to incident ";
                        incidentLocation = "at " + ReverseGeocode.RetrieveFormatedAddress(rem.Latitude.ToString(), rem.Longtitude.ToString(), getClientLic);
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.ACustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Escalated)
                    {
                        actioninfo = "escalated ";
                        incidentLocation = "";
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.CustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    else if (rem.IncidentAction == (int)CustomEvent.IncidentActionStatus.Updated)
                    {
                        actioninfo = "updated ";
                        incidentLocation = "";
                        timelineItem.DatetimeN = rem.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                        timelineItem.TimelineActivity = rem.CustomerFullName + " " + actioninfo + " " + cusevent.Name + " " + incidentLocation;
                    }
                    newTRIlist.Add(timelineItem);
                }




                iTextSharp.text.Paragraph paragraphTable = new iTextSharp.text.Paragraph();
                paragraphTable.SpacingBefore = 120f;
                Paragraph p = new Paragraph();
                p.IndentationLeft = 10;

                p.Add(mainheadertable);
                p.Add(taskdataTable);
                p.Add(maptextdataTable);
                //p.Add(mapdataTable);

                PdfPTable table = new PdfPTable(1);
                table.DefaultCell.Border = Rectangle.NO_BORDER;

                //Map style roadMap

                doc.Add(paragraphTable);

                doc.Add(p);

                //Attachments

                var listycanvasnotes = new List<string>();

                var attachments = new List<MobileHotEventAttachment>();


                //if (cusevent.EventType == CustomEvent.EventTypes.MobileHotEvent)
                //{
                //    var mobEv = MobileHotEvent.GetMobileHotEventByGuid(cusevent.Identifier, dbConnection);
                //    if (mobEv != null)
                //        attachments.AddRange(MobileHotEventAttachment.GetMobileHotEventAttachmentByHotEventId(mobEv.Id, dbConnection));


                //}

                attachments.AddRange(MobileHotEventAttachment.GetMobileHotEventAttachmentByEventId(cusevent.EventId, dbConnection));

                var videoCount = 0;
                var imgCount = 0;
                var pdfCount = 0;

                PdfPTable taskimagetable = new PdfPTable(2);
                taskimagetable.DefaultCell.Border = Rectangle.NO_BORDER;
                taskimagetable.WidthPercentage = 100;

                var atheader1 = new PdfPCell();
                atheader1.AddElement(new Phrase("Attachments", cbtimes));
                atheader1.Border = Rectangle.NO_BORDER;
                atheader1.PaddingTop = 10;
                atheader1.PaddingBottom = 10;
                atheader1.Colspan = 2;
                taskimagetable.AddCell(atheader1);
                taskimagetable.KeepTogether = true;
                if (attachments.Count > 0)
                {
                    var attachcount = 0;
                    var canvasattachcount = 0;
                    var attachlist = new List<MobileHotEventAttachment>();
                    var videosList = new List<MobileHotEventAttachment>();

                    foreach (var item in attachments)
                    {
                        if (!string.IsNullOrEmpty(item.AttachmentPath))
                        {
                            if (VideoExtensions.Contains(System.IO.Path.GetExtension(item.AttachmentPath).ToUpperInvariant()))
                            {
                                videoCount++;
                            }
                            else if (CommonUtility.FileExtensions.Contains(System.IO.Path.GetExtension(item.AttachmentPath).ToUpperInvariant()))//(System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".PDF" || System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".DOCX")
                            {
                                pdfCount++;
                            }
                            else
                            {
                                attachlist.Add(item);
                                attachcount++;
                                imgCount++;
                                listycanvasnotes.Add("Image " + attachcount);
                            }
                        }
                    }
                    taskimagetable.DefaultCell.Border = Rectangle.NO_BORDER;
                    var countz = 0;
                    foreach (var item in attachlist)
                    {
                        if (VideoExtensions.Contains(System.IO.Path.GetExtension(item.AttachmentPath).ToUpperInvariant()))
                        {

                        }
                        else if (CommonUtility.FileExtensions.Contains(System.IO.Path.GetExtension(item.AttachmentPath).ToUpperInvariant()))//(System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".PDF" || System.IO.Path.GetExtension(requiredString).ToUpperInvariant() == ".DOCX")
                        {

                        }
                        else
                        {
                            iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(item.AttachmentPath);

                            if (image.Height > image.Width)
                            {
                                //Maximum height is 800 pixels.
                                float percentage = 0.0f;
                                percentage = 155 / image.Height;
                                image.ScalePercent(percentage * 100);
                            }
                            else
                            {
                                //Maximum width is 600 pixels.
                                float percentage = 0.0f;
                                percentage = 155 / image.Width;
                                image.ScalePercent(percentage * 100);
                            }

                            //image.ScaleAbsolute(120f, 155.25f);
                            image.Border = iTextSharp.text.Rectangle.NO_BORDER;

                            iTextSharp.text.pdf.PdfPCell imgCell1 = new iTextSharp.text.pdf.PdfPCell();
                            if (!string.IsNullOrEmpty(listycanvasnotes[countz]))
                            {
                                Paragraph pcell = new Paragraph(listycanvasnotes[countz].ToUpper());
                                pcell.Alignment = Element.ALIGN_LEFT;
                                pcell.SpacingAfter = 10;
                                imgCell1.AddElement(pcell);
                            }
                            imgCell1.Border = iTextSharp.text.Rectangle.NO_BORDER;
                            imgCell1.AddElement(new Chunk(image, 0, 0));
                            imgCell1.PaddingTop = 10;
                            taskimagetable.AddCell(imgCell1);
                            countz++;
                        }
                    }
                    taskimagetable.CompleteRow();
                }

                var extraattachment = new PdfPTable(3);
                extraattachment.DefaultCell.Border = Rectangle.NO_BORDER;
                extraattachment.WidthPercentage = 100;
                var phrase2 = new Phrase();
                phrase2.Add(
                    new Chunk("Files Attached:  ", btimes)
                );
                phrase2.Add(new Chunk(pdfCount.ToString(), times));

                var zheaderA3a = new PdfPCell();
                zheaderA3a.AddElement(phrase2);
                zheaderA3a.Border = Rectangle.NO_BORDER;
                zheaderA3a.PaddingTop = 20;
                zheaderA3a.PaddingBottom = 20;
                extraattachment.AddCell(zheaderA3a);

                var phrase32 = new Phrase();
                phrase32.Add(
                    new Chunk("Videos Attached:  ", btimes)
                );
                phrase32.Add(new Chunk(videoCount.ToString(), times));

                var aheaderA3a = new PdfPCell();
                aheaderA3a.AddElement(phrase32);
                aheaderA3a.Border = Rectangle.NO_BORDER;
                aheaderA3a.PaddingTop = 20;
                aheaderA3a.PaddingBottom = 20;
                extraattachment.AddCell(aheaderA3a);

                var phrase323 = new Phrase();
                phrase323.Add(
                    new Chunk("Images Attached:  ", btimes)
                );
                phrase323.Add(new Chunk(imgCount.ToString(), times));

                var aaheaderA3a = new PdfPCell();
                aaheaderA3a.AddElement(phrase323);
                aaheaderA3a.Border = Rectangle.NO_BORDER;
                aaheaderA3a.PaddingTop = 20;
                aaheaderA3a.PaddingBottom = 20;
                extraattachment.AddCell(aaheaderA3a);

                //doc.NewPage();

                doc.Add(taskimagetable);

                doc.Add(extraattachment);


                PdfPTable timetable = new PdfPTable(2);
                timetable.WidthPercentage = 100;
                timetable.DefaultCell.Border = Rectangle.NO_BORDER;
                timetable.SetWidths(columnWidths2575);

                if (newTRIlist.Count > 0)
                {
                    var headeventCell = new PdfPCell(new Phrase("Timeline", cbtimes));
                    headeventCell.Border = Rectangle.NO_BORDER;
                    headeventCell.PaddingTop = 10;
                    headeventCell.PaddingBottom = 10;
                    headeventCell.Colspan = 2;
                    timetable.AddCell(headeventCell);

                    foreach (var lit in newTRIlist)
                    {
                        var dateCell = new PdfPCell(new Phrase(lit.DatetimeN, redeleventimes));
                        dateCell.Border = Rectangle.NO_BORDER;
                        timetable.AddCell(dateCell);

                        var eventCell = new PdfPCell(new Phrase(lit.TimelineActivity, eleventimes));
                        eventCell.Border = Rectangle.NO_BORDER;
                        timetable.AddCell(eventCell);
                    }
                }
                doc.Add(timetable);

                PdfPTable notestable = new PdfPTable(2);
                notestable.WidthPercentage = 100;
                notestable.DefaultCell.Border = Rectangle.NO_BORDER;
                notestable.SetWidths(columnWidths2575);

                var tremarks = CustomEventRemarks.GetCustomEventRemarksByEventId(cusevent.EventId, dbConnection);

                if (tremarks.Count > 0)
                {
                    var headeventCell = new PdfPCell(new Phrase("Notes", cbtimes));
                    headeventCell.Border = Rectangle.NO_BORDER;
                    headeventCell.PaddingTop = 10;
                    headeventCell.PaddingBottom = 10;
                    headeventCell.Colspan = 2;
                    notestable.AddCell(headeventCell);
                    foreach (var remark in tremarks)
                    {
                        var dateCell = new PdfPCell(new Phrase(remark.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString(), redeleventimes));
                        dateCell.Border = Rectangle.NO_BORDER;
                        notestable.AddCell(dateCell);

                        var eventCell = new PdfPCell(new Phrase(remark.FName + " " + remark.LName + ":" + remark.Remarks, eleventimes));
                        eventCell.Border = Rectangle.NO_BORDER;
                        notestable.AddCell(eventCell);
                    }
                }

                doc.Add(notestable);

                //TASKS LOOP

                //var utask = UserTask.GetAllTaskByIncidentId(cusevent.EventId, dbConnection);
                if (utask.Count > 0)
                {
                    int count = 1;

                    foreach (var task in utask)
                    {
                        var tskheadertable = new PdfPTable(2);
                        tskheadertable.DefaultCell.Border = Rectangle.NO_BORDER;
                        tskheadertable.WidthPercentage = 100;
                        float[] columnWidths2080 = new float[] { 20, 80 };
                        tskheadertable.SetWidths(columnWidths2080);

                        var tskheader0 = new PdfPCell();
                        //tskheader0.AddElement(new Phrase("Task #:" + count, cbtimes));

                        tskheader0.AddElement(new Phrase("Attached Task", cbtimes));
                        tskheader0.Border = Rectangle.NO_BORDER;
                        tskheader0.Colspan = 2;
                        tskheadertable.AddCell(tskheader0);

                        var tskheader1 = new PdfPCell();
                        tskheader1.AddElement(new Phrase("Task Name:", beleventimes));
                        tskheader1.Border = Rectangle.NO_BORDER;
                        tskheadertable.AddCell(tskheader1);

                        var tskheader1a = new PdfPCell();
                        tskheader1a.AddElement(new Phrase(task.Name, eleventimes));
                        tskheader1a.Border = Rectangle.NO_BORDER;
                        tskheadertable.AddCell(tskheader1a);

                        var header1z = new PdfPCell();
                        header1z.AddElement(new Phrase("Task Number:", beleventimes));
                        header1z.Border = Rectangle.NO_BORDER;
                        tskheadertable.AddCell(header1z);

                        var header1az = new PdfPCell();
                        header1az.AddElement(new Phrase(task.NewCustomerTaskId, eleventimes));
                        header1az.Border = Rectangle.NO_BORDER;
                        tskheadertable.AddCell(header1az);

                        var tskheader2 = new PdfPCell();
                        tskheader2.AddElement(new Phrase("Task Description:", beleventimes));
                        tskheader2.Border = Rectangle.NO_BORDER;
                        tskheadertable.AddCell(tskheader2);

                        var tskheader2a = new PdfPCell();
                        tskheader2a.AddElement(new Phrase(task.Description, eleventimes));
                        tskheader2a.Border = Rectangle.NO_BORDER;
                        tskheadertable.AddCell(tskheader2a);

                        var chklistName = "None";
                        var newTCLlist = new List<TaskChecklistReportItems>();
                        var parentTasks = TemplateCheckList.GetAllTemplateCheckListById(task.TemplateCheckListId.ToString(), dbConnection);
                        if (parentTasks != null)
                        {
                            chklistName = parentTasks.Name;

                            var sessions = TaskCheckList.GetTaskCheckListItemsByTaskId(task.Id, dbConnection);

                            foreach (var item in sessions)
                            {
                                var newTCL = new TaskChecklistReportItems();
                                newTCL.isParent = true;

                                if (item.CheckListItemType != (int)CheckListItemType.Type4 && item.CheckListItemType != (int)CheckListItemType.Type5)
                                {
                                    var ncaheaderA = new PdfPCell();
                                    ncaheaderA.AddElement(new Phrase(item.Name, eleventimes));
                                    ncaheaderA.Border = Rectangle.NO_BORDER;
                                    newTCL.itemName = ncaheaderA;
                                }
                                else
                                {
                                    var ncaheaderA = new PdfPCell();
                                    ncaheaderA.AddElement(new Phrase(item.Name, beleventimes));
                                    ncaheaderA.Border = Rectangle.NO_BORDER;
                                    newTCL.itemName = ncaheaderA;
                                }
                                newTCL.Notes = item.TemplateCheckListItemNote;

                                var myattachments = UserTaskAttachment.GetTaskAttachmentsByChecklistId(item.Id, dbConnection);
                                newTCL.Attachments = myattachments;


                                var stringCheck = string.Empty;
                                var itemNotes = string.Empty;

                                if (item.IsChecked)
                                {
                                    iTextSharp.text.pdf.PdfPCell imgCell1 = new iTextSharp.text.pdf.PdfPCell();
                                    imgCell1.AddElement(new Phrase("3", gweb));
                                    imgCell1.Border = Rectangle.NO_BORDER;
                                    newTCL.Status = imgCell1;
                                }
                                else
                                {
                                    List myList = new ZapfDingbatsList(54);

                                    iTextSharp.text.pdf.PdfPCell imgCell1 = new iTextSharp.text.pdf.PdfPCell();
                                    imgCell1.AddElement(new Phrase("6", rweb));
                                    imgCell1.Border = Rectangle.NO_BORDER;
                                    newTCL.Status = imgCell1;

                                    //if (item.CheckListItemType != (int)CheckListItemType.Type4 && item.CheckListItemType != (int)CheckListItemType.Type5)
                                    //    chkCounter++;
                                }

                                newTCLlist.Add(newTCL);

                                if (item.ChildCheckList != null)
                                {
                                    if (item.ChildCheckList.Count > 0)
                                    {
                                        foreach (var child in item.ChildCheckList)
                                        {

                                            var cnewTCL = new TaskChecklistReportItems();
                                            cnewTCL.isParent = false;

                                            var ncaheaderA = new PdfPCell();
                                            ncaheaderA.AddElement(new Phrase(child.Name, eleventimesI));
                                            ncaheaderA.Border = Rectangle.NO_BORDER;
                                            cnewTCL.itemName = ncaheaderA;

                                            cnewTCL.Notes = child.TemplateCheckListItemNote;

                                            var mycattachments = UserTaskAttachment.GetTaskAttachmentsByChecklistId(child.Id, dbConnection);
                                            cnewTCL.Attachments = mycattachments;

                                            if (item.IsChecked)
                                            {
                                                //gweb rweb
                                                iTextSharp.text.pdf.PdfPCell imgCell1 = new iTextSharp.text.pdf.PdfPCell();
                                                imgCell1.AddElement(new Phrase("3", gweb));
                                                imgCell1.Border = Rectangle.NO_BORDER;
                                                cnewTCL.Status = imgCell1;

                                            }
                                            else
                                            {
                                                iTextSharp.text.pdf.PdfPCell imgCell1 = new iTextSharp.text.pdf.PdfPCell();
                                                imgCell1.AddElement(new Phrase("6", rweb));
                                                imgCell1.Border = Rectangle.NO_BORDER;
                                                cnewTCL.Status = imgCell1;

                                                //chkCounter++;
                                            }

                                            newTCLlist.Add(cnewTCL);
                                        }
                                    }
                                }
                            }
                        }
                        var tskheader3 = new PdfPCell();
                        tskheader3.AddElement(new Phrase("Checklist Name:", beleventimes));
                        tskheader3.Border = Rectangle.NO_BORDER;
                        tskheadertable.AddCell(tskheader3);

                        var tskheader3a = new PdfPCell();
                        tskheader3a.AddElement(new Phrase(chklistName, eleventimes));
                        tskheader3a.Border = Rectangle.NO_BORDER;
                        tskheadertable.AddCell(tskheader3a);

                        doc.Add(tskheadertable);

                        var newchecklistTable = new PdfPTable(4);
                        newchecklistTable.DefaultCell.Border = Rectangle.NO_BORDER;
                        newchecklistTable.WidthPercentage = 100;
                        float[] columnWidthsCHK = new float[] { 30, 10, 30, 30 };
                        newchecklistTable.SetWidths(columnWidthsCHK);

                        var cheader1 = new PdfPCell();
                        cheader1.AddElement(new Phrase("Checklist", cbtimes));
                        cheader1.Border = Rectangle.NO_BORDER;
                        cheader1.PaddingTop = 10;
                        cheader1.PaddingBottom = 10;
                        cheader1.Colspan = 4;
                        newchecklistTable.AddCell(cheader1);

                        var ncaheader1 = new PdfPCell();
                        ncaheader1.AddElement(new Phrase("Item Name", weleventimes));
                        ncaheader1.Border = Rectangle.NO_BORDER;
                        ncaheader1.PaddingLeft = 5;
                        ncaheader1.PaddingBottom = 5;
                        ncaheader1.BackgroundColor = Color.DARK_GRAY;

                        newchecklistTable.AddCell(ncaheader1);

                        var ncaheader2 = new PdfPCell();
                        ncaheader2.AddElement(new Phrase("Status", weleventimes));
                        ncaheader2.Border = Rectangle.NO_BORDER;
                        ncaheader2.BackgroundColor = Color.DARK_GRAY;
                        newchecklistTable.AddCell(ncaheader2);

                        var ncaheader3 = new PdfPCell();
                        ncaheader3.AddElement(new Phrase("Notes", weleventimes));
                        ncaheader3.Border = Rectangle.NO_BORDER;
                        ncaheader3.BackgroundColor = Color.DARK_GRAY;
                        newchecklistTable.AddCell(ncaheader3);

                        var ncaheader4 = new PdfPCell();
                        ncaheader4.AddElement(new Phrase("Attachments", weleventimes));
                        ncaheader4.Border = Rectangle.NO_BORDER;
                        ncaheader4.BackgroundColor = Color.DARK_GRAY;
                        newchecklistTable.AddCell(ncaheader4);

                        foreach (var item in newTCLlist)
                        {
                            newchecklistTable.AddCell(item.itemName);

                            newchecklistTable.AddCell(item.Status);

                            var ncaheaderAB = new PdfPCell();
                            ncaheaderAB.AddElement(new Phrase(item.Notes, eleventimes));
                            ncaheaderAB.Border = Rectangle.NO_BORDER;
                            newchecklistTable.AddCell(ncaheaderAB);

                            if (item.Attachments.Count > 0)
                            {
                                PdfPTable imgTable = new PdfPTable(1);
                                imgTable.DefaultCell.Border = Rectangle.NO_BORDER;
                                foreach (var attach in item.Attachments)
                                {
                                    if (VideoExtensions.Contains(System.IO.Path.GetExtension(attach.DocumentPath).ToUpperInvariant()))
                                    {
                                        imgTable.AddCell(new Phrase("Video Attached", eleventimes));
                                    }
                                    else
                                    {
                                        //var imgstring = String.Format(mimssettings.MIMSMobileAddress + "/Uploads/Tasks/" + task.Id + "/{0}", System.IO.Path.GetFileName(attach.DocumentPath));

                                        iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(attach.DocumentPath);

                                        if (image.Height > image.Width)
                                        {
                                            //Maximum height is 800 pixels.
                                            float percentage = 0.0f;
                                            percentage = 155 / image.Height;
                                            image.ScalePercent(percentage * 100);
                                        }
                                        else
                                        {
                                            //Maximum width is 600 pixels.
                                            float percentage = 0.0f;
                                            percentage = 155 / image.Width;
                                            image.ScalePercent(percentage * 100);
                                        }

                                        //image.ScaleAbsolute(120f, 155.25f);
                                        image.Border = Rectangle.NO_BORDER;
                                        iTextSharp.text.pdf.PdfPCell imgCell1l = new iTextSharp.text.pdf.PdfPCell();
                                        imgCell1l.Border = Rectangle.NO_BORDER;
                                        imgCell1l.AddElement(new Chunk(image, 0, 0));
                                        imgTable.AddCell(imgCell1l);
                                        if (!string.IsNullOrEmpty(attach.ImageNote) && attach.ImageNote != "Task Attachment")
                                            imgTable.AddCell(new Phrase("Img Note:" + attach.ImageNote, eleventimes));
                                    }
                                }
                                newchecklistTable.AddCell(imgTable);
                            }
                            else
                            {
                                newchecklistTable.AddCell(new Phrase(" ", eleventimes));
                            }
                        }

                        doc.Add(newchecklistTable);

                        var tsklistycanvasnotes = new List<string>();
                        var tskattachments = UserTaskAttachment.GetTaskAttachmentsByTaskId(task.Id, dbConnection);
                        var tskvideoCount = 0;
                        var tskimgCount = 0;
                        var tskpdfCount = 0;

                        PdfPTable tsktaskimagetable = new PdfPTable(2);
                        tsktaskimagetable.DefaultCell.Border = Rectangle.NO_BORDER;
                        tsktaskimagetable.WidthPercentage = 100;

                        var tskatheader1 = new PdfPCell();
                        tskatheader1.AddElement(new Phrase("Task Attachments", cbtimes));
                        tskatheader1.Border = Rectangle.NO_BORDER;
                        tskatheader1.PaddingTop = 10;
                        tskatheader1.PaddingBottom = 10;
                        tskatheader1.Colspan = 2;
                        tsktaskimagetable.AddCell(tskatheader1);

                        if (tskattachments.Count > 0)
                        {
                            var attachcount = 0;
                            var canvasattachcount = 0;
                            var attachlist = new List<UserTaskAttachment>();

                            foreach (var item in tskattachments)
                            {
                                if (!string.IsNullOrEmpty(item.DocumentPath) && item.ChecklistId == 0)
                                {
                                    if (VideoExtensions.Contains(System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant()))
                                    {
                                        tskvideoCount++;
                                    }
                                    else if (System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant() == ".PDF")
                                    {
                                        tskpdfCount++;
                                    }
                                    else
                                    {
                                        attachlist.Add(item);
                                        attachcount++;
                                        tskimgCount++;
                                        if (item.ImageNote != "Task Attachment" && !string.IsNullOrEmpty(item.ImageNote))
                                        {
                                            tsklistycanvasnotes.Add("Canvas Notes Image " + attachcount + ":" + item.ImageNote);
                                            canvasattachcount++;
                                        }
                                        else
                                        {
                                            tsklistycanvasnotes.Add("Image " + attachcount);
                                            canvasattachcount++;
                                        }
                                    }
                                }
                            }

                            var countz = 0;
                            foreach (var item in attachlist)
                            {
                                if (VideoExtensions.Contains(System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant()))
                                {

                                }
                                else if (System.IO.Path.GetExtension(item.DocumentPath).ToUpperInvariant() == ".PDF")
                                {

                                }
                                else
                                {
                                    iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(item.DocumentPath);

                                    if (image.Height > image.Width)
                                    {
                                        //Maximum height is 800 pixels.
                                        float percentage = 0.0f;
                                        percentage = 155 / image.Height;
                                        image.ScalePercent(percentage * 100);
                                    }
                                    else
                                    {
                                        //Maximum width is 600 pixels.
                                        float percentage = 0.0f;
                                        percentage = 155 / image.Width;
                                        image.ScalePercent(percentage * 100);
                                    }

                                    //image.ScaleAbsolute(120f, 155.25f);
                                    image.Border = iTextSharp.text.Rectangle.NO_BORDER;

                                    iTextSharp.text.pdf.PdfPCell imgCell1 = new iTextSharp.text.pdf.PdfPCell();
                                    if (!string.IsNullOrEmpty(tsklistycanvasnotes[countz]))
                                    {
                                        Paragraph pcell = new Paragraph(tsklistycanvasnotes[countz].ToUpper());
                                        pcell.Alignment = Element.ALIGN_LEFT;
                                        pcell.SpacingAfter = 10;
                                        imgCell1.AddElement(pcell);
                                    }
                                    imgCell1.Border = iTextSharp.text.Rectangle.NO_BORDER;
                                    imgCell1.AddElement(new Chunk(image, 0, 0));
                                    imgCell1.PaddingTop = 10;
                                    tsktaskimagetable.AddCell(imgCell1);
                                    countz++;
                                }
                            }
                            tsktaskimagetable.CompleteRow();
                        }

                        var tskextraattachment = new PdfPTable(3);
                        tskextraattachment.DefaultCell.Border = Rectangle.NO_BORDER;
                        tskextraattachment.WidthPercentage = 100;
                        var tskphrase2 = new Phrase();
                        tskphrase2.Add(
                            new Chunk("PDF Attached:  ", btimes)
                        );
                        tskphrase2.Add(new Chunk(tskpdfCount.ToString(), times));

                        var tskzheaderA3a = new PdfPCell();
                        tskzheaderA3a.AddElement(tskphrase2);
                        tskzheaderA3a.Border = Rectangle.NO_BORDER;
                        tskzheaderA3a.PaddingTop = 20;
                        tskzheaderA3a.PaddingBottom = 20;
                        tskextraattachment.AddCell(tskzheaderA3a);

                        var tskphrase32 = new Phrase();
                        tskphrase32.Add(
                            new Chunk("Videos Attached:  ", btimes)
                        );
                        tskphrase32.Add(new Chunk(tskvideoCount.ToString(), times));

                        var tskaheaderA3a = new PdfPCell();
                        tskaheaderA3a.AddElement(tskphrase32);
                        tskaheaderA3a.Border = Rectangle.NO_BORDER;
                        tskaheaderA3a.PaddingTop = 20;
                        tskaheaderA3a.PaddingBottom = 20;
                        tskextraattachment.AddCell(tskaheaderA3a);

                        var tskphrase323 = new Phrase();
                        tskphrase323.Add(
                            new Chunk("Images Attached:  ", btimes)
                        );
                        tskphrase323.Add(new Chunk(tskimgCount.ToString(), times));

                        var tskaaheaderA3a = new PdfPCell();
                        tskaaheaderA3a.AddElement(tskphrase323);
                        tskaaheaderA3a.Border = Rectangle.NO_BORDER;
                        tskaaheaderA3a.PaddingTop = 20;
                        tskaaheaderA3a.PaddingBottom = 20;
                        tskextraattachment.AddCell(tskaaheaderA3a);

                        doc.Add(tsktaskimagetable);

                        doc.Add(tskextraattachment);

                        var taskeventhistory = TaskEventHistory.GetTaskEventHistoryByTaskId(task.Id, dbConnection);

                        taskeventhistory = taskeventhistory.OrderByDescending(i => i.CreatedDate).ToList();

                        var tsknewTRIlist = new List<TimelineReportItems>();

                        foreach (var taskEv in taskeventhistory)
                        {
                            var timelineItem = new TimelineReportItems();
                            var taskn = "Task";
                            if (taskEv.isSubTask)
                            {
                                taskn = "Sub-task";
                            }

                            if (taskEv.Action == (int)TaskAction.Complete)
                            {
                                completedBy = taskEv.CustomerFullName;
                                var compLocation = string.Empty;
                                var geoLoc = ReverseGeocode.RetrieveFormatedAddress(task.EndLatitude.ToString(), task.EndLongitude.ToString(), getClientLic);
                                if (!string.IsNullOrEmpty(geoLoc))
                                    compLocation = " at " + geoLoc;
                                var compInfo = "completed";
                                latComp = task.EndLatitude.ToString();
                                longComp = task.EndLongitude.ToString();
                                timelineItem.DatetimeN = taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                                //listy.Add(taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());

                                var compstring = completedBy + " " + compInfo + " " + taskn + " " + compLocation;

                                timelineItem.TimelineActivity = compstring;
                                //listy.Add(compstring);
                            }
                            else if (taskEv.Action == (int)TaskAction.OnRoute)
                            {
                                inprogressby = taskEv.CustomerFullName;
                                var pendingLocation = string.Empty;
                                var geoLoc2 = ReverseGeocode.RetrieveFormatedAddress(task.StartLatitude.ToString(), task.StartLongitude.ToString(), getClientLic);
                                if (!string.IsNullOrEmpty(geoLoc2))
                                    pendingLocation = " at " + geoLoc2;
                                var pendingInfo = "onroute ";
                                // listy.Add(taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());
                                timelineItem.DatetimeN = taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();

                                var returnstring = inprogressby + " " + pendingInfo + " " + taskn + " " + pendingLocation;
                                //listy.Add(returnstring);
                                timelineItem.TimelineActivity = returnstring;

                                latEng = task.StartLatitude.ToString();
                                longEng = task.StartLongitude.ToString();
                            }
                            else if (taskEv.Action == (int)TaskAction.Pause)
                            {
                                inprogressby = taskEv.CustomerFullName;
                                var pendingLocation = string.Empty;
                                var geoLoc2 = ReverseGeocode.RetrieveFormatedAddress(task.StartLatitude.ToString(), task.StartLongitude.ToString(), getClientLic);
                                if (!string.IsNullOrEmpty(geoLoc2))
                                    pendingLocation = " at " + geoLoc2;
                                var pendingInfo = "paused ";
                                // listy.Add(taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());
                                timelineItem.DatetimeN = taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();

                                var returnstring = inprogressby + " " + pendingInfo + " " + taskn + " " + pendingLocation;
                                //listy.Add(returnstring);
                                timelineItem.TimelineActivity = returnstring;

                                latEng = task.StartLatitude.ToString();
                                longEng = task.StartLongitude.ToString();
                            }
                            else if (taskEv.Action == (int)TaskAction.Resume)
                            {
                                inprogressby = taskEv.CustomerFullName;
                                var pendingLocation = string.Empty;
                                var geoLoc2 = ReverseGeocode.RetrieveFormatedAddress(task.StartLatitude.ToString(), task.StartLongitude.ToString(), getClientLic);
                                if (!string.IsNullOrEmpty(geoLoc2))
                                    pendingLocation = " at " + geoLoc2;
                                var pendingInfo = "resumed ";
                                // listy.Add(taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());
                                timelineItem.DatetimeN = taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();

                                var returnstring = inprogressby + " " + pendingInfo + " " + taskn + " " + pendingLocation;
                                //listy.Add(returnstring);
                                timelineItem.TimelineActivity = returnstring;

                                latEng = task.StartLatitude.ToString();
                                longEng = task.StartLongitude.ToString();
                            }
                            else if (taskEv.Action == (int)TaskAction.InProgress)
                            {
                                inprogressby = taskEv.CustomerFullName;
                                var pendingLocation = string.Empty;
                                var geoLoc2 = ReverseGeocode.RetrieveFormatedAddress(task.StartLatitude.ToString(), task.StartLongitude.ToString(), getClientLic);
                                if (!string.IsNullOrEmpty(geoLoc2))
                                    pendingLocation = " at " + geoLoc2;
                                var pendingInfo = "started";
                                // listy.Add(taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());
                                timelineItem.DatetimeN = taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();

                                var returnstring = inprogressby + " " + pendingInfo + " " + taskn + " " + pendingLocation;
                                //listy.Add(returnstring);
                                timelineItem.TimelineActivity = returnstring;

                                latEng = task.StartLatitude.ToString();
                                longEng = task.StartLongitude.ToString();
                            }
                            else if (taskEv.Action == (int)TaskAction.Accepted)
                            {
                                timelineItem.DatetimeN = taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                                //listy.Add(taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());
                                acceptedData = taskEv.CustomerFullName + " accepted " + taskn;
                                //listy.Add(acceptedData);
                                timelineItem.TimelineActivity = acceptedData;
                            }
                            else if (taskEv.Action == (int)TaskAction.Rejected)
                            {
                                //listy.Add(taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());
                                timelineItem.DatetimeN = taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();

                                rejectedData = taskEv.CustomerFullName + " rejected Task " + taskn;

                                timelineItem.TimelineActivity = rejectedData;
                                //listy.Add(rejectedData);
                            }
                            else if (taskEv.Action == (int)TaskAction.Assigned)
                            {
                                //listy.Add(taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());
                                timelineItem.DatetimeN = taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();

                                assignedData = taskEv.CustomerFullName + " assigned " + taskn + " to " + taskEv.ACustomerFullName;

                                timelineItem.TimelineActivity = assignedData;

                                //listy.Add(assignedData);
                            }
                            else if (taskEv.Action == (int)TaskAction.Pending)
                            {
                                var incidentLocation = string.Empty;
                                var geoLoc3 = ReverseGeocode.RetrieveFormatedAddress(task.Latitude.ToString(), task.Longitude.ToString(), getClientLic);
                                if (!string.IsNullOrEmpty(geoLoc3))
                                    incidentLocation = " at " + geoLoc3;
                                var actioninfo = "created";
                                //listy.Add(taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());
                                timelineItem.DatetimeN = taskEv.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString();
                                actioninfo = task.CustomerFullName + " " + actioninfo + " " + taskn + " for " + taskEv.ACustomerFullName + " " + incidentLocation;
                                timelineItem.TimelineActivity = actioninfo;
                                //listy.Add(actioninfo);
                            }
                            tsknewTRIlist.Add(timelineItem);
                        }

                        PdfPTable tsktimetable = new PdfPTable(2);
                        tsktimetable.WidthPercentage = 100;
                        tsktimetable.DefaultCell.Border = Rectangle.NO_BORDER;
                        tsktimetable.SetWidths(columnWidths2575);

                        if (tsknewTRIlist.Count > 0)
                        {
                            var headeventCell = new PdfPCell(new Phrase("Task Timeline", cbtimes));
                            headeventCell.Border = Rectangle.NO_BORDER;
                            headeventCell.PaddingTop = 10;
                            headeventCell.PaddingBottom = 10;
                            headeventCell.Colspan = 2;
                            tsktimetable.AddCell(headeventCell);

                            foreach (var lit in tsknewTRIlist)
                            {
                                var dateCell = new PdfPCell(new Phrase(lit.DatetimeN, redeleventimes));
                                dateCell.Border = Rectangle.NO_BORDER;
                                tsktimetable.AddCell(dateCell);

                                var eventCell = new PdfPCell(new Phrase(lit.TimelineActivity, eleventimes));
                                eventCell.Border = Rectangle.NO_BORDER;
                                tsktimetable.AddCell(eventCell);
                            }
                        }

                        PdfPTable tasknotestable = new PdfPTable(2);
                        tasknotestable.WidthPercentage = 100;
                        tasknotestable.DefaultCell.Border = Rectangle.NO_BORDER;
                        tasknotestable.SetWidths(columnWidths2575);

                        var taskremarks = TaskRemarks.GetTaskRemarksByTaskId(task.Id, dbConnection);

                        if (taskremarks.Count > 0)
                        {
                            var headeventCell = new PdfPCell(new Phrase("Notes", cbtimes));
                            headeventCell.Border = Rectangle.NO_BORDER;
                            headeventCell.PaddingTop = 10;
                            headeventCell.PaddingBottom = 10;
                            headeventCell.Colspan = 2;
                            tasknotestable.AddCell(headeventCell);
                            foreach (var remark in taskremarks)
                            {
                                var dateCell = new PdfPCell(new Phrase(remark.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString(), redeleventimes));
                                dateCell.Border = Rectangle.NO_BORDER;
                                tasknotestable.AddCell(dateCell);

                                var eventCell = new PdfPCell(new Phrase(remark.FName + " " + remark.LName + ":" + remark.Remarks, eleventimes));
                                eventCell.Border = Rectangle.NO_BORDER;
                                tasknotestable.AddCell(eventCell);
                            }
                        }
                        doc.Add(tsktimetable);
                        doc.Add(tasknotestable);
                    }
                }
                //END OF TASKS



                Font geleventimes = new Font(f_cn, 11, Font.NORMAL, Color.GRAY);
                var endTable = new PdfPTable(1);
                endTable.DefaultCell.Border = Rectangle.NO_BORDER;
                endTable.WidthPercentage = 100;

                var endheader = new PdfPCell();
                endheader.AddElement(new Phrase("Report generated: " + CommonUtility.getDTNow().AddHours(userinfo.TimeZone).ToString() + " 	by: " + userinfo.Username, geleventimes));
                endheader.Border = Rectangle.NO_BORDER;
                endTable.AddCell(endheader);
                doc.Add(endTable);

                writer.Flush();
                doc.Close();
                if (File.Exists(path))
                {
                    using (StreamReader sr = new StreamReader(path))
                    {
                        var vpath = CommonUtility.CloudUploadFile(userinfo.CustomerInfoId, fileName, sr.BaseStream);
                        return vpath;
                    }
                }
            }

            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("Ticketing", "GeneratePDFTicket", exp, dbConnection, userinfo.SiteId);
                return "Failed to generate report!";
            }
            return "Failed to generate report!";
        }

        [WebMethod]
        public static List<string> getTaskRemarksData(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var projinfo = UserTask.GetTaskById(id, dbConnection);
                if (projinfo != null)
                {
                    var remarksList = TaskRemarks.GetTaskRemarksByTaskId(id, dbConnection);

                    if (!string.IsNullOrEmpty(projinfo.Notes))
                    {
                        var nTR = new TaskRemarks();
                        nTR.CustomerUName = projinfo.ACustomerUName;
                        nTR.Remarks = projinfo.Notes;
                        nTR.CreatedDate = projinfo.UpdatedDate.Value;
                        remarksList.Add(nTR);
                    }
                    if (remarksList.Count > 0)
                    {
                        remarksList = remarksList.OrderByDescending(i => i.CreatedDate).ToList();
                        var count = 0;
                        foreach (var task in remarksList)
                        {
                            if (count == 3)
                            {
                                break;
                            }
                            count++;
                            var OGremarks = task.Remarks;
                            if (!string.IsNullOrEmpty(task.Remarks) && task.Remarks.Length > 50)
                            {
                                task.Remarks = task.Remarks.Remove(50);
                                task.Remarks = task.Remarks + "...";
                            }
                            var acceptedData = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + task.CustomerUName + " wrote :</span>" + task.Remarks + "</p></div><i class='fa fa-sticky-note absoult-center-left' style='color:#bbbbbb;margin-top:-1px;' onmouseover='style=&apos;cursor: pointer;margin-top:-1px;color:#bbbbbb;&apos;' onclick='showRemarks(&apos;" + OGremarks + "&apos;)'></i>";
                            listy.Add(acceptedData);
                        }
                        if (remarksList.Count > 3)
                        {
                            var seeall = "<h5><span  class='line-center' onmouseover='style=&apos;cursor: pointer;' onclick='showAllRemarks(&apos;" + id + "&apos;)'>SEE ALL</span></h5>";
                            listy.Add(seeall);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Tasks", "getTaskRemarksData", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static List<string> getTaskRemarksData2(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var projinfo = UserTask.GetTaskById(id, dbConnection);
                if (projinfo != null)
                {
                    var remarksList = TaskRemarks.GetTaskRemarksByTaskId(id, dbConnection);

                    if (!string.IsNullOrEmpty(projinfo.Notes))
                    {
                        var nTR = new TaskRemarks();
                        nTR.CustomerUName = projinfo.ACustomerUName;
                        nTR.Remarks = projinfo.Notes;
                        nTR.CreatedDate = projinfo.UpdatedDate.Value;
                        remarksList.Add(nTR);
                    }
                    if (remarksList.Count > 0)
                    {
                        remarksList = remarksList.OrderByDescending(i => i.CreatedDate).ToList();
                        foreach (var task in remarksList)
                        {
                            var OGremarks = task.Remarks;
                            var acceptedData = "<div class='col-md-12'><p class='gray-color no-vmargin'>" + task.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString() + "</p><p><span class='red-color'>" + task.CustomerUName + " wrote :</span>" + task.Remarks + "</p></div><i class='fa fa-sticky-note absoult-center-left' style='color:#bbbbbb;margin-top:-1px;' onmouseover='style=&apos;cursor: pointer;margin-top:-1px;color:#bbbbbb;&apos;' onclick='showRemarks(&apos;" + OGremarks + "&apos;)'></i>";
                            listy.Add(acceptedData);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Tasks", "getTaskRemarksData", ex, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static string getGPSDataFull(int id, bool locCB, bool eventsCB, bool peopleCB, string uname)
        {
            var json = string.Empty;
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    return "LOGOUT";
                }

                var users = new List<Users>();
                var cusEvents = new List<CustomEvent>();
                var vehs = new List<ClientManager>();
                if (getClientLic != null)
                {
                    if (getClientLic.isLocation)
                    {
                        if (peopleCB)
                        {
                            if (userinfo.RoleId == (int)Role.Manager)
                            {
                                users = Users.GetAllFullUsersByManagerId(userinfo.ID, dbConnection);
                            }
                            else if (userinfo.RoleId == (int)Role.Admin)
                            {
                                var sessions = DirectorManager.GetAllFullManagersByDirectorId(userinfo.ID, dbConnection);
                                foreach (var usr in sessions)
                                {
                                    users.Add(usr);
                                    var getallUsers = Users.GetAllFullUsersByManagerIdForDirector(usr.ID, dbConnection);
                                    foreach (var subsubuser in getallUsers)
                                    {
                                        users.Add(subsubuser);
                                    }
                                }
                                var unassigned = Users.GetAllUnassignedOperators(dbConnection);
                                unassigned = unassigned.Where(i => i.SiteId == (int)userinfo.SiteId).ToList();
                                foreach (var subsubuser in unassigned)
                                {
                                    users.Add(subsubuser);
                                }
                            }
                            else if (userinfo.RoleId == (int)Role.Director)
                            {
                                users = Users.GetAllUsersBySiteId(userinfo.SiteId, dbConnection);
                            }
                            else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                            {
                                users = Users.GetAllUsersByCustomerId(userinfo.CustomerInfoId, dbConnection);
                                users = users.Where(i => i.RoleId != (int)Role.CustomerSuperadmin).ToList();
                            }
                            else if (userinfo.RoleId == (int)Role.Regional)
                            {
                                users.AddRange(Users.GetAllUsersByLevel5(userinfo.ID, dbConnection));

                            }
                            else if (userinfo.RoleId == (int)Role.ChiefOfficer)
                            {
                                users = Users.GetAllUsers(dbConnection);
                                users = users.Where(i => i.RoleId != (int)Role.ChiefOfficer).ToList();
                            }
                            else if (userinfo.RoleId == (int)Role.SuperAdmin)
                            {
                                users = Users.GetAllUsers(dbConnection);
                            }
                        }

                        if (eventsCB)
                        {
                            if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                            {
                                cusEvents = CustomEvent.GetAllCustomEventByType((int)CustomEvent.EventTypes.DriverOffence, dbConnection);
                            }
                            else if (userinfo.RoleId == (int)Role.CustomerUser)
                            {
                                cusEvents = CustomEvent.GetAllCustomEventByTypeAndCId((int)CustomEvent.EventTypes.DriverOffence, userinfo.CustomerInfoId, dbConnection);
                                cusEvents = cusEvents.Where(i => i.UserName == userinfo.Username).ToList();
                            }
                            else
                            {
                                cusEvents = CustomEvent.GetAllCustomEventByTypeAndCId((int)CustomEvent.EventTypes.DriverOffence, userinfo.CustomerInfoId, dbConnection);
                            } 
                        }
                    }
                }
                json += "[";


                var grouped = users.GroupBy(item => item.ID);
                users = grouped.Select(grp => grp.OrderBy(item => item.ID).First()).ToList();

                foreach (var item in users)
                {
                    if (item.Username != userinfo.Username)
                    {
                        var newLoginDate = item.LastLogin.Value.AddHours(userinfo.TimeZone).ToString("d MMM  hh:mm tt");
                        if (item.Active == (int)Arrowlabs.Business.Layer.HealthCheck.DeviceStatus.Online)
                        {
                            if (item.RoleId == (int)Role.Manager || item.RoleId == (int)Role.Admin)
                            {
                                if (item.IsServerPortal)
                                {
                                    json += "{ \"Username\" : \"" + item.Username + "\",\"Uname\" : \"" + item.CustomerUName + "\",\"Id\" : \"" + item.ID.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + newLoginDate + "\",\"State\" : \"OFFUSER\",\"Logs\" : \"User\",\"DName\" : \"" + item.FirstName + " " + item.LastName + "\"},";
                                }
                                else
                                {
                                    json += "{ \"Username\" : \"" + item.Username + "\",\"Uname\" : \"" + item.CustomerUName + "\",\"Id\" : \"" + item.ID.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + newLoginDate + "\",\"State\" : \"GREEN\",\"Logs\" : \"User\",\"DName\" : \"" + item.FirstName + " " + item.LastName + "\"},";
                                }
                            }
                            else
                            {
                                json += "{ \"Username\" : \"" + item.Username + "\",\"Uname\" : \"" + item.CustomerUName + "\",\"Id\" : \"" + item.ID.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + newLoginDate + "\",\"State\" : \"GREEN\",\"Logs\" : \"User\",\"DName\" : \"" + item.FirstName + " " + item.LastName + "\"},";
                            }
                        }
                        else if (item.Active == (int)Arrowlabs.Business.Layer.HealthCheck.DeviceStatus.Error)
                        {
                            json += "{ \"Username\" : \"" + item.Username + "\",\"Uname\" : \"" + item.CustomerUName + "\",\"Id\" : \"" + item.ID.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + newLoginDate + "\",\"State\" : \"YELLOW\",\"Logs\" : \"User\",\"DName\" : \"" + item.FirstName + " " + item.LastName + "\"},";
                        }
                        else if (item.Active == (int)Arrowlabs.Business.Layer.HealthCheck.DeviceStatus.Offline)
                        {
                            json += "{ \"Username\" : \"" + item.Username + "\",\"Uname\" : \"" + item.CustomerUName + "\",\"Id\" : \"" + item.ID.ToString() + "\",\"Long\" : \"" + item.Longitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + newLoginDate + "\",\"State\" : \"OFFUSER\",\"Logs\" : \"User\",\"DName\" : \"" + item.FirstName + " " + item.LastName + "\"},";
                        }
                    }
                }

                foreach (var item in cusEvents)
                {
                    if (item.EventType == CustomEvent.EventTypes.DriverOffence)
                    {
                        var getDoFF = DriverOffence.GetDriverOffenceById(item.Identifier, dbConnection);
                        if (getDoFF != null)
                        {
                            if (getDoFF.TicketStatus == (int)CustomEvent.IncidentActionStatus.Engage)
                            {
                                json += "{ \"Username\" : \"" + getDoFF.OffenceCategory + "-" + item.CustomerIncidentId + "\",\"Id\" : \"" + item.EventId + "\",\"Long\" : \"" + item.Longtitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + item.RecevieTime.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"YELLOW\",\"Logs\" : \"INCIDENT\",\"DName\" : \"" + getDoFF.OffenceCategory + "-" + item.CustomerIncidentId + "\"},";
                            }
                            else if (getDoFF.TicketStatus == (int)CustomEvent.IncidentActionStatus.Complete)
                            {
                                json += "{ \"Username\" : \"" + getDoFF.OffenceCategory + "-" + item.CustomerIncidentId + "\",\"Id\" : \"" + item.EventId + "\",\"Long\" : \"" + item.Longtitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + item.RecevieTime.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"GREEN\",\"Logs\" : \"INCIDENT\",\"DName\" : \"" + getDoFF.OffenceCategory + "-" + item.CustomerIncidentId + "\"},";
                            }
                            else
                                json += "{ \"Username\" : \"" + getDoFF.OffenceCategory + "-" + item.CustomerIncidentId + "\",\"Id\" : \"" + item.EventId + "\",\"Long\" : \"" + item.Longtitude.ToString() + "\",\"Lat\" : \"" + item.Latitude.ToString() + "\",\"LastLog\" : \"" + item.RecevieTime.Value.AddHours(userinfo.TimeZone).ToString() + "\",\"State\" : \"PURPLE\",\"Logs\" : \"INCIDENT\",\"DName\" : \"" + getDoFF.OffenceCategory + "-" + item.CustomerIncidentId + "\"},";
                        }
                    }
                }  

                json = json.Substring(0, json.Length - 1);
                json += "]";
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Default.aspx", "getGPSDataFull", ex, dbConnection, userinfo.SiteId);
            }
            return json;
        }

    }
}
