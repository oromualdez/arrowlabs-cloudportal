﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Arrowlabs.Business.Layer;
using System.Configuration;
using ArrowLabs.Licence.Portal.Helpers;
using System.Web.Security;
using System.Web.Mvc;

namespace ArrowLabs.Licence.Portal.Account
{

    public partial class Login : System.Web.UI.Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
            TextBox txtUserName = (TextBox)LoginUser.FindControl("UserName"); 

            if (!IsPostBack)
            {
                if (!String.IsNullOrEmpty(Request.QueryString["u"]) && !String.IsNullOrEmpty(Request.QueryString["p"]))
                {
                    LoginUser.UserName = Request.QueryString["u"].ToString();//Decrypt.DecryptData(Request.QueryString["u"].ToString(),true);
                    LoginUser_Authenticate(new object(), new AuthenticateEventArgs());
                }
            }
            try
            {
                if (!IsPostBack)
                {
                    if (Request.Cookies["loginCookie"] != null)
                    {
                        HttpCookie loginCookie = Request.Cookies["loginCookie"];
                        LoginUser.UserName = Decrypt.DecryptData(loginCookie.Values["username"].ToString(), true, CommonUtility.dbConnection);
                        LoginUser.RememberMeSet = true;
                    }
                }
                //var langs = LanguageClass.GetAllLanguage(CommonUtility.dbConnection);
                //var langlist = new List<LanguageClass>();
                //var newLang = new LanguageClass();
                //newLang.LanguageName = "Please Select Language";
                //langlist.Add(newLang);
                //langlist.AddRange(langs);
                //DropDownList UsersList = LoginUser.FindControl("languageSelect") as DropDownList;
                //UsersList.DataTextField = "LanguageName";
                //UsersList.DataValueField = "LanguageName";
                //UsersList.DataSource = langlist;
                //UsersList.DataBind();
            }
            catch (Exception er)
            {
                MIMSLog.MIMSLogSave("Login", "Page_Load", er, CommonUtility.dbConnection);
            }

            this.SetFocus(txtUserName);
        }
        string dbConnection { get; set; }
        protected string GetIPAddress()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }

            return context.Request.ServerVariables["REMOTE_ADDR"];
        }
        [Authorize]
        protected void LoginUser_Authenticate(object sender, AuthenticateEventArgs e)
        {

            dbConnection = CommonUtility.dbConnection;
            try
            {
                var serviceCheck = true;//false;

                if (serviceCheck)
                {
                    var superuser = Users.GetUserByName(LoginUser.UserName, dbConnection);
                    var localip = GetIPAddress();
                    var isactive = true;
                    var IsLogin = false;
                    var msg = string.Empty;
                    var userLogin = Users.Login(LoginUser.UserName, (!String.IsNullOrEmpty(Request.QueryString["p"]) ? Decrypt.DecryptData(Request.QueryString["p"].ToString().Replace(" ", "+"), true, dbConnection) : LoginUser.Password), dbConnection);
                    var customerId = 0;
                    var isCus = false;
                    CustomerInfo cuInfo;
                    if (superuser != null)
                    {
                        IsLogin = true;
                        customerId = superuser.CustomerInfoId;
                        if (superuser.RoleId == (int)Role.SuperAdmin)
                            isCus = true;
                        else
                        {
                                if (customerId > 0)
                                {
                                    cuInfo = CustomerInfo.GetCustomerInfoById(customerId, dbConnection);
                                    if (cuInfo != null)
                                        isCus = cuInfo.Active;
                                    else
                                    {
                                        e.Authenticated = false;
                                        LoginUser.FailureText = "Customer account could not be found.";
                                    }

                                    if (isCus)
                                    {
                                        var gUsers = Users.GetAllUsersByCustomerId(superuser.CustomerInfoId, dbConnection);
                                        if (gUsers != null && gUsers.Count > 0)
                                        {
                                            if (cuInfo != null)
                                            {
                                                if ((gUsers.Count) < (cuInfo.TotalUser + 1))
                                                {
                                                    isCus = true;
                                                }
                                                else
                                                {
                                                    isCus = false;
                                                    e.Authenticated = false;
                                                    LoginUser.FailureText = "Maximum number of users created has exceeded the allowed license for this customer.";
                                                }
                                            }
                                            else
                                            {
                                                e.Authenticated = false;
                                                LoginUser.FailureText = "Customer account could not be found.";
                                            }
                                        }
                                    }
                                    else
                                    {
                                        e.Authenticated = false;
                                        LoginUser.FailureText = "Customer account is not active.";
                                    }
                                }
                                else
                                {
                                    e.Authenticated = false;
                                    LoginUser.FailureText = "Customer account could not be found.";
                                }
                        }
                    }
                    else
                    {
                        e.Authenticated = false;
                        LoginUser.FailureText = "Login failed. Username or password is incorrect.";
                        var loginHistory = new LoginSession();
                        loginHistory.IPAddress = localip;
                        loginHistory.LoginDate = CommonUtility.getDTNow();
                        loginHistory.LogoutDate = CommonUtility.getDTNow();
                        loginHistory.MacAddress = LoginUser.UserName;
                        loginHistory.SiteId = 0;
                        loginHistory.LoggedIn = true;
                        loginHistory.SessionId = System.Web.HttpContext.Current.Session.SessionID;
                        loginHistory.PCName = "Login failed. Username or password is incorrect.";
                        LoginSession.InsertOrUpdateLoginHistory(loginHistory, dbConnection, CommonUtility.dbConnectionAudit, true);
                        //
                    }

                    if (superuser != null && IsLogin && isCus) 
                    {

                        var allowedOnlineMobileUsers = 0;
                        var clientLicense = ClientLicence.GetLicenseByClientID(CommonUtility.arrowlabsKey, dbConnection);
                        var retUrl = string.Empty;

                        if (clientLicense != null)
                        {
                            allowedOnlineMobileUsers = Convert.ToInt32(clientLicense.TotalMobileUsers);
                        }
                        var currentOnlineMobileUsers = Users.GetAllMobileOnlineUsers(dbConnection);
                        var licensevalid = false;
                        if (currentOnlineMobileUsers < allowedOnlineMobileUsers)
                        {
                            licensevalid = true;
                        }
                        else
                        {

                            if (superuser.RoleId == (int)Role.SuperAdmin)
                            {
                                licensevalid = true;
                            }
                            else
                            {
                                licensevalid = false;
                                LoginUser.FailureText = "Login failed. You are exceeding from allowed online devices.";
                                var loginHistory = new LoginSession();
                                loginHistory.IPAddress = localip;
                                loginHistory.LoginDate = CommonUtility.getDTNow();
                                loginHistory.LogoutDate = CommonUtility.getDTNow();
                                loginHistory.MacAddress = LoginUser.UserName;
                                loginHistory.SiteId = 0;
                                loginHistory.SessionId = System.Web.HttpContext.Current.Session.SessionID;
                                loginHistory.PCName = "Login failed. You are exceeding from allowed online devices.";
                                LoginSession.InsertOrUpdateLoginHistory(loginHistory, dbConnection, CommonUtility.dbConnectionAudit, true);
                            }
                        }
                        if (licensevalid)
                        {
                            if (LoginUser.RememberMeSet)
                            {
                                HttpCookie loginCookie = new HttpCookie("loginCookie");
                                Response.Cookies.Remove("loginCookie");
                                Response.Cookies.Add(loginCookie);
                                loginCookie.Values.Add("username", Encrypt.EncryptData(LoginUser.UserName.ToString(), true, dbConnection));
                                DateTime dtExpiry = CommonUtility.getDTNow().AddDays(15);
                                Response.Cookies["loginCookie"].Expires = dtExpiry;
                            }
                            else
                            {
                                HttpCookie loginCookie = new HttpCookie("loginCookie");
                                Response.Cookies.Remove("loginCookie");
                                Response.Cookies.Add(loginCookie);
                                loginCookie.Values.Add("username", Encrypt.EncryptData(LoginUser.UserName.ToString(), true, dbConnection));
                                DateTime dtExpiry = CommonUtility.getDTNow().AddSeconds(1); //you can add years and months too here
                                Response.Cookies["loginCookie"].Expires = dtExpiry;
                            }
                            var loggedInUser = Users.Login(LoginUser.UserName, LoginUser.Password, dbConnection);
                            if (loggedInUser != null && (loggedInUser.RoleId == (int)Role.Manager || loggedInUser.RoleId == (int)Role.Admin || loggedInUser.RoleId == (int)Role.SuperAdmin || loggedInUser.RoleId == (int)Role.Director || loggedInUser.RoleId == (int)Role.Regional || loggedInUser.RoleId == (int)Role.ChiefOfficer || loggedInUser.RoleId == (int)Role.CustomerSuperadmin || loggedInUser.RoleId == (int)Role.CustomerUser || loggedInUser.RoleId == (int)Role.MessageBoardUser))
                            {
                                var isValid = true;

                                if (isValid)
                                {
                                    if (loggedInUser.Register != "2")
                                    {
                                        LoginSession.LogoutUserByUsername(LoginUser.UserName, dbConnection);
                                        //var pushDev = PushNotificationDevice.GetPushNotificationDeviceByUsername(LoginUser.UserName, dbConnection);
                                        //if (pushDev != null)
                                        //{
                                        //    if (!string.IsNullOrEmpty(pushDev.Username))
                                        //    {
                                        //        pushDev.IsServerPortal = true;
                                        //        PushNotificationDevice.InsertorUpdatePushNotificationDevice(pushDev, dbConnection);
                                        //    }
                                        //}

                                        PushNotificationDevice.UpdatePushNotificationDeviceByUsername(loggedInUser.Username, loggedInUser.SiteId, dbConnection);

                                        Session["SessionId"] = System.Web.HttpContext.Current.Session.SessionID;
                                        var userLoginDetail = new LoginSession();
                                        userLoginDetail.Username = LoginUser.UserName;
                                        userLoginDetail.SessionId = System.Web.HttpContext.Current.Session.SessionID;
                                        userLoginDetail.LoggedIn = true;
                                        userLoginDetail.DeviceType = loggedInUser.DeviceType;
                                        userLoginDetail.SiteId = (loggedInUser == null) ? 0 : loggedInUser.SiteId;
                                        userLoginDetail.CustomerId = (loggedInUser == null) ? 0 : loggedInUser.CustomerInfoId;
                                        LoginSession.InsertLogin(userLoginDetail, dbConnection);

                                        var loginHistory = new LoginSession();
                                        loginHistory.IPAddress = localip;
                                        loginHistory.LoginDate = CommonUtility.getDTNow();
                                        loginHistory.LogoutDate = CommonUtility.getDTNow();
                                        loginHistory.MacAddress = LoginUser.UserName;
                                        loginHistory.SiteId = (loggedInUser == null) ? 0 : loggedInUser.SiteId;
                                        loginHistory.CustomerId = (loggedInUser == null) ? 0 : loggedInUser.CustomerInfoId;
                                        loginHistory.LoggedIn = true;
                                        loginHistory.SessionId = System.Web.HttpContext.Current.Session.SessionID;
                                        loginHistory.PCName = "Successful login";
                                        LoginSession.InsertOrUpdateLoginHistory(loginHistory, dbConnection, CommonUtility.dbConnectionAudit, true);


                                        if (loggedInUser != null)
                                        {
                                            loggedInUser.LastLogin = CommonUtility.getDTNow();
                                            Users.InsertOrUpdateUsers(loggedInUser, CommonUtility.dbConnection, CommonUtility.dbConnectionAudit, true);
                                        }
                                        Users.IsMobileOrServerUser(loggedInUser.Username, dbConnection);
                                        e.Authenticated = true;
                                        var uModules = UserModules.GetAllModulesByUsername(LoginUser.UserName, dbConnection);
                                        if (uModules.Count > 0)
                                        {
                                            foreach (var mods in uModules)
                                            {
                                                if (mods.ModuleId == (int)Accounts.ModuleTypes.Dash)
                                                {
                                                    if (!String.IsNullOrEmpty(Request.QueryString["u"]) & !String.IsNullOrEmpty(Request.QueryString["p"]))
                                                        Response.Redirect("~/Default.aspx");
                                                    else
                                                        LoginUser.DestinationPageUrl = "~/Default.aspx";
                                                    break;
                                                }
                                                else if (mods.ModuleId == (int)Accounts.ModuleTypes.Alarms)
                                                {
                                                    LoginUser.DestinationPageUrl = "~/Pages/Incident.aspx";
                                                    retUrl = "~/Pages/Incident.aspx";
                                                    break;
                                                }
                                                else if (mods.ModuleId == (int)Accounts.ModuleTypes.Database)
                                                {
                                                    LoginUser.DestinationPageUrl = "~/Pages/Ticketing.aspx";
                                                    retUrl = "~/Pages/Ticketing.aspx";
                                                    break;
                                                }
                                                else if (mods.ModuleId == (int)Accounts.ModuleTypes.Tasks)
                                                {
                                                    LoginUser.DestinationPageUrl = "~/Pages/Tasks.aspx";
                                                    retUrl = "~/Pages/Tasks.aspx";
                                                    break;
                                                }
                                                else if (mods.ModuleId == (int)Accounts.ModuleTypes.Reports)
                                                {
                                                    LoginUser.DestinationPageUrl = "~/ReportPages/Reports.aspx";
                                                    retUrl = "~/ReportPages/Reports.aspx";
                                                    break;
                                                }
                                                else if (mods.ModuleId == (int)Accounts.ModuleTypes.Notification)
                                                {
                                                    LoginUser.DestinationPageUrl = "~/Pages/Messages.aspx";
                                                    retUrl = "~/Pages/Messages.aspx";
                                                    break;
                                                }
                                                else if (mods.ModuleId == (int)Accounts.ModuleTypes.Verifier)
                                                {
                                                    LoginUser.DestinationPageUrl = "~/Pages/VerifierPage.aspx";
                                                    retUrl = "~/Pages/VerifierPage.aspx";
                                                    break;
                                                }
                                                else if (mods.ModuleId == (int)Accounts.ModuleTypes.Devices)
                                                {
                                                    LoginUser.DestinationPageUrl = "~/Pages/Devices.aspx";
                                                    retUrl = "~/Pages/Devices.aspx";
                                                    break;
                                                }
                                                else if (mods.ModuleId == (int)Accounts.ModuleTypes.System)
                                                {
                                                    LoginUser.DestinationPageUrl = "~/Pages/UsersDB.aspx";
                                                    retUrl = "~/Pages/UsersDB.aspx";
                                                    break;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            LoginUser.DestinationPageUrl = "~/Default.aspx";
                                        }
                                    }
                                    else
                                    {
                                        LoginUser.FailureText = "User has been deactivated, Kindly contact administrator.";
                                        var loginHistory = new LoginSession();
                                        loginHistory.IPAddress = localip;
                                        loginHistory.LoginDate = CommonUtility.getDTNow();
                                        loginHistory.LogoutDate = CommonUtility.getDTNow();
                                        loginHistory.MacAddress = LoginUser.UserName;
                                        loginHistory.SiteId = (loggedInUser == null) ? 0 : loggedInUser.SiteId;
                                        loginHistory.CustomerId = (loggedInUser == null) ? 0 : loggedInUser.CustomerInfoId;
                                        loginHistory.SessionId = System.Web.HttpContext.Current.Session.SessionID;
                                        loginHistory.PCName = "Deactivated user tried to login";
                                        LoginSession.InsertOrUpdateLoginHistory(loginHistory, dbConnection, CommonUtility.dbConnectionAudit, true);
                                    }
                                }
                            }
                            else if (!IsLogin)
                            {
                                var gUser = Users.GetUserByName(LoginUser.UserName, dbConnection);
                                if (gUser != null)
                                {

                                    if (gUser.Register != "2")
                                    {
                                        var loginHistory = new LoginSession();
                                        loginHistory.IPAddress = localip;
                                        loginHistory.LoginDate = CommonUtility.getDTNow();
                                        loginHistory.LogoutDate = CommonUtility.getDTNow();
                                        loginHistory.MacAddress = LoginUser.UserName;
                                        loginHistory.SiteId = (loggedInUser == null) ? 0 : loggedInUser.SiteId;
                                        loginHistory.CustomerId = (loggedInUser == null) ? 0 : loggedInUser.CustomerInfoId;
                                        loginHistory.SessionId = System.Web.HttpContext.Current.Session.SessionID;
                                        loginHistory.PCName = "Failed login attempt";
                                        LoginSession.InsertOrUpdateLoginHistory(loginHistory, dbConnection, CommonUtility.dbConnectionAudit, true);
                                    }
                                    else
                                    {
                                        LoginUser.FailureText = "User has been deactivated, Kindly contact administrator.";
                                        var loginHistory = new LoginSession();
                                        loginHistory.IPAddress = localip;
                                        loginHistory.LoginDate = CommonUtility.getDTNow();
                                        loginHistory.LogoutDate = CommonUtility.getDTNow();
                                        loginHistory.MacAddress = LoginUser.UserName;
                                        loginHistory.SiteId = (loggedInUser == null) ? 0 : loggedInUser.SiteId;
                                        loginHistory.CustomerId = (loggedInUser == null) ? 0 : loggedInUser.CustomerInfoId;
                                        loginHistory.SessionId = System.Web.HttpContext.Current.Session.SessionID;
                                        loginHistory.PCName = "Deactivated user tried to login";
                                        LoginSession.InsertOrUpdateLoginHistory(loginHistory, dbConnection, CommonUtility.dbConnectionAudit, true);
                                    }
                                }
                                else
                                {
                                    LoginUser.FailureText = "Failed to login entered wrong credentials.";
                                    var loginHistory = new LoginSession();
                                    loginHistory.IPAddress = localip;
                                    loginHistory.LoginDate = CommonUtility.getDTNow();
                                    loginHistory.LogoutDate = CommonUtility.getDTNow();
                                    loginHistory.MacAddress = LoginUser.UserName;
                                    loginHistory.SiteId = (loggedInUser == null) ? 0 : loggedInUser.SiteId;
                                    loginHistory.CustomerId = (loggedInUser == null) ? 0 : loggedInUser.CustomerInfoId;
                                    loginHistory.SessionId = System.Web.HttpContext.Current.Session.SessionID;
                                    loginHistory.PCName = "Failed to login entered wrong credentials";
                                    LoginSession.InsertOrUpdateLoginHistory(loginHistory, dbConnection, CommonUtility.dbConnectionAudit, true);
                                }
                            }
                        }
                        else
                        {
                            LoginUser.DestinationPageUrl = "~/Account/login.aspx";
                            if (!isactive)
                            {
                                LoginUser.FailureText = msg;
                            }
                            else if (isactive)
                            {
                                LoginUser.FailureText = "Login failed. Username or password is incorrect.";

                            }
                            else
                                LoginUser.FailureText = "Login failed. You are not allowed to access the system.";

                            var loginHistory = new LoginSession();
                            loginHistory.IPAddress = localip;
                            loginHistory.LoginDate = CommonUtility.getDTNow();
                            loginHistory.LogoutDate = CommonUtility.getDTNow();
                            loginHistory.MacAddress = LoginUser.UserName;
                            loginHistory.SiteId = 0;
                            loginHistory.LoggedIn = true;
                            loginHistory.SessionId = System.Web.HttpContext.Current.Session.SessionID;
                            loginHistory.PCName = LoginUser.FailureText;
                            LoginSession.InsertOrUpdateLoginHistory(loginHistory, dbConnection, CommonUtility.dbConnectionAudit, true);
                        }

                    }
                    //else
                    //{
                    //    LoginUser.DestinationPageUrl = "~/Account/login.aspx";
                    //    if (!isactive)
                    //    {
                    //        LoginUser.FailureText = msg;
                    //    }
                    //    else if (isactive && superuser != null)
                    //    {
                    //        msg = "Login failed. Username or password is incorrect.";
                    //        LoginUser.FailureText = msg;
                    //    }
                    //    else
                    //    {
                    //        msg = "Login failed. You are not allowed to access the system.";
                    //        LoginUser.FailureText = msg;
                    //    }

                    //    var loginHistory = new LoginSession();
                    //    loginHistory.IPAddress = localip;
                    //    loginHistory.LoginDate = CommonUtility.getDTNow();
                    //    loginHistory.LogoutDate = CommonUtility.getDTNow();
                    //    loginHistory.MacAddress = LoginUser.UserName;
                    //    loginHistory.SiteId = 0;
                    //    loginHistory.LoggedIn = true;
                    //    loginHistory.SessionId = System.Web.HttpContext.Current.Session.SessionID;
                    //    loginHistory.PCName = msg;
                    //    LoginSession.InsertOrUpdateLoginHistory(loginHistory, dbConnection, CommonUtility.dbConnectionAudit, true);
                    //}
                }
                else
                {
                    e.Authenticated = false;
                    LoginUser.FailureText = "Notification Service is not running or error occured while connecting. System will not let you login.";
                }
            }
            catch (Exception er)
            {
                LoginUser.FailureText = er.Message;
                MIMSLog.MIMSLogSave("Login", "LoginUser_Authenticate", er, CommonUtility.dbConnection);
            }
        }
        
        [System.Web.Services.WebMethod]
        public static string saveInfo(string name, string address, string country, string email, string phoneno,
            string total,
            bool notimodule, bool incimodule, bool taskmodule, bool ticketmodule,
            bool postmodule, bool verimodule, bool chatmodule, bool drmodule, bool lfmodule,
            bool survmodule, bool reqmodule, bool collabmodule, bool dispatchmodule, bool locationmodule,
             bool waremodule, string password, string lastname, bool activity)
        {
            var listy = string.Empty;

            try
            {
                var moduleString = string.Empty;
                moduleString += ((int)Arrowlabs.Business.Layer.ClientLicence.LicenseModuleTypes.Notification).ToString() + "?";
                moduleString += ((int)Arrowlabs.Business.Layer.ClientLicence.LicenseModuleTypes.Location).ToString() + "?";
                if (survmodule)
                {
                    moduleString += ((int)Arrowlabs.Business.Layer.ClientLicence.LicenseModuleTypes.Surveillance).ToString() + "?";
                }
                if (notimodule) //NEW MESSAGE BOARD
                {
                    moduleString += ((int)Arrowlabs.Business.Layer.ClientLicence.LicenseModuleTypes.MessageBoard).ToString() + "?";
                }
                if (locationmodule) //NEW CONTRACT
                {
                    moduleString += ((int)Arrowlabs.Business.Layer.ClientLicence.LicenseModuleTypes.Contract).ToString() + "?";
                }
                if (ticketmodule)
                {
                    moduleString += ((int)Arrowlabs.Business.Layer.ClientLicence.LicenseModuleTypes.Ticketing).ToString() + "?";
                }
                if (taskmodule)
                {
                    moduleString += ((int)Arrowlabs.Business.Layer.ClientLicence.LicenseModuleTypes.Task).ToString() + "?";
                }
                if (incimodule)
                {
                    moduleString += ((int)Arrowlabs.Business.Layer.ClientLicence.LicenseModuleTypes.Incident).ToString() + "?";
                }
                if (waremodule)
                {
                    moduleString += ((int)Arrowlabs.Business.Layer.ClientLicence.LicenseModuleTypes.Warehouse).ToString() + "?";
                }
                if (chatmodule)
                {
                    moduleString += ((int)Arrowlabs.Business.Layer.ClientLicence.LicenseModuleTypes.Chat).ToString() + "?";
                }
                //if (collabmodule)
                //{
                    moduleString += ((int)Arrowlabs.Business.Layer.ClientLicence.LicenseModuleTypes.Collaboration).ToString() + "?";
                //}
                if (reqmodule)
                {
                    moduleString += ((int)Arrowlabs.Business.Layer.ClientLicence.LicenseModuleTypes.Request).ToString() + "?";
                }
                if (drmodule)
                {
                    moduleString += ((int)Arrowlabs.Business.Layer.ClientLicence.LicenseModuleTypes.DutyRoster).ToString() + "?";
                }
                if (postmodule)
                {
                    moduleString += ((int)Arrowlabs.Business.Layer.ClientLicence.LicenseModuleTypes.PostOrder).ToString() + "?";
                }
                if (lfmodule)
                {
                    moduleString += ((int)Arrowlabs.Business.Layer.ClientLicence.LicenseModuleTypes.LostandFound).ToString() + "?";
                }
                if (verimodule)
                {
                    moduleString += ((int)Arrowlabs.Business.Layer.ClientLicence.LicenseModuleTypes.Verification).ToString() + "?";
                }
                if (dispatchmodule)
                {
                    moduleString += ((int)Arrowlabs.Business.Layer.ClientLicence.LicenseModuleTypes.Dispatch).ToString() + "?";
                }
                if (activity)
                {
                    moduleString += ((int)Arrowlabs.Business.Layer.ClientLicence.LicenseModuleTypes.Activity).ToString() + "?";
                }
                var newUser = Users.GetUserByName(email, CommonUtility.dbConnection);
                var gInfo = CustomerInfo.GetCustomerInfoByEmail(email, CommonUtility.dbConnection);
                if (gInfo == null && newUser == null)
                {
                    var infoData = new CustomerInfo();

                    var split = country.Split('(');
                    var cname = split[0];
                    var spli2 = split[1].Split(')');
                    var doubles = spli2[0];
                    var ctimezone = Convert.ToDouble(doubles.Split(':')[0]);
                    infoData.Active = false;
                    infoData.Address = address;
                    infoData.Country = cname;
                    infoData.TimeZone = ctimezone;
                    infoData.CreatedBy = "SystemGenerated";
                    infoData.CreatedDate = CommonUtility.getDTNow();
                    infoData.Email = email;
                    infoData.Modules = moduleString;
                    infoData.Name = name;
                    infoData.PhoneNo = phoneno;
                    infoData.TotalUser = Convert.ToInt32(total);
                    infoData.UpdatedBy = "SystemGenerated";
                    infoData.UpdatedDate = CommonUtility.getDTNow();
                    infoData.LastName = lastname;
                    infoData.MIMSMobileGPSInterval = 30;
                    infoData.MaxUploadSize = 5;
                    infoData.MaxNumberAttachments = 3;
                    var latlng = ReverseGeocode.RetrieveFormatedGeo(cname);
                    if (latlng.Count > 0)
                    {
                        infoData.Lati = latlng[0];
                        infoData.Long = latlng[1];
                    }

                    infoData.AverageHourlyCost = 0;
                    infoData.WorkingHoursWeek = 6;
                    infoData.WorkingDaysWeek = 48;

                    var retId = CustomerInfo.InsertorUpdateCustomerInfo(infoData, CommonUtility.dbConnection, CommonUtility.dbConnectionAudit, true);
                    if (retId > 0)
                    {
                        var customData = CustomerInfo.GetCustomerInfoById(retId, CommonUtility.dbConnection);
                       
                        if (newUser == null || newUser.ID == 0)
                        {
                            newUser = new Users();
                            newUser.Username = customData.Email;
                            newUser.CustomerInfoId = customData.Id;
                            newUser.RoleId = (int)Role.CustomerSuperadmin;
                            newUser.Password = Encrypt.EncryptData(password, true, CommonUtility.dbConnection);
                            newUser.Telephone = customData.PhoneNo;
                            newUser.FirstName = customData.Name;
                            newUser.LastName = lastname;
                            newUser.Email = customData.Email;
                            newUser.Engaged = false;
                            newUser.EngagedIn = "";
                            newUser.Register = ((int)Users.UserState.Enable).ToString();
                            var now = CommonUtility.getDTNow();
                            newUser.CreatedDate = now;
                            newUser.LastLogin = now;
                            newUser.CreatedBy = "SystemGenerated";
                            newUser.UpdatedBy = "SystemGenerated";
                            newUser.UpdatedDate = now;
                            newUser.DeviceType = 1;
                            var uid = Users.InsertOrUpdateUsers(newUser, CommonUtility.dbConnection, CommonUtility.dbConnectionAudit, true);
                            if (uid)
                            {
                                var id = Users.GetLastUserId(CommonUtility.dbConnection);
                                var modules = customData.Modules.Split('?');

                                UserModules.InsertUserModuleMap(id, (int)Accounts.ModuleTypes.System, CommonUtility.dbConnection, customData.Email, 0, customData.Id);
                                UserModules.InsertUserModuleMap(id, (int)Accounts.ModuleTypes.Dash, CommonUtility.dbConnection, customData.Email, 0, customData.Id);
                                UserModules.InsertUserModuleMap(id, (int)Accounts.ModuleTypes.Reports, CommonUtility.dbConnection, customData.Email, 0, customData.Id);
                                

                                var isCus = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Contract).ToString()).ToList();
                                if (isCus != null && isCus.Count > 0)
                                {
                                    UserModules.InsertUserModuleMap(id, (int)Accounts.ModuleTypes.Customer, CommonUtility.dbConnection, customData.Email, 0, customData.Id);
                                }
                                var isMB = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.MessageBoard).ToString()).ToList();
                                if (isMB != null && isMB.Count > 0)
                                {
                                    UserModules.InsertUserModuleMap(id, (int)Accounts.ModuleTypes.MessageBoard, CommonUtility.dbConnection, customData.Email, 0, customData.Id);
                                }
                                var isNoti = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Notification).ToString()).ToList();
                                if (isNoti != null && isNoti.Count > 0)
                                {
                                    UserModules.InsertUserModuleMap(id, (int)Accounts.ModuleTypes.Notification, CommonUtility.dbConnection, customData.Email, 0, customData.Id);
                                    UserModules.InsertUserModuleMap(id, (int)Accounts.ModuleTypes.MobileNotification, CommonUtility.dbConnection, customData.Email, 0, customData.Id);
                                }
                                var isInci = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Incident).ToString()).ToList();
                                if (isInci != null && isInci.Count > 0)
                                {
                                    UserModules.InsertUserModuleMap(id, (int)Accounts.ModuleTypes.Incident, CommonUtility.dbConnection, customData.Email, 0, customData.Id);
                                    UserModules.InsertUserModuleMap(id, (int)Accounts.ModuleTypes.Alarms, CommonUtility.dbConnection, customData.Email, 0, customData.Id);
                                    UserModules.InsertUserModuleMap(id, (int)Accounts.ModuleTypes.MobileAlarms, CommonUtility.dbConnection, customData.Email, 0, customData.Id);
                                    UserModules.InsertUserModuleMap(id, (int)Accounts.ModuleTypes.MobileHotevent, CommonUtility.dbConnection, customData.Email, 0, customData.Id);

                                }

                                var isTask = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Task).ToString()).ToList();
                                if (isTask != null && isTask.Count > 0)
                                {
                                    UserModules.InsertUserModuleMap(id, (int)Accounts.ModuleTypes.Tasks, CommonUtility.dbConnection, customData.Email, 0, customData.Id);
                                    UserModules.InsertUserModuleMap(id, (int)Accounts.ModuleTypes.MobileTasks, CommonUtility.dbConnection, customData.Email, 0, customData.Id);
                                }

                                var isTicket = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Ticketing).ToString()).ToList();
                                if (isTicket != null && isTicket.Count > 0)
                                {
                                    UserModules.InsertUserModuleMap(id, (int)Accounts.ModuleTypes.Ticketing, CommonUtility.dbConnection, customData.Email, 0, customData.Id);
                                    UserModules.InsertUserModuleMap(id, (int)Accounts.ModuleTypes.Database, CommonUtility.dbConnection, customData.Email, 0, customData.Id);
                                    UserModules.InsertUserModuleMap(id, (int)Accounts.ModuleTypes.MobileTicketing, CommonUtility.dbConnection, customData.Email, 0, customData.Id);
                                }

                                var isPO = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.PostOrder).ToString()).ToList();
                                if (isPO != null && isPO.Count > 0)
                                {
                                    UserModules.InsertUserModuleMap(id, (int)Accounts.ModuleTypes.PostOrder, CommonUtility.dbConnection, customData.Email, 0, customData.Id);
                                    UserModules.InsertUserModuleMap(id, (int)Accounts.ModuleTypes.MobilePostOrder, CommonUtility.dbConnection, customData.Email, 0, customData.Id);
                                }

                                var isVeri = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Verification).ToString()).ToList();
                                if (isVeri != null && isVeri.Count > 0)
                                {
                                    UserModules.InsertUserModuleMap(id, (int)Accounts.ModuleTypes.Verifier, CommonUtility.dbConnection, customData.Email, 0, customData.Id);
                                    UserModules.InsertUserModuleMap(id, (int)Accounts.ModuleTypes.MobileVerifier, CommonUtility.dbConnection, customData.Email, 0, customData.Id);
                                }

                                var isChat = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Chat).ToString()).ToList();
                                if (isChat != null && isChat.Count > 0)
                                {
                                    UserModules.InsertUserModuleMap(id, (int)Accounts.ModuleTypes.Chat, CommonUtility.dbConnection, customData.Email, 0, customData.Id);
                                    UserModules.InsertUserModuleMap(id, (int)Accounts.ModuleTypes.MobileConversation, CommonUtility.dbConnection, customData.Email, 0, customData.Id);

                                }

                                var isDR = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.DutyRoster).ToString()).ToList();
                                if (isDR != null && isDR.Count > 0)
                                {
                                    UserModules.InsertUserModuleMap(id, (int)Accounts.ModuleTypes.DutyRoster, CommonUtility.dbConnection, customData.Email, 0, customData.Id);
                                    UserModules.InsertUserModuleMap(id, (int)Accounts.ModuleTypes.MobileDutyRoster, CommonUtility.dbConnection, customData.Email, 0, customData.Id);
                                }

                                var isLF = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.LostandFound).ToString()).ToList();
                                if (isLF != null && isLF.Count > 0)
                                {
                                    UserModules.InsertUserModuleMap(id, (int)Accounts.ModuleTypes.LostandFound, CommonUtility.dbConnection, customData.Email, 0, customData.Id);
                                    UserModules.InsertUserModuleMap(id, (int)Accounts.ModuleTypes.MobileLostAndFound, CommonUtility.dbConnection, customData.Email, 0, customData.Id);

                                    
                                }

                                var isSurv = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Surveillance).ToString()).ToList();
                                if (isSurv != null && isSurv.Count > 0)
                                {
                                    UserModules.InsertUserModuleMap(id, (int)Accounts.ModuleTypes.Surveillance, CommonUtility.dbConnection, customData.Email, 0, customData.Id);
                                    UserModules.InsertUserModuleMap(id, (int)Accounts.ModuleTypes.MobileSurveillance, CommonUtility.dbConnection, customData.Email, 0, customData.Id);
                                }

                                var isRequest = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Request).ToString()).ToList();
                                if (isRequest != null && isRequest.Count > 0)
                                {
                                    UserModules.InsertUserModuleMap(id, (int)Accounts.ModuleTypes.Request, CommonUtility.dbConnection, customData.Email, 0, customData.Id);
                                    UserModules.InsertUserModuleMap(id, (int)Accounts.ModuleTypes.MobileRequest, CommonUtility.dbConnection, customData.Email, 0, customData.Id);
                                }

                                var isCollab = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Collaboration).ToString()).ToList();
                                if (isCollab != null && isCollab.Count > 0)
                                {
                                    UserModules.InsertUserModuleMap(id, (int)Accounts.ModuleTypes.Collaboration, CommonUtility.dbConnection, customData.Email, 0, customData.Id);
                                    UserModules.InsertUserModuleMap(id, (int)Accounts.ModuleTypes.MobileCollaboration, CommonUtility.dbConnection, customData.Email, 0, customData.Id);
                                }

                                var isDisp = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Dispatch).ToString()).ToList();
                                if (isDisp != null && isDisp.Count > 0)
                                {
                                    UserModules.InsertUserModuleMap(id, (int)Accounts.ModuleTypes.Dispatch, CommonUtility.dbConnection, customData.Email, 0, customData.Id);
                                }

                                var isLoc = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Location).ToString()).ToList();
                                if (isLoc != null && isLoc.Count > 0)
                                {
                                    UserModules.InsertUserModuleMap(id, (int)Accounts.ModuleTypes.Location, CommonUtility.dbConnection, customData.Email, 0, customData.Id);
                                    UserModules.InsertUserModuleMap(id, (int)Accounts.ModuleTypes.MobileLocation, CommonUtility.dbConnection, customData.Email, 0, customData.Id);
                                }
                                var isAct = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Activity).ToString()).ToList();
                                if (isAct != null && isAct.Count > 0)
                                {
                                    UserModules.InsertUserModuleMap(id, (int)Accounts.ModuleTypes.Activity, CommonUtility.dbConnection, customData.Email, 0, customData.Id);
                                    UserModules.InsertUserModuleMap(id, (int)Accounts.ModuleTypes.MobileActivity, CommonUtility.dbConnection, customData.Email, 0, customData.Id);
                                }
                                var isWare = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Warehouse).ToString()).ToList();
                                if (isWare != null && isWare.Count > 0)
                                {
                                    UserModules.InsertUserModuleMap(id, (int)Accounts.ModuleTypes.Warehouse, CommonUtility.dbConnection, customData.Email, 0, customData.Id);
                                    UserModules.InsertUserModuleMap(id, (int)Accounts.ModuleTypes.MobileAsset, CommonUtility.dbConnection, customData.Email, 0, customData.Id);
                                    //UserModules.InsertUserModuleMap(id, (int)Accounts.ModuleTypes.MobileKey, CommonUtility.dbConnection, customData.Email, 0);
                                    //UserModules.InsertUserModuleMap(id, (int)Accounts.ModuleTypes.ServerKey, CommonUtility.dbConnection, customData.Email, 0);
                                }
                                var g = CommonUtility.sendEmail(customData.Email, customData.Name + " " + customData.LastName, "Thank you for SignUp", "Test", "Thank you for signing up to MIMS", "Sample");
                                if (g)
                                    return "SUCCESS";
                                else
                                {
                                    MIMSLog.MIMSLogSave("Login", "Failed to send email confirmation", new Exception(), CommonUtility.dbConnection, 0);
                                    return "A confirmation email will be sent to you shortly.";
                                }
                                //listy = "SUCCESS";
                            }
                            else
                            {
                                MIMSLog.MIMSLogSave("Login", "Failed to save user", new Exception(), CommonUtility.dbConnection, 0);
                                return "A problem occured trying to register. Kindly send email to support@arrowsecure.com";
                            }
                        }
                        else
                        {
                            MIMSLog.MIMSLogSave("Login", "User name already in use", new Exception(), CommonUtility.dbConnection, 0);
                            return "Email address is already in use kindly use another one.";
                        }
                    }
                    else
                    {
                        listy = "Failed to send request, kindly try again later.";
                    }
                }
                else
                {
                    listy = "A client is already using email address . kindly use another a different one.";
                }
            }
            catch (Exception er)
            {
                MIMSLog.MIMSLogSave("Devices", "changeUserState", er, CommonUtility.dbConnection, 0);
                listy = "A problem occured trying to register the client. "+er.Message;
            }
            return listy;
        }

        [System.Web.Services.WebMethod]
        public static string forgotPW(string password)
        {
            var gInfo = CustomerInfo.GetCustomerInfoByEmail(password, CommonUtility.dbConnection);
            if (gInfo != null)
            {
                var newP = Guid.NewGuid().ToString().Split('-')[0];
                var g = CommonUtility.sendEmail(password, gInfo.Name + " " + gInfo.LastName, "Forgot Password", newP, "MIMS Password Reset", "Forgot");
                if (g)
                {
                    var gUser = Users.GetUserByName(password, CommonUtility.dbConnection);
                    var oldV = gUser.Password;
                    gUser.Password = Encrypt.EncryptData(newP, true, CommonUtility.dbConnection);
                    Users.InsertOrUpdateUsers(gUser, CommonUtility.dbConnection, CommonUtility.dbConnectionAudit, true);
                    SystemLogger.SaveSystemLog(CommonUtility.dbConnectionAudit, "Login", oldV, gUser.Password, gUser, "Password reset");
                    return "SUCCESS";
                }
                else
                    return "Failed to send email request. kindly directly send to support@arrowsecure.com";
            }
            else
            {
                return "Was not able to find your email in the system.";
            }
        }
        
    }
}
