﻿using Arrowlabs.Mims.Audit.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Business.Layer
{
    public class JobOTHours
    {
        public int Id { get; set; }
        public DateTime? OTStart { get; set; }
        public DateTime? OTEnd { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int TaskId { get; set; }
        public int SiteId { get; set; }
        public int CustomerId { get; set; }
        public int UserId { get; set; }
        public string CreatedBy { get; set; }
        public string Username { get; set; }


        public static int InsertOrUpdateJobOTHours(JobOTHours customer, string dbConnection,
string auditDbConnection = "", bool isAuditEnabled = true)
        {
            int id = customer.Id;

            var action = (customer.Id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;

            if (customer != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateJobOTHours", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = customer.Id;
                    cmd.Parameters.Add(new SqlParameter("@OTStart", SqlDbType.DateTime));
                    cmd.Parameters["@OTStart"].Value = customer.OTStart;
                    cmd.Parameters.Add(new SqlParameter("@OTEnd", SqlDbType.DateTime));
                    cmd.Parameters["@OTEnd"].Value = customer.OTEnd;
                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = customer.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@TaskId", SqlDbType.Int));
                    cmd.Parameters["@TaskId"].Value = customer.TaskId;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = customer.SiteId;

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = customer.CustomerId;

                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                    cmd.Parameters["@UserId"].Value = customer.UserId;

                    cmd.Parameters.Add(new SqlParameter("@Username", SqlDbType.NVarChar));
                    cmd.Parameters["@Username"].Value = customer.Username;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = customer.CreatedBy;


                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandText = "InsertOrUpdateJobOTHours";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                    if (id == 0)
                        id = (int)returnParameter.Value;
                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            customer.Id = id;
                            var audit = new JobOTHoursAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, customer.CreatedBy);
                            Reflection.CopyProperties(customer, audit);
                            JobOTHoursAudit.InsertJobOTHoursAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertOrUpdateJobOTHours", ex, dbConnection);
                    }
                }
            }
            return id;
        }

        private static JobOTHours GetJobOTHoursFromSqlReader(SqlDataReader reader)
        {
            var customer = new JobOTHours();
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
            if (reader["Id"] != DBNull.Value)
                customer.Id = Convert.ToInt32(reader["Id"].ToString());


            if (reader["TaskId"] != DBNull.Value)
                customer.TaskId = Convert.ToInt32(reader["TaskId"].ToString());

            if (reader["UserId"] != DBNull.Value)
                customer.UserId = Convert.ToInt32(reader["UserId"].ToString());

            if (reader["CustomerId"] != DBNull.Value)
                customer.CustomerId = Convert.ToInt32(reader["CustomerId"].ToString());

            if (reader["SiteId"] != DBNull.Value)
                customer.SiteId = Convert.ToInt32(reader["SiteId"].ToString());

            if (reader["Username"] != DBNull.Value)
                customer.Username = reader["Username"].ToString();

            if (reader["CreatedBy"] != DBNull.Value)
                customer.CreatedBy = reader["CreatedBy"].ToString();

            if (reader["CreatedDate"] != DBNull.Value)
                customer.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());

            if (reader["OTEnd"] != DBNull.Value)
                customer.OTEnd = Convert.ToDateTime(reader["OTEnd"].ToString());

            if (reader["OTStart"] != DBNull.Value)
                customer.OTStart = Convert.ToDateTime(reader["OTStart"].ToString());

            return customer;
        }

        public static List<JobOTHours> GetJobOTHoursByUserId(int id,DateTime start,DateTime end ,string dbConnection)
        {
            var notifications = new List<JobOTHours>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetJobOTHoursByUserId", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlCommand.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int)).Value = id;

                sqlCommand.Parameters.Add(new SqlParameter("@OTStart", SqlDbType.DateTime)).Value = start;

                sqlCommand.Parameters.Add(new SqlParameter("@OTEnd", SqlDbType.DateTime)).Value = end;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var notification = GetJobOTHoursFromSqlReader(reader);
                    notifications.Add(notification);
                }
                connection.Close();
                reader.Close();
            }
            return notifications;
        }

        public static List<JobOTHours> GetJobOTHoursByUserIdForReport(int id, DateTime start, DateTime end, string dbConnection)
        {
            var notifications = new List<JobOTHours>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetJobOTHoursByUserIdForReport", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlCommand.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int)).Value = id;

                sqlCommand.Parameters.Add(new SqlParameter("@OTStart", SqlDbType.DateTime)).Value = start;

                sqlCommand.Parameters.Add(new SqlParameter("@OTEnd", SqlDbType.DateTime)).Value = end;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var notification = GetJobOTHoursFromSqlReader(reader);
                    notifications.Add(notification);
                }
                connection.Close();
                reader.Close();
            }
            return notifications;
        }
    }
}
