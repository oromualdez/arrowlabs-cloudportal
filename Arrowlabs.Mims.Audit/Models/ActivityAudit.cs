﻿using Arrowlabs.Mims.Audit.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Arrowlabs.Mims.Audit.Models
{
    public class ActivityAudit
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ActivityType { get; set; }
        public int ActivityTypeId { get; set; }
        public int SiteId { get; set; }
        public int CustomerId { get; set; }
        public string Username { get; set; }
        public int UserId { get; set; }

        public int Status { get; set; }

        public BaseAudit BaseAudit { get; set; }

        public string StartLati { get; set; }
        public string EndLati { get; set; }
        public string StartLong { get; set; }
        public string EndLong { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }

        public static int InsertActivityAudit(ActivityAudit notification, string dbConnection)
        {
            var baseAuditId = BaseAudit.InsertBaseAudit(notification.BaseAudit, dbConnection);

            int id = 0;
            if (notification != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertActivityAudit", connection);

                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = notification.Id;

                    cmd.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                    cmd.Parameters["@Name"].Value = notification.Name;

                    cmd.Parameters.Add(new SqlParameter("@Description", SqlDbType.NVarChar));
                    cmd.Parameters["@Description"].Value = notification.Description;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = notification.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = notification.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@ActivityType", SqlDbType.NVarChar));
                    cmd.Parameters["@ActivityType"].Value = notification.ActivityType;

                    cmd.Parameters.Add(new SqlParameter("@ActivityTypeId", SqlDbType.Int));
                    cmd.Parameters["@ActivityTypeId"].Value = notification.ActivityTypeId;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = notification.SiteId;

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = notification.CustomerId;

                    cmd.Parameters.Add(new SqlParameter("@Username", SqlDbType.NVarChar));
                    cmd.Parameters["@Username"].Value = notification.Username;

                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                    cmd.Parameters["@UserId"].Value = notification.UserId;

                    cmd.Parameters.Add(new SqlParameter("@Status", SqlDbType.Int));
                    cmd.Parameters["@Status"].Value = notification.Status;

                    cmd.Parameters.Add(new SqlParameter("@StartTime", SqlDbType.DateTime));
                    cmd.Parameters["@StartTime"].Value = notification.StartTime;
                    cmd.Parameters.Add(new SqlParameter("@EndTime", SqlDbType.DateTime));
                    cmd.Parameters["@EndTime"].Value = notification.EndTime;

                    cmd.Parameters.Add(new SqlParameter("@StartLati", SqlDbType.NVarChar));
                    cmd.Parameters["@StartLati"].Value = notification.StartLati;
                    cmd.Parameters.Add(new SqlParameter("@EndLati", SqlDbType.NVarChar));
                    cmd.Parameters["@EndLati"].Value = notification.EndLati;
                    cmd.Parameters.Add(new SqlParameter("@StartLong", SqlDbType.NVarChar));
                    cmd.Parameters["@StartLong"].Value = notification.StartLong;
                    cmd.Parameters.Add(new SqlParameter("@EndLong", SqlDbType.NVarChar));
                    cmd.Parameters["@EndLong"].Value = notification.EndLong;

                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandText = "InsertActivityAudit";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                    id = (int)returnParameter.Value;
                }
            }
            return id;
        }
    }
}
