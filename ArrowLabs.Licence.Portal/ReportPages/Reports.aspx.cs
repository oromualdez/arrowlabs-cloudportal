﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Arrowlabs.Business.Layer;
using System.Web.Services;
using System.Web.Configuration;
using System.Globalization;
using System.Text;
using PushSharp;
using PushSharp.Apple;
using PushSharp.Core;
using System.IO;
using System.Threading;
using ArrowLabs.Licence.Portal.Helpers;
using iTextSharp.text;
using iTextSharp.text.html;
using iTextSharp.text.pdf;
using System.Web.UI.DataVisualization.Charting;

namespace ArrowLabs.Licence.Portal.Pages
{
    public partial class Reports : System.Web.UI.Page
    {
        static string dbConnection { get; set; }

        protected string senderName;
        protected string pendingPercent;
        protected string inprogressPercent;
        protected string completedPercent;
        static int completedCount;
        static int inprogressCount;
        static int pendingCount;
        static int total;
        protected string ipaddress;
        static ClientLicence getClientLic;
        protected string senderName2;
        protected string userinfoDisplay;

        protected string HEView;
        protected string DRView;
        protected string LFView;
        protected string ASView;
        protected string TAView;

        protected string senderName3;
        static string fpath;
        protected string siteName;
        protected void Page_Load(object sender, EventArgs e)
        {
            fpath = Server.MapPath("~/Uploads/");
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/Default.aspx");
            }
            userinfoDisplay = "block";
            senderName2 = Encrypt.EncryptData(User.Identity.Name, true, dbConnection);
            dbConnection = CommonUtility.dbConnection;
            senderName = User.Identity.Name;
            var userinfo = Users.GetUserByName(senderName, dbConnection);
            getClientLic = ClientLicence.GetLicenseByClientID(CommonUtility.arrowlabsKey, dbConnection);
            senderName3 = userinfo.FirstName + " " + userinfo.LastName;
            
            if (userinfo.SiteId > 0)
            {
                siteName = "<a style='margin-left:0px;color:gray' href='#' class='fa fa-building fa-lg'></a><a style='font-size:smaller;color:gray;margin-right:5px'>" + userinfo.SiteName + "</a>";
            }
            var usersearchSelectItems = new List<SearchSelectItem>();
            var allusers = new List<Users>();
            if (userinfo.RoleId != (int)Role.SuperAdmin)
            {
                //var pushDev = PushNotificationDevice.GetPushNotificationDeviceByUsername(userinfo.Username, dbConnection);
                //if (pushDev != null)
                //{
                //    if (!string.IsNullOrEmpty(pushDev.Username))
                //    {
                //        pushDev.IsServerPortal = true;
                //        pushDev.SiteId = userinfo.SiteId;
                //        PushNotificationDevice.InsertorUpdatePushNotificationDevice(pushDev, dbConnection);
                //    }
                //}
                PushNotificationDevice.UpdatePushNotificationDeviceByUsername(userinfo.Username, userinfo.SiteId, dbConnection);

                if (userinfo.RoleId == (int)Role.ChiefOfficer)
                {
                    allusers = Users.GetAllUsers(dbConnection);

                    allusers = allusers.Where(i => i.RoleId != (int)Role.ChiefOfficer).ToList();
                }
                else if (userinfo.RoleId == (int)Role.Admin)
                {
                    var allmanagers = DirectorManager.GetAllFullManagersByDirectorId(userinfo.ID, dbConnection);
                    foreach (var usermanager in allmanagers)
                    {
                        allusers.Add(usermanager);
                        var allmanagerusers = Users.GetAllFullUsersByManagerIdForDirector(usermanager.ID, dbConnection);
                        allusers.AddRange(allmanagerusers);
                    }
                    var unassigned = Users.GetAllUnassignedOperators(dbConnection);
                    unassigned = unassigned.Where(i => i.SiteId == (int)userinfo.SiteId).ToList();
                    foreach (var subsubuser in unassigned)
                    {
                        allusers.Add(subsubuser);
                    }
                }
                else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                {
                    allusers.AddRange(Users.GetAllUsersByCustomerIdNotIncludeCustomerUser(userinfo.CustomerInfoId, dbConnection));
                    allusers = allusers.Where(i => i.RoleId != (int)Role.CustomerSuperadmin).ToList();
                }
                else if (userinfo.RoleId == (int)Role.Manager)
                {
                    allusers.AddRange(Users.GetAllFullUsersByManagerId(userinfo.ID, dbConnection));
                }
                else if (userinfo.RoleId == (int)Role.Director)
                {
                    allusers.AddRange(Users.GetAllUsersBySiteId(userinfo.SiteId, dbConnection));
                }
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                    //var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                    //foreach (var site in sites)
                    //{
                    allusers.AddRange(Users.GetAllUsersByLevel5(userinfo.ID, dbConnection));

                    //}
                }
            }
            else
            {
                allusers = Users.GetAllUsers(dbConnection);
            }
            allusers = allusers.Where(i => i.RoleId != (int)Role.MessageBoardUser && i.RoleId != (int)Role.CustomerUser).ToList();
            if (userinfo.RoleId != (int)Role.SuperAdmin)
            {
                userinfoDisplay = "none";

                if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                    userinfoDisplay = "block";

                var modules = UserModules.GetAllModulesByUsername(userinfo.Username, dbConnection);
                if (modules != null && modules.Count > 0)
                {
                    var heC = modules.Where(i => i.ModuleId == (int)Accounts.ModuleTypes.Alarms).ToList();
                    if (heC.Count > 0)
                    {

                    }
                    else
                        HEView = "style='display:none'";

                    var heC2 = modules.Where(i => i.ModuleId == (int)Accounts.ModuleTypes.DutyRoster).ToList();
                    if (heC2.Count > 0)
                    {

                    }
                    else
                        DRView = "style='display:none'";

                    var heC3 = modules.Where(i => i.ModuleId == (int)Accounts.ModuleTypes.Warehouse).ToList();
                    if (heC3.Count > 0)
                    {

                    }
                    else
                        LFView = "style='display:none'";

                    var heC4 = modules.Where(i => i.ModuleId == (int)Accounts.ModuleTypes.ServerKey).ToList();
                    if (heC4.Count > 0)
                    {

                    }
                    else
                        ASView = "style='display:none'";

                    var heC5 = modules.Where(i => i.ModuleId == (int)Accounts.ModuleTypes.Tasks).ToList();
                    if (heC5.Count > 0)
                    {

                    }
                    else
                        TAView = "style='display:none'";
                }
                else
                {
                    HEView = "style='display:none'";
                    DRView = "style='display:none'";
                    LFView = "style='display:none'";
                    ASView = "style='display:none'";
                    TAView = "style='display:none'";
                }

            }
            var configtSettings = ConfigSettings.GetConfigSettings(dbConnection);
            ipaddress = configtSettings.ChatServerUrl + "/signalr";
            //getEventStatus(userinfo);

            var nuser = new SearchSelectItem();
            nuser.ID = "0";
            nuser.Name = "Please Select";
            usersearchSelectItems.Add(nuser);
            allusers = allusers.OrderBy(i => i.CustomerUName).ToList();
            if (userinfo.RoleId == (int)Role.Director)
            {
                foreach (var searchSelectUser in allusers)
                {
                    if (searchSelectUser.RoleId != (int)Role.Director)
                    {
                        var newSearchSelectItem = new SearchSelectItem();
                        newSearchSelectItem.ID = searchSelectUser.ID.ToString();
                        newSearchSelectItem.Name = searchSelectUser.CustomerUName;
                        usersearchSelectItems.Add(newSearchSelectItem);
                    }
                }
            }
            else
            {
                foreach (var searchSelectUser in allusers)
                {
                    if (searchSelectUser.Username != userinfo.Username)
                    {
                        var newSearchSelectItem = new SearchSelectItem();
                        newSearchSelectItem.ID = searchSelectUser.ID.ToString();
                        newSearchSelectItem.Name = searchSelectUser.CustomerUName;
                        usersearchSelectItems.Add(newSearchSelectItem);
                    }
                }
            }

            statusDivSelectDTUserRoster.DataTextField = "Name";
            statusDivSelectDTUserRoster.DataValueField = "ID";
            statusDivSelectDTUserRoster.DataSource = usersearchSelectItems;
            statusDivSelectDTUserRoster.DataBind();

            var tasktypes = new List<TaskCategory>();
            var tskType = new TaskCategory();
            tskType.CategoryId = 0;
            tskType.Description = "Select Type";
            tasktypes.Add(tskType);

            var gtTypes = TaskCategory.GetAllTaskCategories(dbConnection);
            if (userinfo.RoleId != (int)Role.SuperAdmin)
            {
                if (userinfo.RoleId == (int)Role.CustomerSuperadmin || userinfo.RoleId == (int)Role.CustomerUser)
                {
                    gtTypes = TaskCategory.GetAllTaskCategoriesByCId(userinfo.CustomerInfoId, dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                    gtTypes = TaskCategory.GetAllTaskCategoriesByLevel5(userinfo.ID, dbConnection);
                }
                else
                {
                    gtTypes = TaskCategory.GetAllTaskCategoriesBySiteId(userinfo.SiteId, dbConnection);
                }
            }
            else
            {
                gtTypes = TaskCategory.GetAllTaskCategories(dbConnection);
            }
            tasktypes.AddRange(gtTypes);
            taskTypeSelect.DataTextField = "Description";
            taskTypeSelect.DataValueField = "CategoryId";
            taskTypeSelect.DataSource = tasktypes;
            taskTypeSelect.DataBind();
        }
        [WebMethod]
        public static List<string> getTop5OffenceData(int id, string uname)
        {
            var listy = new List<string>();
            var hotAlarm = new List<CustomEvent>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            if (userinfo != null)
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                if (userinfo.RoleId == (int)Role.SuperAdmin)
                {
                    hotAlarm = CustomEvent.GetAllCustomEventByDateUNHandled(DateTime.Now, DateTime.Now.Subtract(new TimeSpan(30, 0, 0, 0)), dbConnection);
                }
                else
                {
                    hotAlarm = CustomEvent.GetAllCustomEventByDateAndManagerIdUNHandled(DateTime.Now, DateTime.Now.Subtract(new TimeSpan(30, 0, 0, 0)), userinfo.ID, dbConnection);
                }

                var userList = new List<string>();
                foreach (var hot in hotAlarm)
                {
                    if (hot.EventType == CustomEvent.EventTypes.DriverOffence)
                        userList.Add(hot.UserName);

                }
                Dictionary<string, int> counts = userList.GroupBy(x => x)
                                      .ToDictionary(g => g.Key,
                                                    g => g.Count());
                var items = from pair in counts
                            orderby pair.Value descending
                            select pair;

                if (userList.Count > 0)
                {
                    foreach (KeyValuePair<string, int> pair in items)
                    {
                        var returnstring = "<tr role='row' class='odd'><td class='sorting_1'>" + pair.Key.ToString() + "</td><td>" + pair.Value.ToString() + "</td><td> </td><td> </td><td> </td></tr>";
                        listy.Add(returnstring);
                        //json += "{ \"Name\" : \"" + pair.Key.ToString() + "\",\"Alarms\" : \"" + pair.Value.ToString() + "\"},";
                    }
                }
            }
            //<tr role="row" class="odd clickable-row clickable-row-links"><td><span class="circle-point-container"><span class="circle-point circle-point-orange"></span></span></td><td class="sorting_1">Gecko</td><td>Firefox 1.0</td><td>Win 98+ / OSX.2+</td><td>'++'</td><td><a href="#"><i class="fa fa-eye mr-1x"></i>View</a></td></tr>
            return listy;
        }
        [WebMethod]
        public static List<string> getTop5HotEventData(int id, string uname)
        {
            var listy = new List<string>();
            var hotAlarm = new List<CustomEvent>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            if (userinfo != null)
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                if (userinfo.RoleId == (int)Role.SuperAdmin)
                {
                    hotAlarm = CustomEvent.GetAllCustomEventByDateUNHandled(DateTime.Now, DateTime.Now.Subtract(new TimeSpan(30, 0, 0, 0)), dbConnection);
                }
                else
                {
                    hotAlarm = CustomEvent.GetAllCustomEventByDateAndManagerIdUNHandled(DateTime.Now, DateTime.Now.Subtract(new TimeSpan(30, 0, 0, 0)), userinfo.ID, dbConnection);
                }

                var userList = new List<string>();
                foreach (var hot in hotAlarm)
                {
                    if (hot.EventType == CustomEvent.EventTypes.HotEvent || hot.EventType == CustomEvent.EventTypes.MobileHotEvent || hot.EventType == CustomEvent.EventTypes.Request)

                        userList.Add(hot.UserName);

                }
                Dictionary<string, int> counts = userList.GroupBy(x => x)
                                      .ToDictionary(g => g.Key,
                                                    g => g.Count());
                var items = from pair in counts
                            orderby pair.Value descending
                            select pair;

                if (userList.Count > 0)
                {
                    foreach (KeyValuePair<string, int> pair in items)
                    {
                        var returnstring = "<tr role='row' class='odd'><td class='sorting_1'>" + pair.Key.ToString() + "</td><td>" + pair.Value.ToString() + "</td><td> </td><td> </td>><td> </td></tr>";
                        listy.Add(returnstring);
                        //json += "{ \"Name\" : \"" + pair.Key.ToString() + "\",\"Alarms\" : \"" + pair.Value.ToString() + "\"},";
                    }
                }
            }
            //<tr role="row" class="odd clickable-row clickable-row-links"><td><span class="circle-point-container"><span class="circle-point circle-point-orange"></span></span></td><td class="sorting_1">Gecko</td><td>Firefox 1.0</td><td>Win 98+ / OSX.2+</td><td>'++'</td><td><a href="#"><i class="fa fa-eye mr-1x"></i>View</a></td></tr>
            return listy;
        }
        [WebMethod]
        public static List<string> getLast15MinutesData(int id, string uname)
        {
            var listy = new List<string>();
            var sessions = new List<CustomEvent>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            if (userinfo != null)
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                if (userinfo.RoleId != (int)Role.SuperAdmin)
                {
                    //sessions = CustomEvent.GetAllCustomEventByDateAndManagerIdUNHandled(DateTime.Now, DateTime.Now.Subtract(new TimeSpan(1, 0, 0)), mangID, dbConnection);
                    var session = CustomEvent.GetAllCustomEventByDateAndManagerIdUNHandled(DateTime.Now, DateTime.Now.Subtract(new TimeSpan(1, 0, 0)), userinfo.ID, dbConnection);

                    foreach (var tem in session)
                    {
                        if ((DateTime.Now.Subtract(new TimeSpan(0, 15, 0)) < tem.RecevieTime) && (tem.RecevieTime < DateTime.Now))
                            sessions.Add(tem);
                    }
                }
                else
                {
                    var session = CustomEvent.GetAllCustomEventByDateUNHandled(DateTime.Now, DateTime.Now.Subtract(new TimeSpan(1, 0, 0)), dbConnection);
                    foreach (var tem in session)
                    {
                        if ((DateTime.Now.Subtract(new TimeSpan(0, 15, 0)) < tem.RecevieTime) && (tem.RecevieTime < DateTime.Now))
                            sessions.Add(tem);
                    }
                }

                foreach (var item in sessions)
                {
                    var imageclass = "circle-point-orange";
                    if (item.EventType != CustomEvent.EventTypes.MobileUserEngage)
                    {
                        var returnstring = "<tr role='row' class='odd'><td><span class='circle-point-container'><span class='circle-point " + imageclass + "'></span></span></td><td class='sorting_1'>" + item.StatusName + "</td><td>" + item.Name + "</td><td>" + item.RecevieTime.ToString() + "</td><td>" + item.UserName + "</td></tr>";
                        listy.Add(returnstring);
                    }
                }


            }
            return listy;
        }
        [WebMethod]
        public static List<string> getLast30MinutesData(int id, string uname)
        {
            var listy = new List<string>();
            var sessions = new List<CustomEvent>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            if (userinfo != null)
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                if (userinfo.RoleId != (int)Role.SuperAdmin)
                {
                    //sessions = CustomEvent.GetAllCustomEventByDateAndManagerIdUNHandled(DateTime.Now, DateTime.Now.Subtract(new TimeSpan(0, 30, 0, 0)), mangID, dbConnection);
                    var session = CustomEvent.GetAllCustomEventByDateAndManagerIdUNHandled(DateTime.Now, DateTime.Now.Subtract(new TimeSpan(0, 30, 0, 0)), userinfo.ID, dbConnection);
                    foreach (var tem in session)
                    {
                        if ((DateTime.Now.Subtract(new TimeSpan(0, 30, 0)) < tem.RecevieTime) && (tem.RecevieTime < DateTime.Now) && (DateTime.Now.Subtract(new TimeSpan(0, 15, 0)) > tem.RecevieTime))
                            sessions.Add(tem);

                    }
                }
                else
                {
                    //sessions = CustomEvent.GetAllCustomEventByDateUNHandled(DateTime.Now, DateTime.Now.Subtract(new TimeSpan(0, 30, 0)), dbConnection);
                    var session = CustomEvent.GetAllCustomEventByDateUNHandled(DateTime.Now, DateTime.Now.Subtract(new TimeSpan(1, 0, 0)), dbConnection);
                    foreach (var tem in session)
                    {
                        if ((DateTime.Now.Subtract(new TimeSpan(0, 30, 0)) < tem.RecevieTime) && (tem.RecevieTime < DateTime.Now) && (DateTime.Now.Subtract(new TimeSpan(0, 15, 0)) > tem.RecevieTime))
                            sessions.Add(tem);

                    }
                }

                foreach (var item in sessions)
                {
                    var imageclass = "circle-point-orange";
                    if (item.EventType != CustomEvent.EventTypes.MobileUserEngage)
                    {
                        var returnstring = "<tr role='row' class='odd'><td><span class='circle-point-container'><span class='circle-point " + imageclass + "'></span></span></td><td class='sorting_1'>" + item.StatusName + "</td><td>" + item.Name + "</td><td>" + item.RecevieTime.ToString() + "</td><td>" + item.UserName + "</td></tr>";
                        listy.Add(returnstring);
                    }
                }


            }
            return listy;
        }
        [WebMethod]
        public static List<string> getLastHourData(int id, string uname)
        {
            var listy = new List<string>();
            var sessions = new List<CustomEvent>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            if (userinfo != null)
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }

                if (userinfo.RoleId != (int)Role.SuperAdmin)
                {
                    sessions = CustomEvent.GetAllCustomEventByDateAndManagerIdUNHandled(DateTime.Now, DateTime.Now.Subtract(new TimeSpan(1, 0, 0)), userinfo.ID, dbConnection);
                }
                else
                {
                    //sessions = CustomEvent.GetAllCustomEventByDateUNHandled(DateTime.Now, DateTime.Now.Subtract(new TimeSpan(1, 0, 0)), dbConnection);
                    var session = CustomEvent.GetAllCustomEventByDateUNHandled(DateTime.Now, DateTime.Now.Subtract(new TimeSpan(1, 0, 0)), dbConnection);
                    foreach (var tem in session)
                    {
                        if ((DateTime.Now.Subtract(new TimeSpan(1, 0, 0)) < tem.RecevieTime) && (tem.RecevieTime < DateTime.Now) && (DateTime.Now.Subtract(new TimeSpan(0, 30, 0)) > tem.RecevieTime))
                            sessions.Add(tem);

                    }
                }

                foreach (var item in sessions)
                {
                    var imageclass = "circle-point-orange";
                    if (item.EventType != CustomEvent.EventTypes.MobileUserEngage)
                    {
                        var returnstring = "<tr role='row' class='odd'><td><span class='circle-point-container'><span class='circle-point " + imageclass + "'></span></span></td><td class='sorting_1'>" + item.StatusName + "</td><td>" + item.Name + "</td><td>" + item.RecevieTime.ToString() + "</td><td>" + item.UserName + "</td></tr>";
                        listy.Add(returnstring);
                    }
                }


            }
            return listy;
        }
        [WebMethod]
        public static List<string> getMobileHotData(int id, string uname)
        {
            var listy = new List<string>();
            var allcustomEvents = new List<CustomEvent>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);

            var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

            if (isValidSession == null)
            {
                listy.Add("LOGOUT");
                return listy;
            }

            if (userinfo.RoleId == (int)Role.SuperAdmin)
                allcustomEvents = CustomEvent.GetAllCustomEventByType((int)CustomEvent.EventTypes.MobileHotEvent, dbConnection);
            else
                allcustomEvents = CustomEvent.GetAllCustomEventByTypeAndManagerId((int)CustomEvent.EventTypes.MobileHotEvent, userinfo.ID, dbConnection);
            var imageclass = string.Empty;
            foreach (var item in allcustomEvents)
            {
                //if(item.Status == "")

                var getHotName = MobileHotEvent.GetMobileHotEventByGuid(item.Identifier, dbConnection);
                if (string.IsNullOrEmpty(getHotName.EventTypeName))
                    getHotName.EventTypeName = "None";

                item.Name = getHotName.EventTypeName;


                imageclass = "circle-point-orange";
                var returnstring = "<tr role='row' class='odd'><td><span class='circle-point-container'><span class='circle-point " + imageclass + "'></span></span></td><td class='sorting_1'>" + item.StatusName + "</td><td>" + item.Name + "</td><td>" + item.RecevieTime.ToString() + "</td><td>" + item.UserName + "</td></tr>";
                listy.Add(returnstring);
            }

            //<tr role="row" class="odd clickable-row clickable-row-links"><td><span class="circle-point-container"><span class="circle-point circle-point-orange"></span></span></td><td class="sorting_1">Gecko</td><td>Firefox 1.0</td><td>Win 98+ / OSX.2+</td><td>'++'</td><td><a href="#"><i class="fa fa-eye mr-1x"></i>View</a></td></tr>
            return listy;
        }
        [WebMethod]
        public static List<string> getHotData(int id, string uname)
        {
            var listy = new List<string>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);

            var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

            if (isValidSession == null)
            {
                listy.Add("LOGOUT");
                return listy;
            }

            var allcustomEvents = new List<CustomEvent>();
            if (userinfo.RoleId == (int)Role.SuperAdmin)
                allcustomEvents = CustomEvent.GetAllCustomEventByType((int)CustomEvent.EventTypes.HotEvent, dbConnection);
            else
                allcustomEvents = CustomEvent.GetAllCustomEventByTypeAndManagerId((int)CustomEvent.EventTypes.HotEvent, userinfo.ID, dbConnection);
            var imageclass = string.Empty;
            foreach (var item in allcustomEvents)
            {
                //if(item.Status == "")

                var hotEv = HotEvent.GetHotEventById(item.Identifier, dbConnection);

                if (hotEv != null)
                    item.UserName = CommonUtility.getPCName(hotEv);

                imageclass = "circle-point-orange";
                var returnstring = "<tr role='row' class='odd'><td><span class='circle-point-container'><span class='circle-point " + imageclass + "'></span></span></td><td class='sorting_1'>" + item.StatusName + "</td><td>" + item.Name + "</td><td>" + item.RecevieTime.ToString() + "</td><td>" + item.UserName + "</td></tr>";
                listy.Add(returnstring);
            }

            //<tr role="row" class="odd clickable-row clickable-row-links"><td><span class="circle-point-container"><span class="circle-point circle-point-orange"></span></span></td><td class="sorting_1">Gecko</td><td>Firefox 1.0</td><td>Win 98+ / OSX.2+</td><td>'++'</td><td><a href="#"><i class="fa fa-eye mr-1x"></i>View</a></td></tr>
            return listy;
        }
        //User Profile
        [WebMethod]
        public static int changePW(int id, string password)
        {

            var getuser = Users.GetUserById(id, dbConnection);
            getuser.Password = Encrypt.EncryptData(password, true, dbConnection);
            getuser.UpdatedDate = DateTime.Now;
            if (Users.InsertOrUpdateUsers(getuser, dbConnection))
            {
                return id;
            }
            else
                return 0;

            return id;
        }
        [WebMethod]
        public static int addUserProfile(int id, string username, string firstname, string lastname, string emailaddress, string phonenumber, string password, int devicetype, int supervisor, int role, string imgPath)
        {
            var userinfo = Users.GetUserById(id, dbConnection);
            if (userinfo.ID > 0)
            {
                var getuser = userinfo;
                getuser.FirstName = firstname;
                getuser.LastName = lastname;
                getuser.Email = emailaddress;

                if (getuser.RoleId != role)
                {
                    getuser.RoleId = role;
                    if (role == (int)Role.Manager)
                    {
                        var getMang = Users.GetAllFullManagersByUserId(getuser.ID, dbConnection);
                        if (getMang != null)
                            UserManager.DeleteManagersByUserIdAndMangId(getMang.ID, getuser.ID, dbConnection);
                    }
                    else if (role == (int)Role.Operator || role == (int)Role.UnassignedOperator)
                    {
                        var getdir = Accounts.GetDirectorByManagerName(getuser.Username, dbConnection);
                        if (getdir != null)
                            DirectorManager.DeleteManagersByDirectorIdAndMangName(getuser.Username, getdir.ID, dbConnection);
                    }
                    else
                    {
                        var getdir = Accounts.GetDirectorByManagerName(getuser.Username, dbConnection);
                        if (getdir != null)
                            DirectorManager.DeleteManagersByDirectorIdAndMangName(getuser.Username, getdir.ID, dbConnection);

                        var getMang = Users.GetAllFullManagersByUserId(getuser.ID, dbConnection);
                        if (getMang != null)
                            UserManager.DeleteManagersByUserIdAndMangId(getMang.ID, getuser.ID, dbConnection);
                    }
                }
                if (getuser.RoleId == (int)Role.Manager)
                {

                    var dirUser = Users.GetUserById(supervisor, dbConnection);
                    var getdir = Accounts.GetDirectorByManagerName(getuser.Username, dbConnection);

                    if (getdir != null)
                        DirectorManager.DeleteManagersByDirectorIdAndMangName(getuser.Username, getdir.ID, dbConnection);

                    if (dirUser != null)
                    {
                        if (!string.IsNullOrEmpty(dirUser.Username))
                        {
                            List<DirectorManager> userManagerList = new List<DirectorManager>() { new DirectorManager() 
                                            { 
                                                DirectorId = dirUser.ID, 
                                                ManagerId = getuser.ID,
                                                CreatedBy = dirUser.Username,
                                                CreatedDate = DateTime.Now,
                                                UpdatedBy = dirUser.Username,
                                                UpdatedDate = DateTime.Now,
                                                ManagerName = getuser.Username,
                                                ManagerAccountName = getuser.AccountName ,
                                                SiteId = dirUser.SiteId,
                                                CustomerId = userinfo.CustomerInfoId                           
                                            }};
                            DirectorManager.InsertDirectorManager(userManagerList, dbConnection);
                        }
                    }
                }
                else if (getuser.RoleId == (int)Role.Operator)
                {
                    if (supervisor > 0)
                    {
                        var manUser = Users.GetUserById(supervisor, dbConnection);
                        List<UserManager> userManagerList = new List<UserManager>() { new UserManager() { ManagerId = supervisor, UserId = getuser.ID, SiteId = manUser.SiteId, CustomerId = manUser.CustomerInfoId } };

                        UserManager.InsertUserManagers(userManagerList, dbConnection);
                    }
                }
                else if (getuser.RoleId == (int)Role.UnassignedOperator)
                {
                    var getMang = Users.GetAllFullManagersByUserId(getuser.ID, dbConnection);
                    if (getMang != null)
                        UserManager.DeleteManagersByUserIdAndMangId(getMang.ID, getuser.ID, dbConnection);
                }
                if (Users.InsertOrUpdateUsers(getuser, dbConnection))
                {
                    return userinfo.ID;
                }
                else
                    return 0;
            }
            return userinfo.ID;
        }

        [WebMethod]
        public static List<string> getUserProfileData(int id, string uname)
        {
            var listy = new List<string>();
            var customData = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(customData.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }
                var supervisorId = 0;
                if (customData != null)
                {
                    listy.Add(customData.Username);
                    listy.Add(customData.FirstName + " " + customData.LastName);
                    listy.Add(customData.Telephone);
                    listy.Add(customData.Email);
                    var geoLoc = ReverseGeocode.RetrieveFormatedAddress(customData.Latitude.ToString(), customData.Longitude.ToString(), getClientLic);
                    listy.Add(geoLoc);
                    listy.Add(CommonUtility.getUserRoleName(customData.RoleId));
                    if (customData.RoleId == (int)Role.Operator)
                    {
                        var usermanager = Users.GetAllFullManagersByUserId(customData.ID, dbConnection);
                        if (usermanager != null)
                        {
                            listy.Add(usermanager.CustomerUName);
                            supervisorId = usermanager.ID;
                        }
                        else
                            listy.Add("Unassigned");
                    }
                    else if (customData.RoleId == (int)Role.Manager)
                    {
                        var getdir = Accounts.GetDirectorByManagerName(customData.Username, dbConnection);
                        if (getdir != null)
                        {
                            listy.Add(getdir.CustomerUName);
                            supervisorId = getdir.ID;
                        }
                        else
                            listy.Add("Unassigned");
                    }
                    else if (customData.RoleId == (int)Role.UnassignedOperator)
                    {
                        listy.Add("Unassigned");
                    }
                    else
                    {
                        listy.Add(" ");
                    }
                    listy.Add("Group Name");
                    listy.Add(customData.Status);
                    listy.Add("circle-point " + CommonUtility.getImgUserStatus(customData.Status));
                    var imgSrc = CommonUtility.getUserPhotoUrl(customData.ID, dbConnection);
                    //var userImg = UserImage.GetUserImageByUserId(customData.ID, dbConnection);
                    //var imgSrc = "../images/icon-user-default.png";//images / custom - images / user - 1.png;
                    //if (userImg != null)
                    //{
                    //    var base64 = Convert.ToBase64String(userImg.ImageFile);
                    //    imgSrc = String.Format("data:image/png;base64,{0}", base64);
                    //}
                    var fontstyle = string.Empty;
                    if (customData.Active == (int)Arrowlabs.Business.Layer.HealthCheck.DeviceStatus.Online)
                    {
                        fontstyle = "style='color:lime;'";
                    }
                    //   var pushDev = PushNotificationDevice.GetPushNotificationDeviceByUsername(customData.Username, dbConnection);
                    var monitor = string.Empty;
                    //if (customData.RoleId == (int)Role.Admin || customData.RoleId == (int)Role.Manager || customData.RoleId == (int)Role.Director || customData.RoleId == (int)Role.Regional || customData.RoleId == (int)Role.ChiefOfficer)
                    if (customData.RoleId != (int)Role.SuperAdmin)
                    {
                        //   if (pushDev != null)
                        //   {
                        //     if (!string.IsNullOrEmpty(pushDev.Username))
                        //      {
                        if (customData.IsServerPortal)
                        {
                            monitor = "<i " + fontstyle + " class='fa fa-laptop fa-2x mr-2x'></i>";
                            fontstyle = "";
                        }
                        else
                        {
                            monitor = "<i class='fa fa-laptop fa-2x mr-2x'></i>";
                        }
                        //    }
                        //     else
                        //    {
                        //        monitor = "<i " + fontstyle + " class='fa fa-laptop fa-2x mr-2x'></i>";
                        //        fontstyle = "";
                        //    }
                        //}
                    }
                    listy.Add(imgSrc);
                    listy.Add(CommonUtility.getUserDeviceType(customData.DeviceType, fontstyle, monitor));
                    listy.Add(CommonUtility.getRoleSupervisor(customData.RoleId));
                    listy.Add(customData.FirstName);
                    listy.Add(customData.LastName);
                    listy.Add(supervisorId.ToString());
                    listy.Add(Decrypt.DecryptData(customData.Password, true, dbConnection));
                    listy.Add(customData.Latitude.ToString());
                    listy.Add(customData.Longitude.ToString());

                    var userSiteDisplay = customData == null ? "N/A" : customData.SiteName;
                    if (customData.RoleId != (int)Role.Regional)
                    {
                        if (customData.SiteId == 0)
                            listy.Add("N/A");
                        else
                            listy.Add(userSiteDisplay);
                    }
                    else
                    {
                        listy.Add("Multiple");
                    }



                    listy.Add(customData.Gender);
                    listy.Add(customData.EmployeeID);
                }
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("Reports", "getUserProfileData", er, dbConnection, customData.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static string CreatePDF(string report, string type, string toDate, string fromDate, string searchStatus, string uname, string searchIncidentStatus)
        {
            var allcustomEvents = new List<CustomEvent>();
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                try
                {
                    var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                    if (isValidSession == null)
                    {
                        return "LOGOUT";
                    }

                    var ttype = 0;
                    if (report == "HotEvent")
                        ttype = (int)CustomEvent.EventTypes.HotEvent;
                    else if (report == "MobileHotevent")
                        ttype = (int)CustomEvent.EventTypes.MobileHotEvent;

                    if (report == "Duty Roster")
                    {
                        return GenerateDutyRoster(Convert.ToDateTime(fromDate), Convert.ToDateTime(fromDate).AddDays(7), report, userinfo);
                    }
                    else if (report == "Users")
                    {
                        return GenerateUsers(Convert.ToDateTime(fromDate), Convert.ToDateTime(fromDate).AddDays(7), report, userinfo);
                    }
                    else if (report == "Lost and Found" || report == "Asset")
                    {
                        //var itemsLostFound = ItemFound.GetAllItemFoundByDateRange(Convert.ToDateTime(fromDate).ToString(), Convert.ToDateTime(toDate).AddHours(1).ToString(), dbConnection);

                        //if (userinfo.RoleId != (int)Role.SuperAdmin && userinfo.RoleId != (int)Role.ChiefOfficer)
                        //{
                        //    if (userinfo.RoleId == (int)Role.Manager || userinfo.RoleId == (int)Role.Director || userinfo.RoleId == (int)Role.Admin)
                        //        itemsLostFound = itemsLostFound.Where(i => i.SiteId == userinfo.SiteId).ToList();
                        //    else
                        //    {
                        //        var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                        //        var tempItemstLostFound = new List<ItemFound>();
                        //        foreach (var site in sites)
                        //            tempItemstLostFound.AddRange(itemsLostFound.Where(i => i.SiteId == site.SiteId).ToList());

                        //        itemsLostFound = tempItemstLostFound;
                        //    }
                        //}

                        //var grouped = itemsLostFound.GroupBy(item => item.Id);
                        //itemsLostFound = grouped.Select(grp => grp.OrderBy(item => item.Id).First()).ToList();
                        //itemsLostFound = itemsLostFound.OrderBy(i => i.Status).ToList();
                        //return GeneratePDFItemLost(itemsLostFound, Convert.ToDateTime(fromDate), Convert.ToDateTime(toDate).AddDays(1), report, userinfo, searchStatus);
                        var isKey = false;
                        if (report == "Asset")
                            isKey = true;

                        var itemsLostFound = new List<ItemLost>();


                        if (userinfo.RoleId != (int)Role.SuperAdmin && userinfo.RoleId != (int)Role.ChiefOfficer)
                        {
                            if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                                itemsLostFound = ItemLost.GetAllItemLostByCustomerId(userinfo.CustomerInfoId, dbConnection);
                            else
                                itemsLostFound = ItemLost.GetAllItemLostBySite(userinfo.SiteId, dbConnection);
                        }
                        else
                        {
                            itemsLostFound = ItemLost.GetAllItemLost(dbConnection);
                        }
                        itemsLostFound = itemsLostFound.Where(i => i.isKey == isKey).ToList();
                        return GeneratePDFWareHouse(itemsLostFound, Convert.ToDateTime(fromDate), Convert.ToDateTime(toDate).AddDays(1), report, userinfo, searchStatus, isKey);

                    }
                    else if (report != "Logs")
                    {
                        if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                            allcustomEvents = CustomEvent.GetAllCustomEvents(dbConnection);//allcustomEvents = CustomEvent.GetAllCustomEventByType(ttype, dbConnection);
                        else if (userinfo.RoleId == (int)Role.Director)
                            allcustomEvents = CustomEvent.GetAllCustomEventsBySiteId(userinfo.SiteId, dbConnection);//allcustomEvents = CustomEvent.GetAllCustomEventByTypeBySiteId(ttype, userinfo.SiteId, dbConnection);
                        else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                            allcustomEvents = CustomEvent.GetAllCustomEventByCId(userinfo.CustomerInfoId, dbConnection);//allcustomEvents = CustomEvent.GetAllCustomEventByTypeAndCId(ttype, userinfo.CustomerInfoId, dbConnection);
                        else if (userinfo.RoleId == (int)Role.Admin || userinfo.RoleId == (int)Role.Manager)
                            allcustomEvents = CustomEvent.GetAllCustomEventsByManagerId(userinfo.ID, dbConnection);// allcustomEvents = CustomEvent.GetAllCustomEventByTypeAndManagerId(ttype, userinfo.ID, dbConnection);
                        else if (userinfo.RoleId == (int)Role.Regional)
                        {
                            //var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                            //foreach (var site in sites)
                            //{
                            allcustomEvents.AddRange(CustomEvent.GetAllCustomEventsByLevel5(userinfo.ID, dbConnection));
                            //}
                        }

                        return GeneratePDFforDeviceApplicationHistory(allcustomEvents, Convert.ToDateTime(fromDate), Convert.ToDateTime(toDate).AddDays(1), report, userinfo, searchStatus, searchIncidentStatus);
                    }
                    else
                    {
                        var vamimlogs = MIMSLog.GetAllMIMSLog(Convert.ToDateTime(fromDate), Convert.ToDateTime(toDate).AddDays(1), dbConnection);

                        List<MIMSLog> mimlogs = vamimlogs.OrderByDescending(o => o.CurrentDateTime).ToList();

                        return GeneratePDFLog(mimlogs, Convert.ToDateTime(fromDate), Convert.ToDateTime(toDate).AddDays(1), report, userinfo);
                    }

                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("Reports", "CreatePDF", ex, dbConnection, userinfo.SiteId);
                    return "No entries found!";
                }
            }
        }

        protected string GetIPAddress()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }

            return context.Request.ServerVariables["REMOTE_ADDR"];
        }
        protected void LogoutButton_Click(object sender, EventArgs e)
        {
            var customData = Users.GetUserByName(User.Identity.Name, dbConnection);
            CommonUtility.LogoutUser(customData, System.Web.HttpContext.Current.Session.SessionID, GetIPAddress());
            System.Web.Security.FormsAuthentication.SignOut();
            Response.Redirect("~/Default.aspx");
        }
        protected void forceLogoutButton_Click(object sender, EventArgs e)
        {
            System.Web.Security.FormsAuthentication.SignOut();
            Response.Redirect("~/Default.aspx");
        }

        private static string GeneratePDFWareHouse(List<ItemLost> loghistories, DateTime fromdate, DateTime todate, string reportName, Users userinfo, string searchT, bool iskey)
        {
            string fileName = string.Empty;
            var foundEntry = false;
            try
            {
                if (loghistories.Count != 0)
                {

                    iTextSharp.text.Document doc = new iTextSharp.text.Document(PageSize.LETTER, 25F, 25F, 50F, 25F);

                    doc.SetMargins(25, 25, 65, 35);

                    fileName = DateTime.Now.Day.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Year.ToString() + "-" + DateTime.Now.Hour.ToString() + "-" + DateTime.Now.Minute.ToString() + "-" + DateTime.Now.Second.ToString() + ".pdf";
                    var path = fpath + fileName;

                    PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(path, FileMode.Create));
                    writer.PageEvent = new ITextEvents()
                    {
                        cid = userinfo.CustomerInfoId
                    };
                    doc.Open();


                    BaseFont f_cn = BaseFont.CreateFont(Environment.GetFolderPath(Environment.SpecialFolder.Fonts) + "\\verdana.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    //BaseFont h_cn = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    BaseFont z_cn = BaseFont.CreateFont(BaseFont.ZAPFDINGBATS, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    Font times = new Font(f_cn, 12, Font.NORMAL, Color.BLACK);
                    Font btimes = new Font(f_cn, 12, Font.BOLD, Color.BLACK);

                    iTextSharp.text.Color arrowred = new iTextSharp.text.Color(162, 0, 46);

                    Font eleventimes = new Font(f_cn, 11, Font.NORMAL, Color.BLACK);
                    Font tentimes = new Font(f_cn, 10, Font.NORMAL, Color.BLACK);

                    Font weleventimes = new Font(f_cn, 11, Font.NORMAL, Color.WHITE);

                    Font wnine = new Font(f_cn, 9, Font.NORMAL, Color.WHITE);

                    Font gweb = new Font(z_cn, 11, Font.NORMAL, Color.GREEN);
                    Font rweb = new Font(z_cn, 11, Font.NORMAL, Color.RED);

                    Font ueleventimes = new Font(f_cn, 11, Font.UNDERLINE, Color.BLACK);

                    Font beleventimes = new Font(f_cn, 11, Font.BOLD, Color.BLACK);
                    Font bredeleventimes = new Font(f_cn, 11, Font.BOLD, Color.RED);
                    Font redeleventimes = new Font(f_cn, 11, Font.NORMAL, Color.RED);
                    Font twelvetimes = new Font(f_cn, 12, Font.ITALIC, Color.BLACK);

                    Font btentimes = new Font(f_cn, 11, Font.NORMAL, Color.BLACK);

                    Font headerf = new Font(f_cn, 9, Font.NORMAL, Color.BLACK);

                    Font mbtimes = new Font(f_cn, 18, Font.BOLD, arrowred);
                    Font cbtimes = new Font(f_cn, 13, Font.BOLD, arrowred);

                    float[] columnWidths1585 = new float[] { 15, 85 };
                    float[] columnWidths2080 = new float[] { 20, 80 };

                    float[] columnWidths1783 = new float[] { 18, 82 };

                    float[] columnWidths2575 = new float[] { 25, 75 };
                    float[] columnWidths3070 = new float[] { 30, 70 };
                    float[] columnWidths3565 = new float[] { 35, 65 };
                    float[] columnWidths4555 = new float[] { 45, 55 };
                    float[] columnWidthsH = new float[] { 35, 60, 15 };

                    if (string.IsNullOrEmpty(userinfo.SiteName))
                        userinfo.SiteName = "N/A";

                    var mainheadertable = new PdfPTable(3);
                    mainheadertable.DefaultCell.Border = Rectangle.NO_BORDER;
                    mainheadertable.WidthPercentage = 100;
                    mainheadertable.SetWidths(columnWidthsH);
                    var headerMain = new PdfPCell();
                    if (userinfo.CustomerInfoId == 87 || userinfo.CustomerInfoId == 45)
                    {
                        headerMain.AddElement(new Phrase("G4S ASSET REPORT", mbtimes));
                    }
                    else
                    {
                        headerMain.AddElement(new Phrase("MIMS ASSET REPORT", mbtimes));
                    }
                    headerMain.PaddingBottom = 10;
                    headerMain.HorizontalAlignment = 1;
                    headerMain.Border = Rectangle.NO_BORDER;
                    PdfPCell pdfCell1 = new PdfPCell();
                    pdfCell1.Border = Rectangle.NO_BORDER;
                    PdfPCell pdfCell2 = new PdfPCell();
                    pdfCell2.Border = Rectangle.NO_BORDER;
                    mainheadertable.AddCell(pdfCell1);
                    mainheadertable.AddCell(headerMain);
                    mainheadertable.AddCell(pdfCell2);

                    doc.Add(mainheadertable);

                    var headertable = new PdfPTable(2);
                    headertable.DefaultCell.Border = Rectangle.NO_BORDER;
                    headertable.WidthPercentage = 100;
                    headertable.SetWidths(columnWidths1783);

                    var header1 = new PdfPCell();
                    header1.AddElement(new Phrase("Report Type:", beleventimes));
                    header1.Border = Rectangle.NO_BORDER;
                    headertable.AddCell(header1);

                    var header1a = new PdfPCell();
                    header1a.AddElement(new Phrase(searchT, eleventimes));
                    header1a.Border = Rectangle.NO_BORDER;
                    headertable.AddCell(header1a);

                    var header3 = new PdfPCell();
                    header3.AddElement(new Phrase("Period:", beleventimes));
                    header3.Border = Rectangle.NO_BORDER;
                    headertable.AddCell(header3);

                    var header3a = new PdfPCell();
                    header3a.AddElement(new Phrase(fromdate.ToShortDateString() + " to " + todate.ToShortDateString(), eleventimes));
                    header3a.Border = Rectangle.NO_BORDER;
                    headertable.AddCell(header3a);

                    //var header2 = new PdfPCell();
                    //header2.AddElement(new Phrase("Created By:", beleventimes));
                    //header2.Border = Rectangle.NO_BORDER;

                    //headertable.AddCell(header2);

                    //var header2a = new PdfPCell();
                    //header2a.AddElement(new Phrase(userinfo.CustomerUName, eleventimes));
                    //header2a.Border = Rectangle.NO_BORDER;
                    //headertable.AddCell(header2a);

                    //var header5 = new PdfPCell();
                    //header5.AddElement(new Phrase("Created Date:", beleventimes));
                    //header5.Border = Rectangle.NO_BORDER;
                    //header5.PaddingBottom = 20;
                    //headertable.AddCell(header5);

                    //var header5a = new PdfPCell();
                    //header5a.AddElement(new Phrase(CommonUtility.getDTNow().AddHours(userinfo.TimeZone).ToString(), eleventimes));
                    //header5a.Border = Rectangle.NO_BORDER;
                    //headertable.AddCell(header5a);

                    doc.Add(headertable);

                    iTextSharp.text.Paragraph paragraphTable = new iTextSharp.text.Paragraph();
                    paragraphTable.SpacingBefore = 150f;

                    var checklistHeaderTable = new PdfPTable(1);
                    checklistHeaderTable.DefaultCell.Border = Rectangle.NO_BORDER;
                    checklistHeaderTable.WidthPercentage = 100;



                    PdfPTable table = new PdfPTable(6);
                    table.WidthPercentage = 100;

                    float[] columnWidthTable = new float[] { 15, 20, 15, 20, 15, 15 };

                    table.SetWidths(columnWidthTable);

                    var cheader1 = new PdfPCell();
                    cheader1.AddElement(new Phrase(searchT, cbtimes));
                    cheader1.Border = Rectangle.NO_BORDER;
                    cheader1.PaddingTop = 10;
                    cheader1.PaddingBottom = 10;
                    cheader1.Colspan = 6;
                    table.AddCell(cheader1);

                    var ncaheader2 = new PdfPCell();
                    ncaheader2.AddElement(new Phrase("Name", weleventimes));
                    ncaheader2.Border = Rectangle.NO_BORDER;
                    ncaheader2.BackgroundColor = Color.GRAY;
                    table.AddCell(ncaheader2);

                    var ncaheader3 = new PdfPCell();
                    ncaheader3.AddElement(new Phrase("Category", weleventimes));
                    ncaheader3.Border = Rectangle.NO_BORDER;
                    ncaheader3.BackgroundColor = Color.GRAY;
                    table.AddCell(ncaheader3);

                    var ncaheader4 = new PdfPCell();
                    ncaheader4.AddElement(new Phrase("Location", weleventimes));
                    ncaheader4.Border = Rectangle.NO_BORDER;
                    ncaheader4.BackgroundColor = Color.GRAY;
                    table.AddCell(ncaheader4);

                    var ncaheader5 = new PdfPCell();
                    ncaheader5.AddElement(new Phrase("Updated Date", weleventimes));
                    ncaheader5.Border = Rectangle.NO_BORDER;
                    ncaheader5.BackgroundColor = Color.GRAY;
                    table.AddCell(ncaheader5);

                    var ncaheader6 = new PdfPCell();
                    ncaheader6.AddElement(new Phrase("User", weleventimes));
                    ncaheader6.Border = Rectangle.NO_BORDER;
                    ncaheader6.BackgroundColor = Color.GRAY;
                    table.AddCell(ncaheader6);

                    var ncaheader1 = new PdfPCell();
                    ncaheader1.AddElement(new Phrase("Status", weleventimes));
                    ncaheader1.Border = Rectangle.NO_BORDER;
                    ncaheader1.PaddingLeft = 5;
                    ncaheader1.PaddingBottom = 5;
                    ncaheader1.BackgroundColor = Color.GRAY;

                    table.AddCell(ncaheader1);

                    if (searchT != "All")
                    {
                        if (searchT == "Checked Out")
                        {
                            loghistories = loghistories.Where(i => i.Lost == 1).ToList();
                        }
                        else
                        {
                            loghistories = loghistories.Where(i => i.Lost == 0).ToList();
                        }
                    }


                    var odd = false;
                    Font eighttimes = new Font(f_cn, 8, Font.NORMAL, Color.BLACK);
                    foreach (var device in loghistories)
                    {

                        if (device.CreatedDate.Value.Ticks > fromdate.Ticks && device.CreatedDate.Value.Ticks < todate.Ticks)
                        {
                            foundEntry = true;

                            var xheaderCell = new PdfPCell(new Phrase(device.Name, eighttimes));

                            var assetCat = Asset.GetAssetById(device.AssetCatId, dbConnection);
                            if (assetCat != null)
                                assetCat.AssetCategoryName = assetCat.AssetCategoryName + "-" + assetCat.Name;
                            else
                                assetCat.AssetCategoryName = "N/A";

                            var xheaderCell2 = new PdfPCell(new Phrase(assetCat.AssetCategoryName, eighttimes));
                            var xheaderCell3 = new PdfPCell(new Phrase(device.LocationDesc, eighttimes));
                            var xheaderCell4 = new PdfPCell(new Phrase(device.UpdatedDate.Value.AddHours(userinfo.TimeZone).ToString(), eighttimes));

                            if (string.IsNullOrEmpty(device.OwnerName))
                            {
                                var oUser = Users.GetUserByName(device.UpdatedBy, dbConnection);
                                if (oUser != null)
                                    device.OwnerName = oUser.FirstName + " "+oUser.LastName;
                                else
                                    device.OwnerName = device.UpdatedBy;
                            }
                            else
                            {
                                var oUser = Users.GetUserByName(device.OwnerName, dbConnection);
                                if (oUser != null)
                                    device.OwnerName = oUser.FirstName + " " + oUser.LastName;
                            }
                            var cl = "Checked In";
                            if (device.Lost == 1)
                                cl = "Checked Out";

                            var xheaderCell5 = new PdfPCell(new Phrase(device.OwnerName, eighttimes));
                            var xheaderCell6 = new PdfPCell(new Phrase(cl, eighttimes));
                            if (!odd)
                            {
                                xheaderCell.BackgroundColor = Color.LIGHT_GRAY;
                                xheaderCell2.BackgroundColor = Color.LIGHT_GRAY;
                                xheaderCell3.BackgroundColor = Color.LIGHT_GRAY;
                                xheaderCell4.BackgroundColor = Color.LIGHT_GRAY;
                                xheaderCell5.BackgroundColor = Color.LIGHT_GRAY;
                                xheaderCell6.BackgroundColor = Color.LIGHT_GRAY;
                                odd = true;
                            }
                            else
                            {
                                odd = false;
                            }

                            xheaderCell.Border = Rectangle.NO_BORDER;
                            xheaderCell2.Border = Rectangle.NO_BORDER;
                            xheaderCell3.Border = Rectangle.NO_BORDER;
                            xheaderCell4.Border = Rectangle.NO_BORDER;
                            xheaderCell5.Border = Rectangle.NO_BORDER;
                            xheaderCell6.Border = Rectangle.NO_BORDER;

                            table.AddCell(xheaderCell);
                            table.AddCell(xheaderCell2);
                            table.AddCell(xheaderCell3);
                            table.AddCell(xheaderCell4);
                            table.AddCell(xheaderCell5);
                            table.AddCell(xheaderCell6);


                            //table.AddCell(device.Name);





                        }
                    }
                    checklistHeaderTable.AddCell(table);
                    doc.Add(checklistHeaderTable);

                    Font geleventimes = new Font(f_cn, 11, Font.NORMAL, Color.GRAY);
                    var endTable = new PdfPTable(1);
                    endTable.DefaultCell.Border = Rectangle.NO_BORDER;
                    endTable.WidthPercentage = 100;

                    var endheader = new PdfPCell();
                    endheader.AddElement(new Phrase("Report generated: " + CommonUtility.getDTNow().AddHours(userinfo.TimeZone).ToString() + " 	by: " + userinfo.Username, geleventimes));
                    endheader.Border = Rectangle.NO_BORDER;
                    endTable.AddCell(endheader);
                    doc.Add(endTable);

                    writer.Flush();
                    doc.Close();
                    //  DownloadFile(fileName);


                    if (!foundEntry)
                        return "No entries found!";
                    else
                    {

                        //var vpath = mimssettings.MIMSMobileAddress + "//Uploads//PDFReports//" + fileName;
                        //return vpath;
                        if (File.Exists(path))
                        {
                            using (StreamReader sr = new StreamReader(path))
                            {
                                var vpath = CommonUtility.CloudUploadFile(userinfo.CustomerInfoId, fileName, sr.BaseStream);
                                //var vpath = mimssettings.MIMSMobileAddress + "//Uploads//PDFReports//" + fileName;
                                return vpath;
                            }
                        }
                        return "Failed to generate report!";
                    }
                }
            }

            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("Reports", "GeneratePDFLog", exp, dbConnection, userinfo.SiteId);
                return "No entries found!";
            }
            return "No entries found!";
        }

        private static string GeneratePDFItemLost(List<ItemFound> loghistories, DateTime fromdate, DateTime todate, string reportName, Users userinfo, string status)
        {
            string fileName = string.Empty;
            var foundEntry = false;
            try
            {
                if (loghistories.Count != 0)
                {
                    var mimssettings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);

                    //string appPath = PhysicalPath("~/PDFReports");

                    var preappPath = mimssettings.FilePath.Substring(0, mimssettings.FilePath.Length - 5);

                    string appPath = preappPath + "\\PDFReports";
                    string path = appPath;
                    /*Logo Path*/
                    string strLogoPath = CommonUtility.PhysicalPath("~/Images/custom-images") + "\\site-login-logo.png";
                    string strLogoPath2 = CommonUtility.PhysicalPath("~/Images/custom-images") + "\\site-login-logo.png";

                    iTextSharp.text.Document doc = new iTextSharp.text.Document(PageSize.LETTER, 25F, 25F, 50F, 25F);

                    doc.SetMargins(25, 25, 65, 35);

                    fileName = DateTime.Now.Day.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Year.ToString() + "-" + DateTime.Now.Hour.ToString() + "-" + DateTime.Now.Minute.ToString() + "-" + DateTime.Now.Second.ToString() + ".pdf";
                    path = fpath + fileName;

                    PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(path, FileMode.Create));
                    writer.PageEvent = new ITextEvents()
                    {
                        cid = userinfo.CustomerInfoId
                    };
                    doc.Open();

                    iTextSharp.text.Image img1 = iTextSharp.text.Image.GetInstance(strLogoPath2);
                    img1.SetAbsolutePosition(doc.GetLeft(15), doc.GetTop(-15));
                    img1.ScalePercent(50f);
                    img1.Alignment = iTextSharp.text.Image.ALIGN_LEFT;
                    doc.Add(img1);

                    BaseFont f_cn = BaseFont.CreateFont(Environment.GetFolderPath(Environment.SpecialFolder.Fonts) + "\\verdana.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

                    PdfContentByte cb = writer.DirectContent;
                    cb.BeginText();
                    cb.SetFontAndSize(f_cn, 14);
                    if (userinfo.CustomerInfoId == 87 || userinfo.CustomerInfoId == 45)
                    {
                        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "G4S GENERATED REPORT", 320, 700, 0);
                    }
                    else
                    {
                        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "MIMS GENERATED REPORT", 320, 700, 0);
                    }
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Created Date: " + DateTime.Now.ToString(), 40, 670, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Report Name:  " + reportName, 40, 655, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Generated By: " + userinfo.FirstName + " "+userinfo.LastName, 40, 640, 0);

                    if (fromdate > DateTime.MinValue & todate > DateTime.MinValue)
                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Period: " + fromdate.ToShortDateString() + " To " + todate.Subtract(new TimeSpan(1, 0, 0, 0)).ToShortDateString(), 40, 625, 0);
                    else
                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Period: All", 40, 625, 0);

                    //cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Licence Key: jorpsdjordan public security directorate9/7/2014 04:49:19 pm", 40, 610, 0);

                    cb.EndText();
                    iTextSharp.text.Paragraph paragraphTable = new iTextSharp.text.Paragraph();
                    paragraphTable.SpacingBefore = 150f;
                    PdfPTable table = new PdfPTable(8);
                    table.WidthPercentage = 100;

                    table.AddCell("Status");
                    table.AddCell("Ref#");
                    table.AddCell("Date");
                    table.AddCell("Type");
                    table.AddCell("Brand");
                    table.AddCell("Storage");
                    table.AddCell("Shelf Life");
                    table.AddCell("Username");


                    if (status != "All")
                    {
                        loghistories = loghistories.Where(i => i.Status == status).ToList();
                    }

                    foreach (var device in loghistories)
                    {

                        //if (device.RecevieTime.Value.Ticks > fromdate.Ticks && device.RecevieTime.Value.Ticks < todate.Ticks)
                        {
                            // targetDt is in between d1 and d2
                            var dt = device.DateFound.ToString();
                            var uname = device.FinderName;
                            if (device.Status == "Return")
                            {
                                var Rinfo = ItemOwner.GetItemOwnerByReference(device.Reference, dbConnection);
                                if (Rinfo != null)
                                {
                                    dt = Rinfo.CreatedDate.ToString();
                                    uname = Rinfo.Name;
                                }

                            }
                            else if (device.Status == "Dispose" || device.Status == "Donate")
                            {
                                var Rinfo = ItemDispose.GetItemDisposeByReference(device.Reference, dbConnection);
                                if (Rinfo != null)
                                {
                                    dt = Rinfo.CreatedDate.ToString();
                                    uname = Rinfo.RecipientName;
                                }
                            }
                            foundEntry = true;
                            table.AddCell(device.Status);
                            table.AddCell(device.Reference);
                            table.AddCell(dt);
                            table.AddCell(device.Type + "-" + device.SubType);
                            table.AddCell(device.Brand);
                            table.AddCell(device.StorageLocation + "-" + device.SubStorageLocation);
                            table.AddCell(device.ShelfLife);
                            table.AddCell(uname);
                            //table.AddCell(device.Exception);

                            //table.AddCell(device.CurrentDateTime.ToString());

                            //table.AddCell(device.MacAddress);

                        }
                    }

                    doc.Add(paragraphTable);
                    doc.Add(table);

                    writer.Flush();
                    doc.Close();
                    //  DownloadFile(fileName);


                    if (!foundEntry)
                        return "No entries found!";
                    else
                    {

                        //var vpath = mimssettings.MIMSMobileAddress + "//Uploads//PDFReports//" + fileName;
                        //return vpath;
                        if (File.Exists(path))
                        {
                            using (StreamReader sr = new StreamReader(path))
                            {
                                var vpath = CommonUtility.CloudUploadFile(userinfo.CustomerInfoId, fileName, sr.BaseStream);
                                //var vpath = mimssettings.MIMSMobileAddress + "//Uploads//PDFReports//" + fileName;
                                return vpath;
                            }
                        }
                        return "Failed to generate report!";
                    }
                }
            }

            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("Reports", "GeneratePDFLog", exp, dbConnection, userinfo.SiteId);
                return "No entries found!";
            }
            return "No entries found!";
        }
        private static string GeneratePDFforDeviceApplicationHistory(List<CustomEvent> loghistories, DateTime fromdate, DateTime todate, string reportName, Users userinfo, string searchStatus, string searchIncidentStatus)
        {
            string fileName = string.Empty;
            var foundEntry = false;
            try
            {
                if (loghistories.Count != 0)
                {
                    /*Logo Path*/
                    var isMob = true;
                    var isReq = true;
                    var isSys = true;
                    var x = searchStatus;
                    if (searchStatus != "All")
                    {
                        if (searchStatus == "System Created")
                        {
                            x = "Incident";
                            isMob = false;
                            isReq = false;
                        }
                        else if (searchStatus == "MobileHotEvent")
                        {
                            isReq = false;
                            isSys = false;
                        }
                        else if (searchStatus == "Request")
                        {
                            isMob = false;
                            isSys = false;
                        }
                        loghistories = loghistories.Where(i => i.EventType.ToString() == x).ToList();
                    }
                    if (searchIncidentStatus != "All")
                    {
                        loghistories = loghistories.Where(i => i.StatusName == searchIncidentStatus).ToList();
                    }


                    iTextSharp.text.Document doc = new iTextSharp.text.Document(PageSize.LETTER, 25F, 25F, 50F, 25F);

                    doc.SetMargins(25, 25, 65, 35);

                    fileName = DateTime.Now.Day.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Year.ToString() + "-" + DateTime.Now.Hour.ToString() + "-" + DateTime.Now.Minute.ToString() + "-" + DateTime.Now.Second.ToString() + ".pdf";
                    var path = fpath + fileName;

                    PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(path, FileMode.Create));
                    writer.PageEvent = new ITextEvents()
                    {
                        cid = userinfo.CustomerInfoId
                    };
                    doc.Open();

                    BaseFont f_cn = BaseFont.CreateFont(Environment.GetFolderPath(Environment.SpecialFolder.Fonts) + "\\verdana.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    //BaseFont h_cn = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    BaseFont z_cn = BaseFont.CreateFont(BaseFont.ZAPFDINGBATS, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    Font times = new Font(f_cn, 12, Font.NORMAL, Color.BLACK);
                    Font btimes = new Font(f_cn, 12, Font.BOLD, Color.BLACK);

                    iTextSharp.text.Color arrowred = new iTextSharp.text.Color(162, 0, 46);

                    Font eleventimes = new Font(f_cn, 11, Font.NORMAL, Color.BLACK);
                    Font tentimes = new Font(f_cn, 10, Font.NORMAL, Color.BLACK);

                    Font weleventimes = new Font(f_cn, 11, Font.NORMAL, Color.WHITE);

                    Font wnine = new Font(f_cn, 9, Font.NORMAL, Color.WHITE);

                    Font gweb = new Font(z_cn, 11, Font.NORMAL, Color.GREEN);
                    Font rweb = new Font(z_cn, 11, Font.NORMAL, Color.RED);

                    Font ueleventimes = new Font(f_cn, 11, Font.UNDERLINE, Color.BLACK);

                    Font beleventimes = new Font(f_cn, 11, Font.BOLD, Color.BLACK);
                    Font bredeleventimes = new Font(f_cn, 11, Font.BOLD, Color.RED);
                    Font redeleventimes = new Font(f_cn, 11, Font.NORMAL, Color.RED);
                    Font twelvetimes = new Font(f_cn, 12, Font.ITALIC, Color.BLACK);

                    Font btentimes = new Font(f_cn, 11, Font.NORMAL, Color.BLACK);

                    Font headerf = new Font(f_cn, 9, Font.NORMAL, Color.BLACK);

                    Font mbtimes = new Font(f_cn, 18, Font.BOLD, arrowred);
                    Font cbtimes = new Font(f_cn, 13, Font.BOLD, arrowred);

                    float[] columnWidths1585 = new float[] { 15, 85 };
                    float[] columnWidths2080 = new float[] { 20, 80 };

                    float[] columnWidths1783 = new float[] { 18, 82 };

                    float[] columnWidths2575 = new float[] { 25, 75 };
                    float[] columnWidths3070 = new float[] { 30, 70 };
                    float[] columnWidths3565 = new float[] { 35, 65 };
                    float[] columnWidths4555 = new float[] { 45, 55 };
                    float[] columnWidthsH = new float[] { 35, 60, 15 };

                    if (string.IsNullOrEmpty(userinfo.SiteName))
                        userinfo.SiteName = "N/A";

                    var mainheadertable = new PdfPTable(3);
                    mainheadertable.DefaultCell.Border = Rectangle.NO_BORDER;
                    mainheadertable.WidthPercentage = 100;
                    mainheadertable.SetWidths(columnWidthsH);
                    var headerMain = new PdfPCell();
                    if (userinfo.CustomerInfoId == 87 || userinfo.CustomerInfoId == 45)
                    {
                        headerMain.AddElement(new Phrase("G4S INCIDENT REPORT", mbtimes));
                    }
                    else
                    {
                        headerMain.AddElement(new Phrase("MIMS INCIDENT REPORT", mbtimes));
                    }
                    headerMain.PaddingBottom = 10;
                    headerMain.HorizontalAlignment = 1;
                    headerMain.Border = Rectangle.NO_BORDER;
                    PdfPCell pdfCell1 = new PdfPCell();
                    pdfCell1.Border = Rectangle.NO_BORDER;
                    PdfPCell pdfCell2 = new PdfPCell();
                    pdfCell2.Border = Rectangle.NO_BORDER;
                    mainheadertable.AddCell(pdfCell1);
                    mainheadertable.AddCell(headerMain);
                    mainheadertable.AddCell(pdfCell2);

                    doc.Add(mainheadertable);

                    var headertable = new PdfPTable(2);
                    headertable.DefaultCell.Border = Rectangle.NO_BORDER;
                    headertable.WidthPercentage = 100;
                    headertable.SetWidths(columnWidths1783);

                    var header1 = new PdfPCell();
                    header1.AddElement(new Phrase("Type:", beleventimes));
                    header1.Border = Rectangle.NO_BORDER;

                    headertable.AddCell(header1);

                    var header1a = new PdfPCell();
                    header1a.AddElement(new Phrase(searchStatus, eleventimes));
                    header1a.Border = Rectangle.NO_BORDER;
                    headertable.AddCell(header1a);

                    var header2 = new PdfPCell();
                    header2.AddElement(new Phrase("Status:", beleventimes));
                    header2.Border = Rectangle.NO_BORDER;

                    headertable.AddCell(header2);

                    var header2a = new PdfPCell();
                    header2a.AddElement(new Phrase(searchIncidentStatus, eleventimes));
                    header2a.Border = Rectangle.NO_BORDER;
                    headertable.AddCell(header2a);

                    var header3 = new PdfPCell();
                    header3.AddElement(new Phrase("Period:", beleventimes));
                    header3.Border = Rectangle.NO_BORDER;
                    headertable.AddCell(header3);

                    var header3a = new PdfPCell();
                    header3a.AddElement(new Phrase(fromdate.ToShortDateString() + " to " + todate.Subtract(new TimeSpan(1, 0, 0, 0)).ToShortDateString(), eleventimes));
                    header3a.Border = Rectangle.NO_BORDER;
                    headertable.AddCell(header3a);

                    //var header4 = new PdfPCell();
                    //header4.AddElement(new Phrase("Created By:", beleventimes));
                    //header4.Border = Rectangle.NO_BORDER;

                    //headertable.AddCell(header4);

                    //var header4a = new PdfPCell();
                    //header4a.AddElement(new Phrase(userinfo.CustomerUName, eleventimes));
                    //header4a.Border = Rectangle.NO_BORDER;
                    //headertable.AddCell(header4a);

                    //var header5 = new PdfPCell();
                    //header5.AddElement(new Phrase("Created Date:", beleventimes));
                    //header5.Border = Rectangle.NO_BORDER;
                    //header5.PaddingBottom = 20;
                    //headertable.AddCell(header5);

                    //var header5a = new PdfPCell();
                    //header5a.AddElement(new Phrase(CommonUtility.getDTNow().AddHours(userinfo.TimeZone).ToString(), eleventimes));
                    //header5a.Border = Rectangle.NO_BORDER;
                    //headertable.AddCell(header5a);

                    doc.Add(headertable);
                    float[] columnWidthTable = new float[] { 15, 30, 25, 20, 10 };
                    if (isMob)
                    {
                        PdfPTable table = new PdfPTable(5);
                        table.WidthPercentage = 100;
                        table.SetWidths(columnWidthTable);
                        var cheader1 = new PdfPCell();
                        cheader1.AddElement(new Phrase("Mobile Hot Event", cbtimes));
                        cheader1.Border = Rectangle.NO_BORDER;
                        cheader1.Colspan = 5;
                        cheader1.PaddingBottom = 5;
                        table.AddCell(cheader1);

                        var ancaheader1 = new PdfPCell();
                        ancaheader1.AddElement(new Phrase("Id", weleventimes));
                        ancaheader1.Border = Rectangle.NO_BORDER;
                        ancaheader1.PaddingLeft = 5;
                        ancaheader1.PaddingBottom = 5;
                        ancaheader1.BackgroundColor = Color.GRAY;

                        table.AddCell(ancaheader1);

                        var ncaheader2 = new PdfPCell();
                        ncaheader2.AddElement(new Phrase("Name", weleventimes));
                        ncaheader2.Border = Rectangle.NO_BORDER;
                        ncaheader2.BackgroundColor = Color.GRAY;
                        table.AddCell(ncaheader2);

                        var ncaheader3 = new PdfPCell();
                        ncaheader3.AddElement(new Phrase("Time", weleventimes));
                        ncaheader3.Border = Rectangle.NO_BORDER;
                        ncaheader3.BackgroundColor = Color.GRAY;
                        table.AddCell(ncaheader3);

                        var ncaheader4 = new PdfPCell();
                        ncaheader4.AddElement(new Phrase("User", weleventimes));
                        ncaheader4.Border = Rectangle.NO_BORDER;
                        ncaheader4.BackgroundColor = Color.GRAY;
                        table.AddCell(ncaheader4);

                        var ncaheader1 = new PdfPCell();
                        ncaheader1.AddElement(new Phrase("Status", weleventimes));
                        ncaheader1.Border = Rectangle.NO_BORDER;
                        ncaheader1.BackgroundColor = Color.GRAY;

                        table.AddCell(ncaheader1);

                        var xloghistories = loghistories.Where(i => i.EventType.ToString() == "MobileHotEvent").ToList();
                        var odd = false;
                        foreach (var device in xloghistories)
                        {

                            if (device.RecevieTime.Value.AddHours(userinfo.TimeZone).Ticks > fromdate.Ticks && device.RecevieTime.Value.AddHours(userinfo.TimeZone).Ticks < todate.Ticks)
                            {
                                // targetDt is in between d1 and d2
                                foundEntry = true;


                                if (device.EventType == CustomEvent.EventTypes.MobileHotEvent)
                                {
                                    device.Name = device.EventTypeName;
                                }

                                var xheaderCell = new PdfPCell(new Phrase(device.CustomerIncidentId.ToString(), tentimes));
                                var xheaderCell2 = new PdfPCell(new Phrase(device.Name, tentimes));
                                var xheaderCell3 = new PdfPCell(new Phrase(device.RecevieTime.Value.AddHours(userinfo.TimeZone).ToString(), tentimes));
                                var xheaderCell4 = new PdfPCell(new Phrase(device.CustomerFullName, tentimes));
                                var xheaderCell5 = new PdfPCell(new Phrase(device.StatusName, tentimes));

                                if (!odd)
                                {
                                    xheaderCell.BackgroundColor = Color.LIGHT_GRAY;
                                    xheaderCell2.BackgroundColor = Color.LIGHT_GRAY;
                                    xheaderCell3.BackgroundColor = Color.LIGHT_GRAY;
                                    xheaderCell4.BackgroundColor = Color.LIGHT_GRAY;
                                    xheaderCell5.BackgroundColor = Color.LIGHT_GRAY;
                                    odd = true;
                                }
                                else
                                {
                                    odd = false;
                                }

                                xheaderCell.Border = Rectangle.NO_BORDER;
                                xheaderCell2.Border = Rectangle.NO_BORDER;
                                xheaderCell3.Border = Rectangle.NO_BORDER;
                                xheaderCell4.Border = Rectangle.NO_BORDER;
                                xheaderCell5.Border = Rectangle.NO_BORDER;

                                table.AddCell(xheaderCell);
                                table.AddCell(xheaderCell2);
                                table.AddCell(xheaderCell3);
                                table.AddCell(xheaderCell4);
                                table.AddCell(xheaderCell5);
                            }
                        }
                        doc.Add(table);
                    }
                    if (isReq)
                    {
                        PdfPTable table = new PdfPTable(5);
                        table.WidthPercentage = 100;
                        table.SetWidths(columnWidthTable);

                        var cheader1 = new PdfPCell();
                        cheader1.AddElement(new Phrase("Request", cbtimes));
                        cheader1.Border = Rectangle.NO_BORDER;
                        cheader1.Colspan = 5;
                        table.AddCell(cheader1);

                        var ancaheader1 = new PdfPCell();
                        ancaheader1.AddElement(new Phrase("Id", weleventimes));
                        ancaheader1.Border = Rectangle.NO_BORDER;
                        ancaheader1.PaddingLeft = 5;
                        ancaheader1.PaddingBottom = 5;
                        ancaheader1.BackgroundColor = Color.GRAY;

                        table.AddCell(ancaheader1);



                        var ncaheader2 = new PdfPCell();
                        ncaheader2.AddElement(new Phrase("Name", weleventimes));
                        ncaheader2.Border = Rectangle.NO_BORDER;
                        ncaheader2.BackgroundColor = Color.GRAY;
                        table.AddCell(ncaheader2);

                        var ncaheader3 = new PdfPCell();
                        ncaheader3.AddElement(new Phrase("Time", weleventimes));
                        ncaheader3.Border = Rectangle.NO_BORDER;
                        ncaheader3.BackgroundColor = Color.GRAY;
                        table.AddCell(ncaheader3);

                        var ncaheader4 = new PdfPCell();
                        ncaheader4.AddElement(new Phrase("User", weleventimes));
                        ncaheader4.Border = Rectangle.NO_BORDER;
                        ncaheader4.BackgroundColor = Color.GRAY;
                        table.AddCell(ncaheader4);

                        var ncaheader1 = new PdfPCell();
                        ncaheader1.AddElement(new Phrase("Status", weleventimes));
                        ncaheader1.Border = Rectangle.NO_BORDER;
                        ncaheader1.BackgroundColor = Color.GRAY;

                        table.AddCell(ncaheader1);

                        var xloghistories = loghistories.Where(i => i.EventType.ToString() == "Request").ToList();
                        var odd = false;
                        foreach (var device in xloghistories)
                        {

                            if (device.RecevieTime.Value.AddHours(userinfo.TimeZone).Ticks > fromdate.Ticks && device.RecevieTime.Value.AddHours(userinfo.TimeZone).Ticks < todate.Ticks)
                            {
                                // targetDt is in between d1 and d2
                                foundEntry = true;
                                //table.AddCell(device.EventId.ToString());


                                //if (device.EventType == CustomEvent.EventTypes.MobileHotEvent)
                                //{
                                //    device.Name = device.EventTypeName;
                                //}
                                //else if (device.EventType == CustomEvent.EventTypes.HotEvent)
                                //{
                                //    // var hotEv = HotEvent.GetHotEventById(device.Identifier, dbConnection);

                                //    // if (hotEv != null)
                                //    //    device.UserName = CommonUtility.getPCName(hotEv);
                                //}
                                //table.AddCell(device.Name);

                                //table.AddCell(device.RecevieTime.Value.AddHours(userinfo.TimeZone).ToString());

                                //if (!string.IsNullOrEmpty(device.UserName))
                                //    table.AddCell(device.CustomerUName);
                                //else
                                //    table.AddCell("N/A");

                                //table.AddCell(device.StatusName);

                                if (device.EventType == CustomEvent.EventTypes.MobileHotEvent)
                                {
                                    device.Name = device.EventTypeName;
                                }

                                var xheaderCell = new PdfPCell(new Phrase(device.CustomerIncidentId.ToString(), tentimes));
                                var xheaderCell2 = new PdfPCell(new Phrase(device.Name, tentimes));
                                var xheaderCell3 = new PdfPCell(new Phrase(device.RecevieTime.Value.AddHours(userinfo.TimeZone).ToString(), tentimes));
                                var xheaderCell4 = new PdfPCell(new Phrase(device.CustomerFullName, tentimes));
                                var xheaderCell5 = new PdfPCell(new Phrase(device.StatusName, tentimes));

                                if (!odd)
                                {
                                    xheaderCell.BackgroundColor = Color.LIGHT_GRAY;
                                    xheaderCell2.BackgroundColor = Color.LIGHT_GRAY;
                                    xheaderCell3.BackgroundColor = Color.LIGHT_GRAY;
                                    xheaderCell4.BackgroundColor = Color.LIGHT_GRAY;
                                    xheaderCell5.BackgroundColor = Color.LIGHT_GRAY;
                                    odd = true;
                                }
                                else
                                {
                                    odd = false;
                                }

                                xheaderCell.Border = Rectangle.NO_BORDER;
                                xheaderCell2.Border = Rectangle.NO_BORDER;
                                xheaderCell3.Border = Rectangle.NO_BORDER;
                                xheaderCell4.Border = Rectangle.NO_BORDER;
                                xheaderCell5.Border = Rectangle.NO_BORDER;

                                table.AddCell(xheaderCell);
                                table.AddCell(xheaderCell2);
                                table.AddCell(xheaderCell3);
                                table.AddCell(xheaderCell4);
                                table.AddCell(xheaderCell5);

                            }
                        }

                        doc.Add(table);
                    }
                    if (isSys)
                    {
                        PdfPTable table = new PdfPTable(5);
                        table.WidthPercentage = 100;
                        table.SetWidths(columnWidthTable);
                        var cheader1 = new PdfPCell();
                        cheader1.AddElement(new Phrase("System Created", cbtimes));
                        cheader1.Border = Rectangle.NO_BORDER;
                        cheader1.Colspan = 5;
                        table.AddCell(cheader1);

                        var ancaheader1 = new PdfPCell();
                        ancaheader1.AddElement(new Phrase("Id", weleventimes));
                        ancaheader1.Border = Rectangle.NO_BORDER;
                        ancaheader1.PaddingLeft = 5;
                        ancaheader1.PaddingBottom = 5;
                        ancaheader1.BackgroundColor = Color.GRAY;

                        table.AddCell(ancaheader1);



                        var ncaheader2 = new PdfPCell();
                        ncaheader2.AddElement(new Phrase("Name", weleventimes));
                        ncaheader2.Border = Rectangle.NO_BORDER;
                        ncaheader2.BackgroundColor = Color.GRAY;
                        table.AddCell(ncaheader2);

                        var ncaheader3 = new PdfPCell();
                        ncaheader3.AddElement(new Phrase("Time", weleventimes));
                        ncaheader3.Border = Rectangle.NO_BORDER;
                        ncaheader3.BackgroundColor = Color.GRAY;
                        table.AddCell(ncaheader3);

                        var ncaheader4 = new PdfPCell();
                        ncaheader4.AddElement(new Phrase("User", weleventimes));
                        ncaheader4.Border = Rectangle.NO_BORDER;
                        ncaheader4.BackgroundColor = Color.GRAY;
                        table.AddCell(ncaheader4);

                        var ncaheader1 = new PdfPCell();
                        ncaheader1.AddElement(new Phrase("Status", weleventimes));
                        ncaheader1.Border = Rectangle.NO_BORDER;
                        ncaheader1.BackgroundColor = Color.GRAY;

                        table.AddCell(ncaheader1);

                        var xloghistories = loghistories.Where(i => i.EventType.ToString() == "Incident").ToList();
                        var odd = false;
                        foreach (var device in xloghistories)
                        {

                            if (device.RecevieTime.Value.AddHours(userinfo.TimeZone).Ticks > fromdate.Ticks && device.RecevieTime.Value.AddHours(userinfo.TimeZone).Ticks < todate.Ticks)
                            {
                                // targetDt is in between d1 and d2
                                foundEntry = true;
                                //table.AddCell(device.EventId.ToString());


                                //if (device.EventType == CustomEvent.EventTypes.MobileHotEvent)
                                //{
                                //    device.Name = device.EventTypeName;
                                //}
                                //else if (device.EventType == CustomEvent.EventTypes.HotEvent)
                                //{
                                //    // var hotEv = HotEvent.GetHotEventById(device.Identifier, dbConnection);

                                //    // if (hotEv != null)
                                //    //    device.UserName = CommonUtility.getPCName(hotEv);
                                //}
                                //table.AddCell(device.Name);

                                //table.AddCell(device.RecevieTime.Value.AddHours(userinfo.TimeZone).ToString());

                                //if (!string.IsNullOrEmpty(device.UserName))
                                //    table.AddCell(device.CustomerUName);
                                //else
                                //    table.AddCell("N/A");

                                //table.AddCell(device.StatusName);

                                if (device.EventType == CustomEvent.EventTypes.MobileHotEvent)
                                {
                                    device.Name = device.EventTypeName;
                                }

                                var xheaderCell = new PdfPCell(new Phrase(device.CustomerIncidentId.ToString(), tentimes));
                                var xheaderCell2 = new PdfPCell(new Phrase(device.Name, tentimes));
                                var xheaderCell3 = new PdfPCell(new Phrase(device.RecevieTime.Value.AddHours(userinfo.TimeZone).ToString(), tentimes));
                                var xheaderCell4 = new PdfPCell(new Phrase(device.CustomerFullName, tentimes));
                                var xheaderCell5 = new PdfPCell(new Phrase(device.StatusName, tentimes));

                                if (!odd)
                                {
                                    xheaderCell.BackgroundColor = Color.LIGHT_GRAY;
                                    xheaderCell2.BackgroundColor = Color.LIGHT_GRAY;
                                    xheaderCell3.BackgroundColor = Color.LIGHT_GRAY;
                                    xheaderCell4.BackgroundColor = Color.LIGHT_GRAY;
                                    xheaderCell5.BackgroundColor = Color.LIGHT_GRAY;
                                    odd = true;
                                }
                                else
                                {
                                    odd = false;
                                }

                                xheaderCell.Border = Rectangle.NO_BORDER;
                                xheaderCell2.Border = Rectangle.NO_BORDER;
                                xheaderCell3.Border = Rectangle.NO_BORDER;
                                xheaderCell4.Border = Rectangle.NO_BORDER;
                                xheaderCell5.Border = Rectangle.NO_BORDER;

                                table.AddCell(xheaderCell);
                                table.AddCell(xheaderCell2);
                                table.AddCell(xheaderCell3);
                                table.AddCell(xheaderCell4);
                                table.AddCell(xheaderCell5);

                            }
                        }


                        doc.Add(table);
                    }
                    Font geleventimes = new Font(f_cn, 11, Font.NORMAL, Color.GRAY);
                    var endTable = new PdfPTable(1);
                    endTable.DefaultCell.Border = Rectangle.NO_BORDER;
                    endTable.WidthPercentage = 100;

                    var endheader = new PdfPCell();
                    endheader.AddElement(new Phrase("Report generated: " + CommonUtility.getDTNow().AddHours(userinfo.TimeZone).ToString() + " 	by: " + userinfo.Username, geleventimes));
                    endheader.Border = Rectangle.NO_BORDER;
                    endTable.AddCell(endheader);
                    doc.Add(endTable);

                    writer.Flush();
                    doc.Close();

                    if (!foundEntry)
                        return "No entries found!";
                    else
                    {

                        //var vpath = mimssettings.MIMSMobileAddress + "//Uploads//PDFReports//" + fileName;
                        //return vpath;
                        if (File.Exists(path))
                        {
                            using (StreamReader sr = new StreamReader(path))
                            {
                                var vpath = CommonUtility.CloudUploadFile(userinfo.CustomerInfoId, fileName, sr.BaseStream);
                                //var vpath = mimssettings.MIMSMobileAddress + "//Uploads//PDFReports//" + fileName;
                                return vpath;
                            }
                        }
                        return "Failed to generate report!";
                    }
                }
                else
                {
                    return "No entries found!";
                }
            }

            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("Reports", "GeneratePDFforDeviceApplicationHistory", exp, dbConnection, userinfo.SiteId);
                return "No entries found!";
            }
            return "No entries found!";
        }
        private static string GeneratePDFLog(List<MIMSLog> loghistories, DateTime fromdate, DateTime todate, string reportName, Users userinfo)
        {
            string fileName = string.Empty;
            var foundEntry = false;
            try
            {
                if (loghistories.Count != 0)
                {
                    var mimssettings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);

                    //string appPath = PhysicalPath("~/PDFReports");

                    var preappPath = mimssettings.FilePath.Substring(0, mimssettings.FilePath.Length - 5);

                    string appPath = preappPath + "\\PDFReports";
                    string path = appPath;
                    /*Logo Path*/
                    string strLogoPath = CommonUtility.PhysicalPath("~/Images/custom-images") + "\\site-login-logo.png";
                    string strLogoPath2 = CommonUtility.PhysicalPath("~/Images/custom-images") + "\\site-login-logo.png";

                    iTextSharp.text.Document doc = new iTextSharp.text.Document(PageSize.LETTER, 25F, 25F, 50F, 25F);

                    doc.SetMargins(25, 25, 65, 35);

                    fileName = DateTime.Now.Day.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Year.ToString() + "-" + DateTime.Now.Hour.ToString() + "-" + DateTime.Now.Minute.ToString() + "-" + DateTime.Now.Second.ToString() + ".pdf";
                    path = fpath + fileName;

                    PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(path, FileMode.Create));
                    writer.PageEvent = new ITextEvents()
                    {
                        cid = userinfo.CustomerInfoId
                    };
                    doc.Open();

                    iTextSharp.text.Image img1 = iTextSharp.text.Image.GetInstance(strLogoPath2);
                    img1.SetAbsolutePosition(doc.GetLeft(15), doc.GetTop(-15));
                    img1.ScalePercent(50f);
                    img1.Alignment = iTextSharp.text.Image.ALIGN_LEFT;
                    doc.Add(img1);

                    BaseFont f_cn = BaseFont.CreateFont(Environment.GetFolderPath(Environment.SpecialFolder.Fonts) + "\\verdana.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

                    PdfContentByte cb = writer.DirectContent;
                    cb.BeginText();
                    cb.SetFontAndSize(f_cn, 14); 
                    if (userinfo.CustomerInfoId == 87 || userinfo.CustomerInfoId == 45)
                    {
                        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "G4S GENERATED REPORT", 320, 700, 0);
                    }
                    else
                    {
                        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "MIMS GENERATED REPORT", 320, 700, 0);
                    }
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Created Date: " + DateTime.Now.ToString(), 40, 670, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Report Name:  " + reportName, 40, 655, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Generated By: " + userinfo.FirstName + " "+userinfo.LastName, 40, 640, 0);

                    if (fromdate > DateTime.MinValue & todate > DateTime.MinValue)
                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Period: " + fromdate.ToShortDateString() + " To " + todate.Subtract(new TimeSpan(1, 0, 0, 0)).ToShortDateString(), 40, 625, 0);
                    else
                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Period: All", 40, 625, 0);

                    //cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Licence Key: jorpsdjordan public security directorate9/7/2014 04:49:19 pm", 40, 610, 0);

                    cb.EndText();
                    iTextSharp.text.Paragraph paragraphTable = new iTextSharp.text.Paragraph();
                    paragraphTable.SpacingBefore = 150f;
                    PdfPTable table = new PdfPTable(4);


                    table.AddCell("Message");
                    table.AddCell("Exception");
                    table.AddCell("Time");
                    table.AddCell("Source");

                    foreach (var device in loghistories)
                    {

                        //if (device.RecevieTime.Value.Ticks > fromdate.Ticks && device.RecevieTime.Value.Ticks < todate.Ticks)
                        {
                            // targetDt is in between d1 and d2
                            foundEntry = true;
                            table.AddCell(device.Message);

                            table.AddCell(device.Exception);

                            table.AddCell(device.CurrentDateTime.ToString());

                            table.AddCell(device.MacAddress);

                        }
                    }

                    doc.Add(paragraphTable);
                    doc.Add(table);

                    writer.Flush();
                    doc.Close();
                    //  DownloadFile(fileName);


                    if (!foundEntry)
                        return "No entries found!";
                    else
                    {

                        //var vpath = mimssettings.MIMSMobileAddress + "//Uploads//PDFReports//" + fileName;
                        //return vpath;
                        if (File.Exists(path))
                        {
                            using (StreamReader sr = new StreamReader(path))
                            {
                                var vpath = CommonUtility.CloudUploadFile(userinfo.CustomerInfoId, fileName, sr.BaseStream);
                                //var vpath = mimssettings.MIMSMobileAddress + "//Uploads//PDFReports//" + fileName;
                                return vpath;
                            }
                        }
                        return "Failed to generate report!";
                    }
                }
            }

            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("Reports", "GeneratePDFLog", exp, dbConnection, userinfo.SiteId);
                return "No entries found!";
            }
            return "No entries found!";
        }

        private static string GenerateDutyRoster(DateTime fromdate, DateTime todate, string reportName, Users userinfo)
        {
            string fileName = string.Empty;
            var foundEntry = false;
            try
            {


                iTextSharp.text.Document doc = new iTextSharp.text.Document(PageSize.LETTER, 25F, 25F, 50F, 25F);

                doc.SetMargins(25, 25, 65, 35);

                fileName = DateTime.Now.Day.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Year.ToString() + "-" + DateTime.Now.Hour.ToString() + "-" + DateTime.Now.Minute.ToString() + "-" + DateTime.Now.Second.ToString() + ".pdf";
                var path = fpath + fileName;

                PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(path, FileMode.Create));
                writer.PageEvent = new ITextEvents()
                {
                    cid = userinfo.CustomerInfoId
                };
                doc.Open();


                BaseFont f_cn = BaseFont.CreateFont(Environment.GetFolderPath(Environment.SpecialFolder.Fonts) + "\\verdana.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                // BaseFont h_cn = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                BaseFont z_cn = BaseFont.CreateFont(BaseFont.ZAPFDINGBATS, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                Font times = new Font(f_cn, 12, Font.NORMAL, Color.BLACK);
                Font btimes = new Font(f_cn, 12, Font.BOLD, Color.BLACK);

                iTextSharp.text.Color arrowred = new iTextSharp.text.Color(162, 0, 46);

                Font eleventimes = new Font(f_cn, 11, Font.NORMAL, Color.BLACK);
                Font tentimes = new Font(f_cn, 10, Font.NORMAL, Color.BLACK);

                Font weleventimes = new Font(f_cn, 11, Font.NORMAL, Color.WHITE);

                Font wnine = new Font(f_cn, 9, Font.NORMAL, Color.WHITE);

                Font gweb = new Font(z_cn, 11, Font.NORMAL, Color.GREEN);
                Font rweb = new Font(z_cn, 11, Font.NORMAL, Color.RED);

                Font ueleventimes = new Font(f_cn, 11, Font.UNDERLINE, Color.BLACK);

                Font beleventimes = new Font(f_cn, 11, Font.BOLD, Color.BLACK);
                Font bredeleventimes = new Font(f_cn, 11, Font.BOLD, Color.RED);
                Font redeleventimes = new Font(f_cn, 11, Font.NORMAL, Color.RED);
                Font twelvetimes = new Font(f_cn, 12, Font.ITALIC, Color.BLACK);

                Font btentimes = new Font(f_cn, 11, Font.NORMAL, Color.BLACK);

                Font headerf = new Font(f_cn, 9, Font.NORMAL, Color.BLACK);

                Font mbtimes = new Font(f_cn, 18, Font.BOLD, arrowred);
                Font cbtimes = new Font(f_cn, 13, Font.BOLD, arrowred);

                float[] columnWidths1585 = new float[] { 15, 85 };
                float[] columnWidths2080 = new float[] { 20, 80 };

                float[] columnWidths1783 = new float[] { 18, 82 };

                float[] columnWidths2575 = new float[] { 25, 75 };
                float[] columnWidths3070 = new float[] { 30, 70 };
                float[] columnWidths3565 = new float[] { 35, 65 };
                float[] columnWidths4555 = new float[] { 45, 55 };
                float[] columnWidthsH = new float[] { 35, 60, 15 };

                if (string.IsNullOrEmpty(userinfo.SiteName))
                    userinfo.SiteName = "N/A";

                var mainheadertable = new PdfPTable(3);
                mainheadertable.DefaultCell.Border = Rectangle.NO_BORDER;
                mainheadertable.WidthPercentage = 100;
                mainheadertable.SetWidths(columnWidthsH);
                var headerMain = new PdfPCell();
                if (userinfo.CustomerInfoId == 87 || userinfo.CustomerInfoId == 45)
                {
                    headerMain.AddElement(new Phrase("G4S DUTY REPORT", mbtimes));
                }
                else
                {
                    headerMain.AddElement(new Phrase("MIMS DUTY REPORT", mbtimes));
                }
                headerMain.PaddingBottom = 10;
                headerMain.HorizontalAlignment = 1;
                headerMain.Border = Rectangle.NO_BORDER;
                PdfPCell pdfCell1 = new PdfPCell();
                pdfCell1.Border = Rectangle.NO_BORDER;
                PdfPCell pdfCell2 = new PdfPCell();
                pdfCell2.Border = Rectangle.NO_BORDER;
                mainheadertable.AddCell(pdfCell1);
                mainheadertable.AddCell(headerMain);
                mainheadertable.AddCell(pdfCell2);

                doc.Add(mainheadertable);

                var headertable = new PdfPTable(2);
                headertable.DefaultCell.Border = Rectangle.NO_BORDER;
                headertable.WidthPercentage = 100;
                headertable.SetWidths(columnWidths1783);


                var header3 = new PdfPCell();
                header3.AddElement(new Phrase("Period:", beleventimes));
                header3.Border = Rectangle.NO_BORDER;
                headertable.AddCell(header3);

                var header3a = new PdfPCell();
                header3a.AddElement(new Phrase(fromdate.ToShortDateString() + " to " + todate.ToShortDateString(), eleventimes));
                header3a.Border = Rectangle.NO_BORDER;
                headertable.AddCell(header3a);

                //var header2 = new PdfPCell();
                //header2.AddElement(new Phrase("Created By:", beleventimes));
                //header2.Border = Rectangle.NO_BORDER;

                //headertable.AddCell(header2);

                //var header2a = new PdfPCell();
                //header2a.AddElement(new Phrase(userinfo.CustomerUName, eleventimes));
                //header2a.Border = Rectangle.NO_BORDER;
                //headertable.AddCell(header2a);

                //var header5 = new PdfPCell();
                //header5.AddElement(new Phrase("Created Date:", beleventimes));
                //header5.Border = Rectangle.NO_BORDER;
                //header5.PaddingBottom = 20;
                //headertable.AddCell(header5);

                //var header5a = new PdfPCell();
                //header5a.AddElement(new Phrase(CommonUtility.getDTNow().AddHours(userinfo.TimeZone).ToString(), eleventimes));
                //header5a.Border = Rectangle.NO_BORDER;
                //headertable.AddCell(header5a);

                doc.Add(headertable);

                PdfPTable table = new PdfPTable(8);
                table.WidthPercentage = 100;

                var ncaheader1 = new PdfPCell();
                ncaheader1.AddElement(new Phrase("User", weleventimes));
                ncaheader1.Border = Rectangle.NO_BORDER;
                ncaheader1.PaddingLeft = 5;
                ncaheader1.PaddingBottom = 5;
                ncaheader1.BackgroundColor = Color.GRAY;

                table.AddCell(ncaheader1);

                var ncaheader2 = new PdfPCell();
                ncaheader2.AddElement(new Phrase(fromdate.ToString("ddd"), weleventimes));
                ncaheader2.Border = Rectangle.NO_BORDER;
                ncaheader2.BackgroundColor = Color.GRAY;
                table.AddCell(ncaheader2);

                var ncaheader3 = new PdfPCell();
                ncaheader3.AddElement(new Phrase(fromdate.AddDays(1).ToString("ddd"), weleventimes));
                ncaheader3.Border = Rectangle.NO_BORDER;
                ncaheader3.BackgroundColor = Color.GRAY;
                table.AddCell(ncaheader3);

                var ncaheader4 = new PdfPCell();
                ncaheader4.AddElement(new Phrase(fromdate.AddDays(2).ToString("ddd"), weleventimes));
                ncaheader4.Border = Rectangle.NO_BORDER;
                ncaheader4.BackgroundColor = Color.GRAY;
                table.AddCell(ncaheader4);

                var ncaheader5 = new PdfPCell();
                ncaheader5.AddElement(new Phrase(fromdate.AddDays(3).ToString("ddd"), weleventimes));
                ncaheader5.Border = Rectangle.NO_BORDER;
                ncaheader5.BackgroundColor = Color.GRAY;
                table.AddCell(ncaheader5);

                var ncaheader6 = new PdfPCell();
                ncaheader6.AddElement(new Phrase(fromdate.AddDays(4).ToString("ddd"), weleventimes));
                ncaheader6.Border = Rectangle.NO_BORDER;
                ncaheader6.BackgroundColor = Color.GRAY;
                table.AddCell(ncaheader6);

                var ncaheader7 = new PdfPCell();
                ncaheader7.AddElement(new Phrase(fromdate.AddDays(5).ToString("ddd"), weleventimes));
                ncaheader7.Border = Rectangle.NO_BORDER;
                ncaheader7.BackgroundColor = Color.GRAY;
                table.AddCell(ncaheader7);

                var ncaheader8 = new PdfPCell();
                ncaheader8.AddElement(new Phrase(fromdate.AddDays(6).ToString("ddd"), weleventimes));
                ncaheader8.Border = Rectangle.NO_BORDER;
                ncaheader8.BackgroundColor = Color.GRAY;
                table.AddCell(ncaheader8);


                var siteUsers = new List<Users>();
                if (userinfo.RoleId != (int)Role.SuperAdmin && userinfo.RoleId != (int)Role.ChiefOfficer)
                {
                    if (userinfo.RoleId == (int)Role.Regional)
                    {
                        //var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                        //foreach (var site in sites)
                        //{
                        siteUsers.AddRange(Users.GetAllUsersByLevel5(userinfo.ID, dbConnection));
                        //}
                    }
                    else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                    {
                        siteUsers = Users.GetAllUsersByCustomerIdNotIncludeCustomerUser(userinfo.CustomerInfoId, dbConnection);
                        siteUsers = siteUsers.Where(i => i.RoleId != (int)Role.CustomerSuperadmin).ToList();
                    }
                    else
                    {
                        siteUsers = Users.GetAllUsersBySiteId(userinfo.SiteId, dbConnection);
                    }
                }
                else
                    siteUsers = Users.GetAllUsers(dbConnection);

                siteUsers = siteUsers.Where(i => i.RoleId != (int)Role.MessageBoardUser && i.RoleId != (int)Role.CustomerUser).ToList();
                siteUsers = siteUsers.Where(i => i.Username != userinfo.Username).ToList();

                List myList = new ZapfDingbatsList(52);
                myList.Add(".");
                iTextSharp.text.pdf.PdfPCell imgCell1 = new iTextSharp.text.pdf.PdfPCell();
                imgCell1.AddElement(myList);

                var odd = false;

                foreach (var sUser in siteUsers)
                {
                    var ucolor = Color.WHITE;

                    if (!odd)
                    {
                        ucolor = Color.LIGHT_GRAY;
                        odd = true;
                    }
                    else
                    {
                        odd = false;
                    }

                    var xheaderCell = new PdfPCell(new Phrase(sUser.FirstName + " " + sUser.LastName, tentimes));
                    xheaderCell.Border = Rectangle.NO_BORDER;
                    xheaderCell.BackgroundColor = ucolor;
                    table.AddCell(xheaderCell);

                    var week1Count = DutyRoster.GetDutyRosterByEmpIdAndTime(fromdate.ToString(), fromdate.AddDays(1).ToString(), sUser.ID, dbConnection).Count;
                    if (week1Count > 0)
                    {
                        var xheaderCellA = new PdfPCell(new Phrase("X", tentimes));
                        xheaderCellA.Border = Rectangle.NO_BORDER;
                        xheaderCellA.BackgroundColor = ucolor;
                        table.AddCell(xheaderCellA);
                    }
                    else
                    {
                        var xheaderCellA = new PdfPCell(new Phrase(" ", tentimes));
                        xheaderCellA.Border = Rectangle.NO_BORDER;
                        xheaderCellA.BackgroundColor = ucolor;
                        table.AddCell(xheaderCellA);

                    }

                    var week2Count = DutyRoster.GetDutyRosterByEmpIdAndTime(fromdate.AddDays(1).ToString(), fromdate.AddDays(2).ToString(), sUser.ID, dbConnection).Count;
                    if (week2Count > 0)
                    {
                        var xheaderCellA = new PdfPCell(new Phrase("X", tentimes));
                        xheaderCellA.Border = Rectangle.NO_BORDER;
                        xheaderCellA.BackgroundColor = ucolor;
                        table.AddCell(xheaderCellA);
                    }
                    else
                    {
                        var xheaderCellA = new PdfPCell(new Phrase(" ", tentimes));
                        xheaderCellA.Border = Rectangle.NO_BORDER;
                        xheaderCellA.BackgroundColor = ucolor;
                        table.AddCell(xheaderCellA);

                    }

                    var week3Count = DutyRoster.GetDutyRosterByEmpIdAndTime(fromdate.AddDays(2).ToString(), fromdate.AddDays(3).ToString(), sUser.ID, dbConnection).Count;
                    if (week3Count > 0)
                    {
                        var xheaderCellA = new PdfPCell(new Phrase("X", tentimes));
                        xheaderCellA.Border = Rectangle.NO_BORDER;
                        xheaderCellA.BackgroundColor = ucolor;
                        table.AddCell(xheaderCellA);
                    }
                    else
                    {
                        var xheaderCellA = new PdfPCell(new Phrase(" ", tentimes));
                        xheaderCellA.Border = Rectangle.NO_BORDER;
                        xheaderCellA.BackgroundColor = ucolor;
                        table.AddCell(xheaderCellA);

                    }

                    var week4Count = DutyRoster.GetDutyRosterByEmpIdAndTime(fromdate.AddDays(3).ToString(), fromdate.AddDays(4).ToString(), sUser.ID, dbConnection).Count;
                    if (week4Count > 0)
                    {
                        var xheaderCellA = new PdfPCell(new Phrase("X", tentimes));
                        xheaderCellA.Border = Rectangle.NO_BORDER;
                        xheaderCellA.BackgroundColor = ucolor;
                        table.AddCell(xheaderCellA);
                    }
                    else
                    {
                        var xheaderCellA = new PdfPCell(new Phrase(" ", tentimes));
                        xheaderCellA.Border = Rectangle.NO_BORDER;
                        xheaderCellA.BackgroundColor = ucolor;
                        table.AddCell(xheaderCellA);

                    }

                    var week5Count = DutyRoster.GetDutyRosterByEmpIdAndTime(fromdate.AddDays(4).ToString(), fromdate.AddDays(5).ToString(), sUser.ID, dbConnection).Count;
                    if (week5Count > 0)
                    {
                        var xheaderCellA = new PdfPCell(new Phrase("X", tentimes));
                        xheaderCellA.Border = Rectangle.NO_BORDER;
                        xheaderCellA.BackgroundColor = ucolor;
                        table.AddCell(xheaderCellA);
                    }
                    else
                    {
                        var xheaderCellA = new PdfPCell(new Phrase(" ", tentimes));
                        xheaderCellA.Border = Rectangle.NO_BORDER;
                        xheaderCellA.BackgroundColor = ucolor;
                        table.AddCell(xheaderCellA);

                    }

                    var week6Count = DutyRoster.GetDutyRosterByEmpIdAndTime(fromdate.AddDays(5).ToString(), fromdate.AddDays(6).ToString(), sUser.ID, dbConnection).Count;
                    if (week6Count > 0)
                    {
                        var xheaderCellA = new PdfPCell(new Phrase("X", tentimes));
                        xheaderCellA.Border = Rectangle.NO_BORDER;
                        xheaderCellA.BackgroundColor = ucolor;
                        table.AddCell(xheaderCellA);
                    }
                    else
                    {
                        var xheaderCellA = new PdfPCell(new Phrase(" ", tentimes));
                        xheaderCellA.Border = Rectangle.NO_BORDER;
                        xheaderCellA.BackgroundColor = ucolor;
                        table.AddCell(xheaderCellA);

                    }

                    var week7Count = DutyRoster.GetDutyRosterByEmpIdAndTime(fromdate.AddDays(6).ToString(), fromdate.AddDays(7).ToString(), sUser.ID, dbConnection).Count;
                    if (week7Count > 0)
                    {
                        var xheaderCellA = new PdfPCell(new Phrase("X", tentimes));
                        xheaderCellA.Border = Rectangle.NO_BORDER;
                        xheaderCellA.BackgroundColor = ucolor;
                        table.AddCell(xheaderCellA);
                    }
                    else
                    {
                        var xheaderCellA = new PdfPCell(new Phrase(" ", tentimes));
                        xheaderCellA.Border = Rectangle.NO_BORDER;
                        xheaderCellA.BackgroundColor = ucolor;
                        table.AddCell(xheaderCellA);

                    }
                }

                doc.Add(table);

                Font geleventimes = new Font(f_cn, 11, Font.NORMAL, Color.GRAY);
                var endTable = new PdfPTable(1);
                endTable.DefaultCell.Border = Rectangle.NO_BORDER;
                endTable.WidthPercentage = 100;

                var endheader = new PdfPCell();
                endheader.AddElement(new Phrase("Report generated: " + CommonUtility.getDTNow().AddHours(userinfo.TimeZone).ToString() + " 	by: " + userinfo.Username, geleventimes));
                endheader.Border = Rectangle.NO_BORDER;
                endTable.AddCell(endheader);
                doc.Add(endTable);

                writer.Flush();
                doc.Close();

                if (File.Exists(path))
                {
                    using (StreamReader sr = new StreamReader(path))
                    {
                        var vpath = CommonUtility.CloudUploadFile(userinfo.CustomerInfoId, fileName, sr.BaseStream);

                        return vpath;
                    }
                }
                return "Failed to generate report!";

            }

            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("Reports", "GeneratePDFLog", exp, dbConnection, userinfo.SiteId);
                return "Error occured! " + exp.Message;
            }
            return "No entries found!";
        }
        private static string GenerateUsers(DateTime fromdate, DateTime todate, string reportName, Users userinfo)
        {
            string fileName = string.Empty;
            var foundEntry = false;
            try
            {
                var mimssettings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);

                var preappPath = mimssettings.FilePath.Substring(0, mimssettings.FilePath.Length - 5);

                string appPath = preappPath + "\\PDFReports";
                string path = appPath;
                /*Logo Path*/
                string strLogoPath = CommonUtility.PhysicalPath("~/Images/custom-images") + "\\site-login-logo.png";
                string strLogoPath2 = CommonUtility.PhysicalPath("~/Images/custom-images") + "\\site-login-logo.png";

                iTextSharp.text.Document doc = new iTextSharp.text.Document(PageSize.LETTER, 25F, 25F, 50F, 25F);

                doc.SetMargins(25, 25, 65, 35);

                fileName = DateTime.Now.Day.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Year.ToString() + "-" + DateTime.Now.Hour.ToString() + "-" + DateTime.Now.Minute.ToString() + "-" + DateTime.Now.Second.ToString() + ".pdf";
                path = fpath + fileName;

                PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(path, FileMode.Create));
                writer.PageEvent = new ITextEvents()
                {
                    cid = userinfo.CustomerInfoId
                };
                doc.Open();

                iTextSharp.text.Image img1 = iTextSharp.text.Image.GetInstance(strLogoPath2);
                img1.SetAbsolutePosition(doc.GetLeft(15), doc.GetTop(-15));
                img1.ScalePercent(50f);
                img1.Alignment = iTextSharp.text.Image.ALIGN_LEFT;
                doc.Add(img1);

                BaseFont f_cn = BaseFont.CreateFont(Environment.GetFolderPath(Environment.SpecialFolder.Fonts) + "\\verdana.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

                PdfContentByte cb = writer.DirectContent;
                cb.BeginText();
                cb.SetFontAndSize(f_cn, 14); 
                if (userinfo.CustomerInfoId == 87 || userinfo.CustomerInfoId == 45)
                {
                    cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "G4S GENERATED REPORT", 320, 700, 0);
                }
                else
                {
                    cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "MIMS GENERATED REPORT", 320, 700, 0);
                }
                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Created Date: " + DateTime.Now.ToString(), 40, 670, 0);
                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Report Name:  " + reportName, 40, 655, 0);
                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Generated By: " + userinfo.FirstName + " " +userinfo.LastName , 40, 640, 0);

                if (fromdate > DateTime.MinValue & todate > DateTime.MinValue)
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Period: " + fromdate.ToShortDateString() + " To " + todate.AddDays(-1).ToShortDateString(), 40, 625, 0);
                else
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Period: All", 40, 625, 0);

                //cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Licence Key: jorpsdjordan public security directorate9/7/2014 04:49:19 pm", 40, 610, 0);

                cb.EndText();
                iTextSharp.text.Paragraph paragraphTable = new iTextSharp.text.Paragraph();
                paragraphTable.SpacingBefore = 150f;
                PdfPTable table = new PdfPTable(10);

                table.AddCell("ID");
                table.AddCell("Username");
                table.AddCell("Level");
                table.AddCell("RoleId");
                table.AddCell("DeviceType");
                table.AddCell("Register");
                table.AddCell("LastLogin");
                table.AddCell("CreatedBy");
                table.AddCell("CreatedDate");
                table.AddCell("SiteId");

                var siteUsers = new List<Users>();
                if (userinfo.RoleId != (int)Role.SuperAdmin && userinfo.RoleId != (int)Role.ChiefOfficer)
                {
                    if (userinfo.RoleId == (int)Role.Regional)
                    {
                        //var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                        //foreach (var site in sites)
                        //{
                        siteUsers.AddRange(Users.GetAllUsersByLevel5(userinfo.ID, dbConnection));
                        //}
                    }
                    else
                    {
                        siteUsers = Users.GetAllUsersBySiteId(userinfo.SiteId, dbConnection);
                    }
                }
                else
                    siteUsers = Users.GetAllUsers(dbConnection);

                siteUsers = siteUsers.Where(i => i.Username != userinfo.Username).ToList();

                List myList = new ZapfDingbatsList(52);
                myList.Add(".");
                iTextSharp.text.pdf.PdfPCell imgCell1 = new iTextSharp.text.pdf.PdfPCell();
                imgCell1.AddElement(myList);
                foreach (var sUser in siteUsers)
                {
                    table.AddCell(sUser.ID.ToString());
                    table.AddCell(sUser.Username);
                    if (sUser.RoleId == (int)Role.Manager)
                    {
                        table.AddCell("Level 2");
                    }
                    else if (sUser.RoleId == (int)Role.Admin)
                    {
                        table.AddCell("Level 3");
                    }
                    else if (sUser.RoleId == (int)Role.Director)
                    {
                        table.AddCell("Level 4");
                    }
                    else if (sUser.RoleId == (int)Role.Regional)
                    {
                        table.AddCell("Level 5");
                    }
                    else if (sUser.RoleId == (int)Role.ChiefOfficer)
                    {
                        table.AddCell("Level 6");
                    }
                    else if (sUser.RoleId == (int)Role.UnassignedOperator || sUser.RoleId == (int)Role.Operator)
                    {
                        table.AddCell("Level 1");
                    }
                    table.AddCell(sUser.RoleId.ToString());
                    table.AddCell(sUser.DeviceType.ToString());
                    table.AddCell(sUser.Register.ToString());
                    table.AddCell(sUser.LastLogin.Value.AddHours(userinfo.TimeZone).ToString());
                    table.AddCell(sUser.CreatedBy.ToString());
                    table.AddCell(sUser.CreatedDate.Value.AddHours(userinfo.TimeZone).ToString());
                    table.AddCell(sUser.SiteId.ToString());
                }

                doc.Add(paragraphTable);
                doc.Add(table);

                writer.Flush();
                doc.Close();
                if (File.Exists(path))
                {
                    using (StreamReader sr = new StreamReader(path))
                    {
                        var vpath = CommonUtility.CloudUploadFile(userinfo.CustomerInfoId, fileName, sr.BaseStream);
                        return vpath;
                    }
                }
                return "Failed to generate report!";

            }

            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("Reports", "GeneratePDFLog", exp, dbConnection, userinfo.SiteId);
                return "Error occured! " + exp.Message;
            }
            return "No entries found!";
        }

        [WebMethod]
        public static List<string> getServerData(int id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var isValidSession = LoginSession.GetLoginStatusByUsernameAndSessionId(userinfo.Username,
HttpContext.Current.Session.SessionID, CommonUtility.dbConnection);

                if (isValidSession == null)
                {
                    listy.Add("LOGOUT");
                    return listy;
                }
                else
                {
                    var licenseInfo = ClientLicence.GetLicenseByClientID(CommonUtility.arrowlabsKey, dbConnection);
                    var getalldevs = Device.GetClientDeviceByClientID(CommonUtility.arrowlabsKey, dbConnection);
                    var devinuse = 0;
                    foreach (var dev in getalldevs)
                    {
                        if (dev.State == Arrowlabs.Business.Layer.Device.DeviceState.Enable.ToString())
                            devinuse++;
                    }
                    var users = Users.GetAllUsersByCustomerId(userinfo.CustomerInfoId, dbConnection).Count;//Users.GetAllMobileOnlineUsers(dbConnection);//LoginSession.GetAllMimsMobileOnlineByDeviceType(1, dbConnection);
                    var cInfo = CustomerInfo.GetCustomerInfoById(userinfo.CustomerInfoId, dbConnection);
                    if (licenseInfo != null)
                    {
                        var remaining = (cInfo.TotalUser - users).ToString();
                        listy.Add("N/A");
                        listy.Add("N/A");
                        listy.Add("N/A");
                        listy.Add("N/A");
                        listy.Add("N/A");
                        listy.Add("N/A");
                        listy.Add("N/A"); //Remaining Client
                        listy.Add("N/A"); //Total Client
                        listy.Add("N/A"); //Used Client
                        listy.Add(remaining); // Remaining Mobile
                        listy.Add(cInfo.TotalUser.ToString());//licenseInfo.TotalMobileUsers); //Total Mobile
                        listy.Add(users.ToString()); // Used
                        //licenseInfo.RemainingMobileUsers = remaining;
                        //licenseInfo.UsedMobileUsers = users.ToString();

                        var modules = cInfo.Modules.Split('?');

                        //listy.Add(licenseInfo.isSurveillance.ToString().ToLower());
                        var isSurv = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Surveillance).ToString()).ToList();
                        if (isSurv != null && isSurv.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isNotification.ToString().ToLower());//CHANGED TO MESAGEBOARD
                        var isNoti = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.MessageBoard).ToString()).ToList();
                        if (isNoti != null && isNoti.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isLocation.ToString().ToLower()); //CHANGED TO MESAGEBOARD
                        var isLoc = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Contract).ToString()).ToList();
                        if (isLoc != null && isLoc.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isTicketing.ToString().ToLower());
                        var isTicket = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Ticketing).ToString()).ToList();
                        if (isTicket != null && isTicket.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isTask.ToString().ToLower());
                        var isTask = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Task).ToString()).ToList();
                        if (isTask != null && isTask.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isIncident.ToString().ToLower());
                        var isInci = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Incident).ToString()).ToList();
                        if (isInci != null && isInci.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isWarehouse.ToString().ToLower());
                        var isWare = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Warehouse).ToString()).ToList();
                        if (isWare != null && isWare.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");


                        //listy.Add(licenseInfo.isChat.ToString().ToLower());
                        var isChat = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Chat).ToString()).ToList();
                        if (isChat != null && isChat.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isCollaboration.ToString().ToLower());
                        var isCollab = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Collaboration).ToString()).ToList();
                        if (isCollab != null && isCollab.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isLostandFound.ToString().ToLower());
                        var isLF = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.LostandFound).ToString()).ToList();
                        if (isLF != null && isLF.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");


                        //listy.Add(licenseInfo.isDutyRoster.ToString().ToLower());
                        var isDR = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.DutyRoster).ToString()).ToList();
                        if (isDR != null && isDR.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isPostOrder.ToString().ToLower());
                        var isPO = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.PostOrder).ToString()).ToList();
                        if (isPO != null && isPO.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isVerification.ToString().ToLower());
                        var isVeri = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Verification).ToString()).ToList();
                        if (isVeri != null && isVeri.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //listy.Add(licenseInfo.isRequest.ToString().ToLower());
                        var isRequest = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Request).ToString()).ToList();
                        if (isRequest != null && isRequest.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");


                        //listy.Add(licenseInfo.isDispatch.ToString().ToLower());
                        var isDisp = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Dispatch).ToString()).ToList();
                        if (isDisp != null && isDisp.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        var isAct = modules.Where(i => i == ((int)ClientLicence.LicenseModuleTypes.Activity).ToString()).ToList();
                        if (isAct != null && isAct.Count > 0)
                            listy.Add("true");
                        else
                            listy.Add("false");

                        //ClientLicence.InsertClientLicence(licenseInfo, dbConnection, dbConnectionAudit, true);
                        listy.Add(cInfo.Country);
                    }
                }
            }
            catch (Exception er)
            {
                listy.Clear();
                MIMSLog.MIMSLogSave("Devices", "getServerData", er, dbConnection, userinfo.SiteId);
            }
            return listy;
        }

        [WebMethod]
        public static string saveTZ(string id, string uname)
        {
            var json = string.Empty;
            var newAuthorize = new MyAuthorize();
            var retVal = newAuthorize.AuthorizeCore(HttpContext.Current);
            if (!retVal)
            {
                return "LOGOUT";
            }
            else
            {
                var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
                try
                {
                    var cInfo = CustomerInfo.GetCustomerInfoById(userinfo.CustomerInfoId, dbConnection);
                    if (cInfo != null)
                    {
                        var split = id.Split('(');
                        var cname = split[0];
                        var spli2 = split[1].Split(')');
                        var doubles = spli2[0];
                        var ctimezone = Convert.ToDouble(doubles.Split(':')[0]);

                        cInfo.Country = cname;
                        cInfo.TimeZone = ctimezone;
                        var latlng = ReverseGeocode.RetrieveFormatedGeo(cname);
                        if (latlng.Count > 0)
                        {
                            cInfo.Lati = latlng[0];
                            cInfo.Long = latlng[1];
                        }
                        CustomerInfo.InsertorUpdateCustomerInfo(cInfo, dbConnection);

                        return "SUCCESS";
                    }
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("Tasks.aspx", "deleteSystemType", ex, dbConnection, userinfo.SiteId);
                    return ex.Message;
                }
                return json;
            }
        }

        [WebMethod]
        public static List<ContractInfo> getContractListByCustomerId(int id, string uname)
        {
            var fullcollection = new List<ContractInfo>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                if (userinfo != null)
                {
                    var cust = new ContractInfo();
                    cust.Id = 0; cust.Id = 0;
                    cust.ContractRef = "Select Type";
                    cust.CreatedDate = CommonUtility.getDTNow();
                    cust.CreatedBy = userinfo.Username;
                    fullcollection.Add(cust);
                    fullcollection = ContractInfo.GetContractInfosByCustomerId(id, dbConnection);

                    if (userinfo.RoleId == (int)Role.CustomerUser)
                    {
                        if (userinfo.ContractLinkId > 0)
                        {
                            fullcollection = fullcollection.Where(i => i.Id == userinfo.ContractLinkId).ToList();
                        }
                    }

                    //fullcollection = fullcollection.Where(i => i.Active == true).ToList();
                    //fullcollection = fullcollection.Where(i => i.EndDate.Value.Date >= CommonUtility.getDTNow().Date).ToList();
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Tasks", "getContractListByCustomerId", ex, dbConnection, userinfo.SiteId);
            }
            //<tr role="row" class="odd clickable-row clickable-row-links"><td><span class="circle-point-container"><span class="circle-point circle-point-orange"></span></span></td><td class="sorting_1">Gecko</td><td>Firefox 1.0</td><td>Win 98+ / OSX.2+</td><td>'++'</td><td><a href="#"><i class="fa fa-eye mr-1x"></i>View</a></td></tr>
            return fullcollection;
        }

        [WebMethod]
        public static List<Project> getProjectListByCustomerId(int id, string uname)
        {
            var fullcollection = new List<Project>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                if (userinfo != null)
                {
                    if (id > 0)
                    {

                        fullcollection.AddRange(Project.GetProjectByCustomerId(id, dbConnection));

                        if (userinfo.RoleId == (int)Role.Regional)
                        {
                            var guser = Users.GetAllUsersByCustomerId(userinfo.CustomerInfoId, dbConnection);
                            guser = guser.Where(i => i.RoleId == (int)Role.CustomerSuperadmin).ToList();
                            if (guser.Count > 0)
                            {
                                var sincidenttypes = fullcollection.Where(i => i.CreatedBy != guser[0].Username).ToList();

                                var tincidenttypes = fullcollection.Where(i => i.UserId == userinfo.ID).ToList();

                                tincidenttypes.AddRange(sincidenttypes);

                                fullcollection = tincidenttypes;
                            }
                        }
                        else if (userinfo.RoleId == (int)Role.Director)
                        {
                            fullcollection = fullcollection.Where(i => i.UserId == userinfo.ID || i.CreatedBy == userinfo.Username || i.SiteId == userinfo.SiteId).ToList();
                        }
                        else if (userinfo.RoleId == (int)Role.Admin)
                        {
                            var slist = new List<Project>();
                            slist = fullcollection.Where(i => i.UserId == userinfo.ID || i.CreatedBy == userinfo.Username).ToList();
                            var manglist = DirectorManager.GetAllFullManagersByDirectorId(userinfo.ID, dbConnection);
                            foreach (var mangid in manglist)
                            {
                                var mlist = new List<Project>();
                                mlist = fullcollection.Where(i => i.CreatedBy == mangid.Username).ToList();
                                slist.AddRange(mlist);
                            }
                            var grouped = slist.GroupBy(item => item.Id);
                            slist = grouped.Select(grp => grp.OrderBy(item => item.Name).First()).ToList();
                            slist = slist.OrderBy(i => i.Name).ToList();
                            fullcollection = slist;


                        }
                        else if (userinfo.RoleId == (int)Role.Manager)
                        {
                            fullcollection = fullcollection.Where(i => i.UserId == userinfo.ID || i.CreatedBy == userinfo.Username).ToList();
                        }

                        //fullcollection = fullcollection.Where(i => i.EndDate.Value.Date >= CommonUtility.getDTNow().Date).ToList();
                        if (userinfo.RoleId == (int)Role.CustomerUser)
                        {
                            fullcollection = new List<Project>();
                        }

                    }
                    else
                    {

                        //fullcollection = Project.GetProjectByCustomerId(id, dbConnection);
                        if (userinfo.RoleId == (int)Role.Admin || userinfo.RoleId == (int)Role.Manager || userinfo.RoleId == (int)Role.Director)
                        {
                            fullcollection.AddRange(Project.GetAllProjectsBySiteId(userinfo.SiteId, dbConnection));
                        }
                        else if (userinfo.RoleId == (int)Role.Regional)
                        {
                            fullcollection.AddRange(Project.GetAllProjectsByLevel5(userinfo.ID, dbConnection));
                        }
                        else if (userinfo.RoleId == (int)Role.CustomerUser)
                        {
                            fullcollection.AddRange(Project.GetProjectByCustomerId(userinfo.CustomerLinkId, dbConnection));
                        }
                        else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                        {
                            fullcollection.AddRange(Project.GetAllProjectByCustomerInfoId(userinfo.CustomerInfoId, dbConnection));
                        }
                        else
                        {
                            fullcollection.AddRange(Project.GetAllProject(dbConnection));
                        }

                        if (userinfo.RoleId == (int)Role.Regional)
                        {
                            var guser = Users.GetAllUsersByCustomerId(userinfo.CustomerInfoId, dbConnection);
                            guser = guser.Where(i => i.RoleId == (int)Role.CustomerSuperadmin).ToList();
                            if (guser.Count > 0)
                            {
                                var sincidenttypes = fullcollection.Where(i => i.CreatedBy != guser[0].Username).ToList();

                                var tincidenttypes = fullcollection.Where(i => i.UserId == userinfo.ID).ToList();

                                tincidenttypes.AddRange(sincidenttypes);

                                fullcollection = tincidenttypes;
                            }
                        }
                        else if (userinfo.RoleId == (int)Role.Director)
                        {
                            fullcollection = fullcollection.Where(i => i.UserId == userinfo.ID || i.CreatedBy == userinfo.Username || i.SiteId == userinfo.SiteId).ToList();
                        }
                        else if (userinfo.RoleId == (int)Role.Admin)
                        {
                            var slist = new List<Project>();
                            slist = fullcollection.Where(i => i.UserId == userinfo.ID || i.CreatedBy == userinfo.Username).ToList();
                            var manglist = DirectorManager.GetAllFullManagersByDirectorId(userinfo.ID, dbConnection);
                            foreach (var mangid in manglist)
                            {
                                var mlist = new List<Project>();
                                mlist = fullcollection.Where(i => i.CreatedBy == mangid.Username).ToList();
                                slist.AddRange(mlist);
                            }
                            var grouped = slist.GroupBy(item => item.Id);
                            slist = grouped.Select(grp => grp.OrderBy(item => item.Name).First()).ToList();
                            slist = slist.OrderBy(i => i.Name).ToList();
                            fullcollection = slist;


                        }
                        else if (userinfo.RoleId == (int)Role.Manager)
                        {
                            fullcollection = fullcollection.Where(i => i.UserId == userinfo.ID || i.CreatedBy == userinfo.Username).ToList();
                        }



                        var grouped2 = fullcollection.GroupBy(item => item.Id);
                        fullcollection = grouped2.Select(grp => grp.OrderBy(item => item.Id).First()).ToList();
                        fullcollection = fullcollection.OrderBy(i => i.Name).ToList();


                        //fullcollection = fullcollection.Where(i => i.EndDate.Value.Date >= CommonUtility.getDTNow().Date).ToList();
                        if (userinfo.RoleId == (int)Role.CustomerUser)
                        {
                            fullcollection = new List<Project>();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Tasks", "getProjectListByCustomerId", ex, dbConnection, userinfo.SiteId);
            }
            //<tr role="row" class="odd clickable-row clickable-row-links"><td><span class="circle-point-container"><span class="circle-point circle-point-orange"></span></span></td><td class="sorting_1">Gecko</td><td>Firefox 1.0</td><td>Win 98+ / OSX.2+</td><td>'++'</td><td><a href="#"><i class="fa fa-eye mr-1x"></i>View</a></td></tr>
            return fullcollection;
        }

        [WebMethod]
        public static List<Customer> getCustomerByProjectId(int id, string uname)
        {
            var fullcollection = new List<Customer>();
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            try
            {

                if (userinfo != null)
                {
                    if (id > 0)
                    {

                        var cust = new Customer();
                        cust.Id = 0; cust.Id = 0;
                        cust.ClientName = "Select Account";
                        cust.CreatedDate = CommonUtility.getDTNow();
                        cust.CreatedBy = userinfo.Username;
                        fullcollection.Add(cust);
                        var pproj = Project.GetProjectById(id, dbConnection);
                        if (pproj != null)
                        {
                            var custt = Customer.GetCustomerById(pproj.CustomerId, dbConnection);
                            if (custt != null)
                            {
                                //if (custt.Status)
                                fullcollection.Add(custt);
                            }
                            else
                            {
                                //var cust = new Customer();
                                //cust.Id = 0; cust.Id = 0;
                                //cust.ClientName = "Select Customer";
                                //cust.CreatedDate = DateTime.Now;
                                //cust.CreatedBy = userinfo.Username;
                                //fullcollection.Add(cust); 
                            }
                        }
                        else
                        {
                            //var cust = new Customer();
                            //cust.Id = 0; cust.Id = 0;
                            //cust.ClientName = "Select Customer";
                            //cust.CreatedDate = DateTime.Now;
                            //cust.CreatedBy = userinfo.Username;
                            //fullcollection.Add(cust); 
                        }
                    }
                    else
                    {
                        var cust = new Customer();
                        cust.Id = 0; cust.Id = 0;
                        cust.ClientName = "Select Account";
                        cust.CreatedDate = CommonUtility.getDTNow();
                        cust.CreatedBy = userinfo.Username;
                        //fullcollection.Add(cust); 
                        var ffullcollection = new List<Customer>();
                        if (userinfo.RoleId == (int)Role.Admin || userinfo.RoleId == (int)Role.Manager || userinfo.RoleId == (int)Role.Director)
                        {
                            ffullcollection.AddRange(Customer.GetAllCustomersBySiteId(userinfo.SiteId, dbConnection));
                        }
                        else if (userinfo.RoleId == (int)Role.Regional)
                        {
                            //var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                            //foreach (var site in sites)
                            //{
                            ffullcollection.AddRange(Customer.GetAllCustomersByLevel5(userinfo.ID, dbConnection));
                            //}
                        }
                        else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                        {
                            ffullcollection.AddRange(Customer.GetAllCustomersByCustomerInfoId(userinfo.CustomerInfoId, dbConnection));
                        }
                        else if (userinfo.RoleId == (int)Role.CustomerUser)
                        {
                            ffullcollection.Add(Customer.GetCustomerById(userinfo.CustomerLinkId, dbConnection));
                        }
                        else
                        {
                            ffullcollection.AddRange(Customer.GetAllCustomers(dbConnection));
                        }

                        //ffullcollection = ffullcollection.Where(i => i.Status == true).ToList();
                        fullcollection.Add(cust);
                        fullcollection.AddRange(ffullcollection);
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Tasks", "getCustomerByProjectId", ex, dbConnection, userinfo.SiteId);
            }
            //<tr role="row" class="odd clickable-row clickable-row-links"><td><span class="circle-point-container"><span class="circle-point circle-point-orange"></span></span></td><td class="sorting_1">Gecko</td><td>Firefox 1.0</td><td>Win 98+ / OSX.2+</td><td>'++'</td><td><a href="#"><i class="fa fa-eye mr-1x"></i>View</a></td></tr>
            return fullcollection;
        }

        //protected void btnExport_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        //using (MemoryStream memoryStream = new MemoryStream())
        //        {
        //            string toDate = toHD.Value; //String.Format("{0}", Request.Form["toDate"]);
        //            string fromDate = fromHD.Value; // String.Format("{0}", Request.Form["fromDate"]);
        //            var status = lfstatus.Value;
        //            var contractId = contractID.Value;
        //            var projId = projectID.Value;
        //            var statusId = statusID.Value;
        //            var todate = Convert.ToDateTime(toDate);
        //            var fromdate = Convert.ToDateTime(fromDate);

        //            var userinfo = Users.GetUserByName(User.Identity.Name, dbConnection);

        //            //var itemsLostFound = ItemFound.GetAllItemFoundByDateRange(Convert.ToDateTime(fromDate).ToString(), Convert.ToDateTime(toDate).AddDays(1).ToString(), dbConnection);

        //            var itemsLostFound = UserTask.GetAllTaskByCId(userinfo.CustomerInfoId, dbConnection);

        //            if (!string.IsNullOrEmpty(status))
        //            {
        //                if (Convert.ToInt32(status) > 0)
        //                    itemsLostFound = itemsLostFound.Where(i => i.CustId == Convert.ToInt32(status)).ToList();
        //            }

        //            itemsLostFound = itemsLostFound.Where(i => Convert.ToDateTime(i.StartDate).AddHours(userinfo.TimeZone) >= Convert.ToDateTime(fromDate) && Convert.ToDateTime(i.StartDate).AddHours(userinfo.TimeZone) <= Convert.ToDateTime(toDate).AddDays(1)).ToList();

        //            ContractInfo customers;

        //            Project proj;

        //            if (!string.IsNullOrEmpty(contractId))
        //            {
        //                customers = ContractInfo.GetContractInfoById(Convert.ToInt32(contractId), dbConnection);
        //                if (customers != null)
        //                {
        //                    itemsLostFound = itemsLostFound.Where(i => i.ContractId == Convert.ToInt32(contractId)).ToList();
        //                }
        //            }

        //            if (!string.IsNullOrEmpty(projId))
        //            {
        //                proj = Project.GetProjectById(Convert.ToInt32(projId), dbConnection);
        //                if (proj != null)
        //                {
        //                    itemsLostFound = itemsLostFound.Where(i => i.ProjectId == Convert.ToInt32(projId)).ToList();
        //                }
        //            }

        //            if (!string.IsNullOrEmpty(statusId))
        //            {
        //                if (CommonUtility.isNumeric(statusId))
        //                {
        //                    if (Convert.ToInt32(statusId) > -1)
        //                    {
        //                        itemsLostFound = itemsLostFound.Where(i => i.Status == Convert.ToInt32(statusId)).ToList();
        //                    }
        //                }
        //            }

        //            var nameClient = string.Empty;
        //            if (itemsLostFound.Count > 0 && CommonUtility.isNumeric(status))
        //                nameClient = "for " + itemsLostFound[0].ClientName;
        //            if (CommonUtility.isNumeric(contractId))
        //                nameClient = nameClient + " contract " + itemsLostFound[0].ContractName;
        //            if (CommonUtility.isNumeric(projId) && CommonUtility.isNumeric(status))
        //                nameClient = nameClient + " project " + itemsLostFound[0].ProjectName;
        //            else if (CommonUtility.isNumeric(projId))
        //                nameClient = "for " + itemsLostFound[0].ProjectName;

        //            var loghistories = itemsLostFound;

        //            //var mimssettings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);

        //            //var preappPath = mimssettings.FilePath.Substring(0, mimssettings.FilePath.Length - 5);

        //            //  string appPath = preappPath + "\\PDFReports";
        //            //  string path = appPath;
        //            /*Logo Path*/

        //            string strLogoPath2 = CommonUtility.PhysicalPath("~/Images/custom-images") + "\\site-login-logo.png";

        //            iTextSharp.text.Document doc = new iTextSharp.text.Document(PageSize.LETTER, 25F, 25F, 50F, 25F);
        //            var fileName = DateTime.Now.Day.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Year.ToString() + "-" + DateTime.Now.Hour.ToString() + "-" + DateTime.Now.Minute.ToString() + "-" + DateTime.Now.Second.ToString() + ".pdf";
        //            // path = path + "\\" + fileName;

        //            PdfWriter writer = PdfWriter.GetInstance(doc, Response.OutputStream);//PdfWriter.GetInstance(doc, new FileStream(path, FileMode.Create));
        //            writer.PageEvent = new ITextEvents();
        //            doc.Open();

        //            //iTextSharp.text.Image img1 = iTextSharp.text.Image.GetInstance(strLogoPath2);
        //            //img1.SetAbsolutePosition(doc.GetLeft(15), doc.GetTop(-15));
        //            //img1.ScalePercent(50f);
        //            //img1.Alignment = iTextSharp.text.Image.ALIGN_LEFT;
        //            //doc.Add(img1);

        //            iTextSharp.text.Image img1 = iTextSharp.text.Image.GetInstance(strLogoPath2);
        //            img1.SetAbsolutePosition(doc.GetLeft(10), doc.GetTop(10));
        //            img1.ScalePercent(50f);
        //            img1.Alignment = iTextSharp.text.Image.ALIGN_LEFT;
        //            doc.Add(img1);

        //            BaseFont f_cn = BaseFont.CreateFont(Environment.GetFolderPath(Environment.SpecialFolder.Fonts) + "\\verdana.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
        //            if (string.IsNullOrEmpty(userinfo.SiteName))
        //                userinfo.SiteName = "N/A";
        //            PdfContentByte cb = writer.DirectContent;
        //            cb.BeginText();
        //            cb.SetFontAndSize(f_cn, 14);
        //            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "MIMS GENERATED REPORT", 320, 700, 0);
        //            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Business Unit: " + userinfo.SiteName, 40, 670, 0);
        //            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Created Date: " + CommonUtility.getDTNow().AddHours(userinfo.TimeZone).ToString(), 40, 655, 0);

        //            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Report Name:  Tasks " + nameClient, 40, 640, 0);
        //            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Generated By: " + userinfo.CustomerUName, 40, 625, 0);
        //            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Period: " + fromdate.ToShortDateString() + " To " + todate.ToShortDateString(), 40, 610, 0);

        //            cb.EndText();
        //            iTextSharp.text.Paragraph paragraphTable = new iTextSharp.text.Paragraph();
        //            paragraphTable.SpacingBefore = 150f;

        //            iTextSharp.text.Paragraph smallparagraphTable = new iTextSharp.text.Paragraph();
        //            smallparagraphTable.SpacingBefore = 75f;

        //            PdfPTable table = new PdfPTable(4);
        //            table.WidthPercentage = 100;
        //            Font tentimes = new Font(f_cn, 10, Font.NORMAL, Color.BLACK);
        //            Font btentimes = new Font(f_cn, 11, Font.NORMAL, Color.BLACK);

        //            Font headerf = new Font(f_cn, 9, Font.BOLD | Font.UNDERLINE, Color.BLACK);

        //            PdfPTable consumtiontable = new PdfPTable(3);
        //            consumtiontable.WidthPercentage = 100;

        //            consumtiontable.AddCell(new PdfPCell(new Phrase("Consoumed", tentimes)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER, PaddingBottom = 10 });
        //            consumtiontable.AddCell(new PdfPCell(new Phrase("Remaining", tentimes)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER });
        //            consumtiontable.AddCell(new PdfPCell(new Phrase("Total", tentimes)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER });


        //            table.AddCell(new PdfPCell(new Phrase("Pending", tentimes)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER, PaddingBottom = 10 });
        //            table.AddCell(new PdfPCell(new Phrase("InProgress", tentimes)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER });
        //            table.AddCell(new PdfPCell(new Phrase("Completed", tentimes)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER });
        //            table.AddCell(new PdfPCell(new Phrase("Total", tentimes)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER });

        //            var tPending = loghistories.Where(i => i.StatusDescription == "Pending").ToList();
        //            var tInProgress = loghistories.Where(i => i.StatusDescription == "InProgress").ToList();
        //            var tCompleted = loghistories.Where(i => i.StatusDescription == "Completed" || i.StatusDescription == "Accepted").ToList();


        //            var headerCell = new PdfPCell(new Phrase(tPending.Count.ToString(), tentimes));
        //            var headerCell2 = new PdfPCell(new Phrase(tInProgress.Count.ToString(), tentimes));
        //            var headerCell3 = new PdfPCell(new Phrase(tCompleted.Count.ToString(), tentimes));
        //            var headerCell6 = new PdfPCell(new Phrase(loghistories.Count.ToString(), tentimes));


        //            double[] yValues = new double[3]; //{ rG.Count, aPose.Count, dG.Count, dPose.Count, fG.Count };
        //            string[] xValues = new string[3];//{ "Returned", "Disposed", "Charity", "Authorities", "Found" };

        //            double[] yxValues = new double[2]; //{ rG.Count, aPose.Count, dG.Count, dPose.Count, fG.Count };
        //            string[] yyValues = new string[2];

        //            if (tPending.Count > 0)
        //            {
        //                yValues[0] = tPending.Count;
        //                xValues[0] = "Pending";
        //            }
        //            else
        //            {
        //                yValues[0] = 0;
        //                xValues[0] = "Pending";
        //            }
        //            if (tInProgress.Count > 0)
        //            {
        //                yValues[1] = tInProgress.Count;
        //                xValues[1] = "InProgress";
        //            }
        //            else
        //            {
        //                yValues[1] = 0;
        //                xValues[1] = "InProgress";
        //            }
        //            if (tCompleted.Count > 0)
        //            {
        //                yValues[2] = tCompleted.Count;
        //                xValues[2] = "Completed";
        //            }
        //            else
        //            {
        //                yValues[2] = 0;
        //                xValues[2] = "Completed";
        //            }

        //            Chart1.Series["Series1"].Points.DataBindXY(xValues, yValues);

        //            var contracthours = 0;
        //            var rconsumed = 0;
        //            var rremaining = 0;
        //            if (!string.IsNullOrEmpty(contractId))
        //            {
        //                customers = ContractInfo.GetContractInfoById(Convert.ToInt32(contractId), dbConnection);

        //                if (customers != null)
        //                {
        //                    tCompleted = tCompleted.Where(i => i.ContractId == customers.Id).ToList();

        //                    if (customers.ContractType == (int)ContractInfo.ContractTypes.PPM)
        //                    {
        //                        contracthours = customers.TotalPPMHour;

        //                        if (customers.SRTotal > 0)
        //                            contracthours = customers.SRTotal;
        //                    }
        //                    else
        //                    {
        //                        contracthours = customers.TotalPPMHour;
        //                    }

        //                    if (tCompleted.Count > 0)
        //                    {

        //                        var totaltime = 0.0;

        //                        var contractminutes = 0;



        //                        foreach (var gtask in tCompleted)
        //                        {
        //                            if (gtask.ActualStartDate != null && gtask.ActualEndDate != null)
        //                            {
        //                                var span = gtask.ActualEndDate.Value.Subtract(gtask.ActualStartDate.Value);
        //                                totaltime = totaltime + span.TotalMinutes;
        //                            }
        //                        }


        //                        contractminutes = contracthours * 60;

        //                        var remainingHours = contractminutes - totaltime;
        //                        remainingHours = contractminutes - 60;
        //                        var gConsumed = (int)Math.Round((float)totaltime / (float)60);
        //                        var gRem = (int)Math.Round((float)remainingHours / (float)60);

        //                        if (totaltime > 60)
        //                        {

        //                            rconsumed = gConsumed;
        //                            rremaining = gRem;
        //                        }
        //                        else
        //                        {
        //                            rconsumed = 1;
        //                            rremaining = gRem;
        //                        }
        //                    }
        //                    if (rconsumed > 0)
        //                    {
        //                        yxValues[0] = rconsumed;
        //                        yyValues[0] = "Consumed";
        //                    }
        //                    else
        //                    {
        //                        yxValues[0] = 0;
        //                        yyValues[0] = "Consumed";
        //                    }
        //                    if (rremaining > 0)
        //                    {
        //                        yxValues[1] = rremaining;
        //                        yyValues[1] = "Remaining";
        //                    }
        //                    else
        //                    {
        //                        yxValues[1] = contracthours;
        //                        yyValues[1] = "Remaining";
        //                    }

        //                    Chart2.Series["Series1"].Points.DataBindXY(yyValues, yxValues);

        //                    var headerCellC = new PdfPCell(new Phrase(rconsumed.ToString(), tentimes));
        //                    var headerCellR = new PdfPCell(new Phrase(rremaining.ToString(), tentimes));
        //                    var headerCellT = new PdfPCell(new Phrase(contracthours.ToString(), tentimes));

        //                    consumtiontable.AddCell(headerCellC);
        //                    consumtiontable.AddCell(headerCellR);
        //                    consumtiontable.AddCell(headerCellT);
        //                }
        //            }



        //            //Chart1.Series[0].Color = System.Drawing.Color.Red;
        //            Chart1.Series[0].Points[0].Color = System.Drawing.ColorTranslator.FromHtml("#c8234a");
        //            Chart1.Series[0].Points[1].Color = System.Drawing.ColorTranslator.FromHtml("#f2c400");                      //#f2c40
        //            Chart1.Series[0].Points[2].Color = System.Drawing.ColorTranslator.FromHtml("#3ebb64");  //3ebb64
        //            table.AddCell(headerCell);
        //            table.AddCell(headerCell2);
        //            table.AddCell(headerCell3);
        //            table.AddCell(headerCell6);

        //            iTextSharp.text.Paragraph paragraphTable2 = new iTextSharp.text.Paragraph();
        //            paragraphTable2.SpacingBefore = 150f;
        //            PdfPTable table2 = new PdfPTable(7);
        //            table2.WidthPercentage = 100;

        //            table2.AddCell(new PdfPCell(new Phrase("Id", tentimes)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER, PaddingBottom = 10 });
        //            table2.AddCell(new PdfPCell(new Phrase("Customer", tentimes)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER });
        //            table2.AddCell(new PdfPCell(new Phrase("Name", tentimes)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER });
        //            table2.AddCell(new PdfPCell(new Phrase("Type", tentimes)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER });
        //            table2.AddCell(new PdfPCell(new Phrase("Start Time", tentimes)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER });
        //            table2.AddCell(new PdfPCell(new Phrase("End Time", tentimes)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER });
        //            table2.AddCell(new PdfPCell(new Phrase("Status", tentimes)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER });

        //            var newOrder = new List<UserTask>();

        //            newOrder.AddRange(loghistories.Where(i => i.StatusDescription == "Completed" || i.StatusDescription == "Accepted").ToList());
        //            newOrder.AddRange(loghistories.Where(i => i.StatusDescription == "InProgress").ToList());
        //            newOrder.AddRange(loghistories.Where(i => i.StatusDescription == "Pending").ToList());
        //            foreach (var device in newOrder)
        //            {

        //                //if (device.RecevieTime.Value.Ticks > fromdate.Ticks && device.RecevieTime.Value.Ticks < todate.Ticks)
        //                {
        //                    var xheaderCell = new PdfPCell(new Phrase(device.NewCustomerTaskId, tentimes));
        //                    var xheaderCell2 = new PdfPCell(new Phrase(device.ClientName, tentimes));
        //                    var xheaderCell3 = new PdfPCell(new Phrase(device.Name, tentimes));
        //                    var xheaderCell4 = new PdfPCell(new Phrase(device.TaskTypeName, tentimes));
        //                    var sd = device.ActualStartDate.ToString();
        //                    var ed = device.ActualEndDate.ToString();
        //                    if (device.StatusDescription == "Pending")
        //                    {
        //                        sd = "";
        //                        ed = "";
        //                    }
        //                    else if (device.StatusDescription == "InProgress")
        //                    {
        //                        ed = "";
        //                    }

        //                    if (device.StatusDescription == "Accepted")
        //                    {
        //                        device.StatusDescription = "Completed";
        //                    }

        //                    var xheaderCell5 = new PdfPCell(new Phrase(sd, tentimes));
        //                    var xheaderCell6 = new PdfPCell(new Phrase(ed, tentimes));
        //                    var xheaderCell7 = new PdfPCell(new Phrase(device.StatusDescription, tentimes));

        //                    table2.AddCell(xheaderCell);
        //                    table2.AddCell(xheaderCell2);
        //                    table2.AddCell(xheaderCell3);
        //                    table2.AddCell(xheaderCell4);
        //                    table2.AddCell(xheaderCell5);
        //                    table2.AddCell(xheaderCell6);
        //                    table2.AddCell(xheaderCell7);
        //                }
        //            }
        //            doc.Add(paragraphTable);


        //            if (!string.IsNullOrEmpty(contractId))
        //            {
        //                if (Convert.ToInt32(contractId) > 0)
        //                {

        //                    PdfPTable tableV = new PdfPTable(7);
        //                    tableV.WidthPercentage = 100;
        //                    tableV.AddCell(new PdfPCell(new Phrase("Pending", tentimes)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER, PaddingBottom = 10 });
        //                    tableV.AddCell(new PdfPCell(new Phrase("Inprogress", tentimes)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER });
        //                    tableV.AddCell(new PdfPCell(new Phrase("Completed", tentimes)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER });
        //                    tableV.AddCell(new PdfPCell(new Phrase("Total Tasks", tentimes)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER });


        //                    tableV.AddCell(new PdfPCell(new Phrase("Consumed", tentimes)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER, PaddingBottom = 10 });
        //                    tableV.AddCell(new PdfPCell(new Phrase("Remaining", tentimes)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER });
        //                    tableV.AddCell(new PdfPCell(new Phrase("Total Hours", tentimes)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER });

        //                    var headerCellP = new PdfPCell(new Phrase(tPending.Count.ToString(), tentimes));
        //                    var headerCellI = new PdfPCell(new Phrase(tInProgress.Count.ToString(), tentimes));
        //                    var headerCellC = new PdfPCell(new Phrase(tCompleted.Count.ToString(), tentimes));
        //                    var headerCellTT = new PdfPCell(new Phrase(loghistories.Count.ToString(), tentimes));

        //                    var headerCellCC = new PdfPCell(new Phrase(rconsumed.ToString(), tentimes));
        //                    var headerCellRC = new PdfPCell(new Phrase(rremaining.ToString(), tentimes));
        //                    var headerCellTC = new PdfPCell(new Phrase(contracthours.ToString(), tentimes));

        //                    tableV.AddCell(headerCellP);
        //                    tableV.AddCell(headerCellI);
        //                    tableV.AddCell(headerCellC);
        //                    tableV.AddCell(headerCellTT);
        //                    tableV.AddCell(headerCellCC);
        //                    tableV.AddCell(headerCellRC);
        //                    tableV.AddCell(headerCellTC);

        //                    doc.Add(tableV);

        //                    //doc.Add(table);
        //                    //doc.Add(consumtiontable);

        //                    PdfPTable tableCH = new PdfPTable(2);
        //                    tableCH.WidthPercentage = 100;

        //                    using (MemoryStream memoryStream = new MemoryStream())
        //                    {
        //                        Chart1.SaveImage(memoryStream, ChartImageFormat.Png);
        //                        iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance(memoryStream.GetBuffer());
        //                        img.ScalePercent(75f);
        //                        img.Alignment = iTextSharp.text.Image.ALIGN_CENTER;
        //                        tableCH.AddCell(img);
        //                    }

        //                    using (MemoryStream memoryStream2 = new MemoryStream())
        //                    {
        //                        Chart2.SaveImage(memoryStream2, ChartImageFormat.Png);
        //                        iTextSharp.text.Image img2 = iTextSharp.text.Image.GetInstance(memoryStream2.GetBuffer());
        //                        img2.ScalePercent(75f);
        //                        img2.Alignment = iTextSharp.text.Image.ALIGN_CENTER;
        //                        tableCH.AddCell(img2);
        //                    }

        //                    doc.Add(tableCH);

        //                    doc.Add(smallparagraphTable);
        //                }
        //                else
        //                {
        //                    doc.Add(table);

        //                    using (MemoryStream memoryStream = new MemoryStream())
        //                    {
        //                        Chart1.SaveImage(memoryStream, ChartImageFormat.Png);
        //                        iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance(memoryStream.GetBuffer());
        //                        img.ScalePercent(75f);
        //                        img.Alignment = iTextSharp.text.Image.ALIGN_CENTER;
        //                        doc.Add(img);
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                doc.Add(table);

        //                using (MemoryStream memoryStream = new MemoryStream())
        //                {
        //                    Chart1.SaveImage(memoryStream, ChartImageFormat.Png);
        //                    iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance(memoryStream.GetBuffer());
        //                    img.ScalePercent(75f);
        //                    img.Alignment = iTextSharp.text.Image.ALIGN_CENTER;
        //                    doc.Add(img);
        //                }
        //            }
        //            doc.Add(table2);


        //            doc.Close();

        //            Response.ContentType = "application/pdf";
        //            Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
        //            Response.Cache.SetCacheability(HttpCacheability.NoCache);
        //            Response.Write(doc);
        //            Response.End();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MIMSLog.MIMSLogSave("Reports", "btnExport_Click", ex, dbConnection);
        //    }
        //}

        //protected void btnExport2_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        //using (MemoryStream memoryStream = new MemoryStream())
        //        {
        //            string toDate = toHD.Value; //String.Format("{0}", Request.Form["toDate"]);
        //            string fromDate = fromHD.Value; // String.Format("{0}", Request.Form["fromDate"]);
        //            var status = lfstatus2.Value;
        //            var ttype = type1.Value;
        //            var compare = comparison1.Value;
        //            var year1 = yearly1.Value;
        //            var year2 = yearly2.Value;
        //            var quater1 = qt1.Value;
        //            var quarter2 = qt2.Value;

        //            if (compare == "Yearly")
        //            {
        //                quater1 = "";
        //                quarter2 = "";
        //            }
        //            var todate = Convert.ToDateTime(toDate);
        //            var fromdate = Convert.ToDateTime(fromDate);

        //            var userinfo = Users.GetUserByName(User.Identity.Name, dbConnection);

        //            //var itemsLostFound = ItemFound.GetAllItemFoundByDateRange(Convert.ToDateTime(fromDate).ToString(), Convert.ToDateTime(toDate).AddDays(1).ToString(), dbConnection);

        //            var itemsLostFound = ItemFound.GetAllItemFound(dbConnection);


        //            if (userinfo.RoleId != (int)Role.SuperAdmin && userinfo.RoleId != (int)Role.ChiefOfficer)
        //            {
        //                if (userinfo.RoleId == (int)Role.Manager || userinfo.RoleId == (int)Role.Director || userinfo.RoleId == (int)Role.Admin)
        //                    itemsLostFound = itemsLostFound.Where(i => i.SiteId == userinfo.SiteId).ToList();
        //                else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
        //                {
        //                    itemsLostFound = itemsLostFound.Where(i => i.CustomerId == userinfo.CustomerInfoId).ToList();
        //                }
        //                else
        //                {
        //                    var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
        //                    var tempItemstLostFound = new List<ItemFound>();
        //                    foreach (var site in sites)
        //                        tempItemstLostFound.AddRange(itemsLostFound.Where(i => i.SiteId == site.SiteId).ToList());

        //                    tempItemstLostFound.AddRange(itemsLostFound.Where(i => i.SiteId == userinfo.SiteId).ToList());
        //                    itemsLostFound = tempItemstLostFound;
        //                }
        //            }

        //            if (status != "Comparison")
        //            {
        //                itemsLostFound = itemsLostFound.Where(i => Convert.ToDateTime(i.DateFound) > Convert.ToDateTime(fromDate) && Convert.ToDateTime(i.DateFound) < Convert.ToDateTime(toDate).AddDays(1)).ToList();
        //            }
        //            var grouped = itemsLostFound.GroupBy(item => item.Id);
        //            itemsLostFound = grouped.Select(grp => grp.OrderBy(item => item.Id).First()).ToList();
        //            itemsLostFound = itemsLostFound.OrderBy(i => i.Status).ToList();

        //            var loghistories = itemsLostFound;
        //            var comparisonlist = itemsLostFound;
        //            var range1 = new List<ItemFound>();
        //            var range2 = new List<ItemFound>();
        //            var var1 = new List<ItemFound>();
        //            var var2 = new List<ItemFound>();
        //            if (status == "Comparison")
        //            {

        //                //year1 quater1 compare ttype
        //                var dt1 = DateTime.Now;
        //                var dt2 = DateTime.Now;
        //                if (compare == "Yearly")
        //                {
        //                    dt1 = new DateTime(Convert.ToInt32(year1), 1, 1);
        //                    dt2 = new DateTime(Convert.ToInt32(year2), 1, 1);

        //                    range1 = comparisonlist.Where(i => Convert.ToDateTime(i.DateFound) > dt1 && Convert.ToDateTime(i.DateFound) < dt1.AddMonths(12)).ToList();
        //                    range2 = comparisonlist.Where(i => Convert.ToDateTime(i.DateFound) > dt2 && Convert.ToDateTime(i.DateFound) < dt2.AddMonths(12)).ToList();


        //                    if (ttype == "Valuable/Non-Valuable")
        //                    {
        //                        var1 = range1.Where(i => i.Type == "Valuable" || i.Type == "Valuables").ToList();
        //                        var2 = range2.Where(i => i.Type == "Valuable" || i.Type == "Valuables").ToList();


        //                    }
        //                    else if (ttype == "Delivered/Not-Delivered")
        //                    {
        //                        var1 = range1.Where(i => i.Status == "Found").ToList();
        //                        var2 = range2.Where(i => i.Status == "Found").ToList();
        //                    }
        //                }
        //                else if (compare == "Quarterly")
        //                {
        //                    if (quater1 == "Q1 (Jan-Mar)")
        //                    {
        //                        dt1 = new DateTime(Convert.ToInt32(year1), 1, 1);
        //                    }
        //                    else if (quater1 == "Q2 (Apr-Jun)")
        //                    {
        //                        dt1 = new DateTime(Convert.ToInt32(year1), 4, 1);
        //                    }
        //                    else if (quater1 == "Q3 (Jul-Sep)")
        //                    {
        //                        dt1 = new DateTime(Convert.ToInt32(year1), 7, 1);
        //                    }
        //                    else if (quater1 == "Q4 (Oct-Dec)")
        //                    {
        //                        dt1 = new DateTime(Convert.ToInt32(year1), 10, 1);
        //                    }

        //                    if (quarter2 == "Q1 (Jan-Mar)")
        //                    {
        //                        dt2 = new DateTime(Convert.ToInt32(year2), 1, 1);
        //                    }
        //                    else if (quarter2 == "Q2 (Apr-Jun)")
        //                    {
        //                        dt2 = new DateTime(Convert.ToInt32(year2), 4, 1);
        //                    }
        //                    else if (quarter2 == "Q3 (Jul-Sep)")
        //                    {
        //                        dt2 = new DateTime(Convert.ToInt32(year2), 7, 1);
        //                    }
        //                    else if (quarter2 == "Q4 (Oct-Dec)")
        //                    {
        //                        dt2 = new DateTime(Convert.ToInt32(year2), 10, 1);
        //                    }
        //                    range1 = comparisonlist.Where(i => Convert.ToDateTime(i.DateFound) > dt1 && Convert.ToDateTime(i.DateFound) < dt1.AddMonths(3)).ToList();
        //                    range2 = comparisonlist.Where(i => Convert.ToDateTime(i.DateFound) > dt2 && Convert.ToDateTime(i.DateFound) < dt2.AddMonths(3)).ToList();

        //                    if (ttype == "Valuable/Non-Valuable")
        //                    {
        //                        var1 = range1.Where(i => i.Type == "Valuable" || i.Type == "Valuables").ToList();
        //                        var2 = range2.Where(i => i.Type == "Valuable" || i.Type == "Valuables").ToList();
        //                    }
        //                    else if (ttype == "Delivered/Not-Delivered")
        //                    {
        //                        var1 = range1.Where(i => i.Status == "Found").ToList();
        //                        var2 = range2.Where(i => i.Status == "Found").ToList();
        //                    }
        //                }
        //            }

        //            var mimssettings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);

        //            var preappPath = mimssettings.FilePath.Substring(0, mimssettings.FilePath.Length - 5);

        //            string appPath = preappPath + "\\PDFReports";
        //            string path = appPath;
        //            /*Logo Path*/

        //            string strLogoPath2 = CommonUtility.PhysicalPath("~/Images/custom-images") + "\\site-login-logo.png";

        //            iTextSharp.text.Document doc = new iTextSharp.text.Document(PageSize.LETTER, 25F, 25F, 50F, 25F);

        //            doc.SetMargins(25, 25, 65, 35);

        //            var fileName = DateTime.Now.Day.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Year.ToString() + "-" + DateTime.Now.Hour.ToString() + "-" + DateTime.Now.Minute.ToString() + "-" + DateTime.Now.Second.ToString() + ".pdf";
        //            path = path + "\\" + fileName;

        //            PdfWriter writer = PdfWriter.GetInstance(doc, Response.OutputStream);//PdfWriter.GetInstance(doc, new FileStream(path, FileMode.Create));
        //            writer.PageEvent = new ITextEvents();
        //            doc.Open();



        //            iTextSharp.text.Image img1 = iTextSharp.text.Image.GetInstance(strLogoPath2);
        //            img1.SetAbsolutePosition(doc.GetLeft(15), doc.GetTop(-15));
        //            img1.ScalePercent(50f);
        //            img1.Alignment = iTextSharp.text.Image.ALIGN_LEFT;
        //            doc.Add(img1);


        //            BaseFont f_cn = BaseFont.CreateFont(Environment.GetFolderPath(Environment.SpecialFolder.Fonts) + "\\verdana.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
        //            if (string.IsNullOrEmpty(userinfo.SiteName))
        //                userinfo.SiteName = "N/A";
        //            PdfContentByte cb = writer.DirectContent;
        //            cb.BeginText();
        //            cb.SetFontAndSize(f_cn, 14);
        //            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "MIMS GENERATED REPORT", 320, 700, 0);
        //            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Business Unit: " + userinfo.SiteName, 40, 670, 0);
        //            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Created Date: " + DateTime.Now.ToString(), 40, 655, 0);
        //            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Report Name:  Lost and Found " + status, 40, 640, 0);
        //            //if (userinfo.RoleId != (int)Role.SuperAdmin)
        //            //    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Generated By: " + userinfo.FirstName + " " + userinfo.LastName, 40, 625, 0);
        //            //else
        //            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Generated By: " + HttpContext.Current.User.Identity.Name, 40, 625, 0);

        //            if (status != "Comparison")
        //            {
        //                //if (fromdate > DateTime.MinValue & todate > DateTime.MinValue)
        //                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Period: " + fromdate.ToShortDateString() + " To " + todate.ToShortDateString(), 40, 610, 0);

        //            }
        //            else
        //            {
        //                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Period: " + year1 + " " + quater1 + " To " + year2 + " " + quarter2, 40, 610, 0);
        //            }
        //            //else
        //            //    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Period: All", 40, 610, 0);

        //            cb.EndText();
        //            iTextSharp.text.Paragraph paragraphTable = new iTextSharp.text.Paragraph();
        //            paragraphTable.SpacingBefore = 150f;

        //            iTextSharp.text.Paragraph smallparagraphTable = new iTextSharp.text.Paragraph();
        //            smallparagraphTable.SpacingBefore = 75f;

        //            iTextSharp.text.Paragraph xsmallparagraphTable = new iTextSharp.text.Paragraph();
        //            xsmallparagraphTable.SpacingBefore = 20f;

        //            PdfPTable table = new PdfPTable(6);
        //            table.WidthPercentage = 100;
        //            Font tentimes = new Font(f_cn, 10, Font.NORMAL, Color.BLACK);
        //            Font btentimes = new Font(f_cn, 11, Font.NORMAL, Color.BLACK);

        //            Font headerf = new Font(f_cn, 9, Font.NORMAL, Color.BLACK);

        //            table.AddCell(new PdfPCell(new Phrase("Returned", headerf)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER, PaddingBottom = 10 });
        //            table.AddCell(new PdfPCell(new Phrase("Dispose/Destroyed", headerf)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER });
        //            table.AddCell(new PdfPCell(new Phrase("Donate/Charity", headerf)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER });
        //            table.AddCell(new PdfPCell(new Phrase("Dispose/Authorities", headerf)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER });
        //            table.AddCell(new PdfPCell(new Phrase("Found/Unclaimed", headerf)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER });
        //            table.AddCell(new PdfPCell(new Phrase("Total", headerf)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER });

        //            var rG = loghistories.Where(i => i.Status == "Return").ToList();
        //            var fG = loghistories.Where(i => i.Status == "Found").ToList();
        //            var dG = loghistories.Where(i => i.Status == "Donate").ToList();
        //            var disP = loghistories.Where(i => i.Status == "Dispose").ToList();

        //            var valuables = loghistories.Where(i => i.Type == "Valuable" || i.Type == "Valuables").ToList();
        //            var retvaluables = new List<ItemFound>();
        //            if (status == "Valuable Returned/Not-Returned")
        //            {
        //                retvaluables = valuables.Where(i => i.Status == "Found").ToList();
        //            }
        //            else if (status == "Charity Returned/Not-Returned")
        //            {
        //                valuables = loghistories.Where(i => i.Type == "Charity").ToList();
        //                retvaluables = valuables.Where(i => i.Status == "Found").ToList();
        //            }

        //            var itemDs = ItemDispose.GetAllItemDispose(dbConnection);
        //            var collection = new List<ItemDispose>();
        //            var aPose = new List<ItemDispose>();
        //            var dPose = new List<ItemDispose>();
        //            foreach (var dis in disP)
        //            {
        //                var item = itemDs.Where(i => i.ItemReference == dis.Reference).ToList();
        //                collection.AddRange(item);
        //            }
        //            if (collection.Count > 0)
        //            {
        //                aPose = collection.Where(i => i.DisposedTo == "Authorities").ToList();

        //                dPose = collection.Where(i => i.DisposedTo == "Destroyed").ToList();
        //            }

        //            var headerCell = new PdfPCell(new Phrase(rG.Count.ToString(), tentimes));
        //            var headerCell2 = new PdfPCell(new Phrase(dPose.Count.ToString(), tentimes));
        //            var headerCell3 = new PdfPCell(new Phrase(dG.Count.ToString(), tentimes));
        //            var headerCell4 = new PdfPCell(new Phrase(aPose.Count.ToString(), tentimes));
        //            var headerCell5 = new PdfPCell(new Phrase(fG.Count.ToString(), tentimes));
        //            var headerCell6 = new PdfPCell(new Phrase(loghistories.Count.ToString(), tentimes));


        //            double[] yValues = new double[5]; //{ rG.Count, aPose.Count, dG.Count, dPose.Count, fG.Count };
        //            string[] xValues = new string[5];//{ "Returned", "Disposed", "Charity", "Authorities", "Found" };

        //            double[] vyValues = new double[2]; //{ rG.Count, aPose.Count, dG.Count, dPose.Count, fG.Count };
        //            string[] vxValues = new string[2];

        //            double[] yyValues = new double[2]; //{ rG.Count, aPose.Count, dG.Count, dPose.Count, fG.Count };
        //            string[] yxValues = new string[2];

        //            if (status == "Valuable/Non-Valuable")
        //            {
        //                if (valuables.Count > 0)
        //                {
        //                    vyValues[0] = valuables.Count;
        //                    vxValues[0] = "Valuable";

        //                    vyValues[1] = loghistories.Count - valuables.Count;
        //                    vxValues[1] = "Non-Valuable";
        //                }
        //                else
        //                {
        //                    vyValues[0] = 0;
        //                    vxValues[0] = "Valuable";

        //                    vyValues[1] = loghistories.Count;
        //                    vxValues[1] = "Non-Valuable";
        //                }
        //            }
        //            else if (status == "Valuable Returned/Not-Returned")
        //            {
        //                if (retvaluables.Count > 0)
        //                {
        //                    vyValues[0] = retvaluables.Count;
        //                    vxValues[0] = "Not-Returned";

        //                    vyValues[1] = valuables.Count - retvaluables.Count;
        //                    vxValues[1] = "Returned";
        //                }
        //                else
        //                {
        //                    vyValues[0] = 0;
        //                    vxValues[0] = "Not-Returned";

        //                    vyValues[1] = valuables.Count;
        //                    vxValues[1] = "Returned";
        //                }
        //            }
        //            else if (status == "Charity Returned/Not-Returned")
        //            {
        //                if (retvaluables.Count > 0)
        //                {
        //                    vyValues[0] = retvaluables.Count;
        //                    vxValues[0] = "Not-Returned";

        //                    vyValues[1] = valuables.Count - retvaluables.Count;
        //                    vxValues[1] = "Returned";
        //                }
        //                else
        //                {
        //                    vyValues[0] = 0;
        //                    vxValues[0] = "Not-Returned";

        //                    vyValues[1] = valuables.Count;
        //                    vxValues[1] = "Returned";
        //                }
        //            }
        //            else if (status == "Alcohol Disposed")
        //            {

        //            }
        //            else if (status == "Delivered/Not-Delivered")
        //            {
        //                valuables = loghistories.Where(i => i.Status == "Found").ToList();

        //                if (valuables.Count > 0)
        //                {
        //                    vyValues[0] = valuables.Count;
        //                    vxValues[0] = "Not-Delivered";

        //                    vyValues[1] = loghistories.Count - valuables.Count;
        //                    vxValues[1] = "Delivered";
        //                }
        //                else
        //                {
        //                    vyValues[0] = 0;
        //                    vxValues[0] = "Not-Delivered";

        //                    vyValues[1] = loghistories.Count;
        //                    vxValues[1] = "Delivered";
        //                }
        //            }
        //            else if (status == "All")
        //            {
        //                if (dPose.Count > 0)
        //                {
        //                    yValues[3] = dPose.Count;
        //                    xValues[3] = "Destroyed";
        //                }
        //                else
        //                {
        //                    yValues[3] = 0;
        //                    xValues[3] = "Destroyed";
        //                }

        //                if (fG.Count > 0)
        //                {
        //                    yValues[4] = fG.Count;
        //                    xValues[4] = "Found";
        //                }
        //                else
        //                {
        //                    yValues[4] = 0;
        //                    xValues[4] = "Found";
        //                }

        //                if (rG.Count > 0)
        //                {
        //                    yValues[0] = rG.Count;
        //                    xValues[0] = "Returned";
        //                }
        //                else
        //                {
        //                    yValues[0] = 0;
        //                    xValues[0] = "Returned";
        //                }
        //                if (aPose.Count > 0)
        //                {
        //                    yValues[1] = aPose.Count;
        //                    xValues[1] = "Authorities";
        //                }
        //                else
        //                {
        //                    yValues[1] = 0;
        //                    xValues[1] = "Authorities";
        //                }
        //                if (dG.Count > 0)
        //                {
        //                    yValues[2] = dG.Count;
        //                    xValues[2] = "Charity";
        //                }
        //                else
        //                {
        //                    yValues[2] = 0;
        //                    xValues[2] = "Charity";
        //                }
        //            }
        //            else if (status == "Comparison")
        //            {
        //                //Range1
        //                if (ttype == "Valuable/Non-Valuable")
        //                {
        //                    if (var1.Count > 0)
        //                    {
        //                        vyValues[0] = var1.Count;
        //                        vxValues[0] = "Valuable";

        //                        vyValues[1] = range1.Count - var1.Count;
        //                        vxValues[1] = "Non-Valuable";
        //                    }
        //                    else
        //                    {
        //                        vyValues[0] = 0;
        //                        vxValues[0] = "Valuable";

        //                        vyValues[1] = range1.Count;
        //                        vxValues[1] = "Non-Valuable";
        //                    }
        //                }
        //                else if (ttype == "Delivered/Not-Delivered")
        //                {
        //                    //valuables = loghistories.Where(i => i.Status == "Found").ToList();

        //                    if (var1.Count > 0)
        //                    {
        //                        vyValues[0] = var1.Count;
        //                        vxValues[0] = "Not-Delivered";

        //                        vyValues[1] = range1.Count - var1.Count;
        //                        vxValues[1] = "Delivered";
        //                    }
        //                    else
        //                    {
        //                        vyValues[0] = 0;
        //                        vxValues[0] = "Not-Delivered";

        //                        vyValues[1] = range1.Count;
        //                        vxValues[1] = "Delivered";
        //                    }
        //                }
        //                //Range2
        //                if (ttype == "Valuable/Non-Valuable")
        //                {
        //                    if (var2.Count > 0)
        //                    {
        //                        yyValues[0] = var2.Count;
        //                        yxValues[0] = "Valuable";

        //                        yyValues[1] = range2.Count - var2.Count;
        //                        yxValues[1] = "Non-Valuable";
        //                    }
        //                    else
        //                    {
        //                        yyValues[0] = 0;
        //                        yxValues[0] = "Valuable";

        //                        yyValues[1] = range2.Count;
        //                        yxValues[1] = "Non-Valuable";
        //                    }
        //                }
        //                else if (ttype == "Delivered/Not-Delivered")
        //                {
        //                    //valuables = loghistories.Where(i => i.Status == "Found").ToList();

        //                    if (var2.Count > 0)
        //                    {
        //                        yyValues[0] = var2.Count;
        //                        yxValues[0] = "Not-Delivered";

        //                        yyValues[1] = range2.Count - var2.Count;
        //                        yxValues[1] = "Delivered";
        //                    }
        //                    else
        //                    {
        //                        yyValues[0] = 0;
        //                        yxValues[0] = "Not-Delivered";

        //                        yyValues[1] = range2.Count;
        //                        yxValues[1] = "Delivered";
        //                    }
        //                }
        //            }

        //            if (status == "Valuable/Non-Valuable")
        //            {
        //                Chart1.Series["Series1"].Points.DataBindXY(vxValues, vyValues);
        //            }
        //            else if (status == "Valuable Returned/Not-Returned")
        //            {
        //                Chart1.Series["Series1"].Points.DataBindXY(vxValues, vyValues);
        //            }
        //            else if (status == "Charity Returned/Not-Returned")
        //            {
        //                Chart1.Series["Series1"].Points.DataBindXY(vxValues, vyValues);
        //            }
        //            else if (status == "Delivered/Not-Delivered")
        //            {
        //                Chart1.Series["Series1"].Points.DataBindXY(vxValues, vyValues);
        //            }
        //            else if (status == "All")
        //            {
        //                Chart1.Series["Series1"].Points.DataBindXY(xValues, yValues);
        //            }
        //            else if (status == "Comparison")
        //            {
        //                Chart1.Series["Series1"].Points.DataBindXY(vxValues, vyValues);
        //                Chart2.Series["Series1"].Points.DataBindXY(yxValues, yyValues);
        //            }

        //            table.AddCell(headerCell);
        //            table.AddCell(headerCell2);
        //            table.AddCell(headerCell3);
        //            table.AddCell(headerCell4);
        //            table.AddCell(headerCell5);
        //            table.AddCell(headerCell6);



        //            iTextSharp.text.Paragraph paragraphTable2 = new iTextSharp.text.Paragraph();
        //            paragraphTable2.SpacingBefore = 150f;
        //            PdfPTable table2 = new PdfPTable(10);
        //            table2.WidthPercentage = 100;

        //            table2.AddCell(new PdfPCell(new Phrase("Status", headerf)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER });
        //            table2.AddCell(new PdfPCell(new Phrase("Ref#", headerf)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER });
        //            table2.AddCell(new PdfPCell(new Phrase("Found Date", headerf)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER });
        //            table2.AddCell(new PdfPCell(new Phrase("Type", headerf)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER });
        //            table2.AddCell(new PdfPCell(new Phrase("Sub Type", headerf)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER });
        //            table2.AddCell(new PdfPCell(new Phrase("Brand", headerf)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER });
        //            table2.AddCell(new PdfPCell(new Phrase("Location Found", headerf)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER });
        //            table2.AddCell(new PdfPCell(new Phrase("Finder Name", headerf)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER });
        //            table2.AddCell(new PdfPCell(new Phrase("Department", headerf)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER });
        //            table2.AddCell(new PdfPCell(new Phrase("Receiver Name", headerf)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER, PaddingBottom = 10 });

        //            PdfPTable table3 = new PdfPTable(10);
        //            table3.WidthPercentage = 100;

        //            table3.AddCell(new PdfPCell(new Phrase("Status", headerf)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER });
        //            table3.AddCell(new PdfPCell(new Phrase("Ref#", headerf)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER });
        //            table3.AddCell(new PdfPCell(new Phrase("Found Date", headerf)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER });
        //            table3.AddCell(new PdfPCell(new Phrase("Type", headerf)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER });
        //            table3.AddCell(new PdfPCell(new Phrase("Sub Type", headerf)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER });
        //            table3.AddCell(new PdfPCell(new Phrase("Brand", headerf)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER });
        //            table3.AddCell(new PdfPCell(new Phrase("Location Found", headerf)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER });
        //            table3.AddCell(new PdfPCell(new Phrase("Finder Name", headerf)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER });
        //            table3.AddCell(new PdfPCell(new Phrase("Department", headerf)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER });
        //            table3.AddCell(new PdfPCell(new Phrase("Receiver Name", headerf)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER, PaddingBottom = 10 });



        //            var nonV = new List<ItemFound>();
        //            var totalcount = new List<ItemFound>();
        //            totalcount = loghistories;
        //            if (status != "All" && status != "Valuable/Non-Valuable" && status != "Delivered/Not-Delivered" && status != "Comparison" && status != "Valuable Returned/Not-Returned" && status != "Charity Returned/Not-Returned" && status != "Alcohol Disposed")
        //            {
        //                loghistories = loghistories.Where(i => i.Status == status).ToList();
        //            }
        //            else if (status == "Valuable/Non-Valuable")
        //            {
        //                nonV = loghistories.Where(i => i.Type != "Valuable" && i.Type != "Valuables").ToList();
        //                loghistories = loghistories.Where(i => i.Type == "Valuable" || i.Type == "Valuables").ToList();
        //            }
        //            else if (status == "Valuable Returned/Not-Returned")
        //            {
        //                var gloghistories = loghistories.Where(i => i.Type == "Valuable" || i.Type == "Valuables").ToList();
        //                nonV = gloghistories.Where(i => i.Status != "Found").ToList();
        //                loghistories = gloghistories.Where(i => i.Status == "Found").ToList();
        //            }
        //            else if (status == "Charity Returned/Not-Returned")
        //            {
        //                var gloghistories = loghistories.Where(i => i.Type == "Charity" || i.Type == "Charity").ToList();
        //                nonV = gloghistories.Where(i => i.Status != "Found").ToList();
        //                loghistories = gloghistories.Where(i => i.Status == "Found").ToList();
        //            }
        //            else if (status == "Alcohol Disposed")
        //            {
        //                loghistories = loghistories.Where(i => i.Type == "Disposal" && i.SubType == "Alcohol" && i.Status == "Dispose").ToList();
        //            }
        //            else if (status == "Delivered/Not-Delivered")
        //            {
        //                nonV = loghistories.Where(i => i.Status != "Found").ToList();
        //                loghistories = loghistories.Where(i => i.Status == "Found").ToList();
        //            }
        //            else if (status == "Comparison")
        //            {
        //                if (ttype == "Valuable/Non-Valuable")
        //                {
        //                    nonV = range1.OrderByDescending(i => i.Type).ToList();
        //                    loghistories = range2.OrderByDescending(i => i.Type).ToList();
        //                }
        //                else if (ttype == "Delivered/Not-Delivered")
        //                {
        //                    var nrange1 = range1;
        //                    var nrange2 = range2;

        //                    var delv1 = nrange1.Where(i => i.Status != "Found").ToList();
        //                    var delv2 = nrange2.Where(i => i.Status != "Found").ToList();

        //                    var ndelv1 = nrange1.Where(i => i.Status == "Found").ToList();
        //                    var ndelv2 = nrange2.Where(i => i.Status == "Found").ToList();


        //                    nonV.AddRange(delv1);
        //                    nonV.AddRange(ndelv1);

        //                    loghistories.Clear();

        //                    loghistories.AddRange(delv2);
        //                    loghistories.AddRange(ndelv2);
        //                }
        //            }
        //            foreach (var device in nonV)
        //            {
        //                var dt = device.DateFound.AddHours(userinfo.TimeZone).ToString();
        //                var dto = string.Empty;

        //                if (status == "Comparison")
        //                {
        //                    if (ttype == "Valuable/Non-Valuable")
        //                    {
        //                        if (device.Type != "Valuable" && device.Type != "Valuables")
        //                            device.Type = "Non-Valuable";
        //                    }
        //                    else if (ttype == "Delivered/Not-Delivered")
        //                    {
        //                        if (device.Status != "Found")
        //                            device.Status = "Delivered";
        //                        else
        //                        {
        //                            device.Status = "Not-Delivered";
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    if (status == "Valuable/Non-Valuable")
        //                    {
        //                        device.Type = "Non-Valuable";
        //                    }
        //                    else if (status == "Delivered/Not-Delivered")
        //                    {
        //                        device.Status = "Delivered";
        //                    }
        //                    else if (device.Status == "Dispose" || device.Status == "Donate")
        //                    {
        //                        var Rinfo = ItemDispose.GetItemDisposeByReference(device.Reference, dbConnection);
        //                        if (Rinfo != null)
        //                        {
        //                            dto = "(" + Rinfo.DisposedTo + ")";
        //                        }
        //                    }
        //                }
        //                var xheaderCell = new PdfPCell(new Phrase(device.Status + dto, tentimes));
        //                var xheaderCell2 = new PdfPCell(new Phrase(device.Reference, tentimes));
        //                var xheaderCell3 = new PdfPCell(new Phrase(dt, tentimes));

        //                var xheaderCell4 = new PdfPCell(new Phrase(device.Type, tentimes));
        //                var xheaderCell5 = new PdfPCell(new Phrase(device.SubType, tentimes));
        //                var xheaderCell6 = new PdfPCell(new Phrase(device.Brand, tentimes));
        //                var xheaderCell7 = new PdfPCell(new Phrase(device.LocationFound, tentimes));
        //                var xheaderCell8 = new PdfPCell(new Phrase(device.FinderName, tentimes));
        //                var xheaderCell9 = new PdfPCell(new Phrase(device.FinderDepartment, tentimes));
        //                var xheaderCell10 = new PdfPCell(new Phrase(device.ReceiverName, tentimes));

        //                table3.AddCell(xheaderCell);
        //                table3.AddCell(xheaderCell2);
        //                table3.AddCell(xheaderCell3);
        //                table3.AddCell(xheaderCell4);
        //                table3.AddCell(xheaderCell5);
        //                table3.AddCell(xheaderCell6);
        //                table3.AddCell(xheaderCell7);
        //                table3.AddCell(xheaderCell8);
        //                table3.AddCell(xheaderCell9);
        //                table3.AddCell(xheaderCell10);
        //            }
        //            foreach (var device in loghistories)
        //            {

        //                //if (device.RecevieTime.Value.Ticks > fromdate.Ticks && device.RecevieTime.Value.Ticks < todate.Ticks)
        //                {
        //                    var dt = device.DateFound.AddHours(userinfo.TimeZone).ToString();
        //                    var dto = string.Empty;
        //                    if (status == "Comparison")
        //                    {
        //                        if (ttype == "Valuable/Non-Valuable")
        //                        {
        //                            if (device.Type != "Valuable" && device.Type != "Valuables")
        //                                device.Type = "Non-Valuable";
        //                        }
        //                        else if (ttype == "Delivered/Not-Delivered")
        //                        {
        //                            if (device.Status != "Found" && device.Status != "Not-Delivered")
        //                            {
        //                                device.Status = "Delivered";
        //                            }
        //                            else
        //                            {
        //                                device.Status = "Not-Delivered";
        //                            }
        //                        }
        //                    }
        //                    else
        //                    {
        //                        if (status == "Delivered/Not-Delivered")
        //                        {
        //                            device.Status = "Not-Delivered";
        //                        }
        //                        else if (device.Status == "Dispose" || device.Status == "Donate")
        //                        {
        //                            var Rinfo = ItemDispose.GetItemDisposeByReference(device.Reference, dbConnection);
        //                            if (Rinfo != null)
        //                            {
        //                                // dt = Rinfo.CreatedDate.ToString();
        //                                //uname = Rinfo.RecipientName;
        //                                dto = "(" + Rinfo.DisposedTo + ")";
        //                            }
        //                        }
        //                    }
        //                    var xheaderCell = new PdfPCell(new Phrase(device.Status + dto, tentimes));
        //                    var xheaderCell2 = new PdfPCell(new Phrase(device.Reference, tentimes));
        //                    var xheaderCell3 = new PdfPCell(new Phrase(dt, tentimes));
        //                    var xheaderCell4 = new PdfPCell(new Phrase(device.Type, tentimes));
        //                    var xheaderCell5 = new PdfPCell(new Phrase(device.SubType, tentimes));
        //                    var xheaderCell6 = new PdfPCell(new Phrase(device.Brand, tentimes));
        //                    var xheaderCell7 = new PdfPCell(new Phrase(device.LocationFound, tentimes));
        //                    var xheaderCell8 = new PdfPCell(new Phrase(device.FinderName, tentimes));
        //                    var xheaderCell9 = new PdfPCell(new Phrase(device.FinderDepartment, tentimes));
        //                    var xheaderCell10 = new PdfPCell(new Phrase(device.ReceiverName, tentimes));

        //                    table2.AddCell(xheaderCell);
        //                    table2.AddCell(xheaderCell2);
        //                    table2.AddCell(xheaderCell3);
        //                    table2.AddCell(xheaderCell4);
        //                    table2.AddCell(xheaderCell5);
        //                    table2.AddCell(xheaderCell6);
        //                    table2.AddCell(xheaderCell7);
        //                    table2.AddCell(xheaderCell8);
        //                    table2.AddCell(xheaderCell9);
        //                    table2.AddCell(xheaderCell10);
        //                }
        //            }
        //            doc.Add(paragraphTable);
        //            if (status == "All")
        //            {
        //                doc.Add(table);

        //                using (MemoryStream memoryStream = new MemoryStream())
        //                {
        //                    Chart1.SaveImage(memoryStream, ChartImageFormat.Png);
        //                    iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance(memoryStream.GetBuffer());
        //                    img.ScalePercent(75f);
        //                    img.Alignment = iTextSharp.text.Image.ALIGN_CENTER;
        //                    doc.Add(img);
        //                }
        //                //doc.NewPage();
        //               //// doc.Add(new Paragraph(25, "\u00a0"));
        //                doc.Add(table2);
        //            }
        //            else if (status == "Alcohol Disposed")
        //            {
        //                doc.Add(table2);
        //            }
        //            else if (status == "Valuable/Non-Valuable")
        //            {
        //                PdfPTable tableV = new PdfPTable(3);
        //                tableV.WidthPercentage = 100;
        //                tableV.AddCell(new PdfPCell(new Phrase("Valuable", headerf)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER, PaddingBottom = 10 });
        //                tableV.AddCell(new PdfPCell(new Phrase("Non-Valuable", headerf)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER });
        //                tableV.AddCell(new PdfPCell(new Phrase("Total", headerf)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER });

        //                var headerCellV = new PdfPCell(new Phrase(valuables.Count.ToString(), tentimes));
        //                var headerCellVV = new PdfPCell(new Phrase((nonV.Count).ToString(), tentimes));
        //                var headerCellVVV = new PdfPCell(new Phrase((totalcount.Count).ToString(), tentimes));

        //                tableV.AddCell(headerCellV);
        //                tableV.AddCell(headerCellVV);
        //                tableV.AddCell(headerCellVVV);
        //                doc.Add(tableV);

        //                using (MemoryStream memoryStream = new MemoryStream())
        //                {
        //                    Chart1.SaveImage(memoryStream, ChartImageFormat.Png);
        //                    iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance(memoryStream.GetBuffer());
        //                    img.ScalePercent(75f);
        //                    img.Alignment = iTextSharp.text.Image.ALIGN_CENTER;
        //                    doc.Add(img);
        //                }

        //                if (valuables.Count > 0)
        //                {
        //                    //doc.NewPage();
        //                   //// doc.Add(new Paragraph(25, "\u00a0"));
        //                    doc.Add(table2);
        //                }


        //                if (nonV.Count > 0)
        //                {
        //                    //doc.NewPage();
        //                   // doc.Add(new Paragraph(25, "\u00a0"));
        //                    doc.Add(table3);
        //                }
        //            }
        //            else if (status == "Charity Returned/Not-Returned" || status == "Valuable Returned/Not-Returned")
        //            {
        //                PdfPTable tableV = new PdfPTable(3);
        //                tableV.WidthPercentage = 100;
        //                tableV.AddCell(new PdfPCell(new Phrase("Not-Returned", headerf)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER, PaddingBottom = 10 });
        //                tableV.AddCell(new PdfPCell(new Phrase("Returned", headerf)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER });
        //                tableV.AddCell(new PdfPCell(new Phrase("Total", headerf)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER });

        //                var headerCellV = new PdfPCell(new Phrase((valuables.Count - nonV.Count).ToString(), tentimes));
        //                var headerCellVV = new PdfPCell(new Phrase((nonV.Count).ToString(), tentimes));
        //                var headerCellVVV = new PdfPCell(new Phrase((valuables.Count).ToString(), tentimes));

        //                tableV.AddCell(headerCellV);
        //                tableV.AddCell(headerCellVV);
        //                tableV.AddCell(headerCellVVV);
        //                doc.Add(tableV);

        //                using (MemoryStream memoryStream = new MemoryStream())
        //                {
        //                    Chart1.SaveImage(memoryStream, ChartImageFormat.Png);
        //                    iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance(memoryStream.GetBuffer());
        //                    img.ScalePercent(75f);
        //                    img.Alignment = iTextSharp.text.Image.ALIGN_CENTER;
        //                    doc.Add(img);
        //                }
        //                if (valuables.Count > 0)
        //                {
        //                    //doc.NewPage();
        //                   // doc.Add(new Paragraph(25, "\u00a0"));
        //                    doc.Add(table2);
        //                }

        //                if (nonV.Count > 0)
        //                {
        //                    //doc.NewPage();
        //                   // doc.Add(new Paragraph(25, "\u00a0"));
        //                    doc.Add(table3);
        //                }
        //            }
        //            else if (status == "Delivered/Not-Delivered")
        //            {
        //                PdfPTable tableV = new PdfPTable(3);
        //                tableV.WidthPercentage = 100;
        //                tableV.AddCell(new PdfPCell(new Phrase("Not-Delivered", headerf)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER, PaddingBottom = 10 });
        //                tableV.AddCell(new PdfPCell(new Phrase("Delivered", headerf)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER });
        //                tableV.AddCell(new PdfPCell(new Phrase("Total", headerf)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER });

        //                var headerCellV = new PdfPCell(new Phrase(valuables.Count.ToString(), tentimes));
        //                var headerCellVV = new PdfPCell(new Phrase((nonV.Count).ToString(), tentimes));
        //                var headerCellVVV = new PdfPCell(new Phrase((totalcount.Count).ToString(), tentimes));

        //                tableV.AddCell(headerCellV);
        //                tableV.AddCell(headerCellVV);
        //                tableV.AddCell(headerCellVVV);
        //                doc.Add(tableV);

        //                using (MemoryStream memoryStream = new MemoryStream())
        //                {
        //                    Chart1.SaveImage(memoryStream, ChartImageFormat.Png);
        //                    iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance(memoryStream.GetBuffer());
        //                    img.ScalePercent(75f);
        //                    img.Alignment = iTextSharp.text.Image.ALIGN_CENTER;
        //                    doc.Add(img);
        //                }
        //                if (valuables.Count > 0)
        //                {
        //                    //doc.NewPage();
        //                   // doc.Add(new Paragraph(25, "\u00a0"));
        //                    doc.Add(table2);
        //                }

        //                if (nonV.Count > 0)
        //                {
        //                    //doc.NewPage();
        //                   // doc.Add(new Paragraph(25, "\u00a0"));
        //                    doc.Add(table3);
        //                }
        //            }
        //            else if (status == "Comparison")
        //            {
        //                if (ttype == "Valuable/Non-Valuable")
        //                {
        //                    PdfPTable tableV = new PdfPTable(8);
        //                    tableV.WidthPercentage = 100;
        //                    tableV.AddCell(new PdfPCell(new Phrase("Range 1", headerf)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER, PaddingBottom = 10 });
        //                    tableV.AddCell(new PdfPCell(new Phrase("Valuable", headerf)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER });
        //                    tableV.AddCell(new PdfPCell(new Phrase("Non-Valuable", headerf)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER });
        //                    tableV.AddCell(new PdfPCell(new Phrase("Total", headerf)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER });


        //                    tableV.AddCell(new PdfPCell(new Phrase("Range 2", headerf)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER, PaddingBottom = 10 });
        //                    tableV.AddCell(new PdfPCell(new Phrase("Valuable", headerf)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER });
        //                    tableV.AddCell(new PdfPCell(new Phrase("Non-Valuable", headerf)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER });
        //                    tableV.AddCell(new PdfPCell(new Phrase("Total", headerf)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER });

        //                    var headerCellV1 = new PdfPCell(new Phrase(year1 + " " + quater1, tentimes));
        //                    var headerCellV = new PdfPCell(new Phrase(var1.Count.ToString(), tentimes));
        //                    var headerCellVV = new PdfPCell(new Phrase((range1.Count - var1.Count).ToString(), tentimes));
        //                    var headerCellVVV = new PdfPCell(new Phrase((range1.Count).ToString(), tentimes));

        //                    tableV.AddCell(headerCellV1);
        //                    tableV.AddCell(headerCellV);
        //                    tableV.AddCell(headerCellVV);
        //                    tableV.AddCell(headerCellVVV);

        //                    var headerCellV1R = new PdfPCell(new Phrase(year2 + " " + quarter2, tentimes));
        //                    var headerCellVR = new PdfPCell(new Phrase(var2.Count.ToString(), tentimes));
        //                    var headerCellVVR = new PdfPCell(new Phrase((range2.Count - var2.Count).ToString(), tentimes));
        //                    var headerCellVVVR = new PdfPCell(new Phrase((range2.Count).ToString(), tentimes));

        //                    tableV.AddCell(headerCellV1R);
        //                    tableV.AddCell(headerCellVR);
        //                    tableV.AddCell(headerCellVVR);
        //                    tableV.AddCell(headerCellVVVR);

        //                    doc.Add(tableV);

        //                    PdfPTable tableCH = new PdfPTable(2);
        //                    tableCH.WidthPercentage = 100;

        //                    using (MemoryStream memoryStream = new MemoryStream())
        //                    {
        //                        Chart1.SaveImage(memoryStream, ChartImageFormat.Png);
        //                        iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance(memoryStream.GetBuffer());
        //                        img.ScalePercent(75f);
        //                        img.Alignment = iTextSharp.text.Image.ALIGN_CENTER;
        //                        tableCH.AddCell(img);
        //                    }

        //                    using (MemoryStream memoryStream2 = new MemoryStream())
        //                    {
        //                        Chart2.SaveImage(memoryStream2, ChartImageFormat.Png);
        //                        iTextSharp.text.Image imgV = iTextSharp.text.Image.GetInstance(memoryStream2.GetBuffer());
        //                        imgV.ScalePercent(75f);
        //                        imgV.Alignment = iTextSharp.text.Image.ALIGN_CENTER;
        //                        tableCH.AddCell(imgV);
        //                    }

        //                    doc.Add(tableCH);


        //                    PdfPTable tableCH1 = new PdfPTable(1);
        //                    tableCH1.WidthPercentage = 100;
        //                    tableCH1.AddCell("Range 1 " + year1 + " " + quater1);
        //                    //doc.NewPage();
        //                   // doc.Add(new Paragraph(25, "\u00a0"));
        //                    doc.Add(tableCH1);
        //                    doc.Add(table3); 

        //                    PdfPTable tableCH2 = new PdfPTable(1);
        //                    tableCH2.WidthPercentage = 100;
        //                    tableCH2.AddCell("Range 2 " + year2 + " " + quarter2);
        //                    //doc.NewPage();
        //                   // doc.Add(new Paragraph(25, "\u00a0"));
        //                    doc.Add(tableCH2);
        //                    doc.Add(table2);
        //                }
        //                else if (ttype == "Delivered/Not-Delivered")
        //                {
        //                    PdfPTable tableV = new PdfPTable(8);
        //                    tableV.WidthPercentage = 100;
        //                    tableV.AddCell(new PdfPCell(new Phrase("Range 1", headerf)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER, PaddingBottom = 10 });
        //                    tableV.AddCell(new PdfPCell(new Phrase("Not-Delivered", headerf)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER });
        //                    tableV.AddCell(new PdfPCell(new Phrase("Delivered", headerf)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER });
        //                    tableV.AddCell(new PdfPCell(new Phrase("Total", headerf)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER });


        //                    tableV.AddCell(new PdfPCell(new Phrase("Range 2", headerf)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER, PaddingBottom = 2 });
        //                    tableV.AddCell(new PdfPCell(new Phrase("Not-Delivered", headerf)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER });
        //                    tableV.AddCell(new PdfPCell(new Phrase("Delivered", headerf)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER });
        //                    tableV.AddCell(new PdfPCell(new Phrase("Total", headerf)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER });

        //                    var headerCellV1 = new PdfPCell(new Phrase(year1 + " " + quater1, tentimes));
        //                    var headerCellV = new PdfPCell(new Phrase(var1.Count.ToString(), tentimes));
        //                    var headerCellVV = new PdfPCell(new Phrase((range1.Count - var1.Count).ToString(), tentimes));
        //                    var headerCellVVV = new PdfPCell(new Phrase((range1.Count).ToString(), tentimes));

        //                    tableV.AddCell(headerCellV1);
        //                    tableV.AddCell(headerCellV);
        //                    tableV.AddCell(headerCellVV);
        //                    tableV.AddCell(headerCellVVV);

        //                    var headerCellV1R = new PdfPCell(new Phrase(year2 + " " + quarter2, tentimes));
        //                    var headerCellVR = new PdfPCell(new Phrase(var2.Count.ToString(), tentimes));
        //                    var headerCellVVR = new PdfPCell(new Phrase((range2.Count - var2.Count).ToString(), tentimes));
        //                    var headerCellVVVR = new PdfPCell(new Phrase((range2.Count).ToString(), tentimes));

        //                    tableV.AddCell(headerCellV1R);
        //                    tableV.AddCell(headerCellVR);
        //                    tableV.AddCell(headerCellVVR);
        //                    tableV.AddCell(headerCellVVVR);

        //                    doc.Add(tableV);

        //                    PdfPTable tableCH = new PdfPTable(2);
        //                    tableCH.WidthPercentage = 100;

        //                    using (MemoryStream memoryStream = new MemoryStream())
        //                    {
        //                        Chart1.SaveImage(memoryStream, ChartImageFormat.Png);
        //                        iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance(memoryStream.GetBuffer());
        //                        img.ScalePercent(75f);
        //                        img.Alignment = iTextSharp.text.Image.ALIGN_CENTER;
        //                        tableCH.AddCell(img);
        //                    }

        //                    using (MemoryStream memoryStream2 = new MemoryStream())
        //                    {
        //                        Chart2.SaveImage(memoryStream2, ChartImageFormat.Png);
        //                        iTextSharp.text.Image imgV = iTextSharp.text.Image.GetInstance(memoryStream2.GetBuffer());
        //                        imgV.ScalePercent(75f);
        //                        imgV.Alignment = iTextSharp.text.Image.ALIGN_CENTER;
        //                        tableCH.AddCell(imgV);
        //                    }

        //                    doc.Add(tableCH);


        //                    PdfPTable tableCH1 = new PdfPTable(1);
        //                    tableCH1.WidthPercentage = 100;
        //                    tableCH1.AddCell("Range 1 " + year1 + " " + quater1);
        //                    //doc.NewPage();
        //                   // doc.Add(new Paragraph(25, "\u00a0"));
        //                    doc.Add(tableCH1);
        //                    doc.Add(table3); 

        //                    PdfPTable tableCH2 = new PdfPTable(1);
        //                    tableCH2.WidthPercentage = 100;
        //                    tableCH2.AddCell("Range 2 " + year2 + " " + quarter2);
        //                    //doc.NewPage();
        //                   // doc.Add(new Paragraph(25, "\u00a0"));
        //                    doc.Add(tableCH2);
        //                    doc.Add(table2);

        //                }
        //            }
        //            //using (MemoryStream memoryStream2 = new MemoryStream())
        //            //{
        //            //    Chart2.SaveImage(memoryStream2, ChartImageFormat.Png);
        //            //    iTextSharp.text.Image imgV = iTextSharp.text.Image.GetInstance(memoryStream2.GetBuffer());
        //            //    imgV.ScalePercent(75f);
        //            //    imgV.Alignment = iTextSharp.text.Image.ALIGN_CENTER;
        //            //    doc.Add(imgV);
        //            //}
        //            doc.Close();

        //            Response.ContentType = "application/pdf";
        //            Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
        //            Response.Cache.SetCacheability(HttpCacheability.NoCache);
        //            Response.Write(doc);
        //            Response.End();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MIMSLog.MIMSLogSave("Reports", "btnExport2_Click", ex, dbConnection);
        //    }
        //}

        [WebMethod]
        public static List<string> getYears(string id, string uname)
        {
            var userinfo = Users.GetUserByEncryptedName(uname, dbConnection);
            var listy = new List<string>();
            try
            {
                var gDate = DateTime.Now.Year;
                var totaly = gDate - 2018;
                listy.Add("2018");
                for (var i = 1; i <= totaly; i++)
                {
                    var d = i + 2018;
                    listy.Add(d.ToString());
                }
            }
            catch (Exception err)
            {
                MIMSLog.MIMSLogSave("Reports", "getYears", err, dbConnection, userinfo.SiteId);
            }
            return listy;
        }


        protected void btnExport_Click(object sender, EventArgs e)
        {
            var userinfo = Users.GetUserByName(User.Identity.Name, dbConnection);
            try
            {
                //using (MemoryStream memoryStream = new MemoryStream())
                {
                    string toDate = toHD.Value; //String.Format("{0}", Request.Form["toDate"]);
                    string fromDate = fromHD.Value; // String.Format("{0}", Request.Form["fromDate"]);

                    var status = lfstatus.Value;
                    var contractId = contractID.Value;
                    var projId = projectID.Value;
                    var statusId = statusID.Value;
                    var typeId = typeID.Value;
                    var todate = Convert.ToDateTime(toDate);
                    var fromdate = Convert.ToDateTime(fromDate);
                     
                    var isCustomer = false;
                    var isContract = false;
                    var isProject = false;
                    //var itemsLostFound = ItemFound.GetAllItemFoundByDateRange(Convert.ToDateTime(fromDate).ToString(), Convert.ToDateTime(toDate).AddDays(1).ToString(), dbConnection);

                    var itemsLostFound = new List<UserTask>();

                    if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                    {
                        itemsLostFound = UserTask.GetAllTaskByCId(userinfo.CustomerInfoId, dbConnection);
                    }
                    else if (userinfo.RoleId == (int)Role.SuperAdmin)
                    {
                        itemsLostFound = UserTask.GetAllTask(dbConnection);
                    }
                    else if (userinfo.RoleId == (int)Role.Regional)
                    {
                        itemsLostFound = UserTask.GetAllTaskByLevel5(userinfo.ID, dbConnection);
                    }
                    else if (userinfo.RoleId == (int)Role.CustomerUser)
                    {
                        itemsLostFound = UserTask.GetAllTaskByCustId(userinfo.CustomerLinkId, dbConnection);
                        if (userinfo.ContractLinkId > 0)
                        {
                            itemsLostFound = itemsLostFound.Where(i => i.ContractId == userinfo.ContractLinkId).ToList();
                        }
                    }
                    else
                    {
                        itemsLostFound = UserTask.GetAllTaskBySiteId(userinfo.SiteId, dbConnection);
                    }

                    if (!string.IsNullOrEmpty(status))
                    {
                        if (CommonUtility.isNumeric(status))
                        {
                            if (Convert.ToInt32(status) > 0)
                            {
                                itemsLostFound = itemsLostFound.Where(i => i.CustId == Convert.ToInt32(status)).ToList();
                                isCustomer = true;
                            }
                        }
                    } 
                    if (itemsLostFound.Count > 0)
                    {

                        if (!string.IsNullOrEmpty(contractId))
                        {
                            if (CommonUtility.isNumeric(contractId))
                            {
                                if (Convert.ToInt32(contractId) > 0)
                                {
                                    itemsLostFound = UserTask.GetAllTaskByContractId(Convert.ToInt32(contractId), dbConnection);//itemsLostFound.Where(i => i.ContractId == Convert.ToInt32(contractId)).ToList();

                                    isContract = true;
                                }
                            }
                        }

                        if (!string.IsNullOrEmpty(projId))
                        {
                            if (CommonUtility.isNumeric(projId))
                            {
                                if (Convert.ToInt32(projId) > 0)
                                {
                                    itemsLostFound = itemsLostFound.Where(i => i.ProjectId == Convert.ToInt32(projId)).ToList();

                                    isProject = true;
                                }
                            }
                        }

                        var totaltaskscount = new List<UserTask>();

                        if (!string.IsNullOrEmpty(statusId))
                        {
                            if (CommonUtility.isNumeric(statusId))
                            {
                                if (Convert.ToInt32(statusId) > -1)
                                {

                                    if (Convert.ToInt32(statusId) == (int)TaskStatus.InProgress)
                                    {
                                        itemsLostFound = itemsLostFound.Where(i => i.Status == (int)TaskStatus.InProgress || i.Status == (int)TaskStatus.OnRoute).ToList();
                                    }
                                    else
                                        itemsLostFound = itemsLostFound.Where(i => i.Status == Convert.ToInt32(statusId)).ToList();
                                }
                            }
                        }

                        if (!string.IsNullOrEmpty(typeId))
                        {
                            if (CommonUtility.isNumeric(typeId))
                            {
                                if (Convert.ToInt32(typeId) > 0)
                                {
                                    itemsLostFound = itemsLostFound.Where(i => i.TaskTypeId == Convert.ToInt32(typeId)).ToList();
                                }
                            }
                        }

                        itemsLostFound = itemsLostFound.Where(i => Convert.ToDateTime(i.StartDate).AddHours(userinfo.TimeZone) >= fromdate && Convert.ToDateTime(i.StartDate).AddHours(userinfo.TimeZone) < todate.AddDays(1)).ToList();

                        var loghistories = itemsLostFound;

                        iTextSharp.text.Document doc = new iTextSharp.text.Document(PageSize.LETTER, 25F, 25F, 50F, 25F);

                        doc.SetMargins(25, 25, 65, 35);

                        var fileName = CommonUtility.getDTNow().Day.ToString() + "-" + CommonUtility.getDTNow().Month.ToString() + "-" + CommonUtility.getDTNow().Year.ToString() + "-" + CommonUtility.getDTNow().Hour.ToString() + "-" + CommonUtility.getDTNow().Minute.ToString() + "-" + CommonUtility.getDTNow().Second.ToString() + ".pdf";
                        // path = path + "\\" + fileName;

                        PdfWriter writer = PdfWriter.GetInstance(doc, Response.OutputStream);//PdfWriter.GetInstance(doc, new FileStream(path, FileMode.Create));
                        writer.PageEvent = new ITextEvents()
                        {
                            cid = userinfo.CustomerInfoId
                        };
                        doc.Open();
                        doc.SetMargins(25, 25, 65, 35);
                        BaseFont f_cn = BaseFont.CreateFont(Environment.GetFolderPath(Environment.SpecialFolder.Fonts) + "\\verdana.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        //BaseFont h_cn = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        BaseFont z_cn = BaseFont.CreateFont(BaseFont.ZAPFDINGBATS, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

                        Font times = new Font(f_cn, 12, Font.NORMAL, Color.BLACK);
                        Font btimes = new Font(f_cn, 12, Font.BOLD, Color.BLACK);

                        iTextSharp.text.Color arrowred = new iTextSharp.text.Color(162, 0, 46);

                        Font eleventimes = new Font(f_cn, 11, Font.NORMAL, Color.BLACK);
                        Font tentimes = new Font(f_cn, 10, Font.NORMAL, Color.BLACK);

                        Font weleventimes = new Font(f_cn, 11, Font.NORMAL, Color.WHITE);

                        Font gweb = new Font(z_cn, 11, Font.NORMAL, Color.GREEN);
                        Font rweb = new Font(z_cn, 11, Font.NORMAL, Color.RED);

                        Font ueleventimes = new Font(f_cn, 11, Font.UNDERLINE, Color.BLACK);

                        Font beleventimes = new Font(f_cn, 11, Font.BOLD, Color.BLACK);
                        Font bredeleventimes = new Font(f_cn, 11, Font.BOLD, Color.RED);
                        Font redeleventimes = new Font(f_cn, 11, Font.NORMAL, Color.RED);
                        Font twelvetimes = new Font(f_cn, 12, Font.ITALIC, Color.BLACK);

                        Font mbtimes = new Font(f_cn, 18, Font.BOLD, arrowred);
                        Font cbtimes = new Font(f_cn, 13, Font.BOLD, arrowred);

                        float[] columnWidths1585 = new float[] { 15, 85 };
                        float[] columnWidths2080 = new float[] { 20, 80 };

                        float[] columnWidths1783 = new float[] { 18, 82 };
                        float[] columnWidths7030 = new float[] { 50, 30, 20 };
                        float[] columnWidths2575 = new float[] { 25, 75 };
                        float[] columnWidths3070 = new float[] { 30, 70 };
                        float[] columnWidths4060 = new float[] { 40, 60 };
                        float[] columnWidths3565 = new float[] { 35, 65 };
                        float[] columnWidths4555 = new float[] { 45, 55 };
                        float[] columnWidthsH = new float[] { 30, 50, 20 };

                        if (string.IsNullOrEmpty(userinfo.SiteName))
                            userinfo.SiteName = "N/A";

                        var mainheadertable = new PdfPTable(3);
                        mainheadertable.DefaultCell.Border = Rectangle.NO_BORDER;
                        mainheadertable.WidthPercentage = 100;
                        mainheadertable.SetWidths(columnWidthsH);
                        var headerMain = new PdfPCell();
                        if (userinfo.CustomerInfoId == 87 || userinfo.CustomerInfoId == 45)
                        {
                            if (isContract)
                            {
                                headerMain.AddElement(new Phrase("G4S CONTRACT REPORT", mbtimes));
                            }
                            else if (isCustomer)
                                headerMain.AddElement(new Phrase("G4S ACCOUNT REPORT", mbtimes));
                            else if (isProject)
                                headerMain.AddElement(new Phrase("G4S PROJECT REPORT", mbtimes));
                            else
                                headerMain.AddElement(new Phrase("G4S TASK REPORT", mbtimes));
                        }
                        else
                        {
                            if (isContract)
                            {
                                headerMain.AddElement(new Phrase("MIMS CONTRACT REPORT", mbtimes));
                            }
                            else if (isCustomer)
                                headerMain.AddElement(new Phrase("MIMS ACCOUNT REPORT", mbtimes));
                            else if (isProject)
                                headerMain.AddElement(new Phrase("MIMS PROJECT REPORT", mbtimes));
                            else
                                headerMain.AddElement(new Phrase("MIMS TASK REPORT", mbtimes));
                        }

                        //headerMain.PaddingTop = 20;
                        headerMain.PaddingBottom = 10;
                        headerMain.HorizontalAlignment = 1;
                        headerMain.Border = Rectangle.NO_BORDER;
                        PdfPCell pdfCell1 = new PdfPCell();
                        pdfCell1.Border = Rectangle.NO_BORDER;
                        PdfPCell pdfCell2 = new PdfPCell();
                        pdfCell2.Border = Rectangle.NO_BORDER;
                        mainheadertable.AddCell(pdfCell1);
                        mainheadertable.AddCell(headerMain);
                        mainheadertable.AddCell(pdfCell2);

                        doc.Add(mainheadertable);

                        var headertable = new PdfPTable(2);
                        headertable.DefaultCell.Border = Rectangle.NO_BORDER;
                        headertable.WidthPercentage = 100;
                        headertable.SetWidths(columnWidths1783);


                        var tPending = loghistories.Where(i => i.StatusDescription == "Pending").ToList();
                        var tInProgress = loghistories.Where(i => i.StatusDescription == "InProgress" || i.StatusDescription == "Pause" || i.StatusDescription == "OnRoute").ToList();
                        var tCompleted = loghistories.Where(i => i.StatusDescription == "Completed").ToList();
                        var tAccepted = loghistories.Where(i => i.StatusDescription == "Accepted").ToList();
                        var tCA = loghistories.Where(i => i.StatusDescription == "Completed" || i.StatusDescription == "Accepted").ToList();
                        var ttotal = tPending.Count + tInProgress.Count + tCompleted.Count + tAccepted.Count;

                        double[] yValues = new double[3]; //{ rG.Count, aPose.Count, dG.Count, dPose.Count, fG.Count };
                        string[] xValues = new string[3];//{ "Returned", "Disposed", "Charity", "Authorities", "Found" };

                        double[] yxValues = new double[2]; //{ rG.Count, aPose.Count, dG.Count, dPose.Count, fG.Count };
                        string[] yyValues = new string[2];

                        if (tPending.Count > 0)
                        {
                            yValues[0] = tPending.Count;
                            xValues[0] = "Pending";
                        }
                        else
                        {
                            yValues[0] = 0;
                            xValues[0] = "Pending";
                        }
                        if (tInProgress.Count > 0)
                        {
                            yValues[1] = tInProgress.Count;
                            xValues[1] = "InProgress";
                        }
                        else
                        {
                            yValues[1] = 0;
                            xValues[1] = "InProgress";
                        }
                        if (tCompleted.Count > 0)
                        {
                            yValues[2] = tCompleted.Count;
                            xValues[2] = "Completed";
                        }
                        else
                        {
                            yValues[2] = 0;
                            xValues[2] = "Completed";
                        }

                        Chart1.Series["Series1"].Points.DataBindXY(xValues, yValues);

                        var contracthours = 0;
                        var rconsumed = 0;
                        var rremaining = 0;
                        var excessH = "0";
                        var nlist2 = new List<SearchSelectItem>();
                        if (isContract)
                        {
                            var customers = ContractInfo.GetContractInfoById(Convert.ToInt32(contractId), dbConnection);

                            if (customers != null)
                            {
                                tCA = tCA.Where(i => i.ContractId == customers.Id).ToList();

                                if (customers.ContractType == (int)ContractInfo.ContractTypes.PPM)
                                {
                                    contracthours = customers.TotalPPMHour;

                                    if (customers.SRTotal > 0)
                                        contracthours = customers.SRTotal;
                                }
                                else
                                {
                                    contracthours = customers.TotalPPMHour;
                                }

                                if (tCA.Count > 0)
                                {

                                    var totaltime = 0.0;

                                    var contractminutes = 0;



                                    foreach (var gtask in tCA)
                                    {
                                        var time = TaskJobActivity.GetTotalActivityTimebyTaskId(gtask, dbConnection);
                                        if (time > 0.1)
                                        {
                                            totaltime = totaltime + time;
                                        }
                                        else
                                        {
                                            if (gtask.ActualOnRouteDate != null && gtask.ActualEndDate != null)
                                            {
                                                var span = gtask.ActualEndDate.Value.Subtract(gtask.ActualOnRouteDate.Value);

                                                totaltime = totaltime + span.TotalMinutes;
                                            }
                                            else if (gtask.ActualStartDate != null && gtask.ActualEndDate != null)
                                            {
                                                var span = gtask.ActualEndDate.Value.Subtract(gtask.ActualStartDate.Value);

                                                totaltime = totaltime + span.TotalMinutes;
                                            }
                                        }
                                    }


                                    contractminutes = contracthours * 60;

                                    var remainingHours = contractminutes - totaltime;
                                    var gConsumed = (int)Math.Round((float)totaltime / (float)60);
                                    var gRem = (int)Math.Round((float)remainingHours / (float)60);

                                    if (totaltime > contractminutes)
                                    {
                                        gRem = 0;

                                        var excess = (int)Math.Round((float)(totaltime - contractminutes) / (float)60);
                                        excessH = excess.ToString();
                                    }

                                    if (totaltime > 60)
                                    {
                                        if (totaltime > contractminutes)
                                        {
                                            gConsumed = contracthours;
                                        }

                                        rconsumed = gConsumed;
                                        rremaining = gRem;


                                    }
                                    else
                                    {
                                        remainingHours = contractminutes - 60;
                                        gRem = (int)Math.Round((float)remainingHours / (float)60);
                                        rconsumed = 1;
                                        rremaining = gRem;
                                    }
                                }

                                if (rconsumed > 0)
                                {
                                    yxValues[0] = rconsumed;
                                    yyValues[0] = "Consumed";
                                }
                                else
                                {
                                    yxValues[0] = 0;
                                    yyValues[0] = "Consumed";
                                }
                                if (rremaining > 0)
                                {
                                    yxValues[1] = rremaining;
                                    yyValues[1] = "Remaining";
                                }
                                else
                                {
                                    rremaining = contracthours;

                                    if (Convert.ToInt32(excessH) > 0)
                                    {
                                        rremaining = 0;
                                    }

                                    yxValues[1] = rremaining;
                                    yyValues[1] = "Remaining";
                                }

                                Chart2.Series["Series1"].Points.DataBindXY(yyValues, yxValues);
                                //NEW ADDED
                                var gtaskcategories = new List<TaskCategory>();

                                if (userinfo.RoleId == (int)Role.SuperAdmin || userinfo.RoleId == (int)Role.ChiefOfficer)
                                    gtaskcategories = TaskCategory.GetAllTaskCategories(dbConnection);
                                else if (userinfo.RoleId == (int)Role.Director || userinfo.RoleId == (int)Role.Manager || userinfo.RoleId == (int)Role.Admin)
                                    gtaskcategories = TaskCategory.GetAllTaskCategoriesBySiteId(userinfo.SiteId, dbConnection);
                                else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                                    gtaskcategories = TaskCategory.GetAllTaskCategoriesByCId(userinfo.CustomerInfoId, dbConnection);
                                else if (userinfo.RoleId == (int)Role.Regional)
                                    gtaskcategories = TaskCategory.GetAllTaskCategoriesByLevel5(userinfo.ID, dbConnection);

                                
                                var totaltimeC = 0.0;
                                var nlist = new List<SearchSelectItem>();
                                var t = new TaskCategory();
                                t.CategoryId = 0;
                                t.Description = "None";
                                gtaskcategories.Add(t); 
                                foreach (var v in gtaskcategories)
                                {
                                    var newSearchSelectItemzz = new SearchSelectItem();
                                    newSearchSelectItemzz.Name = v.Description;
                                    var x = tCA.Where(i => i.TaskTypeId == v.CategoryId).ToList();
                                    var xtotaltime = 0.0;
                                    foreach (var gtask in x)
                                    {
                                        var time = TaskJobActivity.GetTotalActivityTimebyTaskId(gtask, dbConnection);
                                        if (time > 0.1)
                                        {
                                            xtotaltime = xtotaltime + time;
                                            totaltimeC = totaltimeC + time;
                                        }
                                        else
                                        {
                                            if (gtask.ActualOnRouteDate != null && gtask.ActualEndDate != null)
                                            {
                                                var span = gtask.ActualEndDate.Value.Subtract(gtask.ActualOnRouteDate.Value);
                                                xtotaltime = xtotaltime + span.TotalMinutes;
                                                totaltimeC = totaltimeC + span.TotalMinutes;
                                            }
                                            else if (gtask.ActualStartDate != null && gtask.ActualEndDate != null)
                                            {
                                                var span = gtask.ActualEndDate.Value.Subtract(gtask.ActualStartDate.Value);
                                                xtotaltime = xtotaltime + span.TotalMinutes;
                                                totaltimeC = totaltimeC + span.TotalMinutes;
                                            }
                                        }
                                    }

                                    if (xtotaltime > 0)
                                    {
                                        var gConsumeds = (int)Math.Round(xtotaltime);//(int)Math.Round((float)xtotaltime / (float)60);
                                        //var gConsumeds = (int)Math.Round((float)xtotaltime / (float)60);
                                        newSearchSelectItemzz.ID = gConsumeds.ToString();
                                        nlist.Add(newSearchSelectItemzz);
                                    }
                                }

                                var gConsumedX = (int)Math.Round(totaltimeC);//(int)Math.Round((float)totaltimeC / (float)60);

                                //if (totaltimeC >= 60)
                                //{
                                //    //gConsumed = (int)Math.Round((float)totaltime / (float)60);
                                //}
                                //else
                                //{
                                //    //get one hour for every task if logged atleast seconds on it.
                                //    if (totaltimeC > 0)
                                //        gConsumedX = 1;
                                //}
                                double[] tyxValues = new double[nlist.Count+1]; //{ rG.Count, aPose.Count, dG.Count, dPose.Count, fG.Count };
                                string[] tyyValues = new string[nlist.Count+1];
                                var contractminutesX = (contracthours * 60);
                                //var pendingPercentage = (int)Math.Round((float)gConsumedX / (float)contracthours * (float)100);

                                var pendingPercentage = (int)Math.Round((float)gConsumedX / (float)contractminutesX * (float)100);

                                var v2 = (float)Convert.ToInt32(gConsumedX) / (float)contractminutesX * (float)100;
                                if (v2 >= 1)
                                {
                                    //v2 = (int)Math.Round((float)Convert.ToInt32(gConsumedX) / (float)contractminutesX * (float)100);
                                }
                                else
                                {
                                    pendingPercentage = 0;
                                }


                                var pendingPercentage2 = 100 - pendingPercentage;
                                var c = 0;
                                var iC = 0;
                                foreach (var n in nlist)
                                {
                                    var newSearchSelectItemzz = new SearchSelectItem();
                                    newSearchSelectItemzz.Name = n.Name;
                                    var percentage = (int)Math.Round((float)Convert.ToInt32(n.ID) / (float)contractminutesX * (float)100);
                                    var v = (float)Convert.ToInt32(n.ID) / (float)contractminutesX * (float)100;
                                    if (v >= 1)
                                    {
                                        //v = (int)Math.Round((float)Convert.ToInt32(n.ID) / (float)contractminutesX * (float)100);
                                    }
                                    else
                                    {
                                        percentage = 0;
                                    }
                                    //var percentage = (int)Math.Round((float)Convert.ToInt32(n.ID) / (float)contractminutesX * (float)100);
                                    // var percentage = (int)Math.Round((float)Convert.ToInt32(n.ID) / (float)contracthours * (float)100);
                                    if (Convert.ToInt32(n.ID) > 0)
                                    {
                                        if (pendingPercentage2 > 0)
                                        {
                                            tyxValues[iC] = percentage;
                                            tyyValues[iC] = n.Name;
                                            newSearchSelectItemzz.ID = percentage.ToString();

                                        }
                                        else
                                        {
                                            percentage = (int)Math.Round((float)Convert.ToInt32(n.ID) / (float)gConsumedX * (float)100);
                                            tyxValues[iC] = percentage;
                                            tyyValues[iC] = n.Name;
                                            newSearchSelectItemzz.ID = percentage.ToString();
                                        }
                                    }
                                    else
                                    {
                                        if (c != pendingPercentage)
                                        {
                                            var p = (float)((float)1 / (float)pendingPercentage);
                                            if (p == 0.5)
                                            {
                                                tyxValues[iC] = 1;
                                                tyyValues[iC] = n.Name;
                                                newSearchSelectItemzz.ID = "1";
                                            }
                                            else
                                            {
                                                percentage = (int)Math.Round((float)1 / (float)pendingPercentage);
                                                if (percentage > 0)
                                                { }
                                                else
                                                    percentage = 0;

                                                tyxValues[iC] = percentage;
                                                tyyValues[iC] = n.Name;
                                                newSearchSelectItemzz.ID = percentage.ToString();
                                            }
                                        }
                                        else
                                        {
                                            tyxValues[iC] = percentage;
                                            tyyValues[iC] = n.Name;
                                            newSearchSelectItemzz.ID = percentage.ToString();
                                        }
                                        c++;
                                    }
                                    nlist2.Add(newSearchSelectItemzz);
                                    iC++;
                                }

                                if (pendingPercentage2 > 0)
                                {
                                    tyxValues[tyxValues.Length-1] = pendingPercentage2;
                                    tyyValues[tyyValues.Length-1] = "Remaining";
                                    var newSearchSelectItemzz = new SearchSelectItem();
                                    newSearchSelectItemzz.Name = "Remaining";
                                    newSearchSelectItemzz.ID = pendingPercentage2.ToString();
                                    nlist2.Add(newSearchSelectItemzz);
                                }

                                Chart3.Series["Series1"].Points.DataBindXY(tyyValues, tyxValues);
                            }
                        }

                        var header3 = new PdfPCell();
                        header3.AddElement(new Phrase("Period:", beleventimes));
                        header3.Border = Rectangle.NO_BORDER;
                        headertable.AddCell(header3);

                        var header3a = new PdfPCell();
                        header3a.AddElement(new Phrase(fromdate.ToShortDateString() + " to " + todate.ToShortDateString(), eleventimes));
                        header3a.Border = Rectangle.NO_BORDER;
                        headertable.AddCell(header3a);

                        if (isContract)
                        {
                            var customers = Customer.GetCustomerById(Convert.ToInt32(status), dbConnection);
                            var contract = ContractInfo.GetContractInfoById(Convert.ToInt32(contractId), dbConnection);
                            var header1 = new PdfPCell();
                            header1.AddElement(new Phrase("Contract Name:", beleventimes));
                            header1.Border = Rectangle.NO_BORDER;
                            headertable.AddCell(header1);

                            var header1a = new PdfPCell();
                            header1a.AddElement(new Phrase(contract.ContractRef, eleventimes));
                            header1a.Border = Rectangle.NO_BORDER;
                            headertable.AddCell(header1a);



                            var gCus = Customer.GetCustomerById(Convert.ToInt32(status), dbConnection);
                            var header2 = new PdfPCell();
                            header2.AddElement(new Phrase("Account Name:", beleventimes));
                            header2.Border = Rectangle.NO_BORDER;

                            headertable.AddCell(header2);

                            var header2a = new PdfPCell();
                            header2a.AddElement(new Phrase(customers.ClientName, eleventimes));
                            header2a.Border = Rectangle.NO_BORDER;
                            headertable.AddCell(header2a);


                            var header4 = new PdfPCell();
                            header4.AddElement(new Phrase("Status:", beleventimes));
                            header4.Border = Rectangle.NO_BORDER;
                            headertable.AddCell(header4);

                            var constate = "Inactive";
                            var contype = "";
                            var concreatedby = userinfo.FirstName + " " + userinfo.LastName;

                            if (isContract)
                            {
                                if (contract.Active)
                                {
                                    constate = "Active";
                                }

                                if (contract.ContractType == (int)ContractInfo.ContractTypes.PPM)
                                {
                                    contype = "PPM";

                                    if (contract.SRTotal > 0)
                                        contype = "PPM+SR";
                                }
                                else
                                {
                                    contype = "SR";
                                }

                                concreatedby = contract.CustomerFullName;
                            }
                            else
                            {
                                if (customers.Status)
                                {
                                    constate = "Active";
                                }

                                contype = customers.CustomerType;

                                concreatedby = customers.CustomerFullName;
                            }

                            var header4a = new PdfPCell();
                            header4a.AddElement(new Phrase(constate, eleventimes));
                            header4a.Border = Rectangle.NO_BORDER;
                            headertable.AddCell(header4a);

                            var header5 = new PdfPCell();
                            header5.AddElement(new Phrase("Type:", beleventimes));
                            header5.Border = Rectangle.NO_BORDER;
                            header5.PaddingBottom = 20;
                            headertable.AddCell(header5);

                            var header5a = new PdfPCell();
                            header5a.AddElement(new Phrase(contype, eleventimes));
                            header5a.Border = Rectangle.NO_BORDER;
                            headertable.AddCell(header5a);

                            var headertableMain = new PdfPTable(1);
                            headertableMain.DefaultCell.Border = Rectangle.NO_BORDER;
                            headertableMain.WidthPercentage = 100;

                            headertableMain.AddCell(headertable);

                            doc.Add(headertableMain);

                            var taskdataTable = new PdfPTable(2);
                            var taskdataTableA = new PdfPTable(2);
                            var taskdataTableB = new PdfPTable(2);
                            taskdataTable.DefaultCell.Border = Rectangle.NO_BORDER;
                            taskdataTable.WidthPercentage = 100;

                            taskdataTableA.DefaultCell.Border = Rectangle.NO_BORDER;
                            taskdataTableB.DefaultCell.Border = Rectangle.NO_BORDER;

                            taskdataTableA.WidthPercentage = 100;
                            taskdataTableB.WidthPercentage = 100;

                            taskdataTableA.SetWidths(columnWidths3565);

                            taskdataTableB.SetWidths(columnWidths4060);

                            //SIDE A
                            var headerA1 = new PdfPCell();
                            headerA1.AddElement(new Phrase("Contact Name:", beleventimes));
                            headerA1.Border = Rectangle.NO_BORDER;

                            taskdataTableA.AddCell(headerA1);

                            var headerA1a = new PdfPCell();
                            headerA1a.AddElement(new Phrase(customers.Name, eleventimes));
                            headerA1a.Border = Rectangle.NO_BORDER;
                            taskdataTableA.AddCell(headerA1a);

                            var headerA3 = new PdfPCell();
                            headerA3.AddElement(new Phrase("Contact Number:", beleventimes));
                            headerA3.Border = Rectangle.NO_BORDER;

                            taskdataTableA.AddCell(headerA3);

                            var headerA3a = new PdfPCell();
                            headerA3a.AddElement(new Phrase(customers.ContactNo, eleventimes));
                            headerA3a.Border = Rectangle.NO_BORDER;
                            taskdataTableA.AddCell(headerA3a);

                            var headerA4 = new PdfPCell();
                            headerA4.AddElement(new Phrase("Contact Email:", beleventimes));
                            headerA4.Border = Rectangle.NO_BORDER;
                            taskdataTableA.AddCell(headerA4);

                            var headerA4a = new PdfPCell();
                            headerA4a.AddElement(new Phrase(customers.Email, eleventimes));
                            headerA4a.Border = Rectangle.NO_BORDER;
                            taskdataTableA.AddCell(headerA4a);


                            var headerA5 = new PdfPCell();
                            headerA5.AddElement(new Phrase("Contact Address:", beleventimes));
                            headerA5.Border = Rectangle.NO_BORDER;
                            taskdataTableA.AddCell(headerA5);

                            var headerA5a = new PdfPCell();
                            headerA5a.AddElement(new Phrase(customers.LocationAddress, eleventimes));
                            headerA5a.Border = Rectangle.NO_BORDER;
                            taskdataTableA.AddCell(headerA5a);

                            //SIDE B

                            var headerB1 = new PdfPCell();
                            headerB1.AddElement(new Phrase("Business Unit:", beleventimes));
                            headerB1.Border = Rectangle.NO_BORDER;
                            taskdataTableB.AddCell(headerB1);

                            var headerB1a = new PdfPCell();
                            headerB1a.AddElement(new Phrase(userinfo.SiteName, eleventimes));
                            headerB1a.Border = Rectangle.NO_BORDER;
                            taskdataTableB.AddCell(headerB1a);


                            var headerB2 = new PdfPCell();
                            headerB2.AddElement(new Phrase("Account Manager:", beleventimes));
                            headerB2.Border = Rectangle.NO_BORDER;
                            taskdataTableB.AddCell(headerB2);

                            var headerB2a = new PdfPCell();
                            headerB2a.AddElement(new Phrase(customers.SalesAccount, eleventimes));
                            headerB2a.Border = Rectangle.NO_BORDER;
                            taskdataTableB.AddCell(headerB2a);

                            var headerB3 = new PdfPCell();
                            headerB3.AddElement(new Phrase("Created By:", beleventimes));
                            headerB3.Border = Rectangle.NO_BORDER;
                            taskdataTableB.AddCell(headerB3);

                            var headerB3a = new PdfPCell();
                            headerB3a.AddElement(new Phrase(customers.CustomerFullName, eleventimes));
                            headerB3a.Border = Rectangle.NO_BORDER;
                            taskdataTableB.AddCell(headerB3a);

                            var headerB4 = new PdfPCell();
                            headerB4.AddElement(new Phrase("Since:", beleventimes));
                            headerB4.Border = Rectangle.NO_BORDER;
                            taskdataTableB.AddCell(headerB4);

                            var headerB4a = new PdfPCell();
                            headerB4a.AddElement(new Phrase(customers.CreatedDate.Value.AddHours(userinfo.TimeZone).ToShortDateString(), eleventimes));
                            headerB4a.Border = Rectangle.NO_BORDER;
                            taskdataTableB.AddCell(headerB4a);

                            var headerB5 = new PdfPCell();
                            headerB5.AddElement(new Phrase("Total Contracts:", beleventimes));
                            headerB5.Border = Rectangle.NO_BORDER;
                            headerB5.PaddingBottom = 20;
                            taskdataTableB.AddCell(headerB5);

                            var contractlist = ContractInfo.GetContractInfosByCustomerId(customers.Id, dbConnection);

                            var headerB5a = new PdfPCell();
                            headerB5a.AddElement(new Phrase(contractlist.Count.ToString(), eleventimes));
                            headerB5a.Border = Rectangle.NO_BORDER;
                            taskdataTableB.AddCell(headerB5a);

                            taskdataTable.AddCell(taskdataTableA);
                            taskdataTable.AddCell(taskdataTableB);

                            doc.Add(taskdataTable);
                            // doc.Add(taskdataTable);

                            //CONTRACT INFO

                            var condataTable = new PdfPTable(2);
                            var condataTableA = new PdfPTable(2);
                            var condataTableB = new PdfPTable(2);
                            condataTable.DefaultCell.Border = Rectangle.NO_BORDER;
                            condataTable.WidthPercentage = 100;

                            condataTableA.DefaultCell.Border = Rectangle.NO_BORDER;
                            condataTableB.DefaultCell.Border = Rectangle.NO_BORDER;

                            condataTableA.WidthPercentage = 100;
                            condataTableB.WidthPercentage = 100;


                            condataTableA.SetWidths(columnWidths3565);

                            condataTableB.SetWidths(columnWidths4060);

                            condataTable.AddCell(condataTableA);

                            condataTable.AddCell(condataTableB);
                            //TABLE A CON

                            if (contype == "PPM" || contype == "PPM+SR")
                            {
                                var headerPPM = new PdfPTable(1);
                                headerPPM.DefaultCell.Border = Rectangle.NO_BORDER;
                                headerPPM.WidthPercentage = 100;

                                var hconheaderA1 = new PdfPCell();
                                hconheaderA1.AddElement(new Phrase("PPM:", cbtimes));
                                hconheaderA1.Border = Rectangle.NO_BORDER;
                                headerPPM.AddCell(hconheaderA1);
                                headerPPM.AddCell(condataTable);

                                var conheaderA1 = new PdfPCell();
                                conheaderA1.AddElement(new Phrase("Visits per year:", beleventimes));
                                conheaderA1.Border = Rectangle.NO_BORDER;
                                condataTableA.AddCell(conheaderA1);

                                var conheaderA1a = new PdfPCell();
                                conheaderA1a.AddElement(new Phrase(contract.PPMVisitPerYear.ToString(), eleventimes));
                                conheaderA1a.Border = Rectangle.NO_BORDER;
                                condataTableA.AddCell(conheaderA1a);

                                var conheaderA2 = new PdfPCell();
                                conheaderA2.AddElement(new Phrase("Hours per visit:", beleventimes));
                                conheaderA2.Border = Rectangle.NO_BORDER;
                                condataTableA.AddCell(conheaderA2);

                                var conheaderA2a = new PdfPCell();
                                conheaderA2a.AddElement(new Phrase(contract.PPMTotalHour.ToString(), eleventimes));
                                conheaderA2a.Border = Rectangle.NO_BORDER;
                                condataTableA.AddCell(conheaderA2a);

                                var conheaderA3 = new PdfPCell();
                                conheaderA3.AddElement(new Phrase("Total hours:", beleventimes));
                                conheaderA3.Border = Rectangle.NO_BORDER;
                                condataTableA.AddCell(conheaderA3);

                                var conheaderA3a = new PdfPCell();
                                conheaderA3a.AddElement(new Phrase(contract.TotalPPMHour.ToString(), eleventimes));
                                conheaderA3a.Border = Rectangle.NO_BORDER;
                                condataTableA.AddCell(conheaderA3a);


                                //TABLE B CON

                                var conheaderB1 = new PdfPCell();
                                conheaderB1.AddElement(new Phrase("Remarks:", beleventimes));
                                conheaderB1.Border = Rectangle.NO_BORDER;
                                condataTableB.AddCell(conheaderB1);

                                var conheaderB1a = new PdfPCell();
                                conheaderB1a.AddElement(new Phrase(contract.Remarks, eleventimes));
                                conheaderB1a.Border = Rectangle.NO_BORDER;
                                condataTableB.AddCell(conheaderB1a);



                                doc.Add(headerPPM);

                            }
                            if (contype == "SR")
                            {
                                //SR CONTRACT
                                var srheaderPPM = new PdfPTable(1);
                                srheaderPPM.DefaultCell.Border = Rectangle.NO_BORDER;
                                srheaderPPM.WidthPercentage = 100;

                                var srcondataTable = new PdfPTable(2);
                                var srcondataTableA = new PdfPTable(2);
                                var srcondataTableB = new PdfPTable(2);
                                srcondataTable.DefaultCell.Border = Rectangle.NO_BORDER;
                                srcondataTable.WidthPercentage = 100;

                                srcondataTableA.DefaultCell.Border = Rectangle.NO_BORDER;
                                srcondataTableB.DefaultCell.Border = Rectangle.NO_BORDER;

                                srcondataTableA.WidthPercentage = 100;
                                srcondataTableB.WidthPercentage = 100;



                                srcondataTableA.SetWidths(columnWidths3565);

                                srcondataTableB.SetWidths(columnWidths4060);

                                srcondataTable.AddCell(srcondataTableA);

                                srcondataTable.AddCell(srcondataTableB);


                                var srhconheaderA1 = new PdfPCell();
                                srhconheaderA1.AddElement(new Phrase("SR:", cbtimes));
                                srhconheaderA1.Border = Rectangle.NO_BORDER;
                                srheaderPPM.AddCell(srhconheaderA1);
                                srheaderPPM.AddCell(srcondataTable);


                                var srconheaderA1 = new PdfPCell();
                                srconheaderA1.AddElement(new Phrase("Visits per year:", beleventimes));
                                srconheaderA1.Border = Rectangle.NO_BORDER;
                                srcondataTableA.AddCell(srconheaderA1);

                                var srconheaderA1a = new PdfPCell();
                                srconheaderA1a.AddElement(new Phrase(contract.SRVisitPerYear.ToString(), eleventimes));
                                srconheaderA1a.Border = Rectangle.NO_BORDER;
                                srcondataTableA.AddCell(srconheaderA1a);

                                var srconheaderA2 = new PdfPCell();
                                srconheaderA2.AddElement(new Phrase("Hours per visit:", beleventimes));
                                srconheaderA2.Border = Rectangle.NO_BORDER;
                                srcondataTableA.AddCell(srconheaderA2);

                                var srconheaderA2a = new PdfPCell();
                                srconheaderA2a.AddElement(new Phrase(contract.SRTotalHours.ToString(), eleventimes));
                                srconheaderA2a.Border = Rectangle.NO_BORDER;
                                srcondataTableA.AddCell(srconheaderA2a);

                                var srconheaderA3 = new PdfPCell();
                                srconheaderA3.AddElement(new Phrase("Total hours:", beleventimes));
                                srconheaderA3.Border = Rectangle.NO_BORDER;
                                srcondataTableA.AddCell(srconheaderA3);

                                var srconheaderA3a = new PdfPCell();
                                srconheaderA3a.AddElement(new Phrase(contract.TotalPPMHour.ToString(), eleventimes));
                                srconheaderA3a.Border = Rectangle.NO_BORDER;
                                srcondataTableA.AddCell(srconheaderA3a);

                                var srconheaderA4 = new PdfPCell();
                                srconheaderA4.AddElement(new Phrase("SLA:", beleventimes));
                                srconheaderA4.Border = Rectangle.NO_BORDER;
                                srcondataTableA.AddCell(srconheaderA4);

                                var srconheaderA4a = new PdfPCell();
                                srconheaderA4a.AddElement(new Phrase(contract.SLA, eleventimes));
                                srconheaderA4a.Border = Rectangle.NO_BORDER;
                                srcondataTableA.AddCell(srconheaderA4a);

                                var srconheaderA5 = new PdfPCell();
                                srconheaderA5.AddElement(new Phrase("Scope of work:", beleventimes));
                                srconheaderA5.Border = Rectangle.NO_BORDER;
                                srcondataTableA.AddCell(srconheaderA5);

                                var srconheaderA5a = new PdfPCell();
                                srconheaderA5a.AddElement(new Phrase(contract.ScopeOfWorks, eleventimes));
                                srconheaderA5a.Border = Rectangle.NO_BORDER;
                                srcondataTableA.AddCell(srconheaderA5a);

                                //B 
                                var srconheaderB1 = new PdfPCell();
                                srconheaderB1.AddElement(new Phrase("Annual Contract:", beleventimes));
                                srconheaderB1.Border = Rectangle.NO_BORDER;
                                srcondataTableB.AddCell(srconheaderB1);

                                var srconheaderB1a = new PdfPCell();
                                srconheaderB1a.AddElement(new Phrase(contract.AnnualContractValue, eleventimes));
                                srconheaderB1a.Border = Rectangle.NO_BORDER;
                                srcondataTableB.AddCell(srconheaderB1a);

                                var ssrconheaderB1 = new PdfPCell();
                                ssrconheaderB1.AddElement(new Phrase("Monthly Contract:", beleventimes));
                                ssrconheaderB1.Border = Rectangle.NO_BORDER;
                                srcondataTableB.AddCell(ssrconheaderB1);

                                var ssrconheaderB1a = new PdfPCell();
                                ssrconheaderB1a.AddElement(new Phrase(contract.MonthlyContractValue, eleventimes));
                                ssrconheaderB1a.Border = Rectangle.NO_BORDER;
                                srcondataTableB.AddCell(ssrconheaderB1a);

                                var sssrconheaderB1 = new PdfPCell();
                                sssrconheaderB1.AddElement(new Phrase("Total Contract:", beleventimes));
                                sssrconheaderB1.Border = Rectangle.NO_BORDER;
                                srcondataTableB.AddCell(sssrconheaderB1);

                                var sssrconheaderB1a = new PdfPCell();
                                sssrconheaderB1a.AddElement(new Phrase(contract.TotalContractValue, eleventimes));
                                sssrconheaderB1a.Border = Rectangle.NO_BORDER;
                                srcondataTableB.AddCell(sssrconheaderB1a);


                                var srconheaderB2 = new PdfPCell();
                                srconheaderB2.AddElement(new Phrase("Payment Terms:", beleventimes));
                                srconheaderB2.Border = Rectangle.NO_BORDER;
                                srcondataTableB.AddCell(srconheaderB2);

                                var srconheaderB2a = new PdfPCell();
                                srconheaderB2a.AddElement(new Phrase(contract.PaymentTerms, eleventimes));
                                srconheaderB2a.Border = Rectangle.NO_BORDER;
                                srcondataTableB.AddCell(srconheaderB2a);

                                var srconheaderB3 = new PdfPCell();
                                srconheaderB3.AddElement(new Phrase("Remarks:", beleventimes));
                                srconheaderB3.Border = Rectangle.NO_BORDER;
                                srcondataTableB.AddCell(srconheaderB3);

                                var srconheaderB3a = new PdfPCell();
                                srconheaderB3a.AddElement(new Phrase(contract.Remarks, eleventimes));
                                srconheaderB3a.Border = Rectangle.NO_BORDER;
                                srcondataTableB.AddCell(srconheaderB3a);

                                doc.Add(srheaderPPM);

                            }
                            else if (contype == "PPM+SR")
                            {
                                //SR CONTRACT
                                var srheaderPPM = new PdfPTable(1);
                                srheaderPPM.DefaultCell.Border = Rectangle.NO_BORDER;
                                srheaderPPM.WidthPercentage = 100;

                                var srcondataTable = new PdfPTable(2);
                                var srcondataTableA = new PdfPTable(2);
                                var srcondataTableB = new PdfPTable(2);
                                srcondataTable.DefaultCell.Border = Rectangle.NO_BORDER;
                                srcondataTable.WidthPercentage = 100;

                                srcondataTableA.DefaultCell.Border = Rectangle.NO_BORDER;
                                srcondataTableB.DefaultCell.Border = Rectangle.NO_BORDER;

                                srcondataTableA.WidthPercentage = 100;
                                srcondataTableB.WidthPercentage = 100;



                                srcondataTableA.SetWidths(columnWidths3565);

                                srcondataTableB.SetWidths(columnWidths4060);

                                srcondataTable.AddCell(srcondataTableA);

                                srcondataTable.AddCell(srcondataTableB);


                                var srhconheaderA1 = new PdfPCell();
                                srhconheaderA1.AddElement(new Phrase("SR:", cbtimes));
                                srhconheaderA1.Border = Rectangle.NO_BORDER;
                                srheaderPPM.AddCell(srhconheaderA1);
                                srheaderPPM.AddCell(srcondataTable);

                                var srconheaderA1 = new PdfPCell();
                                srconheaderA1.AddElement(new Phrase("Visits per year:", beleventimes));
                                srconheaderA1.Border = Rectangle.NO_BORDER;
                                srcondataTableA.AddCell(srconheaderA1);

                                var srconheaderA1a = new PdfPCell();
                                srconheaderA1a.AddElement(new Phrase(contract.SRHourVisitPer.ToString(), eleventimes));
                                srconheaderA1a.Border = Rectangle.NO_BORDER;
                                srcondataTableA.AddCell(srconheaderA1a);

                                var srconheaderA2 = new PdfPCell();
                                srconheaderA2.AddElement(new Phrase("Hours per visit:", beleventimes));
                                srconheaderA2.Border = Rectangle.NO_BORDER;
                                srcondataTableA.AddCell(srconheaderA2);

                                var srconheaderA2a = new PdfPCell();
                                srconheaderA2a.AddElement(new Phrase(contract.SRTotalHours.ToString(), eleventimes));
                                srconheaderA2a.Border = Rectangle.NO_BORDER;
                                srcondataTableA.AddCell(srconheaderA2a);

                                var srconheaderA3 = new PdfPCell();
                                srconheaderA3.AddElement(new Phrase("Total hours:", beleventimes));
                                srconheaderA3.Border = Rectangle.NO_BORDER;
                                srcondataTableA.AddCell(srconheaderA3);

                                var srconheaderA3a = new PdfPCell();
                                srconheaderA3a.AddElement(new Phrase(contract.SRVisitPerYear.ToString(), eleventimes));
                                srconheaderA3a.Border = Rectangle.NO_BORDER;
                                srcondataTableA.AddCell(srconheaderA3a);

                                var srconheaderA4 = new PdfPCell();
                                srconheaderA4.AddElement(new Phrase("SLA:", beleventimes));
                                srconheaderA4.Border = Rectangle.NO_BORDER;
                                srcondataTableA.AddCell(srconheaderA4);

                                var srconheaderA4a = new PdfPCell();
                                srconheaderA4a.AddElement(new Phrase(contract.SLA, eleventimes));
                                srconheaderA4a.Border = Rectangle.NO_BORDER;
                                srcondataTableA.AddCell(srconheaderA4a);

                                var srconheaderA5 = new PdfPCell();
                                srconheaderA5.AddElement(new Phrase("Scope of work:", beleventimes));
                                srconheaderA5.Border = Rectangle.NO_BORDER;
                                srcondataTableA.AddCell(srconheaderA5);

                                var srconheaderA5a = new PdfPCell();
                                srconheaderA5a.AddElement(new Phrase(contract.ScopeOfWorks, eleventimes));
                                srconheaderA5a.Border = Rectangle.NO_BORDER;
                                srcondataTableA.AddCell(srconheaderA5a);

                                //B
                                var srconheaderB1 = new PdfPCell();
                                srconheaderB1.AddElement(new Phrase("Annual Contract Value:", beleventimes));
                                srconheaderB1.Border = Rectangle.NO_BORDER;
                                srcondataTableB.AddCell(srconheaderB1);

                                var srconheaderB1a = new PdfPCell();
                                srconheaderB1a.AddElement(new Phrase(contract.AnnualContractValue, eleventimes));
                                srconheaderB1a.Border = Rectangle.NO_BORDER;
                                srcondataTableB.AddCell(srconheaderB1a);

                                var ssrconheaderB1 = new PdfPCell();
                                ssrconheaderB1.AddElement(new Phrase("Monthly Contract Value:", beleventimes));
                                ssrconheaderB1.Border = Rectangle.NO_BORDER;
                                srcondataTableB.AddCell(ssrconheaderB1);

                                var ssrconheaderB1a = new PdfPCell();
                                ssrconheaderB1a.AddElement(new Phrase(contract.MonthlyContractValue, eleventimes));
                                ssrconheaderB1a.Border = Rectangle.NO_BORDER;
                                srcondataTableB.AddCell(ssrconheaderB1a);

                                var sssrconheaderB1 = new PdfPCell();
                                sssrconheaderB1.AddElement(new Phrase("Total Contract Value:", beleventimes));
                                sssrconheaderB1.Border = Rectangle.NO_BORDER;
                                srcondataTableB.AddCell(sssrconheaderB1);

                                var sssrconheaderB1a = new PdfPCell();
                                sssrconheaderB1a.AddElement(new Phrase(contract.TotalContractValue, eleventimes));
                                sssrconheaderB1a.Border = Rectangle.NO_BORDER;
                                srcondataTableB.AddCell(sssrconheaderB1a);


                                var srconheaderB2 = new PdfPCell();
                                srconheaderB2.AddElement(new Phrase("Payment Terms:", beleventimes));
                                srconheaderB2.Border = Rectangle.NO_BORDER;
                                srcondataTableB.AddCell(srconheaderB2);

                                var srconheaderB2a = new PdfPCell();
                                srconheaderB2a.AddElement(new Phrase(contract.PaymentTerms, eleventimes));
                                srconheaderB2a.Border = Rectangle.NO_BORDER;
                                srcondataTableB.AddCell(srconheaderB2a);

                                var srconheaderB3 = new PdfPCell();
                                srconheaderB3.AddElement(new Phrase("Remarks:", beleventimes));
                                srconheaderB3.Border = Rectangle.NO_BORDER;
                                srcondataTableB.AddCell(srconheaderB3);

                                var srconheaderB3a = new PdfPCell();
                                srconheaderB3a.AddElement(new Phrase(contract.Remarks, eleventimes));
                                srconheaderB3a.Border = Rectangle.NO_BORDER;
                                srcondataTableB.AddCell(srconheaderB3a);

                                doc.Add(srheaderPPM);

                            }
                            //Hours
                            //doc.NewPage();
                            var hheaderTB = new PdfPTable(1);
                            hheaderTB.DefaultCell.Border = Rectangle.NO_BORDER;
                            hheaderTB.WidthPercentage = 100;

                            var hcondataTable = new PdfPTable(3);
                            var hcondataTableA = new PdfPTable(2);
                            var hcondataTableB = new PdfPTable(1);
                            hcondataTable.DefaultCell.Border = Rectangle.NO_BORDER;
                            hcondataTable.WidthPercentage = 100;
                            hcondataTable.SetWidths(columnWidths7030);

                            hcondataTableA.DefaultCell.Border = Rectangle.NO_BORDER;
                            hcondataTableB.DefaultCell.Border = Rectangle.NO_BORDER;

                            hcondataTableA.WidthPercentage = 100;
                            hcondataTableB.WidthPercentage = 100;

                            hcondataTableA.SetWidths(columnWidths3565);

                            hcondataTable.AddCell(hcondataTableA);

                            using (MemoryStream memoryStream2 = new MemoryStream())
                            {
                                Chart2.SaveImage(memoryStream2, ChartImageFormat.Png);
                                iTextSharp.text.Image img2 = iTextSharp.text.Image.GetInstance(memoryStream2.GetBuffer());
                                //img2.ScalePercent(50f);
                                img2.Alignment = iTextSharp.text.Image.ALIGN_CENTER;
                                //img2.ScaleToFit(100f,100f);
                                hcondataTableB.AddCell(img2);
                            }

                            hcondataTable.AddCell(hcondataTableB);
                            hcondataTable.AddCell(new Paragraph(1, "\u00a0"));
                            hheaderTB.AddCell(hcondataTable);

                            var hhconheaderA1 = new PdfPCell();
                            hhconheaderA1.AddElement(new Phrase("Hours:", cbtimes));
                            hhconheaderA1.Border = Rectangle.NO_BORDER;
                            hhconheaderA1.PaddingTop = 10;
                            hhconheaderA1.Colspan = 2;
                            hcondataTableA.AddCell(hhconheaderA1);

                            var ahconheaderA1 = new PdfPCell();
                            ahconheaderA1.AddElement(new Phrase("Contracted Hours:", beleventimes));
                            ahconheaderA1.Border = Rectangle.NO_BORDER;
                            hcondataTableA.AddCell(ahconheaderA1);

                            var ahconheaderA1a = new PdfPCell();
                            ahconheaderA1a.AddElement(new Phrase(contracthours.ToString(), eleventimes));
                            ahconheaderA1a.Border = Rectangle.NO_BORDER;
                            hcondataTableA.AddCell(ahconheaderA1a);

                            var ahconheaderA2 = new PdfPCell();
                            ahconheaderA2.AddElement(new Phrase("Remaining Hours:", beleventimes));
                            ahconheaderA2.Border = Rectangle.NO_BORDER;
                            hcondataTableA.AddCell(ahconheaderA2);

                            var ahconheaderA2a = new PdfPCell();
                            ahconheaderA2a.AddElement(new Phrase(rremaining.ToString(), eleventimes));
                            ahconheaderA2a.Border = Rectangle.NO_BORDER;
                            hcondataTableA.AddCell(ahconheaderA2a);

                            var ahconheaderA3 = new PdfPCell();
                            ahconheaderA3.AddElement(new Phrase("Consumed Hours:", beleventimes));
                            ahconheaderA3.Border = Rectangle.NO_BORDER;
                            hcondataTableA.AddCell(ahconheaderA3);

                            var ahconheaderA3a = new PdfPCell();
                            ahconheaderA3a.AddElement(new Phrase(rconsumed.ToString(), eleventimes));
                            ahconheaderA3a.Border = Rectangle.NO_BORDER;
                            hcondataTableA.AddCell(ahconheaderA3a);

                            if (Convert.ToInt32(excessH) > 0)
                            {
                                var ahconheaderA4 = new PdfPCell();
                                ahconheaderA4.AddElement(new Phrase("Excess Hours:", beleventimes));
                                ahconheaderA4.Border = Rectangle.NO_BORDER;
                                hcondataTableA.AddCell(ahconheaderA4);

                                var ahconheaderA4a = new PdfPCell();
                                ahconheaderA4a.AddElement(new Phrase(excessH, eleventimes));
                                ahconheaderA4a.Border = Rectangle.NO_BORDER;
                                hcondataTableA.AddCell(ahconheaderA4a);
                            }

                            doc.Add(hheaderTB);

                            var hheaderTBX = new PdfPTable(1);
                            hheaderTBX.DefaultCell.Border = Rectangle.NO_BORDER;
                            hheaderTBX.WidthPercentage = 100;

                            var hcondataTableX = new PdfPTable(3);
                            var hcondataTableAX = new PdfPTable(2);
                            var hcondataTableBX = new PdfPTable(1);
                            hcondataTableX.DefaultCell.Border = Rectangle.NO_BORDER;
                            hcondataTableX.WidthPercentage = 100;
                            hcondataTableX.SetWidths(columnWidths7030);

                            hcondataTableAX.DefaultCell.Border = Rectangle.NO_BORDER;
                            hcondataTableBX.DefaultCell.Border = Rectangle.NO_BORDER;

                            hcondataTableAX.WidthPercentage = 100;
                            hcondataTableBX.WidthPercentage = 100;

                            hcondataTableAX.SetWidths(columnWidths3565);

                            hcondataTableX.AddCell(hcondataTableAX);

                            using (MemoryStream memoryStream2 = new MemoryStream())
                            {
                                Chart3.SaveImage(memoryStream2, ChartImageFormat.Png);
                                iTextSharp.text.Image img2 = iTextSharp.text.Image.GetInstance(memoryStream2.GetBuffer());
                                img2.Alignment = iTextSharp.text.Image.ALIGN_CENTER;
                                hcondataTableBX.AddCell(img2);
                            }

                            hcondataTableX.AddCell(hcondataTableBX);
                            hcondataTableX.AddCell(new Paragraph(1, "\u00a0"));
                            hheaderTBX.AddCell(hcondataTableX);

                            var hhconheaderA1X = new PdfPCell();
                            hhconheaderA1X.AddElement(new Phrase("Consumption Breakdown:", cbtimes));
                            hhconheaderA1X.Border = Rectangle.NO_BORDER;
                            hhconheaderA1X.PaddingTop = 10;
                            hhconheaderA1X.Colspan = 2;
                            hcondataTableAX.AddCell(hhconheaderA1X);
                            var less1 = new List<SearchSelectItem>();
                            foreach (var nt in nlist2)
                            {
                                if (Convert.ToInt32(nt.ID) > 0)
                                {
                                    var ahconheaderA4 = new PdfPCell();
                                ahconheaderA4.AddElement(new Phrase(nt.Name+":", beleventimes));
                                ahconheaderA4.Border = Rectangle.NO_BORDER;
                                hcondataTableAX.AddCell(ahconheaderA4);
                                
                                    var ahconheaderA4a = new PdfPCell();
                                    ahconheaderA4a.AddElement(new Phrase(nt.ID + "%", eleventimes));
                                    ahconheaderA4a.Border = Rectangle.NO_BORDER;
                                    hcondataTableAX.AddCell(ahconheaderA4a);
                                }
                                else
                                {
                                    less1.Add(nt);
                                }
                            }
                            if(less1.Count > 0)
                            {
                                var hhconheaderA1XX = new PdfPCell();
                                hhconheaderA1XX.AddElement(new Phrase("Less Than 1 Percent:", cbtimes));
                                hhconheaderA1XX.Border = Rectangle.NO_BORDER;
                                hhconheaderA1XX.PaddingTop = 10;
                                hhconheaderA1XX.Colspan = 2;
                                hcondataTableAX.AddCell(hhconheaderA1XX);
                                foreach(var nt in less1)
                                {
                                    var ahconheaderA4 = new PdfPCell();
                                    ahconheaderA4.AddElement(new Phrase(nt.Name, beleventimes));
                                    ahconheaderA4.Border = Rectangle.NO_BORDER;
                                    ahconheaderA4.Colspan = 2;
                                    hcondataTableAX.AddCell(ahconheaderA4);
                                }
                            }

                            doc.Add(hheaderTBX);
                        }
                        else if (isCustomer)
                        {
                            var customers = Customer.GetCustomerById(Convert.ToInt32(status), dbConnection);

                            var header2 = new PdfPCell();
                            header2.AddElement(new Phrase("Account Name:", beleventimes));
                            header2.Border = Rectangle.NO_BORDER;

                            headertable.AddCell(header2);

                            var header2a = new PdfPCell();
                            header2a.AddElement(new Phrase(customers.ClientName, eleventimes));
                            header2a.Border = Rectangle.NO_BORDER;
                            headertable.AddCell(header2a);

                            if (isProject)
                            {
                                var projinfo = Project.GetProjectById(Convert.ToInt32(projId), dbConnection);

                                var pheader2 = new PdfPCell();
                                pheader2.AddElement(new Phrase("Project Name:", beleventimes));
                                pheader2.Border = Rectangle.NO_BORDER;

                                headertable.AddCell(pheader2);

                                var pheader2a = new PdfPCell();
                                pheader2a.AddElement(new Phrase(projinfo.Name, eleventimes));
                                pheader2a.Border = Rectangle.NO_BORDER;
                                headertable.AddCell(pheader2a);
                            }




                            var header4 = new PdfPCell();
                            header4.AddElement(new Phrase("Status:", beleventimes));
                            header4.Border = Rectangle.NO_BORDER;
                            headertable.AddCell(header4);

                            var constate = "Inactive";
                            var contype = "";
                            var concreatedby = userinfo.Username;

                            if (customers.Status)
                            {
                                constate = "Active";
                            }

                            contype = customers.CustomerType;

                            concreatedby = customers.CreatedBy;


                            var header4a = new PdfPCell();
                            header4a.AddElement(new Phrase(constate, eleventimes));
                            header4a.Border = Rectangle.NO_BORDER;
                            headertable.AddCell(header4a);

                            var header5 = new PdfPCell();
                            header5.AddElement(new Phrase("Type:", beleventimes));
                            header5.Border = Rectangle.NO_BORDER;
                            header5.PaddingBottom = 20;
                            headertable.AddCell(header5);

                            var header5a = new PdfPCell();
                            header5a.AddElement(new Phrase(contype, eleventimes));
                            header5a.Border = Rectangle.NO_BORDER;
                            headertable.AddCell(header5a);

                            var headertableMain = new PdfPTable(1);
                            headertableMain.DefaultCell.Border = Rectangle.NO_BORDER;
                            headertableMain.WidthPercentage = 100;

                            headertableMain.AddCell(headertable);

                            doc.Add(headertableMain);

                            var taskdataTable = new PdfPTable(2);
                            var taskdataTableA = new PdfPTable(2);
                            var taskdataTableB = new PdfPTable(2);
                            taskdataTable.DefaultCell.Border = Rectangle.NO_BORDER;
                            taskdataTable.WidthPercentage = 100;

                            taskdataTableA.DefaultCell.Border = Rectangle.NO_BORDER;
                            taskdataTableB.DefaultCell.Border = Rectangle.NO_BORDER;

                            taskdataTableA.WidthPercentage = 100;
                            taskdataTableB.WidthPercentage = 100;

                            taskdataTableA.SetWidths(columnWidths3565);

                            taskdataTableB.SetWidths(columnWidths4060);

                            //SIDE A
                            var headerA1 = new PdfPCell();
                            headerA1.AddElement(new Phrase("Contact Name:", beleventimes));
                            headerA1.Border = Rectangle.NO_BORDER;

                            taskdataTableA.AddCell(headerA1);

                            var headerA1a = new PdfPCell();
                            headerA1a.AddElement(new Phrase(customers.Name, eleventimes));
                            headerA1a.Border = Rectangle.NO_BORDER;
                            taskdataTableA.AddCell(headerA1a);

                            var headerA3 = new PdfPCell();
                            headerA3.AddElement(new Phrase("Contact Number:", beleventimes));
                            headerA3.Border = Rectangle.NO_BORDER;

                            taskdataTableA.AddCell(headerA3);

                            var headerA3a = new PdfPCell();
                            headerA3a.AddElement(new Phrase(customers.ContactNo, eleventimes));
                            headerA3a.Border = Rectangle.NO_BORDER;
                            taskdataTableA.AddCell(headerA3a);

                            var headerA4 = new PdfPCell();
                            headerA4.AddElement(new Phrase("Contact Email:", beleventimes));
                            headerA4.Border = Rectangle.NO_BORDER;
                            taskdataTableA.AddCell(headerA4);

                            var headerA4a = new PdfPCell();
                            headerA4a.AddElement(new Phrase(customers.Email, eleventimes));
                            headerA4a.Border = Rectangle.NO_BORDER;
                            taskdataTableA.AddCell(headerA4a);


                            var headerA5 = new PdfPCell();
                            headerA5.AddElement(new Phrase("Contact Address:", beleventimes));
                            headerA5.Border = Rectangle.NO_BORDER;
                            taskdataTableA.AddCell(headerA5);

                            var headerA5a = new PdfPCell();
                            headerA5a.AddElement(new Phrase(customers.LocationAddress, eleventimes));
                            headerA5a.Border = Rectangle.NO_BORDER;
                            taskdataTableA.AddCell(headerA5a);

                            //SIDE B

                            var headerB1 = new PdfPCell();
                            headerB1.AddElement(new Phrase("Business Unit:", beleventimes));
                            headerB1.Border = Rectangle.NO_BORDER;
                            taskdataTableB.AddCell(headerB1);

                            var headerB1a = new PdfPCell();
                            headerB1a.AddElement(new Phrase(userinfo.SiteName, eleventimes));
                            headerB1a.Border = Rectangle.NO_BORDER;
                            taskdataTableB.AddCell(headerB1a);


                            var headerB2 = new PdfPCell();
                            headerB2.AddElement(new Phrase("Account Manager:", beleventimes));
                            headerB2.Border = Rectangle.NO_BORDER;
                            taskdataTableB.AddCell(headerB2);

                            var headerB2a = new PdfPCell();
                            headerB2a.AddElement(new Phrase(customers.SalesAccount, eleventimes));
                            headerB2a.Border = Rectangle.NO_BORDER;
                            taskdataTableB.AddCell(headerB2a);

                            var headerB3 = new PdfPCell();
                            headerB3.AddElement(new Phrase("Created By:", beleventimes));
                            headerB3.Border = Rectangle.NO_BORDER;
                            taskdataTableB.AddCell(headerB3);

                            var headerB3a = new PdfPCell();
                            headerB3a.AddElement(new Phrase(customers.CustomerFullName, eleventimes));
                            headerB3a.Border = Rectangle.NO_BORDER;
                            taskdataTableB.AddCell(headerB3a);

                            var headerB4 = new PdfPCell();
                            headerB4.AddElement(new Phrase("Since:", beleventimes));
                            headerB4.Border = Rectangle.NO_BORDER;
                            taskdataTableB.AddCell(headerB4);

                            var headerB4a = new PdfPCell();
                            headerB4a.AddElement(new Phrase(customers.CreatedDate.Value.AddHours(userinfo.TimeZone).ToShortDateString(), eleventimes));
                            headerB4a.Border = Rectangle.NO_BORDER;
                            taskdataTableB.AddCell(headerB4a);

                            var headerB5 = new PdfPCell();
                            headerB5.AddElement(new Phrase("Total Contracts:", beleventimes));
                            headerB5.Border = Rectangle.NO_BORDER;
                            headerB5.PaddingBottom = 20;
                            taskdataTableB.AddCell(headerB5);

                            var contractlist = ContractInfo.GetContractInfosByCustomerId(customers.Id, dbConnection);

                            var headerB5a = new PdfPCell();
                            headerB5a.AddElement(new Phrase(contractlist.Count.ToString(), eleventimes));
                            headerB5a.Border = Rectangle.NO_BORDER;
                            taskdataTableB.AddCell(headerB5a);

                            taskdataTable.AddCell(taskdataTableA);
                            taskdataTable.AddCell(taskdataTableB);

                            doc.Add(taskdataTable);
                        }
                        else if (isProject)
                        {
                            var project = Project.GetProjectById(Convert.ToInt32(projId), dbConnection);

                            var header2 = new PdfPCell();
                            header2.AddElement(new Phrase("Project Name:", beleventimes));
                            header2.Border = Rectangle.NO_BORDER;

                            headertable.AddCell(header2);

                            var header2a = new PdfPCell();
                            header2a.AddElement(new Phrase(project.Name, eleventimes));
                            header2a.Border = Rectangle.NO_BORDER;
                            headertable.AddCell(header2a);


                            if (project.CustomerId > 0)
                            {
                                var header5 = new PdfPCell();
                                header5.AddElement(new Phrase("Customer:", beleventimes));
                                header5.Border = Rectangle.NO_BORDER;
                                header5.PaddingBottom = 20;
                                headertable.AddCell(header5);

                                var header5a = new PdfPCell();
                                header5a.AddElement(new Phrase(project.CustomerName, eleventimes));
                                header5a.Border = Rectangle.NO_BORDER;
                                headertable.AddCell(header5a);

                            }

                            var headertableMain = new PdfPTable(1);
                            headertableMain.DefaultCell.Border = Rectangle.NO_BORDER;
                            headertableMain.WidthPercentage = 100;

                            headertableMain.AddCell(headertable);

                            doc.Add(headertableMain);

                            var taskdataTable = new PdfPTable(2);
                            var taskdataTableA = new PdfPTable(2);
                            var taskdataTableB = new PdfPTable(2);
                            taskdataTable.DefaultCell.Border = Rectangle.NO_BORDER;
                            taskdataTable.WidthPercentage = 100;

                            taskdataTableA.DefaultCell.Border = Rectangle.NO_BORDER;
                            taskdataTableB.DefaultCell.Border = Rectangle.NO_BORDER;

                            taskdataTableA.WidthPercentage = 100;
                            taskdataTableB.WidthPercentage = 100;

                            taskdataTableA.SetWidths(columnWidths3565);

                            taskdataTableB.SetWidths(columnWidths3070);

                            //SIDE A
                            var headerA1 = new PdfPCell();
                            headerA1.AddElement(new Phrase("Project Manager:", beleventimes));
                            headerA1.Border = Rectangle.NO_BORDER;

                            taskdataTableA.AddCell(headerA1);

                            var headerA1a = new PdfPCell();
                            headerA1a.AddElement(new Phrase(project.FName + " " + project.LName, eleventimes));
                            headerA1a.Border = Rectangle.NO_BORDER;
                            taskdataTableA.AddCell(headerA1a);

                            var headerA3 = new PdfPCell();
                            headerA3.AddElement(new Phrase("Project Start:", beleventimes));
                            headerA3.Border = Rectangle.NO_BORDER;

                            taskdataTableA.AddCell(headerA3);

                            var headerA3a = new PdfPCell();
                            headerA3a.AddElement(new Phrase(project.StartDate.Value.ToShortDateString(), eleventimes));
                            headerA3a.Border = Rectangle.NO_BORDER;
                            taskdataTableA.AddCell(headerA3a);

                            var headerA4 = new PdfPCell();
                            headerA4.AddElement(new Phrase("Project End:", beleventimes));
                            headerA4.Border = Rectangle.NO_BORDER;
                            taskdataTableA.AddCell(headerA4);

                            var headerA4a = new PdfPCell();
                            headerA4a.AddElement(new Phrase(project.EndDate.Value.ToShortDateString(), eleventimes));
                            headerA4a.Border = Rectangle.NO_BORDER;
                            taskdataTableA.AddCell(headerA4a);


                            //SIDE B 

                            var headerB3 = new PdfPCell();
                            headerB3.AddElement(new Phrase("Created By:", beleventimes));
                            headerB3.Border = Rectangle.NO_BORDER;
                            taskdataTableB.AddCell(headerB3);

                            var headerB3a = new PdfPCell();
                            headerB3a.AddElement(new Phrase(project.FName + " " + project.LName, eleventimes));
                            headerB3a.Border = Rectangle.NO_BORDER;
                            taskdataTableB.AddCell(headerB3a);

                            var headerB4 = new PdfPCell();
                            headerB4.AddElement(new Phrase("Since:", beleventimes));
                            headerB4.Border = Rectangle.NO_BORDER;
                            headerB4.PaddingBottom = 20;
                            taskdataTableB.AddCell(headerB4);

                            var headerB4a = new PdfPCell();
                            headerB4a.AddElement(new Phrase(project.CreatedDate.Value.AddHours(userinfo.TimeZone).ToShortDateString(), eleventimes));
                            headerB4a.Border = Rectangle.NO_BORDER;
                            taskdataTableB.AddCell(headerB4a);

                            taskdataTable.AddCell(taskdataTableA);
                            taskdataTable.AddCell(taskdataTableB);

                            doc.Add(taskdataTable);
                        }
                        else
                        {
                            var headertableMain = new PdfPTable(1);
                            headertableMain.DefaultCell.Border = Rectangle.NO_BORDER;
                            headertableMain.WidthPercentage = 100;

                            headertableMain.AddCell(headertable);
                            doc.Add(headertableMain);
                        }
                        //TASK
                        var ispen = true;
                        var isprog = true;
                        var iscomp = true;
                        var isacc = true;
                        var showAll = true;
                        if (!string.IsNullOrEmpty(statusId))
                        {
                            if (CommonUtility.isNumeric(statusId))
                            {
                                if (Convert.ToInt32(statusId) > -1)
                                {
                                    if (Convert.ToInt32(statusId) == (int)TaskStatus.Pending)
                                    {
                                        ispen = true;
                                        isprog = false;
                                        iscomp = false;
                                        isacc = false;
                                        showAll = false;
                                    }
                                    else if (Convert.ToInt32(statusId) == (int)TaskStatus.InProgress || Convert.ToInt32(statusId) == (int)TaskStatus.OnRoute)
                                    {
                                        ispen = false;
                                        isprog = true;
                                        iscomp = false;
                                        isacc = false;
                                        showAll = false;
                                    }
                                    else if (Convert.ToInt32(statusId) == (int)TaskStatus.Completed)
                                    {
                                        ispen = false;
                                        isprog = false;
                                        iscomp = true;
                                        isacc = false;
                                        showAll = false;
                                    }
                                    else if (Convert.ToInt32(statusId) == (int)TaskStatus.Accepted)
                                    {
                                        ispen = false;
                                        isprog = false;
                                        iscomp = false;
                                        isacc = true;
                                        showAll = false;
                                    }
                                }
                            }
                        }


                        var taskheaderTB = new PdfPTable(1);
                        taskheaderTB.DefaultCell.Border = Rectangle.NO_BORDER;
                        taskheaderTB.WidthPercentage = 100;

                        var tcondataTable = new PdfPTable(3);
                        var tcondataTableA = new PdfPTable(2);
                        var tcondataTableB = new PdfPTable(1);
                        tcondataTable.DefaultCell.Border = Rectangle.NO_BORDER;
                        tcondataTable.WidthPercentage = 100;



                        tcondataTable.SetWidths(columnWidths7030);

                        tcondataTableA.DefaultCell.Border = Rectangle.NO_BORDER;
                        tcondataTableB.DefaultCell.Border = Rectangle.NO_BORDER;

                        tcondataTableA.WidthPercentage = 100;
                        tcondataTableB.WidthPercentage = 100;

                        tcondataTableA.SetWidths(columnWidths3565);

                        tcondataTable.AddCell(tcondataTableA);

                        taskheaderTB.AddCell(tcondataTable);

                        Chart1.Series[0].Points[0].Color = System.Drawing.ColorTranslator.FromHtml("#c8234a");
                        Chart1.Series[0].Points[1].Color = System.Drawing.ColorTranslator.FromHtml("#f2c400");                      //#f2c40
                        Chart1.Series[0].Points[2].Color = System.Drawing.ColorTranslator.FromHtml("#3ebb64");  //3ebb64

                        using (MemoryStream memoryStream = new MemoryStream())
                        {
                            Chart1.SaveImage(memoryStream, ChartImageFormat.Png);
                            iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance(memoryStream.GetBuffer());
                            //img.ScalePercent(12f);
                            img.Alignment = iTextSharp.text.Image.ALIGN_CENTER;
                            tcondataTableB.AddCell(img);
                        }
                        tcondataTable.AddCell(tcondataTableB);
                        tcondataTable.AddCell(new Paragraph(1, "\u00a0"));

                        var tconheaderA1 = new PdfPCell();
                        tconheaderA1.AddElement(new Phrase("Tasks:", cbtimes));
                        tconheaderA1.Border = Rectangle.NO_BORDER;
                        tconheaderA1.Colspan = 2;
                        tcondataTableA.AddCell(tconheaderA1);

                        if (ispen)
                        {
                            var thconheaderA1 = new PdfPCell();
                            thconheaderA1.AddElement(new Phrase("Pending Tasks:", beleventimes));
                            thconheaderA1.Border = Rectangle.NO_BORDER;
                            tcondataTableA.AddCell(thconheaderA1);

                            var thconheaderA1a = new PdfPCell();
                            thconheaderA1a.AddElement(new Phrase(tPending.Count.ToString(), eleventimes));
                            thconheaderA1a.Border = Rectangle.NO_BORDER;
                            tcondataTableA.AddCell(thconheaderA1a);
                        }
                        if (isprog)
                        {
                            var thconheaderA2 = new PdfPCell();
                            thconheaderA2.AddElement(new Phrase("Tasks in Progress:", beleventimes));
                            thconheaderA2.Border = Rectangle.NO_BORDER;
                            tcondataTableA.AddCell(thconheaderA2);

                            var thconheaderA2a = new PdfPCell();
                            thconheaderA2a.AddElement(new Phrase(tInProgress.Count.ToString(), eleventimes));
                            thconheaderA2a.Border = Rectangle.NO_BORDER;
                            tcondataTableA.AddCell(thconheaderA2a);
                        }
                        if (iscomp)
                        {
                            var thconheaderA3 = new PdfPCell();
                            thconheaderA3.AddElement(new Phrase("Completed:", beleventimes));
                            thconheaderA3.Border = Rectangle.NO_BORDER;
                            tcondataTableA.AddCell(thconheaderA3);

                            var thconheaderA3a = new PdfPCell();
                            thconheaderA3a.AddElement(new Phrase(tCompleted.Count.ToString(), eleventimes));
                            thconheaderA3a.Border = Rectangle.NO_BORDER;
                            tcondataTableA.AddCell(thconheaderA3a);

                        }

                        if (isacc)
                        {
                            var thconheaderA4 = new PdfPCell();
                            thconheaderA4.AddElement(new Phrase("Accepted:", beleventimes));
                            thconheaderA4.Border = Rectangle.NO_BORDER;
                            tcondataTableA.AddCell(thconheaderA4);

                            var thconheaderA4a = new PdfPCell();
                            thconheaderA4a.AddElement(new Phrase(tAccepted.Count.ToString(), eleventimes));
                            thconheaderA4a.Border = Rectangle.NO_BORDER;
                            tcondataTableA.AddCell(thconheaderA4a);

                        }
                        if (showAll)
                        {
                            var thconheaderA5 = new PdfPCell();
                            thconheaderA5.AddElement(new Phrase("Total:", beleventimes));
                            thconheaderA5.Border = Rectangle.NO_BORDER;
                            tcondataTableA.AddCell(thconheaderA5);

                            var thconheaderA5a = new PdfPCell();
                            thconheaderA5a.AddElement(new Phrase(ttotal.ToString(), eleventimes));
                            thconheaderA5a.Border = Rectangle.NO_BORDER;
                            tcondataTableA.AddCell(thconheaderA5a);
                        }

                        doc.Add(taskheaderTB);

                        //TASK TABLES PENDING
                        float[] columnWidthsCHK = new float[] { 15, 15, 24, 23, 23 };

                        if (ispen)
                        {
                            //var checklistHeaderTable = new PdfPTable(1);
                            //checklistHeaderTable.DefaultCell.Border = Rectangle.NO_BORDER;
                            //checklistHeaderTable.WidthPercentage = 100;

                            //checklistHeaderTable.AddCell(cheader1);

                            var newchecklistTable = new PdfPTable(5);
                            newchecklistTable.DefaultCell.Border = Rectangle.NO_BORDER;
                            newchecklistTable.WidthPercentage = 100;
                            newchecklistTable.SetWidths(columnWidthsCHK);

                            var cheader1 = new PdfPCell();
                            cheader1.AddElement(new Phrase("Pending Tasks", cbtimes));
                            cheader1.Border = Rectangle.NO_BORDER;
                            cheader1.PaddingTop = 10;
                            cheader1.PaddingBottom = 10;
                            cheader1.Colspan = 5;
                            newchecklistTable.AddCell(cheader1);

                            var ncaheader1 = new PdfPCell();
                            ncaheader1.AddElement(new Phrase("ID", weleventimes));
                            ncaheader1.Border = Rectangle.NO_BORDER;
                            ncaheader1.PaddingLeft = 5;
                            ncaheader1.PaddingBottom = 5;
                            ncaheader1.BackgroundColor = Color.GRAY;

                            newchecklistTable.AddCell(ncaheader1);

                            var ncaheader2 = new PdfPCell();
                            ncaheader2.AddElement(new Phrase("Type", weleventimes));
                            ncaheader2.Border = Rectangle.NO_BORDER;
                            ncaheader2.BackgroundColor = Color.GRAY;
                            newchecklistTable.AddCell(ncaheader2);

                            var ncaheader3 = new PdfPCell();
                            ncaheader3.AddElement(new Phrase("Name", weleventimes));
                            ncaheader3.Border = Rectangle.NO_BORDER;
                            ncaheader3.BackgroundColor = Color.GRAY;
                            newchecklistTable.AddCell(ncaheader3);

                            var ncaheader4 = new PdfPCell();
                            ncaheader4.AddElement(new Phrase("Start", weleventimes));
                            ncaheader4.Border = Rectangle.NO_BORDER;
                            ncaheader4.BackgroundColor = Color.GRAY;
                            newchecklistTable.AddCell(ncaheader4);

                            var ncaheader5 = new PdfPCell();
                            ncaheader5.AddElement(new Phrase("End", weleventimes));
                            ncaheader5.Border = Rectangle.NO_BORDER;
                            ncaheader5.BackgroundColor = Color.GRAY;
                            newchecklistTable.AddCell(ncaheader5);

                            var odd = false;
                            foreach (var device in tPending)
                            {
                                var xheaderCell = new PdfPCell(new Phrase(device.NewCustomerTaskId, tentimes));
                                var xheaderCell2 = new PdfPCell(new Phrase(device.TaskTypeName, tentimes));
                                var xheaderCell3 = new PdfPCell(new Phrase(device.Name, tentimes));
                                var sd = device.StartDate.Value.AddHours(userinfo.TimeZone).ToString();
                                var ed = device.EndDate.Value.AddHours(userinfo.TimeZone).ToString();

                                var xheaderCell4 = new PdfPCell(new Phrase(sd, tentimes));
                                var xheaderCell5 = new PdfPCell(new Phrase(ed, tentimes));

                                if (!odd)
                                {
                                    xheaderCell.BackgroundColor = Color.LIGHT_GRAY;
                                    xheaderCell2.BackgroundColor = Color.LIGHT_GRAY;
                                    xheaderCell3.BackgroundColor = Color.LIGHT_GRAY;
                                    xheaderCell4.BackgroundColor = Color.LIGHT_GRAY;
                                    xheaderCell5.BackgroundColor = Color.LIGHT_GRAY;
                                    odd = true;
                                }
                                else
                                {
                                    odd = false;
                                }

                                xheaderCell.Border = Rectangle.NO_BORDER;
                                xheaderCell2.Border = Rectangle.NO_BORDER;
                                xheaderCell3.Border = Rectangle.NO_BORDER;
                                xheaderCell4.Border = Rectangle.NO_BORDER;
                                xheaderCell5.Border = Rectangle.NO_BORDER;

                                newchecklistTable.AddCell(xheaderCell);
                                newchecklistTable.AddCell(xheaderCell2);
                                newchecklistTable.AddCell(xheaderCell3);
                                newchecklistTable.AddCell(xheaderCell4);
                                newchecklistTable.AddCell(xheaderCell5);

                            }
                            //checklistHeaderTable.AddCell(newchecklistTable);
                            doc.Add(newchecklistTable);
                        }
                        //Tasks in Progress
                        if (isprog)
                        {
                            //var tipTable = new PdfPTable(1);
                            //tipTable.DefaultCell.Border = Rectangle.NO_BORDER;
                            //tipTable.WidthPercentage = 100;

                            //tipTable.AddCell(tipheader1);

                            var tiplistTable = new PdfPTable(5);
                            tiplistTable.DefaultCell.Border = Rectangle.NO_BORDER;
                            tiplistTable.WidthPercentage = 100;
                            tiplistTable.SetWidths(columnWidthsCHK);

                            var tipheader1 = new PdfPCell();
                            tipheader1.AddElement(new Phrase("Tasks in Progress", cbtimes));
                            tipheader1.Border = Rectangle.NO_BORDER;
                            tipheader1.PaddingTop = 10;
                            tipheader1.PaddingBottom = 10;
                            tipheader1.Colspan = 5;
                            tiplistTable.AddCell(tipheader1);

                            var tipaheader1 = new PdfPCell();
                            tipaheader1.AddElement(new Phrase("ID", weleventimes));
                            tipaheader1.Border = Rectangle.NO_BORDER;
                            tipaheader1.PaddingLeft = 5;
                            tipaheader1.PaddingBottom = 5;
                            tipaheader1.BackgroundColor = Color.GRAY;
                            tiplistTable.AddCell(tipaheader1);

                            var tipaheader2 = new PdfPCell();
                            tipaheader2.AddElement(new Phrase("Type", weleventimes));
                            tipaheader2.Border = Rectangle.NO_BORDER;
                            tipaheader2.BackgroundColor = Color.GRAY;
                            tiplistTable.AddCell(tipaheader2);

                            var tipaheader3 = new PdfPCell();
                            tipaheader3.AddElement(new Phrase("Name", weleventimes));
                            tipaheader3.Border = Rectangle.NO_BORDER;
                            tipaheader3.BackgroundColor = Color.GRAY;
                            tiplistTable.AddCell(tipaheader3);

                            var tipaheader4 = new PdfPCell();
                            tipaheader4.AddElement(new Phrase("Started", weleventimes));
                            tipaheader4.Border = Rectangle.NO_BORDER;
                            tipaheader4.BackgroundColor = Color.GRAY;
                            tiplistTable.AddCell(tipaheader4);

                            var tipaheader5 = new PdfPCell();
                            tipaheader5.AddElement(new Phrase("Scheduled", weleventimes));
                            tipaheader5.Border = Rectangle.NO_BORDER;
                            tipaheader5.BackgroundColor = Color.GRAY;
                            tiplistTable.AddCell(tipaheader5);

                            var odd = false;
                            foreach (var device in tInProgress)
                            {
                                var xheaderCell = new PdfPCell(new Phrase(device.NewCustomerTaskId, tentimes));
                                var xheaderCell2 = new PdfPCell(new Phrase(device.TaskTypeName, tentimes));
                                var xheaderCell3 = new PdfPCell(new Phrase(device.Name, tentimes));

                                var sd = device.StartDate.Value.AddHours(userinfo.TimeZone).ToString();
                                if (device.ActualStartDate != null)
                                    sd = device.ActualStartDate.Value.AddHours(userinfo.TimeZone).ToString();


                                var ed = device.StartDate.Value.AddHours(userinfo.TimeZone).ToString();

                                var xheaderCell4 = new PdfPCell(new Phrase(sd, tentimes));
                                var xheaderCell5 = new PdfPCell(new Phrase(ed, tentimes));

                                if (!odd)
                                {
                                    xheaderCell.BackgroundColor = Color.LIGHT_GRAY;
                                    xheaderCell2.BackgroundColor = Color.LIGHT_GRAY;
                                    xheaderCell3.BackgroundColor = Color.LIGHT_GRAY;
                                    xheaderCell4.BackgroundColor = Color.LIGHT_GRAY;
                                    xheaderCell5.BackgroundColor = Color.LIGHT_GRAY;
                                    odd = true;
                                }
                                else
                                {
                                    odd = false;
                                }
                                xheaderCell.Border = Rectangle.NO_BORDER;
                                xheaderCell2.Border = Rectangle.NO_BORDER;
                                xheaderCell3.Border = Rectangle.NO_BORDER;
                                xheaderCell4.Border = Rectangle.NO_BORDER;
                                xheaderCell5.Border = Rectangle.NO_BORDER;

                                tiplistTable.AddCell(xheaderCell);
                                tiplistTable.AddCell(xheaderCell2);
                                tiplistTable.AddCell(xheaderCell3);
                                tiplistTable.AddCell(xheaderCell4);
                                tiplistTable.AddCell(xheaderCell5);

                            }
                            //tipTable.AddCell(tiplistTable);
                            doc.Add(tiplistTable);
                        }
                        //Completed Tasks
                        if (iscomp)
                        {
                            //var compTable = new PdfPTable(1);
                            //compTable.DefaultCell.Border = Rectangle.NO_BORDER;
                            //compTable.WidthPercentage = 100;

                            var complistTable = new PdfPTable(5);
                            complistTable.DefaultCell.Border = Rectangle.NO_BORDER;
                            complistTable.WidthPercentage = 100;
                            complistTable.SetWidths(columnWidthsCHK);

                            var compheader1 = new PdfPCell();
                            compheader1.AddElement(new Phrase("Completed Tasks", cbtimes));
                            compheader1.Border = Rectangle.NO_BORDER;
                            compheader1.PaddingTop = 10;
                            compheader1.PaddingBottom = 10;
                            compheader1.Colspan = 5;
                            complistTable.AddCell(compheader1);


                            var compaheader1 = new PdfPCell();
                            compaheader1.AddElement(new Phrase("ID", weleventimes));
                            compaheader1.Border = Rectangle.NO_BORDER;
                            compaheader1.PaddingLeft = 5;
                            compaheader1.PaddingBottom = 5;
                            compaheader1.BackgroundColor = Color.GRAY;
                            complistTable.AddCell(compaheader1);

                            var compaheader2 = new PdfPCell();
                            compaheader2.AddElement(new Phrase("Type", weleventimes));
                            compaheader2.Border = Rectangle.NO_BORDER;
                            compaheader2.BackgroundColor = Color.GRAY;
                            complistTable.AddCell(compaheader2);

                            var compaheader3 = new PdfPCell();
                            compaheader3.AddElement(new Phrase("Name", weleventimes));
                            compaheader3.Border = Rectangle.NO_BORDER;
                            compaheader3.BackgroundColor = Color.GRAY;
                            complistTable.AddCell(compaheader3);

                            var compaheader4 = new PdfPCell();
                            compaheader4.AddElement(new Phrase("Started", weleventimes));
                            compaheader4.Border = Rectangle.NO_BORDER;
                            compaheader4.BackgroundColor = Color.GRAY;
                            complistTable.AddCell(compaheader4);

                            var compaheader5 = new PdfPCell();
                            compaheader5.AddElement(new Phrase("Completed", weleventimes));
                            compaheader5.Border = Rectangle.NO_BORDER;
                            compaheader5.BackgroundColor = Color.GRAY;
                            complistTable.AddCell(compaheader5);

                            var odd = false;
                            foreach (var device in tCompleted)
                            {
                                var xheaderCell = new PdfPCell(new Phrase(device.NewCustomerTaskId, tentimes));
                                var xheaderCell2 = new PdfPCell(new Phrase(device.TaskTypeName, tentimes));
                                var xheaderCell3 = new PdfPCell(new Phrase(device.Name, tentimes));

                                var sd = string.Empty;

                                if (device.ActualStartDate != null)
                                    sd = device.ActualStartDate.Value.AddHours(userinfo.TimeZone).ToString();

                                var xheaderCell4 = new PdfPCell(new Phrase(sd, tentimes));

                                var ed = string.Empty;

                                if (device.ActualEndDate != null)
                                    ed = device.ActualEndDate.Value.AddHours(userinfo.TimeZone).ToString();

                                var xheaderCell5 = new PdfPCell(new Phrase(ed, tentimes));


                                if (!odd)
                                {
                                    xheaderCell.BackgroundColor = Color.LIGHT_GRAY;
                                    xheaderCell2.BackgroundColor = Color.LIGHT_GRAY;
                                    xheaderCell3.BackgroundColor = Color.LIGHT_GRAY;
                                    xheaderCell4.BackgroundColor = Color.LIGHT_GRAY;
                                    xheaderCell5.BackgroundColor = Color.LIGHT_GRAY;
                                    odd = true;
                                }
                                else
                                {
                                    odd = false;
                                }
                                xheaderCell.Border = Rectangle.NO_BORDER;
                                xheaderCell2.Border = Rectangle.NO_BORDER;
                                xheaderCell3.Border = Rectangle.NO_BORDER;
                                xheaderCell4.Border = Rectangle.NO_BORDER;
                                xheaderCell5.Border = Rectangle.NO_BORDER;

                                complistTable.AddCell(xheaderCell);
                                complistTable.AddCell(xheaderCell2);
                                complistTable.AddCell(xheaderCell3);
                                complistTable.AddCell(xheaderCell4);
                                complistTable.AddCell(xheaderCell5);

                            }
                            //compTable.AddCell(complistTable);
                            doc.Add(complistTable);
                        }
                        //Accepted Tasks
                        if (isacc)
                        {
                            //var acpTable = new PdfPTable(1);
                            //acpTable.DefaultCell.Border = Rectangle.NO_BORDER;
                            //acpTable.WidthPercentage = 100;

                            var acplistTable = new PdfPTable(5);
                            acplistTable.DefaultCell.Border = Rectangle.NO_BORDER;
                            acplistTable.WidthPercentage = 100;
                            acplistTable.SetWidths(columnWidthsCHK);

                            var aacpheader1 = new PdfPCell();
                            aacpheader1.AddElement(new Phrase("Accepted Tasks", cbtimes));
                            aacpheader1.Border = Rectangle.NO_BORDER;
                            aacpheader1.PaddingTop = 10;
                            aacpheader1.PaddingBottom = 10;
                            aacpheader1.Colspan = 5;
                            acplistTable.AddCell(aacpheader1);

                            var acpheader1 = new PdfPCell();
                            acpheader1.AddElement(new Phrase("ID", weleventimes));
                            acpheader1.Border = Rectangle.NO_BORDER;
                            acpheader1.PaddingLeft = 5;
                            acpheader1.PaddingBottom = 5;
                            acpheader1.BackgroundColor = Color.GRAY;
                            acplistTable.AddCell(acpheader1);

                            var acpheader2 = new PdfPCell();
                            acpheader2.AddElement(new Phrase("Type", weleventimes));
                            acpheader2.Border = Rectangle.NO_BORDER;
                            acpheader2.BackgroundColor = Color.GRAY;
                            acplistTable.AddCell(acpheader2);

                            var acpheader3 = new PdfPCell();
                            acpheader3.AddElement(new Phrase("Name", weleventimes));
                            acpheader3.Border = Rectangle.NO_BORDER;
                            acpheader3.BackgroundColor = Color.GRAY;
                            acplistTable.AddCell(acpheader3);

                            var acpheader4 = new PdfPCell();
                            acpheader4.AddElement(new Phrase("Started", weleventimes));
                            acpheader4.Border = Rectangle.NO_BORDER;
                            acpheader4.BackgroundColor = Color.GRAY;
                            acplistTable.AddCell(acpheader4);

                            var acpheader5 = new PdfPCell();
                            acpheader5.AddElement(new Phrase("Completed", weleventimes));
                            acpheader5.Border = Rectangle.NO_BORDER;
                            acpheader5.BackgroundColor = Color.GRAY;
                            acplistTable.AddCell(acpheader5);

                            var odd = false;
                            foreach (var device in tAccepted)
                            {
                                var xheaderCell = new PdfPCell(new Phrase(device.NewCustomerTaskId, tentimes));
                                var xheaderCell2 = new PdfPCell(new Phrase(device.TaskTypeName, tentimes));
                                var xheaderCell3 = new PdfPCell(new Phrase(device.Name, tentimes));

                                var sd = string.Empty;

                                if (device.ActualStartDate != null)
                                    sd = device.ActualStartDate.Value.AddHours(userinfo.TimeZone).ToString();

                                var xheaderCell4 = new PdfPCell(new Phrase(sd, tentimes));

                                var ed = string.Empty;

                                if (device.ActualEndDate != null)
                                    ed = device.ActualEndDate.Value.AddHours(userinfo.TimeZone).ToString();

                                var xheaderCell5 = new PdfPCell(new Phrase(ed, tentimes));


                                if (!odd)
                                {
                                    xheaderCell.BackgroundColor = Color.LIGHT_GRAY;
                                    xheaderCell2.BackgroundColor = Color.LIGHT_GRAY;
                                    xheaderCell3.BackgroundColor = Color.LIGHT_GRAY;
                                    xheaderCell4.BackgroundColor = Color.LIGHT_GRAY;
                                    xheaderCell5.BackgroundColor = Color.LIGHT_GRAY;
                                    odd = true;
                                }
                                else
                                {
                                    odd = false;
                                }
                                xheaderCell.Border = Rectangle.NO_BORDER;
                                xheaderCell2.Border = Rectangle.NO_BORDER;
                                xheaderCell3.Border = Rectangle.NO_BORDER;
                                xheaderCell4.Border = Rectangle.NO_BORDER;
                                xheaderCell5.Border = Rectangle.NO_BORDER;

                                acplistTable.AddCell(xheaderCell);
                                acplistTable.AddCell(xheaderCell2);
                                acplistTable.AddCell(xheaderCell3);
                                acplistTable.AddCell(xheaderCell4);
                                acplistTable.AddCell(xheaderCell5);

                            }
                            //  acpTable.AddCell(acplistTable);
                            doc.Add(acplistTable);
                        }
                        //
                        Font geleventimes = new Font(f_cn, 11, Font.NORMAL, Color.GRAY);
                        var endTable = new PdfPTable(1);
                        endTable.DefaultCell.Border = Rectangle.NO_BORDER;
                        endTable.WidthPercentage = 100;

                        var endheader = new PdfPCell();
                        endheader.AddElement(new Phrase("Report generated: " + CommonUtility.getDTNow().AddHours(userinfo.TimeZone).ToString() + " 	by: " + userinfo.Username, geleventimes));
                        endheader.Border = Rectangle.NO_BORDER;
                        endTable.AddCell(endheader);
                        doc.Add(endTable);

                        doc.Close();



                        Response.ContentType = "application/pdf";
                        Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
                        Response.Cache.SetCacheability(HttpCacheability.NoCache);
                        Response.Write(doc);
                        Response.End();
                    }
                    else
                    {
                        Response.Write("<script>alert('No entries found')</script>");
                    }
                }
            }
            catch (Exception ex)
            {
                if (userinfo != null)
                    MIMSLog.MIMSLogSave("Reports", "btnExport_Click", ex, dbConnection, userinfo.SiteId, userinfo.CustomerInfoId);
                else
                    MIMSLog.MIMSLogSave("Reports", "btnExport_Click", ex, dbConnection);
            }
        }


        protected void btnExport2_Click(object sender, EventArgs e)
        {
            var userinfo = Users.GetUserByName(User.Identity.Name, dbConnection);
            try
            {
                //using (MemoryStream memoryStream = new MemoryStream())
                {
                    string toDate = toHD.Value; //String.Format("{0}", Request.Form["toDate"]);
                    string fromDate = fromHD.Value; // String.Format("{0}", Request.Form["fromDate"]);
                    var status = lfstatus2.Value;
                    var ttype = type1.Value;
                    var compare = comparison1.Value;
                    var year1 = yearly1.Value;
                    var year2 = yearly2.Value;
                    var quater1 = qt1.Value;
                    var quarter2 = qt2.Value;

                    if (compare == "Yearly")
                    {
                        quater1 = "";
                        quarter2 = "";
                    }
                    var todate = Convert.ToDateTime(toDate);
                    var fromdate = Convert.ToDateTime(fromDate);
                     
                    //var itemsLostFound = ItemFound.GetAllItemFoundByDateRange(Convert.ToDateTime(fromDate).ToString(), Convert.ToDateTime(toDate).AddDays(1).ToString(), dbConnection);

                    var itemsLostFound = ItemFound.GetAllItemFound(dbConnection);


                    if (userinfo.RoleId != (int)Role.SuperAdmin && userinfo.RoleId != (int)Role.ChiefOfficer)
                    {
                        if (userinfo.RoleId == (int)Role.Manager || userinfo.RoleId == (int)Role.Director || userinfo.RoleId == (int)Role.Admin)
                            itemsLostFound = itemsLostFound.Where(i => i.SiteId == userinfo.SiteId).ToList();
                        else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                        {
                            itemsLostFound = itemsLostFound.Where(i => i.CustomerId == userinfo.CustomerInfoId).ToList();
                        }
                        else if (userinfo.RoleId == (int)Role.Regional)
                        {
                            //var sites = UserSiteMap.GetAllUserSiteMapByUsername(userinfo.Username, dbConnection);
                            //var tempItemstLostFound = new List<ItemFound>();
                            //foreach (var site in sites)
                            //    tempItemstLostFound.AddRange(itemsLostFound.Where(i => i.SiteId == site.SiteId).ToList());

                            //tempItemstLostFound.AddRange(itemsLostFound.Where(i => i.SiteId == userinfo.SiteId).ToList());
                            itemsLostFound = ItemFound.GetItemFoundByLevel5(userinfo.ID, dbConnection);
                        }
                    }

                    if (status != "Comparison")
                    {
                        itemsLostFound = itemsLostFound.Where(i => Convert.ToDateTime(i.DateFound) > Convert.ToDateTime(fromDate) && Convert.ToDateTime(i.DateFound) < Convert.ToDateTime(toDate).AddDays(1)).ToList();
                    }
                    var grouped = itemsLostFound.GroupBy(item => item.Id);
                    itemsLostFound = grouped.Select(grp => grp.OrderBy(item => item.Id).First()).ToList();
                    itemsLostFound = itemsLostFound.OrderBy(i => i.Status).ToList();
                    if (itemsLostFound.Count > 0)
                    {
                        var loghistories = itemsLostFound;
                        var comparisonlist = itemsLostFound;
                        var range1 = new List<ItemFound>();
                        var range2 = new List<ItemFound>();
                        var var1 = new List<ItemFound>();
                        var var2 = new List<ItemFound>();
                        if (status == "Comparison")
                        {

                            //year1 quater1 compare ttype
                            var dt1 = DateTime.Now;
                            var dt2 = DateTime.Now;
                            if (compare == "Yearly")
                            {
                                dt1 = new DateTime(Convert.ToInt32(year1), 1, 1);
                                dt2 = new DateTime(Convert.ToInt32(year2), 1, 1);

                                range1 = comparisonlist.Where(i => Convert.ToDateTime(i.DateFound) > dt1 && Convert.ToDateTime(i.DateFound) < dt1.AddMonths(12)).ToList();
                                range2 = comparisonlist.Where(i => Convert.ToDateTime(i.DateFound) > dt2 && Convert.ToDateTime(i.DateFound) < dt2.AddMonths(12)).ToList();


                                if (ttype == "Valuable/Non-Valuable")
                                {
                                    var1 = range1.Where(i => i.Type == "Valuable" || i.Type == "Valuables").ToList();
                                    var2 = range2.Where(i => i.Type == "Valuable" || i.Type == "Valuables").ToList();


                                }
                                else if (ttype == "Delivered/Not-Delivered")
                                {
                                    var1 = range1.Where(i => i.Status == "Found").ToList();
                                    var2 = range2.Where(i => i.Status == "Found").ToList();
                                }
                            }
                            else if (compare == "Quarterly")
                            {
                                if (quater1 == "Q1 (Jan-Mar)")
                                {
                                    dt1 = new DateTime(Convert.ToInt32(year1), 1, 1);
                                }
                                else if (quater1 == "Q2 (Apr-Jun)")
                                {
                                    dt1 = new DateTime(Convert.ToInt32(year1), 4, 1);
                                }
                                else if (quater1 == "Q3 (Jul-Sep)")
                                {
                                    dt1 = new DateTime(Convert.ToInt32(year1), 7, 1);
                                }
                                else if (quater1 == "Q4 (Oct-Dec)")
                                {
                                    dt1 = new DateTime(Convert.ToInt32(year1), 10, 1);
                                }

                                if (quarter2 == "Q1 (Jan-Mar)")
                                {
                                    dt2 = new DateTime(Convert.ToInt32(year2), 1, 1);
                                }
                                else if (quarter2 == "Q2 (Apr-Jun)")
                                {
                                    dt2 = new DateTime(Convert.ToInt32(year2), 4, 1);
                                }
                                else if (quarter2 == "Q3 (Jul-Sep)")
                                {
                                    dt2 = new DateTime(Convert.ToInt32(year2), 7, 1);
                                }
                                else if (quarter2 == "Q4 (Oct-Dec)")
                                {
                                    dt2 = new DateTime(Convert.ToInt32(year2), 10, 1);
                                }
                                range1 = comparisonlist.Where(i => Convert.ToDateTime(i.DateFound) > dt1 && Convert.ToDateTime(i.DateFound) < dt1.AddMonths(3)).ToList();
                                range2 = comparisonlist.Where(i => Convert.ToDateTime(i.DateFound) > dt2 && Convert.ToDateTime(i.DateFound) < dt2.AddMonths(3)).ToList();

                                if (ttype == "Valuable/Non-Valuable")
                                {
                                    var1 = range1.Where(i => i.Type == "Valuable" || i.Type == "Valuables").ToList();
                                    var2 = range2.Where(i => i.Type == "Valuable" || i.Type == "Valuables").ToList();
                                }
                                else if (ttype == "Delivered/Not-Delivered")
                                {
                                    var1 = range1.Where(i => i.Status == "Found").ToList();
                                    var2 = range2.Where(i => i.Status == "Found").ToList();
                                }
                            }
                        }

                        var mimssettings = MIMSConfigSetting.GetMIMSConfigSettings(dbConnection);

                        var preappPath = mimssettings.FilePath.Substring(0, mimssettings.FilePath.Length - 5);

                        string appPath = preappPath + "\\PDFReports";
                        string path = appPath;
                        /*Logo Path*/

                        string strLogoPath2 = CommonUtility.PhysicalPath("~/Images/custom-images") + "\\site-login-logo.png";

                        iTextSharp.text.Document doc = new iTextSharp.text.Document(PageSize.LETTER, 25F, 25F, 50F, 25F);

                        doc.SetMargins(25, 25, 65, 35);

                        var fileName = DateTime.Now.Day.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Year.ToString() + "-" + DateTime.Now.Hour.ToString() + "-" + DateTime.Now.Minute.ToString() + "-" + DateTime.Now.Second.ToString() + ".pdf";
                        path = path + "\\" + fileName;

                        PdfWriter writer = PdfWriter.GetInstance(doc, Response.OutputStream);//PdfWriter.GetInstance(doc, new FileStream(path, FileMode.Create));
                        writer.PageEvent = new ITextEvents()
                        {
                            cid = userinfo.CustomerInfoId
                        };
                        doc.Open();


                        BaseFont f_cn = BaseFont.CreateFont(Environment.GetFolderPath(Environment.SpecialFolder.Fonts) + "\\verdana.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        // BaseFont h_cn = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        BaseFont z_cn = BaseFont.CreateFont(BaseFont.ZAPFDINGBATS, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        Font times = new Font(f_cn, 12, Font.NORMAL, Color.BLACK);
                        Font btimes = new Font(f_cn, 12, Font.BOLD, Color.BLACK);

                        iTextSharp.text.Color arrowred = new iTextSharp.text.Color(162, 0, 46);

                        Font eleventimes = new Font(f_cn, 11, Font.NORMAL, Color.BLACK);
                        Font tentimes = new Font(f_cn, 10, Font.NORMAL, Color.BLACK);

                        Font eighttimes = new Font(f_cn, 8, Font.NORMAL, Color.BLACK);

                        Font weleventimes = new Font(f_cn, 11, Font.NORMAL, Color.WHITE);

                        Font wnine = new Font(f_cn, 9, Font.NORMAL, Color.WHITE);

                        Font gweb = new Font(z_cn, 11, Font.NORMAL, Color.GREEN);
                        Font rweb = new Font(z_cn, 11, Font.NORMAL, Color.RED);

                        Font ueleventimes = new Font(f_cn, 11, Font.UNDERLINE, Color.BLACK);

                        Font beleventimes = new Font(f_cn, 11, Font.BOLD, Color.BLACK);
                        Font bredeleventimes = new Font(f_cn, 11, Font.BOLD, Color.RED);
                        Font redeleventimes = new Font(f_cn, 11, Font.NORMAL, Color.RED);
                        Font twelvetimes = new Font(f_cn, 12, Font.ITALIC, Color.BLACK);

                        Font btentimes = new Font(f_cn, 11, Font.NORMAL, Color.BLACK);

                        Font headerf = new Font(f_cn, 9, Font.NORMAL, Color.BLACK);

                        Font mbtimes = new Font(f_cn, 18, Font.BOLD, arrowred);
                        Font cbtimes = new Font(f_cn, 13, Font.BOLD, arrowred);

                        float[] columnWidths1585 = new float[] { 15, 85 };
                        float[] columnWidths2080 = new float[] { 20, 80 };

                        float[] columnWidths1783 = new float[] { 18, 82 };

                        float[] columnWidths2575 = new float[] { 25, 75 };
                        float[] columnWidths3070 = new float[] { 30, 70 };
                        float[] columnWidths3565 = new float[] { 35, 65 };
                        float[] columnWidths4555 = new float[] { 45, 55 };
                        float[] columnWidthsH = new float[] { 25, 60, 15 };
                        float[] columnWidths7030 = new float[] { 50, 30, 20 };
                        if (string.IsNullOrEmpty(userinfo.SiteName))
                            userinfo.SiteName = "N/A";

                        var mainheadertable = new PdfPTable(3);
                        mainheadertable.DefaultCell.Border = Rectangle.NO_BORDER;
                        mainheadertable.WidthPercentage = 100;
                        mainheadertable.SetWidths(columnWidthsH);
                        var headerMain = new PdfPCell();
                        if (userinfo.CustomerInfoId == 87 || userinfo.CustomerInfoId == 45)
                        {
                            headerMain.AddElement(new Phrase("G4S LOST AND FOUND REPORT", mbtimes));
                        }
                        else
                        {
                            headerMain.AddElement(new Phrase("MIMS LOST AND FOUND REPORT", mbtimes));
                        }
                        headerMain.PaddingBottom = 10;
                        headerMain.HorizontalAlignment = 1;
                        headerMain.Border = Rectangle.NO_BORDER;
                        PdfPCell pdfCell1 = new PdfPCell();
                        pdfCell1.Border = Rectangle.NO_BORDER;
                        PdfPCell pdfCell2 = new PdfPCell();
                        pdfCell2.Border = Rectangle.NO_BORDER;
                        mainheadertable.AddCell(pdfCell1);
                        mainheadertable.AddCell(headerMain);
                        mainheadertable.AddCell(pdfCell2);

                        doc.Add(mainheadertable);

                        var headertable = new PdfPTable(2);
                        headertable.DefaultCell.Border = Rectangle.NO_BORDER;
                        headertable.WidthPercentage = 100;
                        headertable.SetWidths(columnWidths1783);

                        var header1 = new PdfPCell();
                        header1.AddElement(new Phrase("Report Type:", beleventimes));
                        header1.Border = Rectangle.NO_BORDER;
                        headertable.AddCell(header1);

                        var header1a = new PdfPCell();
                        header1a.AddElement(new Phrase(status, eleventimes));
                        header1a.Border = Rectangle.NO_BORDER;
                        headertable.AddCell(header1a);

                        var header3 = new PdfPCell();
                        header3.AddElement(new Phrase("Period:", beleventimes));
                        header3.Border = Rectangle.NO_BORDER;
                        headertable.AddCell(header3);

                        var header3a = new PdfPCell();
                        header3a.AddElement(new Phrase(fromDate + " to " + toDate, eleventimes));
                        header3a.Border = Rectangle.NO_BORDER;
                        headertable.AddCell(header3a);

                        //var header2 = new PdfPCell();
                        //header2.AddElement(new Phrase("Created By:", beleventimes));
                        //header2.Border = Rectangle.NO_BORDER;

                        //headertable.AddCell(header2);

                        //var header2a = new PdfPCell();
                        //header2a.AddElement(new Phrase(userinfo.CustomerUName, eleventimes));
                        //header2a.Border = Rectangle.NO_BORDER;
                        //headertable.AddCell(header2a);

                        //var header5 = new PdfPCell();
                        //header5.AddElement(new Phrase("Created Date:", beleventimes));
                        //header5.Border = Rectangle.NO_BORDER;
                        //header5.PaddingBottom = 20;
                        //headertable.AddCell(header5);

                        //var header5a = new PdfPCell();
                        //header5a.AddElement(new Phrase(CommonUtility.getDTNow().AddHours(userinfo.TimeZone).ToString(), eleventimes));
                        //header5a.Border = Rectangle.NO_BORDER;
                        //headertable.AddCell(header5a);

                        doc.Add(headertable);

                        var rG = loghistories.Where(i => i.Status == "Return").ToList();
                        var fG = loghistories.Where(i => i.Status == "Found").ToList();
                        var dG = loghistories.Where(i => i.Status == "Donate").ToList();
                        var disP = loghistories.Where(i => i.Status == "Dispose").ToList();

                        var valuables = loghistories.Where(i => i.Type == "Valuable" || i.Type == "Valuables").ToList();
                        var retvaluables = new List<ItemFound>();
                        if (status == "Valuable Returned/Not-Returned")
                        {
                            retvaluables = valuables.Where(i => i.Status == "Found").ToList();
                        }
                        else if (status == "Charity Returned/Not-Returned")
                        {
                            valuables = loghistories.Where(i => i.Type == "Charity").ToList();
                            retvaluables = valuables.Where(i => i.Status == "Found").ToList();
                        }

                        var itemDs = ItemDispose.GetAllItemDispose(dbConnection);
                        var collection = new List<ItemDispose>();
                        var aPose = new List<ItemDispose>();
                        var dPose = new List<ItemDispose>();
                        foreach (var dis in disP)
                        {
                            var item = itemDs.Where(i => i.ItemReference == dis.Reference).ToList();
                            collection.AddRange(item);
                        }
                        if (collection.Count > 0)
                        {
                            aPose = collection.Where(i => i.DisposedTo == "Authorities").ToList();

                            dPose = collection.Where(i => i.DisposedTo == "Destroyed").ToList();
                        }

                        var condataTable = new PdfPTable(3);
                        var condataTableA = new PdfPTable(2);
                        var condataTableB = new PdfPTable(1);
                        condataTable.DefaultCell.Border = Rectangle.NO_BORDER;
                        condataTable.WidthPercentage = 100;


                        condataTable.SetWidths(columnWidths7030);

                        condataTableA.DefaultCell.Border = Rectangle.NO_BORDER;
                        condataTableB.DefaultCell.Border = Rectangle.NO_BORDER;

                        condataTableA.WidthPercentage = 100;
                        condataTableB.WidthPercentage = 70;

                        condataTableA.SetWidths(columnWidths3565);

                        //condataTableB.SetWidths(columnWidths3070);

                        condataTable.AddCell(condataTableA);

                        var headerPPM = new PdfPTable(1);
                        headerPPM.DefaultCell.Border = Rectangle.NO_BORDER;
                        headerPPM.WidthPercentage = 100;

                        var hconheaderA1 = new PdfPCell();
                        hconheaderA1.AddElement(new Phrase("Status:", cbtimes));
                        hconheaderA1.Border = Rectangle.NO_BORDER;
                        hconheaderA1.Colspan = 2;
                        condataTableA.AddCell(hconheaderA1);


                        var conheaderA1 = new PdfPCell();
                        conheaderA1.AddElement(new Phrase("Returned:", beleventimes));
                        conheaderA1.Border = Rectangle.NO_BORDER;
                        condataTableA.AddCell(conheaderA1);

                        var conheaderA1a = new PdfPCell();
                        conheaderA1a.AddElement(new Phrase(rG.Count.ToString(), eleventimes));
                        conheaderA1a.Border = Rectangle.NO_BORDER;
                        condataTableA.AddCell(conheaderA1a);

                        var conheaderA2 = new PdfPCell();
                        conheaderA2.AddElement(new Phrase("Dispose/Destroyed:", beleventimes));
                        conheaderA2.Border = Rectangle.NO_BORDER;
                        condataTableA.AddCell(conheaderA2);

                        var conheaderA2a = new PdfPCell();
                        conheaderA2a.AddElement(new Phrase(dPose.Count.ToString(), eleventimes));
                        conheaderA2a.Border = Rectangle.NO_BORDER;
                        condataTableA.AddCell(conheaderA2a);

                        var conheaderA3 = new PdfPCell();
                        conheaderA3.AddElement(new Phrase("Donate/Charity:", beleventimes));
                        conheaderA3.Border = Rectangle.NO_BORDER;
                        condataTableA.AddCell(conheaderA3);

                        var conheaderA3a = new PdfPCell();
                        conheaderA3a.AddElement(new Phrase(dG.Count.ToString(), eleventimes));
                        conheaderA3a.Border = Rectangle.NO_BORDER;
                        condataTableA.AddCell(conheaderA3a);

                        var conheaderB1 = new PdfPCell();
                        conheaderB1.AddElement(new Phrase("Dispose/Authorities:", beleventimes));
                        conheaderB1.Border = Rectangle.NO_BORDER;
                        condataTableA.AddCell(conheaderB1);

                        var conheaderB1a = new PdfPCell();
                        conheaderB1a.AddElement(new Phrase(aPose.Count.ToString(), eleventimes));
                        conheaderB1a.Border = Rectangle.NO_BORDER;
                        condataTableA.AddCell(conheaderB1a);

                        var conheaderB2 = new PdfPCell();
                        conheaderB2.AddElement(new Phrase("Found/Unclaimed:", beleventimes));
                        conheaderB2.Border = Rectangle.NO_BORDER;
                        condataTableA.AddCell(conheaderB2);

                        var conheaderB2a = new PdfPCell();
                        conheaderB2a.AddElement(new Phrase(fG.Count.ToString(), eleventimes));
                        conheaderB2a.Border = Rectangle.NO_BORDER;
                        condataTableA.AddCell(conheaderB2a);

                        var conheaderB3 = new PdfPCell();
                        conheaderB3.AddElement(new Phrase("Total:", beleventimes));
                        conheaderB3.Border = Rectangle.NO_BORDER;
                        condataTableA.AddCell(conheaderB3);

                        var conheaderB3a = new PdfPCell();
                        conheaderB3a.AddElement(new Phrase(loghistories.Count.ToString(), eleventimes));
                        conheaderB3a.Border = Rectangle.NO_BORDER;
                        condataTableA.AddCell(conheaderB3a);

                        //doc.Add(headerPPM);


                        double[] yValues = new double[5]; //{ rG.Count, aPose.Count, dG.Count, dPose.Count, fG.Count };
                        string[] xValues = new string[5];//{ "Returned", "Disposed", "Charity", "Authorities", "Found" };

                        double[] vyValues = new double[2]; //{ rG.Count, aPose.Count, dG.Count, dPose.Count, fG.Count };
                        string[] vxValues = new string[2];

                        double[] yyValues = new double[2]; //{ rG.Count, aPose.Count, dG.Count, dPose.Count, fG.Count };
                        string[] yxValues = new string[2];

                        if (status == "Valuable/Non-Valuable")
                        {
                            if (valuables.Count > 0)
                            {
                                vyValues[0] = valuables.Count;
                                vxValues[0] = "Valuable";

                                vyValues[1] = loghistories.Count - valuables.Count;
                                vxValues[1] = "Non-Valuable";
                            }
                            else
                            {
                                vyValues[0] = 0;
                                vxValues[0] = "Valuable";

                                vyValues[1] = loghistories.Count;
                                vxValues[1] = "Non-Valuable";
                            }
                        }
                        else if (status == "Valuable Returned/Not-Returned")
                        {
                            if (retvaluables.Count > 0)
                            {
                                vyValues[0] = retvaluables.Count;
                                vxValues[0] = "Not-Returned";

                                vyValues[1] = valuables.Count - retvaluables.Count;
                                vxValues[1] = "Returned";
                            }
                            else
                            {
                                vyValues[0] = 0;
                                vxValues[0] = "Not-Returned";

                                vyValues[1] = valuables.Count;
                                vxValues[1] = "Returned";
                            }
                        }
                        else if (status == "Charity Returned/Not-Returned")
                        {
                            if (retvaluables.Count > 0)
                            {
                                vyValues[0] = retvaluables.Count;
                                vxValues[0] = "Not-Returned";

                                vyValues[1] = valuables.Count - retvaluables.Count;
                                vxValues[1] = "Returned";
                            }
                            else
                            {
                                vyValues[0] = 0;
                                vxValues[0] = "Not-Returned";

                                vyValues[1] = valuables.Count;
                                vxValues[1] = "Returned";
                            }
                        }
                        else if (status == "Alcohol Disposed")
                        {

                        }
                        else if (status == "Delivered/Not-Delivered")
                        {
                            valuables = loghistories.Where(i => i.Status == "Found").ToList();

                            if (valuables.Count > 0)
                            {
                                vyValues[0] = valuables.Count;
                                vxValues[0] = "Not-Delivered";

                                vyValues[1] = loghistories.Count - valuables.Count;
                                vxValues[1] = "Delivered";
                            }
                            else
                            {
                                vyValues[0] = 0;
                                vxValues[0] = "Not-Delivered";

                                vyValues[1] = loghistories.Count;
                                vxValues[1] = "Delivered";
                            }
                        }
                        else if (status == "All")
                        {
                            if (dPose.Count > 0)
                            {
                                yValues[3] = dPose.Count;
                                xValues[3] = "Destroyed";
                            }
                            else
                            {
                                yValues[3] = 0;
                                xValues[3] = "Destroyed";
                            }

                            if (fG.Count > 0)
                            {
                                yValues[4] = fG.Count;
                                xValues[4] = "Found";
                            }
                            else
                            {
                                yValues[4] = 0;
                                xValues[4] = "Found";
                            }

                            if (rG.Count > 0)
                            {
                                yValues[0] = rG.Count;
                                xValues[0] = "Returned";
                            }
                            else
                            {
                                yValues[0] = 0;
                                xValues[0] = "Returned";
                            }
                            if (aPose.Count > 0)
                            {
                                yValues[1] = aPose.Count;
                                xValues[1] = "Authorities";
                            }
                            else
                            {
                                yValues[1] = 0;
                                xValues[1] = "Authorities";
                            }
                            if (dG.Count > 0)
                            {
                                yValues[2] = dG.Count;
                                xValues[2] = "Charity";
                            }
                            else
                            {
                                yValues[2] = 0;
                                xValues[2] = "Charity";
                            }
                        }
                        else if (status == "Comparison")
                        {
                            //Range1
                            if (ttype == "Valuable/Non-Valuable")
                            {
                                if (var1.Count > 0)
                                {
                                    vyValues[0] = var1.Count;
                                    vxValues[0] = "Valuable";

                                    vyValues[1] = range1.Count - var1.Count;
                                    vxValues[1] = "Non-Valuable";
                                }
                                else
                                {
                                    vyValues[0] = 0;
                                    vxValues[0] = "Valuable";

                                    vyValues[1] = range1.Count;
                                    vxValues[1] = "Non-Valuable";
                                }
                            }
                            else if (ttype == "Delivered/Not-Delivered")
                            {
                                //valuables = loghistories.Where(i => i.Status == "Found").ToList();

                                if (var1.Count > 0)
                                {
                                    vyValues[0] = var1.Count;
                                    vxValues[0] = "Not-Delivered";

                                    vyValues[1] = range1.Count - var1.Count;
                                    vxValues[1] = "Delivered";
                                }
                                else
                                {
                                    vyValues[0] = 0;
                                    vxValues[0] = "Not-Delivered";

                                    vyValues[1] = range1.Count;
                                    vxValues[1] = "Delivered";
                                }
                            }
                            //Range2
                            if (ttype == "Valuable/Non-Valuable")
                            {
                                if (var2.Count > 0)
                                {
                                    yyValues[0] = var2.Count;
                                    yxValues[0] = "Valuable";

                                    yyValues[1] = range2.Count - var2.Count;
                                    yxValues[1] = "Non-Valuable";
                                }
                                else
                                {
                                    yyValues[0] = 0;
                                    yxValues[0] = "Valuable";

                                    yyValues[1] = range2.Count;
                                    yxValues[1] = "Non-Valuable";
                                }
                            }
                            else if (ttype == "Delivered/Not-Delivered")
                            {
                                //valuables = loghistories.Where(i => i.Status == "Found").ToList();

                                if (var2.Count > 0)
                                {
                                    yyValues[0] = var2.Count;
                                    yxValues[0] = "Not-Delivered";

                                    yyValues[1] = range2.Count - var2.Count;
                                    yxValues[1] = "Delivered";
                                }
                                else
                                {
                                    yyValues[0] = 0;
                                    yxValues[0] = "Not-Delivered";

                                    yyValues[1] = range2.Count;
                                    yxValues[1] = "Delivered";
                                }
                            }
                        }

                        if (status == "Valuable/Non-Valuable")
                        {
                            Chart1.Series["Series1"].Points.DataBindXY(vxValues, vyValues);
                        }
                        else if (status == "Valuable Returned/Not-Returned")
                        {
                            Chart1.Series["Series1"].Points.DataBindXY(vxValues, vyValues);
                        }
                        else if (status == "Charity Returned/Not-Returned")
                        {
                            Chart1.Series["Series1"].Points.DataBindXY(vxValues, vyValues);
                        }
                        else if (status == "Delivered/Not-Delivered")
                        {
                            Chart1.Series["Series1"].Points.DataBindXY(vxValues, vyValues);
                        }
                        else if (status == "All")
                        {
                            Chart1.Series["Series1"].Points.DataBindXY(xValues, yValues);
                        }
                        else if (status == "Comparison")
                        {
                            Chart1.Series["Series1"].Points.DataBindXY(vxValues, vyValues);
                            Chart2.Series["Series1"].Points.DataBindXY(yxValues, yyValues);
                        }


                        iTextSharp.text.Paragraph paragraphTable2 = new iTextSharp.text.Paragraph();
                        paragraphTable2.SpacingBefore = 150f;
                        PdfPTable table2 = new PdfPTable(10);
                        table2.WidthPercentage = 100;

                        var ncaheader2 = new PdfPCell();
                        ncaheader2.AddElement(new Phrase("Ref#", wnine));
                        ncaheader2.Border = Rectangle.NO_BORDER;
                        ncaheader2.BackgroundColor = Color.GRAY;
                        table2.AddCell(ncaheader2);

                        var ncaheader3 = new PdfPCell();
                        ncaheader3.AddElement(new Phrase("Found Date", wnine));
                        ncaheader3.Border = Rectangle.NO_BORDER;
                        ncaheader3.BackgroundColor = Color.GRAY;
                        table2.AddCell(ncaheader3);

                        var ncaheader4 = new PdfPCell();
                        ncaheader4.AddElement(new Phrase("Type", wnine));
                        ncaheader4.Border = Rectangle.NO_BORDER;
                        ncaheader4.BackgroundColor = Color.GRAY;
                        table2.AddCell(ncaheader4);

                        var ncaheader5 = new PdfPCell();
                        ncaheader5.AddElement(new Phrase("Sub Type", wnine));
                        ncaheader5.Border = Rectangle.NO_BORDER;
                        ncaheader5.BackgroundColor = Color.GRAY;
                        table2.AddCell(ncaheader5);

                        var ncaheader6 = new PdfPCell();
                        ncaheader6.AddElement(new Phrase("Brand", wnine));
                        ncaheader6.Border = Rectangle.NO_BORDER;
                        ncaheader6.BackgroundColor = Color.GRAY;
                        table2.AddCell(ncaheader6);

                        var ncaheader61 = new PdfPCell();
                        ncaheader61.AddElement(new Phrase("Location Found", wnine));
                        ncaheader61.Border = Rectangle.NO_BORDER;
                        ncaheader61.BackgroundColor = Color.GRAY;
                        table2.AddCell(ncaheader61);

                        var ncaheader62 = new PdfPCell();
                        ncaheader62.AddElement(new Phrase("Finder Name", wnine));
                        ncaheader62.Border = Rectangle.NO_BORDER;
                        ncaheader62.BackgroundColor = Color.GRAY;
                        table2.AddCell(ncaheader62);

                        var ncaheader63 = new PdfPCell();
                        ncaheader63.AddElement(new Phrase("Department", wnine));
                        ncaheader63.Border = Rectangle.NO_BORDER;
                        ncaheader63.BackgroundColor = Color.GRAY;
                        table2.AddCell(ncaheader63);

                        var ncaheader64 = new PdfPCell();
                        ncaheader64.AddElement(new Phrase("Receiver Name", wnine));
                        ncaheader64.Border = Rectangle.NO_BORDER;
                        ncaheader64.BackgroundColor = Color.GRAY;
                        table2.AddCell(ncaheader64);

                        var ncaheader1 = new PdfPCell();
                        ncaheader1.AddElement(new Phrase("Status", wnine));
                        ncaheader1.Border = Rectangle.NO_BORDER;
                        ncaheader1.PaddingLeft = 5;
                        ncaheader1.PaddingBottom = 5;
                        ncaheader1.BackgroundColor = Color.GRAY;

                        table2.AddCell(ncaheader1);


                        PdfPTable table3 = new PdfPTable(10);
                        table3.WidthPercentage = 100;


                        var ncaheader23 = new PdfPCell();
                        ncaheader23.AddElement(new Phrase("Ref#", wnine));
                        ncaheader23.Border = Rectangle.NO_BORDER;
                        ncaheader23.BackgroundColor = Color.GRAY;
                        table3.AddCell(ncaheader23);

                        var ncaheader33 = new PdfPCell();
                        ncaheader33.AddElement(new Phrase("Found Date", wnine));
                        ncaheader33.Border = Rectangle.NO_BORDER;
                        ncaheader33.BackgroundColor = Color.GRAY;
                        table3.AddCell(ncaheader33);

                        var ncaheader43 = new PdfPCell();
                        ncaheader43.AddElement(new Phrase("Type", wnine));
                        ncaheader43.Border = Rectangle.NO_BORDER;
                        ncaheader43.BackgroundColor = Color.GRAY;
                        table3.AddCell(ncaheader43);

                        var ncaheader53 = new PdfPCell();
                        ncaheader53.AddElement(new Phrase("Sub Type", wnine));
                        ncaheader53.Border = Rectangle.NO_BORDER;
                        ncaheader53.BackgroundColor = Color.GRAY;
                        table3.AddCell(ncaheader53);

                        var ncaheader633 = new PdfPCell();
                        ncaheader633.AddElement(new Phrase("Brand", wnine));
                        ncaheader633.Border = Rectangle.NO_BORDER;
                        ncaheader633.BackgroundColor = Color.GRAY;
                        table3.AddCell(ncaheader633);

                        var ncaheader613 = new PdfPCell();
                        ncaheader613.AddElement(new Phrase("Location Found", wnine));
                        ncaheader613.Border = Rectangle.NO_BORDER;
                        ncaheader613.BackgroundColor = Color.GRAY;
                        table3.AddCell(ncaheader613);

                        var ncaheader623 = new PdfPCell();
                        ncaheader623.AddElement(new Phrase("Finder Name", wnine));
                        ncaheader623.Border = Rectangle.NO_BORDER;
                        ncaheader623.BackgroundColor = Color.GRAY;
                        table3.AddCell(ncaheader623);

                        var ncaheader6333 = new PdfPCell();
                        ncaheader6333.AddElement(new Phrase("Department", wnine));
                        ncaheader6333.Border = Rectangle.NO_BORDER;
                        ncaheader6333.BackgroundColor = Color.GRAY;
                        table3.AddCell(ncaheader6333);

                        var ncaheader643 = new PdfPCell();
                        ncaheader643.AddElement(new Phrase("Receiver Name", wnine));
                        ncaheader643.Border = Rectangle.NO_BORDER;
                        ncaheader643.BackgroundColor = Color.GRAY;
                        table3.AddCell(ncaheader643);

                        var ncaheader13 = new PdfPCell();
                        ncaheader13.AddElement(new Phrase("Status", wnine));
                        ncaheader13.Border = Rectangle.NO_BORDER;
                        ncaheader13.PaddingLeft = 5;
                        ncaheader13.PaddingBottom = 5;
                        ncaheader13.BackgroundColor = Color.GRAY;
                        table3.AddCell(ncaheader13);


                        var nonV = new List<ItemFound>();
                        var totalcount = new List<ItemFound>();
                        totalcount = loghistories;
                        if (status != "All" && status != "Valuable/Non-Valuable" && status != "Delivered/Not-Delivered" && status != "Comparison" && status != "Valuable Returned/Not-Returned" && status != "Charity Returned/Not-Returned" && status != "Alcohol Disposed")
                        {
                            loghistories = loghistories.Where(i => i.Status == status).ToList();
                        }
                        else if (status == "Valuable/Non-Valuable")
                        {
                            nonV = loghistories.Where(i => i.Type != "Valuable" && i.Type != "Valuables").ToList();
                            loghistories = loghistories.Where(i => i.Type == "Valuable" || i.Type == "Valuables").ToList();
                        }
                        else if (status == "Valuable Returned/Not-Returned")
                        {
                            var gloghistories = loghistories.Where(i => i.Type == "Valuable" || i.Type == "Valuables").ToList();
                            nonV = gloghistories.Where(i => i.Status != "Found").ToList();
                            loghistories = gloghistories.Where(i => i.Status == "Found").ToList();
                        }
                        else if (status == "Charity Returned/Not-Returned")
                        {
                            var gloghistories = loghistories.Where(i => i.Type == "Charity" || i.Type == "Charity").ToList();
                            nonV = gloghistories.Where(i => i.Status != "Found").ToList();
                            loghistories = gloghistories.Where(i => i.Status == "Found").ToList();
                        }
                        else if (status == "Alcohol Disposed")
                        {
                            loghistories = loghistories.Where(i => i.Type == "Disposal" && i.SubType == "Alcohol" && i.Status == "Dispose").ToList();
                        }
                        else if (status == "Delivered/Not-Delivered")
                        {
                            nonV = loghistories.Where(i => i.Status != "Found").ToList();
                            loghistories = loghistories.Where(i => i.Status == "Found").ToList();
                        }
                        else if (status == "Comparison")
                        {
                            if (ttype == "Valuable/Non-Valuable")
                            {
                                nonV = range1.OrderByDescending(i => i.Type).ToList();
                                loghistories = range2.OrderByDescending(i => i.Type).ToList();
                            }
                            else if (ttype == "Delivered/Not-Delivered")
                            {
                                var nrange1 = range1;
                                var nrange2 = range2;

                                var delv1 = nrange1.Where(i => i.Status != "Found").ToList();
                                var delv2 = nrange2.Where(i => i.Status != "Found").ToList();

                                var ndelv1 = nrange1.Where(i => i.Status == "Found").ToList();
                                var ndelv2 = nrange2.Where(i => i.Status == "Found").ToList();


                                nonV.AddRange(delv1);
                                nonV.AddRange(ndelv1);

                                loghistories.Clear();

                                loghistories.AddRange(delv2);
                                loghistories.AddRange(ndelv2);
                            }
                        }
                        var odd = false;
                        foreach (var device in nonV)
                        {
                            var dt = device.DateFound.AddHours(userinfo.TimeZone).ToString();
                            var dto = string.Empty;

                            if (status == "Comparison")
                            {
                                if (ttype == "Valuable/Non-Valuable")
                                {
                                    if (device.Type != "Valuable" && device.Type != "Valuables")
                                        device.Type = "Non-Valuable";
                                }
                                else if (ttype == "Delivered/Not-Delivered")
                                {
                                    if (device.Status != "Found")
                                        device.Status = "Delivered";
                                    else
                                    {
                                        device.Status = "Not-Delivered";
                                    }
                                }
                            }
                            else
                            {
                                if (status == "Valuable/Non-Valuable")
                                {
                                    device.Type = "Non-Valuable";
                                }
                                else if (status == "Delivered/Not-Delivered")
                                {
                                    device.Status = "Delivered";
                                }
                                else if (device.Status == "Dispose" || device.Status == "Donate")
                                {
                                    var Rinfo = ItemDispose.GetItemDisposeByReference(device.Reference, dbConnection);
                                    if (Rinfo != null)
                                    {
                                        dto = "(" + Rinfo.DisposedTo + ")";
                                    }
                                }
                            }
                            var xheaderCell = new PdfPCell(new Phrase(device.Status + dto, eighttimes));
                            var xheaderCell2 = new PdfPCell(new Phrase(device.Reference, eighttimes));
                            var xheaderCell3 = new PdfPCell(new Phrase(dt, eighttimes));
                            var xheaderCell4 = new PdfPCell(new Phrase(device.Type, eighttimes));
                            var xheaderCell5 = new PdfPCell(new Phrase(device.SubType, eighttimes));
                            var xheaderCell6 = new PdfPCell(new Phrase(device.Brand, eighttimes));
                            var xheaderCell7 = new PdfPCell(new Phrase(device.LocationFound, eighttimes));
                            var xheaderCell8 = new PdfPCell(new Phrase(device.FinderName, eighttimes));
                            var xheaderCell9 = new PdfPCell(new Phrase(device.FinderDepartment, eighttimes));
                            var xheaderCell10 = new PdfPCell(new Phrase(device.ReceiverName, eighttimes));

                            if (!odd)
                            {
                                xheaderCell.BackgroundColor = Color.LIGHT_GRAY;
                                xheaderCell2.BackgroundColor = Color.LIGHT_GRAY;
                                xheaderCell3.BackgroundColor = Color.LIGHT_GRAY;
                                xheaderCell4.BackgroundColor = Color.LIGHT_GRAY;
                                xheaderCell5.BackgroundColor = Color.LIGHT_GRAY;
                                xheaderCell6.BackgroundColor = Color.LIGHT_GRAY;
                                xheaderCell7.BackgroundColor = Color.LIGHT_GRAY;
                                xheaderCell8.BackgroundColor = Color.LIGHT_GRAY;
                                xheaderCell9.BackgroundColor = Color.LIGHT_GRAY;
                                xheaderCell10.BackgroundColor = Color.LIGHT_GRAY;
                                odd = true;
                            }
                            else
                            {
                                odd = false;
                            }

                            xheaderCell.Border = Rectangle.NO_BORDER;
                            xheaderCell2.Border = Rectangle.NO_BORDER;
                            xheaderCell3.Border = Rectangle.NO_BORDER;
                            xheaderCell4.Border = Rectangle.NO_BORDER;
                            xheaderCell5.Border = Rectangle.NO_BORDER;
                            xheaderCell6.Border = Rectangle.NO_BORDER;
                            xheaderCell7.Border = Rectangle.NO_BORDER;
                            xheaderCell8.Border = Rectangle.NO_BORDER;
                            xheaderCell9.Border = Rectangle.NO_BORDER;
                            xheaderCell10.Border = Rectangle.NO_BORDER;


                            table3.AddCell(xheaderCell2);
                            table3.AddCell(xheaderCell3);
                            table3.AddCell(xheaderCell4);
                            table3.AddCell(xheaderCell5);
                            table3.AddCell(xheaderCell6);
                            table3.AddCell(xheaderCell7);
                            table3.AddCell(xheaderCell8);
                            table3.AddCell(xheaderCell9);
                            table3.AddCell(xheaderCell10);
                            table3.AddCell(xheaderCell);
                        }

                        var odd1 = false;

                        foreach (var device in loghistories)
                        {

                            //if (device.RecevieTime.Value.Ticks > fromdate.Ticks && device.RecevieTime.Value.Ticks < todate.Ticks)
                            {
                                var dt = device.DateFound.AddHours(userinfo.TimeZone).ToString();
                                var dto = string.Empty;
                                if (status == "Comparison")
                                {
                                    if (ttype == "Valuable/Non-Valuable")
                                    {
                                        if (device.Type != "Valuable" && device.Type != "Valuables")
                                            device.Type = "Non-Valuable";
                                    }
                                    else if (ttype == "Delivered/Not-Delivered")
                                    {
                                        if (device.Status != "Found" && device.Status != "Not-Delivered")
                                        {
                                            device.Status = "Delivered";
                                        }
                                        else
                                        {
                                            device.Status = "Not-Delivered";
                                        }
                                    }
                                }
                                else
                                {
                                    if (status == "Delivered/Not-Delivered")
                                    {
                                        device.Status = "Not-Delivered";
                                    }
                                    else if (device.Status == "Dispose" || device.Status == "Donate")
                                    {
                                        var Rinfo = ItemDispose.GetItemDisposeByReference(device.Reference, dbConnection);
                                        if (Rinfo != null)
                                        {
                                            // dt = Rinfo.CreatedDate.ToString();
                                            //uname = Rinfo.RecipientName;
                                            dto = "(" + Rinfo.DisposedTo + ")";
                                        }
                                    }
                                }
                                var xheaderCell = new PdfPCell(new Phrase(device.Status + dto, eighttimes));
                                var xheaderCell2 = new PdfPCell(new Phrase(device.Reference, eighttimes));
                                var xheaderCell3 = new PdfPCell(new Phrase(dt, eighttimes));
                                var xheaderCell4 = new PdfPCell(new Phrase(device.Type, eighttimes));
                                var xheaderCell5 = new PdfPCell(new Phrase(device.SubType, eighttimes));
                                var xheaderCell6 = new PdfPCell(new Phrase(device.Brand, eighttimes));
                                var xheaderCell7 = new PdfPCell(new Phrase(device.LocationFound, eighttimes));
                                var xheaderCell8 = new PdfPCell(new Phrase(device.FinderName, eighttimes));
                                var xheaderCell9 = new PdfPCell(new Phrase(device.FinderDepartment, eighttimes));
                                var xheaderCell10 = new PdfPCell(new Phrase(device.ReceiverName, eighttimes));

                                if (!odd1)
                                {
                                    xheaderCell.BackgroundColor = Color.LIGHT_GRAY;
                                    xheaderCell2.BackgroundColor = Color.LIGHT_GRAY;
                                    xheaderCell3.BackgroundColor = Color.LIGHT_GRAY;
                                    xheaderCell4.BackgroundColor = Color.LIGHT_GRAY;
                                    xheaderCell5.BackgroundColor = Color.LIGHT_GRAY;
                                    xheaderCell6.BackgroundColor = Color.LIGHT_GRAY;
                                    xheaderCell7.BackgroundColor = Color.LIGHT_GRAY;
                                    xheaderCell8.BackgroundColor = Color.LIGHT_GRAY;
                                    xheaderCell9.BackgroundColor = Color.LIGHT_GRAY;
                                    xheaderCell10.BackgroundColor = Color.LIGHT_GRAY;
                                    odd1 = true;
                                }
                                else
                                {
                                    odd1 = false;
                                }

                                xheaderCell.Border = Rectangle.NO_BORDER;
                                xheaderCell2.Border = Rectangle.NO_BORDER;
                                xheaderCell3.Border = Rectangle.NO_BORDER;
                                xheaderCell4.Border = Rectangle.NO_BORDER;
                                xheaderCell5.Border = Rectangle.NO_BORDER;
                                xheaderCell6.Border = Rectangle.NO_BORDER;
                                xheaderCell7.Border = Rectangle.NO_BORDER;
                                xheaderCell8.Border = Rectangle.NO_BORDER;
                                xheaderCell9.Border = Rectangle.NO_BORDER;
                                xheaderCell10.Border = Rectangle.NO_BORDER;

                                table2.AddCell(xheaderCell2);
                                table2.AddCell(xheaderCell3);
                                table2.AddCell(xheaderCell4);
                                table2.AddCell(xheaderCell5);
                                table2.AddCell(xheaderCell6);
                                table2.AddCell(xheaderCell7);
                                table2.AddCell(xheaderCell8);
                                table2.AddCell(xheaderCell9);
                                table2.AddCell(xheaderCell10);
                                table2.AddCell(xheaderCell);
                            }
                        }
                        //doc.Add(paragraphTable);
                        if (status == "All")
                        {
                            using (MemoryStream memoryStream = new MemoryStream())
                            {
                                Chart1.SaveImage(memoryStream, ChartImageFormat.Png);
                                iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance(memoryStream.GetBuffer());
                                img.ScalePercent(75f);
                                img.Alignment = iTextSharp.text.Image.ALIGN_CENTER;
                                condataTableB.AddCell(img);
                            }

                            condataTable.AddCell(condataTableB);
                            condataTable.AddCell(new Paragraph(1, "\u00a0"));
                            headerPPM.AddCell(condataTable);
                            doc.Add(headerPPM);

                            ////doc.NewPage(); 
                            doc.Add(table2);
                        }
                        else if (status == "Alcohol Disposed")
                        {
                            PdfPTable tableCH1 = new PdfPTable(1);
                            tableCH1.WidthPercentage = 100;
                            var cheader1 = new PdfPCell();
                            cheader1.AddElement(new Phrase("Alcohol Disposed", cbtimes));
                            cheader1.Border = Rectangle.NO_BORDER;
                            cheader1.Colspan = 10;
                            tableCH1.AddCell(cheader1);
                            doc.Add(tableCH1);
                            doc.Add(table2);
                        }
                        else if (status == "Valuable/Non-Valuable")
                        {
                            var tableVTable = new PdfPTable(3);
                            tableVTable.DefaultCell.Border = Rectangle.NO_BORDER;
                            tableVTable.WidthPercentage = 100;
                            tableVTable.SetWidths(columnWidths7030);

                            PdfPTable tableV = new PdfPTable(1);
                            tableV.WidthPercentage = 100;
                            tableV.DefaultCell.Border = Rectangle.NO_BORDER;

                            PdfPTable tableV2 = new PdfPTable(2);
                            tableV2.WidthPercentage = 100;
                            tableV2.SetWidths(columnWidths3565);
                            tableV2.DefaultCell.Border = Rectangle.NO_BORDER;


                            var vhconheaderA1 = new PdfPCell();
                            vhconheaderA1.AddElement(new Phrase("Valuable/Non-Valuable", cbtimes));
                            vhconheaderA1.Border = Rectangle.NO_BORDER;
                            vhconheaderA1.Colspan = 2;
                            tableV2.AddCell(vhconheaderA1);

                            var vconheaderA1 = new PdfPCell();
                            vconheaderA1.AddElement(new Phrase("Valuable:", beleventimes));
                            vconheaderA1.Border = Rectangle.NO_BORDER;
                            tableV2.AddCell(vconheaderA1);

                            var vvconheaderA1a = new PdfPCell();
                            vvconheaderA1a.AddElement(new Phrase(valuables.Count.ToString(), eleventimes));
                            vvconheaderA1a.Border = Rectangle.NO_BORDER;
                            tableV2.AddCell(vvconheaderA1a);

                            var vvvconheaderA1 = new PdfPCell();
                            vvvconheaderA1.AddElement(new Phrase("Non-Valuable:", beleventimes));
                            vvvconheaderA1.Border = Rectangle.NO_BORDER;
                            tableV2.AddCell(vvvconheaderA1);

                            var vvvconheaderA1a = new PdfPCell();
                            vvvconheaderA1a.AddElement(new Phrase((nonV.Count).ToString(), eleventimes));
                            vvvconheaderA1a.Border = Rectangle.NO_BORDER;
                            tableV2.AddCell(vvvconheaderA1a);

                            var vvvvconheaderA1 = new PdfPCell();
                            vvvvconheaderA1.AddElement(new Phrase("Total:", beleventimes));
                            vvvvconheaderA1.Border = Rectangle.NO_BORDER;
                            tableV2.AddCell(vvvvconheaderA1);

                            var vvvvconheaderA1a = new PdfPCell();
                            vvvvconheaderA1a.AddElement(new Phrase((totalcount.Count).ToString(), eleventimes));
                            vvvvconheaderA1a.Border = Rectangle.NO_BORDER;
                            tableV2.AddCell(vvvvconheaderA1a);


                            tableV.AddCell(tableV2);
                            tableVTable.AddCell(tableV);

                            using (MemoryStream memoryStream = new MemoryStream())
                            {
                                Chart1.SaveImage(memoryStream, ChartImageFormat.Png);
                                iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance(memoryStream.GetBuffer());
                                img.ScalePercent(75f);
                                img.Alignment = iTextSharp.text.Image.ALIGN_CENTER;
                                tableVTable.AddCell(img);
                            }
                            tableVTable.AddCell(new Paragraph(1, "\u00a0"));

                            doc.Add(tableVTable);

                            if (valuables.Count > 0)
                            {
                                //doc.NewPage();
                                //// doc.Add(new Paragraph(25, "\u00a0"));
                                PdfPTable tableCH1 = new PdfPTable(1);
                                tableCH1.WidthPercentage = 100;
                                var cheader1 = new PdfPCell();
                                cheader1.AddElement(new Phrase("Valuable", cbtimes));
                                cheader1.Border = Rectangle.NO_BORDER;
                                cheader1.Colspan = 10;
                                tableCH1.AddCell(cheader1);
                                doc.Add(tableCH1);
                                doc.Add(table2);
                            }


                            if (nonV.Count > 0)
                            {
                                //doc.NewPage();
                                // doc.Add(new Paragraph(25, "\u00a0"));
                                PdfPTable tableCH1 = new PdfPTable(1);
                                tableCH1.WidthPercentage = 100;
                                var cheader1 = new PdfPCell();
                                cheader1.AddElement(new Phrase("Non-Valuable", cbtimes));
                                cheader1.Border = Rectangle.NO_BORDER;
                                cheader1.Colspan = 10;
                                tableCH1.AddCell(cheader1);
                                doc.Add(tableCH1);
                                doc.Add(table3);
                            }
                        }
                        else if (status == "Charity Returned/Not-Returned" || status == "Valuable Returned/Not-Returned")
                        {

                            var tableVTable = new PdfPTable(3);
                            tableVTable.DefaultCell.Border = Rectangle.NO_BORDER;
                            tableVTable.WidthPercentage = 100;
                            tableVTable.SetWidths(columnWidths7030);

                            PdfPTable tableV = new PdfPTable(1);
                            tableV.WidthPercentage = 100;
                            tableV.DefaultCell.Border = Rectangle.NO_BORDER;
                            PdfPTable tableV2 = new PdfPTable(2);
                            tableV2.WidthPercentage = 100;
                            tableV2.SetWidths(columnWidths3565);
                            tableV2.DefaultCell.Border = Rectangle.NO_BORDER;

                            var vhconheaderA1 = new PdfPCell();
                            vhconheaderA1.AddElement(new Phrase(status, cbtimes));
                            vhconheaderA1.Border = Rectangle.NO_BORDER;
                            vhconheaderA1.Colspan = 2;
                            tableV2.AddCell(vhconheaderA1);

                            var vconheaderA1 = new PdfPCell();
                            vconheaderA1.AddElement(new Phrase("Not-Returned:", beleventimes));
                            vconheaderA1.Border = Rectangle.NO_BORDER;
                            tableV2.AddCell(vconheaderA1);

                            var vvconheaderA1a = new PdfPCell();
                            vvconheaderA1a.AddElement(new Phrase((valuables.Count - nonV.Count).ToString(), eleventimes));
                            vvconheaderA1a.Border = Rectangle.NO_BORDER;
                            tableV2.AddCell(vvconheaderA1a);

                            var vvvconheaderA1 = new PdfPCell();
                            vvvconheaderA1.AddElement(new Phrase("Returned:", beleventimes));
                            vvvconheaderA1.Border = Rectangle.NO_BORDER;
                            tableV2.AddCell(vvvconheaderA1);

                            var vvvconheaderA1a = new PdfPCell();
                            vvvconheaderA1a.AddElement(new Phrase((nonV.Count).ToString(), eleventimes));
                            vvvconheaderA1a.Border = Rectangle.NO_BORDER;
                            tableV2.AddCell(vvvconheaderA1a);

                            var vvvvconheaderA1 = new PdfPCell();
                            vvvvconheaderA1.AddElement(new Phrase("Total:", beleventimes));
                            vvvvconheaderA1.Border = Rectangle.NO_BORDER;
                            tableV2.AddCell(vvvvconheaderA1);

                            var vvvvconheaderA1a = new PdfPCell();
                            vvvvconheaderA1a.AddElement(new Phrase((valuables.Count).ToString(), eleventimes));
                            vvvvconheaderA1a.Border = Rectangle.NO_BORDER;
                            tableV2.AddCell(vvvvconheaderA1a);

                            tableV.AddCell(tableV2);
                            tableVTable.AddCell(tableV);

                            using (MemoryStream memoryStream = new MemoryStream())
                            {
                                Chart1.SaveImage(memoryStream, ChartImageFormat.Png);
                                iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance(memoryStream.GetBuffer());
                                img.ScalePercent(75f);
                                img.Alignment = iTextSharp.text.Image.ALIGN_CENTER;
                                tableVTable.AddCell(img);
                            }

                            tableVTable.AddCell(new Paragraph(1, "\u00a0"));

                            doc.Add(tableVTable);
                            if (valuables.Count > 0)
                            {
                                //doc.NewPage();
                                // doc.Add(new Paragraph(25, "\u00a0"));
                                PdfPTable tableCH1 = new PdfPTable(1);
                                tableCH1.WidthPercentage = 100;
                                var cheader1 = new PdfPCell();
                                cheader1.AddElement(new Phrase("Not-Returned", cbtimes));
                                cheader1.Border = Rectangle.NO_BORDER;
                                cheader1.Colspan = 10;
                                tableCH1.AddCell(cheader1);
                                doc.Add(tableCH1);
                                doc.Add(table2);
                            }

                            if (nonV.Count > 0)
                            {
                                //doc.NewPage();
                                PdfPTable tableCH1 = new PdfPTable(1);
                                tableCH1.WidthPercentage = 100;
                                var cheader1 = new PdfPCell();
                                cheader1.AddElement(new Phrase("Returned", cbtimes));
                                cheader1.Border = Rectangle.NO_BORDER;
                                cheader1.Colspan = 10;
                                tableCH1.AddCell(cheader1);
                                // doc.Add(new Paragraph(25, "\u00a0"));
                                doc.Add(tableCH1);
                                doc.Add(table3);
                            }
                        }
                        else if (status == "Delivered/Not-Delivered")
                        {

                            var tableVTable = new PdfPTable(3);
                            tableVTable.DefaultCell.Border = Rectangle.NO_BORDER;
                            tableVTable.WidthPercentage = 100;
                            tableVTable.SetWidths(columnWidths7030);

                            PdfPTable tableV = new PdfPTable(1);
                            tableV.WidthPercentage = 100;
                            tableV.DefaultCell.Border = Rectangle.NO_BORDER;
                            PdfPTable tableV2 = new PdfPTable(2);
                            tableV2.WidthPercentage = 100;
                            tableV2.SetWidths(columnWidths3565);
                            tableV2.DefaultCell.Border = Rectangle.NO_BORDER;

                            var vhconheaderA1 = new PdfPCell();
                            vhconheaderA1.AddElement(new Phrase(status, cbtimes));
                            vhconheaderA1.Border = Rectangle.NO_BORDER;
                            vhconheaderA1.Colspan = 2;
                            tableV2.AddCell(vhconheaderA1);

                            var vconheaderA1 = new PdfPCell();
                            vconheaderA1.AddElement(new Phrase("Not-Delivered:", beleventimes));
                            vconheaderA1.Border = Rectangle.NO_BORDER;
                            tableV2.AddCell(vconheaderA1);

                            var vvconheaderA1a = new PdfPCell();
                            vvconheaderA1a.AddElement(new Phrase(valuables.Count.ToString(), eleventimes));
                            vvconheaderA1a.Border = Rectangle.NO_BORDER;
                            tableV2.AddCell(vvconheaderA1a);

                            var vvvconheaderA1 = new PdfPCell();
                            vvvconheaderA1.AddElement(new Phrase("Delivered:", beleventimes));
                            vvvconheaderA1.Border = Rectangle.NO_BORDER;
                            tableV2.AddCell(vvvconheaderA1);

                            var vvvconheaderA1a = new PdfPCell();
                            vvvconheaderA1a.AddElement(new Phrase((nonV.Count).ToString(), eleventimes));
                            vvvconheaderA1a.Border = Rectangle.NO_BORDER;
                            tableV2.AddCell(vvvconheaderA1a);

                            var vvvvconheaderA1 = new PdfPCell();
                            vvvvconheaderA1.AddElement(new Phrase("Total:", beleventimes));
                            vvvvconheaderA1.Border = Rectangle.NO_BORDER;
                            tableV2.AddCell(vvvvconheaderA1);

                            var vvvvconheaderA1a = new PdfPCell();
                            vvvvconheaderA1a.AddElement(new Phrase((totalcount.Count).ToString(), eleventimes));
                            vvvvconheaderA1a.Border = Rectangle.NO_BORDER;
                            tableV2.AddCell(vvvvconheaderA1a);

                            tableV.AddCell(tableV2);
                            tableVTable.AddCell(tableV);


                            using (MemoryStream memoryStream = new MemoryStream())
                            {
                                Chart1.SaveImage(memoryStream, ChartImageFormat.Png);
                                iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance(memoryStream.GetBuffer());
                                img.ScalePercent(75f);
                                img.Alignment = iTextSharp.text.Image.ALIGN_CENTER;
                                tableVTable.AddCell(img);
                            }
                            tableVTable.AddCell(new Paragraph(1, "\u00a0"));
                            doc.Add(tableVTable);

                            if (valuables.Count > 0)
                            {
                                //doc.NewPage();
                                // doc.Add(new Paragraph(25, "\u00a0"));
                                PdfPTable tableCH1 = new PdfPTable(1);
                                tableCH1.WidthPercentage = 100;

                                var cheader1 = new PdfPCell();
                                cheader1.AddElement(new Phrase("Not-Delivered", cbtimes));
                                cheader1.Border = Rectangle.NO_BORDER;

                                tableCH1.AddCell(cheader1);
                                doc.Add(tableCH1);
                                doc.Add(table2);
                            }

                            if (nonV.Count > 0)
                            {
                                //doc.NewPage();
                                // doc.Add(new Paragraph(25, "\u00a0"));
                                PdfPTable tableCH1 = new PdfPTable(1);
                                tableCH1.WidthPercentage = 100;

                                var cheader1 = new PdfPCell();
                                cheader1.AddElement(new Phrase("Delivered", cbtimes));
                                cheader1.Border = Rectangle.NO_BORDER;

                                tableCH1.AddCell(cheader1);
                                doc.Add(tableCH1);
                                doc.Add(table3);
                            }
                        }
                        else if (status == "Comparison")
                        {

                            if (ttype == "Valuable/Non-Valuable")
                            {
                                PdfPTable tableV = new PdfPTable(3);
                                tableV.WidthPercentage = 100;
                                tableV.DefaultCell.Border = Rectangle.NO_BORDER;
                                tableV.SetWidths(columnWidths7030);
                                PdfPTable tableV2 = new PdfPTable(2);
                                tableV2.WidthPercentage = 100;
                                tableV2.SetWidths(columnWidths3565);
                                tableV2.DefaultCell.Border = Rectangle.NO_BORDER;
                                PdfPTable tableV3 = new PdfPTable(2);
                                tableV3.WidthPercentage = 100;
                                tableV3.SetWidths(columnWidths3565);
                                tableV3.DefaultCell.Border = Rectangle.NO_BORDER;
                                var vhconheaderA1 = new PdfPCell();
                                vhconheaderA1.AddElement(new Phrase("Range 1", beleventimes));
                                vhconheaderA1.Border = Rectangle.NO_BORDER;
                                tableV2.AddCell(vhconheaderA1);

                                var vvconheaderA1ab = new PdfPCell();
                                vvconheaderA1ab.AddElement(new Phrase(year1 + " " + quater1, eleventimes));
                                vvconheaderA1ab.Border = Rectangle.NO_BORDER;
                                tableV2.AddCell(vvconheaderA1ab);

                                var vhconheaderA1A = new PdfPCell();
                                vhconheaderA1A.AddElement(new Phrase("Range 2", beleventimes));
                                vhconheaderA1A.Border = Rectangle.NO_BORDER;
                                tableV3.AddCell(vhconheaderA1A);

                                var vhconheaderA1Ab = new PdfPCell();
                                vhconheaderA1Ab.AddElement(new Phrase(year2 + " " + quarter2, eleventimes));
                                vhconheaderA1Ab.Border = Rectangle.NO_BORDER;
                                tableV3.AddCell(vhconheaderA1Ab);

                                var vconheaderA1 = new PdfPCell();
                                vconheaderA1.AddElement(new Phrase("Valuable:", beleventimes));
                                vconheaderA1.Border = Rectangle.NO_BORDER;
                                tableV2.AddCell(vconheaderA1);

                                var vvconheaderA1a = new PdfPCell();
                                vvconheaderA1a.AddElement(new Phrase(var1.Count.ToString(), eleventimes));
                                vvconheaderA1a.Border = Rectangle.NO_BORDER;
                                tableV2.AddCell(vvconheaderA1a);

                                var vvvconheaderA1 = new PdfPCell();
                                vvvconheaderA1.AddElement(new Phrase("Non-Valuable:", beleventimes));
                                vvvconheaderA1.Border = Rectangle.NO_BORDER;
                                tableV2.AddCell(vvvconheaderA1);

                                var vvvconheaderA1a = new PdfPCell();
                                vvvconheaderA1a.AddElement(new Phrase((range1.Count - var1.Count).ToString(), eleventimes));
                                vvvconheaderA1a.Border = Rectangle.NO_BORDER;
                                tableV2.AddCell(vvvconheaderA1a);

                                var vvvvconheaderA1 = new PdfPCell();
                                vvvvconheaderA1.AddElement(new Phrase("Total:", beleventimes));
                                vvvvconheaderA1.Border = Rectangle.NO_BORDER;
                                tableV2.AddCell(vvvvconheaderA1);

                                var vvvvconheaderA1a = new PdfPCell();
                                vvvvconheaderA1a.AddElement(new Phrase((range1.Count).ToString(), eleventimes));
                                vvvvconheaderA1a.Border = Rectangle.NO_BORDER;
                                tableV2.AddCell(vvvvconheaderA1a);

                                //Range 2


                                var vconheaderA13 = new PdfPCell();
                                vconheaderA13.AddElement(new Phrase("Valuable:", beleventimes));
                                vconheaderA13.Border = Rectangle.NO_BORDER;
                                tableV3.AddCell(vconheaderA13);

                                var vvconheaderA1a3 = new PdfPCell();
                                vvconheaderA1a3.AddElement(new Phrase(var2.Count.ToString(), eleventimes));
                                vvconheaderA1a3.Border = Rectangle.NO_BORDER;
                                tableV3.AddCell(vvconheaderA1a3);

                                var vvvconheaderA13 = new PdfPCell();
                                vvvconheaderA13.AddElement(new Phrase("Non-Valuable:", beleventimes));
                                vvvconheaderA13.Border = Rectangle.NO_BORDER;
                                tableV3.AddCell(vvvconheaderA13);

                                var vvvconheaderA1a3 = new PdfPCell();
                                vvvconheaderA1a3.AddElement(new Phrase((range2.Count - var2.Count).ToString(), eleventimes));
                                vvvconheaderA1a3.Border = Rectangle.NO_BORDER;
                                tableV3.AddCell(vvvconheaderA1a3);

                                var vvvvconheaderA13 = new PdfPCell();
                                vvvvconheaderA13.AddElement(new Phrase("Total:", beleventimes));
                                vvvvconheaderA13.Border = Rectangle.NO_BORDER;
                                tableV3.AddCell(vvvvconheaderA13);

                                var vvvvconheaderA1a3 = new PdfPCell();
                                vvvvconheaderA1a3.AddElement(new Phrase((range2.Count).ToString(), eleventimes));
                                vvvvconheaderA1a3.Border = Rectangle.NO_BORDER;
                                tableV3.AddCell(vvvvconheaderA1a3);

                                //PdfPTable tableCH = new PdfPTable(2);
                                //tableCH.WidthPercentage = 100;
                                //tableCH.DefaultCell.Border = Rectangle.NO_BORDER;
                                tableV.AddCell(tableV2);
                                using (MemoryStream memoryStream = new MemoryStream())
                                {
                                    Chart1.SaveImage(memoryStream, ChartImageFormat.Png);
                                    iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance(memoryStream.GetBuffer());
                                    img.ScalePercent(75f);
                                    img.Alignment = iTextSharp.text.Image.ALIGN_CENTER;
                                    tableV.AddCell(img);
                                }
                                tableV.AddCell(new Paragraph(1, "\u00a0"));
                                tableV.AddCell(tableV3);
                                using (MemoryStream memoryStream2 = new MemoryStream())
                                {
                                    Chart2.SaveImage(memoryStream2, ChartImageFormat.Png);
                                    iTextSharp.text.Image imgV = iTextSharp.text.Image.GetInstance(memoryStream2.GetBuffer());
                                    imgV.ScalePercent(75f);
                                    imgV.Alignment = iTextSharp.text.Image.ALIGN_CENTER;
                                    tableV.AddCell(imgV);
                                }
                                tableV.AddCell(new Paragraph(1, "\u00a0"));
                                //doc.Add(tableCH);
                                doc.Add(tableV);

                                PdfPTable tableCH1 = new PdfPTable(1);
                                tableCH1.WidthPercentage = 100;

                                var cheader1 = new PdfPCell();
                                cheader1.AddElement(new Phrase("Range 1 " + year1 + " " + quater1, cbtimes));
                                cheader1.Border = Rectangle.NO_BORDER;

                                tableCH1.AddCell(cheader1);



                                //doc.NewPage();
                                // doc.Add(new Paragraph(25, "\u00a0"));
                                doc.Add(tableCH1);
                                doc.Add(table3);

                                PdfPTable tableCH2 = new PdfPTable(1);
                                tableCH2.WidthPercentage = 100;

                                var cheader2 = new PdfPCell();
                                cheader2.AddElement(new Phrase("Range 2 " + year2 + " " + quarter2, cbtimes));
                                cheader2.Border = Rectangle.NO_BORDER;

                                tableCH2.AddCell(cheader2);

                                //doc.NewPage();
                                // doc.Add(new Paragraph(25, "\u00a0"));
                                doc.Add(tableCH2);
                                doc.Add(table2);
                            }
                            else if (ttype == "Delivered/Not-Delivered")
                            {

                                PdfPTable tableV = new PdfPTable(2);
                                tableV.WidthPercentage = 100;
                                tableV.DefaultCell.Border = Rectangle.NO_BORDER;
                                PdfPTable tableV2 = new PdfPTable(2);
                                tableV2.WidthPercentage = 100;
                                tableV2.SetWidths(columnWidths3565);
                                tableV2.DefaultCell.Border = Rectangle.NO_BORDER;
                                PdfPTable tableV3 = new PdfPTable(2);
                                tableV3.WidthPercentage = 100;
                                tableV3.SetWidths(columnWidths3565);
                                tableV3.DefaultCell.Border = Rectangle.NO_BORDER;
                                var vhconheaderA1 = new PdfPCell();
                                vhconheaderA1.AddElement(new Phrase("Range 1", beleventimes));
                                vhconheaderA1.Border = Rectangle.NO_BORDER;
                                tableV2.AddCell(vhconheaderA1);

                                var vvconheaderA1ab = new PdfPCell();
                                vvconheaderA1ab.AddElement(new Phrase(year1 + " " + quater1, eleventimes));
                                vvconheaderA1ab.Border = Rectangle.NO_BORDER;
                                tableV2.AddCell(vvconheaderA1ab);

                                var vhconheaderA1A = new PdfPCell();
                                vhconheaderA1A.AddElement(new Phrase("Range 2", beleventimes));
                                vhconheaderA1A.Border = Rectangle.NO_BORDER;
                                tableV3.AddCell(vhconheaderA1A);

                                var vhconheaderA1Ab = new PdfPCell();
                                vhconheaderA1Ab.AddElement(new Phrase(year2 + " " + quarter2, eleventimes));
                                vhconheaderA1Ab.Border = Rectangle.NO_BORDER;
                                tableV3.AddCell(vhconheaderA1Ab);

                                var vconheaderA1 = new PdfPCell();
                                vconheaderA1.AddElement(new Phrase("Not-Delivered:", beleventimes));
                                vconheaderA1.Border = Rectangle.NO_BORDER;
                                tableV2.AddCell(vconheaderA1);

                                var vvconheaderA1a = new PdfPCell();
                                vvconheaderA1a.AddElement(new Phrase(var1.Count.ToString(), eleventimes));
                                vvconheaderA1a.Border = Rectangle.NO_BORDER;
                                tableV2.AddCell(vvconheaderA1a);

                                var vvvconheaderA1 = new PdfPCell();
                                vvvconheaderA1.AddElement(new Phrase("Delivered:", beleventimes));
                                vvvconheaderA1.Border = Rectangle.NO_BORDER;
                                tableV2.AddCell(vvvconheaderA1);

                                var vvvconheaderA1a = new PdfPCell();
                                vvvconheaderA1a.AddElement(new Phrase((range1.Count - var1.Count).ToString(), eleventimes));
                                vvvconheaderA1a.Border = Rectangle.NO_BORDER;
                                tableV2.AddCell(vvvconheaderA1a);

                                var vvvvconheaderA1 = new PdfPCell();
                                vvvvconheaderA1.AddElement(new Phrase("Total:", beleventimes));
                                vvvvconheaderA1.Border = Rectangle.NO_BORDER;
                                tableV2.AddCell(vvvvconheaderA1);

                                var vvvvconheaderA1a = new PdfPCell();
                                vvvvconheaderA1a.AddElement(new Phrase((range1.Count).ToString(), eleventimes));
                                vvvvconheaderA1a.Border = Rectangle.NO_BORDER;
                                tableV2.AddCell(vvvvconheaderA1a);

                                //Range 2


                                var vconheaderA13 = new PdfPCell();
                                vconheaderA13.AddElement(new Phrase("Not-Delivered:", beleventimes));
                                vconheaderA13.Border = Rectangle.NO_BORDER;
                                tableV3.AddCell(vconheaderA13);

                                var vvconheaderA1a3 = new PdfPCell();
                                vvconheaderA1a3.AddElement(new Phrase(var2.Count.ToString(), eleventimes));
                                vvconheaderA1a3.Border = Rectangle.NO_BORDER;
                                tableV3.AddCell(vvconheaderA1a3);

                                var vvvconheaderA13 = new PdfPCell();
                                vvvconheaderA13.AddElement(new Phrase("Delivered:", beleventimes));
                                vvvconheaderA13.Border = Rectangle.NO_BORDER;
                                tableV3.AddCell(vvvconheaderA13);

                                var vvvconheaderA1a3 = new PdfPCell();
                                vvvconheaderA1a3.AddElement(new Phrase((range2.Count - var2.Count).ToString(), eleventimes));
                                vvvconheaderA1a3.Border = Rectangle.NO_BORDER;
                                tableV3.AddCell(vvvconheaderA1a3);

                                var vvvvconheaderA13 = new PdfPCell();
                                vvvvconheaderA13.AddElement(new Phrase("Total:", beleventimes));
                                vvvvconheaderA13.Border = Rectangle.NO_BORDER;
                                tableV3.AddCell(vvvvconheaderA13);

                                var vvvvconheaderA1a3 = new PdfPCell();
                                vvvvconheaderA1a3.AddElement(new Phrase((range2.Count).ToString(), eleventimes));
                                vvvvconheaderA1a3.Border = Rectangle.NO_BORDER;
                                tableV3.AddCell(vvvvconheaderA1a3);

                                tableV.AddCell(tableV2);
                                tableV.AddCell(tableV3);
                                doc.Add(tableV);

                                PdfPTable tableCH = new PdfPTable(2);
                                tableCH.WidthPercentage = 100;
                                tableCH.DefaultCell.Border = Rectangle.NO_BORDER;
                                using (MemoryStream memoryStream = new MemoryStream())
                                {
                                    Chart1.SaveImage(memoryStream, ChartImageFormat.Png);
                                    iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance(memoryStream.GetBuffer());
                                    img.ScalePercent(75f);
                                    img.Alignment = iTextSharp.text.Image.ALIGN_CENTER;
                                    tableCH.AddCell(img);
                                }

                                using (MemoryStream memoryStream2 = new MemoryStream())
                                {
                                    Chart2.SaveImage(memoryStream2, ChartImageFormat.Png);
                                    iTextSharp.text.Image imgV = iTextSharp.text.Image.GetInstance(memoryStream2.GetBuffer());
                                    imgV.ScalePercent(75f);
                                    imgV.Alignment = iTextSharp.text.Image.ALIGN_CENTER;
                                    tableCH.AddCell(imgV);
                                }

                                doc.Add(tableCH);


                                PdfPTable tableCH1 = new PdfPTable(1);
                                tableCH1.WidthPercentage = 100;

                                var cheader1 = new PdfPCell();
                                cheader1.AddElement(new Phrase("Range 1 " + year1 + " " + quater1, cbtimes));
                                cheader1.Border = Rectangle.NO_BORDER;

                                tableCH1.AddCell(cheader1);
                                //doc.NewPage();
                                // doc.Add(new Paragraph(25, "\u00a0"));
                                doc.Add(tableCH1);
                                doc.Add(table3);

                                PdfPTable tableCH2 = new PdfPTable(1);
                                tableCH2.WidthPercentage = 100;

                                var cheader2 = new PdfPCell();
                                cheader2.AddElement(new Phrase("Range 2 " + year2 + " " + quarter2, cbtimes));
                                cheader2.Border = Rectangle.NO_BORDER;

                                tableCH2.AddCell(cheader2);
                                //doc.NewPage();
                                // doc.Add(new Paragraph(25, "\u00a0"));
                                doc.Add(tableCH2);
                                doc.Add(table2);

                            }
                        }

                        Font geleventimes = new Font(f_cn, 11, Font.NORMAL, Color.GRAY);
                        var endTable = new PdfPTable(1);
                        endTable.DefaultCell.Border = Rectangle.NO_BORDER;
                        endTable.WidthPercentage = 100;

                        var endheader = new PdfPCell();
                        endheader.AddElement(new Phrase("Report generated: " + CommonUtility.getDTNow().AddHours(userinfo.TimeZone).ToString() + " 	by: " + userinfo.Username, geleventimes));
                        endheader.Border = Rectangle.NO_BORDER;
                        endTable.AddCell(endheader);
                        doc.Add(endTable);

                        doc.Close();

                        Response.ContentType = "application/pdf";
                        Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
                        Response.Cache.SetCacheability(HttpCacheability.NoCache);
                        Response.Write(doc);
                        Response.End();
                    }
                    else
                    {
                        Response.Write("<script>alert('No entries found')</script>");
                    }
                }
            }
            catch (Exception ex)
            {
                if (userinfo != null)
                    MIMSLog.MIMSLogSave("Reports", "btnExport2_Click", ex, dbConnection, userinfo.SiteId, userinfo.CustomerInfoId);
                else
                    MIMSLog.MIMSLogSave("Reports", "btnExport2_Click", ex, dbConnection); 
            }
        }

        protected void btnExport3_Click(object sender, EventArgs e)
        {
            var userinfo = Users.GetUserByName(User.Identity.Name, dbConnection);
            try
            {
                //using (MemoryStream memoryStream = new MemoryStream())

                string year = yearlyDuty.Value;
                string month = monthlyDuty.Value;
                var status = dutyStatus.Value;
                var uid = dutyUser.Value;
                var fromdate = Convert.ToDateTime(month + " " + year);
                var maxdays = DateTime.DaysInMonth(fromdate.Year, fromdate.Month);
                var todate = fromdate.AddDays(maxdays - 1);

                var employee = Users.GetUserById(Convert.ToInt32(uid), dbConnection);

                var allusers = new List<Users>();

                if (userinfo.RoleId == (int)Role.SuperAdmin)
                {
                    allusers = Users.GetAllUsers(dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.ChiefOfficer)
                {
                    allusers = Users.GetAllUsers(dbConnection);

                    allusers = allusers.Where(i => i.RoleId != (int)Role.ChiefOfficer).ToList();
                }
                else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                {
                    allusers = Users.GetAllUsersByCustomerIdNotIncludeCustomerUser(userinfo.CustomerInfoId, dbConnection);

                    allusers = allusers.Where(i => i.RoleId != (int)Role.CustomerSuperadmin).ToList();
                }
                else if (userinfo.RoleId == (int)Role.Director)
                {
                    allusers = Users.GetAllUsersBySiteId(userinfo.SiteId, dbConnection);

                    allusers = allusers.Where(i => i.RoleId != (int)Role.Director).ToList();
                }
                else if (userinfo.RoleId == (int)Role.Admin)
                {
                    var allmanagers = DirectorManager.GetAllFullManagersByDirectorId(userinfo.ID, dbConnection);
                    foreach (var usermanager in allmanagers)
                    {
                        allusers.Add(usermanager);
                        var allmanagerusers = Users.GetAllFullUsersByManagerIdForDirector(usermanager.ID, dbConnection);
                        allusers.AddRange(allmanagerusers);
                    }
                    var unassigned = Users.GetAllUnassignedOperators(dbConnection);
                    unassigned = unassigned.Where(i => i.SiteId == (int)userinfo.SiteId).ToList();
                    foreach (var subsubuser in unassigned)
                    {
                        allusers.Add(subsubuser);
                    }
                }
                else if (userinfo.RoleId == (int)Role.Manager)
                {
                    allusers.AddRange(Users.GetAllFullUsersByManagerId(userinfo.ID, dbConnection));
                }
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                    allusers.AddRange(Users.GetAllUsersByLevel5(userinfo.ID, dbConnection));
                }
                allusers = allusers.Where(i => i.RoleId != (int)Role.MessageBoardUser && i.RoleId != (int)Role.CustomerUser).ToList();
                var rosterData = DutyRoster.GetDutyRosterByEmpIdAndTime(fromdate.ToString(), todate.AddHours(23).ToString(), employee.ID, dbConnection);

                if (userinfo.RoleId == (int)Role.SuperAdmin)
                {
                    userinfo.CustomerInfoId = 87;
                }

                var staffData = RosterView.GetStaffAverageTemp(userinfo.CustomerInfoId, (int)TaskAssigneeType.User, fromdate.ToString(), todate.ToString(), dbConnection);

                staffData = staffData.Where(p => allusers.Any(p2 => p2.ID == p.EmployeeId)).ToList();

                var tblocks = rosterData.Where(i => i.TaskOn == true && i.ShiftOn == true).ToList();
                var dblocks = rosterData.Where(i => i.ShiftOn == true).ToList();

                var dutyrosterview = new DateTime(fromdate.Year, fromdate.Month, 1);
                var rv = RosterView.GetRosterViewByCurrentMonth(employee.ID, dutyrosterview.ToString(), dbConnection);

                if (rv != null)
                {

                    var tlist = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                    var evHistory = EventHistory.GetEventHistoryByUsername(employee.Username, dbConnection);

                    evHistory = evHistory.Where(i => i.IncidentAction == (int)CustomEvent.IncidentActionStatus.Complete).ToList();

                    evHistory = evHistory.Where(i => i.CreatedDate.Value.Date >= dutyrosterview.Date && i.CreatedDate.Value.Date <= dutyrosterview.AddDays(DateTime.DaysInMonth(dutyrosterview.Year, dutyrosterview.Month))).ToList();

                    tlist = tlist.Where(i => i.Action == (int)TaskStatus.Completed).ToList();

                    tlist = tlist.Where(i => i.CreatedDate.Value.Date >= dutyrosterview.Date && i.CreatedDate.Value.Date <= dutyrosterview.AddDays(DateTime.DaysInMonth(dutyrosterview.Year, dutyrosterview.Month))).ToList();

                    var totalAssignment = tlist.Count + evHistory.Count;


                    iTextSharp.text.Document doc = new iTextSharp.text.Document(PageSize.LETTER, 25F, 25F, 50F, 25F);

                    doc.SetMargins(25, 25, 65, 35);

                    var fileName = CommonUtility.getDTNow().Day.ToString() + "-" + CommonUtility.getDTNow().Month.ToString() + "-" + CommonUtility.getDTNow().Year.ToString() + "-" + CommonUtility.getDTNow().Hour.ToString() + "-" + CommonUtility.getDTNow().Minute.ToString() + "-" + CommonUtility.getDTNow().Second.ToString() + ".pdf";

                    PdfWriter writer = PdfWriter.GetInstance(doc, Response.OutputStream);//PdfWriter.GetInstance(doc, new FileStream(path, FileMode.Create));
                    writer.PageEvent = new ITextEvents()
                    {
                        cid = userinfo.CustomerInfoId
                    };
                    doc.Open();
                    doc.SetMargins(25, 25, 65, 35);
                    BaseFont f_cn = BaseFont.CreateFont(Environment.GetFolderPath(Environment.SpecialFolder.Fonts) + "\\verdana.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

                    BaseFont z_cn = BaseFont.CreateFont(BaseFont.ZAPFDINGBATS, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

                    Font times = new Font(f_cn, 12, Font.NORMAL, Color.BLACK);
                    Font btimes = new Font(f_cn, 12, Font.BOLD, Color.BLACK);

                    iTextSharp.text.Color arrowred = new iTextSharp.text.Color(162, 0, 46);

                    Font eleventimes = new Font(f_cn, 11, Font.NORMAL, Color.BLACK);
                    Font tentimes = new Font(f_cn, 10, Font.NORMAL, Color.BLACK);
                    Font rtentimes = new Font(f_cn, 10, Font.NORMAL, arrowred);
                    Font weleventimes = new Font(f_cn, 11, Font.NORMAL, Color.WHITE);

                    Font gweb = new Font(z_cn, 11, Font.NORMAL, Color.GREEN);
                    Font rweb = new Font(z_cn, 11, Font.NORMAL, Color.RED);

                    Font ueleventimes = new Font(f_cn, 11, Font.UNDERLINE, Color.BLACK);

                    Font beleventimes = new Font(f_cn, 11, Font.BOLD, Color.BLACK);
                    Font bredeleventimes = new Font(f_cn, 11, Font.BOLD, arrowred);
                    Font redeleventimes = new Font(f_cn, 11, Font.NORMAL, arrowred);
                    Font twelvetimes = new Font(f_cn, 12, Font.ITALIC, Color.BLACK);

                    Font mbtimes = new Font(f_cn, 18, Font.BOLD, arrowred);
                    Font cbtimes = new Font(f_cn, 13, Font.BOLD, arrowred);

                    float[] columnWidths1585 = new float[] { 15, 85 };
                    float[] columnWidths2080 = new float[] { 20, 80 };

                    float[] columnWidths1783 = new float[] { 18, 82 };
                    float[] columnWidths7030 = new float[] { 50, 30, 20 };
                    float[] columnWidths2575 = new float[] { 25, 75 };
                    float[] columnWidths3070 = new float[] { 30, 70 };
                    float[] columnWidths4060 = new float[] { 40, 60 };
                    float[] columnWidths3565 = new float[] { 35, 65 };
                    float[] columnWidths4555 = new float[] { 45, 55 };
                    float[] columnWidthsH = new float[] { 20, 70, 10 };

                    if (string.IsNullOrEmpty(userinfo.SiteName))
                        userinfo.SiteName = "N/A";

                    var mainheadertable = new PdfPTable(3);
                    mainheadertable.DefaultCell.Border = Rectangle.NO_BORDER;
                    mainheadertable.WidthPercentage = 100;
                    mainheadertable.SetWidths(columnWidthsH);
                    var headerMain = new PdfPCell();
                    if (userinfo.CustomerInfoId == 87 || userinfo.CustomerInfoId == 45)
                    {
                        headerMain.AddElement(new Phrase("G4S EMPLOYEE MONTHLY REPORT", mbtimes));
                    }
                    else
                    {
                        headerMain.AddElement(new Phrase("MIMS EMPLOYEE MONTHLY REPORT", mbtimes));
                    }

                    headerMain.PaddingBottom = 10;
                    headerMain.HorizontalAlignment = 1;
                    headerMain.Border = Rectangle.NO_BORDER;
                    PdfPCell pdfCell1 = new PdfPCell();
                    pdfCell1.Border = Rectangle.NO_BORDER;
                    PdfPCell pdfCell2 = new PdfPCell();
                    pdfCell2.Border = Rectangle.NO_BORDER;
                    mainheadertable.AddCell(pdfCell1);
                    mainheadertable.AddCell(headerMain);
                    mainheadertable.AddCell(pdfCell2);

                    doc.Add(mainheadertable);

                    var taskdataTable = new PdfPTable(2);
                    var taskdataTableA = new PdfPTable(2);
                    var taskdataTableB = new PdfPTable(2);
                    taskdataTable.DefaultCell.Border = Rectangle.NO_BORDER;
                    taskdataTable.WidthPercentage = 100;

                    taskdataTableA.DefaultCell.Border = Rectangle.NO_BORDER;
                    taskdataTableB.DefaultCell.Border = Rectangle.NO_BORDER;

                    taskdataTableA.WidthPercentage = 100;
                    taskdataTableB.WidthPercentage = 100;

                    taskdataTableA.SetWidths(columnWidths3565);

                    taskdataTableB.SetWidths(columnWidths3565);

                    var header3 = new PdfPCell();
                    header3.AddElement(new Phrase("Period:", beleventimes));
                    header3.Border = Rectangle.NO_BORDER;
                    taskdataTableA.AddCell(header3);

                    var header3a = new PdfPCell();
                    header3a.AddElement(new Phrase(fromdate.ToShortDateString() + " to " + todate.ToShortDateString(), eleventimes));
                    header3a.Border = Rectangle.NO_BORDER;
                    taskdataTableA.AddCell(header3a);

                    var header1 = new PdfPCell();
                    header1.AddElement(new Phrase("Employee:", beleventimes));
                    header1.Border = Rectangle.NO_BORDER;
                    taskdataTableA.AddCell(header1);

                    var header1a = new PdfPCell();
                    header1a.AddElement(new Phrase(employee.FirstName + " " + employee.LastName, eleventimes));
                    header1a.Border = Rectangle.NO_BORDER;
                    taskdataTableA.AddCell(header1a);

                    var header2 = new PdfPCell();
                    header2.AddElement(new Phrase("Role:", beleventimes));
                    header2.Border = Rectangle.NO_BORDER;

                    taskdataTableA.AddCell(header2);

                    var header2a = new PdfPCell();
                    header2a.AddElement(new Phrase(CommonUtility.getUserRoleName(employee.RoleId), eleventimes));
                    header2a.Border = Rectangle.NO_BORDER;
                    taskdataTableA.AddCell(header2a);

                    var bheader1 = new PdfPCell();
                    bheader1.AddElement(new Phrase("Business Unit:", beleventimes));
                    bheader1.Border = Rectangle.NO_BORDER;
                    taskdataTableB.AddCell(bheader1);

                    var bheader1a = new PdfPCell();
                    bheader1a.AddElement(new Phrase(employee.SiteName, eleventimes));
                    bheader1a.Border = Rectangle.NO_BORDER;
                    taskdataTableB.AddCell(bheader1a);

                    var bheader2 = new PdfPCell();
                    bheader2.AddElement(new Phrase("Supervisor:", beleventimes));
                    bheader2.Border = Rectangle.NO_BORDER;

                    taskdataTableB.AddCell(bheader2);

                    var supervisor = "Unassigned";

                    if (employee.RoleId == (int)Role.Operator)
                    {
                        var usermanager = Users.GetAllFullManagersByUserId(employee.ID, dbConnection);
                        if (usermanager != null)
                        {
                            supervisor = usermanager.FirstName + " " + usermanager.LastName;
                        }
                    }
                    else if (employee.RoleId == (int)Role.Manager)
                    {
                        var getdir = Accounts.GetDirectorByManagerName(employee.Username, dbConnection);
                        if (getdir != null)
                        {
                            supervisor = getdir.FirstName + " " + getdir.LastName;
                        }
                    }
                    else if (employee.RoleId == (int)Role.Admin)
                    {
                        var getdir = Users.GetAllUsersBySiteIdAndRole(employee.SiteId, (int)Role.Director, dbConnection);
                        if (getdir != null && getdir.Count > 0)
                        {
                            supervisor = getdir[0].FirstName + " " + getdir[0].LastName;
                        }
                    }
                    else if (employee.RoleId == (int)Role.Director)
                    {
                        var getdir = Users.GetAllUsersByCustomerId(employee.CustomerInfoId, dbConnection);

                        getdir = getdir.Where(i => i.RoleId == (int)Role.CustomerSuperadmin).ToList();

                        if (getdir != null && getdir.Count > 0)
                        {
                            supervisor = getdir[0].FirstName + " " + getdir[0].LastName;
                        }
                    }
                    var bheader2a = new PdfPCell();
                    bheader2a.AddElement(new Phrase(supervisor, eleventimes));
                    bheader2a.Border = Rectangle.NO_BORDER;
                    taskdataTableB.AddCell(bheader2a);

                    taskdataTable.AddCell(taskdataTableA);
                    taskdataTable.AddCell(taskdataTableB);

                    doc.Add(taskdataTable);

                    var variancetotal = 0;

                    //TASK TABLES PENDING
                    float[] columnWidthsCHK = new float[] { 15, 15, 24, 23, 23 };

                    var newchecklistTable = new PdfPTable(8);
                    newchecklistTable.DefaultCell.Border = Rectangle.NO_BORDER;
                    newchecklistTable.WidthPercentage = 100;
                    //newchecklistTable.SetWidths(columnWidthsCHK);

                    var cheader1 = new PdfPCell();
                    cheader1.AddElement(new Phrase("Details", cbtimes));
                    cheader1.Border = Rectangle.NO_BORDER;
                    cheader1.PaddingTop = 10;
                    cheader1.PaddingBottom = 10;
                    cheader1.Colspan = 8;
                    newchecklistTable.AddCell(cheader1);

                    var ncaheader1 = new PdfPCell();
                    ncaheader1.AddElement(new Phrase("Date", weleventimes));
                    ncaheader1.Border = Rectangle.NO_BORDER;
                    ncaheader1.PaddingLeft = 5;
                    ncaheader1.PaddingBottom = 5;
                    ncaheader1.BackgroundColor = Color.GRAY;

                    newchecklistTable.AddCell(ncaheader1);

                    var ncaheader2 = new PdfPCell();
                    ncaheader2.AddElement(new Phrase("Shift Hours", weleventimes));
                    ncaheader2.Border = Rectangle.NO_BORDER;
                    ncaheader2.BackgroundColor = Color.GRAY;
                    newchecklistTable.AddCell(ncaheader2);

                    var ncaheader3 = new PdfPCell();
                    ncaheader3.AddElement(new Phrase("Worked Hours", weleventimes));
                    ncaheader3.Border = Rectangle.NO_BORDER;
                    ncaheader3.BackgroundColor = Color.GRAY;
                    newchecklistTable.AddCell(ncaheader3);

                    var ncaheader4 = new PdfPCell();
                    ncaheader4.AddElement(new Phrase("Variance", weleventimes));
                    ncaheader4.Border = Rectangle.NO_BORDER;
                    ncaheader4.BackgroundColor = Color.GRAY;
                    newchecklistTable.AddCell(ncaheader4);

                    var ncaheader5tt = new PdfPCell();
                    ncaheader5tt.AddElement(new Phrase("Travel Time", weleventimes));
                    ncaheader5tt.Border = Rectangle.NO_BORDER;
                    ncaheader5tt.BackgroundColor = Color.GRAY;
                    newchecklistTable.AddCell(ncaheader5tt);

                    var ncaheader5ot = new PdfPCell();
                    ncaheader5ot.AddElement(new Phrase("Overtime", weleventimes));
                    ncaheader5ot.Border = Rectangle.NO_BORDER;
                    ncaheader5ot.BackgroundColor = Color.GRAY;
                    newchecklistTable.AddCell(ncaheader5ot);

                    var ncaheader5 = new PdfPCell();
                    ncaheader5.AddElement(new Phrase("Active Hours", weleventimes));
                    ncaheader5.Border = Rectangle.NO_BORDER;
                    ncaheader5.BackgroundColor = Color.GRAY;
                    newchecklistTable.AddCell(ncaheader5);

                    var ncaheader6 = new PdfPCell();
                    ncaheader6.AddElement(new Phrase("Comment", weleventimes));
                    ncaheader6.Border = Rectangle.NO_BORDER;
                    ncaheader6.BackgroundColor = Color.GRAY;
                    newchecklistTable.AddCell(ncaheader6);

                    var day1 = rosterData.Where(i => i.DutyDateTime.Value.Date == dutyrosterview.Date).ToList();
                    var day2 = rosterData.Where(i => i.DutyDateTime.Value.Date == dutyrosterview.Date.AddDays(1)).ToList();
                    var day3 = rosterData.Where(i => i.DutyDateTime.Value.Date == dutyrosterview.Date.AddDays(2)).ToList();
                    var day4 = rosterData.Where(i => i.DutyDateTime.Value.Date == dutyrosterview.Date.AddDays(3)).ToList();
                    var day5 = rosterData.Where(i => i.DutyDateTime.Value.Date == dutyrosterview.Date.AddDays(4)).ToList();
                    var day6 = rosterData.Where(i => i.DutyDateTime.Value.Date == dutyrosterview.Date.AddDays(5)).ToList();
                    var day7 = rosterData.Where(i => i.DutyDateTime.Value.Date == dutyrosterview.Date.AddDays(6)).ToList();
                    var day8 = rosterData.Where(i => i.DutyDateTime.Value.Date == dutyrosterview.Date.AddDays(7)).ToList();
                    var day9 = rosterData.Where(i => i.DutyDateTime.Value.Date == dutyrosterview.Date.AddDays(8)).ToList();
                    var day10 = rosterData.Where(i => i.DutyDateTime.Value.Date == dutyrosterview.Date.AddDays(9)).ToList();
                    var day11 = rosterData.Where(i => i.DutyDateTime.Value.Date == dutyrosterview.Date.AddDays(10)).ToList();
                    var day12 = rosterData.Where(i => i.DutyDateTime.Value.Date == dutyrosterview.Date.AddDays(11)).ToList();
                    var day13 = rosterData.Where(i => i.DutyDateTime.Value.Date == dutyrosterview.Date.AddDays(12)).ToList();
                    var day14 = rosterData.Where(i => i.DutyDateTime.Value.Date == dutyrosterview.Date.AddDays(13)).ToList();
                    var day15 = rosterData.Where(i => i.DutyDateTime.Value.Date == dutyrosterview.Date.AddDays(14)).ToList();
                    var day16 = rosterData.Where(i => i.DutyDateTime.Value.Date == dutyrosterview.Date.AddDays(15)).ToList();
                    var day17 = rosterData.Where(i => i.DutyDateTime.Value.Date == dutyrosterview.Date.AddDays(16)).ToList();
                    var day18 = rosterData.Where(i => i.DutyDateTime.Value.Date == dutyrosterview.Date.AddDays(17)).ToList();
                    var day19 = rosterData.Where(i => i.DutyDateTime.Value.Date == dutyrosterview.Date.AddDays(18)).ToList();
                    var day20 = rosterData.Where(i => i.DutyDateTime.Value.Date == dutyrosterview.Date.AddDays(19)).ToList();
                    var day21 = rosterData.Where(i => i.DutyDateTime.Value.Date == dutyrosterview.Date.AddDays(20)).ToList();
                    var day22 = rosterData.Where(i => i.DutyDateTime.Value.Date == dutyrosterview.Date.AddDays(21)).ToList();
                    var day23 = rosterData.Where(i => i.DutyDateTime.Value.Date == dutyrosterview.Date.AddDays(22)).ToList();
                    var day24 = rosterData.Where(i => i.DutyDateTime.Value.Date == dutyrosterview.Date.AddDays(23)).ToList();
                    var day25 = rosterData.Where(i => i.DutyDateTime.Value.Date == dutyrosterview.Date.AddDays(24)).ToList();
                    var day26 = rosterData.Where(i => i.DutyDateTime.Value.Date == dutyrosterview.Date.AddDays(25)).ToList();
                    var day27 = rosterData.Where(i => i.DutyDateTime.Value.Date == dutyrosterview.Date.AddDays(26)).ToList();
                    var day28 = rosterData.Where(i => i.DutyDateTime.Value.Date == dutyrosterview.Date.AddDays(27)).ToList();
                    var day29 = rosterData.Where(i => i.DutyDateTime.Value.Date == dutyrosterview.Date.AddDays(28)).ToList();
                    var day30 = rosterData.Where(i => i.DutyDateTime.Value.Date == dutyrosterview.Date.AddDays(29)).ToList();
                    var day31 = rosterData.Where(i => i.DutyDateTime.Value.Date == dutyrosterview.Date.AddDays(30)).ToList();

                    var sickdays = 0;
                    var holidays = 0;
                    var dayoffs = 0;
                    var vacations = 0;

                    var getots = JobOTHours.GetJobOTHoursByUserId(employee.ID, fromdate, todate.AddHours(23), dbConnection);
                    var totalOT = 0.0;
                    var totalTT = 0.0;
                    //day 1
                    if (day1.Count > 0)
                    {
                        var sdays = day1.Where(i => i.DutyPost == "SICK DAY").ToList();
                        sickdays = sickdays + sdays.Count;
                        var hdays = day1.Where(i => i.DutyPost == "HOLIDAY").ToList();
                        holidays = holidays + hdays.Count;
                        var ddays = day1.Where(i => i.DutyPost == "DAY OFF").ToList();
                        dayoffs = dayoffs + ddays.Count;
                        var vdays = day1.Where(i => i.DutyPost == "VACATION").ToList();
                        vacations = vacations + vdays.Count;

                        day1 = day1.Where(i => i.DutyPost != "DAY OFF" && i.DutyPost != "HOLIDAY" && i.DutyPost != "SICK DAY" && i.DutyPost != "VACATION").ToList();

                        var comments = new Phrase(1, "\u00a0");

                        if (sdays.Count > 0)
                        {
                            comments = new Phrase("Sick Day", tentimes);
                        }
                        if (hdays.Count > 0)
                        {
                            comments = new Phrase("Holiday", tentimes);
                        }
                        if (ddays.Count > 0)
                        {
                            comments = new Phrase("Day Off", tentimes);
                        }
                        if (vdays.Count > 0)
                        {
                            comments = new Phrase("Vacation", tentimes);
                        }
                        var xheaderCell6 = new PdfPCell(comments);

                        var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.ToShortDateString(), tentimes));

                        var xheaderCell2 = new PdfPCell(new Phrase(day1.Count.ToString(), tentimes));

                        var wh = day1.Where(i => i.ShiftOn == true).ToList();

                        var xheaderCell3 = new PdfPCell(new Phrase(wh.Count.ToString(), tentimes));

                        var xheaderCell4 = new PdfPCell(new Phrase((wh.Count - day1.Count).ToString(), rtentimes));

                        variancetotal = (wh.Count - day1.Count);

                        var ah = day1.Where(i => i.TaskOn == true && i.ShiftOn == true).ToList();

                        var xheaderCell5 = new PdfPCell(new Phrase(ah.Count.ToString(), tentimes));



                        var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                        tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                        tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dutyrosterview.Date && i.CreatedDate.Value.Date <= dutyrosterview.Date.AddHours(23)).ToList();

                        var groupedtevents = tevents.GroupBy(item => item.TaskId);
                        tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                        var ttraveldurationlist = 0.0;

                        foreach (var t in tevents)
                        {
                            var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                            if (task != null)
                            {
                                var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dutyrosterview.Date);
                                ttraveldurationlist = tt + ttraveldurationlist;
                            }
                        }

                        var d = CommonUtility.GetUserOTHoursPerDay(dutyrosterview.Date, dutyrosterview.Date.AddHours(23), getots);//GetUserOTHoursForEmployeeReport(employee.ID, dutyrosterview.Date, dutyrosterview.Date.AddHours(23));

                        var xheaderCelltt = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                        var xheaderCelld = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));
                        totalOT = totalOT + d;
                        totalTT = totalTT + ttraveldurationlist;
                        xheaderCell.Border = Rectangle.NO_BORDER;
                        xheaderCell2.Border = Rectangle.NO_BORDER;
                        xheaderCell3.Border = Rectangle.NO_BORDER;
                        xheaderCell4.Border = Rectangle.NO_BORDER;
                        xheaderCell5.Border = Rectangle.NO_BORDER;
                        xheaderCell6.Border = Rectangle.NO_BORDER;
                        xheaderCelltt.Border = Rectangle.NO_BORDER;
                        xheaderCelld.Border = Rectangle.NO_BORDER;

                        xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCelltt.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCelld.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        newchecklistTable.AddCell(xheaderCell);
                        newchecklistTable.AddCell(xheaderCell2);
                        newchecklistTable.AddCell(xheaderCell3);
                        newchecklistTable.AddCell(xheaderCell4);

                        newchecklistTable.AddCell(xheaderCelltt);
                        newchecklistTable.AddCell(xheaderCelld);

                        newchecklistTable.AddCell(xheaderCell5);
                        newchecklistTable.AddCell(xheaderCell6);
                    }
                    else
                    {
                        var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.ToShortDateString(), tentimes));
                        var xheaderCell2 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell3 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell4 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell5 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell6 = new PdfPCell(new Phrase(1, "\u00a0"));

                        var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                        tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                        tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dutyrosterview.Date && i.CreatedDate.Value.Date <= dutyrosterview.Date.AddHours(23)).ToList();

                        var groupedtevents = tevents.GroupBy(item => item.TaskId);
                        tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                        var ttraveldurationlist = 0.0;

                        foreach (var t in tevents)
                        {
                            var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                            if (task != null)
                            {
                                var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dutyrosterview.Date);
                                ttraveldurationlist = tt + ttraveldurationlist;
                            }
                        }

                        //var d = CommonUtility.GetUserOTHoursForEmployeeReport(employee.ID, dutyrosterview.Date, dutyrosterview.Date.AddHours(23));

                        var d = CommonUtility.GetUserOTHoursPerDay(dutyrosterview.Date, dutyrosterview.Date.AddHours(23), getots);

                        var xheaderCell7 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                        var xheaderCell8 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));
                        totalOT = totalOT + d;
                        totalTT = totalTT + ttraveldurationlist;
                        xheaderCell.Border = Rectangle.NO_BORDER;
                        xheaderCell2.Border = Rectangle.NO_BORDER;
                        xheaderCell3.Border = Rectangle.NO_BORDER;
                        xheaderCell4.Border = Rectangle.NO_BORDER;
                        xheaderCell5.Border = Rectangle.NO_BORDER;
                        xheaderCell6.Border = Rectangle.NO_BORDER;
                        xheaderCell7.Border = Rectangle.NO_BORDER;
                        xheaderCell8.Border = Rectangle.NO_BORDER;

                        xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell7.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell8.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        newchecklistTable.AddCell(xheaderCell);
                        newchecklistTable.AddCell(xheaderCell2);
                        newchecklistTable.AddCell(xheaderCell3);
                        newchecklistTable.AddCell(xheaderCell4);
                        newchecklistTable.AddCell(xheaderCell7);
                        newchecklistTable.AddCell(xheaderCell8);
                        newchecklistTable.AddCell(xheaderCell5);
                        newchecklistTable.AddCell(xheaderCell6);

                    }
                    //day 2
                    if (day2.Count > 0)
                    {

                        var sdays = day2.Where(i => i.DutyPost == "SICK DAY").ToList();
                        sickdays = sickdays + sdays.Count;
                        var hdays = day2.Where(i => i.DutyPost == "HOLIDAY").ToList();
                        holidays = holidays + hdays.Count;
                        var ddays = day2.Where(i => i.DutyPost == "DAY OFF").ToList();
                        dayoffs = dayoffs + ddays.Count;
                        var vdays = day2.Where(i => i.DutyPost == "VACATION").ToList();
                        vacations = vacations + vdays.Count;

                        day2 = day2.Where(i => i.DutyPost != "DAY OFF" && i.DutyPost != "HOLIDAY" && i.DutyPost != "SICK DAY" && i.DutyPost != "VACATION").ToList();

                        var comments = new Phrase(1, "\u00a0");

                        if (sdays.Count > 0)
                        {
                            comments = new Phrase("Sick Day", tentimes);
                        }
                        if (hdays.Count > 0)
                        {
                            comments = new Phrase("Holiday", tentimes);
                        }
                        if (ddays.Count > 0)
                        {
                            comments = new Phrase("Day Off", tentimes);
                        }
                        if (vdays.Count > 0)
                        {
                            comments = new Phrase("Vacation", tentimes);
                        }
                        var xheaderCell6 = new PdfPCell(comments);

                        var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.AddDays(1).ToShortDateString(), tentimes));

                        var xheaderCell2 = new PdfPCell(new Phrase(day2.Count.ToString(), tentimes));

                        var wh = day2.Where(i => i.ShiftOn == true).ToList();

                        var xheaderCell3 = new PdfPCell(new Phrase(wh.Count.ToString(), tentimes));

                        var xheaderCell4 = new PdfPCell(new Phrase((wh.Count - day2.Count).ToString(), rtentimes));

                        variancetotal = variancetotal + (wh.Count - day2.Count);

                        var ah = day2.Where(i => i.TaskOn == true && i.ShiftOn == true).ToList();

                        var xheaderCell5 = new PdfPCell(new Phrase(ah.Count.ToString(), tentimes));


                        var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                        tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                        tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dutyrosterview.Date.AddDays(1) && i.CreatedDate.Value.Date <= dutyrosterview.Date.AddDays(1).AddHours(23)).ToList();

                        var groupedtevents = tevents.GroupBy(item => item.TaskId);
                        tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                        var ttraveldurationlist = 0.0;

                        foreach (var t in tevents)
                        {
                            var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                            if (task != null)
                            {
                                var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dutyrosterview.Date.AddDays(1));
                                ttraveldurationlist = tt + ttraveldurationlist;
                            }
                        }

                        //var d = CommonUtility.GetUserOTHoursForEmployeeReport(employee.ID, dutyrosterview.Date.AddDays(1), dutyrosterview.Date.AddDays(1).AddHours(23));

                        var d = CommonUtility.GetUserOTHoursPerDay(dutyrosterview.Date.AddDays(1), dutyrosterview.Date.AddDays(1).AddHours(23), getots);

                        var xheaderCelltt = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                        var xheaderCelld = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));
                        totalOT = totalOT + d;
                        totalTT = totalTT + ttraveldurationlist;
                        xheaderCell.Border = Rectangle.NO_BORDER;
                        xheaderCell2.Border = Rectangle.NO_BORDER;
                        xheaderCell3.Border = Rectangle.NO_BORDER;
                        xheaderCell4.Border = Rectangle.NO_BORDER;
                        xheaderCell5.Border = Rectangle.NO_BORDER;
                        xheaderCell6.Border = Rectangle.NO_BORDER;
                        xheaderCelltt.Border = Rectangle.NO_BORDER;
                        xheaderCelld.Border = Rectangle.NO_BORDER;

                        xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCelltt.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCelld.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        xheaderCell.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell2.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell3.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell4.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell5.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell6.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCelltt.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCelld.BackgroundColor = Color.LIGHT_GRAY;

                        newchecklistTable.AddCell(xheaderCell);
                        newchecklistTable.AddCell(xheaderCell2);
                        newchecklistTable.AddCell(xheaderCell3);
                        newchecklistTable.AddCell(xheaderCell4);

                        newchecklistTable.AddCell(xheaderCelltt);
                        newchecklistTable.AddCell(xheaderCelld);

                        newchecklistTable.AddCell(xheaderCell5);
                        newchecklistTable.AddCell(xheaderCell6);
                    }
                    else
                    {
                        var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.AddDays(1).ToShortDateString(), tentimes));
                        var xheaderCell2 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell3 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell4 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell5 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell6 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                        tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                        tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dutyrosterview.Date.AddDays(1) && i.CreatedDate.Value.Date <= dutyrosterview.Date.AddDays(1).AddHours(23)).ToList();

                        var groupedtevents = tevents.GroupBy(item => item.TaskId);
                        tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                        var ttraveldurationlist = 0.0;

                        foreach (var t in tevents)
                        {
                            var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                            if (task != null)
                            {
                                var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dutyrosterview.Date.AddDays(1));
                                ttraveldurationlist = tt + ttraveldurationlist;
                            }
                        }

                        var d = CommonUtility.GetUserOTHoursPerDay(dutyrosterview.Date.AddDays(1), dutyrosterview.Date.AddDays(1).AddHours(23), getots);

                        var xheaderCell7 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                        var xheaderCell8 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));
                        totalOT = totalOT + d;
                        totalTT = totalTT + ttraveldurationlist;
                        xheaderCell.Border = Rectangle.NO_BORDER;
                        xheaderCell2.Border = Rectangle.NO_BORDER;
                        xheaderCell3.Border = Rectangle.NO_BORDER;
                        xheaderCell4.Border = Rectangle.NO_BORDER;
                        xheaderCell5.Border = Rectangle.NO_BORDER;
                        xheaderCell6.Border = Rectangle.NO_BORDER;
                        xheaderCell7.Border = Rectangle.NO_BORDER;
                        xheaderCell8.Border = Rectangle.NO_BORDER;

                        xheaderCell.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell2.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell3.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell4.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell5.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell6.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell7.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell8.BackgroundColor = Color.LIGHT_GRAY;

                        xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell7.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell8.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        newchecklistTable.AddCell(xheaderCell);
                        newchecklistTable.AddCell(xheaderCell2);
                        newchecklistTable.AddCell(xheaderCell3);
                        newchecklistTable.AddCell(xheaderCell4);
                        newchecklistTable.AddCell(xheaderCell7);
                        newchecklistTable.AddCell(xheaderCell8);
                        newchecklistTable.AddCell(xheaderCell5);
                        newchecklistTable.AddCell(xheaderCell6);
                    }
                    //day 3
                    if (day3.Count > 0)
                    {
                        var sdays = day3.Where(i => i.DutyPost == "SICK DAY").ToList();
                        sickdays = sickdays + sdays.Count;
                        var hdays = day3.Where(i => i.DutyPost == "HOLIDAY").ToList();
                        holidays = holidays + hdays.Count;
                        var ddays = day3.Where(i => i.DutyPost == "DAY OFF").ToList();
                        dayoffs = dayoffs + ddays.Count;
                        var vdays = day3.Where(i => i.DutyPost == "VACATION").ToList();
                        vacations = vacations + vdays.Count;

                        day3 = day3.Where(i => i.DutyPost != "DAY OFF" && i.DutyPost != "HOLIDAY" && i.DutyPost != "SICK DAY" && i.DutyPost != "VACATION").ToList();

                        var comments = new Phrase(1, "\u00a0");

                        if (sdays.Count > 0)
                        {
                            comments = new Phrase("Sick Day", tentimes);
                        }
                        if (hdays.Count > 0)
                        {
                            comments = new Phrase("Holiday", tentimes);
                        }
                        if (ddays.Count > 0)
                        {
                            comments = new Phrase("Day Off", tentimes);
                        }
                        if (vdays.Count > 0)
                        {
                            comments = new Phrase("Vacation", tentimes);
                        }
                        var xheaderCell6 = new PdfPCell(comments);

                        var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.AddDays(2).ToShortDateString(), tentimes));

                        var xheaderCell2 = new PdfPCell(new Phrase(day3.Count.ToString(), tentimes));

                        var wh = day3.Where(i => i.ShiftOn == true).ToList();

                        var xheaderCell3 = new PdfPCell(new Phrase(wh.Count.ToString(), tentimes));

                        var xheaderCell4 = new PdfPCell(new Phrase((wh.Count - day3.Count).ToString(), rtentimes));

                        variancetotal = variancetotal + (wh.Count - day3.Count);

                        var ah = day3.Where(i => i.TaskOn == true && i.ShiftOn == true).ToList();

                        var xheaderCell5 = new PdfPCell(new Phrase(ah.Count.ToString(), tentimes));


                        var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                        tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                        tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dutyrosterview.Date.AddDays(2) && i.CreatedDate.Value.Date <= dutyrosterview.Date.AddDays(2).AddHours(23)).ToList();

                        var groupedtevents = tevents.GroupBy(item => item.TaskId);
                        tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                        var ttraveldurationlist = 0.0;
                        var dttocheck = dutyrosterview.Date.AddDays(2);
                        foreach (var t in tevents)
                        {
                            var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                            if (task != null)
                            {
                                var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dttocheck);
                                ttraveldurationlist = tt + ttraveldurationlist;
                            }
                        }

                        //var d = CommonUtility.GetUserOTHoursForEmployeeReport(employee.ID, dutyrosterview.Date.AddDays(2), dutyrosterview.Date.AddDays(2).AddHours(23));

                        var d = CommonUtility.GetUserOTHoursPerDay(dutyrosterview.Date.AddDays(2), dutyrosterview.Date.AddDays(2).AddHours(23), getots);

                        var xheaderCelltt = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                        var xheaderCelld = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));
                        totalOT = totalOT + d;
                        totalTT = totalTT + ttraveldurationlist;
                        xheaderCell.Border = Rectangle.NO_BORDER;
                        xheaderCell2.Border = Rectangle.NO_BORDER;
                        xheaderCell3.Border = Rectangle.NO_BORDER;
                        xheaderCell4.Border = Rectangle.NO_BORDER;
                        xheaderCell5.Border = Rectangle.NO_BORDER;
                        xheaderCell6.Border = Rectangle.NO_BORDER;

                        xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        newchecklistTable.AddCell(xheaderCell);
                        newchecklistTable.AddCell(xheaderCell2);
                        newchecklistTable.AddCell(xheaderCell3);
                        newchecklistTable.AddCell(xheaderCell4);

                        xheaderCelltt.Border = Rectangle.NO_BORDER;
                        xheaderCelld.Border = Rectangle.NO_BORDER;

                        xheaderCelltt.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCelld.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        newchecklistTable.AddCell(xheaderCelltt);
                        newchecklistTable.AddCell(xheaderCelld);

                        newchecklistTable.AddCell(xheaderCell5);
                        newchecklistTable.AddCell(xheaderCell6);
                    }
                    else
                    {
                        var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.AddDays(2).ToShortDateString(), tentimes));
                        var xheaderCell2 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell3 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell4 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell5 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell6 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                        tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                        var dttocheck = dutyrosterview.Date.AddDays(2);

                        tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dttocheck && i.CreatedDate.Value.Date <= dttocheck.AddHours(23)).ToList();

                        var groupedtevents = tevents.GroupBy(item => item.TaskId);
                        tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                        var ttraveldurationlist = 0.0;

                        foreach (var t in tevents)
                        {
                            var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                            if (task != null)
                            {
                                var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dttocheck);
                                ttraveldurationlist = tt + ttraveldurationlist;
                            }
                        }

                        var d = CommonUtility.GetUserOTHoursPerDay(dttocheck, dttocheck.AddHours(23), getots);

                        var xheaderCell7 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                        var xheaderCell8 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));
                        totalOT = totalOT + d;
                        totalTT = totalTT + ttraveldurationlist;
                        xheaderCell.Border = Rectangle.NO_BORDER;
                        xheaderCell2.Border = Rectangle.NO_BORDER;
                        xheaderCell3.Border = Rectangle.NO_BORDER;
                        xheaderCell4.Border = Rectangle.NO_BORDER;
                        xheaderCell5.Border = Rectangle.NO_BORDER;
                        xheaderCell6.Border = Rectangle.NO_BORDER;
                        xheaderCell7.Border = Rectangle.NO_BORDER;
                        xheaderCell8.Border = Rectangle.NO_BORDER;

                        xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell7.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell8.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        newchecklistTable.AddCell(xheaderCell);
                        newchecklistTable.AddCell(xheaderCell2);
                        newchecklistTable.AddCell(xheaderCell3);
                        newchecklistTable.AddCell(xheaderCell4);
                        newchecklistTable.AddCell(xheaderCell7);
                        newchecklistTable.AddCell(xheaderCell8);
                        newchecklistTable.AddCell(xheaderCell5);
                        newchecklistTable.AddCell(xheaderCell6);
                    }
                    //day 4
                    if (day4.Count > 0)
                    {
                        var sdays = day4.Where(i => i.DutyPost == "SICK DAY").ToList();
                        sickdays = sickdays + sdays.Count;
                        var hdays = day4.Where(i => i.DutyPost == "HOLIDAY").ToList();
                        holidays = holidays + hdays.Count;
                        var ddays = day4.Where(i => i.DutyPost == "DAY OFF").ToList();
                        dayoffs = dayoffs + ddays.Count;
                        var vdays = day4.Where(i => i.DutyPost == "VACATION").ToList();
                        vacations = vacations + vdays.Count;

                        day4 = day4.Where(i => i.DutyPost != "DAY OFF" && i.DutyPost != "HOLIDAY" && i.DutyPost != "SICK DAY" && i.DutyPost != "VACATION").ToList();

                        var comments = new Phrase(1, "\u00a0");

                        if (sdays.Count > 0)
                        {
                            comments = new Phrase("Sick Day", tentimes);
                        }
                        if (hdays.Count > 0)
                        {
                            comments = new Phrase("Holiday", tentimes);
                        }
                        if (ddays.Count > 0)
                        {
                            comments = new Phrase("Day Off", tentimes);
                        }
                        if (vdays.Count > 0)
                        {
                            comments = new Phrase("Vacation", tentimes);
                        }
                        var xheaderCell6 = new PdfPCell(comments);

                        var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.AddDays(3).ToShortDateString(), tentimes));

                        var xheaderCell2 = new PdfPCell(new Phrase(day4.Count.ToString(), tentimes));

                        var wh = day4.Where(i => i.ShiftOn == true).ToList();

                        var xheaderCell3 = new PdfPCell(new Phrase(wh.Count.ToString(), tentimes));

                        var xheaderCell4 = new PdfPCell(new Phrase((wh.Count - day4.Count).ToString(), rtentimes));

                        variancetotal = variancetotal + (wh.Count - day4.Count);

                        var ah = day4.Where(i => i.TaskOn == true && i.ShiftOn == true).ToList();

                        var xheaderCell5 = new PdfPCell(new Phrase(ah.Count.ToString(), tentimes));


                        var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                        tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                        tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dutyrosterview.Date.AddDays(3) && i.CreatedDate.Value.Date <= dutyrosterview.Date.AddDays(3).AddHours(23)).ToList();

                        var groupedtevents = tevents.GroupBy(item => item.TaskId);
                        tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                        var ttraveldurationlist = 0.0;
                        var dttocheck = dutyrosterview.Date.AddDays(3);
                        foreach (var t in tevents)
                        {
                            var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                            if (task != null)
                            {
                                var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dttocheck);
                                ttraveldurationlist = tt + ttraveldurationlist;
                            }
                        }


                        var d = CommonUtility.GetUserOTHoursPerDay(dttocheck, dttocheck.AddHours(23), getots);
                        //var d = CommonUtility.GetUserOTHoursForEmployeeReport(employee.ID, dutyrosterview.Date.AddDays(3), dutyrosterview.Date.AddDays(3).AddHours(23));

                        var xheaderCelltt = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                        var xheaderCelld = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));

                        totalOT = totalOT + d;
                        totalTT = totalTT + ttraveldurationlist;

                        xheaderCell.Border = Rectangle.NO_BORDER;
                        xheaderCell2.Border = Rectangle.NO_BORDER;
                        xheaderCell3.Border = Rectangle.NO_BORDER;
                        xheaderCell4.Border = Rectangle.NO_BORDER;
                        xheaderCell5.Border = Rectangle.NO_BORDER;
                        xheaderCell6.Border = Rectangle.NO_BORDER;

                        xheaderCell.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell2.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell3.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell4.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell5.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell6.BackgroundColor = Color.LIGHT_GRAY;


                        xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;


                        newchecklistTable.AddCell(xheaderCell);
                        newchecklistTable.AddCell(xheaderCell2);
                        newchecklistTable.AddCell(xheaderCell3);
                        newchecklistTable.AddCell(xheaderCell4);

                        xheaderCelltt.Border = Rectangle.NO_BORDER;
                        xheaderCelld.Border = Rectangle.NO_BORDER;

                        xheaderCelltt.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCelld.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        xheaderCelltt.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCelld.BackgroundColor = Color.LIGHT_GRAY;

                        newchecklistTable.AddCell(xheaderCelltt);
                        newchecklistTable.AddCell(xheaderCelld);

                        newchecklistTable.AddCell(xheaderCell5);
                        newchecklistTable.AddCell(xheaderCell6);
                    }
                    else
                    {
                        var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.AddDays(3).ToShortDateString(), tentimes));
                        var xheaderCell2 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell3 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell4 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell5 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell6 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                        tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                        var dttocheck = dutyrosterview.Date.AddDays(3);

                        tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dttocheck && i.CreatedDate.Value.Date <= dttocheck.AddHours(23)).ToList();

                        var groupedtevents = tevents.GroupBy(item => item.TaskId);
                        tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                        var ttraveldurationlist = 0.0;

                        foreach (var t in tevents)
                        {
                            var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                            if (task != null)
                            {
                                var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dttocheck);
                                ttraveldurationlist = tt + ttraveldurationlist;
                            }
                        }

                        var d = CommonUtility.GetUserOTHoursPerDay(dttocheck, dttocheck.AddHours(23), getots);

                        var xheaderCell7 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                        var xheaderCell8 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));

                        totalOT = totalOT + d;
                        totalTT = totalTT + ttraveldurationlist;

                        xheaderCell.Border = Rectangle.NO_BORDER;
                        xheaderCell2.Border = Rectangle.NO_BORDER;
                        xheaderCell3.Border = Rectangle.NO_BORDER;
                        xheaderCell4.Border = Rectangle.NO_BORDER;
                        xheaderCell5.Border = Rectangle.NO_BORDER;
                        xheaderCell6.Border = Rectangle.NO_BORDER;
                        xheaderCell7.Border = Rectangle.NO_BORDER;
                        xheaderCell8.Border = Rectangle.NO_BORDER;

                        xheaderCell.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell2.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell3.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell4.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell5.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell6.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell7.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell8.BackgroundColor = Color.LIGHT_GRAY;

                        xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell7.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell8.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        newchecklistTable.AddCell(xheaderCell);
                        newchecklistTable.AddCell(xheaderCell2);
                        newchecklistTable.AddCell(xheaderCell3);
                        newchecklistTable.AddCell(xheaderCell4);
                        newchecklistTable.AddCell(xheaderCell7);
                        newchecklistTable.AddCell(xheaderCell8);
                        newchecklistTable.AddCell(xheaderCell5);
                        newchecklistTable.AddCell(xheaderCell6);
                    }

                    //day 5
                    if (day5.Count > 0)
                    {
                        var sdays = day5.Where(i => i.DutyPost == "SICK DAY").ToList();
                        sickdays = sickdays + sdays.Count;
                        var hdays = day5.Where(i => i.DutyPost == "HOLIDAY").ToList();
                        holidays = holidays + hdays.Count;
                        var ddays = day5.Where(i => i.DutyPost == "DAY OFF").ToList();
                        dayoffs = dayoffs + ddays.Count;
                        var vdays = day5.Where(i => i.DutyPost == "VACATION").ToList();
                        vacations = vacations + vdays.Count;

                        day5 = day5.Where(i => i.DutyPost != "DAY OFF" && i.DutyPost != "HOLIDAY" && i.DutyPost != "SICK DAY" && i.DutyPost != "VACATION").ToList();

                        var comments = new Phrase(1, "\u00a0");



                        if (sdays.Count > 0)
                        {
                            comments = new Phrase("Sick Day", tentimes);
                        }
                        if (hdays.Count > 0)
                        {
                            comments = new Phrase("Holiday", tentimes);
                        }
                        if (ddays.Count > 0)
                        {
                            comments = new Phrase("Day Off", tentimes);
                        }
                        if (vdays.Count > 0)
                        {
                            comments = new Phrase("Vacation", tentimes);
                        }
                        var xheaderCell6 = new PdfPCell(comments);

                        var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.AddDays(4).ToShortDateString(), tentimes));

                        var xheaderCell2 = new PdfPCell(new Phrase(day5.Count.ToString(), tentimes));

                        var wh = day5.Where(i => i.ShiftOn == true).ToList();

                        var xheaderCell3 = new PdfPCell(new Phrase(wh.Count.ToString(), tentimes));

                        var xheaderCell4 = new PdfPCell(new Phrase((wh.Count - day5.Count).ToString(), rtentimes));

                        variancetotal = variancetotal + (wh.Count - day5.Count);

                        var ah = day5.Where(i => i.TaskOn == true && i.ShiftOn == true).ToList();

                        var xheaderCell5 = new PdfPCell(new Phrase(ah.Count.ToString(), tentimes));



                        var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                        tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                        tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dutyrosterview.Date.AddDays(4) && i.CreatedDate.Value.Date <= dutyrosterview.Date.AddDays(4).AddHours(23)).ToList();

                        var groupedtevents = tevents.GroupBy(item => item.TaskId);
                        tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                        var ttraveldurationlist = 0.0;
                        var dttocheck = dutyrosterview.Date.AddDays(4);
                        foreach (var t in tevents)
                        {
                            var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                            if (task != null)
                            {
                                var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dttocheck);
                                ttraveldurationlist = tt + ttraveldurationlist;
                            }
                        }


                        var d = CommonUtility.GetUserOTHoursPerDay(dttocheck, dttocheck.AddHours(23), getots);
                        //var d = CommonUtility.GetUserOTHoursForEmployeeReport(employee.ID, dutyrosterview.Date.AddDays(4), dutyrosterview.Date.AddDays(4).AddHours(23));

                        var xheaderCelltt = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                        var xheaderCelld = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));

                        totalOT = totalOT + d;
                        totalTT = totalTT + ttraveldurationlist;

                        xheaderCell.Border = Rectangle.NO_BORDER;
                        xheaderCell2.Border = Rectangle.NO_BORDER;
                        xheaderCell3.Border = Rectangle.NO_BORDER;
                        xheaderCell4.Border = Rectangle.NO_BORDER;
                        xheaderCell5.Border = Rectangle.NO_BORDER;
                        xheaderCell6.Border = Rectangle.NO_BORDER;


                        xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        newchecklistTable.AddCell(xheaderCell);
                        newchecklistTable.AddCell(xheaderCell2);
                        newchecklistTable.AddCell(xheaderCell3);
                        newchecklistTable.AddCell(xheaderCell4);

                        xheaderCelltt.Border = Rectangle.NO_BORDER;
                        xheaderCelld.Border = Rectangle.NO_BORDER;

                        xheaderCelltt.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCelld.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        newchecklistTable.AddCell(xheaderCelltt);
                        newchecklistTable.AddCell(xheaderCelld);

                        newchecklistTable.AddCell(xheaderCell5);
                        newchecklistTable.AddCell(xheaderCell6);
                    }
                    else
                    {
                        var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.AddDays(4).ToShortDateString(), tentimes));
                        var xheaderCell2 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell3 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell4 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell5 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell6 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                        tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                        var dttocheck = dutyrosterview.Date.AddDays(4);

                        tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dttocheck && i.CreatedDate.Value.Date <= dttocheck.AddHours(23)).ToList();

                        var groupedtevents = tevents.GroupBy(item => item.TaskId);
                        tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                        var ttraveldurationlist = 0.0;

                        foreach (var t in tevents)
                        {
                            var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                            if (task != null)
                            {
                                var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dttocheck);
                                ttraveldurationlist = tt + ttraveldurationlist;
                            }
                        }

                        var d = CommonUtility.GetUserOTHoursPerDay(dttocheck, dttocheck.AddHours(23), getots);

                        var xheaderCell7 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                        var xheaderCell8 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));

                        totalOT = totalOT + d;
                        totalTT = totalTT + ttraveldurationlist;

                        xheaderCell.Border = Rectangle.NO_BORDER;
                        xheaderCell2.Border = Rectangle.NO_BORDER;
                        xheaderCell3.Border = Rectangle.NO_BORDER;
                        xheaderCell4.Border = Rectangle.NO_BORDER;
                        xheaderCell5.Border = Rectangle.NO_BORDER;
                        xheaderCell6.Border = Rectangle.NO_BORDER;
                        xheaderCell7.Border = Rectangle.NO_BORDER;
                        xheaderCell8.Border = Rectangle.NO_BORDER;

                        xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell7.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell8.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        newchecklistTable.AddCell(xheaderCell);
                        newchecklistTable.AddCell(xheaderCell2);
                        newchecklistTable.AddCell(xheaderCell3);
                        newchecklistTable.AddCell(xheaderCell4);
                        newchecklistTable.AddCell(xheaderCell7);
                        newchecklistTable.AddCell(xheaderCell8);
                        newchecklistTable.AddCell(xheaderCell5);
                        newchecklistTable.AddCell(xheaderCell6);
                    }

                    if (day6.Count > 0)
                    {
                        var sdays = day6.Where(i => i.DutyPost == "SICK DAY").ToList();
                        sickdays = sickdays + sdays.Count;
                        var hdays = day6.Where(i => i.DutyPost == "HOLIDAY").ToList();
                        holidays = holidays + hdays.Count;
                        var ddays = day6.Where(i => i.DutyPost == "DAY OFF").ToList();
                        dayoffs = dayoffs + ddays.Count;
                        var vdays = day6.Where(i => i.DutyPost == "VACATION").ToList();
                        vacations = vacations + vdays.Count;

                        day6 = day6.Where(i => i.DutyPost != "DAY OFF" && i.DutyPost != "HOLIDAY" && i.DutyPost != "SICK DAY" && i.DutyPost != "VACATION").ToList();

                        var comments = new Phrase(1, "\u00a0");



                        if (sdays.Count > 0)
                        {
                            comments = new Phrase("Sick Day", tentimes);
                        }
                        if (hdays.Count > 0)
                        {
                            comments = new Phrase("Holiday", tentimes);
                        }
                        if (ddays.Count > 0)
                        {
                            comments = new Phrase("Day Off", tentimes);
                        }
                        if (vdays.Count > 0)
                        {
                            comments = new Phrase("Vacation", tentimes);
                        }
                        var xheaderCell6 = new PdfPCell(comments);

                        var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.AddDays(5).ToShortDateString(), tentimes));

                        var xheaderCell2 = new PdfPCell(new Phrase(day6.Count.ToString(), tentimes));

                        var wh = day6.Where(i => i.ShiftOn == true).ToList();

                        var xheaderCell3 = new PdfPCell(new Phrase(wh.Count.ToString(), tentimes));

                        var xheaderCell4 = new PdfPCell(new Phrase((wh.Count - day6.Count).ToString(), rtentimes));

                        variancetotal = variancetotal + (wh.Count - day6.Count);

                        var ah = day6.Where(i => i.TaskOn == true && i.ShiftOn == true).ToList();

                        var xheaderCell5 = new PdfPCell(new Phrase(ah.Count.ToString(), tentimes));


                        var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                        tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                        tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dutyrosterview.Date.AddDays(5) && i.CreatedDate.Value.Date <= dutyrosterview.Date.AddDays(5).AddHours(23)).ToList();

                        var groupedtevents = tevents.GroupBy(item => item.TaskId);
                        tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                        var ttraveldurationlist = 0.0;
                        var dttocheck = dutyrosterview.Date.AddDays(5);
                        foreach (var t in tevents)
                        {
                            var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                            if (task != null)
                            {
                                var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dttocheck);
                                ttraveldurationlist = tt + ttraveldurationlist;
                            }
                        }


                        var d = CommonUtility.GetUserOTHoursPerDay(dttocheck, dttocheck.AddHours(23), getots);
                        var xheaderCelltt = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                        var xheaderCelld = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));

                        totalOT = totalOT + d;
                        totalTT = totalTT + ttraveldurationlist;

                        xheaderCell.Border = Rectangle.NO_BORDER;
                        xheaderCell2.Border = Rectangle.NO_BORDER;
                        xheaderCell3.Border = Rectangle.NO_BORDER;
                        xheaderCell4.Border = Rectangle.NO_BORDER;
                        xheaderCell5.Border = Rectangle.NO_BORDER;
                        xheaderCell6.Border = Rectangle.NO_BORDER;

                        xheaderCell.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell2.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell3.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell4.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell5.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell6.BackgroundColor = Color.LIGHT_GRAY;


                        xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        newchecklistTable.AddCell(xheaderCell);
                        newchecklistTable.AddCell(xheaderCell2);
                        newchecklistTable.AddCell(xheaderCell3);
                        newchecklistTable.AddCell(xheaderCell4);

                        xheaderCelltt.Border = Rectangle.NO_BORDER;
                        xheaderCelld.Border = Rectangle.NO_BORDER;

                        xheaderCelltt.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCelld.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        xheaderCelltt.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCelld.BackgroundColor = Color.LIGHT_GRAY;

                        newchecklistTable.AddCell(xheaderCelltt);
                        newchecklistTable.AddCell(xheaderCelld);

                        newchecklistTable.AddCell(xheaderCell5);
                        newchecklistTable.AddCell(xheaderCell6);
                    }
                    else
                    {
                        var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.AddDays(5).ToShortDateString(), tentimes));
                        var xheaderCell2 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell3 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell4 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell5 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell6 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                        tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                        var dttocheck = dutyrosterview.Date.AddDays(5);

                        tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dttocheck && i.CreatedDate.Value.Date <= dttocheck.AddHours(23)).ToList();

                        var groupedtevents = tevents.GroupBy(item => item.TaskId);
                        tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                        var ttraveldurationlist = 0.0;

                        foreach (var t in tevents)
                        {
                            var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                            if (task != null)
                            {
                                var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dttocheck);
                                ttraveldurationlist = tt + ttraveldurationlist;
                            }
                        }

                        var d = CommonUtility.GetUserOTHoursPerDay(dttocheck, dttocheck.AddHours(23), getots);

                        var xheaderCell7 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                        var xheaderCell8 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));

                        totalOT = totalOT + d;
                        totalTT = totalTT + ttraveldurationlist;

                        xheaderCell.Border = Rectangle.NO_BORDER;
                        xheaderCell2.Border = Rectangle.NO_BORDER;
                        xheaderCell3.Border = Rectangle.NO_BORDER;
                        xheaderCell4.Border = Rectangle.NO_BORDER;
                        xheaderCell5.Border = Rectangle.NO_BORDER;
                        xheaderCell6.Border = Rectangle.NO_BORDER;
                        xheaderCell7.Border = Rectangle.NO_BORDER;
                        xheaderCell8.Border = Rectangle.NO_BORDER;

                        xheaderCell.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell2.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell3.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell4.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell5.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell6.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell7.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell8.BackgroundColor = Color.LIGHT_GRAY;

                        xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell7.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell8.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        newchecklistTable.AddCell(xheaderCell);
                        newchecklistTable.AddCell(xheaderCell2);
                        newchecklistTable.AddCell(xheaderCell3);
                        newchecklistTable.AddCell(xheaderCell4);
                        newchecklistTable.AddCell(xheaderCell7);
                        newchecklistTable.AddCell(xheaderCell8);
                        newchecklistTable.AddCell(xheaderCell5);
                        newchecklistTable.AddCell(xheaderCell6);
                    }

                    if (day7.Count > 0)
                    {
                        var sdays = day7.Where(i => i.DutyPost == "SICK DAY").ToList();
                        sickdays = sickdays + sdays.Count;
                        var hdays = day7.Where(i => i.DutyPost == "HOLIDAY").ToList();
                        holidays = holidays + hdays.Count;
                        var ddays = day7.Where(i => i.DutyPost == "DAY OFF").ToList();
                        dayoffs = dayoffs + ddays.Count;
                        var vdays = day7.Where(i => i.DutyPost == "VACATION").ToList();
                        vacations = vacations + vdays.Count;

                        day7 = day7.Where(i => i.DutyPost != "DAY OFF" && i.DutyPost != "HOLIDAY" && i.DutyPost != "SICK DAY" && i.DutyPost != "VACATION").ToList();

                        var comments = new Phrase(1, "\u00a0");



                        if (sdays.Count > 0)
                        {
                            comments = new Phrase("Sick Day", tentimes);
                        }
                        if (hdays.Count > 0)
                        {
                            comments = new Phrase("Holiday", tentimes);
                        }
                        if (ddays.Count > 0)
                        {
                            comments = new Phrase("Day Off", tentimes);
                        }
                        if (vdays.Count > 0)
                        {
                            comments = new Phrase("Vacation", tentimes);
                        }
                        var xheaderCell6 = new PdfPCell(comments);

                        var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.AddDays(6).ToShortDateString(), tentimes));

                        var xheaderCell2 = new PdfPCell(new Phrase(day7.Count.ToString(), tentimes));

                        var wh = day7.Where(i => i.ShiftOn == true).ToList();

                        var xheaderCell3 = new PdfPCell(new Phrase(wh.Count.ToString(), tentimes));

                        var xheaderCell4 = new PdfPCell(new Phrase((wh.Count - day7.Count).ToString(), rtentimes));

                        variancetotal = variancetotal + (wh.Count - day7.Count);

                        var ah = day7.Where(i => i.TaskOn == true && i.ShiftOn == true).ToList();

                        var xheaderCell5 = new PdfPCell(new Phrase(ah.Count.ToString(), tentimes));


                        var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                        tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                        tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dutyrosterview.Date.AddDays(6) && i.CreatedDate.Value.Date <= dutyrosterview.Date.AddDays(6).AddHours(23)).ToList();

                        var groupedtevents = tevents.GroupBy(item => item.TaskId);
                        tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                        var ttraveldurationlist = 0.0;
                        var dttocheck = dutyrosterview.Date.AddDays(6);
                        foreach (var t in tevents)
                        {
                            var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                            if (task != null)
                            {
                                var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dttocheck);
                                ttraveldurationlist = tt + ttraveldurationlist;
                            }
                        }


                        var d = CommonUtility.GetUserOTHoursPerDay(dttocheck, dttocheck.AddHours(23), getots);
                        var xheaderCelltt = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                        var xheaderCelld = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));

                        totalOT = totalOT + d;
                        totalTT = totalTT + ttraveldurationlist;

                        xheaderCell.Border = Rectangle.NO_BORDER;
                        xheaderCell2.Border = Rectangle.NO_BORDER;
                        xheaderCell3.Border = Rectangle.NO_BORDER;
                        xheaderCell4.Border = Rectangle.NO_BORDER;
                        xheaderCell5.Border = Rectangle.NO_BORDER;
                        xheaderCell6.Border = Rectangle.NO_BORDER;


                        xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        newchecklistTable.AddCell(xheaderCell);
                        newchecklistTable.AddCell(xheaderCell2);
                        newchecklistTable.AddCell(xheaderCell3);
                        newchecklistTable.AddCell(xheaderCell4);

                        xheaderCelltt.Border = Rectangle.NO_BORDER;
                        xheaderCelld.Border = Rectangle.NO_BORDER;

                        xheaderCelltt.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCelld.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        newchecklistTable.AddCell(xheaderCelltt);
                        newchecklistTable.AddCell(xheaderCelld);

                        newchecklistTable.AddCell(xheaderCell5);
                        newchecklistTable.AddCell(xheaderCell6);
                    }
                    else
                    {
                        var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.AddDays(6).ToShortDateString(), tentimes));
                        var xheaderCell2 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell3 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell4 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell5 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell6 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                        tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                        var dttocheck = dutyrosterview.Date.AddDays(6);

                        tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dttocheck && i.CreatedDate.Value.Date <= dttocheck.AddHours(23)).ToList();

                        var groupedtevents = tevents.GroupBy(item => item.TaskId);
                        tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                        var ttraveldurationlist = 0.0;

                        foreach (var t in tevents)
                        {
                            var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                            if (task != null)
                            {
                                var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dttocheck);
                                ttraveldurationlist = tt + ttraveldurationlist;
                            }
                        }
                        var d = CommonUtility.GetUserOTHoursPerDay(dttocheck, dttocheck.AddHours(23), getots);

                        var xheaderCell7 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                        var xheaderCell8 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));

                        totalOT = totalOT + d;
                        totalTT = totalTT + ttraveldurationlist;

                        xheaderCell.Border = Rectangle.NO_BORDER;
                        xheaderCell2.Border = Rectangle.NO_BORDER;
                        xheaderCell3.Border = Rectangle.NO_BORDER;
                        xheaderCell4.Border = Rectangle.NO_BORDER;
                        xheaderCell5.Border = Rectangle.NO_BORDER;
                        xheaderCell6.Border = Rectangle.NO_BORDER;
                        xheaderCell7.Border = Rectangle.NO_BORDER;
                        xheaderCell8.Border = Rectangle.NO_BORDER;

                        xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell7.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell8.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        newchecklistTable.AddCell(xheaderCell);
                        newchecklistTable.AddCell(xheaderCell2);
                        newchecklistTable.AddCell(xheaderCell3);
                        newchecklistTable.AddCell(xheaderCell4);
                        newchecklistTable.AddCell(xheaderCell7);
                        newchecklistTable.AddCell(xheaderCell8);
                        newchecklistTable.AddCell(xheaderCell5);
                        newchecklistTable.AddCell(xheaderCell6);
                    }

                    if (day8.Count > 0)
                    {

                        var sdays = day8.Where(i => i.DutyPost == "SICK DAY").ToList();
                        sickdays = sickdays + sdays.Count;
                        var hdays = day8.Where(i => i.DutyPost == "HOLIDAY").ToList();
                        holidays = holidays + hdays.Count;
                        var ddays = day8.Where(i => i.DutyPost == "DAY OFF").ToList();
                        dayoffs = dayoffs + ddays.Count;
                        var vdays = day8.Where(i => i.DutyPost == "VACATION").ToList();
                        vacations = vacations + vdays.Count;

                        day8 = day8.Where(i => i.DutyPost != "DAY OFF" && i.DutyPost != "HOLIDAY" && i.DutyPost != "SICK DAY" && i.DutyPost != "VACATION").ToList();

                        var comments = new Phrase(1, "\u00a0");


                        if (sdays.Count > 0)
                        {
                            comments = new Phrase("Sick Day", tentimes);
                        }
                        if (hdays.Count > 0)
                        {
                            comments = new Phrase("Holiday", tentimes);
                        }
                        if (ddays.Count > 0)
                        {
                            comments = new Phrase("Day Off", tentimes);
                        }
                        if (vdays.Count > 0)
                        {
                            comments = new Phrase("Vacation", tentimes);
                        }
                        var xheaderCell6 = new PdfPCell(comments);

                        var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.AddDays(7).ToShortDateString(), tentimes));

                        var xheaderCell2 = new PdfPCell(new Phrase(day8.Count.ToString(), tentimes));

                        var wh = day8.Where(i => i.ShiftOn == true).ToList();

                        var xheaderCell3 = new PdfPCell(new Phrase(wh.Count.ToString(), tentimes));

                        var xheaderCell4 = new PdfPCell(new Phrase((wh.Count - day8.Count).ToString(), rtentimes));

                        variancetotal = variancetotal + (wh.Count - day8.Count);

                        var ah = day8.Where(i => i.TaskOn == true && i.ShiftOn == true).ToList();

                        var xheaderCell5 = new PdfPCell(new Phrase(ah.Count.ToString(), tentimes));


                        var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                        tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                        tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dutyrosterview.Date.AddDays(7) && i.CreatedDate.Value.Date <= dutyrosterview.Date.AddDays(7).AddHours(23)).ToList();

                        var groupedtevents = tevents.GroupBy(item => item.TaskId);
                        tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                        var ttraveldurationlist = 0.0;
                        var dttocheck = dutyrosterview.Date.AddDays(7);
                        foreach (var t in tevents)
                        {
                            var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                            if (task != null)
                            {
                                var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dttocheck);
                                ttraveldurationlist = tt + ttraveldurationlist;
                            }
                        }


                        var d = CommonUtility.GetUserOTHoursPerDay(dttocheck, dttocheck.AddHours(23), getots);
                        var xheaderCelltt = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                        var xheaderCelld = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));

                        totalOT = totalOT + d;
                        totalTT = totalTT + ttraveldurationlist;

                        xheaderCell.Border = Rectangle.NO_BORDER;
                        xheaderCell2.Border = Rectangle.NO_BORDER;
                        xheaderCell3.Border = Rectangle.NO_BORDER;
                        xheaderCell4.Border = Rectangle.NO_BORDER;
                        xheaderCell5.Border = Rectangle.NO_BORDER;
                        xheaderCell6.Border = Rectangle.NO_BORDER;

                        xheaderCell.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell2.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell3.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell4.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell5.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell6.BackgroundColor = Color.LIGHT_GRAY;


                        xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        newchecklistTable.AddCell(xheaderCell);
                        newchecklistTable.AddCell(xheaderCell2);
                        newchecklistTable.AddCell(xheaderCell3);
                        newchecklistTable.AddCell(xheaderCell4);

                        xheaderCelltt.Border = Rectangle.NO_BORDER;
                        xheaderCelld.Border = Rectangle.NO_BORDER;

                        xheaderCelltt.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCelld.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        xheaderCelltt.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCelld.BackgroundColor = Color.LIGHT_GRAY;

                        newchecklistTable.AddCell(xheaderCelltt);
                        newchecklistTable.AddCell(xheaderCelld);

                        newchecklistTable.AddCell(xheaderCell5);
                        newchecklistTable.AddCell(xheaderCell6);
                    }
                    else
                    {
                        var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.AddDays(7).ToShortDateString(), tentimes));
                        var xheaderCell2 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell3 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell4 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell5 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell6 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                        tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                        var dttocheck = dutyrosterview.Date.AddDays(7);

                        tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dttocheck && i.CreatedDate.Value.Date <= dttocheck.AddHours(23)).ToList();

                        var groupedtevents = tevents.GroupBy(item => item.TaskId);
                        tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                        var ttraveldurationlist = 0.0;

                        foreach (var t in tevents)
                        {
                            var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                            if (task != null)
                            {
                                var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dttocheck);
                                ttraveldurationlist = tt + ttraveldurationlist;
                            }
                        }

                        var d = CommonUtility.GetUserOTHoursPerDay(dttocheck, dttocheck.AddHours(23), getots);

                        var xheaderCell7 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                        var xheaderCell8 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));

                        totalOT = totalOT + d;
                        totalTT = totalTT + ttraveldurationlist;

                        xheaderCell.Border = Rectangle.NO_BORDER;
                        xheaderCell2.Border = Rectangle.NO_BORDER;
                        xheaderCell3.Border = Rectangle.NO_BORDER;
                        xheaderCell4.Border = Rectangle.NO_BORDER;
                        xheaderCell5.Border = Rectangle.NO_BORDER;
                        xheaderCell6.Border = Rectangle.NO_BORDER;
                        xheaderCell7.Border = Rectangle.NO_BORDER;
                        xheaderCell8.Border = Rectangle.NO_BORDER;

                        xheaderCell.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell2.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell3.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell4.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell5.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell6.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell7.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell8.BackgroundColor = Color.LIGHT_GRAY;

                        xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell7.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell8.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        newchecklistTable.AddCell(xheaderCell);
                        newchecklistTable.AddCell(xheaderCell2);
                        newchecklistTable.AddCell(xheaderCell3);
                        newchecklistTable.AddCell(xheaderCell4);
                        newchecklistTable.AddCell(xheaderCell7);
                        newchecklistTable.AddCell(xheaderCell8);
                        newchecklistTable.AddCell(xheaderCell5);
                        newchecklistTable.AddCell(xheaderCell6);
                    }

                    if (day9.Count > 0)
                    {

                        var sdays = day9.Where(i => i.DutyPost == "SICK DAY").ToList();
                        sickdays = sickdays + sdays.Count;
                        var hdays = day9.Where(i => i.DutyPost == "HOLIDAY").ToList();
                        holidays = holidays + hdays.Count;
                        var ddays = day9.Where(i => i.DutyPost == "DAY OFF").ToList();
                        dayoffs = dayoffs + ddays.Count;
                        var vdays = day9.Where(i => i.DutyPost == "VACATION").ToList();
                        vacations = vacations + vdays.Count;

                        day9 = day9.Where(i => i.DutyPost != "DAY OFF" && i.DutyPost != "HOLIDAY" && i.DutyPost != "SICK DAY" && i.DutyPost != "VACATION").ToList();

                        var comments = new Phrase(1, "\u00a0");


                        if (sdays.Count > 0)
                        {
                            comments = new Phrase("Sick Day", tentimes);
                        }
                        if (hdays.Count > 0)
                        {
                            comments = new Phrase("Holiday", tentimes);
                        }
                        if (ddays.Count > 0)
                        {
                            comments = new Phrase("Day Off", tentimes);
                        }
                        if (vdays.Count > 0)
                        {
                            comments = new Phrase("Vacation", tentimes);
                        }
                        var xheaderCell6 = new PdfPCell(comments);

                        var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.AddDays(8).ToShortDateString(), tentimes));

                        var xheaderCell2 = new PdfPCell(new Phrase(day9.Count.ToString(), tentimes));

                        var wh = day9.Where(i => i.ShiftOn == true).ToList();

                        var xheaderCell3 = new PdfPCell(new Phrase(wh.Count.ToString(), tentimes));
                        var xheaderCell4 = new PdfPCell(new Phrase((wh.Count - day9.Count).ToString(), rtentimes));

                        variancetotal = variancetotal + (wh.Count - day9.Count);

                        var ah = day9.Where(i => i.TaskOn == true && i.ShiftOn == true).ToList();

                        var xheaderCell5 = new PdfPCell(new Phrase(ah.Count.ToString(), tentimes));


                        var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                        tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                        tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dutyrosterview.Date.AddDays(8) && i.CreatedDate.Value.Date <= dutyrosterview.Date.AddDays(8).AddHours(23)).ToList();

                        var groupedtevents = tevents.GroupBy(item => item.TaskId);
                        tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                        var ttraveldurationlist = 0.0;
                        var dttocheck = dutyrosterview.Date.AddDays(8);
                        foreach (var t in tevents)
                        {
                            var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                            if (task != null)
                            {
                                var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dttocheck);
                                ttraveldurationlist = tt + ttraveldurationlist;
                            }
                        }



                        var d = CommonUtility.GetUserOTHoursPerDay(dttocheck, dttocheck.AddHours(23), getots);

                        var xheaderCelltt = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                        var xheaderCelld = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));

                        totalOT = totalOT + d;
                        totalTT = totalTT + ttraveldurationlist;

                        xheaderCell.Border = Rectangle.NO_BORDER;
                        xheaderCell2.Border = Rectangle.NO_BORDER;
                        xheaderCell3.Border = Rectangle.NO_BORDER;
                        xheaderCell4.Border = Rectangle.NO_BORDER;
                        xheaderCell5.Border = Rectangle.NO_BORDER;
                        xheaderCell6.Border = Rectangle.NO_BORDER;


                        xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        newchecklistTable.AddCell(xheaderCell);
                        newchecklistTable.AddCell(xheaderCell2);
                        newchecklistTable.AddCell(xheaderCell3);
                        newchecklistTable.AddCell(xheaderCell4);

                        xheaderCelltt.Border = Rectangle.NO_BORDER;
                        xheaderCelld.Border = Rectangle.NO_BORDER;

                        xheaderCelltt.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCelld.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        newchecklistTable.AddCell(xheaderCelltt);
                        newchecklistTable.AddCell(xheaderCelld);

                        newchecklistTable.AddCell(xheaderCell5);
                        newchecklistTable.AddCell(xheaderCell6);
                    }
                    else
                    {
                        var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.AddDays(8).ToShortDateString(), tentimes));
                        var xheaderCell2 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell3 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell4 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell5 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell6 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                        tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                        var dttocheck = dutyrosterview.Date.AddDays(8);

                        tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dttocheck && i.CreatedDate.Value.Date <= dttocheck.AddHours(23)).ToList();

                        var groupedtevents = tevents.GroupBy(item => item.TaskId);
                        tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                        var ttraveldurationlist = 0.0;

                        foreach (var t in tevents)
                        {
                            var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                            if (task != null)
                            {
                                var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dttocheck);
                                ttraveldurationlist = tt + ttraveldurationlist;
                            }
                        }

                        var d = CommonUtility.GetUserOTHoursPerDay(dttocheck, dttocheck.AddHours(23), getots);

                        var xheaderCell7 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                        var xheaderCell8 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));

                        totalOT = totalOT + d;
                        totalTT = totalTT + ttraveldurationlist;

                        xheaderCell.Border = Rectangle.NO_BORDER;
                        xheaderCell2.Border = Rectangle.NO_BORDER;
                        xheaderCell3.Border = Rectangle.NO_BORDER;
                        xheaderCell4.Border = Rectangle.NO_BORDER;
                        xheaderCell5.Border = Rectangle.NO_BORDER;
                        xheaderCell6.Border = Rectangle.NO_BORDER;
                        xheaderCell7.Border = Rectangle.NO_BORDER;
                        xheaderCell8.Border = Rectangle.NO_BORDER;

                        xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell7.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell8.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        newchecklistTable.AddCell(xheaderCell);
                        newchecklistTable.AddCell(xheaderCell2);
                        newchecklistTable.AddCell(xheaderCell3);
                        newchecklistTable.AddCell(xheaderCell4);
                        newchecklistTable.AddCell(xheaderCell7);
                        newchecklistTable.AddCell(xheaderCell8);
                        newchecklistTable.AddCell(xheaderCell5);
                        newchecklistTable.AddCell(xheaderCell6);
                    }

                    if (day10.Count > 0)
                    {

                        var sdays = day10.Where(i => i.DutyPost == "SICK DAY").ToList();
                        sickdays = sickdays + sdays.Count;
                        var hdays = day10.Where(i => i.DutyPost == "HOLIDAY").ToList();
                        holidays = holidays + hdays.Count;
                        var ddays = day10.Where(i => i.DutyPost == "DAY OFF").ToList();
                        dayoffs = dayoffs + ddays.Count;
                        var vdays = day10.Where(i => i.DutyPost == "VACATION").ToList();
                        vacations = vacations + vdays.Count;

                        day10 = day10.Where(i => i.DutyPost != "DAY OFF" && i.DutyPost != "HOLIDAY" && i.DutyPost != "SICK DAY" && i.DutyPost != "VACATION").ToList();

                        var comments = new Phrase(1, "\u00a0");


                        if (sdays.Count > 0)
                        {
                            comments = new Phrase("Sick Day", tentimes);
                        }
                        if (hdays.Count > 0)
                        {
                            comments = new Phrase("Holiday", tentimes);
                        }
                        if (ddays.Count > 0)
                        {
                            comments = new Phrase("Day Off", tentimes);
                        }
                        if (vdays.Count > 0)
                        {
                            comments = new Phrase("Vacation", tentimes);
                        }
                        var xheaderCell6 = new PdfPCell(comments);

                        var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.AddDays(9).ToShortDateString(), tentimes));

                        var xheaderCell2 = new PdfPCell(new Phrase(day10.Count.ToString(), tentimes));

                        var wh = day10.Where(i => i.ShiftOn == true).ToList();

                        var xheaderCell3 = new PdfPCell(new Phrase(wh.Count.ToString(), tentimes));

                        var xheaderCell4 = new PdfPCell(new Phrase((wh.Count - day10.Count).ToString(), rtentimes));

                        variancetotal = variancetotal + (wh.Count - day10.Count);

                        var ah = day10.Where(i => i.TaskOn == true && i.ShiftOn == true).ToList();

                        var xheaderCell5 = new PdfPCell(new Phrase(ah.Count.ToString(), tentimes));


                        var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                        tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                        tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dutyrosterview.Date.AddDays(9) && i.CreatedDate.Value.Date <= dutyrosterview.Date.AddDays(9).AddHours(23)).ToList();

                        var groupedtevents = tevents.GroupBy(item => item.TaskId);
                        tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                        var ttraveldurationlist = 0.0;
                        var dttocheck = dutyrosterview.Date.AddDays(9);

                        foreach (var t in tevents)
                        {
                            var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                            if (task != null)
                            {
                                var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dttocheck);
                                ttraveldurationlist = tt + ttraveldurationlist;
                            }
                        }


                        var d = CommonUtility.GetUserOTHoursPerDay(dttocheck, dttocheck.AddHours(23), getots);

                        var xheaderCelltt = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                        var xheaderCelld = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));

                        totalOT = totalOT + d;
                        totalTT = totalTT + ttraveldurationlist;

                        xheaderCell.Border = Rectangle.NO_BORDER;
                        xheaderCell2.Border = Rectangle.NO_BORDER;
                        xheaderCell3.Border = Rectangle.NO_BORDER;
                        xheaderCell4.Border = Rectangle.NO_BORDER;
                        xheaderCell5.Border = Rectangle.NO_BORDER;
                        xheaderCell6.Border = Rectangle.NO_BORDER;

                        xheaderCell.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell2.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell3.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell4.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell5.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell6.BackgroundColor = Color.LIGHT_GRAY;


                        xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        newchecklistTable.AddCell(xheaderCell);
                        newchecklistTable.AddCell(xheaderCell2);
                        newchecklistTable.AddCell(xheaderCell3);
                        newchecklistTable.AddCell(xheaderCell4);

                        xheaderCelltt.Border = Rectangle.NO_BORDER;
                        xheaderCelld.Border = Rectangle.NO_BORDER;

                        xheaderCelltt.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCelld.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        xheaderCelltt.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCelld.BackgroundColor = Color.LIGHT_GRAY;

                        newchecklistTable.AddCell(xheaderCelltt);
                        newchecklistTable.AddCell(xheaderCelld);

                        newchecklistTable.AddCell(xheaderCell5);
                        newchecklistTable.AddCell(xheaderCell6);
                    }
                    else
                    {
                        var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.AddDays(9).ToShortDateString(), tentimes));
                        var xheaderCell2 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell3 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell4 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell5 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell6 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                        tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                        var dttocheck = dutyrosterview.Date.AddDays(9);

                        tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dttocheck && i.CreatedDate.Value.Date <= dttocheck.AddHours(23)).ToList();

                        var groupedtevents = tevents.GroupBy(item => item.TaskId);
                        tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                        var ttraveldurationlist = 0.0;

                        foreach (var t in tevents)
                        {
                            var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                            if (task != null)
                            {
                                var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dttocheck);
                                ttraveldurationlist = tt + ttraveldurationlist;
                            }
                        }

                        var d = CommonUtility.GetUserOTHoursPerDay(dttocheck, dttocheck.AddHours(23), getots);

                        var xheaderCell7 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                        var xheaderCell8 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));

                        totalOT = totalOT + d;
                        totalTT = totalTT + ttraveldurationlist;

                        xheaderCell.Border = Rectangle.NO_BORDER;
                        xheaderCell2.Border = Rectangle.NO_BORDER;
                        xheaderCell3.Border = Rectangle.NO_BORDER;
                        xheaderCell4.Border = Rectangle.NO_BORDER;
                        xheaderCell5.Border = Rectangle.NO_BORDER;
                        xheaderCell6.Border = Rectangle.NO_BORDER;
                        xheaderCell7.Border = Rectangle.NO_BORDER;
                        xheaderCell8.Border = Rectangle.NO_BORDER;

                        xheaderCell.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell2.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell3.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell4.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell5.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell6.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell7.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell8.BackgroundColor = Color.LIGHT_GRAY;

                        xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell7.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell8.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        newchecklistTable.AddCell(xheaderCell);
                        newchecklistTable.AddCell(xheaderCell2);
                        newchecklistTable.AddCell(xheaderCell3);
                        newchecklistTable.AddCell(xheaderCell4);
                        newchecklistTable.AddCell(xheaderCell7);
                        newchecklistTable.AddCell(xheaderCell8);
                        newchecklistTable.AddCell(xheaderCell5);
                        newchecklistTable.AddCell(xheaderCell6);
                    }

                    if (day11.Count > 0)
                    {

                        var sdays = day11.Where(i => i.DutyPost == "SICK DAY").ToList();
                        sickdays = sickdays + sdays.Count;
                        var hdays = day11.Where(i => i.DutyPost == "HOLIDAY").ToList();
                        holidays = holidays + hdays.Count;
                        var ddays = day11.Where(i => i.DutyPost == "DAY OFF").ToList();
                        dayoffs = dayoffs + ddays.Count;
                        var vdays = day11.Where(i => i.DutyPost == "VACATION").ToList();
                        vacations = vacations + vdays.Count;

                        day11 = day11.Where(i => i.DutyPost != "DAY OFF" && i.DutyPost != "HOLIDAY" && i.DutyPost != "SICK DAY" && i.DutyPost != "VACATION").ToList();

                        var comments = new Phrase(1, "\u00a0");


                        if (sdays.Count > 0)
                        {
                            comments = new Phrase("Sick Day", tentimes);
                        }
                        if (hdays.Count > 0)
                        {
                            comments = new Phrase("Holiday", tentimes);
                        }
                        if (ddays.Count > 0)
                        {
                            comments = new Phrase("Day Off", tentimes);
                        }
                        if (vdays.Count > 0)
                        {
                            comments = new Phrase("Vacation", tentimes);
                        }
                        var xheaderCell6 = new PdfPCell(comments);

                        var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.AddDays(10).ToShortDateString(), tentimes));

                        var xheaderCell2 = new PdfPCell(new Phrase(day11.Count.ToString(), tentimes));

                        var wh = day11.Where(i => i.ShiftOn == true).ToList();

                        var xheaderCell3 = new PdfPCell(new Phrase(wh.Count.ToString(), tentimes));

                        var xheaderCell4 = new PdfPCell(new Phrase((wh.Count - day11.Count).ToString(), rtentimes));

                        variancetotal = variancetotal + (wh.Count - day11.Count);

                        var ah = day11.Where(i => i.TaskOn == true && i.ShiftOn == true).ToList();

                        var xheaderCell5 = new PdfPCell(new Phrase(ah.Count.ToString(), tentimes));


                        var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                        tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                        tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dutyrosterview.Date.AddDays(10) && i.CreatedDate.Value.Date <= dutyrosterview.Date.AddDays(10).AddHours(23)).ToList();

                        var groupedtevents = tevents.GroupBy(item => item.TaskId);
                        tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                        var ttraveldurationlist = 0.0;
                        var dttocheck = dutyrosterview.Date.AddDays(10);
                        foreach (var t in tevents)
                        {
                            var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                            if (task != null)
                            {
                                var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dttocheck);
                                ttraveldurationlist = tt + ttraveldurationlist;
                            }
                        }


                        var d = CommonUtility.GetUserOTHoursPerDay(dttocheck, dttocheck.AddHours(23), getots);
                        var xheaderCelltt = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                        var xheaderCelld = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));

                        totalOT = totalOT + d;
                        totalTT = totalTT + ttraveldurationlist;

                        xheaderCell.Border = Rectangle.NO_BORDER;
                        xheaderCell2.Border = Rectangle.NO_BORDER;
                        xheaderCell3.Border = Rectangle.NO_BORDER;
                        xheaderCell4.Border = Rectangle.NO_BORDER;
                        xheaderCell5.Border = Rectangle.NO_BORDER;
                        xheaderCell6.Border = Rectangle.NO_BORDER;


                        xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        newchecklistTable.AddCell(xheaderCell);
                        newchecklistTable.AddCell(xheaderCell2);
                        newchecklistTable.AddCell(xheaderCell3);
                        newchecklistTable.AddCell(xheaderCell4);

                        xheaderCelltt.Border = Rectangle.NO_BORDER;
                        xheaderCelld.Border = Rectangle.NO_BORDER;

                        xheaderCelltt.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCelld.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        newchecklistTable.AddCell(xheaderCelltt);
                        newchecklistTable.AddCell(xheaderCelld);

                        newchecklistTable.AddCell(xheaderCell5);
                        newchecklistTable.AddCell(xheaderCell6);
                    }
                    else
                    {
                        var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.AddDays(10).ToShortDateString(), tentimes));
                        var xheaderCell2 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell3 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell4 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell5 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell6 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                        tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                        var dttocheck = dutyrosterview.Date.AddDays(10);

                        tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dttocheck && i.CreatedDate.Value.Date <= dttocheck.AddHours(23)).ToList();

                        var groupedtevents = tevents.GroupBy(item => item.TaskId);
                        tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                        var ttraveldurationlist = 0.0;

                        foreach (var t in tevents)
                        {
                            var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                            if (task != null)
                            {
                                var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dttocheck);
                                ttraveldurationlist = tt + ttraveldurationlist;
                            }
                        }

                        var d = CommonUtility.GetUserOTHoursPerDay(dttocheck, dttocheck.AddHours(23), getots);

                        var xheaderCell7 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                        var xheaderCell8 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));

                        totalOT = totalOT + d;
                        totalTT = totalTT + ttraveldurationlist;

                        xheaderCell.Border = Rectangle.NO_BORDER;
                        xheaderCell2.Border = Rectangle.NO_BORDER;
                        xheaderCell3.Border = Rectangle.NO_BORDER;
                        xheaderCell4.Border = Rectangle.NO_BORDER;
                        xheaderCell5.Border = Rectangle.NO_BORDER;
                        xheaderCell6.Border = Rectangle.NO_BORDER;
                        xheaderCell7.Border = Rectangle.NO_BORDER;
                        xheaderCell8.Border = Rectangle.NO_BORDER;

                        xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell7.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell8.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        newchecklistTable.AddCell(xheaderCell);
                        newchecklistTable.AddCell(xheaderCell2);
                        newchecklistTable.AddCell(xheaderCell3);
                        newchecklistTable.AddCell(xheaderCell4);
                        newchecklistTable.AddCell(xheaderCell7);
                        newchecklistTable.AddCell(xheaderCell8);
                        newchecklistTable.AddCell(xheaderCell5);
                        newchecklistTable.AddCell(xheaderCell6);
                    }

                    if (day12.Count > 0)
                    {

                        var sdays = day12.Where(i => i.DutyPost == "SICK DAY").ToList();
                        sickdays = sickdays + sdays.Count;
                        var hdays = day12.Where(i => i.DutyPost == "HOLIDAY").ToList();
                        holidays = holidays + hdays.Count;
                        var ddays = day12.Where(i => i.DutyPost == "DAY OFF").ToList();
                        dayoffs = dayoffs + ddays.Count;
                        var vdays = day12.Where(i => i.DutyPost == "VACATION").ToList();
                        vacations = vacations + vdays.Count;

                        day12 = day12.Where(i => i.DutyPost != "DAY OFF" && i.DutyPost != "HOLIDAY" && i.DutyPost != "SICK DAY" && i.DutyPost != "VACATION").ToList();

                        var comments = new Phrase(1, "\u00a0");


                        if (sdays.Count > 0)
                        {
                            comments = new Phrase("Sick Day", tentimes);
                        }
                        if (hdays.Count > 0)
                        {
                            comments = new Phrase("Holiday", tentimes);
                        }
                        if (ddays.Count > 0)
                        {
                            comments = new Phrase("Day Off", tentimes);
                        }
                        if (vdays.Count > 0)
                        {
                            comments = new Phrase("Vacation", tentimes);
                        }
                        var xheaderCell6 = new PdfPCell(comments);

                        var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.AddDays(11).ToShortDateString(), tentimes));

                        var xheaderCell2 = new PdfPCell(new Phrase(day12.Count.ToString(), tentimes));

                        var wh = day12.Where(i => i.ShiftOn == true).ToList();

                        var xheaderCell3 = new PdfPCell(new Phrase(wh.Count.ToString(), tentimes));

                        var xheaderCell4 = new PdfPCell(new Phrase((wh.Count - day12.Count).ToString(), rtentimes));

                        variancetotal = variancetotal + (wh.Count - day12.Count);

                        var ah = day12.Where(i => i.TaskOn == true && i.ShiftOn == true).ToList();

                        var xheaderCell5 = new PdfPCell(new Phrase(ah.Count.ToString(), tentimes));


                        var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                        tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                        tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dutyrosterview.Date.AddDays(11) && i.CreatedDate.Value.Date <= dutyrosterview.Date.AddDays(11).AddHours(23)).ToList();

                        var groupedtevents = tevents.GroupBy(item => item.TaskId);
                        tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                        var ttraveldurationlist = 0.0;
                        var dttocheck = dutyrosterview.Date.AddDays(11);
                        foreach (var t in tevents)
                        {
                            var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                            if (task != null)
                            {
                                var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dttocheck);
                                ttraveldurationlist = tt + ttraveldurationlist;
                            }
                        }


                        var d = CommonUtility.GetUserOTHoursPerDay(dttocheck, dttocheck.AddHours(23), getots);
                        var xheaderCelltt = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                        var xheaderCelld = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));

                        totalOT = totalOT + d;
                        totalTT = totalTT + ttraveldurationlist;

                        xheaderCell.Border = Rectangle.NO_BORDER;
                        xheaderCell2.Border = Rectangle.NO_BORDER;
                        xheaderCell3.Border = Rectangle.NO_BORDER;
                        xheaderCell4.Border = Rectangle.NO_BORDER;
                        xheaderCell5.Border = Rectangle.NO_BORDER;
                        xheaderCell6.Border = Rectangle.NO_BORDER;

                        xheaderCell.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell2.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell3.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell4.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell5.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell6.BackgroundColor = Color.LIGHT_GRAY;


                        xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        newchecklistTable.AddCell(xheaderCell);
                        newchecklistTable.AddCell(xheaderCell2);
                        newchecklistTable.AddCell(xheaderCell3);
                        newchecklistTable.AddCell(xheaderCell4);

                        xheaderCelltt.Border = Rectangle.NO_BORDER;
                        xheaderCelld.Border = Rectangle.NO_BORDER;

                        xheaderCelltt.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCelld.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        xheaderCelltt.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCelld.BackgroundColor = Color.LIGHT_GRAY;

                        newchecklistTable.AddCell(xheaderCelltt);
                        newchecklistTable.AddCell(xheaderCelld);

                        newchecklistTable.AddCell(xheaderCell5);
                        newchecklistTable.AddCell(xheaderCell6);
                    }
                    else
                    {
                        var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.AddDays(11).ToShortDateString(), tentimes));
                        var xheaderCell2 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell3 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell4 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell5 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell6 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                        tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                        var dttocheck = dutyrosterview.Date.AddDays(11);

                        tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dttocheck && i.CreatedDate.Value.Date <= dttocheck.AddHours(23)).ToList();

                        var groupedtevents = tevents.GroupBy(item => item.TaskId);
                        tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                        var ttraveldurationlist = 0.0;

                        foreach (var t in tevents)
                        {
                            var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                            if (task != null)
                            {
                                var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dttocheck);
                                ttraveldurationlist = tt + ttraveldurationlist;
                            }
                        }

                        var d = CommonUtility.GetUserOTHoursPerDay(dttocheck, dttocheck.AddHours(23), getots);

                        var xheaderCell7 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                        var xheaderCell8 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));

                        totalOT = totalOT + d;
                        totalTT = totalTT + ttraveldurationlist;

                        xheaderCell.Border = Rectangle.NO_BORDER;
                        xheaderCell2.Border = Rectangle.NO_BORDER;
                        xheaderCell3.Border = Rectangle.NO_BORDER;
                        xheaderCell4.Border = Rectangle.NO_BORDER;
                        xheaderCell5.Border = Rectangle.NO_BORDER;
                        xheaderCell6.Border = Rectangle.NO_BORDER;
                        xheaderCell7.Border = Rectangle.NO_BORDER;
                        xheaderCell8.Border = Rectangle.NO_BORDER;

                        xheaderCell.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell2.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell3.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell4.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell5.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell6.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell7.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell8.BackgroundColor = Color.LIGHT_GRAY;

                        xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell7.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell8.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        newchecklistTable.AddCell(xheaderCell);
                        newchecklistTable.AddCell(xheaderCell2);
                        newchecklistTable.AddCell(xheaderCell3);
                        newchecklistTable.AddCell(xheaderCell4);
                        newchecklistTable.AddCell(xheaderCell7);
                        newchecklistTable.AddCell(xheaderCell8);
                        newchecklistTable.AddCell(xheaderCell5);
                        newchecklistTable.AddCell(xheaderCell6);
                    }

                    if (day13.Count > 0)
                    {

                        var sdays = day13.Where(i => i.DutyPost == "SICK DAY").ToList();
                        sickdays = sickdays + sdays.Count;
                        var hdays = day13.Where(i => i.DutyPost == "HOLIDAY").ToList();
                        holidays = holidays + hdays.Count;
                        var ddays = day13.Where(i => i.DutyPost == "DAY OFF").ToList();
                        dayoffs = dayoffs + ddays.Count;
                        var vdays = day13.Where(i => i.DutyPost == "VACATION").ToList();
                        vacations = vacations + vdays.Count;

                        day13 = day13.Where(i => i.DutyPost != "DAY OFF" && i.DutyPost != "HOLIDAY" && i.DutyPost != "SICK DAY" && i.DutyPost != "VACATION").ToList();

                        var comments = new Phrase(1, "\u00a0");


                        if (sdays.Count > 0)
                        {
                            comments = new Phrase("Sick Day", tentimes);
                        }
                        if (hdays.Count > 0)
                        {
                            comments = new Phrase("Holiday", tentimes);
                        }
                        if (ddays.Count > 0)
                        {
                            comments = new Phrase("Day Off", tentimes);
                        }
                        if (vdays.Count > 0)
                        {
                            comments = new Phrase("Vacation", tentimes);
                        }
                        var xheaderCell6 = new PdfPCell(comments);

                        var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.AddDays(12).ToShortDateString(), tentimes));

                        var xheaderCell2 = new PdfPCell(new Phrase(day13.Count.ToString(), tentimes));

                        var wh = day13.Where(i => i.ShiftOn == true).ToList();

                        var xheaderCell3 = new PdfPCell(new Phrase(wh.Count.ToString(), tentimes));

                        var xheaderCell4 = new PdfPCell(new Phrase((wh.Count - day13.Count).ToString(), rtentimes));

                        variancetotal = variancetotal + (wh.Count - day13.Count);

                        var ah = day13.Where(i => i.TaskOn == true && i.ShiftOn == true).ToList();

                        var xheaderCell5 = new PdfPCell(new Phrase(ah.Count.ToString(), tentimes));


                        var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                        tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                        tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dutyrosterview.Date.AddDays(12) && i.CreatedDate.Value.Date <= dutyrosterview.Date.AddDays(12).AddHours(23)).ToList();

                        var groupedtevents = tevents.GroupBy(item => item.TaskId);
                        tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                        var ttraveldurationlist = 0.0;
                        var dttocheck = dutyrosterview.Date.AddDays(12);
                        foreach (var t in tevents)
                        {
                            var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                            if (task != null)
                            {
                                var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dttocheck);
                                ttraveldurationlist = tt + ttraveldurationlist;
                            }
                        }


                        var d = CommonUtility.GetUserOTHoursPerDay(dttocheck, dttocheck.AddHours(23), getots);
                        var xheaderCelltt = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                        var xheaderCelld = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));

                        totalOT = totalOT + d;
                        totalTT = totalTT + ttraveldurationlist;

                        xheaderCell.Border = Rectangle.NO_BORDER;
                        xheaderCell2.Border = Rectangle.NO_BORDER;
                        xheaderCell3.Border = Rectangle.NO_BORDER;
                        xheaderCell4.Border = Rectangle.NO_BORDER;
                        xheaderCell5.Border = Rectangle.NO_BORDER;
                        xheaderCell6.Border = Rectangle.NO_BORDER;


                        xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        newchecklistTable.AddCell(xheaderCell);
                        newchecklistTable.AddCell(xheaderCell2);
                        newchecklistTable.AddCell(xheaderCell3);
                        newchecklistTable.AddCell(xheaderCell4);

                        xheaderCelltt.Border = Rectangle.NO_BORDER;
                        xheaderCelld.Border = Rectangle.NO_BORDER;

                        xheaderCelltt.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCelld.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        newchecklistTable.AddCell(xheaderCelltt);
                        newchecklistTable.AddCell(xheaderCelld);

                        newchecklistTable.AddCell(xheaderCell5);
                        newchecklistTable.AddCell(xheaderCell6);
                    }
                    else
                    {
                        var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.AddDays(12).ToShortDateString(), tentimes));
                        var xheaderCell2 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell3 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell4 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell5 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell6 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                        tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                        var dttocheck = dutyrosterview.Date.AddDays(12);

                        tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dttocheck && i.CreatedDate.Value.Date <= dttocheck.AddHours(23)).ToList();

                        var groupedtevents = tevents.GroupBy(item => item.TaskId);
                        tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                        var ttraveldurationlist = 0.0;

                        foreach (var t in tevents)
                        {
                            var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                            if (task != null)
                            {
                                var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dttocheck);
                                ttraveldurationlist = tt + ttraveldurationlist;
                            }
                        }

                        var d = CommonUtility.GetUserOTHoursPerDay(dttocheck, dttocheck.AddHours(23), getots);

                        var xheaderCell7 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                        var xheaderCell8 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));

                        totalOT = totalOT + d;
                        totalTT = totalTT + ttraveldurationlist;

                        xheaderCell.Border = Rectangle.NO_BORDER;
                        xheaderCell2.Border = Rectangle.NO_BORDER;
                        xheaderCell3.Border = Rectangle.NO_BORDER;
                        xheaderCell4.Border = Rectangle.NO_BORDER;
                        xheaderCell5.Border = Rectangle.NO_BORDER;
                        xheaderCell6.Border = Rectangle.NO_BORDER;
                        xheaderCell7.Border = Rectangle.NO_BORDER;
                        xheaderCell8.Border = Rectangle.NO_BORDER;

                        xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell7.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell8.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        newchecklistTable.AddCell(xheaderCell);
                        newchecklistTable.AddCell(xheaderCell2);
                        newchecklistTable.AddCell(xheaderCell3);
                        newchecklistTable.AddCell(xheaderCell4);
                        newchecklistTable.AddCell(xheaderCell7);
                        newchecklistTable.AddCell(xheaderCell8);
                        newchecklistTable.AddCell(xheaderCell5);
                        newchecklistTable.AddCell(xheaderCell6);
                    }

                    if (day14.Count > 0)
                    {

                        var sdays = day14.Where(i => i.DutyPost == "SICK DAY").ToList();
                        sickdays = sickdays + sdays.Count;
                        var hdays = day14.Where(i => i.DutyPost == "HOLIDAY").ToList();
                        holidays = holidays + hdays.Count;
                        var ddays = day14.Where(i => i.DutyPost == "DAY OFF").ToList();
                        dayoffs = dayoffs + ddays.Count;
                        var vdays = day14.Where(i => i.DutyPost == "VACATION").ToList();
                        vacations = vacations + vdays.Count;

                        day14 = day14.Where(i => i.DutyPost != "DAY OFF" && i.DutyPost != "HOLIDAY" && i.DutyPost != "SICK DAY" && i.DutyPost != "VACATION").ToList();

                        var comments = new Phrase(1, "\u00a0");


                        if (sdays.Count > 0)
                        {
                            comments = new Phrase("Sick Day", tentimes);
                        }
                        if (hdays.Count > 0)
                        {
                            comments = new Phrase("Holiday", tentimes);
                        }
                        if (ddays.Count > 0)
                        {
                            comments = new Phrase("Day Off", tentimes);
                        }
                        if (vdays.Count > 0)
                        {
                            comments = new Phrase("Vacation", tentimes);
                        }
                        var xheaderCell6 = new PdfPCell(comments);

                        var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.AddDays(13).ToShortDateString(), tentimes));

                        var xheaderCell2 = new PdfPCell(new Phrase(day14.Count.ToString(), tentimes));

                        var wh = day14.Where(i => i.ShiftOn == true).ToList();

                        var xheaderCell3 = new PdfPCell(new Phrase(wh.Count.ToString(), tentimes));

                        var xheaderCell4 = new PdfPCell(new Phrase((wh.Count - day14.Count).ToString(), rtentimes));

                        variancetotal = variancetotal + (wh.Count - day14.Count);

                        var ah = day14.Where(i => i.TaskOn == true && i.ShiftOn == true).ToList();

                        var xheaderCell5 = new PdfPCell(new Phrase(ah.Count.ToString(), tentimes));


                        var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                        tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                        tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dutyrosterview.Date.AddDays(13) && i.CreatedDate.Value.Date <= dutyrosterview.Date.AddDays(13).AddHours(23)).ToList();

                        var groupedtevents = tevents.GroupBy(item => item.TaskId);
                        tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                        var ttraveldurationlist = 0.0;
                        var dttocheck = dutyrosterview.Date.AddDays(13);
                        foreach (var t in tevents)
                        {
                            var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                            if (task != null)
                            {
                                var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dttocheck);
                                ttraveldurationlist = tt + ttraveldurationlist;
                            }
                        }


                        var d = CommonUtility.GetUserOTHoursPerDay(dttocheck, dttocheck.AddHours(23), getots);
                        var xheaderCelltt = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                        var xheaderCelld = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));

                        totalOT = totalOT + d;
                        totalTT = totalTT + ttraveldurationlist;

                        xheaderCell.Border = Rectangle.NO_BORDER;
                        xheaderCell2.Border = Rectangle.NO_BORDER;
                        xheaderCell3.Border = Rectangle.NO_BORDER;
                        xheaderCell4.Border = Rectangle.NO_BORDER;
                        xheaderCell5.Border = Rectangle.NO_BORDER;
                        xheaderCell6.Border = Rectangle.NO_BORDER;


                        xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        xheaderCell.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell2.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell3.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell4.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell5.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell6.BackgroundColor = Color.LIGHT_GRAY;

                        newchecklistTable.AddCell(xheaderCell);
                        newchecklistTable.AddCell(xheaderCell2);
                        newchecklistTable.AddCell(xheaderCell3);
                        newchecklistTable.AddCell(xheaderCell4);

                        xheaderCelltt.Border = Rectangle.NO_BORDER;
                        xheaderCelld.Border = Rectangle.NO_BORDER;

                        xheaderCelltt.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCelld.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        xheaderCelltt.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCelld.BackgroundColor = Color.LIGHT_GRAY;

                        newchecklistTable.AddCell(xheaderCelltt);
                        newchecklistTable.AddCell(xheaderCelld);

                        newchecklistTable.AddCell(xheaderCell5);
                        newchecklistTable.AddCell(xheaderCell6);
                    }
                    else
                    {
                        var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.AddDays(13).ToShortDateString(), tentimes));
                        var xheaderCell2 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell3 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell4 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell5 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell6 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                        tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                        var dttocheck = dutyrosterview.Date.AddDays(13);

                        tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dttocheck && i.CreatedDate.Value.Date <= dttocheck.AddHours(23)).ToList();

                        var groupedtevents = tevents.GroupBy(item => item.TaskId);
                        tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                        var ttraveldurationlist = 0.0;

                        foreach (var t in tevents)
                        {
                            var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                            if (task != null)
                            {
                                var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dttocheck);
                                ttraveldurationlist = tt + ttraveldurationlist;
                            }
                        }

                        var d = CommonUtility.GetUserOTHoursPerDay(dttocheck, dttocheck.AddHours(23), getots);

                        var xheaderCell7 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                        var xheaderCell8 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));

                        totalOT = totalOT + d;
                        totalTT = totalTT + ttraveldurationlist;

                        xheaderCell.Border = Rectangle.NO_BORDER;
                        xheaderCell2.Border = Rectangle.NO_BORDER;
                        xheaderCell3.Border = Rectangle.NO_BORDER;
                        xheaderCell4.Border = Rectangle.NO_BORDER;
                        xheaderCell5.Border = Rectangle.NO_BORDER;
                        xheaderCell6.Border = Rectangle.NO_BORDER;
                        xheaderCell7.Border = Rectangle.NO_BORDER;
                        xheaderCell8.Border = Rectangle.NO_BORDER;

                        xheaderCell.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell2.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell3.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell4.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell5.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell6.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell7.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell8.BackgroundColor = Color.LIGHT_GRAY;

                        xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell7.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell8.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        newchecklistTable.AddCell(xheaderCell);
                        newchecklistTable.AddCell(xheaderCell2);
                        newchecklistTable.AddCell(xheaderCell3);
                        newchecklistTable.AddCell(xheaderCell4);
                        newchecklistTable.AddCell(xheaderCell7);
                        newchecklistTable.AddCell(xheaderCell8);
                        newchecklistTable.AddCell(xheaderCell5);
                        newchecklistTable.AddCell(xheaderCell6);
                    }

                    if (day15.Count > 0)
                    {

                        var sdays = day15.Where(i => i.DutyPost == "SICK DAY").ToList();
                        sickdays = sickdays + sdays.Count;
                        var hdays = day15.Where(i => i.DutyPost == "HOLIDAY").ToList();
                        holidays = holidays + hdays.Count;
                        var ddays = day15.Where(i => i.DutyPost == "DAY OFF").ToList();
                        dayoffs = dayoffs + ddays.Count;
                        var vdays = day15.Where(i => i.DutyPost == "VACATION").ToList();
                        vacations = vacations + vdays.Count;

                        day15 = day15.Where(i => i.DutyPost != "DAY OFF" && i.DutyPost != "HOLIDAY" && i.DutyPost != "SICK DAY" && i.DutyPost != "VACATION").ToList();

                        var comments = new Phrase(1, "\u00a0");


                        if (sdays.Count > 0)
                        {
                            comments = new Phrase("Sick Day", tentimes);
                        }
                        if (hdays.Count > 0)
                        {
                            comments = new Phrase("Holiday", tentimes);
                        }
                        if (ddays.Count > 0)
                        {
                            comments = new Phrase("Day Off", tentimes);
                        }
                        if (vdays.Count > 0)
                        {
                            comments = new Phrase("Vacation", tentimes);
                        }
                        var xheaderCell6 = new PdfPCell(comments);

                        var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.AddDays(14).ToShortDateString(), tentimes));

                        var xheaderCell2 = new PdfPCell(new Phrase(day15.Count.ToString(), tentimes));

                        var wh = day15.Where(i => i.ShiftOn == true).ToList();

                        var xheaderCell3 = new PdfPCell(new Phrase(wh.Count.ToString(), tentimes));

                        var xheaderCell4 = new PdfPCell(new Phrase((wh.Count - day15.Count).ToString(), rtentimes));

                        variancetotal = variancetotal + (wh.Count - day15.Count);

                        var ah = day15.Where(i => i.TaskOn == true && i.ShiftOn == true).ToList();

                        var xheaderCell5 = new PdfPCell(new Phrase(ah.Count.ToString(), tentimes));


                        var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                        tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                        tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dutyrosterview.Date.AddDays(14) && i.CreatedDate.Value.Date <= dutyrosterview.Date.AddDays(14).AddHours(23)).ToList();

                        var groupedtevents = tevents.GroupBy(item => item.TaskId);
                        tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                        var ttraveldurationlist = 0.0;
                        var dttocheck = dutyrosterview.Date.AddDays(14);
                        foreach (var t in tevents)
                        {
                            var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                            if (task != null)
                            {
                                var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dttocheck);
                                ttraveldurationlist = tt + ttraveldurationlist;
                            }
                        }


                        var d = CommonUtility.GetUserOTHoursPerDay(dttocheck, dttocheck.AddHours(23), getots);
                        var xheaderCelltt = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                        var xheaderCelld = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));

                        totalOT = totalOT + d;
                        totalTT = totalTT + ttraveldurationlist;

                        xheaderCell.Border = Rectangle.NO_BORDER;
                        xheaderCell2.Border = Rectangle.NO_BORDER;
                        xheaderCell3.Border = Rectangle.NO_BORDER;
                        xheaderCell4.Border = Rectangle.NO_BORDER;
                        xheaderCell5.Border = Rectangle.NO_BORDER;
                        xheaderCell6.Border = Rectangle.NO_BORDER;


                        xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        newchecklistTable.AddCell(xheaderCell);
                        newchecklistTable.AddCell(xheaderCell2);
                        newchecklistTable.AddCell(xheaderCell3);
                        newchecklistTable.AddCell(xheaderCell4);

                        xheaderCelltt.Border = Rectangle.NO_BORDER;
                        xheaderCelld.Border = Rectangle.NO_BORDER;

                        xheaderCelltt.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCelld.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        newchecklistTable.AddCell(xheaderCelltt);
                        newchecklistTable.AddCell(xheaderCelld);

                        newchecklistTable.AddCell(xheaderCell5);
                        newchecklistTable.AddCell(xheaderCell6);
                    }
                    else
                    {
                        var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.AddDays(14).ToShortDateString(), tentimes));
                        var xheaderCell2 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell3 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell4 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell5 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell6 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                        tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                        var dttocheck = dutyrosterview.Date.AddDays(14);

                        tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dttocheck && i.CreatedDate.Value.Date <= dttocheck.AddHours(23)).ToList();

                        var groupedtevents = tevents.GroupBy(item => item.TaskId);
                        tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                        var ttraveldurationlist = 0.0;

                        foreach (var t in tevents)
                        {
                            var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                            if (task != null)
                            {
                                var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dttocheck);
                                ttraveldurationlist = tt + ttraveldurationlist;
                            }
                        }

                        var d = CommonUtility.GetUserOTHoursPerDay(dttocheck, dttocheck.AddHours(23), getots);

                        var xheaderCell7 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                        var xheaderCell8 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));

                        totalOT = totalOT + d;
                        totalTT = totalTT + ttraveldurationlist;

                        xheaderCell.Border = Rectangle.NO_BORDER;
                        xheaderCell2.Border = Rectangle.NO_BORDER;
                        xheaderCell3.Border = Rectangle.NO_BORDER;
                        xheaderCell4.Border = Rectangle.NO_BORDER;
                        xheaderCell5.Border = Rectangle.NO_BORDER;
                        xheaderCell6.Border = Rectangle.NO_BORDER;
                        xheaderCell7.Border = Rectangle.NO_BORDER;
                        xheaderCell8.Border = Rectangle.NO_BORDER;

                        xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell7.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell8.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        newchecklistTable.AddCell(xheaderCell);
                        newchecklistTable.AddCell(xheaderCell2);
                        newchecklistTable.AddCell(xheaderCell3);
                        newchecklistTable.AddCell(xheaderCell4);
                        newchecklistTable.AddCell(xheaderCell7);
                        newchecklistTable.AddCell(xheaderCell8);
                        newchecklistTable.AddCell(xheaderCell5);
                        newchecklistTable.AddCell(xheaderCell6);
                    }

                    if (day16.Count > 0)
                    {

                        var sdays = day16.Where(i => i.DutyPost == "SICK DAY").ToList();
                        sickdays = sickdays + sdays.Count;
                        var hdays = day16.Where(i => i.DutyPost == "HOLIDAY").ToList();
                        holidays = holidays + hdays.Count;
                        var ddays = day16.Where(i => i.DutyPost == "DAY OFF").ToList();
                        dayoffs = dayoffs + ddays.Count;
                        var vdays = day16.Where(i => i.DutyPost == "VACATION").ToList();
                        vacations = vacations + vdays.Count;

                        day16 = day16.Where(i => i.DutyPost != "DAY OFF" && i.DutyPost != "HOLIDAY" && i.DutyPost != "SICK DAY" && i.DutyPost != "VACATION").ToList();

                        var comments = new Phrase(1, "\u00a0");


                        if (sdays.Count > 0)
                        {
                            comments = new Phrase("Sick Day", tentimes);
                        }
                        if (hdays.Count > 0)
                        {
                            comments = new Phrase("Holiday", tentimes);
                        }
                        if (ddays.Count > 0)
                        {
                            comments = new Phrase("Day Off", tentimes);
                        }
                        if (vdays.Count > 0)
                        {
                            comments = new Phrase("Vacation", tentimes);
                        }
                        var xheaderCell6 = new PdfPCell(comments);

                        var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.AddDays(15).ToShortDateString(), tentimes));

                        var xheaderCell2 = new PdfPCell(new Phrase(day16.Count.ToString(), tentimes));

                        var wh = day16.Where(i => i.ShiftOn == true).ToList();

                        var xheaderCell3 = new PdfPCell(new Phrase(wh.Count.ToString(), tentimes));

                        var xheaderCell4 = new PdfPCell(new Phrase((wh.Count - day16.Count).ToString(), rtentimes));

                        variancetotal = variancetotal + (wh.Count - day16.Count);

                        var ah = day16.Where(i => i.TaskOn == true && i.ShiftOn == true).ToList();

                        var xheaderCell5 = new PdfPCell(new Phrase(ah.Count.ToString(), tentimes));


                        var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);


                        tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                        tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dutyrosterview.Date.AddDays(15) && i.CreatedDate.Value.Date <= dutyrosterview.Date.AddDays(15).AddHours(23)).ToList();

                        var groupedtevents = tevents.GroupBy(item => item.TaskId);
                        tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                        var ttraveldurationlist = 0.0;
                        var dttocheck = dutyrosterview.Date.AddDays(15);
                        foreach (var t in tevents)
                        {
                            var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                            if (task != null)
                            {
                                var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dttocheck);
                                ttraveldurationlist = tt + ttraveldurationlist;
                            }
                        }


                        var d = CommonUtility.GetUserOTHoursPerDay(dttocheck, dttocheck.AddHours(23), getots);
                        var xheaderCelltt = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                        var xheaderCelld = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));

                        totalOT = totalOT + d;
                        totalTT = totalTT + ttraveldurationlist;

                        xheaderCell.Border = Rectangle.NO_BORDER;
                        xheaderCell2.Border = Rectangle.NO_BORDER;
                        xheaderCell3.Border = Rectangle.NO_BORDER;
                        xheaderCell4.Border = Rectangle.NO_BORDER;
                        xheaderCell5.Border = Rectangle.NO_BORDER;
                        xheaderCell6.Border = Rectangle.NO_BORDER;


                        xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        xheaderCell.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell2.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell3.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell4.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell5.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell6.BackgroundColor = Color.LIGHT_GRAY;

                        newchecklistTable.AddCell(xheaderCell);
                        newchecklistTable.AddCell(xheaderCell2);
                        newchecklistTable.AddCell(xheaderCell3);
                        newchecklistTable.AddCell(xheaderCell4);

                        xheaderCelltt.Border = Rectangle.NO_BORDER;
                        xheaderCelld.Border = Rectangle.NO_BORDER;

                        xheaderCelltt.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCelld.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        xheaderCelltt.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCelld.BackgroundColor = Color.LIGHT_GRAY;

                        newchecklistTable.AddCell(xheaderCelltt);
                        newchecklistTable.AddCell(xheaderCelld);

                        newchecklistTable.AddCell(xheaderCell5);
                        newchecklistTable.AddCell(xheaderCell6);
                    }
                    else
                    {
                        var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.AddDays(15).ToShortDateString(), tentimes));
                        var xheaderCell2 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell3 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell4 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell5 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell6 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                        tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                        var dttocheck = dutyrosterview.Date.AddDays(15);

                        tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dttocheck && i.CreatedDate.Value.Date <= dttocheck.AddHours(23)).ToList();

                        var groupedtevents = tevents.GroupBy(item => item.TaskId);
                        tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                        var ttraveldurationlist = 0.0;

                        foreach (var t in tevents)
                        {
                            var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                            if (task != null)
                            {
                                var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dttocheck);
                                ttraveldurationlist = tt + ttraveldurationlist;
                            }
                        }

                        var d = CommonUtility.GetUserOTHoursPerDay(dttocheck, dttocheck.AddHours(23), getots);

                        var xheaderCell7 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                        var xheaderCell8 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));

                        totalOT = totalOT + d;
                        totalTT = totalTT + ttraveldurationlist;

                        xheaderCell.Border = Rectangle.NO_BORDER;
                        xheaderCell2.Border = Rectangle.NO_BORDER;
                        xheaderCell3.Border = Rectangle.NO_BORDER;
                        xheaderCell4.Border = Rectangle.NO_BORDER;
                        xheaderCell5.Border = Rectangle.NO_BORDER;
                        xheaderCell6.Border = Rectangle.NO_BORDER;
                        xheaderCell7.Border = Rectangle.NO_BORDER;
                        xheaderCell8.Border = Rectangle.NO_BORDER;

                        xheaderCell.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell2.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell3.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell4.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell5.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell6.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell7.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell8.BackgroundColor = Color.LIGHT_GRAY;

                        xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell7.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell8.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        newchecklistTable.AddCell(xheaderCell);
                        newchecklistTable.AddCell(xheaderCell2);
                        newchecklistTable.AddCell(xheaderCell3);
                        newchecklistTable.AddCell(xheaderCell4);
                        newchecklistTable.AddCell(xheaderCell7);
                        newchecklistTable.AddCell(xheaderCell8);
                        newchecklistTable.AddCell(xheaderCell5);
                        newchecklistTable.AddCell(xheaderCell6);
                    }

                    if (day17.Count > 0)
                    {

                        var sdays = day17.Where(i => i.DutyPost == "SICK DAY").ToList();
                        sickdays = sickdays + sdays.Count;
                        var hdays = day17.Where(i => i.DutyPost == "HOLIDAY").ToList();
                        holidays = holidays + hdays.Count;
                        var ddays = day17.Where(i => i.DutyPost == "DAY OFF").ToList();
                        dayoffs = dayoffs + ddays.Count;
                        var vdays = day17.Where(i => i.DutyPost == "VACATION").ToList();
                        vacations = vacations + vdays.Count;

                        day17 = day17.Where(i => i.DutyPost != "DAY OFF" && i.DutyPost != "HOLIDAY" && i.DutyPost != "SICK DAY" && i.DutyPost != "VACATION").ToList();

                        var comments = new Phrase(1, "\u00a0");


                        if (sdays.Count > 0)
                        {
                            comments = new Phrase("Sick Day", tentimes);
                        }
                        if (hdays.Count > 0)
                        {
                            comments = new Phrase("Holiday", tentimes);
                        }
                        if (ddays.Count > 0)
                        {
                            comments = new Phrase("Day Off", tentimes);
                        }
                        if (vdays.Count > 0)
                        {
                            comments = new Phrase("Vacation", tentimes);
                        }
                        var xheaderCell6 = new PdfPCell(comments);

                        var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.AddDays(16).ToShortDateString(), tentimes));

                        var xheaderCell2 = new PdfPCell(new Phrase(day17.Count.ToString(), tentimes));

                        var wh = day17.Where(i => i.ShiftOn == true).ToList();

                        var xheaderCell3 = new PdfPCell(new Phrase(wh.Count.ToString(), tentimes));

                        var xheaderCell4 = new PdfPCell(new Phrase((wh.Count - day17.Count).ToString(), rtentimes));

                        variancetotal = variancetotal + (wh.Count - day17.Count);

                        var ah = day17.Where(i => i.TaskOn == true && i.ShiftOn == true).ToList();

                        var xheaderCell5 = new PdfPCell(new Phrase(ah.Count.ToString(), tentimes));

                        var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                        tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                        tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dutyrosterview.Date.AddDays(16) && i.CreatedDate.Value.Date <= dutyrosterview.Date.AddDays(16).AddHours(23)).ToList();

                        var groupedtevents = tevents.GroupBy(item => item.TaskId);
                        tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                        var ttraveldurationlist = 0.0;
                        var dttocheck = dutyrosterview.Date.AddDays(16);
                        foreach (var t in tevents)
                        {
                            var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                            if (task != null)
                            {
                                var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dttocheck);
                                ttraveldurationlist = tt + ttraveldurationlist;
                            }
                        }


                        var d = CommonUtility.GetUserOTHoursPerDay(dttocheck, dttocheck.AddHours(23), getots);
                        var xheaderCelltt = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                        var xheaderCelld = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));

                        totalOT = totalOT + d;
                        totalTT = totalTT + ttraveldurationlist;

                        xheaderCell.Border = Rectangle.NO_BORDER;
                        xheaderCell2.Border = Rectangle.NO_BORDER;
                        xheaderCell3.Border = Rectangle.NO_BORDER;
                        xheaderCell4.Border = Rectangle.NO_BORDER;
                        xheaderCell5.Border = Rectangle.NO_BORDER;
                        xheaderCell6.Border = Rectangle.NO_BORDER;


                        xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        newchecklistTable.AddCell(xheaderCell);
                        newchecklistTable.AddCell(xheaderCell2);
                        newchecklistTable.AddCell(xheaderCell3);
                        newchecklistTable.AddCell(xheaderCell4);

                        xheaderCelltt.Border = Rectangle.NO_BORDER;
                        xheaderCelld.Border = Rectangle.NO_BORDER;

                        xheaderCelltt.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCelld.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        newchecklistTable.AddCell(xheaderCelltt);
                        newchecklistTable.AddCell(xheaderCelld);

                        newchecklistTable.AddCell(xheaderCell5);
                        newchecklistTable.AddCell(xheaderCell6);
                    }
                    else
                    {
                        var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.AddDays(16).ToShortDateString(), tentimes));
                        var xheaderCell2 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell3 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell4 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell5 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell6 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                        tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                        var dttocheck = dutyrosterview.Date.AddDays(16);

                        tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dttocheck && i.CreatedDate.Value.Date <= dttocheck.AddHours(23)).ToList();

                        var groupedtevents = tevents.GroupBy(item => item.TaskId);

                        tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();
                        var ttraveldurationlist = 0.0;

                        foreach (var t in tevents)
                        {
                            var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                            if (task != null)
                            {
                                var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dttocheck);
                                ttraveldurationlist = tt + ttraveldurationlist;
                            }
                        }

                        var d = CommonUtility.GetUserOTHoursPerDay(dttocheck, dttocheck.AddHours(23), getots);

                        var xheaderCell7 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                        var xheaderCell8 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));

                        totalOT = totalOT + d;
                        totalTT = totalTT + ttraveldurationlist;

                        xheaderCell.Border = Rectangle.NO_BORDER;
                        xheaderCell2.Border = Rectangle.NO_BORDER;
                        xheaderCell3.Border = Rectangle.NO_BORDER;
                        xheaderCell4.Border = Rectangle.NO_BORDER;
                        xheaderCell5.Border = Rectangle.NO_BORDER;
                        xheaderCell6.Border = Rectangle.NO_BORDER;
                        xheaderCell7.Border = Rectangle.NO_BORDER;
                        xheaderCell8.Border = Rectangle.NO_BORDER;

                        xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell7.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell8.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        newchecklistTable.AddCell(xheaderCell);
                        newchecklistTable.AddCell(xheaderCell2);
                        newchecklistTable.AddCell(xheaderCell3);
                        newchecklistTable.AddCell(xheaderCell4);
                        newchecklistTable.AddCell(xheaderCell7);
                        newchecklistTable.AddCell(xheaderCell8);
                        newchecklistTable.AddCell(xheaderCell5);
                        newchecklistTable.AddCell(xheaderCell6);
                    }

                    if (day18.Count > 0)
                    {

                        var sdays = day18.Where(i => i.DutyPost == "SICK DAY").ToList();
                        sickdays = sickdays + sdays.Count;
                        var hdays = day18.Where(i => i.DutyPost == "HOLIDAY").ToList();
                        holidays = holidays + hdays.Count;
                        var ddays = day18.Where(i => i.DutyPost == "DAY OFF").ToList();
                        dayoffs = dayoffs + ddays.Count;
                        var vdays = day18.Where(i => i.DutyPost == "VACATION").ToList();
                        vacations = vacations + vdays.Count;

                        day18 = day18.Where(i => i.DutyPost != "DAY OFF" && i.DutyPost != "HOLIDAY" && i.DutyPost != "SICK DAY" && i.DutyPost != "VACATION").ToList();

                        var comments = new Phrase(1, "\u00a0");


                        if (sdays.Count > 0)
                        {
                            comments = new Phrase("Sick Day", tentimes);
                        }
                        if (hdays.Count > 0)
                        {
                            comments = new Phrase("Holiday", tentimes);
                        }
                        if (ddays.Count > 0)
                        {
                            comments = new Phrase("Day Off", tentimes);
                        }
                        if (vdays.Count > 0)
                        {
                            comments = new Phrase("Vacation", tentimes);
                        }
                        var xheaderCell6 = new PdfPCell(comments);

                        var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.AddDays(17).ToShortDateString(), tentimes));

                        var xheaderCell2 = new PdfPCell(new Phrase(day18.Count.ToString(), tentimes));

                        var wh = day18.Where(i => i.ShiftOn == true).ToList();

                        var xheaderCell3 = new PdfPCell(new Phrase(wh.Count.ToString(), tentimes));

                        var xheaderCell4 = new PdfPCell(new Phrase((wh.Count - day18.Count).ToString(), rtentimes));

                        variancetotal = variancetotal + (wh.Count - day18.Count);

                        var ah = day18.Where(i => i.TaskOn == true && i.ShiftOn == true).ToList();

                        var xheaderCell5 = new PdfPCell(new Phrase(ah.Count.ToString(), tentimes));

                        var dttocheck = dutyrosterview.Date.AddDays(17);

                        var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                        tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                        tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dutyrosterview.Date.AddDays(17) && i.CreatedDate.Value.Date <= dutyrosterview.Date.AddDays(17).AddHours(23)).ToList();

                        var groupedtevents = tevents.GroupBy(item => item.TaskId);
                        tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                        var ttraveldurationlist = 0.0;

                        foreach (var t in tevents)
                        {
                            var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                            if (task != null)
                            {
                                var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dttocheck);
                                ttraveldurationlist = tt + ttraveldurationlist;
                            }
                        }


                        var d = CommonUtility.GetUserOTHoursPerDay(dttocheck, dttocheck.AddHours(23), getots);
                        var xheaderCelltt = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                        var xheaderCelld = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));

                        totalOT = totalOT + d;
                        totalTT = totalTT + ttraveldurationlist;

                        xheaderCell.Border = Rectangle.NO_BORDER;
                        xheaderCell2.Border = Rectangle.NO_BORDER;
                        xheaderCell3.Border = Rectangle.NO_BORDER;
                        xheaderCell4.Border = Rectangle.NO_BORDER;
                        xheaderCell5.Border = Rectangle.NO_BORDER;
                        xheaderCell6.Border = Rectangle.NO_BORDER;


                        xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        xheaderCell.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell2.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell3.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell4.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell5.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell6.BackgroundColor = Color.LIGHT_GRAY;

                        newchecklistTable.AddCell(xheaderCell);
                        newchecklistTable.AddCell(xheaderCell2);
                        newchecklistTable.AddCell(xheaderCell3);
                        newchecklistTable.AddCell(xheaderCell4);

                        xheaderCelltt.Border = Rectangle.NO_BORDER;
                        xheaderCelld.Border = Rectangle.NO_BORDER;

                        xheaderCelltt.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCelld.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        xheaderCelltt.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCelld.BackgroundColor = Color.LIGHT_GRAY;

                        newchecklistTable.AddCell(xheaderCelltt);
                        newchecklistTable.AddCell(xheaderCelld);

                        newchecklistTable.AddCell(xheaderCell5);
                        newchecklistTable.AddCell(xheaderCell6);
                    }
                    else
                    {
                        var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.AddDays(17).ToShortDateString(), tentimes));
                        var xheaderCell2 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell3 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell4 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell5 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell6 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                        tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                        var dttocheck = dutyrosterview.Date.AddDays(17);

                        tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dttocheck && i.CreatedDate.Value.Date <= dttocheck.AddHours(23)).ToList();

                        var groupedtevents = tevents.GroupBy(item => item.TaskId);
                        tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                        var ttraveldurationlist = 0.0;

                        foreach (var t in tevents)
                        {
                            var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                            if (task != null)
                            {
                                var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dttocheck);
                                ttraveldurationlist = tt + ttraveldurationlist;
                            }
                        }

                        var d = CommonUtility.GetUserOTHoursPerDay(dttocheck, dttocheck.AddHours(23), getots);

                        var xheaderCell7 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                        var xheaderCell8 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));

                        totalOT = totalOT + d;
                        totalTT = totalTT + ttraveldurationlist;

                        xheaderCell.Border = Rectangle.NO_BORDER;
                        xheaderCell2.Border = Rectangle.NO_BORDER;
                        xheaderCell3.Border = Rectangle.NO_BORDER;
                        xheaderCell4.Border = Rectangle.NO_BORDER;
                        xheaderCell5.Border = Rectangle.NO_BORDER;
                        xheaderCell6.Border = Rectangle.NO_BORDER;
                        xheaderCell7.Border = Rectangle.NO_BORDER;
                        xheaderCell8.Border = Rectangle.NO_BORDER;

                        xheaderCell.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell2.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell3.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell4.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell5.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell6.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell7.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell8.BackgroundColor = Color.LIGHT_GRAY;

                        xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell7.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell8.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        newchecklistTable.AddCell(xheaderCell);
                        newchecklistTable.AddCell(xheaderCell2);
                        newchecklistTable.AddCell(xheaderCell3);
                        newchecklistTable.AddCell(xheaderCell4);
                        newchecklistTable.AddCell(xheaderCell7);
                        newchecklistTable.AddCell(xheaderCell8);
                        newchecklistTable.AddCell(xheaderCell5);
                        newchecklistTable.AddCell(xheaderCell6);
                    }

                    if (day19.Count > 0)
                    {

                        var sdays = day19.Where(i => i.DutyPost == "SICK DAY").ToList();
                        sickdays = sickdays + sdays.Count;
                        var hdays = day19.Where(i => i.DutyPost == "HOLIDAY").ToList();
                        holidays = holidays + hdays.Count;
                        var ddays = day19.Where(i => i.DutyPost == "DAY OFF").ToList();
                        dayoffs = dayoffs + ddays.Count;
                        var vdays = day19.Where(i => i.DutyPost == "VACATION").ToList();
                        vacations = vacations + vdays.Count;

                        day19 = day19.Where(i => i.DutyPost != "DAY OFF" && i.DutyPost != "HOLIDAY" && i.DutyPost != "SICK DAY" && i.DutyPost != "VACATION").ToList();

                        var comments = new Phrase(1, "\u00a0");


                        if (sdays.Count > 0)
                        {
                            comments = new Phrase("Sick Day", tentimes);
                        }
                        if (hdays.Count > 0)
                        {
                            comments = new Phrase("Holiday", tentimes);
                        }
                        if (ddays.Count > 0)
                        {
                            comments = new Phrase("Day Off", tentimes);
                        }
                        if (vdays.Count > 0)
                        {
                            comments = new Phrase("Vacation", tentimes);
                        }
                        var xheaderCell6 = new PdfPCell(comments);

                        var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.AddDays(18).ToShortDateString(), tentimes));

                        var xheaderCell2 = new PdfPCell(new Phrase(day19.Count.ToString(), tentimes));

                        var wh = day19.Where(i => i.ShiftOn == true).ToList();

                        var xheaderCell3 = new PdfPCell(new Phrase(wh.Count.ToString(), tentimes));

                        var xheaderCell4 = new PdfPCell(new Phrase((wh.Count - day19.Count).ToString(), rtentimes));

                        variancetotal = variancetotal + (wh.Count - day19.Count);

                        var ah = day19.Where(i => i.TaskOn == true && i.ShiftOn == true).ToList();

                        var xheaderCell5 = new PdfPCell(new Phrase(ah.Count.ToString(), tentimes));



                        var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                        tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                        tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dutyrosterview.Date.AddDays(18) && i.CreatedDate.Value.Date <= dutyrosterview.Date.AddDays(18).AddHours(23)).ToList();

                        var groupedtevents = tevents.GroupBy(item => item.TaskId);
                        tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                        var ttraveldurationlist = 0.0;
                        var dttocheck = dutyrosterview.Date.AddDays(18);
                        foreach (var t in tevents)
                        {
                            var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                            if (task != null)
                            {
                                var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dttocheck);
                                ttraveldurationlist = tt + ttraveldurationlist;
                            }
                        }


                        var d = CommonUtility.GetUserOTHoursPerDay(dttocheck, dttocheck.AddHours(23), getots);
                        var xheaderCelltt = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                        var xheaderCelld = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));

                        totalOT = totalOT + d;
                        totalTT = totalTT + ttraveldurationlist;

                        xheaderCell.Border = Rectangle.NO_BORDER;
                        xheaderCell2.Border = Rectangle.NO_BORDER;
                        xheaderCell3.Border = Rectangle.NO_BORDER;
                        xheaderCell4.Border = Rectangle.NO_BORDER;
                        xheaderCell5.Border = Rectangle.NO_BORDER;
                        xheaderCell6.Border = Rectangle.NO_BORDER;


                        xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        newchecklistTable.AddCell(xheaderCell);
                        newchecklistTable.AddCell(xheaderCell2);
                        newchecklistTable.AddCell(xheaderCell3);
                        newchecklistTable.AddCell(xheaderCell4);

                        xheaderCelltt.Border = Rectangle.NO_BORDER;
                        xheaderCelld.Border = Rectangle.NO_BORDER;

                        xheaderCelltt.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCelld.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        newchecklistTable.AddCell(xheaderCelltt);
                        newchecklistTable.AddCell(xheaderCelld);

                        newchecklistTable.AddCell(xheaderCell5);
                        newchecklistTable.AddCell(xheaderCell6);
                    }
                    else
                    {
                        var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.AddDays(18).ToShortDateString(), tentimes));
                        var xheaderCell2 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell3 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell4 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell5 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell6 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                        tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                        var dttocheck = dutyrosterview.Date.AddDays(18);

                        tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dttocheck && i.CreatedDate.Value.Date <= dttocheck.AddHours(23)).ToList();

                        var groupedtevents = tevents.GroupBy(item => item.TaskId);
                        tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                        var ttraveldurationlist = 0.0;

                        foreach (var t in tevents)
                        {
                            var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                            if (task != null)
                            {
                                var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dttocheck);
                                ttraveldurationlist = tt + ttraveldurationlist;
                            }
                        }

                        var d = CommonUtility.GetUserOTHoursPerDay(dttocheck, dttocheck.AddHours(23), getots);
                        var xheaderCell7 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                        var xheaderCell8 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));

                        totalOT = totalOT + d;
                        totalTT = totalTT + ttraveldurationlist;

                        xheaderCell.Border = Rectangle.NO_BORDER;
                        xheaderCell2.Border = Rectangle.NO_BORDER;
                        xheaderCell3.Border = Rectangle.NO_BORDER;
                        xheaderCell4.Border = Rectangle.NO_BORDER;
                        xheaderCell5.Border = Rectangle.NO_BORDER;
                        xheaderCell6.Border = Rectangle.NO_BORDER;
                        xheaderCell7.Border = Rectangle.NO_BORDER;
                        xheaderCell8.Border = Rectangle.NO_BORDER;

                        xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell7.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell8.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        newchecklistTable.AddCell(xheaderCell);
                        newchecklistTable.AddCell(xheaderCell2);
                        newchecklistTable.AddCell(xheaderCell3);
                        newchecklistTable.AddCell(xheaderCell4);
                        newchecklistTable.AddCell(xheaderCell7);
                        newchecklistTable.AddCell(xheaderCell8);
                        newchecklistTable.AddCell(xheaderCell5);
                        newchecklistTable.AddCell(xheaderCell6);
                    }

                    if (day20.Count > 0)
                    {

                        var sdays = day20.Where(i => i.DutyPost == "SICK DAY").ToList();
                        sickdays = sickdays + sdays.Count;
                        var hdays = day20.Where(i => i.DutyPost == "HOLIDAY").ToList();
                        holidays = holidays + hdays.Count;
                        var ddays = day20.Where(i => i.DutyPost == "DAY OFF").ToList();
                        dayoffs = dayoffs + ddays.Count;
                        var vdays = day20.Where(i => i.DutyPost == "VACATION").ToList();
                        vacations = vacations + vdays.Count;

                        day20 = day20.Where(i => i.DutyPost != "DAY OFF" && i.DutyPost != "HOLIDAY" && i.DutyPost != "SICK DAY" && i.DutyPost != "VACATION").ToList();

                        var comments = new Phrase(1, "\u00a0");


                        if (sdays.Count > 0)
                        {
                            comments = new Phrase("Sick Day", tentimes);
                        }
                        if (hdays.Count > 0)
                        {
                            comments = new Phrase("Holiday", tentimes);
                        }
                        if (ddays.Count > 0)
                        {
                            comments = new Phrase("Day Off", tentimes);
                        }
                        if (vdays.Count > 0)
                        {
                            comments = new Phrase("Vacation", tentimes);
                        }
                        var xheaderCell6 = new PdfPCell(comments);

                        var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.AddDays(19).ToShortDateString(), tentimes));

                        var xheaderCell2 = new PdfPCell(new Phrase(day20.Count.ToString(), tentimes));

                        var wh = day20.Where(i => i.ShiftOn == true).ToList();

                        var xheaderCell3 = new PdfPCell(new Phrase(wh.Count.ToString(), tentimes));

                        var xheaderCell4 = new PdfPCell(new Phrase((wh.Count - day20.Count).ToString(), rtentimes));

                        variancetotal = variancetotal + (wh.Count - day20.Count);

                        var ah = day20.Where(i => i.TaskOn == true && i.ShiftOn == true).ToList();

                        var xheaderCell5 = new PdfPCell(new Phrase(ah.Count.ToString(), tentimes));


                        var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                        tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                        tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dutyrosterview.Date.AddDays(19) && i.CreatedDate.Value.Date <= dutyrosterview.Date.AddDays(19).AddHours(23)).ToList();

                        var groupedtevents = tevents.GroupBy(item => item.TaskId);
                        tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                        var ttraveldurationlist = 0.0;

                        var dttocheck = dutyrosterview.Date.AddDays(19);
                        foreach (var t in tevents)
                        {
                            var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                            if (task != null)
                            {
                                var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dttocheck);
                                ttraveldurationlist = tt + ttraveldurationlist;
                            }
                        }

                        var d = CommonUtility.GetUserOTHoursPerDay(dttocheck, dttocheck.AddHours(23), getots);
                        var xheaderCelltt = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                        var xheaderCelld = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));

                        totalOT = totalOT + d;
                        totalTT = totalTT + ttraveldurationlist;

                        xheaderCell.Border = Rectangle.NO_BORDER;
                        xheaderCell2.Border = Rectangle.NO_BORDER;
                        xheaderCell3.Border = Rectangle.NO_BORDER;
                        xheaderCell4.Border = Rectangle.NO_BORDER;
                        xheaderCell5.Border = Rectangle.NO_BORDER;
                        xheaderCell6.Border = Rectangle.NO_BORDER;

                        xheaderCell.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell2.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell3.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell4.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell5.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell6.BackgroundColor = Color.LIGHT_GRAY;


                        xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        newchecklistTable.AddCell(xheaderCell);
                        newchecklistTable.AddCell(xheaderCell2);
                        newchecklistTable.AddCell(xheaderCell3);
                        newchecklistTable.AddCell(xheaderCell4);

                        xheaderCelltt.Border = Rectangle.NO_BORDER;
                        xheaderCelld.Border = Rectangle.NO_BORDER;

                        xheaderCelltt.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCelld.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        xheaderCelltt.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCelld.BackgroundColor = Color.LIGHT_GRAY;

                        newchecklistTable.AddCell(xheaderCelltt);
                        newchecklistTable.AddCell(xheaderCelld);

                        newchecklistTable.AddCell(xheaderCell5);
                        newchecklistTable.AddCell(xheaderCell6);
                    }
                    else
                    {
                        var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.AddDays(19).ToShortDateString(), tentimes));
                        var xheaderCell2 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell3 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell4 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell5 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell6 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                        tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                        var dttocheck = dutyrosterview.Date.AddDays(19);

                        tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dttocheck && i.CreatedDate.Value.Date <= dttocheck.AddHours(23)).ToList();

                        var groupedtevents = tevents.GroupBy(item => item.TaskId);
                        tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                        var ttraveldurationlist = 0.0;

                        foreach (var t in tevents)
                        {
                            var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                            if (task != null)
                            {
                                var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dttocheck);
                                ttraveldurationlist = tt + ttraveldurationlist;
                            }
                        }

                        var d = CommonUtility.GetUserOTHoursPerDay(dttocheck, dttocheck.AddHours(23), getots);
                        var xheaderCell7 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                        var xheaderCell8 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));

                        totalOT = totalOT + d;
                        totalTT = totalTT + ttraveldurationlist;

                        xheaderCell.Border = Rectangle.NO_BORDER;
                        xheaderCell2.Border = Rectangle.NO_BORDER;
                        xheaderCell3.Border = Rectangle.NO_BORDER;
                        xheaderCell4.Border = Rectangle.NO_BORDER;
                        xheaderCell5.Border = Rectangle.NO_BORDER;
                        xheaderCell6.Border = Rectangle.NO_BORDER;
                        xheaderCell7.Border = Rectangle.NO_BORDER;
                        xheaderCell8.Border = Rectangle.NO_BORDER;

                        xheaderCell.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell2.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell3.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell4.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell5.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell6.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell7.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell8.BackgroundColor = Color.LIGHT_GRAY;

                        xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell7.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell8.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        newchecklistTable.AddCell(xheaderCell);
                        newchecklistTable.AddCell(xheaderCell2);
                        newchecklistTable.AddCell(xheaderCell3);
                        newchecklistTable.AddCell(xheaderCell4);
                        newchecklistTable.AddCell(xheaderCell7);
                        newchecklistTable.AddCell(xheaderCell8);
                        newchecklistTable.AddCell(xheaderCell5);
                        newchecklistTable.AddCell(xheaderCell6);
                    }

                    if (day21.Count > 0)
                    {

                        var sdays = day21.Where(i => i.DutyPost == "SICK DAY").ToList();
                        sickdays = sickdays + sdays.Count;
                        var hdays = day21.Where(i => i.DutyPost == "HOLIDAY").ToList();
                        holidays = holidays + hdays.Count;
                        var ddays = day21.Where(i => i.DutyPost == "DAY OFF").ToList();
                        dayoffs = dayoffs + ddays.Count;
                        var vdays = day21.Where(i => i.DutyPost == "VACATION").ToList();
                        vacations = vacations + vdays.Count;

                        day21 = day21.Where(i => i.DutyPost != "DAY OFF" && i.DutyPost != "HOLIDAY" && i.DutyPost != "SICK DAY" && i.DutyPost != "VACATION").ToList();

                        var comments = new Phrase(1, "\u00a0");


                        if (sdays.Count > 0)
                        {
                            comments = new Phrase("Sick Day", tentimes);
                        }
                        if (hdays.Count > 0)
                        {
                            comments = new Phrase("Holiday", tentimes);
                        }
                        if (ddays.Count > 0)
                        {
                            comments = new Phrase("Day Off", tentimes);
                        }
                        if (vdays.Count > 0)
                        {
                            comments = new Phrase("Vacation", tentimes);
                        }
                        var xheaderCell6 = new PdfPCell(comments);


                        var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.AddDays(20).ToShortDateString(), tentimes));

                        var xheaderCell2 = new PdfPCell(new Phrase(day21.Count.ToString(), tentimes));

                        var wh = day21.Where(i => i.ShiftOn == true).ToList();

                        var xheaderCell3 = new PdfPCell(new Phrase(wh.Count.ToString(), tentimes));

                        var xheaderCell4 = new PdfPCell(new Phrase((wh.Count - day21.Count).ToString(), rtentimes));

                        variancetotal = variancetotal + (wh.Count - day21.Count);

                        var ah = day21.Where(i => i.TaskOn == true && i.ShiftOn == true).ToList();

                        var xheaderCell5 = new PdfPCell(new Phrase(ah.Count.ToString(), tentimes));



                        var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                        tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                        tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dutyrosterview.Date.AddDays(20) && i.CreatedDate.Value.Date <= dutyrosterview.Date.AddDays(20).AddHours(23)).ToList();

                        var groupedtevents = tevents.GroupBy(item => item.TaskId);
                        tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                        var ttraveldurationlist = 0.0;
                        var dttocheck = dutyrosterview.Date.AddDays(20);
                        foreach (var t in tevents)
                        {
                            var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                            if (task != null)
                            {
                                var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dttocheck);
                                ttraveldurationlist = tt + ttraveldurationlist;
                            }
                        }


                        var d = CommonUtility.GetUserOTHoursPerDay(dttocheck, dttocheck.AddHours(23), getots);
                        var xheaderCelltt = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                        var xheaderCelld = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));

                        totalOT = totalOT + d;
                        totalTT = totalTT + ttraveldurationlist;

                        xheaderCell.Border = Rectangle.NO_BORDER;
                        xheaderCell2.Border = Rectangle.NO_BORDER;
                        xheaderCell3.Border = Rectangle.NO_BORDER;
                        xheaderCell4.Border = Rectangle.NO_BORDER;
                        xheaderCell5.Border = Rectangle.NO_BORDER;
                        xheaderCell6.Border = Rectangle.NO_BORDER;


                        xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        newchecklistTable.AddCell(xheaderCell);
                        newchecklistTable.AddCell(xheaderCell2);
                        newchecklistTable.AddCell(xheaderCell3);
                        newchecklistTable.AddCell(xheaderCell4);

                        xheaderCelltt.Border = Rectangle.NO_BORDER;
                        xheaderCelld.Border = Rectangle.NO_BORDER;

                        xheaderCelltt.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCelld.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        newchecklistTable.AddCell(xheaderCelltt);
                        newchecklistTable.AddCell(xheaderCelld);

                        newchecklistTable.AddCell(xheaderCell5);
                        newchecklistTable.AddCell(xheaderCell6);
                    }
                    else
                    {
                        var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.AddDays(20).ToShortDateString(), tentimes));
                        var xheaderCell2 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell3 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell4 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell5 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell6 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                        tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                        var dttocheck = dutyrosterview.Date.AddDays(20);

                        tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dttocheck && i.CreatedDate.Value.Date <= dttocheck.AddHours(23)).ToList();

                        var groupedtevents = tevents.GroupBy(item => item.TaskId);
                        tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                        var ttraveldurationlist = 0.0;

                        foreach (var t in tevents)
                        {
                            var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                            if (task != null)
                            {
                                var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dttocheck);
                                ttraveldurationlist = tt + ttraveldurationlist;
                            }
                        }

                        var d = CommonUtility.GetUserOTHoursPerDay(dttocheck, dttocheck.AddHours(23), getots);
                        var xheaderCell7 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                        var xheaderCell8 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));

                        totalOT = totalOT + d;
                        totalTT = totalTT + ttraveldurationlist;

                        xheaderCell.Border = Rectangle.NO_BORDER;
                        xheaderCell2.Border = Rectangle.NO_BORDER;
                        xheaderCell3.Border = Rectangle.NO_BORDER;
                        xheaderCell4.Border = Rectangle.NO_BORDER;
                        xheaderCell5.Border = Rectangle.NO_BORDER;
                        xheaderCell6.Border = Rectangle.NO_BORDER;
                        xheaderCell7.Border = Rectangle.NO_BORDER;
                        xheaderCell8.Border = Rectangle.NO_BORDER;

                        xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell7.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell8.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        newchecklistTable.AddCell(xheaderCell);
                        newchecklistTable.AddCell(xheaderCell2);
                        newchecklistTable.AddCell(xheaderCell3);
                        newchecklistTable.AddCell(xheaderCell4);
                        newchecklistTable.AddCell(xheaderCell7);
                        newchecklistTable.AddCell(xheaderCell8);
                        newchecklistTable.AddCell(xheaderCell5);
                        newchecklistTable.AddCell(xheaderCell6);
                    }

                    if (day22.Count > 0)
                    {

                        var sdays = day22.Where(i => i.DutyPost == "SICK DAY").ToList();
                        sickdays = sickdays + sdays.Count;
                        var hdays = day22.Where(i => i.DutyPost == "HOLIDAY").ToList();
                        holidays = holidays + hdays.Count;
                        var ddays = day22.Where(i => i.DutyPost == "DAY OFF").ToList();
                        dayoffs = dayoffs + ddays.Count;
                        var vdays = day22.Where(i => i.DutyPost == "VACATION").ToList();
                        vacations = vacations + vdays.Count;

                        day22 = day22.Where(i => i.DutyPost != "DAY OFF" && i.DutyPost != "HOLIDAY" && i.DutyPost != "SICK DAY" && i.DutyPost != "VACATION").ToList();

                        var comments = new Phrase(1, "\u00a0");


                        if (sdays.Count > 0)
                        {
                            comments = new Phrase("Sick Day", tentimes);
                        }
                        if (hdays.Count > 0)
                        {
                            comments = new Phrase("Holiday", tentimes);
                        }
                        if (ddays.Count > 0)
                        {
                            comments = new Phrase("Day Off", tentimes);
                        }
                        if (vdays.Count > 0)
                        {
                            comments = new Phrase("Vacation", tentimes);
                        }
                        var xheaderCell6 = new PdfPCell(comments);

                        var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.AddDays(21).ToShortDateString(), tentimes));

                        var xheaderCell2 = new PdfPCell(new Phrase(day22.Count.ToString(), tentimes));

                        var wh = day22.Where(i => i.ShiftOn == true).ToList();

                        var xheaderCell3 = new PdfPCell(new Phrase(wh.Count.ToString(), tentimes));

                        var xheaderCell4 = new PdfPCell(new Phrase((wh.Count - day22.Count).ToString(), rtentimes));

                        variancetotal = variancetotal + (wh.Count - day22.Count);

                        var ah = day22.Where(i => i.TaskOn == true && i.ShiftOn == true).ToList();

                        var xheaderCell5 = new PdfPCell(new Phrase(ah.Count.ToString(), tentimes));



                        var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                        tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                        tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dutyrosterview.Date.AddDays(21) && i.CreatedDate.Value.Date <= dutyrosterview.Date.AddDays(21).AddHours(23)).ToList();

                        var groupedtevents = tevents.GroupBy(item => item.TaskId);
                        tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                        var ttraveldurationlist = 0.0;
                        var dttocheck = dutyrosterview.Date.AddDays(21);
                        foreach (var t in tevents)
                        {
                            var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                            if (task != null)
                            {
                                var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dttocheck);
                                ttraveldurationlist = tt + ttraveldurationlist;
                            }
                        }

                        var d = CommonUtility.GetUserOTHoursPerDay(dutyrosterview.Date.AddDays(21), dutyrosterview.Date.AddDays(21).AddHours(23), getots);

                        var xheaderCelltt = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                        var xheaderCelld = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));

                        totalOT = totalOT + d;
                        totalTT = totalTT + ttraveldurationlist;

                        xheaderCell.Border = Rectangle.NO_BORDER;
                        xheaderCell2.Border = Rectangle.NO_BORDER;
                        xheaderCell3.Border = Rectangle.NO_BORDER;
                        xheaderCell4.Border = Rectangle.NO_BORDER;
                        xheaderCell5.Border = Rectangle.NO_BORDER;
                        xheaderCell6.Border = Rectangle.NO_BORDER;


                        xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        xheaderCell.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell2.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell3.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell4.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell5.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell6.BackgroundColor = Color.LIGHT_GRAY;

                        newchecklistTable.AddCell(xheaderCell);
                        newchecklistTable.AddCell(xheaderCell2);
                        newchecklistTable.AddCell(xheaderCell3);
                        newchecklistTable.AddCell(xheaderCell4);

                        xheaderCelltt.Border = Rectangle.NO_BORDER;
                        xheaderCelld.Border = Rectangle.NO_BORDER;

                        xheaderCelltt.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCelld.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        xheaderCelltt.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCelld.BackgroundColor = Color.LIGHT_GRAY;

                        newchecklistTable.AddCell(xheaderCelltt);
                        newchecklistTable.AddCell(xheaderCelld);

                        newchecklistTable.AddCell(xheaderCell5);
                        newchecklistTable.AddCell(xheaderCell6);
                    }
                    else
                    {
                        var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.AddDays(21).ToShortDateString(), tentimes));
                        var xheaderCell2 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell3 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell4 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell5 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell6 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                        tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                        var dttocheck = dutyrosterview.Date.AddDays(21);

                        tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dttocheck && i.CreatedDate.Value.Date <= dttocheck.AddHours(23)).ToList();

                        var groupedtevents = tevents.GroupBy(item => item.TaskId);
                        tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                        var ttraveldurationlist = 0.0;

                        foreach (var t in tevents)
                        {
                            var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                            if (task != null)
                            {
                                var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dttocheck);
                                ttraveldurationlist = tt + ttraveldurationlist;
                            }
                        }

                        var d = CommonUtility.GetUserOTHoursPerDay(dttocheck, dttocheck.AddHours(23), getots);

                        var xheaderCell7 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                        var xheaderCell8 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));

                        totalOT = totalOT + d;
                        totalTT = totalTT + ttraveldurationlist;

                        xheaderCell.Border = Rectangle.NO_BORDER;
                        xheaderCell2.Border = Rectangle.NO_BORDER;
                        xheaderCell3.Border = Rectangle.NO_BORDER;
                        xheaderCell4.Border = Rectangle.NO_BORDER;
                        xheaderCell5.Border = Rectangle.NO_BORDER;
                        xheaderCell6.Border = Rectangle.NO_BORDER;
                        xheaderCell7.Border = Rectangle.NO_BORDER;
                        xheaderCell8.Border = Rectangle.NO_BORDER;

                        xheaderCell.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell2.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell3.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell4.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell5.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell6.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell7.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell8.BackgroundColor = Color.LIGHT_GRAY;

                        xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell7.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell8.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        newchecklistTable.AddCell(xheaderCell);
                        newchecklistTable.AddCell(xheaderCell2);
                        newchecklistTable.AddCell(xheaderCell3);
                        newchecklistTable.AddCell(xheaderCell4);
                        newchecklistTable.AddCell(xheaderCell7);
                        newchecklistTable.AddCell(xheaderCell8);
                        newchecklistTable.AddCell(xheaderCell5);
                        newchecklistTable.AddCell(xheaderCell6);
                    }

                    if (day23.Count > 0)
                    {


                        var sdays = day23.Where(i => i.DutyPost == "SICK DAY").ToList();
                        sickdays = sickdays + sdays.Count;
                        var hdays = day23.Where(i => i.DutyPost == "HOLIDAY").ToList();
                        holidays = holidays + hdays.Count;
                        var ddays = day23.Where(i => i.DutyPost == "DAY OFF").ToList();
                        dayoffs = dayoffs + ddays.Count;
                        var vdays = day23.Where(i => i.DutyPost == "VACATION").ToList();
                        vacations = vacations + vdays.Count;

                        day23 = day23.Where(i => i.DutyPost != "DAY OFF" && i.DutyPost != "HOLIDAY" && i.DutyPost != "SICK DAY" && i.DutyPost != "VACATION").ToList();

                        var comments = new Phrase(1, "\u00a0");

                        if (sdays.Count > 0)
                        {
                            comments = new Phrase("Sick Day", tentimes);
                        }
                        if (hdays.Count > 0)
                        {
                            comments = new Phrase("Holiday", tentimes);
                        }
                        if (ddays.Count > 0)
                        {
                            comments = new Phrase("Day Off", tentimes);
                        }
                        if (vdays.Count > 0)
                        {
                            comments = new Phrase("Vacation", tentimes);
                        }
                        var xheaderCell6 = new PdfPCell(comments);


                        var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.AddDays(22).ToShortDateString(), tentimes));

                        var xheaderCell2 = new PdfPCell(new Phrase(day23.Count.ToString(), tentimes));

                        var wh = day23.Where(i => i.ShiftOn == true).ToList();

                        var xheaderCell3 = new PdfPCell(new Phrase(wh.Count.ToString(), tentimes));

                        var xheaderCell4 = new PdfPCell(new Phrase((wh.Count - day23.Count).ToString(), rtentimes));

                        variancetotal = variancetotal + (wh.Count - day23.Count);

                        var ah = day23.Where(i => i.TaskOn == true && i.ShiftOn == true).ToList();

                        var xheaderCell5 = new PdfPCell(new Phrase(ah.Count.ToString(), tentimes));


                        var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                        tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                        tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dutyrosterview.Date.AddDays(22) && i.CreatedDate.Value.Date <= dutyrosterview.Date.AddDays(22).AddHours(23)).ToList();

                        var groupedtevents = tevents.GroupBy(item => item.TaskId);
                        tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                        var ttraveldurationlist = 0.0;
                        var dttocheck = dutyrosterview.Date.AddDays(22);
                        foreach (var t in tevents)
                        {
                            var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                            if (task != null)
                            {
                                var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dttocheck);
                                ttraveldurationlist = tt + ttraveldurationlist;
                            }
                        }


                        var d = CommonUtility.GetUserOTHoursPerDay(dttocheck, dttocheck.AddHours(23), getots);

                        var xheaderCelltt = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                        var xheaderCelld = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));

                        totalOT = totalOT + d;
                        totalTT = totalTT + ttraveldurationlist;

                        xheaderCell.Border = Rectangle.NO_BORDER;
                        xheaderCell2.Border = Rectangle.NO_BORDER;
                        xheaderCell3.Border = Rectangle.NO_BORDER;
                        xheaderCell4.Border = Rectangle.NO_BORDER;
                        xheaderCell5.Border = Rectangle.NO_BORDER;
                        xheaderCell6.Border = Rectangle.NO_BORDER;


                        xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        newchecklistTable.AddCell(xheaderCell);
                        newchecklistTable.AddCell(xheaderCell2);
                        newchecklistTable.AddCell(xheaderCell3);
                        newchecklistTable.AddCell(xheaderCell4);

                        xheaderCelltt.Border = Rectangle.NO_BORDER;
                        xheaderCelld.Border = Rectangle.NO_BORDER;

                        xheaderCelltt.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCelld.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        newchecklistTable.AddCell(xheaderCelltt);
                        newchecklistTable.AddCell(xheaderCelld);

                        newchecklistTable.AddCell(xheaderCell5);
                        newchecklistTable.AddCell(xheaderCell6);
                    }
                    else
                    {
                        var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.AddDays(22).ToShortDateString(), tentimes));
                        var xheaderCell2 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell3 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell4 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell5 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell6 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                        tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                        var dttocheck = dutyrosterview.Date.AddDays(22);

                        tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dttocheck && i.CreatedDate.Value.Date <= dttocheck.AddHours(23)).ToList();

                        var groupedtevents = tevents.GroupBy(item => item.TaskId);
                        tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                        var ttraveldurationlist = 0.0;

                        foreach (var t in tevents)
                        {
                            var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                            if (task != null)
                            {
                                var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dttocheck);
                                ttraveldurationlist = tt + ttraveldurationlist;
                            }
                        }

                        // var d = CommonUtility.GetUserOTHoursForEmployeeReport(employee.ID, dttocheck, dttocheck.AddHours(23));
                        var d = CommonUtility.GetUserOTHoursPerDay(dttocheck, dttocheck.AddHours(23), getots);
                        var xheaderCell7 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                        var xheaderCell8 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));

                        totalOT = totalOT + d;
                        totalTT = totalTT + ttraveldurationlist;

                        xheaderCell.Border = Rectangle.NO_BORDER;
                        xheaderCell2.Border = Rectangle.NO_BORDER;
                        xheaderCell3.Border = Rectangle.NO_BORDER;
                        xheaderCell4.Border = Rectangle.NO_BORDER;
                        xheaderCell5.Border = Rectangle.NO_BORDER;
                        xheaderCell6.Border = Rectangle.NO_BORDER;
                        xheaderCell7.Border = Rectangle.NO_BORDER;
                        xheaderCell8.Border = Rectangle.NO_BORDER;

                        xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell7.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell8.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        newchecklistTable.AddCell(xheaderCell);
                        newchecklistTable.AddCell(xheaderCell2);
                        newchecklistTable.AddCell(xheaderCell3);
                        newchecklistTable.AddCell(xheaderCell4);
                        newchecklistTable.AddCell(xheaderCell7);
                        newchecklistTable.AddCell(xheaderCell8);
                        newchecklistTable.AddCell(xheaderCell5);
                        newchecklistTable.AddCell(xheaderCell6);
                    }

                    if (day24.Count > 0)
                    {

                        var sdays = day24.Where(i => i.DutyPost == "SICK DAY").ToList();
                        sickdays = sickdays + sdays.Count;
                        var hdays = day24.Where(i => i.DutyPost == "HOLIDAY").ToList();
                        holidays = holidays + hdays.Count;
                        var ddays = day24.Where(i => i.DutyPost == "DAY OFF").ToList();
                        dayoffs = dayoffs + ddays.Count;
                        var vdays = day24.Where(i => i.DutyPost == "VACATION").ToList();
                        vacations = vacations + vdays.Count;

                        day24 = day24.Where(i => i.DutyPost != "DAY OFF" && i.DutyPost != "HOLIDAY" && i.DutyPost != "SICK DAY" && i.DutyPost != "VACATION").ToList();

                        var comments = new Phrase(1, "\u00a0");


                        if (sdays.Count > 0)
                        {
                            comments = new Phrase("Sick Day", tentimes);
                        }
                        if (hdays.Count > 0)
                        {
                            comments = new Phrase("Holiday", tentimes);
                        }
                        if (ddays.Count > 0)
                        {
                            comments = new Phrase("Day Off", tentimes);
                        }
                        if (vdays.Count > 0)
                        {
                            comments = new Phrase("Vacation", tentimes);
                        }
                        var xheaderCell6 = new PdfPCell(comments);


                        var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.AddDays(23).ToShortDateString(), tentimes));

                        var xheaderCell2 = new PdfPCell(new Phrase(day24.Count.ToString(), tentimes));

                        var wh = day24.Where(i => i.ShiftOn == true).ToList();

                        var xheaderCell3 = new PdfPCell(new Phrase(wh.Count.ToString(), tentimes));

                        var xheaderCell4 = new PdfPCell(new Phrase((wh.Count - day24.Count).ToString(), rtentimes));

                        variancetotal = variancetotal + (wh.Count - day24.Count);

                        var ah = day24.Where(i => i.TaskOn == true && i.ShiftOn == true).ToList();

                        var xheaderCell5 = new PdfPCell(new Phrase(ah.Count.ToString(), tentimes));



                        var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                        tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                        tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dutyrosterview.Date.AddDays(23) && i.CreatedDate.Value.Date <= dutyrosterview.Date.AddDays(23).AddHours(23)).ToList();

                        var groupedtevents = tevents.GroupBy(item => item.TaskId);
                        tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                        var ttraveldurationlist = 0.0;
                        var dttocheck = dutyrosterview.Date.AddDays(23);
                        foreach (var t in tevents)
                        {
                            var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                            if (task != null)
                            {
                                var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dttocheck);
                                ttraveldurationlist = tt + ttraveldurationlist;
                            }
                        }


                        var d = CommonUtility.GetUserOTHoursPerDay(dttocheck, dttocheck.AddHours(23), getots);
                        var xheaderCelltt = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                        var xheaderCelld = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));

                        totalOT = totalOT + d;
                        totalTT = totalTT + ttraveldurationlist;

                        xheaderCell.Border = Rectangle.NO_BORDER;
                        xheaderCell2.Border = Rectangle.NO_BORDER;
                        xheaderCell3.Border = Rectangle.NO_BORDER;
                        xheaderCell4.Border = Rectangle.NO_BORDER;
                        xheaderCell5.Border = Rectangle.NO_BORDER;
                        xheaderCell6.Border = Rectangle.NO_BORDER;

                        xheaderCell.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell2.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell3.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell4.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell5.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell6.BackgroundColor = Color.LIGHT_GRAY;


                        xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        newchecklistTable.AddCell(xheaderCell);
                        newchecklistTable.AddCell(xheaderCell2);
                        newchecklistTable.AddCell(xheaderCell3);
                        newchecklistTable.AddCell(xheaderCell4);

                        xheaderCelltt.Border = Rectangle.NO_BORDER;
                        xheaderCelld.Border = Rectangle.NO_BORDER;

                        xheaderCelltt.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCelld.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        xheaderCelltt.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCelld.BackgroundColor = Color.LIGHT_GRAY;

                        newchecklistTable.AddCell(xheaderCelltt);
                        newchecklistTable.AddCell(xheaderCelld);

                        newchecklistTable.AddCell(xheaderCell5);
                        newchecklistTable.AddCell(xheaderCell6);
                    }
                    else
                    {
                        var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.AddDays(23).ToShortDateString(), tentimes));
                        var xheaderCell2 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell3 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell4 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell5 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell6 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                        tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                        var dttocheck = dutyrosterview.Date.AddDays(23);

                        tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dttocheck && i.CreatedDate.Value.Date <= dttocheck.AddHours(23)).ToList();

                        var groupedtevents = tevents.GroupBy(item => item.TaskId);
                        tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                        var ttraveldurationlist = 0.0;

                        foreach (var t in tevents)
                        {
                            var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                            if (task != null)
                            {
                                var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dttocheck);
                                ttraveldurationlist = tt + ttraveldurationlist;
                            }
                        }

                        var d = CommonUtility.GetUserOTHoursPerDay(dttocheck, dttocheck.AddHours(23), getots);
                        var xheaderCell7 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                        var xheaderCell8 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));

                        totalOT = totalOT + d;
                        totalTT = totalTT + ttraveldurationlist;

                        xheaderCell.Border = Rectangle.NO_BORDER;
                        xheaderCell2.Border = Rectangle.NO_BORDER;
                        xheaderCell3.Border = Rectangle.NO_BORDER;
                        xheaderCell4.Border = Rectangle.NO_BORDER;
                        xheaderCell5.Border = Rectangle.NO_BORDER;
                        xheaderCell6.Border = Rectangle.NO_BORDER;
                        xheaderCell7.Border = Rectangle.NO_BORDER;
                        xheaderCell8.Border = Rectangle.NO_BORDER;

                        xheaderCell.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell2.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell3.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell4.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell5.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell6.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell7.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell8.BackgroundColor = Color.LIGHT_GRAY;

                        xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell7.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell8.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        newchecklistTable.AddCell(xheaderCell);
                        newchecklistTable.AddCell(xheaderCell2);
                        newchecklistTable.AddCell(xheaderCell3);
                        newchecklistTable.AddCell(xheaderCell4);
                        newchecklistTable.AddCell(xheaderCell7);
                        newchecklistTable.AddCell(xheaderCell8);
                        newchecklistTable.AddCell(xheaderCell5);
                        newchecklistTable.AddCell(xheaderCell6);
                    }

                    if (day25.Count > 0)
                    {

                        var sdays = day25.Where(i => i.DutyPost == "SICK DAY").ToList();
                        sickdays = sickdays + sdays.Count;
                        var hdays = day25.Where(i => i.DutyPost == "HOLIDAY").ToList();
                        holidays = holidays + hdays.Count;
                        var ddays = day25.Where(i => i.DutyPost == "DAY OFF").ToList();
                        dayoffs = dayoffs + ddays.Count;
                        var vdays = day25.Where(i => i.DutyPost == "VACATION").ToList();
                        vacations = vacations + vdays.Count;

                        day25 = day25.Where(i => i.DutyPost != "DAY OFF" && i.DutyPost != "HOLIDAY" && i.DutyPost != "SICK DAY" && i.DutyPost != "VACATION").ToList();

                        var comments = new Phrase(1, "\u00a0");


                        if (sdays.Count > 0)
                        {
                            comments = new Phrase("Sick Day", tentimes);
                        }
                        if (hdays.Count > 0)
                        {
                            comments = new Phrase("Holiday", tentimes);
                        }
                        if (ddays.Count > 0)
                        {
                            comments = new Phrase("Day Off", tentimes);
                        }
                        if (vdays.Count > 0)
                        {
                            comments = new Phrase("Vacation", tentimes);
                        }
                        var xheaderCell6 = new PdfPCell(comments);


                        var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.AddDays(24).ToShortDateString(), tentimes));

                        var xheaderCell2 = new PdfPCell(new Phrase(day25.Count.ToString(), tentimes));

                        var wh = day25.Where(i => i.ShiftOn == true).ToList();

                        var xheaderCell3 = new PdfPCell(new Phrase(wh.Count.ToString(), tentimes));

                        var xheaderCell4 = new PdfPCell(new Phrase((wh.Count - day25.Count).ToString(), rtentimes));

                        variancetotal = variancetotal + (wh.Count - day25.Count);

                        var ah = day25.Where(i => i.TaskOn == true && i.ShiftOn == true).ToList();

                        var xheaderCell5 = new PdfPCell(new Phrase(ah.Count.ToString(), tentimes));


                        var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                        tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                        tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dutyrosterview.Date.AddDays(24) && i.CreatedDate.Value.Date <= dutyrosterview.Date.AddDays(24).AddHours(23)).ToList();

                        var groupedtevents = tevents.GroupBy(item => item.TaskId);
                        tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                        var ttraveldurationlist = 0.0;
                        var dttocheck = dutyrosterview.Date.AddDays(24);
                        foreach (var t in tevents)
                        {
                            var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                            if (task != null)
                            {
                                var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dttocheck);
                                ttraveldurationlist = tt + ttraveldurationlist;
                            }
                        }


                        var d = CommonUtility.GetUserOTHoursPerDay(dttocheck, dttocheck.AddHours(23), getots);
                        var xheaderCelltt = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                        var xheaderCelld = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));

                        totalOT = totalOT + d;
                        totalTT = totalTT + ttraveldurationlist;

                        xheaderCell.Border = Rectangle.NO_BORDER;
                        xheaderCell2.Border = Rectangle.NO_BORDER;
                        xheaderCell3.Border = Rectangle.NO_BORDER;
                        xheaderCell4.Border = Rectangle.NO_BORDER;
                        xheaderCell5.Border = Rectangle.NO_BORDER;
                        xheaderCell6.Border = Rectangle.NO_BORDER;


                        xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        newchecklistTable.AddCell(xheaderCell);
                        newchecklistTable.AddCell(xheaderCell2);
                        newchecklistTable.AddCell(xheaderCell3);
                        newchecklistTable.AddCell(xheaderCell4);

                        xheaderCelltt.Border = Rectangle.NO_BORDER;
                        xheaderCelld.Border = Rectangle.NO_BORDER;

                        xheaderCelltt.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCelld.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;


                        newchecklistTable.AddCell(xheaderCelltt);
                        newchecklistTable.AddCell(xheaderCelld);

                        newchecklistTable.AddCell(xheaderCell5);
                        newchecklistTable.AddCell(xheaderCell6);
                    }
                    else
                    {
                        var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.AddDays(24).ToShortDateString(), tentimes));
                        var xheaderCell2 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell3 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell4 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell5 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell6 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                        tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                        var dttocheck = dutyrosterview.Date.AddDays(24);

                        tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dttocheck && i.CreatedDate.Value.Date <= dttocheck.AddHours(23)).ToList();

                        var groupedtevents = tevents.GroupBy(item => item.TaskId);
                        tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                        var ttraveldurationlist = 0.0;

                        foreach (var t in tevents)
                        {
                            var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                            if (task != null)
                            {
                                var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dttocheck);
                                ttraveldurationlist = tt + ttraveldurationlist;
                            }
                        }

                        var d = CommonUtility.GetUserOTHoursPerDay(dttocheck, dttocheck.AddHours(23), getots);

                        var xheaderCell7 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                        var xheaderCell8 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));

                        totalOT = totalOT + d;
                        totalTT = totalTT + ttraveldurationlist;

                        xheaderCell.Border = Rectangle.NO_BORDER;
                        xheaderCell2.Border = Rectangle.NO_BORDER;
                        xheaderCell3.Border = Rectangle.NO_BORDER;
                        xheaderCell4.Border = Rectangle.NO_BORDER;
                        xheaderCell5.Border = Rectangle.NO_BORDER;
                        xheaderCell6.Border = Rectangle.NO_BORDER;
                        xheaderCell7.Border = Rectangle.NO_BORDER;
                        xheaderCell8.Border = Rectangle.NO_BORDER;

                        xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell7.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell8.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        newchecklistTable.AddCell(xheaderCell);
                        newchecklistTable.AddCell(xheaderCell2);
                        newchecklistTable.AddCell(xheaderCell3);
                        newchecklistTable.AddCell(xheaderCell4);
                        newchecklistTable.AddCell(xheaderCell7);
                        newchecklistTable.AddCell(xheaderCell8);
                        newchecklistTable.AddCell(xheaderCell5);
                        newchecklistTable.AddCell(xheaderCell6);
                    }

                    if (day26.Count > 0)
                    {

                        var sdays = day26.Where(i => i.DutyPost == "SICK DAY").ToList();
                        sickdays = sickdays + sdays.Count;
                        var hdays = day26.Where(i => i.DutyPost == "HOLIDAY").ToList();
                        holidays = holidays + hdays.Count;
                        var ddays = day26.Where(i => i.DutyPost == "DAY OFF").ToList();
                        dayoffs = dayoffs + ddays.Count;
                        var vdays = day26.Where(i => i.DutyPost == "VACATION").ToList();
                        vacations = vacations + vdays.Count;

                        day26 = day26.Where(i => i.DutyPost != "DAY OFF" && i.DutyPost != "HOLIDAY" && i.DutyPost != "SICK DAY" && i.DutyPost != "VACATION").ToList();

                        var comments = new Phrase(1, "\u00a0");


                        if (sdays.Count > 0)
                        {
                            comments = new Phrase("Sick Day", tentimes);
                        }
                        if (hdays.Count > 0)
                        {
                            comments = new Phrase("Holiday", tentimes);
                        }
                        if (ddays.Count > 0)
                        {
                            comments = new Phrase("Day Off", tentimes);
                        }
                        if (vdays.Count > 0)
                        {
                            comments = new Phrase("Vacation", tentimes);
                        }
                        var xheaderCell6 = new PdfPCell(comments);

                        var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.AddDays(25).ToShortDateString(), tentimes));

                        var xheaderCell2 = new PdfPCell(new Phrase(day26.Count.ToString(), tentimes));

                        var wh = day26.Where(i => i.ShiftOn == true).ToList();

                        var xheaderCell3 = new PdfPCell(new Phrase(wh.Count.ToString(), tentimes));

                        var xheaderCell4 = new PdfPCell(new Phrase((wh.Count - day26.Count).ToString(), rtentimes));

                        variancetotal = variancetotal + (wh.Count - day26.Count);

                        var ah = day26.Where(i => i.TaskOn == true && i.ShiftOn == true).ToList();

                        var xheaderCell5 = new PdfPCell(new Phrase(ah.Count.ToString(), tentimes));



                        var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                        tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                        tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dutyrosterview.Date.AddDays(25) && i.CreatedDate.Value.Date <= dutyrosterview.Date.AddDays(25).AddHours(23)).ToList();

                        var groupedtevents = tevents.GroupBy(item => item.TaskId);
                        tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                        var ttraveldurationlist = 0.0;
                        var dttocheck = dutyrosterview.Date.AddDays(25);
                        foreach (var t in tevents)
                        {
                            var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                            if (task != null)
                            {
                                var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dttocheck);
                                ttraveldurationlist = tt + ttraveldurationlist;
                            }
                        }


                        var d = CommonUtility.GetUserOTHoursPerDay(dttocheck, dttocheck.AddHours(23), getots);
                        var xheaderCelltt = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                        var xheaderCelld = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));

                        totalOT = totalOT + d;
                        totalTT = totalTT + ttraveldurationlist;

                        xheaderCell.Border = Rectangle.NO_BORDER;
                        xheaderCell2.Border = Rectangle.NO_BORDER;
                        xheaderCell3.Border = Rectangle.NO_BORDER;
                        xheaderCell4.Border = Rectangle.NO_BORDER;
                        xheaderCell5.Border = Rectangle.NO_BORDER;
                        xheaderCell6.Border = Rectangle.NO_BORDER;


                        xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        xheaderCell.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell2.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell3.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell4.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell5.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell6.BackgroundColor = Color.LIGHT_GRAY;

                        newchecklistTable.AddCell(xheaderCell);
                        newchecklistTable.AddCell(xheaderCell2);
                        newchecklistTable.AddCell(xheaderCell3);
                        newchecklistTable.AddCell(xheaderCell4);

                        xheaderCelltt.Border = Rectangle.NO_BORDER;
                        xheaderCelld.Border = Rectangle.NO_BORDER;

                        xheaderCelltt.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCelld.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        xheaderCelltt.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCelld.BackgroundColor = Color.LIGHT_GRAY;

                        newchecklistTable.AddCell(xheaderCelltt);
                        newchecklistTable.AddCell(xheaderCelld);

                        newchecklistTable.AddCell(xheaderCell5);
                        newchecklistTable.AddCell(xheaderCell6);
                    }
                    else
                    {
                        var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.AddDays(25).ToShortDateString(), tentimes));
                        var xheaderCell2 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell3 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell4 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell5 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell6 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                        tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                        var dttocheck = dutyrosterview.Date.AddDays(25);

                        tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dttocheck && i.CreatedDate.Value.Date <= dttocheck.AddHours(23)).ToList();

                        var groupedtevents = tevents.GroupBy(item => item.TaskId);
                        tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                        var ttraveldurationlist = 0.0;

                        foreach (var t in tevents)
                        {
                            var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                            if (task != null)
                            {
                                var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dttocheck);
                                ttraveldurationlist = tt + ttraveldurationlist;
                            }
                        }

                        var d = CommonUtility.GetUserOTHoursPerDay(dttocheck, dttocheck.AddHours(23), getots);
                        var xheaderCell7 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                        var xheaderCell8 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));

                        totalOT = totalOT + d;
                        totalTT = totalTT + ttraveldurationlist;

                        xheaderCell.Border = Rectangle.NO_BORDER;
                        xheaderCell2.Border = Rectangle.NO_BORDER;
                        xheaderCell3.Border = Rectangle.NO_BORDER;
                        xheaderCell4.Border = Rectangle.NO_BORDER;
                        xheaderCell5.Border = Rectangle.NO_BORDER;
                        xheaderCell6.Border = Rectangle.NO_BORDER;
                        xheaderCell7.Border = Rectangle.NO_BORDER;
                        xheaderCell8.Border = Rectangle.NO_BORDER;

                        xheaderCell.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell2.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell3.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell4.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell5.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell6.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell7.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell8.BackgroundColor = Color.LIGHT_GRAY;

                        xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell7.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell8.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        newchecklistTable.AddCell(xheaderCell);
                        newchecklistTable.AddCell(xheaderCell2);
                        newchecklistTable.AddCell(xheaderCell3);
                        newchecklistTable.AddCell(xheaderCell4);
                        newchecklistTable.AddCell(xheaderCell7);
                        newchecklistTable.AddCell(xheaderCell8);
                        newchecklistTable.AddCell(xheaderCell5);
                        newchecklistTable.AddCell(xheaderCell6);
                    }

                    if (day27.Count > 0)
                    {
                        var sdays = day27.Where(i => i.DutyPost == "SICK DAY").ToList();
                        sickdays = sickdays + sdays.Count;
                        var hdays = day27.Where(i => i.DutyPost == "HOLIDAY").ToList();
                        holidays = holidays + hdays.Count;
                        var ddays = day27.Where(i => i.DutyPost == "DAY OFF").ToList();
                        dayoffs = dayoffs + ddays.Count;
                        var vdays = day27.Where(i => i.DutyPost == "VACATION").ToList();
                        vacations = vacations + vdays.Count;

                        day27 = day27.Where(i => i.DutyPost != "DAY OFF" && i.DutyPost != "HOLIDAY" && i.DutyPost != "SICK DAY" && i.DutyPost != "VACATION").ToList();

                        var comments = new Phrase(1, "\u00a0");


                        if (sdays.Count > 0)
                        {
                            comments = new Phrase("Sick Day", tentimes);
                        }
                        if (hdays.Count > 0)
                        {
                            comments = new Phrase("Holiday", tentimes);
                        }
                        if (ddays.Count > 0)
                        {
                            comments = new Phrase("Day Off", tentimes);
                        }
                        if (vdays.Count > 0)
                        {
                            comments = new Phrase("Vacation", tentimes);
                        }
                        var xheaderCell6 = new PdfPCell(comments);

                        var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.AddDays(26).ToShortDateString(), tentimes));

                        var xheaderCell2 = new PdfPCell(new Phrase(day27.Count.ToString(), tentimes));

                        var wh = day27.Where(i => i.ShiftOn == true).ToList();

                        var xheaderCell3 = new PdfPCell(new Phrase(wh.Count.ToString(), tentimes));

                        var xheaderCell4 = new PdfPCell(new Phrase((wh.Count - day27.Count).ToString(), rtentimes));

                        variancetotal = variancetotal + (wh.Count - day27.Count);

                        var ah = day27.Where(i => i.TaskOn == true && i.ShiftOn == true).ToList();

                        var xheaderCell5 = new PdfPCell(new Phrase(ah.Count.ToString(), tentimes));



                        var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                        tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                        tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dutyrosterview.Date.AddDays(26) && i.CreatedDate.Value.Date <= dutyrosterview.Date.AddDays(26).AddHours(23)).ToList();

                        var groupedtevents = tevents.GroupBy(item => item.TaskId);
                        tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                        var ttraveldurationlist = 0.0;
                        var dttocheck = dutyrosterview.Date.AddDays(26);
                        foreach (var t in tevents)
                        {
                            var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                            if (task != null)
                            {
                                var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dttocheck);
                                ttraveldurationlist = tt + ttraveldurationlist;
                            }
                        }


                        var d = CommonUtility.GetUserOTHoursPerDay(dttocheck, dttocheck.AddHours(23), getots);
                        var xheaderCelltt = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                        var xheaderCelld = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));

                        totalOT = totalOT + d;
                        totalTT = totalTT + ttraveldurationlist;

                        xheaderCell.Border = Rectangle.NO_BORDER;
                        xheaderCell2.Border = Rectangle.NO_BORDER;
                        xheaderCell3.Border = Rectangle.NO_BORDER;
                        xheaderCell4.Border = Rectangle.NO_BORDER;
                        xheaderCell5.Border = Rectangle.NO_BORDER;
                        xheaderCell6.Border = Rectangle.NO_BORDER;


                        xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        newchecklistTable.AddCell(xheaderCell);
                        newchecklistTable.AddCell(xheaderCell2);
                        newchecklistTable.AddCell(xheaderCell3);
                        newchecklistTable.AddCell(xheaderCell4);

                        xheaderCelltt.Border = Rectangle.NO_BORDER;
                        xheaderCelld.Border = Rectangle.NO_BORDER;

                        xheaderCelltt.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCelld.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        newchecklistTable.AddCell(xheaderCelltt);
                        newchecklistTable.AddCell(xheaderCelld);

                        newchecklistTable.AddCell(xheaderCell5);
                        newchecklistTable.AddCell(xheaderCell6);
                    }
                    else
                    {
                        var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.AddDays(26).ToShortDateString(), tentimes));
                        var xheaderCell2 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell3 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell4 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell5 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell6 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                        tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                        var dttocheck = dutyrosterview.Date.AddDays(26);

                        tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dttocheck && i.CreatedDate.Value.Date <= dttocheck.AddHours(23)).ToList();

                        var groupedtevents = tevents.GroupBy(item => item.TaskId);
                        tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                        var ttraveldurationlist = 0.0;

                        foreach (var t in tevents)
                        {
                            var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                            if (task != null)
                            {
                                var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dttocheck);
                                ttraveldurationlist = tt + ttraveldurationlist;
                            }
                        }

                        var d = CommonUtility.GetUserOTHoursPerDay(dttocheck, dttocheck.AddHours(23), getots);
                        var xheaderCell7 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                        var xheaderCell8 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));

                        totalOT = totalOT + d;
                        totalTT = totalTT + ttraveldurationlist;

                        xheaderCell.Border = Rectangle.NO_BORDER;
                        xheaderCell2.Border = Rectangle.NO_BORDER;
                        xheaderCell3.Border = Rectangle.NO_BORDER;
                        xheaderCell4.Border = Rectangle.NO_BORDER;
                        xheaderCell5.Border = Rectangle.NO_BORDER;
                        xheaderCell6.Border = Rectangle.NO_BORDER;
                        xheaderCell7.Border = Rectangle.NO_BORDER;
                        xheaderCell8.Border = Rectangle.NO_BORDER;

                        xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell7.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell8.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        newchecklistTable.AddCell(xheaderCell);
                        newchecklistTable.AddCell(xheaderCell2);
                        newchecklistTable.AddCell(xheaderCell3);
                        newchecklistTable.AddCell(xheaderCell4);
                        newchecklistTable.AddCell(xheaderCell7);
                        newchecklistTable.AddCell(xheaderCell8);
                        newchecklistTable.AddCell(xheaderCell5);
                        newchecklistTable.AddCell(xheaderCell6);
                    }

                    if (day28.Count > 0)
                    {

                        var sdays = day28.Where(i => i.DutyPost == "SICK DAY").ToList();
                        sickdays = sickdays + sdays.Count;
                        var hdays = day28.Where(i => i.DutyPost == "HOLIDAY").ToList();
                        holidays = holidays + hdays.Count;
                        var ddays = day28.Where(i => i.DutyPost == "DAY OFF").ToList();
                        dayoffs = dayoffs + ddays.Count;
                        var vdays = day28.Where(i => i.DutyPost == "VACATION").ToList();
                        vacations = vacations + vdays.Count;

                        day28 = day28.Where(i => i.DutyPost != "DAY OFF" && i.DutyPost != "HOLIDAY" && i.DutyPost != "SICK DAY" && i.DutyPost != "VACATION").ToList();

                        var comments = new Phrase(1, "\u00a0");


                        if (sdays.Count > 0)
                        {
                            comments = new Phrase("Sick Day", tentimes);
                        }
                        if (hdays.Count > 0)
                        {
                            comments = new Phrase("Holiday", tentimes);
                        }
                        if (ddays.Count > 0)
                        {
                            comments = new Phrase("Day Off", tentimes);
                        }
                        if (vdays.Count > 0)
                        {
                            comments = new Phrase("Vacation", tentimes);
                        }
                        var xheaderCell6 = new PdfPCell(comments);


                        var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.AddDays(27).ToShortDateString(), tentimes));

                        var xheaderCell2 = new PdfPCell(new Phrase(day28.Count.ToString(), tentimes));

                        var wh = day28.Where(i => i.ShiftOn == true).ToList();

                        var xheaderCell3 = new PdfPCell(new Phrase(wh.Count.ToString(), tentimes));

                        var xheaderCell4 = new PdfPCell(new Phrase((wh.Count - day28.Count).ToString(), rtentimes));

                        variancetotal = variancetotal + (wh.Count - day28.Count);

                        var ah = day28.Where(i => i.TaskOn == true && i.ShiftOn == true).ToList();

                        var xheaderCell5 = new PdfPCell(new Phrase(ah.Count.ToString(), tentimes));


                        var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                        tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                        tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dutyrosterview.Date.AddDays(27) && i.CreatedDate.Value.Date <= dutyrosterview.Date.AddDays(27).AddHours(23)).ToList();

                        var groupedtevents = tevents.GroupBy(item => item.TaskId);
                        tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                        var ttraveldurationlist = 0.0;

                        var dttocheck = dutyrosterview.Date.AddDays(27);
                        foreach (var t in tevents)
                        {
                            var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                            if (task != null)
                            {
                                var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dttocheck);
                                ttraveldurationlist = tt + ttraveldurationlist;
                            }
                        }

                        var d = CommonUtility.GetUserOTHoursPerDay(dttocheck, dttocheck.AddHours(23), getots);
                        var xheaderCelltt = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                        var xheaderCelld = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));

                        totalOT = totalOT + d;
                        totalTT = totalTT + ttraveldurationlist;

                        xheaderCell.Border = Rectangle.NO_BORDER;
                        xheaderCell2.Border = Rectangle.NO_BORDER;
                        xheaderCell3.Border = Rectangle.NO_BORDER;
                        xheaderCell4.Border = Rectangle.NO_BORDER;
                        xheaderCell5.Border = Rectangle.NO_BORDER;
                        xheaderCell6.Border = Rectangle.NO_BORDER;

                        xheaderCell.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell2.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell3.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell4.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell5.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell6.BackgroundColor = Color.LIGHT_GRAY;


                        xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        newchecklistTable.AddCell(xheaderCell);
                        newchecklistTable.AddCell(xheaderCell2);
                        newchecklistTable.AddCell(xheaderCell3);
                        newchecklistTable.AddCell(xheaderCell4);

                        xheaderCelltt.Border = Rectangle.NO_BORDER;
                        xheaderCelld.Border = Rectangle.NO_BORDER;

                        xheaderCelltt.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCelld.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        xheaderCelltt.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCelld.BackgroundColor = Color.LIGHT_GRAY;

                        newchecklistTable.AddCell(xheaderCelltt);
                        newchecklistTable.AddCell(xheaderCelld);

                        newchecklistTable.AddCell(xheaderCell5);
                        newchecklistTable.AddCell(xheaderCell6);
                    }
                    else
                    {
                        var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.AddDays(27).ToShortDateString(), tentimes));
                        var xheaderCell2 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell3 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell4 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell5 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var xheaderCell6 = new PdfPCell(new Phrase(1, "\u00a0"));
                        var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                        tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                        var dttocheck = dutyrosterview.Date.AddDays(27);

                        tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dttocheck && i.CreatedDate.Value.Date <= dttocheck.AddHours(23)).ToList();

                        var groupedtevents = tevents.GroupBy(item => item.TaskId);
                        tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                        var ttraveldurationlist = 0.0;

                        foreach (var t in tevents)
                        {
                            var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                            if (task != null)
                            {
                                var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dttocheck);
                                ttraveldurationlist = tt + ttraveldurationlist;
                            }
                        }
                        var d = CommonUtility.GetUserOTHoursPerDay(dttocheck, dttocheck.AddHours(23), getots);

                        var xheaderCell7 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                        var xheaderCell8 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));

                        totalOT = totalOT + d;
                        totalTT = totalTT + ttraveldurationlist;

                        xheaderCell.Border = Rectangle.NO_BORDER;
                        xheaderCell2.Border = Rectangle.NO_BORDER;
                        xheaderCell3.Border = Rectangle.NO_BORDER;
                        xheaderCell4.Border = Rectangle.NO_BORDER;
                        xheaderCell5.Border = Rectangle.NO_BORDER;
                        xheaderCell6.Border = Rectangle.NO_BORDER;
                        xheaderCell7.Border = Rectangle.NO_BORDER;
                        xheaderCell8.Border = Rectangle.NO_BORDER;

                        xheaderCell.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell2.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell3.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell4.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell5.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell6.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell7.BackgroundColor = Color.LIGHT_GRAY;
                        xheaderCell8.BackgroundColor = Color.LIGHT_GRAY;

                        xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell7.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell8.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        newchecklistTable.AddCell(xheaderCell);
                        newchecklistTable.AddCell(xheaderCell2);
                        newchecklistTable.AddCell(xheaderCell3);
                        newchecklistTable.AddCell(xheaderCell4);
                        newchecklistTable.AddCell(xheaderCell7);
                        newchecklistTable.AddCell(xheaderCell8);
                        newchecklistTable.AddCell(xheaderCell5);
                        newchecklistTable.AddCell(xheaderCell6);
                    }
                    if (dutyrosterview.Date.Month == dutyrosterview.Date.AddDays(28).Month)
                    {
                        if (day29.Count > 0)
                        {

                            var sdays = day29.Where(i => i.DutyPost == "SICK DAY").ToList();
                            sickdays = sickdays + sdays.Count;
                            var hdays = day29.Where(i => i.DutyPost == "HOLIDAY").ToList();
                            holidays = holidays + hdays.Count;
                            var ddays = day29.Where(i => i.DutyPost == "DAY OFF").ToList();
                            dayoffs = dayoffs + ddays.Count;
                            var vdays = day29.Where(i => i.DutyPost == "VACATION").ToList();
                            vacations = vacations + vdays.Count;

                            day29 = day29.Where(i => i.DutyPost != "DAY OFF" && i.DutyPost != "HOLIDAY" && i.DutyPost != "SICK DAY" && i.DutyPost != "VACATION").ToList();

                            var comments = new Phrase(1, "\u00a0");


                            if (sdays.Count > 0)
                            {
                                comments = new Phrase("Sick Day", tentimes);
                            }
                            if (hdays.Count > 0)
                            {
                                comments = new Phrase("Holiday", tentimes);
                            }
                            if (ddays.Count > 0)
                            {
                                comments = new Phrase("Day Off", tentimes);
                            }
                            if (vdays.Count > 0)
                            {
                                comments = new Phrase("Vacation", tentimes);
                            }
                            var xheaderCell6 = new PdfPCell(comments);


                            var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.AddDays(28).ToShortDateString(), tentimes));

                            var xheaderCell2 = new PdfPCell(new Phrase(day29.Count.ToString(), tentimes));

                            var wh = day29.Where(i => i.ShiftOn == true).ToList();

                            var xheaderCell3 = new PdfPCell(new Phrase(wh.Count.ToString(), tentimes));

                            var xheaderCell4 = new PdfPCell(new Phrase((wh.Count - day29.Count).ToString(), rtentimes));

                            variancetotal = variancetotal + (wh.Count - day29.Count);

                            var ah = day29.Where(i => i.TaskOn == true && i.ShiftOn == true).ToList();

                            var xheaderCell5 = new PdfPCell(new Phrase(ah.Count.ToString(), tentimes));


                            var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                            tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                            tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dutyrosterview.Date.AddDays(28) && i.CreatedDate.Value.Date <= dutyrosterview.Date.AddDays(28).AddHours(23)).ToList();

                            var groupedtevents = tevents.GroupBy(item => item.TaskId);
                            tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                            var ttraveldurationlist = 0.0;
                            var dttocheck = dutyrosterview.Date.AddDays(28);
                            foreach (var t in tevents)
                            {
                                var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                                if (task != null)
                                {
                                    var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dttocheck);
                                    ttraveldurationlist = tt + ttraveldurationlist;
                                }
                            }


                            var d = CommonUtility.GetUserOTHoursPerDay(dttocheck, dttocheck.AddHours(23), getots);
                            var xheaderCelltt = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                            var xheaderCelld = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));

                            totalOT = totalOT + d;
                            totalTT = totalTT + ttraveldurationlist;

                            xheaderCell.Border = Rectangle.NO_BORDER;
                            xheaderCell2.Border = Rectangle.NO_BORDER;
                            xheaderCell3.Border = Rectangle.NO_BORDER;
                            xheaderCell4.Border = Rectangle.NO_BORDER;
                            xheaderCell5.Border = Rectangle.NO_BORDER;
                            xheaderCell6.Border = Rectangle.NO_BORDER;


                            xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                            xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                            xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                            xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                            xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                            newchecklistTable.AddCell(xheaderCell);
                            newchecklistTable.AddCell(xheaderCell2);
                            newchecklistTable.AddCell(xheaderCell3);
                            newchecklistTable.AddCell(xheaderCell4);

                            xheaderCelltt.Border = Rectangle.NO_BORDER;
                            xheaderCelld.Border = Rectangle.NO_BORDER;

                            xheaderCelltt.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                            xheaderCelld.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                            newchecklistTable.AddCell(xheaderCelltt);
                            newchecklistTable.AddCell(xheaderCelld);

                            newchecklistTable.AddCell(xheaderCell5);
                            newchecklistTable.AddCell(xheaderCell6);
                        }
                        else
                        {
                            var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.AddDays(28).ToShortDateString(), tentimes));
                            var xheaderCell2 = new PdfPCell(new Phrase(1, "\u00a0"));
                            var xheaderCell3 = new PdfPCell(new Phrase(1, "\u00a0"));
                            var xheaderCell4 = new PdfPCell(new Phrase(1, "\u00a0"));
                            var xheaderCell5 = new PdfPCell(new Phrase(1, "\u00a0"));
                            var xheaderCell6 = new PdfPCell(new Phrase(1, "\u00a0"));
                            var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                            tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                            var dttocheck = dutyrosterview.Date.AddDays(28);

                            tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dttocheck && i.CreatedDate.Value.Date <= dttocheck.AddHours(23)).ToList();

                            var groupedtevents = tevents.GroupBy(item => item.TaskId);
                            tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                            var ttraveldurationlist = 0.0;

                            foreach (var t in tevents)
                            {
                                var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                                if (task != null)
                                {
                                    var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dttocheck);
                                    ttraveldurationlist = tt + ttraveldurationlist;
                                }
                            }

                            var d = CommonUtility.GetUserOTHoursPerDay(dttocheck, dttocheck.AddHours(23), getots);
                            var xheaderCell7 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                            var xheaderCell8 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));

                            totalOT = totalOT + d;
                            totalTT = totalTT + ttraveldurationlist;

                            xheaderCell.Border = Rectangle.NO_BORDER;
                            xheaderCell2.Border = Rectangle.NO_BORDER;
                            xheaderCell3.Border = Rectangle.NO_BORDER;
                            xheaderCell4.Border = Rectangle.NO_BORDER;
                            xheaderCell5.Border = Rectangle.NO_BORDER;
                            xheaderCell6.Border = Rectangle.NO_BORDER;
                            xheaderCell7.Border = Rectangle.NO_BORDER;
                            xheaderCell8.Border = Rectangle.NO_BORDER;

                            xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                            xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                            xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                            xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                            xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                            xheaderCell7.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                            xheaderCell8.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                            newchecklistTable.AddCell(xheaderCell);
                            newchecklistTable.AddCell(xheaderCell2);
                            newchecklistTable.AddCell(xheaderCell3);
                            newchecklistTable.AddCell(xheaderCell4);
                            newchecklistTable.AddCell(xheaderCell7);
                            newchecklistTable.AddCell(xheaderCell8);
                            newchecklistTable.AddCell(xheaderCell5);
                            newchecklistTable.AddCell(xheaderCell6);
                        }
                    }
                    if (dutyrosterview.Date.Month == dutyrosterview.Date.AddDays(29).Month)
                    {
                        if (day30.Count > 0)
                        {

                            var sdays = day30.Where(i => i.DutyPost == "SICK DAY").ToList();
                            sickdays = sickdays + sdays.Count;
                            var hdays = day30.Where(i => i.DutyPost == "HOLIDAY").ToList();
                            holidays = holidays + hdays.Count;
                            var ddays = day30.Where(i => i.DutyPost == "DAY OFF").ToList();
                            dayoffs = dayoffs + ddays.Count;
                            var vdays = day30.Where(i => i.DutyPost == "VACATION").ToList();
                            vacations = vacations + vdays.Count;

                            day30 = day30.Where(i => i.DutyPost != "DAY OFF" && i.DutyPost != "HOLIDAY" && i.DutyPost != "SICK DAY" && i.DutyPost != "VACATION").ToList();

                            var comments = new Phrase(1, "\u00a0");


                            if (sdays.Count > 0)
                            {
                                comments = new Phrase("Sick Day", tentimes);
                            }
                            if (hdays.Count > 0)
                            {
                                comments = new Phrase("Holiday", tentimes);
                            }
                            if (ddays.Count > 0)
                            {
                                comments = new Phrase("Day Off", tentimes);
                            }
                            if (vdays.Count > 0)
                            {
                                comments = new Phrase("Vacation", tentimes);
                            }
                            var xheaderCell6 = new PdfPCell(comments);


                            var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.AddDays(29).ToShortDateString(), tentimes));

                            var xheaderCell2 = new PdfPCell(new Phrase(day30.Count.ToString(), tentimes));

                            var wh = day30.Where(i => i.ShiftOn == true).ToList();

                            var xheaderCell3 = new PdfPCell(new Phrase(wh.Count.ToString(), tentimes));

                            var xheaderCell4 = new PdfPCell(new Phrase((wh.Count - day30.Count).ToString(), rtentimes));

                            variancetotal = variancetotal + (wh.Count - day30.Count);

                            var ah = day30.Where(i => i.TaskOn == true && i.ShiftOn == true).ToList();

                            var xheaderCell5 = new PdfPCell(new Phrase(ah.Count.ToString(), tentimes));


                            var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                            tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                            tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dutyrosterview.Date.AddDays(29) && i.CreatedDate.Value.Date <= dutyrosterview.Date.AddDays(29).AddHours(23)).ToList();

                            var groupedtevents = tevents.GroupBy(item => item.TaskId);
                            tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                            var ttraveldurationlist = 0.0;
                            var dttocheck = dutyrosterview.Date.AddDays(29);
                            foreach (var t in tevents)
                            {
                                var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                                if (task != null)
                                {
                                    var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dttocheck);
                                    ttraveldurationlist = tt + ttraveldurationlist;
                                }
                            }


                            var d = CommonUtility.GetUserOTHoursPerDay(dttocheck, dttocheck.AddHours(23), getots);
                            var xheaderCelltt = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                            var xheaderCelld = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));

                            totalOT = totalOT + d;
                            totalTT = totalTT + ttraveldurationlist;

                            xheaderCell.Border = Rectangle.NO_BORDER;
                            xheaderCell2.Border = Rectangle.NO_BORDER;
                            xheaderCell3.Border = Rectangle.NO_BORDER;
                            xheaderCell4.Border = Rectangle.NO_BORDER;
                            xheaderCell5.Border = Rectangle.NO_BORDER;
                            xheaderCell6.Border = Rectangle.NO_BORDER;

                            xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                            xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                            xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                            xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                            xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;


                            xheaderCell.BackgroundColor = Color.LIGHT_GRAY;
                            xheaderCell2.BackgroundColor = Color.LIGHT_GRAY;
                            xheaderCell3.BackgroundColor = Color.LIGHT_GRAY;
                            xheaderCell4.BackgroundColor = Color.LIGHT_GRAY;
                            xheaderCell5.BackgroundColor = Color.LIGHT_GRAY;
                            xheaderCell6.BackgroundColor = Color.LIGHT_GRAY;

                            newchecklistTable.AddCell(xheaderCell);
                            newchecklistTable.AddCell(xheaderCell2);
                            newchecklistTable.AddCell(xheaderCell3);
                            newchecklistTable.AddCell(xheaderCell4);

                            xheaderCelltt.Border = Rectangle.NO_BORDER;
                            xheaderCelld.Border = Rectangle.NO_BORDER;

                            xheaderCelltt.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                            xheaderCelld.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                            xheaderCelltt.BackgroundColor = Color.LIGHT_GRAY;
                            xheaderCelld.BackgroundColor = Color.LIGHT_GRAY;

                            newchecklistTable.AddCell(xheaderCelltt);
                            newchecklistTable.AddCell(xheaderCelld);

                            newchecklistTable.AddCell(xheaderCell5);
                            newchecklistTable.AddCell(xheaderCell6);
                        }
                        else
                        {
                            var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.AddDays(29).ToShortDateString(), tentimes));
                            var xheaderCell2 = new PdfPCell(new Phrase(1, "\u00a0"));
                            var xheaderCell3 = new PdfPCell(new Phrase(1, "\u00a0"));
                            var xheaderCell4 = new PdfPCell(new Phrase(1, "\u00a0"));
                            var xheaderCell5 = new PdfPCell(new Phrase(1, "\u00a0"));
                            var xheaderCell6 = new PdfPCell(new Phrase(1, "\u00a0"));
                            var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                            tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                            var dttocheck = dutyrosterview.Date.AddDays(29);

                            tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dttocheck && i.CreatedDate.Value.Date <= dttocheck.AddHours(23)).ToList();

                            var groupedtevents = tevents.GroupBy(item => item.TaskId);
                            tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                            var ttraveldurationlist = 0.0;

                            foreach (var t in tevents)
                            {
                                var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                                if (task != null)
                                {
                                    var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dttocheck);
                                    ttraveldurationlist = tt + ttraveldurationlist;
                                }
                            }

                            var d = CommonUtility.GetUserOTHoursPerDay(dttocheck, dttocheck.AddHours(23), getots);
                            var xheaderCell7 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                            var xheaderCell8 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));

                            totalOT = totalOT + d;
                            totalTT = totalTT + ttraveldurationlist;

                            xheaderCell.Border = Rectangle.NO_BORDER;
                            xheaderCell2.Border = Rectangle.NO_BORDER;
                            xheaderCell3.Border = Rectangle.NO_BORDER;
                            xheaderCell4.Border = Rectangle.NO_BORDER;
                            xheaderCell5.Border = Rectangle.NO_BORDER;
                            xheaderCell6.Border = Rectangle.NO_BORDER;
                            xheaderCell7.Border = Rectangle.NO_BORDER;
                            xheaderCell8.Border = Rectangle.NO_BORDER;

                            xheaderCell.BackgroundColor = Color.LIGHT_GRAY;
                            xheaderCell2.BackgroundColor = Color.LIGHT_GRAY;
                            xheaderCell3.BackgroundColor = Color.LIGHT_GRAY;
                            xheaderCell4.BackgroundColor = Color.LIGHT_GRAY;
                            xheaderCell5.BackgroundColor = Color.LIGHT_GRAY;
                            xheaderCell6.BackgroundColor = Color.LIGHT_GRAY;
                            xheaderCell7.BackgroundColor = Color.LIGHT_GRAY;
                            xheaderCell8.BackgroundColor = Color.LIGHT_GRAY;

                            xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                            xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                            xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                            xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                            xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                            xheaderCell7.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                            xheaderCell8.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                            newchecklistTable.AddCell(xheaderCell);
                            newchecklistTable.AddCell(xheaderCell2);
                            newchecklistTable.AddCell(xheaderCell3);
                            newchecklistTable.AddCell(xheaderCell4);
                            newchecklistTable.AddCell(xheaderCell7);
                            newchecklistTable.AddCell(xheaderCell8);
                            newchecklistTable.AddCell(xheaderCell5);
                            newchecklistTable.AddCell(xheaderCell6);
                        }
                    }


                    if (dutyrosterview.Date.Month == dutyrosterview.Date.AddDays(30).Month)
                    {
                        if (day31.Count > 0)
                        {

                            var sdays = day31.Where(i => i.DutyPost == "SICK DAY").ToList();
                            sickdays = sickdays + sdays.Count;
                            var hdays = day31.Where(i => i.DutyPost == "HOLIDAY").ToList();
                            holidays = holidays + hdays.Count;
                            var ddays = day31.Where(i => i.DutyPost == "DAY OFF").ToList();
                            dayoffs = dayoffs + ddays.Count;
                            var vdays = day31.Where(i => i.DutyPost == "VACATION").ToList();
                            vacations = vacations + vdays.Count;

                            day31 = day31.Where(i => i.DutyPost != "DAY OFF" && i.DutyPost != "HOLIDAY" && i.DutyPost != "SICK DAY" && i.DutyPost != "VACATION").ToList();

                            var comments = new Phrase(1, "\u00a0");


                            if (sdays.Count > 0)
                            {
                                comments = new Phrase("Sick Day", tentimes);
                            }
                            if (hdays.Count > 0)
                            {
                                comments = new Phrase("Holiday", tentimes);
                            }
                            if (ddays.Count > 0)
                            {
                                comments = new Phrase("Day Off", tentimes);
                            }
                            if (vdays.Count > 0)
                            {
                                comments = new Phrase("Vacation", tentimes);
                            }
                            var xheaderCell6 = new PdfPCell(comments);

                            var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.AddDays(30).ToShortDateString(), tentimes));

                            var xheaderCell2 = new PdfPCell(new Phrase(day31.Count.ToString(), tentimes));

                            var wh = day31.Where(i => i.ShiftOn == true).ToList();

                            var xheaderCell3 = new PdfPCell(new Phrase(wh.Count.ToString(), tentimes));

                            var xheaderCell4 = new PdfPCell(new Phrase((wh.Count - day31.Count).ToString(), rtentimes));

                            variancetotal = variancetotal + (wh.Count - day31.Count);

                            var ah = day31.Where(i => i.TaskOn == true && i.ShiftOn == true).ToList();

                            var xheaderCell5 = new PdfPCell(new Phrase(ah.Count.ToString(), tentimes));


                            var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                            tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                            tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dutyrosterview.Date.AddDays(30) && i.CreatedDate.Value.Date <= dutyrosterview.Date.AddDays(30).AddHours(23)).ToList();

                            var groupedtevents = tevents.GroupBy(item => item.TaskId);
                            tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                            var ttraveldurationlist = 0.0;
                            var dttocheck = dutyrosterview.Date.AddDays(30);
                            foreach (var t in tevents)
                            {
                                var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                                if (task != null)
                                {
                                    var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dttocheck);
                                    ttraveldurationlist = tt + ttraveldurationlist;
                                }
                            }

                            var d = CommonUtility.GetUserOTHoursPerDay(dttocheck, dttocheck.AddHours(23), getots);
                            var xheaderCelltt = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                            var xheaderCelld = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));

                            totalOT = totalOT + d;
                            totalTT = totalTT + ttraveldurationlist;

                            xheaderCell.Border = Rectangle.NO_BORDER;
                            xheaderCell2.Border = Rectangle.NO_BORDER;
                            xheaderCell3.Border = Rectangle.NO_BORDER;
                            xheaderCell4.Border = Rectangle.NO_BORDER;
                            xheaderCell5.Border = Rectangle.NO_BORDER;
                            xheaderCell6.Border = Rectangle.NO_BORDER;

                            xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                            xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                            xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                            xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                            xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                            newchecklistTable.AddCell(xheaderCell);
                            newchecklistTable.AddCell(xheaderCell2);
                            newchecklistTable.AddCell(xheaderCell3);
                            newchecklistTable.AddCell(xheaderCell4);


                            xheaderCelltt.Border = Rectangle.NO_BORDER;
                            xheaderCelld.Border = Rectangle.NO_BORDER;

                            xheaderCelltt.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                            xheaderCelld.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                            newchecklistTable.AddCell(xheaderCelltt);
                            newchecklistTable.AddCell(xheaderCelld);


                            newchecklistTable.AddCell(xheaderCell5);
                            newchecklistTable.AddCell(xheaderCell6);
                        }
                        else
                        {
                            var xheaderCell = new PdfPCell(new Phrase(dutyrosterview.Date.AddDays(30).ToShortDateString(), tentimes));
                            var xheaderCell2 = new PdfPCell(new Phrase(1, "\u00a0"));
                            var xheaderCell3 = new PdfPCell(new Phrase(1, "\u00a0"));
                            var xheaderCell4 = new PdfPCell(new Phrase(1, "\u00a0"));
                            var xheaderCell5 = new PdfPCell(new Phrase(1, "\u00a0"));
                            var xheaderCell6 = new PdfPCell(new Phrase(1, "\u00a0"));
                            var tevents = TaskEventHistory.GetTaskEventHistoryByUser(employee.Username, dbConnection);

                            tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                            var dttocheck = dutyrosterview.Date.AddDays(30);

                            tevents = tevents.Where(i => i.CreatedDate.Value.Date >= dttocheck && i.CreatedDate.Value.Date <= dttocheck.AddHours(23)).ToList();

                            var groupedtevents = tevents.GroupBy(item => item.TaskId);
                            tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                            var ttraveldurationlist = 0.0;

                            foreach (var t in tevents)
                            {
                                var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                                if (task != null)
                                {
                                    var tt = CommonUtility.GetTravelTimeByTaskByDay(task, dttocheck);
                                    ttraveldurationlist = tt + ttraveldurationlist;
                                }
                            }

                            var d = CommonUtility.GetUserOTHoursPerDay(dttocheck, dttocheck.AddHours(23), getots);

                            var xheaderCell7 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), tentimes));

                            var xheaderCell8 = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(d).ToString(), tentimes));

                            totalOT = totalOT + d;
                            totalTT = totalTT + ttraveldurationlist;

                            xheaderCell.Border = Rectangle.NO_BORDER;
                            xheaderCell2.Border = Rectangle.NO_BORDER;
                            xheaderCell3.Border = Rectangle.NO_BORDER;
                            xheaderCell4.Border = Rectangle.NO_BORDER;
                            xheaderCell5.Border = Rectangle.NO_BORDER;
                            xheaderCell6.Border = Rectangle.NO_BORDER;
                            xheaderCell7.Border = Rectangle.NO_BORDER;
                            xheaderCell8.Border = Rectangle.NO_BORDER;

                            xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                            xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                            xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                            xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                            xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                            xheaderCell7.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                            xheaderCell8.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                            newchecklistTable.AddCell(xheaderCell);
                            newchecklistTable.AddCell(xheaderCell2);
                            newchecklistTable.AddCell(xheaderCell3);
                            newchecklistTable.AddCell(xheaderCell4);
                            newchecklistTable.AddCell(xheaderCell7);
                            newchecklistTable.AddCell(xheaderCell8);
                            newchecklistTable.AddCell(xheaderCell5);
                            newchecklistTable.AddCell(xheaderCell6);
                        }
                    }

                    var AtaskdataTable = new PdfPTable(2);
                    var AtaskdataTableA = new PdfPTable(2);
                    var AtaskdataTableB = new PdfPTable(2);
                    AtaskdataTable.DefaultCell.Border = Rectangle.NO_BORDER;
                    AtaskdataTable.WidthPercentage = 100;

                    AtaskdataTableA.DefaultCell.Border = Rectangle.NO_BORDER;
                    AtaskdataTableB.DefaultCell.Border = Rectangle.NO_BORDER;

                    AtaskdataTableA.WidthPercentage = 100;
                    AtaskdataTableB.WidthPercentage = 100;

                    AtaskdataTableA.SetWidths(columnWidths3565);

                    AtaskdataTableB.SetWidths(columnWidths3565);

                    var ATheader1T = new PdfPCell();
                    ATheader1T.AddElement(new Phrase("During report period:", cbtimes));
                    ATheader1T.Border = Rectangle.NO_BORDER;
                    ATheader1T.Colspan = 2;
                    AtaskdataTableA.AddCell(ATheader1T);

                    var ATheader1 = new PdfPCell();
                    ATheader1.AddElement(new Phrase("Off Days:", beleventimes));
                    ATheader1.Border = Rectangle.NO_BORDER;
                    AtaskdataTableA.AddCell(ATheader1);

                    var ATheader1a = new PdfPCell();
                    ATheader1a.AddElement(new Phrase(dayoffs.ToString(), eleventimes));
                    ATheader1a.Border = Rectangle.NO_BORDER;
                    AtaskdataTableA.AddCell(ATheader1a);

                    var ATheader2 = new PdfPCell();
                    ATheader2.AddElement(new Phrase("Sick Days:", beleventimes));
                    ATheader2.Border = Rectangle.NO_BORDER;

                    AtaskdataTableA.AddCell(ATheader2);

                    var ATheader2a = new PdfPCell();
                    ATheader2a.AddElement(new Phrase(sickdays.ToString(), eleventimes));
                    ATheader2a.Border = Rectangle.NO_BORDER;
                    AtaskdataTableA.AddCell(ATheader2a);

                    var ATheader3tt = new PdfPCell();
                    ATheader3tt.AddElement(new Phrase("Travel Time(h):", beleventimes));
                    ATheader3tt.Border = Rectangle.NO_BORDER;
                    AtaskdataTableA.AddCell(ATheader3tt);

                    var ATheader3att = new PdfPCell();
                    ATheader3att.AddElement(new Phrase(CommonUtility.MinutesToHoursAndMinutes(totalTT).ToString(), eleventimes));
                    ATheader3att.Border = Rectangle.NO_BORDER;
                    AtaskdataTableA.AddCell(ATheader3att);

                    var ATheader3ot = new PdfPCell();
                    ATheader3ot.AddElement(new Phrase("Overtime(h):", beleventimes));
                    ATheader3ot.Border = Rectangle.NO_BORDER;
                    AtaskdataTableA.AddCell(ATheader3ot);

                    var ATheader3aot = new PdfPCell();
                    ATheader3aot.AddElement(new Phrase(CommonUtility.MinutesToHoursAndMinutes(totalOT).ToString(), eleventimes));
                    ATheader3aot.Border = Rectangle.NO_BORDER;
                    AtaskdataTableA.AddCell(ATheader3aot);

                    var ATheader3 = new PdfPCell();
                    ATheader3.AddElement(new Phrase("Amendment:", beleventimes));
                    ATheader3.Border = Rectangle.NO_BORDER;
                    AtaskdataTableA.AddCell(ATheader3);

                    var ATheader3a = new PdfPCell();
                    ATheader3a.AddElement(new Phrase(rv.Amendment.ToString(), eleventimes));
                    ATheader3a.Border = Rectangle.NO_BORDER;
                    AtaskdataTableA.AddCell(ATheader3a);

                    var ATheader4 = new PdfPCell();
                    ATheader4.AddElement(new Phrase("Notes:", beleventimes));
                    ATheader4.Border = Rectangle.NO_BORDER;
                    AtaskdataTableA.AddCell(ATheader4);

                    var ATheader4a = new PdfPCell();
                    ATheader4a.AddElement(new Phrase(rv.Notes, eleventimes));
                    ATheader4a.Border = Rectangle.NO_BORDER;
                    AtaskdataTableA.AddCell(ATheader4a);

                    var BTheader1T = new PdfPCell();
                    BTheader1T.AddElement(new Phrase("Status per " + month, cbtimes));
                    BTheader1T.Border = Rectangle.NO_BORDER;
                    BTheader1T.Colspan = 2;
                    AtaskdataTableB.AddCell(BTheader1T);

                    var BTheader1 = new PdfPCell();
                    BTheader1.AddElement(new Phrase("Net Holidays:", beleventimes));
                    BTheader1.Border = Rectangle.NO_BORDER;
                    AtaskdataTableB.AddCell(BTheader1);

                    var BTheader1a = new PdfPCell();
                    BTheader1a.AddElement(new Phrase(holidays.ToString(), eleventimes));
                    BTheader1a.Border = Rectangle.NO_BORDER;
                    AtaskdataTableB.AddCell(BTheader1a);

                    var BTheader2 = new PdfPCell();
                    BTheader2.AddElement(new Phrase("Total Sick days:", beleventimes));
                    BTheader2.Border = Rectangle.NO_BORDER;

                    AtaskdataTableB.AddCell(BTheader2);

                    var BTheader2a = new PdfPCell();
                    BTheader2a.AddElement(new Phrase(sickdays.ToString(), eleventimes));
                    BTheader2a.Border = Rectangle.NO_BORDER;
                    AtaskdataTableB.AddCell(BTheader2a);

                    var BTheader3 = new PdfPCell();
                    BTheader3.AddElement(new Phrase("Total Days in lieu:", beleventimes));
                    BTheader3.Border = Rectangle.NO_BORDER;
                    AtaskdataTableB.AddCell(BTheader3);

                    var BTheader3a = new PdfPCell();
                    BTheader3a.AddElement(new Phrase(rv.TotalDayLieu, eleventimes));
                    BTheader3a.Border = Rectangle.NO_BORDER;
                    AtaskdataTableB.AddCell(BTheader3a);

                    var BTheader4 = new PdfPCell();
                    BTheader4.AddElement(new Phrase("Total Overtime:", beleventimes));
                    BTheader4.Border = Rectangle.NO_BORDER;
                    AtaskdataTableB.AddCell(BTheader4);

                    var BTheader4a = new PdfPCell();
                    BTheader4a.AddElement(new Phrase(rv.TotalOTHours, eleventimes));
                    BTheader4a.Border = Rectangle.NO_BORDER;
                    AtaskdataTableB.AddCell(BTheader4a);

                    AtaskdataTable.AddCell(AtaskdataTableA);
                    AtaskdataTable.AddCell(AtaskdataTableB);

                    doc.Add(AtaskdataTable);

                    var BtaskdataTable = new PdfPTable(2);
                    var BtaskdataTableA = new PdfPTable(2);
                    var BtaskdataTableB = new PdfPTable(2);
                    BtaskdataTable.DefaultCell.Border = Rectangle.NO_BORDER;
                    BtaskdataTable.WidthPercentage = 100;

                    BtaskdataTableA.DefaultCell.Border = Rectangle.NO_BORDER;
                    BtaskdataTableB.DefaultCell.Border = Rectangle.NO_BORDER;

                    BtaskdataTableA.WidthPercentage = 100;
                    BtaskdataTableB.WidthPercentage = 100;

                    BtaskdataTableA.SetWidths(columnWidths3565);

                    BtaskdataTableB.SetWidths(columnWidths3565);

                    var BTheader = new PdfPCell();
                    BTheader.AddElement(new Phrase("Work Performance:", cbtimes));
                    BTheader.Border = Rectangle.NO_BORDER;
                    BTheader.Colspan = 2;
                    BtaskdataTable.AddCell(BTheader);

                    var ABTheader1 = new PdfPCell();
                    ABTheader1.AddElement(new Phrase("Worked Hours:", beleventimes));
                    ABTheader1.Border = Rectangle.NO_BORDER;
                    BtaskdataTableA.AddCell(ABTheader1);

                    var cinfo = CustomerInfo.GetCustomerInfoById(employee.CustomerInfoId, dbConnection);

                    var ABTheader1a = new PdfPCell();
                    ABTheader1a.AddElement(new Phrase(dblocks.Count.ToString() + " of " + (cinfo.WorkingDaysWeek * 4).ToString() + " Base Hours", eleventimes));
                    ABTheader1a.Border = Rectangle.NO_BORDER;
                    BtaskdataTableA.AddCell(ABTheader1a);

                    var ABTheader2 = new PdfPCell();
                    ABTheader2.AddElement(new Phrase("Variance:", beleventimes));
                    ABTheader2.Border = Rectangle.NO_BORDER;
                    BtaskdataTableA.AddCell(ABTheader2);

                    var ABTheader2a = new PdfPCell();
                    ABTheader2a.AddElement(new Phrase(variancetotal.ToString(), redeleventimes));
                    ABTheader2a.Border = Rectangle.NO_BORDER;
                    BtaskdataTableA.AddCell(ABTheader2a);

                    var ABTheader3 = new PdfPCell();
                    ABTheader3.AddElement(new Phrase("Active Hours:", beleventimes));
                    ABTheader3.Border = Rectangle.NO_BORDER;
                    BtaskdataTableA.AddCell(ABTheader3);

                    var ABTheader3a = new PdfPCell();
                    ABTheader3a.AddElement(new Phrase(tblocks.Count.ToString(), eleventimes));
                    ABTheader3a.Border = Rectangle.NO_BORDER;
                    BtaskdataTableA.AddCell(ABTheader3a);

                    var ABTheader4 = new PdfPCell();
                    ABTheader4.AddElement(new Phrase("Assignments:", beleventimes));
                    ABTheader4.Border = Rectangle.NO_BORDER;
                    BtaskdataTableA.AddCell(ABTheader4);

                    var ABTheader4a = new PdfPCell();
                    ABTheader4a.AddElement(new Phrase(totalAssignment.ToString(), eleventimes));
                    ABTheader4a.Border = Rectangle.NO_BORDER;
                    BtaskdataTableA.AddCell(ABTheader4a);

                    var ABTheader5 = new PdfPCell();
                    ABTheader5.AddElement(new Phrase("Utilization:", beleventimes));
                    ABTheader5.Border = Rectangle.NO_BORDER;
                    BtaskdataTableA.AddCell(ABTheader5);

                    if (dblocks.Count > 0 && tblocks.Count > 0)
                    {
                        var utilizationpercentage = (int)Math.Round((float)tblocks.Count / (float)dblocks.Count * (float)100);

                        var ABTheader5a = new PdfPCell();
                        ABTheader5a.AddElement(new Phrase(utilizationpercentage.ToString() + "%", eleventimes));
                        ABTheader5a.Border = Rectangle.NO_BORDER;
                        BtaskdataTableA.AddCell(ABTheader5a);
                    }
                    else
                    {
                        var ABTheader5a = new PdfPCell();
                        ABTheader5a.AddElement(new Phrase("0%", eleventimes));
                        ABTheader5a.Border = Rectangle.NO_BORDER;
                        BtaskdataTableA.AddCell(ABTheader5a);
                    }

                    var sstaffData = staffData.Where(p => p.TotalDays > 0).ToList();

                    var workedaverage = Math.Round(sstaffData.Average(item => item.DutyOn), 2);
                    var activeaverage = Math.Round(sstaffData.Average(item => item.TaskOn), 2);
                    var assaverage = Math.Round(sstaffData.Average(item => item.TotalAssignment), 2);
                    var utilizationaverage = Math.Round(sstaffData.Average(item => item.UtilizationAverage), 2);

                    var BBTheader1 = new PdfPCell();
                    BBTheader1.AddElement(new Phrase("Peer Average:", beleventimes));
                    BBTheader1.Border = Rectangle.NO_BORDER;
                    BtaskdataTableB.AddCell(BBTheader1);

                    var BBTheader1a = new PdfPCell();
                    BBTheader1a.AddElement(new Phrase(workedaverage.ToString(), eleventimes));
                    BBTheader1a.Border = Rectangle.NO_BORDER;
                    BtaskdataTableB.AddCell(BBTheader1a);

                    var blankcell = new PdfPCell(new Phrase(1, "\u00a0"));
                    blankcell.Border = Rectangle.NO_BORDER;
                    blankcell.Colspan = 2;
                    BtaskdataTableB.AddCell(blankcell);

                    var BBTheader2 = new PdfPCell();
                    BBTheader2.AddElement(new Phrase("Peer Average:", beleventimes));
                    BBTheader2.Border = Rectangle.NO_BORDER;
                    BtaskdataTableB.AddCell(BBTheader2);

                    var BBTheader2a = new PdfPCell();
                    BBTheader2a.AddElement(new Phrase(activeaverage.ToString(), eleventimes));
                    BBTheader2a.Border = Rectangle.NO_BORDER;
                    BtaskdataTableB.AddCell(BBTheader2a);

                    var BBTheader3 = new PdfPCell();
                    BBTheader3.AddElement(new Phrase("Peer Average:", beleventimes));
                    BBTheader3.Border = Rectangle.NO_BORDER;
                    BtaskdataTableB.AddCell(BBTheader3);

                    var BBTheader3a = new PdfPCell();
                    BBTheader3a.AddElement(new Phrase(assaverage.ToString(), eleventimes));
                    BBTheader3a.Border = Rectangle.NO_BORDER;
                    BtaskdataTableB.AddCell(BBTheader3a);

                    var BBTheader4 = new PdfPCell();
                    BBTheader4.AddElement(new Phrase("Peer Average:", beleventimes));
                    BBTheader4.Border = Rectangle.NO_BORDER;
                    BtaskdataTableB.AddCell(BBTheader4);

                    var BBTheader4a = new PdfPCell();
                    BBTheader4a.AddElement(new Phrase(utilizationaverage.ToString() + "%", eleventimes));
                    BBTheader4a.Border = Rectangle.NO_BORDER;
                    BtaskdataTableB.AddCell(BBTheader4a);

                    BtaskdataTable.AddCell(BtaskdataTableA);
                    BtaskdataTable.AddCell(BtaskdataTableB);

                    doc.Add(BtaskdataTable);

                    doc.Add(newchecklistTable);

                    Font geleventimes = new Font(f_cn, 11, Font.NORMAL, Color.GRAY);
                    var endTable = new PdfPTable(1);
                    endTable.DefaultCell.Border = Rectangle.NO_BORDER;
                    endTable.WidthPercentage = 100;

                    var endheader = new PdfPCell();
                    endheader.AddElement(new Phrase("Report generated: " + CommonUtility.getDTNow().AddHours(userinfo.TimeZone).ToString() + " 	by: " + userinfo.Username, geleventimes));
                    endheader.Border = Rectangle.NO_BORDER;
                    endTable.AddCell(endheader);
                    doc.Add(endTable);

                    doc.Close();

                    Response.ContentType = "application/pdf";
                    Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.Write(doc);
                    Response.End();
                }
                else
                {
                    Response.Write("<script>alert('No entries found')</script>");
                }
            }
            catch (Exception ex)
            {
                if (userinfo != null)
                    MIMSLog.MIMSLogSave("Reports", "btnExport3_Click", ex, dbConnection, userinfo.SiteId, userinfo.CustomerInfoId);
                else
                    MIMSLog.MIMSLogSave("Reports", "btnExport3_Click", ex, dbConnection);
            }
        }

        protected void btnExport4_Click(object sender, EventArgs e)
        {
            var userinfo = Users.GetUserByName(User.Identity.Name, dbConnection);
            try
            {
                //using (MemoryStream memoryStream = new MemoryStream())

                string toDate = toHD.Value; //String.Format("{0}", Request.Form["toDate"]);
                string fromDate = fromHD.Value; // String.Format("{0}", Request.Form["fromDate"]);
                var status = dutyStatus.Value;
                var uid = dutyUser.Value;
                var todate = Convert.ToDateTime(toDate);
                var fromdate = Convert.ToDateTime(fromDate);
                 
                var allusers = new List<Users>();

                if (userinfo.RoleId == (int)Role.SuperAdmin)
                {
                    allusers = Users.GetAllUsers(dbConnection);
                }
                else if (userinfo.RoleId == (int)Role.ChiefOfficer)
                {
                    allusers = Users.GetAllUsers(dbConnection);

                    allusers = allusers.Where(i => i.RoleId != (int)Role.ChiefOfficer).ToList();
                }
                else if (userinfo.RoleId == (int)Role.CustomerSuperadmin)
                {
                    allusers = Users.GetAllUsersByCustomerIdNotIncludeCustomerUser(userinfo.CustomerInfoId, dbConnection);

                    allusers = allusers.Where(i => i.RoleId != (int)Role.CustomerSuperadmin).ToList();
                }
                else if (userinfo.RoleId == (int)Role.Director)
                {
                    allusers = Users.GetAllUsersBySiteId(userinfo.SiteId, dbConnection);

                    allusers = allusers.Where(i => i.RoleId != (int)Role.Director).ToList();
                }
                else if (userinfo.RoleId == (int)Role.Admin)
                {
                    var allmanagers = DirectorManager.GetAllFullManagersByDirectorId(userinfo.ID, dbConnection);
                    foreach (var usermanager in allmanagers)
                    {
                        allusers.Add(usermanager);
                        var allmanagerusers = Users.GetAllFullUsersByManagerIdForDirector(usermanager.ID, dbConnection);
                        allusers.AddRange(allmanagerusers);
                    }
                    var unassigned = Users.GetAllUnassignedOperators(dbConnection);
                    unassigned = unassigned.Where(i => i.SiteId == (int)userinfo.SiteId).ToList();
                    foreach (var subsubuser in unassigned)
                    {
                        allusers.Add(subsubuser);
                    }
                }
                else if (userinfo.RoleId == (int)Role.Manager)
                {
                    allusers.AddRange(Users.GetAllFullUsersByManagerId(userinfo.ID, dbConnection));
                }
                else if (userinfo.RoleId == (int)Role.Regional)
                {
                    allusers.AddRange(Users.GetAllUsersByLevel5(userinfo.ID, dbConnection));
                }
                allusers = allusers.Where(i => i.RoleId != (int)Role.MessageBoardUser && i.RoleId != (int)Role.CustomerUser).ToList();
                var staffData = RosterView.GetStaffAverageTemp(userinfo.CustomerInfoId, (int)TaskAssigneeType.User, fromdate.ToString(), todate.AddHours(23).ToString(), dbConnection);

                staffData = staffData.Where(p => allusers.Any(p2 => p2.ID == p.EmployeeId)).ToList();



                if (staffData.Count > 0)
                {
                    var dutyrosterview = new DateTime(fromdate.Year, fromdate.Month, 1);


                    iTextSharp.text.Document doc = new iTextSharp.text.Document(PageSize.LETTER, 25F, 25F, 50F, 25F);

                    doc.SetMargins(25, 25, 65, 35);

                    var fileName = CommonUtility.getDTNow().Day.ToString() + "-" + CommonUtility.getDTNow().Month.ToString() + "-" + CommonUtility.getDTNow().Year.ToString() + "-" + CommonUtility.getDTNow().Hour.ToString() + "-" + CommonUtility.getDTNow().Minute.ToString() + "-" + CommonUtility.getDTNow().Second.ToString() + ".pdf";
                    // path = path + "\\" + fileName;

                    PdfWriter writer = PdfWriter.GetInstance(doc, Response.OutputStream);//PdfWriter.GetInstance(doc, new FileStream(path, FileMode.Create));
                    writer.PageEvent = new ITextEvents()
                    {
                        cid = userinfo.CustomerInfoId
                    };
                    doc.Open();
                    doc.SetMargins(25, 25, 65, 35);
                    BaseFont f_cn = BaseFont.CreateFont(Environment.GetFolderPath(Environment.SpecialFolder.Fonts) + "\\verdana.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

                    BaseFont z_cn = BaseFont.CreateFont(BaseFont.ZAPFDINGBATS, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

                    Font times = new Font(f_cn, 12, Font.NORMAL, Color.BLACK);
                    Font btimes = new Font(f_cn, 12, Font.BOLD, Color.BLACK);

                    iTextSharp.text.Color arrowred = new iTextSharp.text.Color(162, 0, 46);

                    Font eleventimes = new Font(f_cn, 11, Font.NORMAL, Color.BLACK);
                    Font tentimes = new Font(f_cn, 10, Font.NORMAL, Color.BLACK);
                    Font rtentimes = new Font(f_cn, 10, Font.NORMAL, arrowred);
                    Font weleventimes = new Font(f_cn, 11, Font.NORMAL, Color.WHITE);

                    Font weighttimes = new Font(f_cn, 8, Font.NORMAL, Color.WHITE);

                    Font seventimes = new Font(f_cn, 7, Font.NORMAL, Color.BLACK);

                    Font gweb = new Font(z_cn, 11, Font.NORMAL, Color.GREEN);
                    Font rweb = new Font(z_cn, 11, Font.NORMAL, Color.RED);

                    Font ueleventimes = new Font(f_cn, 11, Font.UNDERLINE, Color.BLACK);

                    Font beleventimes = new Font(f_cn, 11, Font.BOLD, Color.BLACK);
                    Font bredeleventimes = new Font(f_cn, 11, Font.BOLD, arrowred);
                    Font redeleventimes = new Font(f_cn, 11, Font.NORMAL, arrowred);
                    Font twelvetimes = new Font(f_cn, 12, Font.ITALIC, Color.BLACK);

                    Font mbtimes = new Font(f_cn, 18, Font.BOLD, arrowred);
                    Font cbtimes = new Font(f_cn, 13, Font.BOLD, arrowred);

                    float[] columnWidths1585 = new float[] { 15, 85 };
                    float[] columnWidths2080 = new float[] { 20, 80 };

                    float[] columnWidths1783 = new float[] { 18, 82 };
                    float[] columnWidths7030 = new float[] { 50, 30, 20 };
                    float[] columnWidths2575 = new float[] { 25, 75 };
                    float[] columnWidths3070 = new float[] { 30, 70 };
                    float[] columnWidths4060 = new float[] { 40, 60 };
                    float[] columnWidths3565 = new float[] { 35, 65 };
                    float[] columnWidths4555 = new float[] { 45, 55 };
                    float[] columnWidths5545 = new float[] { 65, 35 };
                    float[] columnWidthsH = new float[] { 25, 60, 15 };

                    if (string.IsNullOrEmpty(userinfo.SiteName))
                        userinfo.SiteName = "N/A";

                    var mainheadertable = new PdfPTable(3);
                    mainheadertable.DefaultCell.Border = Rectangle.NO_BORDER;
                    mainheadertable.WidthPercentage = 100;
                    mainheadertable.SetWidths(columnWidthsH);
                    var headerMain = new PdfPCell();
                    if (userinfo.CustomerInfoId == 87 || userinfo.CustomerInfoId == 45)
                    {
                        headerMain.AddElement(new Phrase("G4S STAFF MONTHLY REPORT", mbtimes));
                    }
                    else
                    {
                        headerMain.AddElement(new Phrase("MIMS STAFF MONTHLY REPORT", mbtimes));
                    }
                    headerMain.PaddingBottom = 10;
                    headerMain.HorizontalAlignment = 1;
                    headerMain.Border = Rectangle.NO_BORDER;
                    PdfPCell pdfCell1 = new PdfPCell();
                    pdfCell1.Border = Rectangle.NO_BORDER;
                    PdfPCell pdfCell2 = new PdfPCell();
                    pdfCell2.Border = Rectangle.NO_BORDER;
                    mainheadertable.AddCell(pdfCell1);
                    mainheadertable.AddCell(headerMain);
                    mainheadertable.AddCell(pdfCell2);

                    doc.Add(mainheadertable);

                    var taskdataTable = new PdfPTable(2);
                    var taskdataTableA = new PdfPTable(2);
                    var taskdataTableB = new PdfPTable(2);
                    taskdataTable.DefaultCell.Border = Rectangle.NO_BORDER;
                    taskdataTable.WidthPercentage = 100;

                    taskdataTableA.DefaultCell.Border = Rectangle.NO_BORDER;
                    taskdataTableB.DefaultCell.Border = Rectangle.NO_BORDER;

                    taskdataTableA.WidthPercentage = 100;
                    taskdataTableB.WidthPercentage = 100;

                    taskdataTableA.SetWidths(columnWidths3565);

                    taskdataTableB.SetWidths(columnWidths3565);



                    var header3 = new PdfPCell();
                    header3.AddElement(new Phrase("Period:", beleventimes));
                    header3.Border = Rectangle.NO_BORDER;
                    taskdataTableA.AddCell(header3);

                    var header3a = new PdfPCell();
                    header3a.AddElement(new Phrase(fromDate + " to " + toDate, eleventimes));
                    header3a.Border = Rectangle.NO_BORDER;
                    taskdataTableA.AddCell(header3a);


                    var bheader1 = new PdfPCell();
                    bheader1.AddElement(new Phrase("Business Unit:", beleventimes));
                    bheader1.Border = Rectangle.NO_BORDER;
                    taskdataTableB.AddCell(bheader1);

                    var bheader1a = new PdfPCell();
                    bheader1a.AddElement(new Phrase(userinfo.SiteName, eleventimes));
                    bheader1a.Border = Rectangle.NO_BORDER;
                    taskdataTableB.AddCell(bheader1a);

                    taskdataTable.AddCell(taskdataTableA);
                    taskdataTable.AddCell(taskdataTableB);

                    doc.Add(taskdataTable);

                    var BtaskdataTable = new PdfPTable(2);
                    var BtaskdataTableA = new PdfPTable(2);
                    var BtaskdataTableB = new PdfPTable(2);
                    BtaskdataTable.DefaultCell.Border = Rectangle.NO_BORDER;
                    BtaskdataTable.WidthPercentage = 100;

                    BtaskdataTableA.DefaultCell.Border = Rectangle.NO_BORDER;
                    BtaskdataTableB.DefaultCell.Border = Rectangle.NO_BORDER;

                    BtaskdataTableA.WidthPercentage = 100;
                    BtaskdataTableB.WidthPercentage = 100;

                    BtaskdataTableA.SetWidths(columnWidths5545);

                    BtaskdataTableB.SetWidths(columnWidths5545);

                    var BTheader = new PdfPCell();
                    BTheader.AddElement(new Phrase("Performance Summary:", cbtimes));
                    BTheader.Border = Rectangle.NO_BORDER;
                    BTheader.Colspan = 2;
                    BtaskdataTable.AddCell(BTheader);

                    var workdaymax = staffData.Max(item => item.TotalDays);
                    var workedmax = staffData.Max(item => item.DutyOn);
                    var activemax = staffData.Max(item => item.TaskOn);
                    var assmax = staffData.Max(item => item.TotalAssignment);
                    var utilizationmax = staffData.Max(item => item.UtilizationAverage);
                    var amendmentmax = staffData.Max(item => item.Amendment);

                    var sstaffData = staffData.Where(p => p.TotalDays > 0).ToList();

                    var getdurationlist = new List<double>();

                    var traveldurationlist = new List<double>();

                    foreach (var u in sstaffData)
                    {
                        var d = CommonUtility.GetUserOTHours(u.EmployeeId, fromdate, todate);
                        getdurationlist.Add(d);

                        var tevents = TaskEventHistory.GetTaskEventHistoryByUser(u.EmployeeName, dbConnection);

                        //tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                        tevents = tevents.Where(i => i.CreatedDate.Value.Date >= fromdate.Date && i.CreatedDate.Value.Date <= todate.Date).ToList();

                        var groupedtevents = tevents.GroupBy(item => item.Id);
                        tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                        var ttraveldurationlist = 0.0;

                        //foreach (var t in tevents)
                        //{
                        //    var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                        //    if (task != null)
                        //   {
                        var tt = CommonUtility.GetTravelTimeByTaskByDuration(tevents);
                        ttraveldurationlist = tt + ttraveldurationlist;
                        //    }
                        // }
                        traveldurationlist.Add(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist));
                    }
                    var otmax = 0.0;
                    var ttmax = 0.0;

                    if (getdurationlist.Count > 0)
                        otmax = getdurationlist.Max();

                    if (traveldurationlist.Count > 0)
                        ttmax = traveldurationlist.Max();

                    var ABTheader1 = new PdfPCell();
                    ABTheader1.AddElement(new Phrase("Worked Days(max):", beleventimes));
                    ABTheader1.Border = Rectangle.NO_BORDER;
                    BtaskdataTableA.AddCell(ABTheader1);

                    var ABTheader1a = new PdfPCell();
                    ABTheader1a.AddElement(new Phrase(workdaymax.ToString(), eleventimes));
                    ABTheader1a.Border = Rectangle.NO_BORDER;
                    BtaskdataTableA.AddCell(ABTheader1a);

                    var ABTheader2 = new PdfPCell();
                    ABTheader2.AddElement(new Phrase("Worked Hours(max):", beleventimes));
                    ABTheader2.Border = Rectangle.NO_BORDER;
                    BtaskdataTableA.AddCell(ABTheader2);

                    var ABTheader2a = new PdfPCell();
                    ABTheader2a.AddElement(new Phrase(workedmax.ToString(), eleventimes));
                    ABTheader2a.Border = Rectangle.NO_BORDER;
                    BtaskdataTableA.AddCell(ABTheader2a);

                    var ABTheader3 = new PdfPCell();
                    ABTheader3.AddElement(new Phrase("Active Hours(max):", beleventimes));
                    ABTheader3.Border = Rectangle.NO_BORDER;
                    BtaskdataTableA.AddCell(ABTheader3);

                    var ABTheader3a = new PdfPCell();
                    ABTheader3a.AddElement(new Phrase(activemax.ToString(), eleventimes));
                    ABTheader3a.Border = Rectangle.NO_BORDER;
                    BtaskdataTableA.AddCell(ABTheader3a);

                    var ABTheader4 = new PdfPCell();
                    ABTheader4.AddElement(new Phrase("Assignments(max):", beleventimes));
                    ABTheader4.Border = Rectangle.NO_BORDER;
                    BtaskdataTableA.AddCell(ABTheader4);

                    var ABTheader4a = new PdfPCell();
                    ABTheader4a.AddElement(new Phrase(assmax.ToString(), eleventimes));
                    ABTheader4a.Border = Rectangle.NO_BORDER;
                    BtaskdataTableA.AddCell(ABTheader4a);

                    var ABTheader5 = new PdfPCell();
                    ABTheader5.AddElement(new Phrase("Utilization(max):", beleventimes));
                    ABTheader5.Border = Rectangle.NO_BORDER;
                    BtaskdataTableA.AddCell(ABTheader5);

                    var ABTheader5a = new PdfPCell();
                    ABTheader5a.AddElement(new Phrase(utilizationmax.ToString() + "%", eleventimes));
                    ABTheader5a.Border = Rectangle.NO_BORDER;
                    BtaskdataTableA.AddCell(ABTheader5a);

                    var ABTheader6TT = new PdfPCell();
                    ABTheader6TT.AddElement(new Phrase("Travel Time(max):", beleventimes));
                    ABTheader6TT.Border = Rectangle.NO_BORDER;
                    BtaskdataTableA.AddCell(ABTheader6TT);

                    var ABTheader6aTT = new PdfPCell();
                    ABTheader6aTT.AddElement(new Phrase(ttmax.ToString(), eleventimes));
                    ABTheader6aTT.Border = Rectangle.NO_BORDER;
                    BtaskdataTableA.AddCell(ABTheader6aTT);

                    var ABTheader6OT = new PdfPCell();
                    ABTheader6OT.AddElement(new Phrase("Overtime(max):", beleventimes));
                    ABTheader6OT.Border = Rectangle.NO_BORDER;
                    BtaskdataTableA.AddCell(ABTheader6OT);

                    var ABTheader6aOT = new PdfPCell();
                    ABTheader6aOT.AddElement(new Phrase(otmax.ToString(), eleventimes));
                    ABTheader6aOT.Border = Rectangle.NO_BORDER;
                    BtaskdataTableA.AddCell(ABTheader6aOT);

                    var ABTheader6 = new PdfPCell();
                    ABTheader6.AddElement(new Phrase("Amendment(max):", beleventimes));
                    ABTheader6.Border = Rectangle.NO_BORDER;
                    ABTheader6.PaddingBottom = 10;
                    BtaskdataTableA.AddCell(ABTheader6);

                    var ABTheader6a = new PdfPCell();
                    ABTheader6a.AddElement(new Phrase(amendmentmax.ToString(), eleventimes));
                    ABTheader6a.Border = Rectangle.NO_BORDER;
                    BtaskdataTableA.AddCell(ABTheader6a);
                     
                    var workdaysaverage = sstaffData.Count == 0 ? 0 : Math.Round(sstaffData.Average(item => item.TotalDays), 2);
                    var workedaverage = sstaffData.Count == 0 ? 0 : Math.Round(sstaffData.Average(item => item.DutyOn), 2);
                    var activeaverage = sstaffData.Count == 0 ? 0 : Math.Round(sstaffData.Average(item => item.TaskOn), 2);
                    var assaverage = sstaffData.Count == 0 ? 0 : Math.Round(sstaffData.Average(item => item.TotalAssignment), 2);
                    var utilizationaverage = sstaffData.Count == 0 ? 0 : Math.Round(sstaffData.Average(item => item.UtilizationAverage), 2);
                    var amendaverage = sstaffData.Count == 0 ? 0 : Math.Round(sstaffData.Average(item => item.Amendment), 2);

                    var otaverage = getdurationlist.Count == 0 ? 0 : Math.Round(getdurationlist.Average(), 2);
                    var ttaverage = traveldurationlist.Count == 0 ? 0 : Math.Round(traveldurationlist.Average(), 2);

                    var BBTheader1 = new PdfPCell();
                    BBTheader1.AddElement(new Phrase("Worked Days(average):", beleventimes));
                    BBTheader1.Border = Rectangle.NO_BORDER;
                    BtaskdataTableB.AddCell(BBTheader1);

                    var BBTheader1a = new PdfPCell();
                    BBTheader1a.AddElement(new Phrase(workdaysaverage.ToString(), eleventimes));
                    BBTheader1a.Border = Rectangle.NO_BORDER;
                    BtaskdataTableB.AddCell(BBTheader1a);


                    var BBTheader2 = new PdfPCell();
                    BBTheader2.AddElement(new Phrase("Worked Hours(average):", beleventimes));
                    BBTheader2.Border = Rectangle.NO_BORDER;
                    BtaskdataTableB.AddCell(BBTheader2);

                    var BBTheader2a = new PdfPCell();
                    BBTheader2a.AddElement(new Phrase(workedaverage.ToString(), eleventimes));
                    BBTheader2a.Border = Rectangle.NO_BORDER;
                    BtaskdataTableB.AddCell(BBTheader2a);

                    var BBTheader3 = new PdfPCell();
                    BBTheader3.AddElement(new Phrase("Active Hours(average):", beleventimes));
                    BBTheader3.Border = Rectangle.NO_BORDER;
                    BtaskdataTableB.AddCell(BBTheader3);

                    var BBTheader3a = new PdfPCell();
                    BBTheader3a.AddElement(new Phrase(activeaverage.ToString(), eleventimes));
                    BBTheader3a.Border = Rectangle.NO_BORDER;
                    BtaskdataTableB.AddCell(BBTheader3a);

                    var BBTheader5 = new PdfPCell();
                    BBTheader5.AddElement(new Phrase("Assignments(average):", beleventimes));
                    BBTheader5.Border = Rectangle.NO_BORDER;
                    BtaskdataTableB.AddCell(BBTheader5);

                    var BBTheader5a = new PdfPCell();
                    BBTheader5a.AddElement(new Phrase(assaverage.ToString(), eleventimes));
                    BBTheader5a.Border = Rectangle.NO_BORDER;
                    BtaskdataTableB.AddCell(BBTheader5a);

                    var BBTheader4 = new PdfPCell();
                    BBTheader4.AddElement(new Phrase("Utilization(average):", beleventimes));
                    BBTheader4.Border = Rectangle.NO_BORDER;
                    BtaskdataTableB.AddCell(BBTheader4);

                    var BBTheader4a = new PdfPCell();
                    BBTheader4a.AddElement(new Phrase(utilizationaverage.ToString() + "%", eleventimes));
                    BBTheader4a.Border = Rectangle.NO_BORDER;
                    BtaskdataTableB.AddCell(BBTheader4a);

                    var BBTheader6TT = new PdfPCell();
                    BBTheader6TT.AddElement(new Phrase("Travel Time(average):", beleventimes));
                    BBTheader6TT.Border = Rectangle.NO_BORDER;
                    BtaskdataTableB.AddCell(BBTheader6TT);

                    var BBTheader6aTT = new PdfPCell();
                    BBTheader6aTT.AddElement(new Phrase(ttaverage.ToString(), eleventimes));
                    BBTheader6aTT.Border = Rectangle.NO_BORDER;
                    BtaskdataTableB.AddCell(BBTheader6aTT);

                    var BBTheader6OT = new PdfPCell();
                    BBTheader6OT.AddElement(new Phrase("Overtime(average):", beleventimes));
                    BBTheader6OT.Border = Rectangle.NO_BORDER;
                    BtaskdataTableB.AddCell(BBTheader6OT);

                    var BBTheader6aOT = new PdfPCell();
                    BBTheader6aOT.AddElement(new Phrase(otaverage.ToString(), eleventimes));
                    BBTheader6aOT.Border = Rectangle.NO_BORDER;
                    BtaskdataTableB.AddCell(BBTheader6aOT);

                    var BBTheader6 = new PdfPCell();
                    BBTheader6.AddElement(new Phrase("Amendment(average):", beleventimes));
                    BBTheader6.Border = Rectangle.NO_BORDER;
                    BBTheader6.PaddingBottom = 10;
                    BtaskdataTableB.AddCell(BBTheader6);

                    var BBTheader6a = new PdfPCell();
                    BBTheader6a.AddElement(new Phrase(amendaverage.ToString(), eleventimes));
                    BBTheader6a.Border = Rectangle.NO_BORDER;
                    BtaskdataTableB.AddCell(BBTheader6a);

                    BtaskdataTable.AddCell(BtaskdataTableA);
                    BtaskdataTable.AddCell(BtaskdataTableB);

                    doc.Add(BtaskdataTable);

                    var tiplistTable = new PdfPTable(9);
                    tiplistTable.DefaultCell.Border = Rectangle.NO_BORDER;
                    tiplistTable.WidthPercentage = 100;

                    var tipaheader1 = new PdfPCell();
                    tipaheader1.AddElement(new Phrase("Employee", weighttimes));
                    tipaheader1.Border = Rectangle.NO_BORDER;
                    tipaheader1.PaddingLeft = 5;
                    tipaheader1.PaddingBottom = 5;
                    tipaheader1.BackgroundColor = Color.GRAY;
                    tiplistTable.AddCell(tipaheader1);

                    var tipaheader2 = new PdfPCell();
                    tipaheader2.AddElement(new Phrase("Worked Days", weighttimes));
                    tipaheader2.Border = Rectangle.NO_BORDER;
                    tipaheader2.BackgroundColor = Color.GRAY;
                    tiplistTable.AddCell(tipaheader2);

                    var tipaheader3 = new PdfPCell();
                    tipaheader3.AddElement(new Phrase("Duty Hours", weighttimes));
                    tipaheader3.Border = Rectangle.NO_BORDER;
                    tipaheader3.BackgroundColor = Color.GRAY;
                    tiplistTable.AddCell(tipaheader3);

                    var tipaheader4 = new PdfPCell();
                    tipaheader4.AddElement(new Phrase("Active Hours", weighttimes));
                    tipaheader4.Border = Rectangle.NO_BORDER;
                    tipaheader4.BackgroundColor = Color.GRAY;
                    tiplistTable.AddCell(tipaheader4);

                    var tipaheader5 = new PdfPCell();
                    tipaheader5.AddElement(new Phrase("Utilization", weighttimes));
                    tipaheader5.Border = Rectangle.NO_BORDER;
                    tipaheader5.BackgroundColor = Color.GRAY;
                    tiplistTable.AddCell(tipaheader5);

                    var tipaheader6 = new PdfPCell();
                    tipaheader6.AddElement(new Phrase("Assignments", weighttimes));
                    tipaheader6.Border = Rectangle.NO_BORDER;
                    tipaheader6.BackgroundColor = Color.GRAY;
                    tiplistTable.AddCell(tipaheader6);

                    var tipaheader9 = new PdfPCell();
                    tipaheader9.AddElement(new Phrase("Travel time", weighttimes));
                    tipaheader9.Border = Rectangle.NO_BORDER;
                    tipaheader9.BackgroundColor = Color.GRAY;
                    tiplistTable.AddCell(tipaheader9);

                    var tipaheader8 = new PdfPCell();
                    tipaheader8.AddElement(new Phrase("Overtime", weighttimes));
                    tipaheader8.Border = Rectangle.NO_BORDER;
                    tipaheader8.BackgroundColor = Color.GRAY;
                    tiplistTable.AddCell(tipaheader8);

                    var tipaheader7 = new PdfPCell();
                    tipaheader7.AddElement(new Phrase("Amendment", weighttimes));
                    tipaheader7.Border = Rectangle.NO_BORDER;
                    tipaheader7.BackgroundColor = Color.GRAY;
                    tiplistTable.AddCell(tipaheader7);

                    var odd = false;
                    foreach (var device in staffData)
                    {
                        var xheaderCell = new PdfPCell(new Phrase(device.FirstName + " " + device.LastName, seventimes));
                        var xheaderCell2 = new PdfPCell(new Phrase(device.TotalDays.ToString(), seventimes));
                        var xheaderCell3 = new PdfPCell(new Phrase(device.DutyOn.ToString(), seventimes));
                        var xheaderCell4 = new PdfPCell(new Phrase(device.TaskOn.ToString(), seventimes));
                        var xheaderCell5 = new PdfPCell(new Phrase(device.UtilizationAverage.ToString() + "%", seventimes));
                        var xheaderCell6 = new PdfPCell(new Phrase(device.TotalAssignment.ToString(), seventimes));

                        var d = CommonUtility.GetUserOTHours(device.EmployeeId, fromdate, todate);

                        var xheaderCelld = new PdfPCell(new Phrase(d.ToString(), seventimes));

                        var tevents = TaskEventHistory.GetTaskEventHistoryByUser(device.EmployeeName, dbConnection);

                        //tevents = tevents.Where(i => i.Action == (int)TaskAction.InProgress).ToList();

                        tevents = tevents.Where(i => i.CreatedDate.Value.Date >= fromdate && i.CreatedDate.Value.Date <= todate.AddHours(23)).ToList();

                        var groupedtevents = tevents.GroupBy(item => item.Id);
                        tevents = groupedtevents.Select(grp => grp.OrderBy(item => item.TaskId).First()).ToList();

                        var ttraveldurationlist = 0.0;

                       // foreach (var t in tevents)
                       // {
                        //    var task = UserTask.GetTaskById(t.TaskId, dbConnection);
                        //    if (task != null)
                        //    {
                             var tt = CommonUtility.GetTravelTimeByTaskByDuration(tevents);
                             ttraveldurationlist = ttraveldurationlist + tt;
                         //   }
                    //    }
                    
                        var xheaderCelltt = new PdfPCell(new Phrase(CommonUtility.MinutesToHoursAndMinutes(ttraveldurationlist).ToString(), seventimes));

                        var xheaderCell7 = new PdfPCell(new Phrase(device.Amendment.ToString(), seventimes));


                        xheaderCelltt.Border = Rectangle.NO_BORDER;
                        xheaderCelld.Border = Rectangle.NO_BORDER;

                        xheaderCell.Border = Rectangle.NO_BORDER;
                        xheaderCell2.Border = Rectangle.NO_BORDER;
                        xheaderCell3.Border = Rectangle.NO_BORDER;
                        xheaderCell4.Border = Rectangle.NO_BORDER;
                        xheaderCell5.Border = Rectangle.NO_BORDER;
                        xheaderCell6.Border = Rectangle.NO_BORDER;
                        xheaderCell7.Border = Rectangle.NO_BORDER;

                        xheaderCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCell7.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        xheaderCelltt.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
                        xheaderCelld.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                        if (!odd)
                        {
                            xheaderCell.BackgroundColor = Color.LIGHT_GRAY;
                            xheaderCell2.BackgroundColor = Color.LIGHT_GRAY;
                            xheaderCell3.BackgroundColor = Color.LIGHT_GRAY;
                            xheaderCell4.BackgroundColor = Color.LIGHT_GRAY;
                            xheaderCell5.BackgroundColor = Color.LIGHT_GRAY;
                            xheaderCell6.BackgroundColor = Color.LIGHT_GRAY;
                            xheaderCell7.BackgroundColor = Color.LIGHT_GRAY;

                            xheaderCelltt.BackgroundColor = Color.LIGHT_GRAY;
                            xheaderCelld.BackgroundColor = Color.LIGHT_GRAY;

                            odd = true;
                        }
                        else
                        {
                            odd = false;
                        }
                        xheaderCell.Border = Rectangle.NO_BORDER;
                        xheaderCell2.Border = Rectangle.NO_BORDER;
                        xheaderCell3.Border = Rectangle.NO_BORDER;
                        xheaderCell4.Border = Rectangle.NO_BORDER;
                        xheaderCell5.Border = Rectangle.NO_BORDER;
                        xheaderCell6.Border = Rectangle.NO_BORDER;
                        xheaderCell7.Border = Rectangle.NO_BORDER;

                        xheaderCelltt.Border = Rectangle.NO_BORDER;
                        xheaderCelld.Border = Rectangle.NO_BORDER;

                        tiplistTable.AddCell(xheaderCell);
                        tiplistTable.AddCell(xheaderCell2);
                        tiplistTable.AddCell(xheaderCell3);
                        tiplistTable.AddCell(xheaderCell4);
                        tiplistTable.AddCell(xheaderCell5);
                        tiplistTable.AddCell(xheaderCell6);
                        tiplistTable.AddCell(xheaderCelltt);
                        tiplistTable.AddCell(xheaderCelld); 
                        tiplistTable.AddCell(xheaderCell7);
                    }
                    doc.Add(tiplistTable);


                    Font geleventimes = new Font(f_cn, 11, Font.NORMAL, Color.GRAY);
                    var endTable = new PdfPTable(1);
                    endTable.DefaultCell.Border = Rectangle.NO_BORDER;
                    endTable.WidthPercentage = 100;

                    var endheader = new PdfPCell();
                    endheader.AddElement(new Phrase("Report generated: " + CommonUtility.getDTNow().AddHours(userinfo.TimeZone).ToString() + " 	by: " + userinfo.Username, geleventimes));
                    endheader.Border = Rectangle.NO_BORDER;
                    endTable.AddCell(endheader);
                    doc.Add(endTable);

                    doc.Close();

                    Response.ContentType = "application/pdf";
                    Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.Write(doc);
                    Response.End();
                }
                else
                {
                    Response.Write("<script>alert('No entries found')</script>");
                }
            }
            catch (Exception ex)
            {
                if (userinfo != null)
                    MIMSLog.MIMSLogSave("Reports", "btnExport4_Click", ex, dbConnection, userinfo.SiteId, userinfo.CustomerInfoId);
                else
                    MIMSLog.MIMSLogSave("Reports", "btnExport4_Click", ex, dbConnection);  
            }
        }
    }
}