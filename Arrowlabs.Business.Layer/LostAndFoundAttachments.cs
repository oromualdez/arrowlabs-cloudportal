﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Business.Layer
{
  public  class LostAndFoundAttachments
    {
        public int Id { get; set; }
        public int ItemDisposeId { get; set; }
        public int ItemFoundId { get; set; }
        public int ItemInquiryId { get; set; }
        public int ItemOwnerId { get; set; }  
        public string ImagePath { get; set; }
        public int SiteId { get; set; }
        public int CustomerId { get; set; }     
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

        private static LostAndFoundAttachments GetLostAndFoundAttachmentsFromSqlReader(SqlDataReader reader)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
            var lostAndFoundAttachment = new LostAndFoundAttachments();
            if (reader["Id"] != DBNull.Value)
                lostAndFoundAttachment.Id = Convert.ToInt32(reader["Id"].ToString());
            if (reader["ItemDisposeId"] != DBNull.Value)
                lostAndFoundAttachment.ItemDisposeId = Convert.ToInt32(reader["ItemDisposeId"].ToString());
            if (reader["ItemFoundId"] != DBNull.Value)
                lostAndFoundAttachment.ItemFoundId = Convert.ToInt32(reader["ItemFoundId"].ToString());
            if (reader["ItemInquiryId"] != DBNull.Value)
                lostAndFoundAttachment.ItemInquiryId = Convert.ToInt32(reader["ItemInquiryId"].ToString());
            if (reader["ItemOwnerId"] != DBNull.Value)
                lostAndFoundAttachment.ItemOwnerId = Convert.ToInt32(reader["ItemOwnerId"].ToString());
            if (reader["SiteId"] != DBNull.Value)
                lostAndFoundAttachment.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
            if (reader["CustomerId"] != DBNull.Value)
                lostAndFoundAttachment.CustomerId = Convert.ToInt32(reader["CustomerId"].ToString());         
            


            if (reader["CreatedDate"] != DBNull.Value)
                lostAndFoundAttachment.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            if (reader["CreatedBy"] != DBNull.Value)
                lostAndFoundAttachment.CreatedBy = reader["CreatedBy"].ToString();
            if (reader["UpdatedDate"] != DBNull.Value)
                lostAndFoundAttachment.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"].ToString());
            if (reader["UpdatedBy"] != DBNull.Value)
                lostAndFoundAttachment.UpdatedBy = reader["UpdatedBy"].ToString();
            if (reader["ImagePath"] != DBNull.Value)
                lostAndFoundAttachment.ImagePath = reader["ImagePath"].ToString();
            return lostAndFoundAttachment;
        }
        public static List<LostAndFoundAttachments> GetAttachmentsbyItemDisposeId(int itemDisposeId, string dbConnection)
        {
            var lostAndFoundAttachments = new List<LostAndFoundAttachments>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("GetAttachmentsbyItemDisposeId", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@ItemDisposeId", SqlDbType.Int));
                    sqlCommand.Parameters["@ItemDisposeId"].Value = itemDisposeId;
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        var lostAndFoundAttachment = GetLostAndFoundAttachmentsFromSqlReader(reader);
                        lostAndFoundAttachments.Add(lostAndFoundAttachment);
                    }
                    connection.Close();
                    reader.Close();
                }

            }
            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("LostAndFoundAttachments", "GetAttachmentsbyItemDisposeId", exp, dbConnection);

            }
            return lostAndFoundAttachments;
        }
        public static List<LostAndFoundAttachments> GetAttachmentsByItemFoundId(int itemFoundId, string dbConnection)
        {
            var lostAndFoundAttachments = new List<LostAndFoundAttachments>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("GetAttachmentsByItemFoundId", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@ItemFoundId", SqlDbType.Int));
                    sqlCommand.Parameters["@ItemFoundId"].Value = itemFoundId;
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        var lostAndFoundAttachment = GetLostAndFoundAttachmentsFromSqlReader(reader);
                        lostAndFoundAttachments.Add(lostAndFoundAttachment);
                    }
                    connection.Close();
                    reader.Close();
                }

            }
            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("LostAndFoundAttachments", "GetAttachmentsByItemFoundId", exp, dbConnection);

            }
            return lostAndFoundAttachments;
        }
        public static List<LostAndFoundAttachments> GetAttachmentsByItemInquiryId(int itemOwnerId, string dbConnection)
        {
            var lostAndFoundAttachments = new List<LostAndFoundAttachments>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("GetAttachmentsByItemInquiryId", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@ItemOwnerId", SqlDbType.Int));
                    sqlCommand.Parameters["@ItemOwnerId"].Value = itemOwnerId;
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        var lostAndFoundAttachment = GetLostAndFoundAttachmentsFromSqlReader(reader);
                        lostAndFoundAttachments.Add(lostAndFoundAttachment);
                    }
                    connection.Close();
                    reader.Close();
                }

            }
            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("LostAndFoundAttachments", "GetAttachmentsByItemInquiryId", exp, dbConnection);

            }
            return lostAndFoundAttachments;
        }
        public static List<LostAndFoundAttachments> GetAttachmentsByItemOwnerId(int itemOwnerId, string dbConnection)
        {
            var lostAndFoundAttachments = new List<LostAndFoundAttachments>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("GetAttachmentsByItemOwnerId", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@ItemOwnerId", SqlDbType.Int));
                    sqlCommand.Parameters["@ItemOwnerId"].Value = itemOwnerId;
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        var lostAndFoundAttachment = GetLostAndFoundAttachmentsFromSqlReader(reader);
                        lostAndFoundAttachments.Add(lostAndFoundAttachment);
                    }
                    connection.Close();
                    reader.Close();
                }

            }
            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("LostAndFoundAttachments", "GetAttachmentsByItemOwnerId", exp, dbConnection);

            }
            return lostAndFoundAttachments;
        }
        public static List<LostAndFoundAttachments> GetLostAndFoundAttachmentsByCreatedUser(string Name, string dbConnection)
        {
            var lostAndFoundAttachments = new List<LostAndFoundAttachments>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("GetLostAndFoundAttachmentsByCreatedUser", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@Name"].Value = Name;
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        var lostAndFoundAttachment = GetLostAndFoundAttachmentsFromSqlReader(reader);
                        lostAndFoundAttachments.Add(lostAndFoundAttachment);
                    }
                    connection.Close();
                    reader.Close();
                }

            }
            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("LostAndFoundAttachments", "GetAttachmentsByItemOwnerId", exp, dbConnection);

            }
            return lostAndFoundAttachments;
        }
        public static List<LostAndFoundAttachments> GetLostAndFoundAttachmentsBySiteId(int siteId, string dbConnection)
        {
            var lostAndFoundAttachments = new List<LostAndFoundAttachments>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("GetLostAndFoundAttachmentsBySiteId", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    sqlCommand.Parameters["@SiteId"].Value = siteId;
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        var lostAndFoundAttachment = GetLostAndFoundAttachmentsFromSqlReader(reader);
                        lostAndFoundAttachments.Add(lostAndFoundAttachment);
                    }
                    connection.Close();
                    reader.Close();
                }

            }
            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("LostAndFoundAttachments", "GetAttachmentsByItemOwnerId", exp, dbConnection);

            }
            return lostAndFoundAttachments;
        }
        public static bool DeleteLostAndFoundAttachmentsById(int id, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteLostAndFoundAttachmentsById", connection);

                cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                cmd.Parameters["@Id"].Value = id;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.ExecuteNonQuery();

            }
            return true;
        }
        public static int InsertorUpdateLostAndFoundAttachments(LostAndFoundAttachments lostAndFoundAttachment, string dbConnection)
        {
            int id = 0;
            if (lostAndFoundAttachment != null)
            {
                id = lostAndFoundAttachment.Id;
               
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertorUpdateLostAndFoundAttachments", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = lostAndFoundAttachment.Id;
                    cmd.Parameters.Add(new SqlParameter("@ItemDisposeId", SqlDbType.Int));
                    cmd.Parameters["@ItemDisposeId"].Value = lostAndFoundAttachment.ItemDisposeId;
                    cmd.Parameters.Add(new SqlParameter("@ItemFoundId", SqlDbType.Int));
                    cmd.Parameters["@ItemFoundId"].Value = lostAndFoundAttachment.ItemFoundId;
                    cmd.Parameters.Add(new SqlParameter("@ItemInquiryId", SqlDbType.Int));
                    cmd.Parameters["@ItemInquiryId"].Value = lostAndFoundAttachment.ItemInquiryId;
                    cmd.Parameters.Add(new SqlParameter("@ItemOwnerId", SqlDbType.Int));
                    cmd.Parameters["@ItemOwnerId"].Value = lostAndFoundAttachment.ItemOwnerId;
                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = lostAndFoundAttachment.SiteId;
                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = lostAndFoundAttachment.CustomerId;     
                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = lostAndFoundAttachment.CreatedDate;
                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = lostAndFoundAttachment.UpdatedDate;
                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = lostAndFoundAttachment.CreatedBy;
                    cmd.Parameters.Add(new SqlParameter("@ImagePath", SqlDbType.NVarChar));
                    cmd.Parameters["@ImagePath"].Value = lostAndFoundAttachment.ImagePath;
                    cmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@UpdatedBy"].Value = lostAndFoundAttachment.UpdatedBy;

                    var returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                    if (id == 0)
                        id = (int)returnParameter.Value;
                }
            }
            return id;
        }
    }
       
}
