﻿// -----------------------------------------------------------------------
// <copyright file="Role.cs" company="Microsoft">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Arrowlabs.Business.Layer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public enum Role
    {
        SuperAdmin = 0, //Level 7
        Admin      = 1, //Level 3
        Manager    = 2, //Level 2
        Operator   = 3, //Level 1
        WebportalAccount = 4,
        UnassignedOperator = 5, //Level 1
        Director = 6, //Level 4
        Regional = 7, //Level 5
        ChiefOfficer = 8, //Level 6
        CustomerSuperadmin = 9, //Level 6
        CustomerUser = 10,
        MessageBoardUser = 11
    }
}
