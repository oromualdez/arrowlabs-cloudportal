﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class TaskTravelHistoryAudit
    {
        public BaseAudit BaseAudit { get; set; }

        public int Id { get; set; }
        public int TaskId { get; set; }
        public int Action { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int SiteId { get; set; }
        public int CustomerId { get; set; }
        public float Latitude { get; set; }
        public float Longtitude { get; set; }

        public static bool InsertTaskTravelHistoryAudit(TaskTravelHistoryAudit eventHistory, string dbConnection)
        {
            var baseAuditId = BaseAudit.InsertBaseAudit(eventHistory.BaseAudit, dbConnection);

            if (eventHistory != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertTaskTravelHistoryAudit", connection);

                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;

                    cmd.Parameters.Add(new SqlParameter("@TaskId", SqlDbType.Int));
                    cmd.Parameters["@TaskId"].Value = eventHistory.TaskId;
             
                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = eventHistory.CustomerId;

                    cmd.Parameters.Add(new SqlParameter("@Action", SqlDbType.Int));
                    cmd.Parameters["@Action"].Value = eventHistory.Action;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = eventHistory.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = eventHistory.CreatedDate;
                    
                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = eventHistory.SiteId;
                    
                    cmd.Parameters.Add(new SqlParameter("@Longitude", SqlDbType.Float));
                    cmd.Parameters["@Longitude"].Value = eventHistory.Longtitude;

                    cmd.Parameters.Add(new SqlParameter("@Latitude", SqlDbType.Float));
                    cmd.Parameters["@Latitude"].Value = eventHistory.Latitude;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }
    }
}
