﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class VerifierResultsAudit
    {
        public BaseAudit BaseAudit { get; set; } 
        public int Id { get; set; }
        public int VerifierID { get; set; }
        public int CaseId { get; set; }
        public float Percent { get; set; }
        public string FilePath { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int SiteId { get; set; }

        public static bool InsertOrUpdateVerifierResult(VerifierResultsAudit verifierResult, string dbConnection)
        {
            var baseAuditId = BaseAudit.InsertBaseAudit(verifierResult.BaseAudit, dbConnection);
            if (verifierResult != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateVerifierResult", connection);

                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = verifierResult.Id;

                    cmd.Parameters.Add(new SqlParameter("@VerifierID", SqlDbType.Int));
                    cmd.Parameters["@VerifierID"].Value = verifierResult.VerifierID;

                    cmd.Parameters.Add(new SqlParameter("@CaseId", SqlDbType.Int));
                    cmd.Parameters["@CaseId"].Value = verifierResult.CaseId;

                    cmd.Parameters.Add(new SqlParameter("@Percent", SqlDbType.Float));
                    cmd.Parameters["@Percent"].Value = verifierResult.Percent;

                    cmd.Parameters.Add(new SqlParameter("@FilePath", SqlDbType.NVarChar));
                    cmd.Parameters["@FilePath"].Value = verifierResult.FilePath;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = verifierResult.CreatedDate;
                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = verifierResult.SiteId;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }
    }
}
