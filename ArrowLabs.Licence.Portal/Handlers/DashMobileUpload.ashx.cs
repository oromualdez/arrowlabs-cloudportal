﻿using Arrowlabs.Business.Layer;
using ArrowLabs.Licence.Portal.Helpers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
namespace ArrowLabs.Licence.Portal.Handlers
{
    /// <summary>
    /// Summary description for DashMobileUpload
    /// </summary>
    public class DashMobileUpload : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                string ImeTvrtke = context.Request["Data"];
                if (!string.IsNullOrEmpty(ImeTvrtke))
                {
                    var uname = ImeTvrtke.Split('"')[3];
                    var siteId = Users.GetUserByEncryptedName(uname, CommonUtility.dbConnection);
                    if (context.Request.Files.Count > 0)
                    {
                        HttpFileCollection files = context.Request.Files;
                        string fname = string.Empty;
                        for (int i = 0; i < files.Count; i++)
                        {
                            HttpPostedFile file = files[i];
                            fname = context.Server.MapPath("~/Uploads/" + file.FileName);
                            file.SaveAs(fname);
                        }
                        System.Drawing.Image imageVar = System.Drawing.Image.FromFile(fname);
                        if (imageVar.Height == 1080 && imageVar.Width == 1920)
                        {
                            if (File.Exists(fname))
                            {
                                Stream fs = File.OpenRead(fname);
                                if (siteId.RoleId == (int)Role.CustomerSuperadmin)
                                {
                                    var getFileName = "dashboard-imageC" + siteId.CustomerInfoId + ".jpeg";
                                    var savestring = CommonUtility.CloudUploadFile(0, getFileName, fs);
                                    if (savestring != "FAIL")
                                    {

                                    }
                                    else
                                    {

                                    }
                                    if (fs != null)
                                    {
                                        fs.Close();
                                    }
                                }
                                else
                                {
                                    var getFileName = "dashboard-image" + siteId.SiteId + ".jpeg";
                                    var savestring = CommonUtility.CloudUploadFile(0, getFileName, fs);
                                    if (savestring != "FAIL")
                                    {

                                    }
                                    else
                                    {

                                    }
                                    if (fs != null)
                                    {
                                        fs.Close();
                                    }
                                }
                                //if (File.Exists(fname))
                                //    File.Delete(fname);
                            }
                            //var mimcon = MIMSConfigSetting.GetMIMSConfigSettings(CommonUtility.dbConnection);
                            //var consplit = mimcon.FilePath.Split('\\');
                            //var newstring = string.Empty;
                            //var count = 0;
                            //foreach (var split in consplit)
                            //{
                            //    if (count < 4)
                            //    {
                            //        newstring += split + "\\";
                            //        count++;
                            //    }
                            //    else
                            //    {
                            //        break;
                            //    }
                            //}
                            //HttpPostedFile file = files[0];
                            //newstring = newstring + "Content\\Theme-2.2\\images\\custom-images\\dashboard-image" + siteId + ".jpeg";
                            //file.SaveAs(newstring);
                            context.Response.ContentType = "text/plain";
                            context.Response.Write("Success");
                        }
                        else
                        {
                            context.Response.ContentType = "text/plain";
                            context.Response.Write("ImageFail");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("DashMobileUpload", "ProcessRequest", ex, CommonUtility.dbConnection);
                context.Response.Write("Error 38: Problem occured while trying to upload photo.-" + ex.Message);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}