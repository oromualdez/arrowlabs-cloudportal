﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class VehicleMakeAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public int ID { get; set; }
        public string VehicleMakeDesc { get; set; }
        public string VehicleMakeDesc_AR { get; set; }
        public string VehicleMakeCode { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public int SiteId { get; set; }
        public int CustomerId { get; set; }
        public static VehicleMakeAudit GetVehicleMakeAuditFromSqlReader(SqlDataReader reader)
        {
            var vehicleMakeAudit = new VehicleMakeAudit();
            vehicleMakeAudit.BaseAudit = BaseAudit.GetBaseAuditFromSqlReader(reader);
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

            if (columns.Contains("Id") && reader["Id"] != DBNull.Value)
                vehicleMakeAudit.ID = Convert.ToInt32(reader["Id"].ToString());
            if (columns.Contains("VEHICLEMAKEDESC") && reader["VEHICLEMAKEDESC"] != DBNull.Value)
                vehicleMakeAudit.VehicleMakeDesc = reader["VEHICLEMAKEDESC"].ToString();
            if (columns.Contains("VEHICLEMAKEDESC_AR") && reader["VEHICLEMAKEDESC_AR"] != DBNull.Value)
                vehicleMakeAudit.VehicleMakeDesc_AR = reader["VEHICLEMAKEDESC_AR"].ToString();
            if (columns.Contains("VEHICLEMAKECODE") && reader["VEHICLEMAKECODE"] != DBNull.Value)
                vehicleMakeAudit.VehicleMakeCode = reader["VEHICLEMAKECODE"].ToString();
            if (columns.Contains("CreatedDate") && reader["CreatedDate"] != DBNull.Value)
                vehicleMakeAudit.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            if (columns.Contains("CreatedBy") && reader["CreatedBy"] != DBNull.Value)
                vehicleMakeAudit.CreatedBy = reader["CreatedBy"].ToString();
            if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                vehicleMakeAudit.SiteId =Convert.ToInt32(reader["SiteId"].ToString());

            return vehicleMakeAudit;
        }
        public static List<VehicleMakeAudit> GetAllVehicleMakeAudit(string dbConnection)
        {
            var vehicleMakes = new List<VehicleMakeAudit>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllVehicleMakeAudit", connection);
                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    var vehicleMake =  GetVehicleMakeAuditFromSqlReader(reader);
                    vehicleMakes.Add(vehicleMake);
                }
                connection.Close();
                reader.Close();
            }
            return vehicleMakes;
        }
        public static bool InsertVehicleMakeAudit(VehicleMakeAudit vehicleMake, string dbConnection)
        {
            if (vehicleMake != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertVehicleMakeAudit", connection);

                    var baseAuditId = BaseAudit.InsertBaseAudit(vehicleMake.BaseAudit, dbConnection);
                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;

                    cmd.Parameters.Add("@Id", SqlDbType.Int).Value = vehicleMake.ID;

                    cmd.Parameters.Add("@VEHICLEMAKEDESC", SqlDbType.NVarChar).Value = vehicleMake.VehicleMakeDesc;

                    cmd.Parameters.Add("@VEHICLEMAKEDESC_AR", SqlDbType.NVarChar).Value = vehicleMake.VehicleMakeDesc_AR;

                    cmd.Parameters.Add("@VEHICLEMAKECODE", SqlDbType.NVarChar).Value = vehicleMake.VehicleMakeCode;

                    cmd.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = vehicleMake.CreatedDate;

                    cmd.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = vehicleMake.CreatedBy;
                    cmd.Parameters.Add("@SiteId", SqlDbType.Int).Value = vehicleMake.SiteId;
                    cmd.Parameters.Add("@CustomerId", SqlDbType.Int).Value = vehicleMake.CustomerId;
                    
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }

    }
}
