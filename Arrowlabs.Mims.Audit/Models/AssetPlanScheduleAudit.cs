﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Arrowlabs.Mims.Audit.Models
{
    public class AssetPlanScheduleAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public int Id { get; set; }

        public int TaskId { get; set; }

        public int PlanId { get; set; }

        public bool isSent { get; set; }

        public DateTime? PlanDateTime { get; set; }

        public DateTime? SentDateTime { get; set; }

        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int SiteId { get; set; }
        public int CustomerId { get; set; }

        public int AssetId { get; set; }



        public static int InsertAssetPlanScheduleAudit(AssetPlanScheduleAudit taskRemarks, string dbConnection,
string auditDbConnection = "", bool isAuditEnabled = true)
        {
            int id = taskRemarks.Id;
            var baseAuditId = BaseAudit.InsertBaseAudit(taskRemarks.BaseAudit, dbConnection);
            var action = (taskRemarks.Id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;

            if (taskRemarks != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateAssetPlanScheduleAudit", connection);
                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;
                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = taskRemarks.Id;

                    cmd.Parameters.Add(new SqlParameter("@AssetId", SqlDbType.Int));
                    cmd.Parameters["@AssetId"].Value = taskRemarks.AssetId;

                    cmd.Parameters.Add(new SqlParameter("@PlanId", SqlDbType.Int));
                    cmd.Parameters["@PlanId"].Value = taskRemarks.PlanId;

                    cmd.Parameters.Add(new SqlParameter("@IsSent", SqlDbType.Bit));
                    cmd.Parameters["@IsSent"].Value = taskRemarks.isSent;

                    cmd.Parameters.Add(new SqlParameter("@PlanDateTime", SqlDbType.DateTime));
                    cmd.Parameters["@PlanDateTime"].Value = taskRemarks.PlanDateTime;

                    cmd.Parameters.Add(new SqlParameter("@SentDateTime", SqlDbType.DateTime));
                    cmd.Parameters["@SentDateTime"].Value = taskRemarks.SentDateTime;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = taskRemarks.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = taskRemarks.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = taskRemarks.CustomerId;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = taskRemarks.SiteId;

                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandText = "InsertOrUpdateAssetPlanScheduleAudit";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();


                    if (id == 0)
                        id = (int)returnParameter.Value;
 
                }
            }
            return id;
        }
    }
}
