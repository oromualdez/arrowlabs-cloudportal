﻿using Arrowlabs.Mims.Audit.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Arrowlabs.Business.Layer
{
    public class TaskCategory
    {
        public int CategoryId { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int SiteId { get; set; }
        public int CustomerId { get; set; }

        public string CustomerUName { get; set; }
        public static void DeleteTaskCategory(int id, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteTaskCategory", connection);

                cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                cmd.Parameters["@Id"].Value = id;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteTaskCategory";

                cmd.ExecuteNonQuery();
            }
        }

        private static TaskCategory GetTaskCategoryFromSqlReader(SqlDataReader reader)
        {
            var hotEventCategory = new TaskCategory();
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
            if (reader["CategoryId"] != DBNull.Value)
                hotEventCategory.CategoryId = Convert.ToInt32(reader["CategoryId"].ToString());
            if (reader["Description"] != DBNull.Value)
                hotEventCategory.Description = reader["Description"].ToString();
            if (reader["CreatedBy"] != DBNull.Value)
                hotEventCategory.CreatedBy = reader["CreatedBy"].ToString().ToLower();
            if (reader["CreatedDate"] != DBNull.Value)
                hotEventCategory.CreatedDate = Convert.ToDateTime(reader["CreatedDate"]);
            if (reader["UpdatedBy"] != DBNull.Value)
                hotEventCategory.UpdatedBy = reader["UpdatedBy"].ToString().ToLower();
            if (reader["UpdatedDate"] != DBNull.Value)
                hotEventCategory.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"]);
            if (reader["IsActive"] != DBNull.Value)
                hotEventCategory.IsActive = Convert.ToBoolean(reader["IsActive"]);

            if (columns.Contains( "SiteId") && reader["SiteId"] != DBNull.Value)
                hotEventCategory.SiteId = Convert.ToInt32(reader["SiteId"]);

            if (columns.Contains("CustomerUName") && reader["CustomerUName"] != DBNull.Value)
                hotEventCategory.CustomerUName = reader["CustomerUName"].ToString();
             
            if (columns.Contains( "CustomerId") && reader["CustomerId"] != DBNull.Value)
                hotEventCategory.CustomerId = Convert.ToInt32(reader["CustomerId"]);
            
            return hotEventCategory;
        }
        public static List<TaskCategory> GetAllTaskCategories(string dbConnection)
        {
            var hotEventCategories = new List<TaskCategory>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetAllTaskCategories";
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var hotEventCategory = GetTaskCategoryFromSqlReader(reader);
                    hotEventCategories.Add(hotEventCategory);
                }
                connection.Close();
                reader.Close();
            }
            return hotEventCategories;
        }
        public static List<TaskCategory> GetAllTaskCategoriesByCId(int id, string dbConnection)
        {
            var hotEventCategories = new List<TaskCategory>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetAllTaskCategoriesByCId";
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Id"].Value = id;
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var hotEventCategory = GetTaskCategoryFromSqlReader(reader);
                    hotEventCategories.Add(hotEventCategory);
                }
                connection.Close();
                reader.Close();
            }
            return hotEventCategories;
        }

        public static List<TaskCategory> GetAllTaskCategoriesBySiteId(int id, string dbConnection)
        {
            var hotEventCategories = new List<TaskCategory>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetAllTaskCategoriesBySiteId";
                sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.NVarChar));
                sqlCommand.Parameters["@SiteId"].Value = id;
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var hotEventCategory = GetTaskCategoryFromSqlReader(reader);
                    hotEventCategories.Add(hotEventCategory);
                }
                connection.Close();
                reader.Close();
            }
            return hotEventCategories;
        }

        public static List<TaskCategory> GetAllTaskCategoriesByLevel5(int id, string dbConnection)
        {
            var hotEventCategories = new List<TaskCategory>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetAllTaskCategoriesByLevel5";
                sqlCommand.Parameters.Add(new SqlParameter("@UserId", SqlDbType.NVarChar));
                sqlCommand.Parameters["@UserId"].Value = id;
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var hotEventCategory = GetTaskCategoryFromSqlReader(reader);
                    hotEventCategories.Add(hotEventCategory);
                }
                connection.Close();
                reader.Close();
            }
            return hotEventCategories;
        }

        public static bool InsertOrUpdateTaskCategory(TaskCategory mobileHotEventCategory,
            string dbConnection, string auditDbConnection = "", bool isAuditEnabled = true)
        {
            if (mobileHotEventCategory != null)
            {
                int id = mobileHotEventCategory.CategoryId;
                var action = (id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;

                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateTaskCategory", connection);

                    cmd.Parameters.Add("@CategoryId", SqlDbType.Int).Value = mobileHotEventCategory.CategoryId;
                    cmd.Parameters.Add("@Description", SqlDbType.NVarChar).Value = mobileHotEventCategory.Description;
                    cmd.Parameters.Add("@IsActive", SqlDbType.Bit).Value = mobileHotEventCategory.IsActive;
                    cmd.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = mobileHotEventCategory.CreatedDate;
                    cmd.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = mobileHotEventCategory.CreatedBy.ToLower();
                    cmd.Parameters.Add("@UpdatedDate", SqlDbType.DateTime).Value = mobileHotEventCategory.UpdatedDate;
                    cmd.Parameters.Add("@UpdatedBy", SqlDbType.NVarChar).Value = mobileHotEventCategory.UpdatedBy.ToLower();
                    cmd.Parameters.Add("@SiteId", SqlDbType.Int).Value = mobileHotEventCategory.SiteId;
                    cmd.Parameters.Add("@CustomerId", SqlDbType.Int).Value = mobileHotEventCategory.CustomerId;
                    var returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                    if (id == 0)
                        id = (int)returnParameter.Value;

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            mobileHotEventCategory.CategoryId = id;
                            var audit = new TaskCategoryAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, mobileHotEventCategory.UpdatedBy);
                            Reflection.CopyProperties(mobileHotEventCategory, audit);
                            TaskCategoryAudit.InsertTaskCategoryAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertOrUpdateTaskCategory", ex, dbConnection);
                    }
                }
            }
            return true;
        }

        public static TaskCategory GetTaskCategoryByName(string name, string dbConnection)
        {
            TaskCategory taskCheckListItem = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetTaskCategoryByName", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = name;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    taskCheckListItem = GetTaskCategoryFromSqlReader(reader);
                }
                connection.Close();
                reader.Close();
            }
            return taskCheckListItem;
        }

        public static TaskCategory GetTaskCategoryByNameAndCId(string name,int id ,string dbConnection)
        {
            TaskCategory taskCheckListItem = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetTaskCategoryByNameAndCId", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = name;

                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    taskCheckListItem = GetTaskCategoryFromSqlReader(reader);
                }
                connection.Close();
                reader.Close();
            }
            return taskCheckListItem;
        }

        public static TaskCategory GetTaskCategoryByNameAndCIdAndSiteId(string name, int id,int siteid ,string dbConnection)
        {
            TaskCategory taskCheckListItem = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetTaskCategoryByNameAndCIdAndSiteId", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = name;

                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;

                sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                sqlCommand.Parameters["@SiteId"].Value = siteid;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    taskCheckListItem = GetTaskCategoryFromSqlReader(reader);
                }
                connection.Close();
                reader.Close();
            }
            return taskCheckListItem;
        }
    }
}