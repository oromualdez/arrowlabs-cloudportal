﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using Arrowlabs.Mims.Audit.Models;

namespace Arrowlabs.Business.Layer
{
    public class Device
    {
        public string ClientID { get; set; }
        public string MACAddress { get; set; }
        public string Version { get; set; }
        public string Register { get; set; }
        public string PCName { get; set; }
        public string UpdatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ExpiryDate { get; set; }
        public string LastLoginDate { get; set; }
        public string Curfew { get; set; }
        public string State { get; set; }
        public string [] Devicestates { get; set; }
        public static string[] States { get; set; }
        public string GroupName { get; set; }
        public string IP { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string EngagedIn { get; set; }
       public bool Engaged {get;set;}
       public int GeofenceStatus { get; set; }
        public enum DeviceState : int
        {
            Unknown = -1,
            UnRegistered = 0,
            Enable = 1,
            Disable = 2,
            Curfew = 3
        }

        public static void DeviceStates()
        {
            States = Enum.GetNames(typeof(DeviceState));
        }
        public static Device GetClientDeviceByPcName(string pcname, string dbConnection)
        {
            Device device = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetClientDeviceByPcName", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@pcname", SqlDbType.NVarChar));
                sqlCommand.Parameters["@pcname"].Value = Encrypt.EncryptData(pcname, true,dbConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetClientDeviceByPcName";

                var reader = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                while (reader.Read())
                {
                    device = new Device();
                    device.ClientID = Decrypt.DecryptData(reader["ClientID"].ToString(), true, dbConnection);

                    device.MACAddress = Decrypt.DecryptData(reader["MACAddress"].ToString(), true, dbConnection);

                    device.Version = Decrypt.DecryptData(reader["Version"].ToString(), true, dbConnection);
                    device.Register = Decrypt.DecryptData(reader["Register"].ToString(), true, dbConnection);
                    device.PCName = Decrypt.DecryptData(reader["PCName"].ToString(), true, dbConnection);
                    device.CreatedDate = Decrypt.DecryptData(reader["CreatedDate"].ToString(), true, dbConnection);
                    device.ExpiryDate = Decrypt.DecryptData(reader["ExpiryDate"].ToString(), true, dbConnection);
                    device.LastLoginDate = Decrypt.DecryptData(reader["LastLoginDate"].ToString(), true, dbConnection);
                    device.Curfew = Decrypt.DecryptData(reader["Curfew"].ToString(), true, dbConnection);
                    if (reader["Latitude"] != DBNull.Value)
                        device.Latitude = Convert.ToDouble(reader["Latitude"].ToString());
                    if (reader["Longitude"] != DBNull.Value)
                        device.Longitude = Convert.ToDouble(reader["Longitude"].ToString());

                    if (reader["EngagedIn"] != DBNull.Value)
                        device.EngagedIn = reader["EngagedIn"].ToString();
                    if (reader["Engaged"] != DBNull.Value)
                        device.Engaged = Convert.ToBoolean(reader["Engaged"].ToString());

                    if (columns.Contains( "GeofenceStatus") && reader["GeofenceStatus"] != DBNull.Value)
                    {
                        device.GeofenceStatus = Convert.ToInt32(reader["GeofenceStatus"]);
                    }

                }
                connection.Close();
                reader.Close();
            }
            return device;
        }
        public static Device GetClientDeviceByMacAddress(string macAddress,string dbConnection)
        {
            Device device = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetClientDeviceByMacAddress", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@macAddress", SqlDbType.NVarChar));
                sqlCommand.Parameters["@macAddress"].Value = Encrypt.EncryptData(macAddress, true, dbConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetClientDeviceByMacAddress";

                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    device = new Device();
                    device.ClientID = Decrypt.DecryptData(reader["ClientID"].ToString(), true, dbConnection);

                    device.MACAddress = Decrypt.DecryptData(reader["MACAddress"].ToString(), true, dbConnection);

                    device.Version = Decrypt.DecryptData(reader["Version"].ToString(), true, dbConnection);
                    device.Register = Decrypt.DecryptData(reader["Register"].ToString(), true, dbConnection);
                    device.PCName = Decrypt.DecryptData(reader["PCName"].ToString(), true, dbConnection);
                    device.CreatedDate = Decrypt.DecryptData(reader["CreatedDate"].ToString(), true, dbConnection);
                    device.ExpiryDate = Decrypt.DecryptData(reader["ExpiryDate"].ToString(), true, dbConnection);
                    device.LastLoginDate = Decrypt.DecryptData(reader["LastLoginDate"].ToString(), true, dbConnection);
                    device.Curfew = Decrypt.DecryptData(reader["Curfew"].ToString(), true, dbConnection);
                    if (reader["Latitude"] != DBNull.Value)
                        device.Latitude = Convert.ToDouble(reader["Latitude"].ToString());
                    if (reader["Longitude"] != DBNull.Value)
                        device.Longitude = Convert.ToDouble(reader["Longitude"].ToString());

                    if (reader["EngagedIn"] != DBNull.Value)
                        device.EngagedIn = reader["EngagedIn"].ToString();
                    if (reader["Engaged"] != DBNull.Value)
                        device.Engaged = Convert.ToBoolean(reader["Engaged"].ToString());

                }
                connection.Close();
                reader.Close();
            }
            return device;
        }

        public static List<Device> GetClientDeviceByClientID(string clientID, string dbConnection)
        {
            var deviceList = new List<Device>();
            DeviceStates();
            Device device = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetClientDeviceByClientID", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@ClientID", SqlDbType.NVarChar));
                sqlCommand.Parameters["@ClientID"].Value = Encrypt.EncryptData(clientID, true, dbConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetClientDeviceByClientID";

                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    device = new Device();
                    device.ClientID = Decrypt.DecryptData(reader["ClientID"].ToString(), true, dbConnection);
                    device.MACAddress = Decrypt.DecryptData(reader["MACAddress"].ToString(), true, dbConnection);
                    device.Version = Decrypt.DecryptData(reader["Version"].ToString(), true, dbConnection);
                    device.Register = Decrypt.DecryptData(Decrypt.DecryptData(reader["Register"].ToString(), true, dbConnection), true, dbConnection);//Decrypt.DecryptData(reader["Register"].ToString(), true);

                    if (device.Register == ((int)DeviceState.Disable).ToString())
                        device.State = DeviceState.Disable.ToString();
                    else if (device.Register == ((int)DeviceState.Enable).ToString())
                        device.State = DeviceState.Enable.ToString();
                    else if (device.Register == ((int)DeviceState.UnRegistered).ToString())
                        device.State = DeviceState.UnRegistered.ToString();
                    else if (device.Register == ((int)DeviceState.Curfew).ToString())
                        device.State = DeviceState.Curfew.ToString();
                    else
                        device.State = DeviceState.Unknown.ToString();

                    device.Devicestates = States;

                    device.PCName = Decrypt.DecryptData(reader["PCName"].ToString(), true, dbConnection);
                    device.CreatedDate = Decrypt.DecryptData(reader["CreatedDate"].ToString(), true, dbConnection);
                    device.ExpiryDate = Decrypt.DecryptData(reader["ExpiryDate"].ToString(), true, dbConnection);
                    device.LastLoginDate = Decrypt.DecryptData(reader["LastLoginDate"].ToString(), true, dbConnection);
                    device.Curfew = Decrypt.DecryptData(reader["Curfew"].ToString(), true, dbConnection);
                    device.IP = reader["IP"].ToString();

                    if (reader["Latitude"] != DBNull.Value)
                        device.Latitude = Convert.ToDouble(reader["Latitude"].ToString());
                    if (reader["Longitude"] != DBNull.Value)
                        device.Longitude = Convert.ToDouble(reader["Longitude"].ToString());

                    if (reader["EngagedIn"] != DBNull.Value)
                        device.EngagedIn = reader["EngagedIn"].ToString();
                    if (reader["Engaged"] != DBNull.Value)
                        device.Engaged = Convert.ToBoolean(reader["Engaged"].ToString());

                    deviceList.Add(device);
                }
                connection.Close();
                reader.Close();
            }
            return deviceList;
        }

        public static List<Device> GetAllDevice( string dbConnection)
        {
            var deviceList = new List<Device>();
            DeviceStates();
            Device device = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllClientDevices", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllClientDevices";

                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    device = new Device();
                    device.ClientID = Decrypt.DecryptData(reader["ClientID"].ToString(), true, dbConnection);
                    device.MACAddress = Decrypt.DecryptData(reader["MACAddress"].ToString(), true, dbConnection);
                    device.Version = Decrypt.DecryptData(reader["Version"].ToString(), true, dbConnection);
                    device.Register = Decrypt.DecryptData(reader["Register"].ToString(), true, dbConnection);

                    if (device.Register == ((int)DeviceState.Disable).ToString())
                        device.State = DeviceState.Disable.ToString();
                    else if (device.Register == ((int)DeviceState.Enable).ToString())
                        device.State = DeviceState.Enable.ToString();
                    else if (device.Register == ((int)DeviceState.UnRegistered).ToString())
                        device.State = DeviceState.UnRegistered.ToString();
                    else if (device.Register == ((int)DeviceState.Curfew).ToString())
                        device.State = DeviceState.Curfew.ToString();
                    else 
                        device.State = DeviceState.Unknown.ToString();

                    device.Devicestates = States;

                    device.PCName = Decrypt.DecryptData(reader["PCName"].ToString(), true, dbConnection);
                    device.CreatedDate = Decrypt.DecryptData(reader["CreatedDate"].ToString(), true, dbConnection);
                    device.ExpiryDate = Decrypt.DecryptData(reader["ExpiryDate"].ToString(), true, dbConnection);
                    device.LastLoginDate = Decrypt.DecryptData(reader["LastLoginDate"].ToString(), true, dbConnection);
                    device.Curfew = Decrypt.DecryptData(reader["Curfew"].ToString(), true, dbConnection);
                    if (reader["Latitude"] != DBNull.Value)
                        device.Latitude = Convert.ToDouble(reader["Latitude"].ToString());
                    if (reader["Longitude"] != DBNull.Value)
                    device.Longitude = Convert.ToDouble(reader["Longitude"].ToString());

                    if (reader["EngagedIn"] != DBNull.Value)
                        device.EngagedIn = reader["EngagedIn"].ToString();
                    if (reader["Engaged"] != DBNull.Value)
                        device.Engaged = Convert.ToBoolean(reader["Engaged"].ToString());

                    deviceList.Add(device);
                }
                connection.Close();
                reader.Close();
            }
            return deviceList;
        }

        public static void InsertClientDevice(Device dev,string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("InsertOrUpdateClientDevice", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@ClientID", SqlDbType.NVarChar));
                sqlCommand.Parameters["@ClientID"].Value = Encrypt.EncryptData(dev.ClientID, true, dbConnection);

                sqlCommand.Parameters.Add(new SqlParameter("@MACAddress", SqlDbType.NVarChar));
                sqlCommand.Parameters["@MACAddress"].Value = Encrypt.EncryptData(dev.MACAddress, true, dbConnection);

                sqlCommand.Parameters.Add(new SqlParameter("@Version", SqlDbType.NVarChar));
                sqlCommand.Parameters["@version"].Value = Encrypt.EncryptData(dev.Version, true, dbConnection);
              

                sqlCommand.Parameters.Add(new SqlParameter("@Register", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Register"].Value = Encrypt.EncryptData(dev.Register, true, dbConnection);
                

                sqlCommand.Parameters.Add(new SqlParameter("@PCName", SqlDbType.NVarChar));
                sqlCommand.Parameters["@PCName"].Value = Encrypt.EncryptData(dev.PCName, true, dbConnection);

                sqlCommand.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.NVarChar));
                sqlCommand.Parameters["@CreatedDate"].Value = Encrypt.EncryptData(dev.CreatedDate, true, dbConnection);

                sqlCommand.Parameters.Add(new SqlParameter("@ExpiryDate", SqlDbType.NVarChar));
                sqlCommand.Parameters["@ExpiryDate"].Value = Encrypt.EncryptData(dev.ExpiryDate, true, dbConnection);

                sqlCommand.Parameters.Add(new SqlParameter("@LastLoginDate", SqlDbType.NVarChar));
                sqlCommand.Parameters["@LastLoginDate"].Value = Encrypt.EncryptData(dev.LastLoginDate, true, dbConnection);

                sqlCommand.Parameters.Add(new SqlParameter("@Curfew", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Curfew"].Value = Encrypt.EncryptData(dev.Curfew, true, dbConnection);

                sqlCommand.Parameters.Add(new SqlParameter("@ip", SqlDbType.NVarChar));
                sqlCommand.Parameters["@ip"].Value = dev.IP;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlCommand.CommandText = "InsertOrUpdateClientDevice";

                sqlCommand.ExecuteNonQuery();

                connection.Close();
            }
        }

        public static void InsertClientDevice(Device dev, string dbConnection,
      string auditDbConnection = "", bool isAuditEnabled = true)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("InsertOrUpdateClientDevice", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@ClientID", SqlDbType.NVarChar));
                sqlCommand.Parameters["@ClientID"].Value = Encrypt.EncryptData(dev.ClientID, true, dbConnection);

                sqlCommand.Parameters.Add(new SqlParameter("@MACAddress", SqlDbType.NVarChar));
                sqlCommand.Parameters["@MACAddress"].Value = Encrypt.EncryptData(dev.MACAddress, true, dbConnection);

                sqlCommand.Parameters.Add(new SqlParameter("@Version", SqlDbType.NVarChar));
                sqlCommand.Parameters["@version"].Value = Encrypt.EncryptData(dev.Version, true, dbConnection);


                sqlCommand.Parameters.Add(new SqlParameter("@Register", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Register"].Value = Encrypt.EncryptData(dev.Register, true, dbConnection);


                sqlCommand.Parameters.Add(new SqlParameter("@PCName", SqlDbType.NVarChar));
                sqlCommand.Parameters["@PCName"].Value = Encrypt.EncryptData(dev.PCName, true, dbConnection);

                sqlCommand.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.NVarChar));
                sqlCommand.Parameters["@CreatedDate"].Value = Encrypt.EncryptData(dev.CreatedDate, true, dbConnection);

                sqlCommand.Parameters.Add(new SqlParameter("@ExpiryDate", SqlDbType.NVarChar));
                sqlCommand.Parameters["@ExpiryDate"].Value = Encrypt.EncryptData(dev.ExpiryDate, true, dbConnection);

                sqlCommand.Parameters.Add(new SqlParameter("@LastLoginDate", SqlDbType.NVarChar));
                sqlCommand.Parameters["@LastLoginDate"].Value = Encrypt.EncryptData(dev.LastLoginDate, true, dbConnection);

                sqlCommand.Parameters.Add(new SqlParameter("@Curfew", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Curfew"].Value = Encrypt.EncryptData(dev.Curfew, true, dbConnection);

                sqlCommand.Parameters.Add(new SqlParameter("@ip", SqlDbType.NVarChar));
                sqlCommand.Parameters["@ip"].Value = dev.IP;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlCommand.CommandText = "InsertOrUpdateClientDevice";

                sqlCommand.ExecuteNonQuery();

                try
                {
                    if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                    {
                        var audit = new Arrowlabs.Mims.Audit.Models.DeviceAudit();
                        audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, Arrowlabs.Mims.Audit.Enums.Action.Create, dev.UpdatedBy);
                        Reflection.CopyProperties(dev, audit);
                        Arrowlabs.Mims.Audit.Models.DeviceAudit.InsertClientDevice(audit, auditDbConnection);
                    }
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("Business Layer", "InsertorUpdateChatUser", ex, dbConnection);
                }

                connection.Close();
            }
        }

        public static Device GetServerDeviceByMacAddress(string macAddress, string dbConnection)
        {
            Device device = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetServerDeviceByMacAddress", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@macAddress", SqlDbType.NVarChar));
                sqlCommand.Parameters["@macAddress"].Value = Encrypt.EncryptData(macAddress, true, dbConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetServerDeviceByMacAddress";

                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    device = new Device();
                    device.ClientID = Decrypt.DecryptData(reader["ClientID"].ToString(), true, dbConnection);

                    device.MACAddress = Decrypt.DecryptData(reader["MACAddress"].ToString(), true, dbConnection);

                    device.Version = Decrypt.DecryptData(reader["Version"].ToString(), true, dbConnection);
                    device.Register = Decrypt.DecryptData(reader["Register"].ToString(), true, dbConnection);
                    device.PCName = Decrypt.DecryptData(reader["PCName"].ToString(), true, dbConnection);
                    device.CreatedDate = Decrypt.DecryptData(reader["CreatedDate"].ToString(), true, dbConnection);
                    device.ExpiryDate = Decrypt.DecryptData(reader["ExpiryDate"].ToString(), true, dbConnection);
                    device.LastLoginDate = Decrypt.DecryptData(reader["LastLoginDate"].ToString(), true, dbConnection);
                    device.Curfew = Decrypt.DecryptData(reader["Curfew"].ToString(), true, dbConnection);
                }
                connection.Close();
                reader.Close();
            }
            return device;
        }

        public static void InsertServerDevice(Device dev, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("InsertOrUpdateServerDevice", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@ClientID", SqlDbType.NVarChar));
                sqlCommand.Parameters["@ClientID"].Value = Encrypt.EncryptData(dev.ClientID, true, dbConnection);

                sqlCommand.Parameters.Add(new SqlParameter("@MACAddress", SqlDbType.NVarChar));
                sqlCommand.Parameters["@MACAddress"].Value = Encrypt.EncryptData(dev.MACAddress, true, dbConnection);

                sqlCommand.Parameters.Add(new SqlParameter("@Version", SqlDbType.NVarChar));
                sqlCommand.Parameters["@version"].Value = Encrypt.EncryptData(dev.Version, true, dbConnection);


                sqlCommand.Parameters.Add(new SqlParameter("@Register", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Register"].Value = Encrypt.EncryptData(dev.Register, true, dbConnection);


                sqlCommand.Parameters.Add(new SqlParameter("@PCName", SqlDbType.NVarChar));
                sqlCommand.Parameters["@PCName"].Value = Encrypt.EncryptData(dev.PCName, true, dbConnection);

                sqlCommand.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.NVarChar));
                sqlCommand.Parameters["@CreatedDate"].Value = Encrypt.EncryptData(dev.CreatedDate, true, dbConnection);

                sqlCommand.Parameters.Add(new SqlParameter("@ExpiryDate", SqlDbType.NVarChar));
                sqlCommand.Parameters["@ExpiryDate"].Value = Encrypt.EncryptData(dev.ExpiryDate, true, dbConnection);

                sqlCommand.Parameters.Add(new SqlParameter("@LastLoginDate", SqlDbType.NVarChar));
                sqlCommand.Parameters["@LastLoginDate"].Value = Encrypt.EncryptData(dev.LastLoginDate, true, dbConnection);

                sqlCommand.Parameters.Add(new SqlParameter("@Curfew", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Curfew"].Value = Encrypt.EncryptData(dev.Curfew, true, dbConnection);

                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlCommand.CommandText = "InsertOrUpdateServerDevice";

                sqlCommand.ExecuteNonQuery();



                connection.Close();
            }
        }

        public static bool DeleteDeviceByMacAddress(string macAddress, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteDeviceByMacAddress", connection);

                cmd.Parameters.Add(new SqlParameter("@macAddress", SqlDbType.NVarChar));
                cmd.Parameters["@macAddress"].Value = macAddress;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteDeviceByMacAddress";

                cmd.ExecuteNonQuery();
            }
            return true;
        }


        public static List<Device> GetAllDevicesByGroupId(int GroupId, string dbConnection)
        {
            var deviceList = new List<Device>();
            DeviceStates();
            Device device = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllDevicesByGroupId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = GroupId;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllDevicesByGroupId";

                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    device = new Device();
                    device.ClientID = Decrypt.DecryptData(reader["ClientID"].ToString(), true, dbConnection);
                    device.MACAddress = Decrypt.DecryptData(reader["MACAddress"].ToString(), true, dbConnection);
                    device.Version = Decrypt.DecryptData(reader["Version"].ToString(), true, dbConnection);
                    device.Register = Decrypt.DecryptData(reader["Register"].ToString(), true, dbConnection);

                    if (device.Register == ((int)DeviceState.Disable).ToString())
                        device.State = DeviceState.Disable.ToString();
                    else if (device.Register == ((int)DeviceState.Enable).ToString())
                        device.State = DeviceState.Enable.ToString();
                    else if (device.Register == ((int)DeviceState.UnRegistered).ToString())
                        device.State = DeviceState.UnRegistered.ToString();
                    else if (device.Register == ((int)DeviceState.Curfew).ToString())
                        device.State = DeviceState.Curfew.ToString();
                    else
                        device.State = DeviceState.Unknown.ToString();

                    device.Devicestates = States;
                    device.PCName = reader["Name"].ToString();
                    device.PCName = Decrypt.DecryptData(reader["PCName"].ToString(), true, dbConnection);
                    device.CreatedDate = Decrypt.DecryptData(reader["CreatedDate"].ToString(), true, dbConnection);
                    device.ExpiryDate = Decrypt.DecryptData(reader["ExpiryDate"].ToString(), true, dbConnection);
                    device.LastLoginDate = Decrypt.DecryptData(reader["LastLoginDate"].ToString(), true, dbConnection);
                    device.Curfew = Decrypt.DecryptData(reader["Curfew"].ToString(), true, dbConnection);
                    deviceList.Add(device);
                }
                connection.Close();
                reader.Close();
            }
            return deviceList;
        }

        public static IList<TamplateClientDevice> GetAllClientDevices(string dbConnection)
        {
            var deviceList = new List<TamplateClientDevice>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllClientDevices", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllClientDevices";

                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    var device = new TamplateClientDevice();
                    device.MacAddress = Decrypt.DecryptData(reader["MACAddress"].ToString(), true, dbConnection);
                    device.PcName = Decrypt.DecryptData(reader["PCName"].ToString(), true, dbConnection);
                    device.RtspURL = Decrypt.DecryptData(reader["PCName"].ToString(), true, dbConnection);
                    deviceList.Add(device);
                }
                connection.Close();
                reader.Close();
            }
            return deviceList;
        }

        public static IList<DeviceCoordinate> GetAllDevicesCoordinate(string dbConnection)
        {
            var deviceList = new List<DeviceCoordinate>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllDevicesCoordinate", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllDevicesCoordinate";

                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    var device = new DeviceCoordinate();
                    device.MacAddress = Decrypt.DecryptData(reader["MACAddress"].ToString(), true, dbConnection);
                    device.PcName = Decrypt.DecryptData(reader["PCName"].ToString(), true, dbConnection);
                    if (reader["Latitude"] != DBNull.Value)
                        device.Latitude = (float)Convert.ToDouble(reader["Latitude"].ToString());
                    if (reader["Longitude"] != DBNull.Value)
                        device.Longitude = (float)Convert.ToDouble(reader["Longitude"].ToString());
                    deviceList.Add(device);
                }
                connection.Close();
                reader.Close();
            }
            return deviceList;
        }

        public static DeviceCoordinate GetDeviceCoordinateByPcName(string pcName, string dbConnection)
        {
            DeviceCoordinate device = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetDeviceCoordinateByPcName", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@pcName", SqlDbType.NVarChar));
                sqlCommand.Parameters["@pcName"].Value = Encrypt.EncryptData(pcName, true, dbConnection); ;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetDeviceCoordinateByPcName";

                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    device = new DeviceCoordinate();
                    device.MacAddress = Decrypt.DecryptData(reader["MACAddress"].ToString(), true, dbConnection);
                    device.PcName = Decrypt.DecryptData(reader["PCName"].ToString(), true, dbConnection);
                    if (reader["Latitude"] != DBNull.Value)
                        device.Latitude = (float)Convert.ToDouble(reader["Latitude"].ToString());
                    if (reader["Longitude"] != DBNull.Value)
                        device.Longitude = (float)Convert.ToDouble(reader["Longitude"].ToString());
                }
                connection.Close();
                reader.Close();
            }
            return device;
        }

        public static IList<Device> GetAllFullClientDevices(string dbConnection)
        {
            var deviceList = new List<Device>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllFullClientDevices", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllFullClientDevices";

                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    var device = new Device();
                    device.MACAddress = Decrypt.DecryptData(reader["MACAddress"].ToString(), true, dbConnection);
                    device.PCName = Decrypt.DecryptData(reader["PCName"].ToString(), true, dbConnection);
                  //  device.Register = Decrypt.DecryptData(reader["Register"].ToString(), true);
                    device.Register = reader["Register"].ToString();
                    if (Decrypt.DecryptData(device.Register, true, dbConnection) == ((int)DeviceState.Disable).ToString())
                        device.State = DeviceState.Disable.ToString();
                    else if (Decrypt.DecryptData(device.Register, true, dbConnection) == ((int)DeviceState.Enable).ToString())
                        device.State = DeviceState.Enable.ToString();
                    else if (Decrypt.DecryptData(device.Register, true, dbConnection) == ((int)DeviceState.UnRegistered).ToString())
                        device.State = DeviceState.UnRegistered.ToString();
                    else if (Decrypt.DecryptData(device.Register, true, dbConnection) == ((int)DeviceState.Curfew).ToString())
                        device.State = DeviceState.Curfew.ToString();
                    else
                        device.State = DeviceState.Unknown.ToString();

                    device.LastLoginDate = Decrypt.DecryptData(reader["LastLoginDate"].ToString(), true, dbConnection);
                    device.ExpiryDate = Decrypt.DecryptData(reader["ExpiryDate"].ToString(), true, dbConnection);
                    device.ClientID = Decrypt.DecryptData(reader["ClientID"].ToString(), true, dbConnection);

                    device.CreatedDate = Decrypt.DecryptData(reader["CreatedDate"].ToString(), true, dbConnection);
                    device.Curfew = Decrypt.DecryptData(reader["Curfew"].ToString(), true, dbConnection);
                    device.Version = Decrypt.DecryptData(reader["Version"].ToString(), true, dbConnection);

                    deviceList.Add(device);
                }
                connection.Close();
                reader.Close();
            }
            return deviceList;
        }

        public static bool UpdateClientDeviceLastLogin(string MacAddress, DateTime LastLoginDate, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("UpdateClientDeviceLastLogin", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@MacAddress", SqlDbType.NVarChar));
                sqlCommand.Parameters["@MacAddress"].Value = Encrypt.EncryptData(MacAddress, true, dbConnection);

                sqlCommand.Parameters.Add(new SqlParameter("@LastLoginDate", SqlDbType.NVarChar));
                sqlCommand.Parameters["@LastLoginDate"].Value = Encrypt.EncryptData(LastLoginDate.ToString(), true, dbConnection);

                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlCommand.CommandText = "UpdateClientDeviceLastLogin";

                sqlCommand.ExecuteNonQuery();

                connection.Close();
            }
            return true;
        }

        public static bool UpdateClientDeviceCoordinate(string MacAddress, float Latitude, float Longitude, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("UpdateClientDeviceCoordinate", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@MacAddress", SqlDbType.NVarChar));
                sqlCommand.Parameters["@MacAddress"].Value = Encrypt.EncryptData(MacAddress, true, dbConnection);

                sqlCommand.Parameters.Add(new SqlParameter("@LastLoginDate", SqlDbType.NVarChar));
                sqlCommand.Parameters["@LastLoginDate"].Value = Encrypt.EncryptData(DateTime.Now.ToString(), true, dbConnection);

                sqlCommand.Parameters.Add(new SqlParameter("@Latitude", SqlDbType.Float));
                sqlCommand.Parameters["@Latitude"].Value = Latitude;

                sqlCommand.Parameters.Add(new SqlParameter("@Longitude", SqlDbType.Float));
                sqlCommand.Parameters["@Longitude"].Value = Longitude;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlCommand.CommandText = "UpdateClientDeviceCoordinate";

                sqlCommand.ExecuteNonQuery();

                connection.Close();
            }
            return true;
        }
        public static bool UpdateClientDeviceCoordinatebyCamera(string MacAddress, float Latitude, float Longitude, string cameraip, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("UpdateClientDeviceCoordinatebyCamera", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@MacAddress", SqlDbType.NVarChar));
                sqlCommand.Parameters["@MacAddress"].Value = Encrypt.EncryptData(MacAddress, true, dbConnection);

                sqlCommand.Parameters.Add(new SqlParameter("@LastLoginDate", SqlDbType.NVarChar));
                sqlCommand.Parameters["@LastLoginDate"].Value = Encrypt.EncryptData(DateTime.Now.ToString(), true, dbConnection);

                sqlCommand.Parameters.Add(new SqlParameter("@Latitude", SqlDbType.Float));
                sqlCommand.Parameters["@Latitude"].Value = Latitude;

                sqlCommand.Parameters.Add(new SqlParameter("@Longitude", SqlDbType.Float));
                sqlCommand.Parameters["@Longitude"].Value = Longitude;

                sqlCommand.Parameters.Add(new SqlParameter("@CameraIP", SqlDbType.NVarChar));
                sqlCommand.Parameters["@CameraIP"].Value = cameraip;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlCommand.CommandText = "UpdateClientDeviceCoordinatebyCamera";

                sqlCommand.ExecuteNonQuery();

                connection.Close();
            }
            return true;
        }
        public static bool UpdateClientCoordinate(string MacAddress, float Latitude, float Longitude, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("UpdateDeviceCoordinate", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@MacAddress", SqlDbType.NVarChar));
                sqlCommand.Parameters["@MacAddress"].Value = MacAddress;               

                sqlCommand.Parameters.Add(new SqlParameter("@Latitude", SqlDbType.Float));
                sqlCommand.Parameters["@Latitude"].Value = Latitude;

                sqlCommand.Parameters.Add(new SqlParameter("@Longitude", SqlDbType.Float));
                sqlCommand.Parameters["@Longitude"].Value = Longitude;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlCommand.CommandText = "UpdateDeviceCoordinate";

                sqlCommand.ExecuteNonQuery();

                connection.Close();
            }
            return true;
        }

        public static bool UpdateDeviceEngagedStatus(string MacAddress, bool engage,string description, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("UpdateDeviceEngagedStatus", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@MacAddress", SqlDbType.NVarChar));
                sqlCommand.Parameters["@MacAddress"].Value = Encrypt.EncryptData(MacAddress, true, dbConnection);

                sqlCommand.Parameters.Add(new SqlParameter("@Engaged", SqlDbType.Bit));
                sqlCommand.Parameters["@Engaged"].Value = engage;

                sqlCommand.Parameters.Add(new SqlParameter("@EngagedIn", SqlDbType.NVarChar));
                sqlCommand.Parameters["@EngagedIn"].Value = description;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlCommand.CommandText = "UpdateDeviceEngagedStatus";

                sqlCommand.ExecuteNonQuery();

                connection.Close();
            }
            return true;
        }

        public static bool UpdateDeviceGeofenceStatus(string MacAddress, int geofenceStatus, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("UpdateDeviceGeofenceStatus", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@MacAddress", SqlDbType.NVarChar));
                sqlCommand.Parameters["@MacAddress"].Value = Encrypt.EncryptData(MacAddress, true, dbConnection);

                sqlCommand.Parameters.Add(new SqlParameter("@GeofenceStatus", SqlDbType.Int));
                sqlCommand.Parameters["@GeofenceStatus"].Value = geofenceStatus;
               
                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlCommand.CommandText = "UpdateDeviceGeofenceStatus";

                sqlCommand.ExecuteNonQuery();

                connection.Close();
            }
            return true;
        }

        public static bool UpdateClientDeviceEngageIn(string engagein, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("UpdateDeviceEngageIn", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@EngagedIn", SqlDbType.NVarChar));
                sqlCommand.Parameters["@EngagedIn"].Value = engagein;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlCommand.CommandText = "UpdateDeviceEngageIn";

                sqlCommand.ExecuteNonQuery();

                connection.Close();
            }
            return true;
        }

        public static List<DeviceInfo> GetClientDeviceForCNL(string dbConnection)
        {
            var deviceList = new List<DeviceInfo>();

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetClientDeviceForCNL", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetClientDeviceForCNL";

                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    var device = new DeviceInfo();
                    device.DeviceName = Decrypt.DecryptData(reader["PCName"].ToString(), true, dbConnection);
                    if (reader["Latitude"] != DBNull.Value)
                        device.Latitude = (float)Convert.ToDouble(reader["Latitude"].ToString());
                    if (reader["Longitude"] != DBNull.Value)
                        device.Longitude = (float)Convert.ToDouble(reader["Longitude"].ToString());
                    if (reader["CameraIP"] != DBNull.Value)
                        device.CameraIP = reader["CameraIP"].ToString();
                    deviceList.Add(device);
                }
                connection.Close();
                reader.Close();
            }
            return deviceList;
        }
    }
}
