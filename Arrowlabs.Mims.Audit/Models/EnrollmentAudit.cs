﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class  EnrollmentAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public int CaseId { get; set; }       
        public string PID { get; set; }       
        public string YearOfBirth { get; set; }       
        public string Name { get; set; }       
        public string Gender { get; set; }       
        public string Ethnicity { get; set; }       
        public string Reason { get; set; }       
        public string ConfirmReason { get; set; }       
        public int ListCategory { get; set; }       
        public string CreatedBy { get; set; }        
        public DateTime? CreatedDate { get; set; }           
        public int Type { get; set; }       
        public string TransactionID { get; set; }       
        public byte[] EnrollImage { get; set; }
        public string TypeName { get; set; }
        public string ListName { get; set; }
        public int SiteId { get; set; }
        public enum EnrollmentTypes
        {
            FRS = 0,
            ANPR = 1
        }
        public enum GenderTypes
        {
            Female = 0,
            Male = 1
        }
        public enum EthnicityTypes
        {
            Asian = 0,
            Black = 1,
            Caucasian = 2
        }
        public enum ListTypes
        {
            BlackList = 0,
            WhiteList = 1
        }
        public static List<string> GetListTypes()
        {
            return Enum.GetNames(typeof(ListTypes)).ToList();
        }
        public static List<string> GetEnrollmentTypes()
        {
            return Enum.GetNames(typeof(EnrollmentTypes)).ToList();
        }
        public static List<string> GetGenderTypes()
        {
            return Enum.GetNames(typeof(GenderTypes)).ToList();
        }
        public static List<string> GetEthnicityTypes()
        {
            return Enum.GetNames(typeof(EthnicityTypes)).ToList();
        }
        public static void InsertEnrollmentAudit(EnrollmentAudit enrollment, string dbConnection)
        {
            var baseAuditId = BaseAudit.InsertBaseAudit(enrollment.BaseAudit, dbConnection);

            if (enrollment != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertEnrollmentAudit", connection);

                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;
                    cmd.Parameters.Add("@CaseId", SqlDbType.Int).Value = enrollment.CaseId;
                    cmd.Parameters.Add("@PID", SqlDbType.NVarChar).Value = enrollment.PID;
                    cmd.Parameters.Add("@YearOfBirth", SqlDbType.NVarChar).Value = enrollment.YearOfBirth;
                    cmd.Parameters.Add("@Name", SqlDbType.NVarChar).Value = enrollment.Name;
                    cmd.Parameters.Add("@Gender", SqlDbType.NVarChar).Value = enrollment.Gender;
                    cmd.Parameters.Add("@Ethnicity", SqlDbType.NVarChar).Value = enrollment.Ethnicity;
                    cmd.Parameters.Add("@Reason", SqlDbType.NVarChar).Value = enrollment.Reason;
                    cmd.Parameters.Add("@ListCategory", SqlDbType.Int).Value = enrollment.ListCategory;
                    cmd.Parameters.Add("@ConfirmReason", SqlDbType.NVarChar).Value = enrollment.ConfirmReason;
                    cmd.Parameters.Add("@TransactionID", SqlDbType.NVarChar).Value = enrollment.TransactionID;
                    cmd.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = enrollment.CreatedBy;                  
                    cmd.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = enrollment.CreatedDate;
                    cmd.Parameters.Add("@Type", SqlDbType.Int).Value = enrollment.Type;

                    cmd.Parameters.Add("@SiteId", SqlDbType.Int).Value = enrollment.SiteId;
                    
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                }
            }
        }
        private static EnrollmentAudit GetEnrollmentAuditFromSqlReader(SqlDataReader reader)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

            var enrollment = new EnrollmentAudit();
            enrollment.BaseAudit = BaseAudit.GetBaseAuditFromSqlReader(reader);

            if (columns.Contains("CaseId") && reader["CaseId"] != DBNull.Value)
                enrollment.CaseId = Convert.ToInt16(reader["CaseId"].ToString());
            if (columns.Contains("PID") && reader["PID"] != DBNull.Value)
                enrollment.PID = reader["PID"].ToString();
            if (columns.Contains("Type") && reader["Type"] != DBNull.Value)
                enrollment.Type = Convert.ToInt16(reader["Type"].ToString());

            if (enrollment.Type == (int)EnrollmentTypes.ANPR)
                enrollment.TypeName = "ANPR";
            else
                enrollment.TypeName = "FRS";

            if ( enrollment.ListCategory == (int)ListTypes.BlackList)
                enrollment.ListName = "BlackList";
            else
                enrollment.ListName = "WhiteList";

            if (columns.Contains("YearOfBirth") && reader["YearOfBirth"] != DBNull.Value)
                enrollment.YearOfBirth = reader["YearOfBirth"].ToString();

            if (columns.Contains("Name") && reader["Name"] != DBNull.Value)
                enrollment.Name = reader["Name"].ToString();
            if (columns.Contains("Gender") && reader["Gender"] != DBNull.Value)
                enrollment.Gender = reader["Gender"].ToString();
            if (columns.Contains("Reason") && reader["Reason"] != DBNull.Value)
                enrollment.Reason = reader["Reason"].ToString();

            if (columns.Contains("Ethnicity") && reader["Ethnicity"] != DBNull.Value)
                enrollment.Ethnicity = reader["Ethnicity"].ToString();
            if (columns.Contains("ConfirmReason") && reader["ConfirmReason"] != DBNull.Value)
                enrollment.ConfirmReason = reader["ConfirmReason"].ToString();
            if (columns.Contains("ListCategory") && reader["ListCategory"] != DBNull.Value)
                enrollment.ListCategory = Convert.ToInt16(reader["ListCategory"].ToString());
            if (columns.Contains("ConfirmReason") && reader["ConfirmReason"] != DBNull.Value)
                enrollment.ConfirmReason = reader["ConfirmReason"].ToString();
            if (columns.Contains("CreatedBy") && reader["CreatedBy"] != DBNull.Value)
                enrollment.CreatedBy = reader["CreatedBy"].ToString();
            if (columns.Contains("CreatedDate") && reader["CreatedDate"] != DBNull.Value)
                enrollment.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            if (columns.Contains("TransactionID") && reader["TransactionID"] != DBNull.Value)
                enrollment.TransactionID = reader["TransactionID"].ToString();
            
            if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                enrollment.SiteId =Convert.ToInt32(reader["SiteId"].ToString());


            return enrollment;
        }
        public static List<EnrollmentAudit> GetAllEnrollmentAudit(string dbConnection)
        {
            var enrollments = new List<EnrollmentAudit>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = connection;
                sqlCommand.CommandText = "GetAllEnrollmentAudit";
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var enrollment = GetEnrollmentAuditFromSqlReader(reader);
                    enrollments.Add(enrollment);
                }
                connection.Close();
                reader.Close();
            }
            return enrollments;
        }

    }
}
