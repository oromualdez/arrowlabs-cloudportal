﻿using Arrowlabs.Mims.Audit.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Business.Layer
{
    public class ItemLost
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Comments { get; set; }
        public int AssetCatId { get; set; }
        public int MainAssetCatId { get; set; }
        public int MainLocationId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string MainLocationDesc { get; set; }
        public string LocationDesc { get; set; }
        public string MainCategoryName { get; set; }
        public string CategoryName { get; set; }
        public string UpdatedBy { get; set; }
        public int LocationId { get; set; }
        public int Lost { get; set; }
        public Double Longitude { get; set; }
        public Double Latitude { get; set; }
        public string OwnerName { get; set; }
        public string Barcode { get; set; }
        public string SerialNo { get; set; }

        public string CheckedinBy { get; set; }

        public bool isKey { get; set; }

        public int SiteId { get; set; }

        public int CustomerId { get; set; }

        public string CustomerUName { get; set; }

        //NEW PARAMTERS
        public string Remarks { get; set; }

        public DateTime? LastVisitDate { get; set; }
        
        public string AssetMake { get; set; }

        public string AssetModel { get; set; }

        public string Contractor { get; set; }

        public int AccountId { get; set; }
        public string AccountName { get; set; }

        public string Usage { get; set; }

        public int Status { get; set; }

        public int SparePartParentId { get; set; }
        public string SparePartParentName { get; set; }
        public bool isSparePart { get; set; }

        public int ProjectId { get; set; }
        public string ProjectName { get; set; }

        public int MaintenancePlan { get; set; }

        //Spare Parts????
        public enum LostStatus : int
        {
            Unknown = -1,
            CheckedIn = 0,
            CheckedOut = 1,
            Transfered = 2
        }
        public static int InsertOrUpdateItemLost(ItemLost usersClass, string dbConnection,
      string auditDbConnection = "", bool isAuditEnabled = true)
        {

            int id = usersClass.Id;
            var action = (id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;

            try
            {
                if (usersClass != null)
                {

                    using (var connection = new SqlConnection(dbConnection))
                    {
                        connection.Open();
                        var cmd = new SqlCommand("InsertOrUpdateItemLost", connection);
                        
                        cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                        cmd.Parameters["@Id"].Value = usersClass.Id;

                        cmd.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                        cmd.Parameters["@Name"].Value = usersClass.Name;

                        cmd.Parameters.Add(new SqlParameter("@Comments", SqlDbType.NVarChar));
                        cmd.Parameters["@Comments"].Value = usersClass.Comments;

                        cmd.Parameters.Add(new SqlParameter("@SerialNo", SqlDbType.NVarChar));
                        cmd.Parameters["@SerialNo"].Value = usersClass.SerialNo;

                        cmd.Parameters.Add(new SqlParameter("@Barcode", SqlDbType.NVarChar));
                        cmd.Parameters["@Barcode"].Value = usersClass.Barcode;

                        cmd.Parameters.Add(new SqlParameter("@AssetCatId", SqlDbType.Int));
                        cmd.Parameters["@AssetCatId"].Value = usersClass.AssetCatId;

                        cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                        cmd.Parameters["@CreatedBy"].Value = usersClass.CreatedBy;

                        cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                        cmd.Parameters["@CreatedDate"].Value = usersClass.CreatedDate;

                        cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                        cmd.Parameters["@UpdatedDate"].Value = usersClass.UpdatedDate;

                        cmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                        cmd.Parameters["@UpdatedBy"].Value = usersClass.UpdatedBy;

                        cmd.Parameters.Add(new SqlParameter("@OwnerName", SqlDbType.NVarChar));
                        cmd.Parameters["@OwnerName"].Value = usersClass.OwnerName;
                        
                        cmd.Parameters.Add(new SqlParameter("@LocationId", SqlDbType.Int));
                        cmd.Parameters["@LocationId"].Value = usersClass.LocationId;

                        cmd.Parameters.Add(new SqlParameter("@MainAssetCatId", SqlDbType.Int));
                        cmd.Parameters["@MainAssetCatId"].Value = usersClass.MainAssetCatId;

                        cmd.Parameters.Add(new SqlParameter("@MainLocationId", SqlDbType.Int));
                        cmd.Parameters["@MainLocationId"].Value = usersClass.MainLocationId;

                        cmd.Parameters.Add(new SqlParameter("@Lost", SqlDbType.Int));
                        cmd.Parameters["@Lost"].Value = usersClass.Lost;

                        cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                        cmd.Parameters["@SiteId"].Value = usersClass.SiteId;

                        cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                        cmd.Parameters["@CustomerId"].Value = usersClass.CustomerId;
                        

                        cmd.Parameters.Add(new SqlParameter("@Latitude", SqlDbType.Float));
                        cmd.Parameters["@Latitude"].Value = usersClass.Latitude;

                        cmd.Parameters.Add(new SqlParameter("@Longitude", SqlDbType.Float));
                        cmd.Parameters["@Longitude"].Value = usersClass.Longitude;

                        cmd.Parameters.Add(new SqlParameter("@IsKey", SqlDbType.Bit));
                        cmd.Parameters["@IsKey"].Value = usersClass.isKey;

                        cmd.Parameters.Add(new SqlParameter("@CheckedinBy", SqlDbType.NVarChar));
                        cmd.Parameters["@CheckedinBy"].Value = usersClass.CheckedinBy;

                        cmd.Parameters.Add(new SqlParameter("@Remarks", SqlDbType.NVarChar));
                        cmd.Parameters["@Remarks"].Value = usersClass.Remarks;

                        cmd.Parameters.Add(new SqlParameter("@LastVisitDate", SqlDbType.DateTime));
                        cmd.Parameters["@LastVisitDate"].Value = usersClass.LastVisitDate;

                        cmd.Parameters.Add(new SqlParameter("@AssetMake", SqlDbType.NVarChar));
                        cmd.Parameters["@AssetMake"].Value = usersClass.AssetMake;

                        cmd.Parameters.Add(new SqlParameter("@AssetModel", SqlDbType.NVarChar));
                        cmd.Parameters["@AssetModel"].Value = usersClass.AssetModel;

                        cmd.Parameters.Add(new SqlParameter("@Contractor", SqlDbType.NVarChar));
                        cmd.Parameters["@Contractor"].Value = usersClass.Contractor;

                        cmd.Parameters.Add(new SqlParameter("@AccountId", SqlDbType.Int));
                        cmd.Parameters["@AccountId"].Value = usersClass.AccountId;

                        cmd.Parameters.Add(new SqlParameter("@ProjectId", SqlDbType.Int));
                        cmd.Parameters["@ProjectId"].Value = usersClass.ProjectId;

                        cmd.Parameters.Add(new SqlParameter("@MaintenancePlan", SqlDbType.Int));
                        cmd.Parameters["@MaintenancePlan"].Value = usersClass.MaintenancePlan;

                        cmd.Parameters.Add(new SqlParameter("@Status", SqlDbType.Int));
                        cmd.Parameters["@Status"].Value = usersClass.Status;

                        cmd.Parameters.Add(new SqlParameter("@SparePartParentId", SqlDbType.Int));
                        cmd.Parameters["@SparePartParentId"].Value = usersClass.SparePartParentId;

                        cmd.Parameters.Add(new SqlParameter("@isSparePart", SqlDbType.NVarChar));
                        cmd.Parameters["@isSparePart"].Value = usersClass.isSparePart;

                        cmd.Parameters.Add(new SqlParameter("@Usage", SqlDbType.NVarChar));
                        cmd.Parameters["@Usage"].Value = usersClass.Usage;


                        SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                        returnParameter.Direction = ParameterDirection.ReturnValue;

                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.ExecuteNonQuery();

                        if (id == 0)
                            id = (int)returnParameter.Value;
                        try
                        {
                            if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                            {
                                usersClass.Id = id;
                                var audit = new Arrowlabs.Mims.Audit.Models.ItemLost();
                                audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, usersClass.UpdatedBy);
                                Reflection.CopyProperties(usersClass, audit);
                                Arrowlabs.Mims.Audit.Models.ItemLost.InsertOrUpdateItemLost(audit, auditDbConnection);
                            }
                        }
                        catch (Exception ex)
                        {
                            MIMSLog.MIMSLogSave("Business Layer", "InsertOrUpdateItemLost", ex, dbConnection);
                        }
                    }
                }
                return id;
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("Business Layer", "InsertOrUpdateItemLost", ex, dbConnection);
                return id;
            }
        }
        public static ItemLost GetFullItemLostFromSqlReader(SqlDataReader reader)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

            var itemLostClass = new ItemLost();

            if (!string.IsNullOrEmpty(reader["Id"].ToString()))
                itemLostClass.Id = Convert.ToInt32(reader["Id"].ToString());
            itemLostClass.Name = reader["Name"].ToString();
            itemLostClass.Comments = reader["Comments"].ToString();
            itemLostClass.CreatedBy = reader["CreatedBy"].ToString();
            if (!string.IsNullOrEmpty(reader["AssetCatId"].ToString()))
                itemLostClass.AssetCatId = Convert.ToInt32(reader["AssetCatId"].ToString());

            if (!string.IsNullOrEmpty(reader["CreatedDate"].ToString()))
                itemLostClass.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());

            if (!string.IsNullOrEmpty(reader["UpdatedDate"].ToString()))
                itemLostClass.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"].ToString());

            if (!string.IsNullOrEmpty(reader["UpdatedBy"].ToString()))
                itemLostClass.UpdatedBy = reader["UpdatedBy"].ToString();

            if (!string.IsNullOrEmpty(reader["OwnerName"].ToString()))
                itemLostClass.OwnerName = reader["OwnerName"].ToString();
             
            if (!string.IsNullOrEmpty(reader["LocationId"].ToString()))
                itemLostClass.LocationId = Convert.ToInt32(reader["LocationId"].ToString());

            if (!string.IsNullOrEmpty(reader["Lost"].ToString()))
                itemLostClass.Lost = Convert.ToInt32(reader["Lost"].ToString());

            if (!string.IsNullOrEmpty(reader["Latitude"].ToString()))
                itemLostClass.Latitude = Convert.ToDouble(reader["Latitude"].ToString());

            if (!string.IsNullOrEmpty(reader["Longitude"].ToString()))
                itemLostClass.Longitude = Convert.ToDouble(reader["Longitude"].ToString());

            if (columns.Contains( "LocationDesc") && reader["LocationDesc"] != DBNull.Value)
                itemLostClass.LocationDesc = reader["LocationDesc"].ToString();

            if (columns.Contains( "SiteId") && reader["SiteId"] != DBNull.Value)
                itemLostClass.SiteId = Convert.ToInt32(reader["SiteId"].ToString());

            if (columns.Contains( "CustomerId") && reader["CustomerId"] != DBNull.Value)
                itemLostClass.CustomerId = Convert.ToInt32(reader["CustomerId"].ToString());
             
            if (columns.Contains( "IsKey") && reader["IsKey"] != DBNull.Value)
                itemLostClass.isKey = Convert.ToBoolean(reader["IsKey"].ToString());

            if (columns.Contains( "CheckedinBy") && reader["CheckedinBy"] != DBNull.Value)
                itemLostClass.CheckedinBy = reader["CheckedinBy"].ToString();

            if (columns.Contains( "CustomerUName") && reader["CustomerUName"] != DBNull.Value)
                itemLostClass.CustomerUName = reader["CustomerUName"].ToString();

            if (columns.Contains("MainLocationId") && reader["MainLocationId"] != DBNull.Value)
                itemLostClass.MainLocationId = Convert.ToInt32(reader["MainLocationId"].ToString());

            if (columns.Contains("MainAssetCatId") && reader["MainAssetCatId"] != DBNull.Value)
                itemLostClass.MainAssetCatId = Convert.ToInt32(reader["MainAssetCatId"].ToString());

            if (columns.Contains("Barcode") && reader["Barcode"] != DBNull.Value)
                itemLostClass.Barcode = reader["Barcode"].ToString();

            if (columns.Contains("SerialNo") && reader["SerialNo"] != DBNull.Value)
                itemLostClass.SerialNo = reader["SerialNo"].ToString();

            if (columns.Contains("Remarks") && reader["Remarks"] != DBNull.Value)
                itemLostClass.Remarks = reader["Remarks"].ToString();

            if (columns.Contains("LastVisitDate") && !string.IsNullOrEmpty(reader["LastVisitDate"].ToString()))
                itemLostClass.LastVisitDate = Convert.ToDateTime(reader["LastVisitDate"].ToString());

            if (columns.Contains("AssetMake") && reader["AssetMake"] != DBNull.Value)
                itemLostClass.AssetMake = reader["AssetMake"].ToString();

            if (columns.Contains("AssetModel") && reader["AssetModel"] != DBNull.Value)
                itemLostClass.AssetModel = reader["AssetModel"].ToString();
             
            if (columns.Contains("Contractor") && reader["Contractor"] != DBNull.Value)
                itemLostClass.Contractor = reader["Contractor"].ToString();

            if (columns.Contains("AccountName") && reader["AccountName"] != DBNull.Value)
                itemLostClass.AccountName = reader["AccountName"].ToString();

            if (columns.Contains("AccountId") && reader["AccountId"] != DBNull.Value)
                itemLostClass.AccountId = Convert.ToInt32(reader["AccountId"].ToString());

            if (columns.Contains("ProjectId") && reader["ProjectId"] != DBNull.Value)
                itemLostClass.ProjectId = Convert.ToInt32(reader["ProjectId"].ToString());

            if (columns.Contains("ProjectName") && reader["ProjectName"] != DBNull.Value)
                itemLostClass.ProjectName = reader["ProjectName"].ToString();
                        if (columns.Contains("SparePartParentName") && reader["SparePartParentName"] != DBNull.Value)
                itemLostClass.SparePartParentName = reader["SparePartParentName"].ToString();
            
            if (columns.Contains("MaintenancePlan") && reader["MaintenancePlan"] != DBNull.Value)
                itemLostClass.MaintenancePlan = Convert.ToInt32(reader["MaintenancePlan"].ToString());
            
            if (columns.Contains("SparePartParentId") && reader["SparePartParentId"] != DBNull.Value)
                itemLostClass.SparePartParentId = Convert.ToInt32(reader["SparePartParentId"].ToString());

            if (columns.Contains("Status") && reader["Status"] != DBNull.Value)
                itemLostClass.Status = Convert.ToInt32(reader["Status"].ToString());

            if (columns.Contains("Usage") && reader["Usage"] != DBNull.Value)
                itemLostClass.Usage = reader["Usage"].ToString();

            if (columns.Contains("isSparePart") && reader["isSparePart"] != DBNull.Value)
                itemLostClass.isSparePart = Convert.ToBoolean(reader["isSparePart"].ToString());
 
            return itemLostClass;
        }
        public static List<ItemLost> GetAllItemLost(string dbConnection)
        {
            var itemLostList = new List<ItemLost>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllItemLost", connection);

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = GetFullItemLostFromSqlReader(reader);
                    itemLostList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return itemLostList;
        }

        public static List<ItemLost> GetAllItemLostByLevel5(int siteid, string dbConnection)
        {
            var itemLostList = new List<ItemLost>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllItemLostByLevel5", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                sqlCommand.Parameters["@UserId"].Value = siteid;
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = GetFullItemLostFromSqlReader(reader);
                    itemLostList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return itemLostList;
        }
        public static List<ItemLost> GetAllItemLostBySparePartId(int siteid, string dbConnection)
        {
            var itemLostList = new List<ItemLost>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllItemLostBySparePartId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = siteid;
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = GetFullItemLostFromSqlReader(reader);
                    itemLostList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return itemLostList;
        }
        public static List<ItemLost> GetAllItemLostBySite(int siteid,string dbConnection)
        {
            var itemLostList = new List<ItemLost>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllItemLostBySite", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                sqlCommand.Parameters["@SiteId"].Value = siteid;
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = GetFullItemLostFromSqlReader(reader);
                    itemLostList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return itemLostList;
        }
        public static List<ItemLost> GetAllItemLostByCustomerId(int siteid, string dbConnection)
        {
            var itemLostList = new List<ItemLost>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllItemLostByCustomerId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = siteid;
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = GetFullItemLostFromSqlReader(reader);
                    itemLostList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return itemLostList;
        }
        public static bool DeleteItemLostById(int id, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var cmd = new SqlCommand("DeleteItemLostById", connection);

                cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                cmd.Parameters["@Id"].Value = id;

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            }
            return true;
        }

        public static ItemLost GetItemLostById(int id, string dbConnection)
        {
            ItemLost itemLostList = new ItemLost();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetItemLostById", connection);

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = id;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    itemLostList = GetFullItemLostFromSqlReader(reader);
                }
                connection.Close();
                reader.Close();
            }
            return itemLostList;
        }

        public static List<ItemLost> SearchItemLost(int AssetId, string Name ,int Location,bool IsKey,int SiteId ,int cid,int massetid,int mainloc,string dbConnection)
        {
            var itemLostList = new List<ItemLost>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("SearchItemLost", connection);

                if (AssetId > 0)
                {
                    sqlCommand.Parameters.Add(new SqlParameter("@AssetId", SqlDbType.Int));
                    sqlCommand.Parameters["@AssetId"].Value = AssetId;
                }
                if (massetid > 0)
                {
                    sqlCommand.Parameters.Add(new SqlParameter("@MainAssetCatId", SqlDbType.Int));
                    sqlCommand.Parameters["@MainAssetCatId"].Value = massetid;
                }
                if (mainloc > 0)
                {
                    sqlCommand.Parameters.Add(new SqlParameter("@MainLocation", SqlDbType.Int));
                    sqlCommand.Parameters["@MainLocation"].Value = mainloc;
                }
                if (!string.IsNullOrEmpty(Name))
                {
                    sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@Name"].Value = Name;
                }

                if (Location > 0)
                {
                    sqlCommand.Parameters.Add(new SqlParameter("@Location", SqlDbType.Int));
                    sqlCommand.Parameters["@Location"].Value = Location;
                }
                sqlCommand.Parameters.Add(new SqlParameter("@IsKey", SqlDbType.Bit));
                sqlCommand.Parameters["@IsKey"].Value = IsKey;

                sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                sqlCommand.Parameters["@SiteId"].Value = SiteId;

                sqlCommand.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                sqlCommand.Parameters["@CustomerId"].Value = cid;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = GetFullItemLostFromSqlReader(reader);
                    itemLostList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return itemLostList;
        }

        public static List<ItemLost> SearchItemLostByCustomerId(int AssetId, string Name, int Location, bool IsKey, int customerid, int massetid, int mainloc, string dbConnection)
        {
            var itemLostList = new List<ItemLost>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("SearchItemLostByCustomerId", connection);

                if (AssetId > 0)
                {
                    sqlCommand.Parameters.Add(new SqlParameter("@AssetId", SqlDbType.Int));
                    sqlCommand.Parameters["@AssetId"].Value = AssetId;
                }
                if (!string.IsNullOrEmpty(Name))
                {
                    sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@Name"].Value = Name;
                }

                if (Location > 0)
                {
                    sqlCommand.Parameters.Add(new SqlParameter("@Location", SqlDbType.Int));
                    sqlCommand.Parameters["@Location"].Value = Location;
                }
                if (massetid > 0)
                {
                    sqlCommand.Parameters.Add(new SqlParameter("@MainAssetCatId", SqlDbType.Int));
                    sqlCommand.Parameters["@MainAssetCatId"].Value = massetid;
                }
                if (mainloc > 0)
                {
                    sqlCommand.Parameters.Add(new SqlParameter("@MainLocation", SqlDbType.Int));
                    sqlCommand.Parameters["@MainLocation"].Value = mainloc;
                }
                sqlCommand.Parameters.Add(new SqlParameter("@IsKey", SqlDbType.Bit));
                sqlCommand.Parameters["@IsKey"].Value = IsKey;

                sqlCommand.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                sqlCommand.Parameters["@CustomerId"].Value = customerid;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = GetFullItemLostFromSqlReader(reader);
                    itemLostList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return itemLostList;
        }
        public static List<ItemLost> SearchItemLostByLevel5(int AssetId, string Name, int Location, bool IsKey, int customerid, string dbConnection)
        {
            var itemLostList = new List<ItemLost>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("SearchItemLostByLevel5", connection);

                if (AssetId > 0)
                {
                    sqlCommand.Parameters.Add(new SqlParameter("@AssetId", SqlDbType.Int));
                    sqlCommand.Parameters["@AssetId"].Value = AssetId;
                }
                if (!string.IsNullOrEmpty(Name))
                {
                    sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@Name"].Value = Name;
                }

                if (Location > 0)
                {
                    sqlCommand.Parameters.Add(new SqlParameter("@Location", SqlDbType.Int));
                    sqlCommand.Parameters["@Location"].Value = Location;
                }
                sqlCommand.Parameters.Add(new SqlParameter("@IsKey", SqlDbType.Bit));
                sqlCommand.Parameters["@IsKey"].Value = IsKey;

                sqlCommand.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                sqlCommand.Parameters["@UserId"].Value = customerid;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var userClass = GetFullItemLostFromSqlReader(reader);
                    itemLostList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return itemLostList;
        }

        public static int GetLastItemLostIdBySiteId(DateTime from, DateTime todate, int siteid, string dbConnection)
        {
            int lastUserId = 0;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetLastItemLostBySiteId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                sqlCommand.Parameters["@SiteId"].Value = siteid;
                sqlCommand.Parameters.Add(new SqlParameter("@fromdate", SqlDbType.DateTime));
                sqlCommand.Parameters["@fromdate"].Value = from;
                sqlCommand.Parameters.Add(new SqlParameter("@todate", SqlDbType.DateTime));
                sqlCommand.Parameters["@todate"].Value = todate;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    lastUserId = Convert.ToInt32(reader["ID"].ToString());
                    break;
                }
                connection.Close();
                reader.Close();
            }
            return lastUserId;
        }
        public static int GetLastItemLostIdByCustomerId(DateTime from, DateTime todate, int siteid, string dbConnection)
        {
            int lastUserId = 0;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetLastItemLostIdByCustomerId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@CId", SqlDbType.Int));
                sqlCommand.Parameters["@CId"].Value = siteid;
                sqlCommand.Parameters.Add(new SqlParameter("@fromdate", SqlDbType.DateTime));
                sqlCommand.Parameters["@fromdate"].Value = from;
                sqlCommand.Parameters.Add(new SqlParameter("@todate", SqlDbType.DateTime));
                sqlCommand.Parameters["@todate"].Value = todate;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    lastUserId = Convert.ToInt32(reader["ID"].ToString());
                    break;
                }
                connection.Close();
                reader.Close();
            }
            return lastUserId;
        }
        public static ItemLost GetItemLostByBarcode(string reference, string dbConnection)
        {
            ItemLost itemFound = null;
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("GetItemLostByBarcode", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@Reference", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@Reference"].Value = reference;
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        itemFound = GetFullItemLostFromSqlReader(reader);

                    }
                }
            }
            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("ItemLost", "GetItemLostByBarcode", exp, dbConnection);
            }
            return itemFound;
        }

        public static List<ItemLost> GetAllItemLostByLocationIdAndAssetCatId(int locationId, int assetCategoryId, string dbConnection)
        {
            var assetList = new List<ItemLost>();
            ItemLost asset = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllItemLostByLocationIdAndAssetCatId", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@LocationId", SqlDbType.Int));
                sqlCommand.Parameters["@LocationId"].Value = locationId;

                sqlCommand.Parameters.Add(new SqlParameter("@AssetCatId", SqlDbType.Int));
                sqlCommand.Parameters["@AssetCatId"].Value = assetCategoryId;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    asset = GetFullItemLostFromSqlReader(reader);
                    assetList.Add(asset);
                }

                connection.Close();
                reader.Close();
            }
            return assetList;
        }
    }
}
