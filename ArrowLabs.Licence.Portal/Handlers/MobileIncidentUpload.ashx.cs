﻿using Arrowlabs.Business.Layer;
using ArrowLabs.Licence.Portal.Helpers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace ArrowLabs.Licence.Portal.Handlers
{
    /// <summary>
    /// Summary description for MobileIncidentUpload
    /// </summary>
    public class MobileIncidentUpload : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                if (context.Request.Files.Count > 0)
                {
                    HttpFileCollection files = context.Request.Files;
                    string fname = string.Empty;

                    HttpPostedFile file = files[0];
                    fname = context.Server.MapPath("~/Uploads/" + file.FileName);

                    if (!System.IO.File.Exists(fname))
                    {
                        var fileStream = System.IO.File.Create(fname);
                        file.InputStream.Seek(0, System.IO.SeekOrigin.Begin);
                        file.InputStream.CopyTo(fileStream);
                        fileStream.Close();
                    }
                    //CommonUtility.CloudUploadFile()
                    context.Response.ContentType = "text/plain";
                    context.Response.Write(fname);

                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("MobileIncidentUpload", "ProcessRequest", ex, CommonUtility.dbConnection);
                context.Response.Write("Error");
            }
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}