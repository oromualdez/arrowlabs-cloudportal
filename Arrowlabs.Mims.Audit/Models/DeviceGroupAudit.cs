﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class DeviceGroupAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public int Id { get; set; }
        public int GroupId { get; set; }
        public int DeviceId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public int SiteId { get; set; }


        public static bool InsertDeviceGroupAudit(DeviceGroupAudit deviceGroup, string dbConnection)
        {
            var baseAuditId = BaseAudit.InsertBaseAudit(deviceGroup.BaseAudit, dbConnection);

            if (deviceGroup != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertOrUpdateDeviceGroup", connection);

                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;

                    cmd.Parameters.Add("@GroupId", SqlDbType.Int).Value = deviceGroup.GroupId;

                    cmd.Parameters.Add("@DeviceId", SqlDbType.Int).Value = deviceGroup.DeviceId;

                    cmd.Parameters.Add("@CreateDate", SqlDbType.DateTime).Value = deviceGroup.CreateDate;

                    cmd.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = deviceGroup.CreatedBy;
                    
                    cmd.Parameters.Add("@SiteId", SqlDbType.Int).Value = deviceGroup.SiteId;


                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }

    }
}
