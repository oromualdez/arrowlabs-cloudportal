﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Arrowlabs.Mims.Audit.Models
{
    public class  AssetAudit
    {
        public BaseAudit BaseAudit { get; set; } 
        public int Id { get; set; }
        public string Name { get; set; }
        public int Quantity { get; set; }
        public int LocationId { get; set; }
        public string LocationName { get; set; }
        public int AssetCategoryId { get; set; }
        public int SiteId { get; set; }
        public string AssetCategoryName { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }

        public int CustomerId { get; set; }


        private static AssetAudit GetAssetAuditFromSqlReader(SqlDataReader reader)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

            var obj = new AssetAudit();
            obj.BaseAudit = BaseAudit.GetBaseAuditFromSqlReader(reader);

            if (columns.Contains("Id") && reader["Id"] != DBNull.Value)
                obj.Id = Convert.ToInt32(reader["Id"].ToString());

            if (columns.Contains("Name") && reader["Name"] != DBNull.Value)
                obj.Name = reader["Name"].ToString();

            if (columns.Contains("Quantity") && reader["Quantity"] != DBNull.Value)
                obj.Quantity = Convert.ToInt32(reader["Quantity"].ToString());

            if (columns.Contains("LocationId") && reader["LocationId"] != DBNull.Value)
                obj.LocationId = Convert.ToInt32(reader["LocationId"].ToString());

            if (columns.Contains("AssetCategoryId") && reader["AssetCategoryId"] != DBNull.Value)
                obj.AssetCategoryId = Convert.ToInt32(reader["AssetCategoryId"].ToString());

            if (columns.Contains("CreatedDate") && reader["CreatedDate"] != DBNull.Value)
                obj.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());

            if (columns.Contains("CreatedBy") && reader["CreatedBy"] != DBNull.Value)
                obj.CreatedBy = reader["CreatedBy"].ToString();

            if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                obj.SiteId = Convert.ToInt32(reader["SiteId"].ToString());

            if (columns.Contains("CustomerId") && reader["CustomerId"] != DBNull.Value)
                obj.CustomerId = Convert.ToInt32(reader["CustomerId"].ToString());


            return obj;
        }
        public static List<AssetAudit> GetAllAssetAudit(DateTime fromDateTime, DateTime toDateTime, string dbConnection)
        {
            var assetAuditList = new List<AssetAudit>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var cmd = new SqlCommand("GetAllAssetAudit", connection);

                cmd.Parameters.Add("@fromDateTime", SqlDbType.DateTime).Value = fromDateTime;
                cmd.Parameters.Add("@toDateTime", SqlDbType.DateTime).Value = toDateTime;

                cmd.CommandType = CommandType.StoredProcedure;
                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var assetAudit = GetAssetAuditFromSqlReader(reader);
                    assetAuditList.Add(assetAudit);
                }
                connection.Close();
                reader.Close();
            }
            return assetAuditList;
        }
        public static bool InsertAssetAudit(AssetAudit assetAudit, string dbConnection)
        {
            var baseAuditId = BaseAudit.InsertBaseAudit(assetAudit.BaseAudit, dbConnection);

            if (assetAudit != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertAssetAudit", connection);

                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;
                    cmd.Parameters.Add("@Id", SqlDbType.Int).Value = assetAudit.Id;
                    cmd.Parameters.Add("@Name", SqlDbType.NVarChar).Value = assetAudit.Name;
                    cmd.Parameters.Add("@Quantity", SqlDbType.Int).Value = assetAudit.Quantity;
                    cmd.Parameters.Add("@AssetCategoryId", SqlDbType.Int).Value = assetAudit.AssetCategoryId;
                    cmd.Parameters.Add("@LocationId", SqlDbType.Int).Value = assetAudit.LocationId;
                    cmd.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = assetAudit.CreatedDate;
                    cmd.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = assetAudit.CreatedBy;
                    cmd.Parameters.Add("@SiteId", SqlDbType.Int).Value = assetAudit.SiteId;
                    cmd.Parameters.Add("@CustomerId", SqlDbType.Int).Value = assetAudit.CustomerId;
                    
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }
    }
}
