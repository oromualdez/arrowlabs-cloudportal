﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Arrowlabs.Business.Layer
{
    [Serializable]
    public class UserModules
    {
        public int UserId { get; set; }
        public int ModuleId { get; set; }
        public string AccountName { get; set; }
        public string UserName { get; set; }
        public int SiteId { get; set; }
        public int CustomerId { get; set; }


        public static List<UserModules> GetAllModulesByUserIdAndUserName(string acctName, int userid, string dbConnection)
        {
            var modList = new List<UserModules>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllModulesByUserIdAndAccountName", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@AccountName", SqlDbType.NVarChar));
                sqlCommand.Parameters["@AccountName"].Value = acctName;
                sqlCommand.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                sqlCommand.Parameters["@UserId"].Value = userid;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllModulesByUserIdAndAccountName";
                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    var userClass = GetFromSqlReader(reader);
                    modList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return modList;
        }

        public static List<UserModules> GetAllModulesByUsername(string Name, string dbConnection)
        {
            var modList = new List<UserModules>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllModulesByUsername", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = Name;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetAllModulesByUsername";
                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    var userClass = GetFromSqlReader(reader);
                    modList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return modList;
        }


        public static List<UserModules> GetUserModuleMapByCIdAndModuleId(int cid,int mid, string dbConnection)
        {
            var modList = new List<UserModules>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetUserModuleMapByCIdAndModuleId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@CId", SqlDbType.NVarChar));
                sqlCommand.Parameters["@CId"].Value = cid;
                sqlCommand.Parameters.Add(new SqlParameter("@ModuleId", SqlDbType.NVarChar));
                sqlCommand.Parameters["@ModuleId"].Value = mid;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetUserModuleMapByCIdAndModuleId";
                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    var userClass = GetFromSqlReader(reader);
                    modList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return modList;
        }

        public static List<UserModules> GetUserModuleMapByUserIdAndModuleId(int uid, int mid, string dbConnection)
        {
            var modList = new List<UserModules>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetUserModuleMapByUserIdAndModuleId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@UserId", SqlDbType.NVarChar));
                sqlCommand.Parameters["@UserId"].Value = uid;
                sqlCommand.Parameters.Add(new SqlParameter("@ModuleId", SqlDbType.NVarChar));
                sqlCommand.Parameters["@ModuleId"].Value = mid;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "GetUserModuleMapByUserIdAndModuleId";
                var reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    var userClass = GetFromSqlReader(reader);
                    modList.Add(userClass);
                }
                connection.Close();
                reader.Close();
            }
            return modList;
        }

        public static UserModules GetFromSqlReader(SqlDataReader reader)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
            var Class = new UserModules();

            if (!string.IsNullOrEmpty(reader["UserId"].ToString()))
                Class.UserId = Convert.ToInt32(reader["UserId"].ToString());
            if (!string.IsNullOrEmpty(reader["ModuleId"].ToString()))
                Class.ModuleId = Convert.ToInt32(reader["ModuleId"].ToString());
            Class.AccountName = reader["AccountName"].ToString();
            Class.UserName = reader["UserName"].ToString();

            if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                Class.SiteId = Convert.ToInt32(reader["SiteId"].ToString());

            if (columns.Contains("CustomerId") && reader["CustomerId"] != DBNull.Value)
                Class.CustomerId = Convert.ToInt32(reader["CustomerId"].ToString()); 

            return Class;
        }
         public static void DeleteUserModuleMapByUserIdAndAccountName(string acctName, int userid, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteUserModuleMapByUserIdAndAccountName", connection);

                cmd.Parameters.Add(new SqlParameter("@AccountName", SqlDbType.NVarChar));
                cmd.Parameters["@AccountName"].Value = acctName;

                cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                cmd.Parameters["@UserId"].Value = userid;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "DeleteUserModuleMapByUserIdAndAccountName";

                cmd.ExecuteNonQuery();
            }
        }

         public static void DeleteUserModuleMapByUserId(int userid, string dbConnection)
         {
             using (var connection = new SqlConnection(dbConnection))
             {
                 connection.Open();
                 var cmd = new SqlCommand("DeleteUserModuleMapByUserId", connection);


                 cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                 cmd.Parameters["@UserId"].Value = userid;

                 cmd.CommandType = CommandType.StoredProcedure;

                 cmd.CommandText = "DeleteUserModuleMapByUserId";

                 cmd.ExecuteNonQuery();
             }
         }
         public static void DeleteUserModuleMapByUserIdAndModuleId(int userid,int moduleid, string dbConnection)
         {
             using (var connection = new SqlConnection(dbConnection))
             {
                 connection.Open();
                 var cmd = new SqlCommand("DeleteUserModuleMapByUserIdAndModuleId", connection);


                 cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                 cmd.Parameters["@UserId"].Value = userid;
                 cmd.Parameters.Add(new SqlParameter("@moduleid", SqlDbType.Int));
                 cmd.Parameters["@ModuleId"].Value = moduleid;

                 cmd.CommandType = CommandType.StoredProcedure;

                 cmd.CommandText = "DeleteUserModuleMapByUserIdAndModuleId";

                 cmd.ExecuteNonQuery();
             }
         }
         public static bool InsertUserModuleMap(int UserId, int ModuleId, string dbConnection, string username,int siteId,int cid)
         {
             if (UserId > 0 && ModuleId > 0)
             {


                 using (var connection = new SqlConnection(dbConnection))
                 {
                     connection.Open();
                     var cmd = new SqlCommand("InsertUserModuleMap", connection);

                     cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                     cmd.Parameters["@UserId"].Value = UserId;

                     cmd.Parameters.Add(new SqlParameter("@ModuleId", SqlDbType.Int));
                     cmd.Parameters["@ModuleId"].Value = ModuleId;

                     cmd.Parameters.Add(new SqlParameter("@UserName", SqlDbType.NVarChar));
                     cmd.Parameters["@UserName"].Value = username;

                     cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                     cmd.Parameters["@SiteId"].Value = siteId;

                     cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                     cmd.Parameters["@CustomerId"].Value = cid;

                     cmd.CommandType = CommandType.StoredProcedure;

                     cmd.CommandText = "InsertUserModuleMap";

                     cmd.ExecuteNonQuery();
                 }
             }
             return true;
         }

    }
}
