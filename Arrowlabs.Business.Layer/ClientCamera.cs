﻿using Arrowlabs.Mims.Audit.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Business.Layer
{
    public class ClientCamera
    {
        public int Id { get; set; }
        public string MacAddress { get; set; }
        public string CameraName { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public int SiteId { get; set; }
        private static ClientCamera GetClientCameraFromSqlReader(SqlDataReader reader)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
            var clientCamera = new ClientCamera();
            if (reader["Id"] != DBNull.Value)
                clientCamera.Id = Convert.ToInt32(reader["Id"].ToString());
            if (reader["MacAddress"] != DBNull.Value)
                clientCamera.MacAddress = reader["MacAddress"].ToString();
            if (reader["CameraName"] != DBNull.Value)
                clientCamera.CameraName = reader["CameraName"].ToString();
            if (reader["CreatedDate"] != DBNull.Value)
                clientCamera.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            if (reader["UpdatedDate"] != DBNull.Value)
                clientCamera.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"].ToString());
            if (reader["CreatedBy"] != DBNull.Value)
                clientCamera.CreatedBy = reader["CreatedBy"].ToString();
            if (columns.Contains("SiteId") && reader["SiteId"] != DBNull.Value)
                clientCamera.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
         
            return clientCamera;
        }
        public static bool InsertorUpdateClientCamera(ClientCamera clientCamera, string dbConnection,
        string auditDbConnection = "", bool isAuditEnabled = true)
        {
            int id = 0;

            var action = (clientCamera.Id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;


            if (clientCamera != null)
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertorUpdateClientCamera", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = clientCamera.Id;

                    cmd.Parameters.Add(new SqlParameter("@MacAddress", SqlDbType.NChar));
                    cmd.Parameters["@MacAddress"].Value = clientCamera.MacAddress;

                    cmd.Parameters.Add(new SqlParameter("@CameraName", SqlDbType.NVarChar));
                    cmd.Parameters["@CameraName"].Value = clientCamera.CameraName;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = clientCamera.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = clientCamera.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = clientCamera.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = clientCamera.SiteId;

                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();

                    id = (int)returnParameter.Value;


                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            var audit = new ClientCameraAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, clientCamera.CreatedBy);
                            Reflection.CopyProperties(clientCamera, audit);
                            //ClientCameraAudit.InsertorUpdateClientCamera(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertorUpdateClientCamera", ex, dbConnection);
                    }
                }
            }
            return true;
        }
        public static List<ClientCamera> GetAllClientCameras( string dbConnection)
        {
            List<ClientCamera> allClientCamera = new List<ClientCamera>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var cmd = new SqlCommand("GetAllClientCameras", connection);

                cmd.CommandType = CommandType.StoredProcedure;

                var resultSet = cmd.ExecuteReader();
                while (resultSet.Read())
                {
                    var groupRole = GetClientCameraFromSqlReader(resultSet);
                    allClientCamera.Add(groupRole);
                }
                connection.Close();
                resultSet.Close();
                return allClientCamera;
            }
        }
        public static List<ClientCamera> GetAllClientCamerasByMacAddress(string macAddress, string dbConnection)
        {
            List<ClientCamera> allClientCamera = new List<ClientCamera>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var cmd = new SqlCommand("GetAllClientCamerasByMacAddress", connection);

                cmd.Parameters.Add(new SqlParameter("@MacAddress", SqlDbType.NChar));
                cmd.Parameters["@MacAddress"].Value = macAddress;

                cmd.CommandType = CommandType.StoredProcedure;

                var resultSet = cmd.ExecuteReader();
                while (resultSet.Read())
                {
                    var groupRole = GetClientCameraFromSqlReader(resultSet);
                    allClientCamera.Add(groupRole);
                }
                connection.Close();
                resultSet.Close();
                return allClientCamera;
            }
        }
        public static bool DeleteClientCamerabyCameraName(string cameraName, string dbConnection,
        string auditDbConnection = "", bool isAuditEnabled = true,string auditBy = "")
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("DeleteClientCamerabyCameraName", connection);

                cmd.Parameters.Add(new SqlParameter("@CameraName", SqlDbType.NChar));
                cmd.Parameters["@CameraName"].Value = cameraName;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.ExecuteNonQuery();

                try
                {
                    if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection) && !string.IsNullOrEmpty(auditBy))
                    {
                        var bAudit = BaseAudit.FillBaseAudit(AppConstants.Account, Arrowlabs.Mims.Audit.Enums.Action.BeforeDelete, auditBy, "DeleteClientCamerabyCameraName");
                        BaseAudit.InsertBaseAudit(bAudit, auditDbConnection);
                    }
                }
                catch (Exception ex)
                {
                    MIMSLog.MIMSLogSave("Business Layer", "InsertorUpdateClientCamera", ex, dbConnection);
                }


            }
            return true;
        }
    }
}
