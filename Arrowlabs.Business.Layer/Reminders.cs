﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using Arrowlabs.Mims.Audit.Models;


namespace Arrowlabs.Business.Layer
{
    public class Reminders
    {
        public enum ColorCodes
        {
            Red = 0,
            Blue = 1,
            Green = 2,
            Yellow = 3
        }
        public string CreatedBy { get; set; }
        public int ID { get; set; }
        public string ReminderNote { get; set; }
        public string ReminderTopic { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? ReminderDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int AccountId { get; set; }
        public string AccountName { get; set; }

        public bool IsCompleted { get; set; }

        public string FromDate { get; set; }

        public string ToDate { get; set; }

        public bool isBlank = false;

        public int ColorCode { get; set; }

        public int SiteId { get; set; }

        public int CustomerId { get; set; }

        public static Reminders GetRemindersFromSqlReader(SqlDataReader resultSet)
        {
            var reminder = new Reminders();
            var columns = Enumerable.Range(0, resultSet.FieldCount).Select(resultSet.GetName).ToList();
            if (resultSet["ID"] != DBNull.Value)
                reminder.ID = Convert.ToInt16(resultSet["ID"].ToString());
            if (resultSet["CreatedBy"] != DBNull.Value)
                reminder.CreatedBy = resultSet["CreatedBy"].ToString();
            if (resultSet["CreatedDate"] != DBNull.Value)
                reminder.CreatedDate = Convert.ToDateTime(resultSet["CreatedDate"].ToString());
            if (resultSet["Updateddate"] != DBNull.Value)
                reminder.UpdatedDate = Convert.ToDateTime(resultSet["Updateddate"].ToString());
            if (resultSet["ReminderNote"] != DBNull.Value)
                reminder.ReminderNote = resultSet["ReminderNote"].ToString();
            if (resultSet["ReminderTopic"] != DBNull.Value)
                reminder.ReminderTopic = resultSet["ReminderTopic"].ToString();
            if (resultSet["ReminderDate"] != DBNull.Value)
                reminder.ReminderDate = Convert.ToDateTime(resultSet["ReminderDate"].ToString());

            if (columns.Contains( "ColorCode"))
                if (resultSet["ColorCode"] != DBNull.Value)
                    reminder.ColorCode = Convert.ToInt32(resultSet["ColorCode"]);

            if (columns.Contains( "AccountId"))
                if (resultSet["AccountId"] != DBNull.Value)
                    reminder.AccountId = Convert.ToInt32(resultSet["AccountId"]);

            if (columns.Contains( "AccountName"))
                if (resultSet["AccountName"] != DBNull.Value)
                    reminder.AccountName = resultSet["AccountName"].ToString();

            if (columns.Contains( "FromDate"))
                if (resultSet["FromDate"] != DBNull.Value)
                    reminder.FromDate = resultSet["FromDate"].ToString();

            if (columns.Contains( "ToDate"))
                if (resultSet["ToDate"] != DBNull.Value)
                    reminder.ToDate = resultSet["ToDate"].ToString();

            if (columns.Contains( "IsCompleted"))
                if (resultSet["IsCompleted"] != DBNull.Value)
                    reminder.IsCompleted = Convert.ToBoolean(resultSet["IsCompleted"].ToString());

            if (columns.Contains( "SiteId") && resultSet["SiteId"] != DBNull.Value)
                reminder.SiteId = Convert.ToInt32(resultSet["SiteId"]);
            
            return reminder;
        }
        public static Reminders GetReminderById(int Id, string dbConnection)
        {
            using (var connection = new SqlConnection(dbConnection))
            {
                Reminders userTask = null;
                connection.Open();
                var sqlCommand = new SqlCommand("GetReminderById", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = Id;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var resultSet = sqlCommand.ExecuteReader();
                while (resultSet.Read())
                {
                    userTask = GetRemindersFromSqlReader(resultSet);
                }
                connection.Close();
                resultSet.Close();
                return userTask;
            }

        }
        public static List<Reminders> GetAllReminders(string dbConnection)
        {
            var userTasks = new List<Reminders>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllReminders", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var resultSet = sqlCommand.ExecuteReader();
                while (resultSet.Read())
                {
                    var userTask = GetRemindersFromSqlReader(resultSet);
                    userTasks.Add(userTask);
                }
                connection.Close();
                resultSet.Close();
            }
            return userTasks;
        }
        public static List<Reminders> GetAllRemindersByDateAndCreator(DateTime date, string name, string dbConnection)
        {
            var userTasks = new List<Reminders>();
            using (var connection = new SqlConnection(dbConnection))
            {
                Reminders userTask = null;
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllRemindersByDateAndCreator", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                sqlCommand.Parameters["@CreatedBy"].Value = name;
                sqlCommand.Parameters.Add(new SqlParameter("@ReminderDate", SqlDbType.DateTime));
                sqlCommand.Parameters["@ReminderDate"].Value = date;
                sqlCommand.CommandText = "GetAllRemindersByDateAndCreator";
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var resultSet = sqlCommand.ExecuteReader();
                while (resultSet.Read())
                {
                    userTask = GetRemindersFromSqlReader(resultSet);
                    userTasks.Add(userTask);
                }
                connection.Close();
                resultSet.Close();
                return userTasks;
            }
        }
        public static List<Reminders> GetAllRemindersByCreator(string name, string dbConnection)
        {
            var userTasks = new List<Reminders>();
            using (var connection = new SqlConnection(dbConnection))
            {
                Reminders userTask = null;
                connection.Open();
                var sqlCommand = new SqlCommand("GetAllRemindersByCreator", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                sqlCommand.Parameters["@CreatedBy"].Value = name;
                sqlCommand.CommandText = "GetAllRemindersByCreator";
                sqlCommand.CommandType = CommandType.StoredProcedure;
                var resultSet = sqlCommand.ExecuteReader();
                while (resultSet.Read())
                {
                    userTask = GetRemindersFromSqlReader(resultSet);
                    userTasks.Add(userTask);
                }
                connection.Close();
                resultSet.Close();
                return userTasks;
            }
        }
        public static int InsertorUpdateReminder(Reminders reminder, string dbConnection,
            string auditDbConnection = "", bool isAuditEnabled = true)
        {
            int id = 0;
            if (reminder != null)
            {
                id = reminder.ID;
                var action = (id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate; 

                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var cmd = new SqlCommand("InsertOrUpdateReminder", connection);

                    cmd.Parameters.Add(new SqlParameter("@ID", SqlDbType.Int));
                    cmd.Parameters["@ID"].Value = reminder.ID;

                    cmd.Parameters.Add(new SqlParameter("@ColorCode", SqlDbType.Int));
                    cmd.Parameters["@ColorCode"].Value = reminder.ColorCode;

                    cmd.Parameters.Add(new SqlParameter("@FromDate", SqlDbType.NVarChar));
                    cmd.Parameters["@FromDate"].Value = reminder.FromDate;

                    cmd.Parameters.Add(new SqlParameter("@ToDate", SqlDbType.NVarChar));
                    cmd.Parameters["@ToDate"].Value = reminder.ToDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = reminder.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = reminder.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = reminder.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@ReminderNote", SqlDbType.NVarChar));
                    cmd.Parameters["@ReminderNote"].Value = reminder.ReminderNote;

                    cmd.Parameters.Add(new SqlParameter("@ReminderTopic", SqlDbType.NVarChar));
                    cmd.Parameters["@ReminderTopic"].Value = reminder.ReminderTopic;

                    cmd.Parameters.Add(new SqlParameter("@ReminderDate", SqlDbType.DateTime));
                    cmd.Parameters["@ReminderDate"].Value = reminder.ReminderDate;

                    cmd.Parameters.Add(new SqlParameter("@IsCompleted", SqlDbType.Bit));
                    cmd.Parameters["@IsCompleted"].Value = reminder.IsCompleted;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = reminder.SiteId;

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                                        cmd.Parameters["@CustomerId"].Value = reminder.CustomerId;
                    
                    

                    var returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.CommandText = "InsertOrUpdateReminder";

                    cmd.ExecuteNonQuery();

                    if (id == 0)
                        id = (int)returnParameter.Value;

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            reminder.ID = id;
                            var audit = new ReminderAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, reminder.UpdatedBy);
                            Reflection.CopyProperties(reminder, audit);
                            ReminderAudit.InsertReminderAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertOrUpdateReminder", ex, dbConnection);
                    }

                }
            }
            return id;
        }
        public static bool DeleteReminderById(int RemindersId, string dbConnection)
        {
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var cmd = new SqlCommand("DeleteReminderById", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = RemindersId;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.CommandText = "DeleteReminderById";

                    cmd.ExecuteNonQuery();
                    return true;
                }

            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
