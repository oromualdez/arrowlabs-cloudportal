﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Runtime.Serialization;
using System.IO;

namespace Arrowlabs.Business.Layer
{
    public enum UserTaskDocumentType
    {
        None,
        TaskImage,
        TaskDocument,
        Signature
    }
    public class UserTaskAttachment
    {
        public int Id { get; set; }
        public int TaskId { get; set; }
        public int UserId { get; set; }
    //    public bool IsInProgress { get; set; }
        public bool IsDeleted { get; set; }
        public string UpdatedBy { get; set; }
        public string DocumentType { get; set; }
        public string DocumentName { get; set; }
        public string DocumentPath { get; set; }
        public string ImageNote { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string AccountName { get; set; }
        public bool IsUpdated { get; set; }

        public bool IsSignature { get; set; }

        public bool IsPortal { get; set; }

        public string ImageByte { get; set; }

        public int ChecklistId { get; set; }
        public int SiteId { get; set; }

        public int CustomerId { get; set; }
   
        /// <summary>
        /// It returns all attachments linked with task.
        /// </summary>
        /// <param name="taskId"></param>
        /// <param name="dbConnection"></param>
        /// <returns></returns>
        public static List<UserTaskAttachment> GetTaskAttachmentsByTaskId(int taskId, string dbConnection)
        {
            List<UserTaskAttachment> taskAttachments = new List<UserTaskAttachment>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetTaskAttachmentsByTaskId", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@TaskId", SqlDbType.Int));
                sqlCommand.Parameters["@TaskId"].Value = taskId;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                var resultSet = sqlCommand.ExecuteReader();
                while (resultSet.Read())
                {
                    if (taskAttachments == null)
                    {
                        taskAttachments = new List<UserTaskAttachment>();
                    }
                    var taskAttachment = new UserTaskAttachment();

                    if (resultSet["Id"] != DBNull.Value)
                        taskAttachment.Id = Convert.ToInt32(resultSet["Id"].ToString());
                    if (resultSet["TaskId"] != DBNull.Value)
                        taskAttachment.TaskId = Convert.ToInt32(resultSet["TaskId"].ToString());
                    if (resultSet["DocumentType"] != DBNull.Value)
                        taskAttachment.DocumentType = resultSet["DocumentType"].ToString();
                    if (resultSet["CreatedDate"] != DBNull.Value)
                       taskAttachment.CreatedDate = Convert.ToDateTime(resultSet["CreatedDate"].ToString());
                    if (resultSet["UpdatedDate"] != DBNull.Value)
                       taskAttachment.UpdatedDate = Convert.ToDateTime(resultSet["UpdatedDate"].ToString());
                    if (resultSet["CreatedBy"] != DBNull.Value)
                      taskAttachment.CreatedBy = resultSet["CreatedBy"].ToString();
                    if (resultSet["UserId"] != DBNull.Value)
                      taskAttachment.UserId = Convert.ToInt32(resultSet["UserId"].ToString());
                    if (resultSet["IsDeleted"] != DBNull.Value)
                       taskAttachment.IsDeleted = (String.IsNullOrEmpty(resultSet["IsDeleted"].ToString())) ? false : Convert.ToBoolean(resultSet["IsDeleted"].ToString());
                    if (resultSet["DocumentPath"] != DBNull.Value)
                      taskAttachment.DocumentPath = resultSet["DocumentPath"].ToString();
                    if (resultSet["DocumentName"] != DBNull.Value)
                        taskAttachment.DocumentName = resultSet["DocumentName"].ToString();
                    if (resultSet["ImageNote"] != DBNull.Value)
                        taskAttachment.ImageNote = resultSet["ImageNote"].ToString();

                    if (resultSet["IsSignature"] != DBNull.Value)
                        taskAttachment.IsSignature = Convert.ToBoolean(resultSet["IsSignature"].ToString());

                    if (resultSet["IsPortal"] != DBNull.Value)
                        taskAttachment.IsPortal = Convert.ToBoolean(resultSet["IsPortal"].ToString());
                     
                    if (resultSet["ChecklistId"] != DBNull.Value)
                        taskAttachment.ChecklistId = Convert.ToInt32(resultSet["ChecklistId"].ToString());

                    if (resultSet["SiteId"] != DBNull.Value)
                        taskAttachment.SiteId = Convert.ToInt32(resultSet["SiteId"].ToString());

                    if (resultSet["CustomerId"] != DBNull.Value)
                        taskAttachment.CustomerId = Convert.ToInt32(resultSet["CustomerId"].ToString());
                    

                    if (!String.IsNullOrEmpty(taskAttachment.DocumentPath))
                    {
                        if (File.Exists(taskAttachment.DocumentPath))
                            taskAttachment.ImageByte = Convert.ToBase64String(File.ReadAllBytes(taskAttachment.DocumentPath));
                    }

                    taskAttachments.Insert(0,taskAttachment);
                }
                connection.Close();
                resultSet.Close();
                return taskAttachments;
            }

        }

        public static List<UserTaskAttachment> GetTaskAttachmentByCreatedUser(string name, string dbConnection)
        {
            List<UserTaskAttachment> taskAttachments = new List<UserTaskAttachment>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetTaskAttachmentByCreatedUser", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar));
                sqlCommand.Parameters["@Name"].Value = name;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                var resultSet = sqlCommand.ExecuteReader();
                while (resultSet.Read())
                {
                    if (taskAttachments == null)
                    {
                        taskAttachments = new List<UserTaskAttachment>();
                    }
                    var taskAttachment = new UserTaskAttachment();

                    if (resultSet["Id"] != DBNull.Value)
                        taskAttachment.Id = Convert.ToInt32(resultSet["Id"].ToString());
                    if (resultSet["TaskId"] != DBNull.Value)
                        taskAttachment.TaskId = Convert.ToInt32(resultSet["TaskId"].ToString());
                    if (resultSet["DocumentType"] != DBNull.Value)
                        taskAttachment.DocumentType = resultSet["DocumentType"].ToString();
                    if (resultSet["CreatedDate"] != DBNull.Value)
                        taskAttachment.CreatedDate = Convert.ToDateTime(resultSet["CreatedDate"].ToString());
                    if (resultSet["UpdatedDate"] != DBNull.Value)
                        taskAttachment.UpdatedDate = Convert.ToDateTime(resultSet["UpdatedDate"].ToString());
                    if (resultSet["CreatedBy"] != DBNull.Value)
                        taskAttachment.CreatedBy = resultSet["CreatedBy"].ToString();
                    if (resultSet["UserId"] != DBNull.Value)
                        taskAttachment.UserId = Convert.ToInt32(resultSet["UserId"].ToString());
                    if (resultSet["IsDeleted"] != DBNull.Value)
                        taskAttachment.IsDeleted = (String.IsNullOrEmpty(resultSet["IsDeleted"].ToString())) ? false : Convert.ToBoolean(resultSet["IsDeleted"].ToString());
                    if (resultSet["DocumentPath"] != DBNull.Value)
                        taskAttachment.DocumentPath = resultSet["DocumentPath"].ToString();
                    if (resultSet["DocumentName"] != DBNull.Value)
                        taskAttachment.DocumentName = resultSet["DocumentName"].ToString();
                    if (resultSet["ImageNote"] != DBNull.Value)
                        taskAttachment.ImageNote = resultSet["ImageNote"].ToString();

                    if (resultSet["IsSignature"] != DBNull.Value)
                        taskAttachment.IsSignature = Convert.ToBoolean(resultSet["IsSignature"].ToString());

                    if (resultSet["IsPortal"] != DBNull.Value)
                        taskAttachment.IsPortal = Convert.ToBoolean(resultSet["IsPortal"].ToString());

                    if (resultSet["ChecklistId"] != DBNull.Value)
                        taskAttachment.ChecklistId = Convert.ToInt32(resultSet["ChecklistId"].ToString());

                    if (resultSet["CustomerId"] != DBNull.Value)
                        taskAttachment.CustomerId = Convert.ToInt32(resultSet["CustomerId"].ToString());

                    if (resultSet["SiteId"] != DBNull.Value)
                        taskAttachment.SiteId = Convert.ToInt32(resultSet["SiteId"].ToString());

                    if (!String.IsNullOrEmpty(taskAttachment.DocumentPath))
                    {
                        if (File.Exists(taskAttachment.DocumentPath))
                            taskAttachment.ImageByte = Convert.ToBase64String(File.ReadAllBytes(taskAttachment.DocumentPath));
                    }

                    taskAttachments.Insert(0, taskAttachment);
                }
                connection.Close();
                resultSet.Close();
                return taskAttachments;
            }

        }

        public static List<UserTaskAttachment> GetTaskAttachmentBySiteId(int siteId, string dbConnection)
        {
            List<UserTaskAttachment> taskAttachments = new List<UserTaskAttachment>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetTaskAttachmentBySiteId", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                sqlCommand.Parameters["@SiteId"].Value = siteId;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                var resultSet = sqlCommand.ExecuteReader();
                while (resultSet.Read())
                {
                    if (taskAttachments == null)
                    {
                        taskAttachments = new List<UserTaskAttachment>();
                    }
                    var taskAttachment = new UserTaskAttachment();

                    if (resultSet["Id"] != DBNull.Value)
                        taskAttachment.Id = Convert.ToInt32(resultSet["Id"].ToString());
                    if (resultSet["TaskId"] != DBNull.Value)
                        taskAttachment.TaskId = Convert.ToInt32(resultSet["TaskId"].ToString());
                    if (resultSet["DocumentType"] != DBNull.Value)
                        taskAttachment.DocumentType = resultSet["DocumentType"].ToString();
                    if (resultSet["CreatedDate"] != DBNull.Value)
                        taskAttachment.CreatedDate = Convert.ToDateTime(resultSet["CreatedDate"].ToString());
                    if (resultSet["UpdatedDate"] != DBNull.Value)
                        taskAttachment.UpdatedDate = Convert.ToDateTime(resultSet["UpdatedDate"].ToString());
                    if (resultSet["CreatedBy"] != DBNull.Value)
                        taskAttachment.CreatedBy = resultSet["CreatedBy"].ToString();
                    if (resultSet["UserId"] != DBNull.Value)
                        taskAttachment.UserId = Convert.ToInt32(resultSet["UserId"].ToString());
                    if (resultSet["IsDeleted"] != DBNull.Value)
                        taskAttachment.IsDeleted = (String.IsNullOrEmpty(resultSet["IsDeleted"].ToString())) ? false : Convert.ToBoolean(resultSet["IsDeleted"].ToString());
                    if (resultSet["DocumentPath"] != DBNull.Value)
                        taskAttachment.DocumentPath = resultSet["DocumentPath"].ToString();
                    if (resultSet["DocumentName"] != DBNull.Value)
                        taskAttachment.DocumentName = resultSet["DocumentName"].ToString();
                    if (resultSet["ImageNote"] != DBNull.Value)
                        taskAttachment.ImageNote = resultSet["ImageNote"].ToString();

                    if (resultSet["IsSignature"] != DBNull.Value)
                        taskAttachment.IsSignature = Convert.ToBoolean(resultSet["IsSignature"].ToString());

                    if (resultSet["IsPortal"] != DBNull.Value)
                        taskAttachment.IsPortal = Convert.ToBoolean(resultSet["IsPortal"].ToString());

                    if (resultSet["ChecklistId"] != DBNull.Value)
                        taskAttachment.ChecklistId = Convert.ToInt32(resultSet["ChecklistId"].ToString());

                    if (resultSet["SiteId"] != DBNull.Value)
                        taskAttachment.SiteId = Convert.ToInt32(resultSet["SiteId"].ToString());

                    if (resultSet["CustomerId"] != DBNull.Value)
                        taskAttachment.CustomerId = Convert.ToInt32(resultSet["CustomerId"].ToString());

                    taskAttachments.Add(taskAttachment);
                }
                connection.Close();
                resultSet.Close();
                return taskAttachments;
            }

        }

        public static List<UserTaskAttachment> GetTaskAttachmentsByChecklistId(int taskId, string dbConnection)
        {
            List<UserTaskAttachment> taskAttachments = new List<UserTaskAttachment>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var sqlCommand = new SqlCommand("GetTaskAttachmentsByChecklistId", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                sqlCommand.Parameters["@Id"].Value = taskId;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                var resultSet = sqlCommand.ExecuteReader();
                var columns = Enumerable.Range(0, resultSet.FieldCount).Select(resultSet.GetName).ToList(); 
                while (resultSet.Read())
                {
                    if (taskAttachments == null)
                    {
                        taskAttachments = new List<UserTaskAttachment>();
                    }
                    var taskAttachment = new UserTaskAttachment();

                    if (resultSet["Id"] != DBNull.Value)
                        taskAttachment.Id = Convert.ToInt32(resultSet["Id"].ToString());
                    if (resultSet["TaskId"] != DBNull.Value)
                        taskAttachment.TaskId = Convert.ToInt32(resultSet["TaskId"].ToString());
                    if (resultSet["DocumentType"] != DBNull.Value)
                        taskAttachment.DocumentType = resultSet["DocumentType"].ToString();
                    if (resultSet["CreatedDate"] != DBNull.Value)
                        taskAttachment.CreatedDate = Convert.ToDateTime(resultSet["CreatedDate"].ToString());
                    if (resultSet["UpdatedDate"] != DBNull.Value)
                        taskAttachment.UpdatedDate = Convert.ToDateTime(resultSet["UpdatedDate"].ToString());
                    if (resultSet["CreatedBy"] != DBNull.Value)
                        taskAttachment.CreatedBy = resultSet["CreatedBy"].ToString();
                    if (resultSet["UserId"] != DBNull.Value)
                        taskAttachment.UserId = Convert.ToInt32(resultSet["UserId"].ToString());
                    if (resultSet["IsDeleted"] != DBNull.Value)
                        taskAttachment.IsDeleted = (String.IsNullOrEmpty(resultSet["IsDeleted"].ToString())) ? false : Convert.ToBoolean(resultSet["IsDeleted"].ToString());
                    if (resultSet["DocumentPath"] != DBNull.Value)
                        taskAttachment.DocumentPath = resultSet["DocumentPath"].ToString();
                    if (resultSet["DocumentName"] != DBNull.Value)
                        taskAttachment.DocumentName = resultSet["DocumentName"].ToString();
                    if (resultSet["ImageNote"] != DBNull.Value)
                        taskAttachment.ImageNote = resultSet["ImageNote"].ToString();

                    if (columns.Contains( "ChecklistId"))
                    {
                        if (resultSet["ChecklistId"] != DBNull.Value)
                            taskAttachment.ChecklistId = Convert.ToInt32(resultSet["ChecklistId"].ToString());
                    }
                    if (resultSet["IsPortal"] != DBNull.Value)
                        taskAttachment.IsPortal = Convert.ToBoolean(resultSet["IsPortal"].ToString());

                    if (resultSet["SiteId"] != DBNull.Value)
                        taskAttachment.SiteId = Convert.ToInt32(resultSet["SiteId"].ToString());

                    if (resultSet["CustomerId"] != DBNull.Value)
                        taskAttachment.CustomerId = Convert.ToInt32(resultSet["CustomerId"].ToString());
                    //if (!String.IsNullOrEmpty(taskAttachment.DocumentPath))
                    //{
                    //    if (File.Exists(taskAttachment.DocumentPath))
                    //        taskAttachment.ImageByte = Convert.ToBase64String(File.ReadAllBytes(taskAttachment.DocumentPath));
                    //}

                    taskAttachments.Insert(0, taskAttachment);
                }
                connection.Close();
                resultSet.Close();
                return taskAttachments;
            }

        }

        public static bool UpdateTaskAttachmentSignature(int taskId, bool dt, string dbConnection)
        {
            //if (GroupDeviceTask.DeleteGroupDeviceTaskByTaskId(taskId, dbConnection))
            //{
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var cmd = new SqlCommand("UpdateTaskAttachmentSignature", connection);

                    cmd.Parameters.Add(new SqlParameter("@taskId", SqlDbType.Int));
                    cmd.Parameters["@taskId"].Value = taskId;
                    cmd.Parameters.Add(new SqlParameter("@signature", SqlDbType.TinyInt));
                    cmd.Parameters["@signature"].Value = dt;
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.CommandText = "UpdateTaskAttachmentSignature";

                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTaskAttachment", "UpdateTaskAttachmentSignature", ex, dbConnection);
                return false;
            }
            return true;
        }

        public static bool UpdateTaskAttachmentIsPortal(int taskId, bool dt, string dbConnection)
        {
            //if (GroupDeviceTask.DeleteGroupDeviceTaskByTaskId(taskId, dbConnection))
            //{
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var cmd = new SqlCommand("UpdateTaskAttachmentIsPortal", connection);

                    cmd.Parameters.Add(new SqlParameter("@taskId", SqlDbType.Int));
                    cmd.Parameters["@taskId"].Value = taskId;
                    cmd.Parameters.Add(new SqlParameter("@portal", SqlDbType.TinyInt));
                    cmd.Parameters["@portal"].Value = dt;
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.CommandText = "UpdateTaskAttachmentIsPortal";

                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                MIMSLog.MIMSLogSave("UserTaskAttachment", "UpdateTaskAttachmentIsPortal", ex, dbConnection);
                return false;
            }
            return true;
        }
    
    }
}
