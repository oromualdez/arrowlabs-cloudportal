﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Data.SqlClient;
using System.Data;
using Arrowlabs.Mims.Audit.Models;

namespace Arrowlabs.Business.Layer
{
    [Serializable]
    [DataContract]
    public class Notification
    {
        [DataMember]
        public int Id { get; set; }
      [DataMember]
        public bool IsUtc { get; set; }  
        [DataMember]

        public string Description { get; set; }
        [DataMember]
        public string UpdatedBy { get; set; }
        [DataMember]
        public bool IsDeleted { get; set; }
        [DataMember]
        public DateTime? CreatedDate { get; set; }
        [DataMember]
        public DateTime? UpdatedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public string LocationName { get; set; }
        [DataMember]
        public string SubLocationName { get; set; }
        [DataMember]
        public List<NotificationAttachment> Attachments { get; set; }
        [DataMember]
        public int AssigneeType { get; set; }
        [DataMember]
        public int AssigneeId { get; set; }
        [DataMember]
        public int CreatorId { get; set; }
        [DataMember]
        public string AssigneeName { get; set; }
        [DataMember]
        public string Instruction { get; set; }
        [DataMember]
        public string Latitude { get; set; }
        [DataMember]
        public string Longitude { get; set; }
        [DataMember]
        public string Topic { get; set; }
        [DataMember]
        public bool IsRead { get; set; }
        [DataMember]
        public int LookUpUserId { get; set; }
        [DataMember]
        public int AccountId { get; set; }
        [DataMember]
        public int TaskId { get; set; }
        [DataMember]
        public int LocationId { get; set; }
        [DataMember]
        public int IncidentId { get; set; }
        [DataMember]
        public string AccountName { get; set; }
        [DataMember]
        public NotificationTypes NotificationType { get; set; }
        [DataMember]
        public string Prefix { get; set; }
         [DataMember]
        public bool IsHandled{ get; set; }
         [DataMember]
         public string CameraDetails { get; set; }

        [DataMember]
         public int SubLocationId {get;set;}
        [DataMember]
        public int SiteId { get; set; }
        [DataMember]
        public int CustomerId { get; set; }

        public string CustomerIncidentId { get; set; }


        public string CustomerFullName { get; set; }
        public string ACustomerFullName { get; set; } 

        public string CustomerUName { get; set; }
        public string ACustomerUName { get; set; }

        public int CreatedByID { get; set; }

        public string VoiceUrl { get; set; }

        [DataMember]
        public string Notes { get; set; }
        public enum NotificationAssigneeType
        {
            User = 0,
            Location = 1,
            Group = 2,
            Device = 3,
            All = 4,
            AdminAll = 5
        }
        public enum NotificationTypes
        {
            None = -1,
            Alarm = 0,
            Notification = 1,
            Chat = 2,
            CNLAlarm = 3,
            VoiceNote = 4
        }


        public static string conversationPrefix = "mimschat-";
        public static string alarmPrefix = "arl-";
        public static List<string> GetNotificationAssigneeTypes()
        {
            return Enum.GetNames(typeof(NotificationAssigneeType)).ToList();
        }
        private static Notification GetNotificationFromSqlReader(SqlDataReader reader, string dbConnection)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();


            var notification = new Notification();

            if (reader["Id"] != DBNull.Value)
                notification.Id = Convert.ToInt32(reader["Id"].ToString());
            if (reader["Description"] != DBNull.Value)
                notification.Description = reader["Description"].ToString();
            if (reader["IsDeleted"] != DBNull.Value)
                notification.IsDeleted = (String.IsNullOrEmpty(reader["IsDeleted"].ToString())) ? false : Convert.ToBoolean(reader["IsDeleted"].ToString());
            if (reader["CreatedDate"] != DBNull.Value)
                notification.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            if (reader["Updateddate"] != DBNull.Value)
                notification.UpdatedDate = Convert.ToDateTime(reader["Updateddate"].ToString());
            if (reader["CreatedBy"] != DBNull.Value)
                notification.CreatedBy = reader["CreatedBy"].ToString();
            if (reader["AssigneeType"] != DBNull.Value)
                notification.AssigneeType = Convert.ToInt32(reader["AssigneeType"].ToString());
            if (reader["AssigneeId"] != DBNull.Value)
                notification.AssigneeId = Convert.ToInt32(reader["AssigneeId"].ToString());
            if (reader["AssigneeName"] != DBNull.Value)
                notification.AssigneeName = reader["AssigneeName"].ToString();
            if (columns.Contains("IsRead") && reader["IsRead"] != DBNull.Value)
                notification.IsRead = Convert.ToBoolean(reader["IsRead"].ToString());
            if (columns.Contains("LookUpUserId") && reader["LookUpUserId"] != DBNull.Value)
                notification.LookUpUserId = Convert.ToInt32(reader["LookUpUserId"].ToString());
            if (columns.Contains("NotificationType") && reader["NotificationType"] != DBNull.Value)
                notification.NotificationType = (NotificationTypes)Convert.ToInt32(reader["NotificationType"].ToString());
            if (columns.Contains("Topic") && reader["Topic"] != DBNull.Value)
                notification.Topic = reader["Topic"].ToString();
            if (columns.Contains("Prefix") && reader["Prefix"] != DBNull.Value)
                notification.Prefix = reader["Prefix"].ToString();
            if (columns.Contains("Latitude") && reader["Latitude"] != DBNull.Value)
                notification.Latitude = reader["Latitude"].ToString();
            if (columns.Contains("Longitude") && reader["Longitude"] != DBNull.Value)
                notification.Longitude = reader["Longitude"].ToString();
            if (reader["TaskId"] != DBNull.Value)
                notification.TaskId = Convert.ToInt32(reader["TaskId"].ToString());
            if (reader["LocationId"] != DBNull.Value)
                notification.LocationId = Convert.ToInt32(reader["LocationId"].ToString());

            if (columns.Contains("CameraDetails") && reader["CameraDetails"] != DBNull.Value) 
                notification.CameraDetails = reader["CameraDetails"].ToString();

            if (columns.Contains("VoiceUrl") && reader["VoiceUrl"] != DBNull.Value)
                notification.VoiceUrl = reader["VoiceUrl"].ToString();
             
            if (columns.Contains("CustomerIncidentId") && reader["CustomerIncidentId"] != DBNull.Value)
                notification.CustomerIncidentId = reader["CustomerIncidentId"].ToString();
            
            if (columns.Contains( "IncidentId"))
            {
                if (reader["IncidentId"] != DBNull.Value)
                    notification.IncidentId = Convert.ToInt32(reader["IncidentId"]);
            }
            if (reader["IsHandled"] != DBNull.Value)
                notification.IsHandled = Convert.ToBoolean(reader["IsHandled"].ToString());

            if (columns.Contains( "SiteId") && reader["SiteId"] != DBNull.Value)
                notification.SiteId = Convert.ToInt32(reader["SiteId"]);

            if (columns.Contains( "CustomerId") && reader["CustomerId"] != DBNull.Value)
                notification.CustomerId = Convert.ToInt32(reader["CustomerId"]);

            if (columns.Contains( "CreatedByID") && reader["CreatedByID"] != DBNull.Value)
                notification.CreatedByID = Convert.ToInt32(reader["CreatedByID"]);


            if (columns.Contains( "ACustomerUName") && reader["ACustomerUName"] != DBNull.Value)
            {
                notification.ACustomerUName = reader["ACustomerUName"].ToString();
                if (notification.ACustomerUName == " ..")
                {
                    notification.ACustomerUName = notification.AssigneeName;
                }
            }
            if (columns.Contains( "CustomerUName") && reader["CustomerUName"] != DBNull.Value)
            {
                notification.CustomerUName = reader["CustomerUName"].ToString();
                        
            }

            if (columns.Contains("ACustomerFullName") && reader["ACustomerFullName"] != DBNull.Value)
            {
                notification.ACustomerFullName = reader["ACustomerFullName"].ToString();
                        
            }

                        if (columns.Contains( "CustomerFullName") && reader["CustomerFullName"] != DBNull.Value)
            {
                notification.CustomerFullName = reader["CustomerFullName"].ToString();
                        
            }
             


            var splits = notification.Description.Split('-');
            if (splits.Length >= 3)
            {
                notification.Instruction = splits[2];
            }
            else
            {
                notification.Instruction = "";
            }

            var attachments = NotificationAttachment.GetNotificationAttachmentsByNotificationId(notification.Id, dbConnection);
            notification.Attachments = attachments;
            return notification;
        }

        /// <summary>
        /// It returns all notifications
        /// </summary>
        /// <param name="dbConnection"></param>
        /// <returns></returns>
        public static List<Notification> GetAllNotifications(string dbConnection)
        {
            var notifications = new List<Notification>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllNotifications", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var notification = GetNotificationFromSqlReader(reader, dbConnection);
                    notifications.Add(notification);
                }
                connection.Close();
                reader.Close();
            }
            return notifications;
        }
        public static List<Notification> GetAllNotificationsByLevel5(int siteId, string dbConnection)
        {
            var notifications = new List<Notification>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllNotificationsByLevel5", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int)).Value = siteId;
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var notification = GetNotificationFromSqlReader(reader, dbConnection);
                    notifications.Add(notification);
                }
                connection.Close();
                reader.Close();
            }
            return notifications;
        }
        public static List<Notification> GetAllNotificationsBySiteId(int siteId, string dbConnection)
        {
            var notifications = new List<Notification>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllNotificationsBySiteId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int)).Value = siteId;
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var notification = GetNotificationFromSqlReader(reader, dbConnection);
                    notifications.Add(notification);
                }
                connection.Close();
                reader.Close();
            }
            return notifications;
        }
        public static List<Notification> GetAllNotificationsByCId(int siteId, string dbConnection)
        {
            var notifications = new List<Notification>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllNotificationsByCId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int)).Value = siteId;
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var notification = GetNotificationFromSqlReader(reader, dbConnection);
                    notifications.Add(notification);
                }
                connection.Close();
                reader.Close();
            }
            return notifications;
        }
        /// <summary>
        /// It returns all notifications
        /// </summary>
        /// <param name="dbConnection"></param>
        /// <returns></returns>
        public static List<Notification> GetAllNotificationsByCreator(string name, string dbConnection)
        {
            var notifications = new List<Notification>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllNotificationsByCreator", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar)).Value = name;
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var notification = GetNotificationFromSqlReader(reader, dbConnection);
                    notifications.Add(notification);
                }
                connection.Close();
                reader.Close();
            }
            return notifications;
        }

        public static List<Notification> GetAllNotificationsByCreatorandBySiteId(string name,int siteId ,string dbConnection)
        {
            var notifications = new List<Notification>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllNotificationsByCreatorandBySiteId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar)).Value = name;
                sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int)).Value = siteId;
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var notification = GetNotificationFromSqlReader(reader, dbConnection);
                    notifications.Add(notification);
                }
                connection.Close();
                reader.Close();
            }
            return notifications;
        }

        public static List<Notification> GetAllNotificationChatByUser(int userId, string name, string dbConnection)
        {
            var notifications = new List<Notification>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllNotificationChatByUser", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int)).Value = userId;
                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar)).Value = name;
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var notification = GetNotificationFromSqlReader(reader, dbConnection);
                    if (reader["CreatorId"] != DBNull.Value)
                        notification.CreatorId = Convert.ToInt32(reader["CreatorId"].ToString());
                    notifications.Add(notification);
                }
                connection.Close();
                reader.Close();
            }
            return notifications;
        }

        public static List<Notification> GetAllNotificationChatByUserGroup(int userId, string name, string dbConnection)
        {
            var notifications = new List<Notification>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllNotificationChatByUserGroup", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int)).Value = userId;
                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar)).Value = name;
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var notification = GetNotificationFromSqlReader(reader, dbConnection);
                    if (reader["CreatorId"] != DBNull.Value)
                        notification.CreatorId = Convert.ToInt32(reader["CreatorId"].ToString());
                    notifications.Add(notification);
                }
                connection.Close();
                reader.Close();
            }
            return notifications;
        }

        public static List<Notification> GetAllNotificationChatByGroup(int userId, string name, string dbConnection)
        {
            var notifications = new List<Notification>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllNotificationChatByGroup", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int)).Value = userId;
                sqlCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar)).Value = name;
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var notification = GetNotificationFromSqlReader(reader, dbConnection);
                    if (reader["CreatorId"] != DBNull.Value)
                        notification.CreatorId = Convert.ToInt32(reader["CreatorId"].ToString());
                    notifications.Add(notification);
                }
                connection.Close();
                reader.Close();
            }
            return notifications;
        }

        public static List<Notification> GetAllNotificationsByTypeAndProcedureName(int typeId,
           string storedProcedureName, int userId, DateTime fromDateTime,
           DateTime toDateTime, string dbConnection)
        {
            var notifications = new List<Notification>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand(storedProcedureName, connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlCommand.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int)).Value = userId;
                sqlCommand.Parameters.Add(new SqlParameter("@AssigneeType", SqlDbType.Int)).Value = typeId;
                sqlCommand.Parameters.Add(new SqlParameter("@FromDateTime", SqlDbType.DateTime)).Value = fromDateTime;
                sqlCommand.Parameters.Add(new SqlParameter("@ToDateTime", SqlDbType.DateTime)).Value = toDateTime;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var notification = GetNotificationFromSqlReader(reader, dbConnection);
                    notifications.Add(notification);
                }
                connection.Close();
                reader.Close();
            }
            return notifications;
        }



        public static List<Notification> GetAllNotificationsByUserId(int userId,
           DateTime fromDateTime, DateTime toDateTime, string dbConnection)
        {
            var notifications = new List<Notification>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllNotificationsByUserId", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlCommand.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int)).Value = userId;
                sqlCommand.Parameters.Add(new SqlParameter("@UserAssigneeType", SqlDbType.Int)).Value = (int)NotificationAssigneeType.User;
                sqlCommand.Parameters.Add(new SqlParameter("@GroupAssigneeType", SqlDbType.Int)).Value = (int)NotificationAssigneeType.Group;
                sqlCommand.Parameters.Add(new SqlParameter("@ManagerAssigneeType", SqlDbType.Int)).Value = (int)NotificationAssigneeType.All;
                sqlCommand.Parameters.Add(new SqlParameter("@AdminAssigneeType", SqlDbType.Int)).Value = (int)NotificationAssigneeType.AdminAll;
                sqlCommand.Parameters.Add(new SqlParameter("@NotificationType", SqlDbType.Int)).Value = (int)Notification.NotificationTypes.Notification;
                sqlCommand.Parameters.Add(new SqlParameter("@FromDateTime", SqlDbType.DateTime)).Value = fromDateTime;
                sqlCommand.Parameters.Add(new SqlParameter("@ToDateTime", SqlDbType.DateTime)).Value = toDateTime;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var notification = GetNotificationFromSqlReader(reader, dbConnection);
                    notifications.Add(notification);
                }
                connection.Close();
                reader.Close();
            }
            return notifications;
        }

        public static List<Notification> GetAllAlarmsByUserId(int userId,
           DateTime fromDateTime, DateTime toDateTime, string dbConnection)
        {
            var notifications = new List<Notification>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllAlarmsByUserId", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlCommand.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int)).Value = userId;
                sqlCommand.Parameters.Add(new SqlParameter("@UserAssigneeType", SqlDbType.Int)).Value = (int)NotificationAssigneeType.User;
                sqlCommand.Parameters.Add(new SqlParameter("@GroupAssigneeType", SqlDbType.Int)).Value = (int)NotificationAssigneeType.Group;
                sqlCommand.Parameters.Add(new SqlParameter("@ManagerAssigneeType", SqlDbType.Int)).Value = (int)NotificationAssigneeType.All;
                sqlCommand.Parameters.Add(new SqlParameter("@AdminAssigneeType", SqlDbType.Int)).Value = (int)NotificationAssigneeType.AdminAll;
                sqlCommand.Parameters.Add(new SqlParameter("@NotificationType", SqlDbType.Int)).Value = (int)NotificationTypes.Alarm;
                sqlCommand.Parameters.Add(new SqlParameter("@FromDateTime", SqlDbType.DateTime)).Value = fromDateTime;
                sqlCommand.Parameters.Add(new SqlParameter("@ToDateTime", SqlDbType.DateTime)).Value = toDateTime;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var notification = GetNotificationFromSqlReader(reader, dbConnection);
                    notifications.Add(notification);
                }
                connection.Close();
                reader.Close();
            }
            return notifications;
        }


        public static List<Notification> GetAllUserNotificationsByUserId(int userId,
            DateTime fromDateTime, DateTime toDateTime, string dbConnection)
        {
            return GetAllNotificationsByTypeAndProcedureName((int)NotificationAssigneeType.User,
            "GetAllUserNotificationsByUserId", userId, fromDateTime,
            toDateTime, dbConnection);
        }

        public static List<Notification> GetAllGroupNotificationsByUserId(int userId,
            DateTime fromDateTime, DateTime toDateTime, string dbConnection)
        {
            return GetAllNotificationsByTypeAndProcedureName((int)NotificationAssigneeType.Group,
            "GetAllGroupNotificationsByUserId", userId, fromDateTime,
            toDateTime, dbConnection);
        }

        public static List<Notification> GetAllManagerNotificationsByUserId(int userId,
            DateTime fromDateTime, DateTime toDateTime, string dbConnection)
        {
            return GetAllNotificationsByTypeAndProcedureName((int)NotificationAssigneeType.All,
            "GetAllManagerNotificationsByUserId", userId, fromDateTime,
            toDateTime, dbConnection);
        }
        public static List<Notification> GetAllAdminLevelNotifications(int userId,
            DateTime fromDateTime, DateTime toDateTime, string dbConnection)
        {
            return GetAllNotificationsByTypeAndProcedureName((int)NotificationAssigneeType.AdminAll,
            "GetAllUserNotificationsByUserId", userId, fromDateTime,
            toDateTime, dbConnection);
        }


        public static List<Notification> GetAllLocationNotificationsByLocationId(int locationId,
            DateTime fromDateTime, DateTime toDateTime, string dbConnection)
        {
            var notifications = new List<Notification>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllLocationNotificationsByLocationId", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlCommand.Parameters.Add(new SqlParameter("@LocationId", SqlDbType.Int)).Value = locationId;
                sqlCommand.Parameters.Add(new SqlParameter("@AssigneeType", SqlDbType.Int)).Value = (int)NotificationAssigneeType.Location;
                sqlCommand.Parameters.Add(new SqlParameter("@FromDateTime", SqlDbType.DateTime)).Value = fromDateTime;
                sqlCommand.Parameters.Add(new SqlParameter("@ToDateTime", SqlDbType.DateTime)).Value = toDateTime;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var notification = GetNotificationFromSqlReader(reader, dbConnection);
                    notifications.Add(notification);
                }
                connection.Close();
                reader.Close();
            }
            return notifications;
        }

        public static List<Notification> GetAllDeviceNotificationsByAssigneeName(string assigneeName,
    DateTime fromDateTime, DateTime toDateTime, string dbConnection)
        {
            var notifications = new List<Notification>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllDeviceNotificationsByAssigneeName", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlCommand.Parameters.Add(new SqlParameter("@AssigneeName", SqlDbType.NVarChar)).Value = assigneeName;
                sqlCommand.Parameters.Add(new SqlParameter("@AssigneeType", SqlDbType.Int)).Value = (int)NotificationAssigneeType.Device;
                sqlCommand.Parameters.Add(new SqlParameter("@FromDateTime", SqlDbType.DateTime)).Value = fromDateTime;
                sqlCommand.Parameters.Add(new SqlParameter("@ToDateTime", SqlDbType.DateTime)).Value = toDateTime;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var notification = GetNotificationFromSqlReader(reader, dbConnection);
                    notifications.Add(notification);
                }
                connection.Close();
                reader.Close();
            }
            return notifications;
        }
        /// <summary>
        /// It returns all notifications for specific location
        /// </summary>
        /// <param name="locationId"></param>
        /// <param name="dbConnection"></param>
        /// <returns></returns>
        public static List<Notification> GetAllNotificationsByMainLocationIdAndSubLocationId(int mainLocationId, int subLocationId,
            string dbConnection)
        {

            var allNotifications = GetAllNotifications(dbConnection);
            var filteredNotifications = new List<Notification>();

            foreach (var notification in allNotifications)
            {
                if (notification.LocationId == mainLocationId && notification.SubLocationId == subLocationId)
                {
                    filteredNotifications.Add(notification);
                }
                else if (notification.LocationId == mainLocationId && notification.SubLocationId == 0)
                {
                    filteredNotifications.Add(notification);
                }
                else if (notification.LocationId == 0 && notification.SubLocationId == 0)
                {
                    filteredNotifications.Add(notification);
                }
            }

            return filteredNotifications;
        }

        /// <summary>
        /// It inserts or updates notification.
        /// </summary>
        /// <param name="notification"></param>
        /// <param name="dbConnection"></param>
        /// <returns></returns>
        public static int InsertorUpdateNotification(Notification notification, string dbConnection)
        {
            var notificationId = 0;
            if (notification != null)
            {
                int id = notification.Id;
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertorUpdateNotification", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = notification.Id;

                    cmd.Parameters.Add(new SqlParameter("@Description", SqlDbType.NVarChar));
                    cmd.Parameters["@Description"].Value = notification.Description;

                    cmd.Parameters.Add(new SqlParameter("@AssigneeType", SqlDbType.Int));
                    cmd.Parameters["@AssigneeType"].Value = notification.AssigneeType;

                    cmd.Parameters.Add(new SqlParameter("@AssigneeId", SqlDbType.Int));
                    cmd.Parameters["@AssigneeId"].Value = notification.AssigneeId;

                    cmd.Parameters.Add(new SqlParameter("@AssigneeName", SqlDbType.NVarChar));
                    cmd.Parameters["@AssigneeName"].Value = notification.AssigneeName;

                    cmd.Parameters.Add(new SqlParameter("@IsDeleted", SqlDbType.Bit));
                    cmd.Parameters["@IsDeleted"].Value = notification.IsDeleted;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = notification.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = notification.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = notification.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = notification.SiteId;

                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();
                    notificationId = (int)returnParameter.Value;
                    if (id == 0)
                        id = (int)returnParameter.Value;
                }
            }
            return notificationId;
        }

        public static int InsertorUpdateNotification(Notification notification, string dbConnection,
            string auditDbConnection = "", bool isAuditEnabled = true)
        {
            var notificationId = 0;
            if (notification != null)
            {
                int id = notification.Id;
                var action = (id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;


                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertorUpdateNotification", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = notification.Id;

                    cmd.Parameters.Add(new SqlParameter("@Description", SqlDbType.NVarChar));
                    cmd.Parameters["@Description"].Value = notification.Description;

                    cmd.Parameters.Add(new SqlParameter("@AssigneeType", SqlDbType.Int));
                    cmd.Parameters["@AssigneeType"].Value = notification.AssigneeType;

                    cmd.Parameters.Add(new SqlParameter("@AssigneeId", SqlDbType.Int));
                    cmd.Parameters["@AssigneeId"].Value = notification.AssigneeId;

                    cmd.Parameters.Add(new SqlParameter("@TaskId", SqlDbType.Int));
                    cmd.Parameters["@TaskId"].Value = notification.TaskId;

                    cmd.Parameters.Add(new SqlParameter("@LocationId", SqlDbType.Int));
                    cmd.Parameters["@LocationId"].Value = notification.LocationId;

                    cmd.Parameters.Add(new SqlParameter("@AssigneeName", SqlDbType.NVarChar));
                    cmd.Parameters["@AssigneeName"].Value = notification.AssigneeName;

                    cmd.Parameters.Add(new SqlParameter("@IsDeleted", SqlDbType.Bit));
                    cmd.Parameters["@IsDeleted"].Value = notification.IsDeleted;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = notification.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = notification.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = notification.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@NotificationType", SqlDbType.Int));
                    cmd.Parameters["@NotificationType"].Value = notification.NotificationType;

                    cmd.Parameters.Add(new SqlParameter("@Topic", SqlDbType.NVarChar));
                    cmd.Parameters["@Topic"].Value = notification.Topic;

                    cmd.Parameters.Add(new SqlParameter("@Latitude", SqlDbType.NVarChar));
                    cmd.Parameters["@Latitude"].Value = notification.Latitude;

                    cmd.Parameters.Add(new SqlParameter("@Prefix", SqlDbType.NVarChar));
                    cmd.Parameters["@Prefix"].Value = notification.Prefix;

                    cmd.Parameters.Add(new SqlParameter("@Longitude", SqlDbType.NVarChar));
                    cmd.Parameters["@Longitude"].Value = notification.Longitude;

                    cmd.Parameters.Add(new SqlParameter("@IncidentId", SqlDbType.Int));
                    cmd.Parameters["@IncidentId"].Value = notification.IncidentId;

                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = notification.SiteId;

                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                                        cmd.Parameters["@CustomerId"].Value = notification.CustomerId;
                    

                    cmd.Parameters.Add(new SqlParameter("@CameraDetails", SqlDbType.NVarChar));
                    cmd.Parameters["@CameraDetails"].Value = notification.CameraDetails;


                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();
                    notificationId = (int)returnParameter.Value;
                    if (id == 0)
                        id = (int)returnParameter.Value;
                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            notification.Id = id;

                            var audit = new NotificationAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(notification.AccountName, action, notification.UpdatedBy);
                            Reflection.CopyProperties(notification, audit);
                            NotificationAudit.InsertNotificationAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertorUpdateNotification", ex, dbConnection);
                    }


                    if (notification.NotificationType == NotificationTypes.Chat
                        && notification.AssigneeType == (int)NotificationAssigneeType.User)
                    {
                        var notificationLookUpReceiver = new NotificationChatLookUp();

                        notificationLookUpReceiver.NotificationId = notificationId;
                        notificationLookUpReceiver.UserId = notification.AssigneeId;
                        notificationLookUpReceiver.IsDeleted = false;
                        notificationLookUpReceiver.IsRead = false;
                        notificationLookUpReceiver.CreatedDate = DateTime.Now;
                        notificationLookUpReceiver.UpdatedDate = DateTime.Now;

                        var notificationLookUpSender = new NotificationChatLookUp();

                        notificationLookUpSender.NotificationId = notificationId;
                        notificationLookUpSender.UserId = notification.CreatorId;
                        notificationLookUpSender.IsDeleted = false;
                        notificationLookUpSender.IsRead = true;
                        notificationLookUpSender.CreatedDate = DateTime.Now;
                        notificationLookUpSender.UpdatedDate = DateTime.Now;

                        NotificationChatLookUp.InsertorUpdateNotificationChatLookUp(notificationLookUpReceiver, dbConnection);
                        NotificationChatLookUp.InsertorUpdateNotificationChatLookUp(notificationLookUpSender, dbConnection);

                    }
                    else if (notification.NotificationType == NotificationTypes.Chat
                        && notification.AssigneeType == (int)NotificationAssigneeType.Group)
                    {
                        var groupMembers = Users.GetAllUserByGroupId(notification.AssigneeId, dbConnection);
                        foreach (var member in groupMembers)
                        {
                            if (member.ID != notification.CreatorId)
                            {
                                var notificationLookUpReceiver = new NotificationChatLookUp();

                                notificationLookUpReceiver.NotificationId = notificationId;
                                notificationLookUpReceiver.UserId = member.ID;
                                notificationLookUpReceiver.IsDeleted = false;
                                notificationLookUpReceiver.IsRead = false;
                                notificationLookUpReceiver.CreatedDate = DateTime.Now;
                                notificationLookUpReceiver.UpdatedDate = DateTime.Now;
                                notificationLookUpReceiver.GroupId = notification.AssigneeId;
                                NotificationChatLookUp.InsertorUpdateNotificationChatLookUp(notificationLookUpReceiver, dbConnection);
                            }
                        }
                        var groupInfo = Group.GetGroupById(notification.AssigneeId, dbConnection);
                        if (groupInfo != null)
                        {
                            var gUser = Users.GetUserByName(groupInfo.CreatedBy, dbConnection);
                            if (gUser != null)
                            {
                                if (gUser.ID != notification.CreatorId)
                                {
                                    var notificationLookUpReceiver = new NotificationChatLookUp();

                                    notificationLookUpReceiver.NotificationId = notificationId;
                                    notificationLookUpReceiver.UserId = gUser.ID;
                                    notificationLookUpReceiver.IsDeleted = false;
                                    notificationLookUpReceiver.IsRead = false;
                                    notificationLookUpReceiver.CreatedDate = DateTime.Now;
                                    notificationLookUpReceiver.UpdatedDate = DateTime.Now;
                                    notificationLookUpReceiver.GroupId = notification.AssigneeId;
                                    NotificationChatLookUp.InsertorUpdateNotificationChatLookUp(notificationLookUpReceiver, dbConnection);
                                }
                            }
                        }


                        var notificationLookUpSender = new NotificationChatLookUp();

                        notificationLookUpSender.NotificationId = notificationId;
                        notificationLookUpSender.UserId = notification.CreatorId;
                        notificationLookUpSender.IsDeleted = false;
                        notificationLookUpSender.IsRead = true;
                        notificationLookUpSender.CreatedDate = DateTime.Now;
                        notificationLookUpSender.UpdatedDate = DateTime.Now;

                        NotificationChatLookUp.InsertorUpdateNotificationChatLookUp(notificationLookUpSender, dbConnection);
                    }
                }
            }
            return notificationId;
        }

        public static bool MarkNotificationAsRead(string createdBy, int userId, int notificationType, string dbConnection)
        {
            try
            {

                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("UpdateNotificationsAsRead", connection);

                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                    cmd.Parameters["@UserId"].Value = userId;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = createdBy;

                    cmd.Parameters.Add(new SqlParameter("@NotificationType", SqlDbType.Int));
                    cmd.Parameters["@NotificationType"].Value = notificationType;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool MarkNotificationAsReadByGroup(int gid, int userId, string dbConnection)
        {
            try
            {

                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("UpdateNotificationsAsReadByGroup", connection);

                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                    cmd.Parameters["@UserId"].Value = userId;

                    cmd.Parameters.Add(new SqlParameter("@GroupId", SqlDbType.Int));
                    cmd.Parameters["@GroupId"].Value = gid;
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool MarkChatAsRead(string createdBy, int userId, string prefix, string dbConnection)
        {
            try
            {

                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("UpdateChatAsRead", connection);

                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                    cmd.Parameters["@UserId"].Value = userId;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = createdBy;

                    cmd.Parameters.Add(new SqlParameter("@NotificationPrefix", SqlDbType.NVarChar));
                    cmd.Parameters["@NotificationPrefix"].Value = prefix;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool MarkAlarmsAsReadByUserId(int userId, int notificationType, string dbConnection)
        {
            try
            {

                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("MarkAlarmsAsReadByUserId", connection);

                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                    cmd.Parameters["@UserId"].Value = userId;

                    cmd.Parameters.Add(new SqlParameter("@NotificationType", SqlDbType.Int));
                    cmd.Parameters["@NotificationType"].Value = notificationType;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();
                }
                return true;
            }
            catch (Exception)
            {
                return false;

            }
        }
        public static bool UpdateNotificationStatusByIncidentId(int incidentId, bool handled, string dbConnection)
        {

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("UpdateNotificationStatusByIncidentId", connection);

                cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                cmd.Parameters["@Id"].Value = incidentId;

                cmd.Parameters.Add(new SqlParameter("@Handled", SqlDbType.Bit));
                cmd.Parameters["@Handled"].Value = handled;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.ExecuteNonQuery();
            }
            return true;
        }
        public static bool UpdateNotificationStatusPrefixByIncidentId(int incidentId, bool handled, string prefix, string dbConnection)
        {

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("UpdateNotificationStatusPrefixByIncidentId", connection);

                cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                cmd.Parameters["@Id"].Value = incidentId;

                cmd.Parameters.Add(new SqlParameter("@Handled", SqlDbType.Bit));
                cmd.Parameters["@Handled"].Value = handled;

                cmd.Parameters.Add(new SqlParameter("@Prefix", SqlDbType.NVarChar));
                cmd.Parameters["@Prefix"].Value = prefix;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.ExecuteNonQuery();
            }
            return true;
        }
        public static bool UpdateNotificationStatus(int notificationId, bool handled, string dbConnection)
        {

            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();
                var cmd = new SqlCommand("UpdateNotificationStatus", connection);

                cmd.Parameters.Add(new SqlParameter("@NotificationId", SqlDbType.Int));
                cmd.Parameters["@NotificationId"].Value = notificationId;

                cmd.Parameters.Add(new SqlParameter("@Handled", SqlDbType.Bit));
                cmd.Parameters["@Handled"].Value = handled;

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.ExecuteNonQuery();
            }
            return true;
        }

        public static Notification GetNotificationsById(int id, string dbConnection)
        {
            Notification notifications = null;
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllNotificationsById", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@id", SqlDbType.Int)).Value = id;
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    notifications = GetNotificationFromSqlReader(reader, dbConnection);

                }
                connection.Close();
                reader.Close();
            }
            return notifications;
        }
        public static bool DeleteNotificationByIncidentId(int taskId, string dbConnection)
        {
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var cmd = new SqlCommand("DeleteNotificationByIncidentId", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = taskId;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.CommandText = "DeleteNotificationByIncidentId";

                    cmd.ExecuteNonQuery();
                }
                return true;
            }
            catch (Exception ex)
            {

            }

            return false;
        }
        public static bool DeleteNotificationChatHistory(DataTable notificationIds, int userId, string dbConnection)
        {
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("DeleteNotificationChatHistory", connection);
                    cmd.CommandType = CommandType.StoredProcedure;

                    var param = cmd.Parameters.Add(new SqlParameter("@ToBeDeletedLookUpIds", SqlDbType.Structured));
                    cmd.Parameters["@ToBeDeletedLookUpIds"].TypeName = "dbo.NotificationChatLookUpIdList";
                    cmd.Parameters["@ToBeDeletedLookUpIds"].Value = notificationIds;

                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                    cmd.Parameters["@UserId"].Value = userId;


                    cmd.ExecuteNonQuery();

                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }

        }
         
        public static List<Notification> GetAllAlarmNotificationsByUserId(int userId, string dbConnection)
        {
            var notifications = new List<Notification>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllAlarmNotificationsByUserId", connection);

                sqlCommand.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int)).Value = userId;
                sqlCommand.Parameters.Add(new SqlParameter("@UserAssigneeType", SqlDbType.Int)).Value = (int)NotificationAssigneeType.User;
                sqlCommand.Parameters.Add(new SqlParameter("@GroupAssigneeType", SqlDbType.Int)).Value = (int)NotificationAssigneeType.Group;
                sqlCommand.Parameters.Add(new SqlParameter("@AllAssigneeType", SqlDbType.Int)).Value = (int)NotificationAssigneeType.All;
                sqlCommand.Parameters.Add(new SqlParameter("@AdminAllAssigneeType", SqlDbType.Int)).Value = (int)NotificationAssigneeType.AdminAll;

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    notifications.Add(GetNotificationFromSqlReader(reader, dbConnection));
                }
                connection.Close();
                reader.Close();
            }
            return notifications;
        }

        public static List<Notification> GetAllNotificationsByIncidentIdAndByHandled(int Id,bool handled ,string dbConnection)
        {
            var notifications = new List<Notification>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllNotificationsByIncidentIdAndByHandled", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int)).Value = Id;
                sqlCommand.Parameters.Add(new SqlParameter("@Handled", SqlDbType.Bit)).Value = handled;
                sqlCommand.CommandType = CommandType.StoredProcedure;


                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var notification = GetNotificationFromSqlReader(reader, dbConnection);
                    notifications.Add(notification);
                }
                connection.Close();
                reader.Close();
            }
            return notifications;
        }
        public static List<Notification> GetAllNotificationsByIncidentId(int userId, string dbConnection)
        {
            var notifications = new List<Notification>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllNotificationsByIncidentId", connection);
                sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int)).Value = userId;
                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var notification = GetNotificationFromSqlReader(reader, dbConnection);
                    notifications.Add(notification);
                }
                connection.Close();
                reader.Close();
            }
            return notifications;
        }

        public static List<Notification> GetAllNotificationsByLocationId(int locationId, string dbConnection)
        {
            var notifications = new List<Notification>();
            using (var connection = new SqlConnection(dbConnection))
            {
                connection.Open();

                var sqlCommand = new SqlCommand("GetAllNotificationsByLocationId", connection);
                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlCommand.Parameters.Add(new SqlParameter("@LocationId", SqlDbType.Int)).Value = locationId;
             

                var reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    var notification = GetNotificationFromSqlReader(reader, dbConnection);
                    notifications.Add(notification);
                }
                connection.Close();
                reader.Close();
            }
            return notifications;
        }
    }
}