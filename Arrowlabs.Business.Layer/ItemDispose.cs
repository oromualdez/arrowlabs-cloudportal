﻿using Arrowlabs.Mims.Audit.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Business.Layer
{
   public class ItemDispose
    {
        public int Id { get; set; }
        public DateTime DisposedDate { get; set; }
        public string DisposedTo { get; set; }
        public string RecipientName { get; set; }
        public string RecipientCompany { get; set; }
        public string RecipientLocation { get; set; }
        public string RecipientContactNo { get; set; }
        public string RecipientIdNo { get; set; }
        public string Destroyed { get; set; }
        public string DisposedAfter { get; set; }
        public string DisposedBy { get; set; }
        public string Witness { get; set; }
        public string ImagePath { get; set; }
        public int ItemFoundId { get; set; }
        public string ItemReference { get; set; }
        public int SiteId { get; set; }
        public int CustomerId { get; set; } 
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

        public string CustomerUName { get; set; }

        private static ItemDispose GetItemDisposeFromSqlReader(SqlDataReader reader)
        {
            var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
            var itemDispose = new ItemDispose();
            if (reader["Id"] != DBNull.Value)
                itemDispose.Id = Convert.ToInt32(reader["Id"].ToString());
            if (reader["DisposedDate"] != DBNull.Value)
                itemDispose.DisposedDate = Convert.ToDateTime(reader["DisposedDate"].ToString());
            if (reader["DisposedTo"] != DBNull.Value)
                itemDispose.DisposedTo = reader["DisposedTo"].ToString();
            if (reader["RecipientName"] != DBNull.Value)
                itemDispose.RecipientName = reader["RecipientName"].ToString();
            if (reader["RecipientCompany"] != DBNull.Value)
                itemDispose.RecipientCompany = reader["RecipientCompany"].ToString();
            if (reader["RecipientLocation"] != DBNull.Value)
                itemDispose.RecipientLocation = reader["RecipientLocation"].ToString();
            if (reader["RecipientContactNo"] != DBNull.Value)
                itemDispose.RecipientContactNo = reader["RecipientContactNo"].ToString();
            if (reader["RecipientIdNo"] != DBNull.Value)
                itemDispose.RecipientIdNo = reader["RecipientIdNo"].ToString();
            if (reader["Destroyed"] != DBNull.Value)
                itemDispose.Destroyed = reader["Destroyed"].ToString();
            if (reader["DisposedAfter"] != DBNull.Value)
                itemDispose.DisposedAfter = reader["DisposedAfter"].ToString();
            if (reader["DisposedBy"] != DBNull.Value)
                itemDispose.DisposedBy = reader["DisposedBy"].ToString();
            if (reader["Witness"] != DBNull.Value)
                itemDispose.Witness = reader["Witness"].ToString();
            if (reader["ImagePath"] != DBNull.Value)
                itemDispose.ImagePath = reader["ImagePath"].ToString();
            if (reader["ItemFoundId"] != DBNull.Value)
                itemDispose.ItemFoundId = Convert.ToInt32(reader["ItemFoundId"].ToString());
            if (reader["SiteId"] != DBNull.Value)
                itemDispose.SiteId = Convert.ToInt32(reader["SiteId"].ToString());
            if (reader["CustomerId"] != DBNull.Value)
                itemDispose.CustomerId = Convert.ToInt32(reader["CustomerId"].ToString());


            if (reader["ItemReference"] != DBNull.Value)
                itemDispose.ItemReference = reader["ItemReference"].ToString();
            if (reader["CreatedDate"] != DBNull.Value)
                itemDispose.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
            if (reader["CreatedBy"] != DBNull.Value)
                itemDispose.CreatedBy = reader["CreatedBy"].ToString();
            if (reader["UpdatedDate"] != DBNull.Value)
                itemDispose.UpdatedDate = Convert.ToDateTime(reader["UpdatedDate"].ToString());
            if (reader["UpdatedBy"] != DBNull.Value)
                itemDispose.UpdatedBy = reader["UpdatedBy"].ToString();

            if (columns.Contains( "CustomerUName"))
                itemDispose.CustomerUName = reader["CustomerUName"].ToString();

            return itemDispose;
        }
        public static ItemDispose GetItemDisposeByReference(string reference, string dbConnection)
        {
            ItemDispose itemDispose = null;
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("GetItemDisposeByReference", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@Reference", SqlDbType.NVarChar));
                    sqlCommand.Parameters["@Reference"].Value = reference;
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        itemDispose = GetItemDisposeFromSqlReader(reader);

                    }
                }
            }
            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("ItemFound", "GetItemDisposeByReference", exp, dbConnection);
            }
            return itemDispose;
        }
        public static List<ItemDispose> GetAllItemDispose(string dbConnection)
        {
            var itemDisposes = new List<ItemDispose>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("GetAllItemDispose", connection);

                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        var itemOwner = GetItemDisposeFromSqlReader(reader);
                        itemDisposes.Add(itemOwner);
                    }
                    connection.Close();
                    reader.Close();
                }

            }
            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("ItemFound", "GetAllItemDispose", exp, dbConnection);

            }
            return itemDisposes;
        }

        public static List<ItemDispose> GetItemDisposeBySiteId(int siteId, string dbConnection)
        {
            var itemDisposes = new List<ItemDispose>();
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("GetItemDisposeBySiteId", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    sqlCommand.Parameters["@SiteId"].Value = siteId;
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        var itemOwner = GetItemDisposeFromSqlReader(reader);
                        itemDisposes.Add(itemOwner);
                    }
                    connection.Close();
                    reader.Close();
                }

            }
            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("ItemFound", "GetItemOwnerBySiteId", exp, dbConnection);

            }
            return itemDisposes;
        }
        public static ItemDispose GetItemDisposeById(int id, string dbConnection)
        {
            ItemDispose itemOwner = null;
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();

                    var sqlCommand = new SqlCommand("GetItemDisposeById", connection);
                    sqlCommand.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    sqlCommand.Parameters["@Id"].Value = id;
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        itemOwner = GetItemDisposeFromSqlReader(reader);

                    }
                }
            }
            catch (Exception exp)
            {
                MIMSLog.MIMSLogSave("ItemFound", "GetItemOwnerById", exp, dbConnection);
            }
            return itemOwner;
        }

        public static int InsertorUpdateItemDispose(ItemDispose itemOwner, string dbConnection,
          string auditDbConnection = "", bool isAuditEnabled = true)
        {
            int id = 0;
            if (itemOwner != null)
            {
                id = itemOwner.Id;
                var action = (id == 0) ? Arrowlabs.Mims.Audit.Enums.Action.Create : Arrowlabs.Mims.Audit.Enums.Action.AfterUpdate;

                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertorUpdateItemDispose", connection);

                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = itemOwner.Id;
                    cmd.Parameters.Add(new SqlParameter("@DisposedDate", SqlDbType.DateTime));
                    cmd.Parameters["@DisposedDate"].Value = itemOwner.DisposedDate;
                    cmd.Parameters.Add(new SqlParameter("@DisposedTo", SqlDbType.NVarChar));
                    cmd.Parameters["@DisposedTo"].Value = itemOwner.DisposedTo;
                    cmd.Parameters.Add(new SqlParameter("@RecipientName", SqlDbType.NVarChar));
                    cmd.Parameters["@RecipientName"].Value = itemOwner.RecipientName;
                    cmd.Parameters.Add(new SqlParameter("@RecipientCompany", SqlDbType.NVarChar));
                    cmd.Parameters["@RecipientCompany"].Value = itemOwner.RecipientCompany;
                    cmd.Parameters.Add(new SqlParameter("@RecipientLocation", SqlDbType.NVarChar));
                    cmd.Parameters["@RecipientLocation"].Value = itemOwner.RecipientLocation;
                    cmd.Parameters.Add(new SqlParameter("@RecipientContactNo", SqlDbType.NVarChar));
                    cmd.Parameters["@RecipientContactNo"].Value = itemOwner.RecipientContactNo;
                    cmd.Parameters.Add(new SqlParameter("@RecipientIdNo", SqlDbType.NVarChar));
                    cmd.Parameters["@RecipientIdNo"].Value = itemOwner.RecipientIdNo;
                    cmd.Parameters.Add(new SqlParameter("@Destroyed", SqlDbType.NVarChar));
                    cmd.Parameters["@Destroyed"].Value = itemOwner.Destroyed;
                    cmd.Parameters.Add(new SqlParameter("@DisposedAfter", SqlDbType.NVarChar));
                    cmd.Parameters["@DisposedAfter"].Value = itemOwner.DisposedAfter;
                    cmd.Parameters.Add(new SqlParameter("@DisposedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@DisposedBy"].Value = itemOwner.DisposedBy;
                    cmd.Parameters.Add(new SqlParameter("@Witness", SqlDbType.NVarChar));
                    cmd.Parameters["@Witness"].Value = itemOwner.Witness;
                    cmd.Parameters.Add(new SqlParameter("@ImagePath", SqlDbType.NVarChar));
                    cmd.Parameters["@ImagePath"].Value = itemOwner.ImagePath;
                    cmd.Parameters.Add(new SqlParameter("@ItemFoundId", SqlDbType.Int));
                    cmd.Parameters["@ItemFoundId"].Value = itemOwner.ItemFoundId;
                    cmd.Parameters.Add(new SqlParameter("@ItemReference", SqlDbType.NVarChar));
                    cmd.Parameters["@ItemReference"].Value = itemOwner.ItemReference;
                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = itemOwner.CreatedDate;
                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = itemOwner.UpdatedDate;
                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = itemOwner.CreatedBy;
                    cmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@UpdatedBy"].Value = itemOwner.UpdatedBy;
                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = itemOwner.SiteId;
                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = itemOwner.CustomerId;
                    
                    var returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                    if (id == 0)
                        id = (int)returnParameter.Value;

                    try
                    {
                        if (isAuditEnabled && !string.IsNullOrEmpty(auditDbConnection))
                        {
                            itemOwner.Id = id;
                            var audit = new ItemDisposeAudit();
                            audit.BaseAudit = BaseAudit.FillBaseAudit(AppConstants.Account, action, itemOwner.UpdatedBy);
                            Reflection.CopyProperties(itemOwner, audit);
                            ItemDisposeAudit.InsertorUpdateItemOwnerAudit(audit, auditDbConnection);
                        }
                    }
                    catch (Exception ex)
                    {
                        MIMSLog.MIMSLogSave("Business Layer", "InsertorUpdateItemFound", ex, dbConnection);
                    }
                }
            }
            return id;
        }
    }
}
