﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrowlabs.Mims.Audit.Models
{
    public class ItemFoundRemarksAudit
    {
        public BaseAudit BaseAudit { get; set; }
        public int Id { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public int CustomerId { get; set; }
        public string Remarks { get; set; }
        public int SiteId { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public string CustomerUName { get; set; }
        public string Reference { get; set; }
        public int ItemFoundId { get; set; }
        public int ItemInquiryId { get; set; }
        public int ItemDisposeId { get; set; }
        public int ItemLostId { get; set; }
        public int ItemOwnerId { get; set; }
        public static int InsertItemFoundRemarksAudit(ItemFoundRemarksAudit taskRemarks, string auditDbConnection)
        {
            var baseAuditId = BaseAudit.InsertBaseAudit(taskRemarks.BaseAudit, auditDbConnection);

            int id = 0;

            if (taskRemarks != null)
            {
                using (var connection = new SqlConnection(auditDbConnection))
                {
                    connection.Open();
                    var cmd = new SqlCommand("InsertItemFoundRemarksAudit", connection);
                    cmd.Parameters.Add("@BaseAuditId", SqlDbType.Int).Value = baseAuditId;
                    cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int));
                    cmd.Parameters["@Id"].Value = taskRemarks.Id;

                    cmd.Parameters.Add(new SqlParameter("@Remarks", SqlDbType.NVarChar));
                    cmd.Parameters["@Remarks"].Value = taskRemarks.Remarks;

                    cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@CreatedDate"].Value = taskRemarks.CreatedDate;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedDate", SqlDbType.DateTime));
                    cmd.Parameters["@UpdatedDate"].Value = taskRemarks.UpdatedDate;

                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@CreatedBy"].Value = taskRemarks.CreatedBy;

                    cmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                    cmd.Parameters["@UpdatedBy"].Value = taskRemarks.UpdatedBy;

                    cmd.Parameters.Add(new SqlParameter("@Reference", SqlDbType.NVarChar));
                    cmd.Parameters["@Reference"].Value = taskRemarks.Reference;
                     
                    cmd.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    cmd.Parameters["@CustomerId"].Value = taskRemarks.CustomerId;
                      
                    cmd.Parameters.Add(new SqlParameter("@SiteId", SqlDbType.Int));
                    cmd.Parameters["@SiteId"].Value = taskRemarks.SiteId;

                    cmd.Parameters.Add(new SqlParameter("@ItemFoundId", SqlDbType.Int));
                    cmd.Parameters["@ItemFoundId"].Value = taskRemarks.ItemFoundId;

                    cmd.Parameters.Add(new SqlParameter("@ItemOwnerId", SqlDbType.Int));
                    cmd.Parameters["@ItemOwnerId"].Value = taskRemarks.ItemOwnerId;

                    cmd.Parameters.Add(new SqlParameter("@ItemLostId", SqlDbType.Int));
                    cmd.Parameters["@ItemLostId"].Value = taskRemarks.ItemLostId;

                    cmd.Parameters.Add(new SqlParameter("@ItemDisposeId", SqlDbType.Int));
                    cmd.Parameters["@ItemDisposeId"].Value = taskRemarks.ItemDisposeId;

                    cmd.Parameters.Add(new SqlParameter("@ItemInquiryId", SqlDbType.Int));
                    cmd.Parameters["@ItemInquiryId"].Value = taskRemarks.ItemInquiryId;
                        
        SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.CommandText = "InsertItemFoundRemarksAudit";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                }

            }
            return id;
        }
    }
}
